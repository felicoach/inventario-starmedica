<?php

	#########################################################
	/*
	~~~~~~ LIST OF FUNCTIONS ~~~~~~
		getTableList() -- returns an associative array (tableName => tableData, tableData is array(tableCaption, tableDescription, tableIcon)) of tables accessible by current user
		get_table_groups() -- returns an associative array (table_group => tables_array)
		logInMember() -- checks POST login. If not valid, redirects to index.php, else returns TRUE
		getTablePermissions($tn) -- returns an array of permissions allowed for logged member to given table (allowAccess, allowInsert, allowView, allowEdit, allowDelete) -- allowAccess is set to true if any access level is allowed
		get_sql_fields($tn) -- returns the SELECT part of the table view query
		get_sql_from($tn[, true, [, false]]) -- returns the FROM part of the table view query, with full joins (unless third paramaeter is set to true), optionally skipping permissions if true passed as 2nd param.
		get_joined_record($table, $id[, true]) -- returns assoc array of record values for given PK value of given table, with full joins, optionally skipping permissions if true passed as 3rd param.
		get_defaults($table) -- returns assoc array of table fields as array keys and default values (or empty), excluding automatic values as array values
		htmlUserBar() -- returns html code for displaying user login status to be used on top of pages.
		showNotifications($msg, $class) -- returns html code for displaying a notification. If no parameters provided, processes the GET request for possible notifications.
		parseMySQLDate(a, b) -- returns a if valid mysql date, or b if valid mysql date, or today if b is true, or empty if b is false.
		parseCode(code) -- calculates and returns special values to be inserted in automatic fields.
		addFilter(i, filterAnd, filterField, filterOperator, filterValue) -- enforce a filter over data
		clearFilters() -- clear all filters
		loadView($view, $data) -- passes $data to templates/{$view}.php and returns the output
		loadTable($table, $data) -- loads table template, passing $data to it
		filterDropdownBy($filterable, $filterers, $parentFilterers, $parentPKField, $parentCaption, $parentTable, &$filterableCombo) -- applies cascading drop-downs for a lookup field, returns js code to be inserted into the page
		br2nl($text) -- replaces all variations of HTML <br> tags with a new line character
		htmlspecialchars_decode($text) -- inverse of htmlspecialchars()
		entitiesToUTF8($text) -- convert unicode entities (e.g. &#1234;) to actual UTF8 characters, requires multibyte string PHP extension
		func_get_args_byref() -- returns an array of arguments passed to a function, by reference
		permissions_sql($table, $level) -- returns an array containing the FROM and WHERE additions for applying permissions to an SQL query
		error_message($msg[, $back_url]) -- returns html code for a styled error message .. pass explicit false in second param to suppress back button
		toMySQLDate($formattedDate, $sep = datalist_date_separator, $ord = datalist_date_format)
		reIndex(&$arr) -- returns a copy of the given array, with keys replaced by 1-based numeric indices, and values replaced by original keys
		get_embed($provider, $url[, $width, $height, $retrieve]) -- returns embed code for a given url (supported providers: youtube, googlemap)
		check_record_permission($table, $id, $perm = 'view') -- returns true if current user has the specified permission $perm ('view', 'edit' or 'delete') for the given recors, false otherwise
		NavMenus($options) -- returns the HTML code for the top navigation menus. $options is not implemented currently.
		StyleSheet() -- returns the HTML code for included style sheet files to be placed in the <head> section.
		getUploadDir($dir) -- if dir is empty, returns upload dir configured in defaultLang.php, else returns $dir.
		PrepareUploadedFile($FieldName, $MaxSize, $FileTypes='jpg|jpeg|gif|png', $NoRename=false, $dir="") -- validates and moves uploaded file for given $FieldName into the given $dir (or the default one if empty)
		get_home_links($homeLinks, $default_classes, $tgroup) -- process $homeLinks array and return custom links for homepage. Applies $default_classes to links if links have classes defined, and filters links by $tgroup (using '*' matches all table_group values)
		quick_search_html($search_term, $label, $separate_dv = true) -- returns HTML code for the quick search box.
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	*/

	#########################################################

	function getTableList($skip_authentication = false) {
		$arrAccessTables = array();
		$arrTables = array(
			/* 'table_name' => ['table caption', 'homepage description', 'icon', 'table group name'] */   
			'dispositivos' => array('Dispositivos', 'Dispositivos digitales biomedicos y accesorios dentro de la unidad', 'resources/table_icons/building_add.png', 'INVENTARIO'),
			'mobiliario' => array('Mobiliario', 'Inventario del mobiliario para cada unidad', 'resources/table_icons/brick_add.png', 'INVENTARIO'),
			'compras' => array('Compras', 'Registro de compras realiado para los dispositivos', 'resources/table_icons/book_edit.png', 'INVENTARIO'),
			'contactos' => array('Contactos', 'Proveedores, Clientes, Socios y Apoyo', 'resources/table_icons/ceo.png', 'INVENTARIO'),
			'movimientos' => array('Movimientos', 'Registro de movimientos de dispositivos', 'resources/table_icons/arrow_branch.png', 'INVENTARIO'),
			'documentos' => array('Documentos del dispositivo', 'Carga de documentos por dispositivo', 'resources/table_icons/page_add.png', 'DOCUMENTOS'),
			'mantenimientos' => array('Mantenimientos', 'Registro de mantenimientos a equipos biomedicos', 'resources/table_icons/book_add.png', 'INVENTARIO'),
			'calibraciones' => array('Calibraciones', 'Registro de calibraciones a equipos biomedicos', 'resources/table_icons/document_next.png', 'INVENTARIO'),
			'unidades' => array('Unidades Medicas', 'Informacion de las unidades medicas', 'resources/table_icons/building_add.png', 'INVENTARIO'),
			'marcas' => array('Marcas', '', 'resources/table_icons/application_form_add.png', 'INVENTARIO'),
			'marca_modelo' => array('Marca y Modelo', 'Marca y Modelo del dispositivo', 'resources/table_icons/book_next.png', 'INVENTARIO'),
			'accesorios' => array('Accesorios', '', 'resources/table_icons/basket_add.png', 'INVENTARIO'),
			'ciudades' => array('Ciudades', '', 'resources/table_icons/ftp.png', 'CONFIGURACION'),
			'imagenes' => array('Imagenes', '', 'resources/table_icons/slideshow.png', 'CONFIGURACION'),
			'verificacion' => array('Verificacion', '', 'resources/table_icons/accept.png', 'INVENTARIO'),
			'fichatecnica' => array('Ficha Tecnica', 'Caracteristicas tecnicas del dispositivo', 'resources/table_icons/application_view_list.png', 'DOCUMENTOS'),
			'docu_modelo' => array('Documentos del modelo', '', 'resources/table_icons/document_import.png', 'DOCUMENTOS'),
			'tipo_relacion' => array('Tipo de relacion', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_dispositivo' => array('Tipo de dispositivo', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_documento' => array('Tipo de documento', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_iden' => array('Tipo de identificacion', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_razon_social' => array('Tipo de razon social', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_contacto' => array('Tipo de contacto', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_mobiliario' => array('Tipo de Mobiliario', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_estado_dispo' => array('Estado del dispositivo', 'Estado del dispositivo', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'grupo_dispo' => array('Grupo del dispositivo', '', 'resources/table_icons/cog.png', 'CONFIGURACION'),
			'tipo_movimiento' => array('Tipo de movimiento', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_estado_movi' => array('Estado del movimiento', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_estado_verifica' => array('Tipo estado de verificacion', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_mtto' => array('Tipo de mantenimiento', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_calibracion' => array('Tipo calibracion', '', 'resources/table_icons/cog_add.png', 'CONFIGURACION'),
			'tipo_grupo_mobilia' => array('Tipo grupo mobilia', '', 'resources/table_icons/application_add.png', 'CONFIGURACION')
		);
		if($skip_authentication || getLoggedAdmin()) return $arrTables;

		if(is_array($arrTables)) {
			foreach($arrTables as $tn => $tc) {
				$arrPerm = getTablePermissions($tn);
				if($arrPerm[0]) {
					$arrAccessTables[$tn] = $tc;
				}
			}
		}

		return $arrAccessTables;
	}

	#########################################################

	function get_table_groups($skip_authentication = false) {
		$tables = getTableList($skip_authentication);
		$all_groups = array('None', 'INVENTARIO', 'DOCUMENTOS', 'CONFIGURACION');

		$groups = array();
		foreach($all_groups as $grp) {
			foreach($tables as $tn => $td) {
				if($td[3] && $td[3] == $grp) $groups[$grp][] = $tn;
				if(!$td[3]) $groups[0][] = $tn;
			}
		}

		return $groups;
	}

	#########################################################

	function getTablePermissions($tn) {
		static $table_permissions = array();
		if(isset($table_permissions[$tn])) return $table_permissions[$tn];

		$groupID = getLoggedGroupID();
		$memberID = makeSafe(getLoggedMemberID());
		$res_group = sql("select tableName, allowInsert, allowView, allowEdit, allowDelete from membership_grouppermissions where groupID='{$groupID}'", $eo);
		$res_user = sql("select tableName, allowInsert, allowView, allowEdit, allowDelete from membership_userpermissions where lcase(memberID)='{$memberID}'", $eo);

		while($row = db_fetch_assoc($res_group)) {
			$table_permissions[$row['tableName']] = array(
				1 => intval($row['allowInsert']),
				2 => intval($row['allowView']),
				3 => intval($row['allowEdit']),
				4 => intval($row['allowDelete']),
				'insert' => intval($row['allowInsert']),
				'view' => intval($row['allowView']),
				'edit' => intval($row['allowEdit']),
				'delete' => intval($row['allowDelete'])
			);
		}

		// user-specific permissions, if specified, overwrite his group permissions
		while($row = db_fetch_assoc($res_user)) {
			$table_permissions[$row['tableName']] = array(
				1 => intval($row['allowInsert']),
				2 => intval($row['allowView']),
				3 => intval($row['allowEdit']),
				4 => intval($row['allowDelete']),
				'insert' => intval($row['allowInsert']),
				'view' => intval($row['allowView']),
				'edit' => intval($row['allowEdit']),
				'delete' => intval($row['allowDelete'])
			);
		}

		// if user has any type of access, set 'access' flag
		foreach($table_permissions as $t => $p) {
			$table_permissions[$t]['access'] = $table_permissions[$t][0] = false;

			if($p['insert'] || $p['view'] || $p['edit'] || $p['delete']) {
				$table_permissions[$t]['access'] = $table_permissions[$t][0] = true;
			}
		}

		return $table_permissions[$tn];
	}

	#########################################################

	function get_sql_fields($table_name) {
		$sql_fields = array(
			'dispositivos' => "`dispositivos`.`id_dispo` as 'id_dispo', IF(    CHAR_LENGTH(`marca_modelo1`.`img`), CONCAT_WS('',   `marca_modelo1`.`img`), '') as 'foto', `dispositivos`.`code` as 'code', `dispositivos`.`codigo` as 'codigo', `dispositivos`.`serial` as 'serial', IF(    CHAR_LENGTH(`tipo_dispositivo1`.`tipo_dispositivo`), CONCAT_WS('',   `tipo_dispositivo1`.`tipo_dispositivo`), '') as 'tipo_dispositivo', IF(    CHAR_LENGTH(`marcas1`.`marca`), CONCAT_WS('',   `marcas1`.`marca`), '') as 'marca', IF(    CHAR_LENGTH(`marca_modelo1`.`modelo`), CONCAT_WS('',   `marca_modelo1`.`modelo`), '') as 'modelo', IF(    CHAR_LENGTH(`unidades1`.`nombre`), CONCAT_WS('',   `unidades1`.`nombre`), '') as 'ubicacion', IF(    CHAR_LENGTH(`unidades1`.`abreviado`), CONCAT_WS('',   `unidades1`.`abreviado`), '') as 'ubicacion_abre', if(`dispositivos`.`fecha_ingreso`,date_format(`dispositivos`.`fecha_ingreso`,'%d/%m/%Y'),'') as 'fecha_ingreso', IF(    CHAR_LENGTH(`tipo_estado_dispo1`.`estado_dispo`), CONCAT_WS('',   `tipo_estado_dispo1`.`estado_dispo`), '') as 'estado', `dispositivos`.`descripcion` as 'descripcion', if(`dispositivos`.`creado`,date_format(`dispositivos`.`creado`,'%d/%m/%Y %h:%i %p'),'') as 'creado', if(`dispositivos`.`creado1`,date_format(`dispositivos`.`creado1`,'%d/%m/%Y %h:%i %p'),'') as 'creado1', `dispositivos`.`editado` as 'editado', `dispositivos`.`creado_por` as 'creado_por', `dispositivos`.`editado_por` as 'editado_por'",
			'mobiliario' => "`mobiliario`.`id_mobiliario` as 'id_mobiliario', `mobiliario`.`codigo` as 'codigo', IF(    CHAR_LENGTH(`tipo_grupo_mobilia1`.`grupo`), CONCAT_WS('',   `tipo_grupo_mobilia1`.`grupo`), '') as 'grupo', IF(    CHAR_LENGTH(`tipo_mobiliario1`.`tipo_mobiliario`), CONCAT_WS('',   `tipo_mobiliario1`.`tipo_mobiliario`), '') as 'tipo_mobiliario', `mobiliario`.`fecha_ingreso` as 'fecha_ingreso', IF(    CHAR_LENGTH(`unidades1`.`nombre`), CONCAT_WS('',   `unidades1`.`nombre`), '') as 'unidad', IF(    CHAR_LENGTH(`unidades1`.`abreviado`), CONCAT_WS('',   `unidades1`.`abreviado`), '') as 'uni_abreviado', `mobiliario`.`descripcion` as 'descripcion', `mobiliario`.`accesorios` as 'accesorios', `mobiliario`.`foto` as 'foto', if(`mobiliario`.`creado`,date_format(`mobiliario`.`creado`,'%d/%m/%Y %h:%i %p'),'') as 'creado', `mobiliario`.`creado_por` as 'creado_por', `mobiliario`.`editado` as 'editado', `mobiliario`.`editado_por` as 'editado_por'",
			'compras' => "`compras`.`id_compras` as 'id_compras', IF(    CHAR_LENGTH(`tipo_dispositivo1`.`tipo_dispositivo`), CONCAT_WS('',   `tipo_dispositivo1`.`tipo_dispositivo`), '') as 'tipo_dispo', `compras`.`serial` as 'serial', IF(    CHAR_LENGTH(`marcas1`.`marca`), CONCAT_WS('',   `marcas1`.`marca`), '') as 'marca', IF(    CHAR_LENGTH(`marca_modelo1`.`modelo`), CONCAT_WS('',   `marca_modelo1`.`modelo`), '') as 'modelo', `compras`.`referencia` as 'referencia', if(`compras`.`fecha_compra`,date_format(`compras`.`fecha_compra`,'%d/%m/%Y'),'') as 'fecha_compra', IF(    CHAR_LENGTH(`contactos1`.`nombre`), CONCAT_WS('',   `contactos1`.`nombre`), '') as 'proveedor', IF(    CHAR_LENGTH(`tipo_iden1`.`tipo_iden_abre`) || CHAR_LENGTH(`contactos1`.`identificacion`), CONCAT_WS('',   `tipo_iden1`.`tipo_iden_abre`, ' ', ' ', `contactos1`.`identificacion`), '') as 'identi', IF(    CHAR_LENGTH(`contactos1`.`telefono`), CONCAT_WS('',   `contactos1`.`telefono`), '') as 'tel_provededor', IF(    CHAR_LENGTH(`ciudades1`.`ciudad`), CONCAT_WS('',   `ciudades1`.`ciudad`), '') as 'ciudad_origen', if(`compras`.`fecha_factura`,date_format(`compras`.`fecha_factura`,'%d/%m/%Y'),'') as 'fecha_factura', `compras`.`factura` as 'factura', `compras`.`precio` as 'precio', IF(    CHAR_LENGTH(`tipo_razon_social1`.`razon_social`), CONCAT_WS('',   `tipo_razon_social1`.`razon_social`), '') as 'comprado_por', if(`compras`.`garantia`,date_format(`compras`.`garantia`,'%d/%m/%Y'),'') as 'garantia', `compras`.`descripcion` as 'descripcion', `compras`.`foto` as 'foto', `compras`.`soporte` as 'soporte', `compras`.`ficha_tecnica` as 'ficha_tecnica', `compras`.`otro_documento` as 'otro_documento', `compras`.`comentarios` as 'comentarios', `compras`.`reg_creado` as 'reg_creado', `compras`.`update` as 'update', `compras`.`creado_por` as 'creado_por', `compras`.`editado_por` as 'editado_por', if(`compras`.`creado`,date_format(`compras`.`creado`,'%d/%m/%Y %h:%i %p'),'') as 'creado', if(`compras`.`editado`,date_format(`compras`.`editado`,'%d/%m/%Y %h:%i %p'),'') as 'editado'",
			'contactos' => "`contactos`.`id_contactos` as 'id_contactos', IF(    CHAR_LENGTH(`tipo_contacto1`.`tipo_contacto`), CONCAT_WS('',   `tipo_contacto1`.`tipo_contacto`), '') as 'tipo_contacto', IF(    CHAR_LENGTH(`tipo_iden1`.`tipo_iden_abre`), CONCAT_WS('',   `tipo_iden1`.`tipo_iden_abre`), '') as 'tipo_iden', `contactos`.`identificacion` as 'identificacion', `contactos`.`nombre` as 'nombre', IF(    CHAR_LENGTH(`ciudades1`.`ciudad`), CONCAT_WS('',   `ciudades1`.`ciudad`), '') as 'ciudad', `contactos`.`telefono` as 'telefono', `contactos`.`direccion` as 'direccion', IF(    CHAR_LENGTH(`tipo_relacion1`.`tipo_relac`), CONCAT_WS('',   `tipo_relacion1`.`tipo_relac`), '') as 'tipo_relacion', `contactos`.`nota` as 'nota', if(`contactos`.`reg_creado`,date_format(`contactos`.`reg_creado`,'%d/%m/%Y %h:%i %p'),'') as 'reg_creado', if(`contactos`.`update`,date_format(`contactos`.`update`,'%d/%m/%Y %h:%i %p'),'') as 'update', `contactos`.`creado_por` as 'creado_por', `contactos`.`editado_por` as 'editado_por', if(`contactos`.`creado`,date_format(`contactos`.`creado`,'%d/%m/%Y %h:%i %p'),'') as 'creado', if(`contactos`.`editado`,date_format(`contactos`.`editado`,'%d/%m/%Y %h:%i %p'),'') as 'editado'",
			'movimientos' => "`movimientos`.`id_movi` as 'id_movi', IF(    CHAR_LENGTH(`tipo_movimiento1`.`tipo_movimiento`), CONCAT_WS('',   `tipo_movimiento1`.`tipo_movimiento`), '') as 'tipo_movimiento', IF(    CHAR_LENGTH(`dispositivos1`.`code`), CONCAT_WS('',   `dispositivos1`.`code`), '') as 'codigo_dispo', IF(    CHAR_LENGTH(`dispositivos1`.`serial`), CONCAT_WS('',   `dispositivos1`.`serial`), '') as 'serial_dispo', IF(    CHAR_LENGTH(`tipo_dispositivo1`.`tipo_dispositivo`), CONCAT_WS('',   `tipo_dispositivo1`.`tipo_dispositivo`), '') as 'tipo_dispo', IF(    CHAR_LENGTH(`unidades1`.`nombre`), CONCAT_WS('',   `unidades1`.`nombre`), '') as 'origen', if(`movimientos`.`fecha_movimiento`,date_format(`movimientos`.`fecha_movimiento`,'%d/%m/%Y'),'') as 'fecha_movimiento', IF(    CHAR_LENGTH(`unidades2`.`nombre`), CONCAT_WS('',   `unidades2`.`nombre`), '') as 'destino', IF(    CHAR_LENGTH(`contactos1`.`nombre`), CONCAT_WS('',   `contactos1`.`nombre`), '') as 'responsable', IF(    CHAR_LENGTH(`contactos1`.`telefono`), CONCAT_WS('',   `contactos1`.`telefono`), '') as 'telefono_respon', IF(    CHAR_LENGTH(`tipo_estado_movi1`.`estado_movi`), CONCAT_WS('',   `tipo_estado_movi1`.`estado_movi`), '') as 'estado_movi', `movimientos`.`comentarios` as 'comentarios', `movimientos`.`documento` as 'documento', `movimientos`.`creado` as 'creado', `movimientos`.`creado_por` as 'creado_por', `movimientos`.`update` as 'update', `movimientos`.`editado_por` as 'editado_por', if(`movimientos`.`editado`,date_format(`movimientos`.`editado`,'%d/%m/%Y %h:%i %p'),'') as 'editado'",
			'documentos' => "`documentos`.`id` as 'id', IF(    CHAR_LENGTH(`dispositivos1`.`code`), CONCAT_WS('',   `dispositivos1`.`code`), '') as 'dispo_codigo', IF(    CHAR_LENGTH(`dispositivos1`.`serial`), CONCAT_WS('',   `dispositivos1`.`serial`), '') as 'dispo_serial', IF(    CHAR_LENGTH(`tipo_documento1`.`tipo_doc`), CONCAT_WS('',   `tipo_documento1`.`tipo_doc`), '') as 'tipo_docu', `documentos`.`enlace` as 'enlace', `documentos`.`carga` as 'carga', `documentos`.`descripcion` as 'descripcion', `documentos`.`fecha_carga` as 'fecha_carga', `documentos`.`timestamp` as 'timestamp', `documentos`.`update` as 'update', `documentos`.`creado_por` as 'creado_por', `documentos`.`editado_por` as 'editado_por', `documentos`.`creado` as 'creado', `documentos`.`editado` as 'editado'",
			'mantenimientos' => "`mantenimientos`.`id_mtto` as 'id_mtto', IF(    CHAR_LENGTH(`dispositivos1`.`code`), CONCAT_WS('',   `dispositivos1`.`code`), '') as 'codigo', IF(    CHAR_LENGTH(`dispositivos1`.`serial`), CONCAT_WS('',   `dispositivos1`.`serial`), '') as 'serial', if(`mantenimientos`.`fecha_mtto`,date_format(`mantenimientos`.`fecha_mtto`,'%d/%m/%Y'),'') as 'fecha_mtto', IF(    CHAR_LENGTH(`tipo_mtto1`.`tipo_mtto`), CONCAT_WS('',   `tipo_mtto1`.`tipo_mtto`), '') as 'tipo_mtto', IF(    CHAR_LENGTH(`contactos1`.`nombre`), CONCAT_WS('',   `contactos1`.`nombre`), '') as 'responsable', IF(    CHAR_LENGTH(`contactos1`.`identificacion`), CONCAT_WS('',   `contactos1`.`identificacion`), '') as 'id_responsable', `mantenimientos`.`documento` as 'documento', `mantenimientos`.`timestamp` as 'timestamp', `mantenimientos`.`update` as 'update', `mantenimientos`.`creado_por` as 'creado_por', `mantenimientos`.`editado_por` as 'editado_por', if(`mantenimientos`.`creado`,date_format(`mantenimientos`.`creado`,'%d/%m/%Y %h:%i %p'),'') as 'creado', if(`mantenimientos`.`editado`,date_format(`mantenimientos`.`editado`,'%d/%m/%Y %h:%i %p'),'') as 'editado'",
			'calibraciones' => "`calibraciones`.`id` as 'id', IF(    CHAR_LENGTH(`dispositivos1`.`code`), CONCAT_WS('',   `dispositivos1`.`code`), '') as 'codigo', IF(    CHAR_LENGTH(`dispositivos1`.`serial`), CONCAT_WS('',   `dispositivos1`.`serial`), '') as 'serial', IF(    CHAR_LENGTH(`tipo_calibracion1`.`tipo_calibracion`), CONCAT_WS('',   `tipo_calibracion1`.`tipo_calibracion`), '') as 'tipo_calibracion', if(`calibraciones`.`fecha_calibra`,date_format(`calibraciones`.`fecha_calibra`,'%d/%m/%Y'),'') as 'fecha_calibra', if(`calibraciones`.`fecha_expiracion`,date_format(`calibraciones`.`fecha_expiracion`,'%d/%m/%Y'),'') as 'fecha_expiracion', IF(    CHAR_LENGTH(`contactos1`.`nombre`), CONCAT_WS('',   `contactos1`.`nombre`), '') as 'empresa', `calibraciones`.`documento` as 'documento', `calibraciones`.`descripcion` as 'descripcion', `calibraciones`.`timestamp` as 'timestamp', `calibraciones`.`update` as 'update', `calibraciones`.`creado_por` as 'creado_por', `calibraciones`.`editado_por` as 'editado_por'",
			'unidades' => "`unidades`.`id_unidades` as 'id_unidades', `unidades`.`nombre` as 'nombre', `unidades`.`abreviado` as 'abreviado', IF(    CHAR_LENGTH(`ciudades1`.`ciudad`), CONCAT_WS('',   `ciudades1`.`ciudad`), '') as 'ciudad', `unidades`.`telefono` as 'telefono', IF(    CHAR_LENGTH(`tipo_razon_social1`.`razon_social`), CONCAT_WS('',   `tipo_razon_social1`.`razon_social`), '') as 'razon_social', `unidades`.`logo` as 'logo'",
			'marcas' => "`marcas`.`id_marca` as 'id_marca', `marcas`.`marca` as 'marca', `marcas`.`descripcion` as 'descripcion'",
			'marca_modelo' => "`marca_modelo`.`id_mmodelo` as 'id_mmodelo', IF(    CHAR_LENGTH(`marcas1`.`marca`), CONCAT_WS('',   `marcas1`.`marca`), '') as 'marca', `marca_modelo`.`modelo` as 'modelo', `marca_modelo`.`img` as 'img', `marca_modelo`.`fabricante` as 'fabricante', `marca_modelo`.`descripcion` as 'descripcion'",
			'accesorios' => "`accesorios`.`id` as 'id', IF(    CHAR_LENGTH(`dispositivos1`.`id_dispo`), CONCAT_WS('',   `dispositivos1`.`id_dispo`), '') as 'id_dispo', IF(    CHAR_LENGTH(`dispositivos2`.`code`), CONCAT_WS('',   `dispositivos2`.`code`), '') as 'iddispo', IF(    CHAR_LENGTH(`dispositivos3`.`code`), CONCAT_WS('',   `dispositivos3`.`code`), '') as 'Accesorio', if(`accesorios`.`fecha_asignacion`,date_format(`accesorios`.`fecha_asignacion`,'%d/%m/%Y'),'') as 'fecha_asignacion', `accesorios`.`comentarios` as 'comentarios'",
			'ciudades' => "`ciudades`.`id_ciudad` as 'id_ciudad', `ciudades`.`ciudad` as 'ciudad', `ciudades`.`descripcion` as 'descripcion'",
			'imagenes' => "`imagenes`.`id` as 'id', `imagenes`.`imagen` as 'imagen', IF(    CHAR_LENGTH(`tipo_dispositivo1`.`tipo_dispositivo`), CONCAT_WS('',   `tipo_dispositivo1`.`tipo_dispositivo`), '') as 'tipo_dispo', IF(    CHAR_LENGTH(`marcas1`.`marca`), CONCAT_WS('',   `marcas1`.`marca`), '') as 'marca', IF(    CHAR_LENGTH(`marca_modelo1`.`modelo`), CONCAT_WS('',   `marca_modelo1`.`modelo`), '') as 'modelo', `imagenes`.`descripcion` as 'descripcion'",
			'verificacion' => "`verificacion`.`id` as 'id', IF(    CHAR_LENGTH(`dispositivos1`.`codigo`), CONCAT_WS('',   `dispositivos1`.`codigo`), '') as 'codigo', IF(    CHAR_LENGTH(`tipo_dispositivo1`.`tipo_dispositivo`), CONCAT_WS('',   `tipo_dispositivo1`.`tipo_dispositivo`), '') as 'tipo_dispo', IF(    CHAR_LENGTH(`dispositivos1`.`serial`), CONCAT_WS('',   `dispositivos1`.`serial`), '') as 'serial', IF(    CHAR_LENGTH(`unidades1`.`nombre`), CONCAT_WS('',   `unidades1`.`nombre`), '') as 'ubicacion', `verificacion`.`verificacion` as 'verificacion', `verificacion`.`comentario` as 'comentario', `verificacion`.`verifi_por` as 'verifi_por', `verificacion`.`fecha_verifi` as 'fecha_verifi'",
			'fichatecnica' => "`fichatecnica`.`id` as 'id', IF(    CHAR_LENGTH(`dispositivos1`.`code`), CONCAT_WS('',   `dispositivos1`.`code`), '') as 'dispo_codigo', IF(    CHAR_LENGTH(`dispositivos1`.`serial`), CONCAT_WS('',   `dispositivos1`.`serial`), '') as 'dispo_serial', `fichatecnica`.`hardware` as 'hardware', `fichatecnica`.`software` as 'software', `fichatecnica`.`enlace` as 'enlace', `fichatecnica`.`carga` as 'carga', if(`fichatecnica`.`fecha_carga`,date_format(`fichatecnica`.`fecha_carga`,'%d/%m/%Y'),'') as 'fecha_carga', `fichatecnica`.`creado` as 'creado', `fichatecnica`.`creado_por` as 'creado_por', if(`fichatecnica`.`editado`,date_format(`fichatecnica`.`editado`,'%d/%m/%Y %h:%i %p'),'') as 'editado', `fichatecnica`.`editado_por` as 'editado_por'",
			'docu_modelo' => "`docu_modelo`.`id` as 'id', IF(    CHAR_LENGTH(`tipo_dispositivo1`.`tipo_dispositivo`), CONCAT_WS('',   `tipo_dispositivo1`.`tipo_dispositivo`), '') as 'tipo_dispo', IF(    CHAR_LENGTH(`marcas1`.`marca`), CONCAT_WS('',   `marcas1`.`marca`), '') as 'marca', IF(    CHAR_LENGTH(`marca_modelo1`.`modelo`), CONCAT_WS('',   `marca_modelo1`.`modelo`), '') as 'modelo', IF(    CHAR_LENGTH(`tipo_documento1`.`tipo_doc`), CONCAT_WS('',   `tipo_documento1`.`tipo_doc`), '') as 'tipo_documento', `docu_modelo`.`documento` as 'documento', `docu_modelo`.`descripcion` as 'descripcion', `docu_modelo`.`creado` as 'creado', `docu_modelo`.`creado_por` as 'creado_por', `docu_modelo`.`editado` as 'editado', `docu_modelo`.`editado_por` as 'editado_por'",
			'tipo_relacion' => "`tipo_relacion`.`id_tiporelac` as 'id_tiporelac', `tipo_relacion`.`tipo_relac` as 'tipo_relac', `tipo_relacion`.`descripcion` as 'descripcion'",
			'tipo_dispositivo' => "`tipo_dispositivo`.`id_tipodispo` as 'id_tipodispo', `tipo_dispositivo`.`tipo_dispositivo` as 'tipo_dispositivo', IF(    CHAR_LENGTH(`grupo_dispo1`.`grupo_dispo`), CONCAT_WS('',   `grupo_dispo1`.`grupo_dispo`), '') as 'grupo_dispo', `tipo_dispositivo`.`prefijo` as 'prefijo', `tipo_dispositivo`.`descripcion` as 'descripcion'",
			'tipo_documento' => "`tipo_documento`.`id_tipodoc` as 'id_tipodoc', `tipo_documento`.`tipo_doc` as 'tipo_doc', `tipo_documento`.`descripcion` as 'descripcion'",
			'tipo_iden' => "`tipo_iden`.`id_iden` as 'id_iden', `tipo_iden`.`tipo_iden` as 'tipo_iden', `tipo_iden`.`tipo_iden_abre` as 'tipo_iden_abre'",
			'tipo_razon_social' => "`tipo_razon_social`.`id` as 'id', `tipo_razon_social`.`razon_social` as 'razon_social', `tipo_razon_social`.`nit` as 'nit', `tipo_razon_social`.`descripcion` as 'descripcion'",
			'tipo_contacto' => "`tipo_contacto`.`id_tipo_contacto` as 'id_tipo_contacto', `tipo_contacto`.`tipo_contacto` as 'tipo_contacto', `tipo_contacto`.`descripcion` as 'descripcion'",
			'tipo_mobiliario' => "`tipo_mobiliario`.`id` as 'id', IF(    CHAR_LENGTH(`tipo_grupo_mobilia1`.`grupo`), CONCAT_WS('',   `tipo_grupo_mobilia1`.`grupo`), '') as 'grupo_mobi', `tipo_mobiliario`.`tipo_mobiliario` as 'tipo_mobiliario', `tipo_mobiliario`.`descripcion` as 'descripcion'",
			'tipo_estado_dispo' => "`tipo_estado_dispo`.`id` as 'id', `tipo_estado_dispo`.`estado_dispo` as 'estado_dispo', `tipo_estado_dispo`.`descripcion` as 'descripcion'",
			'grupo_dispo' => "`grupo_dispo`.`id_grupo_dispo` as 'id_grupo_dispo', `grupo_dispo`.`grupo_dispo` as 'grupo_dispo', `grupo_dispo`.`descripcion` as 'descripcion'",
			'tipo_movimiento' => "`tipo_movimiento`.`id` as 'id', `tipo_movimiento`.`tipo_movimiento` as 'tipo_movimiento', `tipo_movimiento`.`descripcion` as 'descripcion'",
			'tipo_estado_movi' => "`tipo_estado_movi`.`id` as 'id', `tipo_estado_movi`.`estado_movi` as 'estado_movi', `tipo_estado_movi`.`descripcion` as 'descripcion'",
			'tipo_estado_verifica' => "`tipo_estado_verifica`.`id` as 'id', `tipo_estado_verifica`.`estado_verificado` as 'estado_verificado', `tipo_estado_verifica`.`descripcion` as 'descripcion'",
			'tipo_mtto' => "`tipo_mtto`.`id` as 'id', `tipo_mtto`.`tipo_mtto` as 'tipo_mtto', `tipo_mtto`.`descripcion` as 'descripcion'",
			'tipo_calibracion' => "`tipo_calibracion`.`id` as 'id', `tipo_calibracion`.`tipo_calibracion` as 'tipo_calibracion', `tipo_calibracion`.`descripcion` as 'descripcion'",
			'tipo_grupo_mobilia' => "`tipo_grupo_mobilia`.`id_gru_mo` as 'id_gru_mo', `tipo_grupo_mobilia`.`grupo` as 'grupo', `tipo_grupo_mobilia`.`prefijo` as 'prefijo', `tipo_grupo_mobilia`.`descripcion` as 'descripcion'",
		);

		if(isset($sql_fields[$table_name])) {
			return $sql_fields[$table_name];
		}

		return false;
	}

	#########################################################

	function get_sql_from($table_name, $skip_permissions = false, $skip_joins = false) {
		$sql_from = array(
			'dispositivos' => "`dispositivos` LEFT JOIN `tipo_dispositivo` as tipo_dispositivo1 ON `tipo_dispositivo1`.`id_tipodispo`=`dispositivos`.`tipo_dispositivo` LEFT JOIN `marcas` as marcas1 ON `marcas1`.`id_marca`=`dispositivos`.`marca` LEFT JOIN `marca_modelo` as marca_modelo1 ON `marca_modelo1`.`id_mmodelo`=`dispositivos`.`modelo` LEFT JOIN `unidades` as unidades1 ON `unidades1`.`id_unidades`=`dispositivos`.`ubicacion` LEFT JOIN `tipo_estado_dispo` as tipo_estado_dispo1 ON `tipo_estado_dispo1`.`id`=`dispositivos`.`estado` ",
			'mobiliario' => "`mobiliario` LEFT JOIN `tipo_grupo_mobilia` as tipo_grupo_mobilia1 ON `tipo_grupo_mobilia1`.`id_gru_mo`=`mobiliario`.`grupo` LEFT JOIN `tipo_mobiliario` as tipo_mobiliario1 ON `tipo_mobiliario1`.`id`=`mobiliario`.`tipo_mobiliario` LEFT JOIN `unidades` as unidades1 ON `unidades1`.`id_unidades`=`mobiliario`.`unidad` ",
			'compras' => "`compras` LEFT JOIN `tipo_dispositivo` as tipo_dispositivo1 ON `tipo_dispositivo1`.`id_tipodispo`=`compras`.`tipo_dispo` LEFT JOIN `marcas` as marcas1 ON `marcas1`.`id_marca`=`compras`.`marca` LEFT JOIN `marca_modelo` as marca_modelo1 ON `marca_modelo1`.`id_mmodelo`=`compras`.`modelo` LEFT JOIN `contactos` as contactos1 ON `contactos1`.`id_contactos`=`compras`.`proveedor` LEFT JOIN `ciudades` as ciudades1 ON `ciudades1`.`id_ciudad`=`compras`.`ciudad_origen` LEFT JOIN `tipo_razon_social` as tipo_razon_social1 ON `tipo_razon_social1`.`id`=`compras`.`comprado_por` LEFT JOIN `tipo_iden` as tipo_iden1 ON `tipo_iden1`.`id_iden`=`contactos1`.`tipo_iden` ",
			'contactos' => "`contactos` LEFT JOIN `tipo_contacto` as tipo_contacto1 ON `tipo_contacto1`.`id_tipo_contacto`=`contactos`.`tipo_contacto` LEFT JOIN `tipo_iden` as tipo_iden1 ON `tipo_iden1`.`id_iden`=`contactos`.`tipo_iden` LEFT JOIN `ciudades` as ciudades1 ON `ciudades1`.`id_ciudad`=`contactos`.`ciudad` LEFT JOIN `tipo_relacion` as tipo_relacion1 ON `tipo_relacion1`.`id_tiporelac`=`contactos`.`tipo_relacion` ",
			'movimientos' => "`movimientos` LEFT JOIN `tipo_movimiento` as tipo_movimiento1 ON `tipo_movimiento1`.`id`=`movimientos`.`tipo_movimiento` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`movimientos`.`codigo_dispo` LEFT JOIN `unidades` as unidades1 ON `unidades1`.`id_unidades`=`movimientos`.`origen` LEFT JOIN `unidades` as unidades2 ON `unidades2`.`id_unidades`=`movimientos`.`destino` LEFT JOIN `contactos` as contactos1 ON `contactos1`.`id_contactos`=`movimientos`.`responsable` LEFT JOIN `tipo_estado_movi` as tipo_estado_movi1 ON `tipo_estado_movi1`.`id`=`movimientos`.`estado_movi` LEFT JOIN `tipo_dispositivo` as tipo_dispositivo1 ON `tipo_dispositivo1`.`id_tipodispo`=`dispositivos1`.`tipo_dispositivo` ",
			'documentos' => "`documentos` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`documentos`.`dispo_codigo` LEFT JOIN `tipo_documento` as tipo_documento1 ON `tipo_documento1`.`id_tipodoc`=`documentos`.`tipo_docu` ",
			'mantenimientos' => "`mantenimientos` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`mantenimientos`.`codigo` LEFT JOIN `tipo_mtto` as tipo_mtto1 ON `tipo_mtto1`.`id`=`mantenimientos`.`tipo_mtto` LEFT JOIN `contactos` as contactos1 ON `contactos1`.`id_contactos`=`mantenimientos`.`responsable` ",
			'calibraciones' => "`calibraciones` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`calibraciones`.`codigo` LEFT JOIN `tipo_calibracion` as tipo_calibracion1 ON `tipo_calibracion1`.`id`=`calibraciones`.`tipo_calibracion` LEFT JOIN `contactos` as contactos1 ON `contactos1`.`id_contactos`=`calibraciones`.`empresa` ",
			'unidades' => "`unidades` LEFT JOIN `ciudades` as ciudades1 ON `ciudades1`.`id_ciudad`=`unidades`.`ciudad` LEFT JOIN `tipo_razon_social` as tipo_razon_social1 ON `tipo_razon_social1`.`id`=`unidades`.`razon_social` ",
			'marcas' => "`marcas` ",
			'marca_modelo' => "`marca_modelo` LEFT JOIN `marcas` as marcas1 ON `marcas1`.`id_marca`=`marca_modelo`.`marca` ",
			'accesorios' => "`accesorios` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`accesorios`.`id_dispo` LEFT JOIN `dispositivos` as dispositivos2 ON `dispositivos2`.`id_dispo`=`accesorios`.`iddispo` LEFT JOIN `dispositivos` as dispositivos3 ON `dispositivos3`.`id_dispo`=`accesorios`.`Accesorio` ",
			'ciudades' => "`ciudades` ",
			'imagenes' => "`imagenes` LEFT JOIN `tipo_dispositivo` as tipo_dispositivo1 ON `tipo_dispositivo1`.`id_tipodispo`=`imagenes`.`tipo_dispo` LEFT JOIN `marcas` as marcas1 ON `marcas1`.`id_marca`=`imagenes`.`marca` LEFT JOIN `marca_modelo` as marca_modelo1 ON `marca_modelo1`.`id_mmodelo`=`imagenes`.`modelo` ",
			'verificacion' => "`verificacion` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`verificacion`.`codigo` LEFT JOIN `tipo_dispositivo` as tipo_dispositivo1 ON `tipo_dispositivo1`.`id_tipodispo`=`dispositivos1`.`tipo_dispositivo` LEFT JOIN `unidades` as unidades1 ON `unidades1`.`id_unidades`=`dispositivos1`.`ubicacion` ",
			'fichatecnica' => "`fichatecnica` LEFT JOIN `dispositivos` as dispositivos1 ON `dispositivos1`.`id_dispo`=`fichatecnica`.`dispo_codigo` ",
			'docu_modelo' => "`docu_modelo` LEFT JOIN `tipo_dispositivo` as tipo_dispositivo1 ON `tipo_dispositivo1`.`id_tipodispo`=`docu_modelo`.`tipo_dispo` LEFT JOIN `marcas` as marcas1 ON `marcas1`.`id_marca`=`docu_modelo`.`marca` LEFT JOIN `marca_modelo` as marca_modelo1 ON `marca_modelo1`.`id_mmodelo`=`docu_modelo`.`modelo` LEFT JOIN `tipo_documento` as tipo_documento1 ON `tipo_documento1`.`id_tipodoc`=`docu_modelo`.`tipo_documento` ",
			'tipo_relacion' => "`tipo_relacion` ",
			'tipo_dispositivo' => "`tipo_dispositivo` LEFT JOIN `grupo_dispo` as grupo_dispo1 ON `grupo_dispo1`.`id_grupo_dispo`=`tipo_dispositivo`.`grupo_dispo` ",
			'tipo_documento' => "`tipo_documento` ",
			'tipo_iden' => "`tipo_iden` ",
			'tipo_razon_social' => "`tipo_razon_social` ",
			'tipo_contacto' => "`tipo_contacto` ",
			'tipo_mobiliario' => "`tipo_mobiliario` LEFT JOIN `tipo_grupo_mobilia` as tipo_grupo_mobilia1 ON `tipo_grupo_mobilia1`.`id_gru_mo`=`tipo_mobiliario`.`grupo_mobi` ",
			'tipo_estado_dispo' => "`tipo_estado_dispo` ",
			'grupo_dispo' => "`grupo_dispo` ",
			'tipo_movimiento' => "`tipo_movimiento` ",
			'tipo_estado_movi' => "`tipo_estado_movi` ",
			'tipo_estado_verifica' => "`tipo_estado_verifica` ",
			'tipo_mtto' => "`tipo_mtto` ",
			'tipo_calibracion' => "`tipo_calibracion` ",
			'tipo_grupo_mobilia' => "`tipo_grupo_mobilia` ",
		);

		$pkey = array(
			'dispositivos' => 'id_dispo',
			'mobiliario' => 'id_mobiliario',
			'compras' => 'id_compras',
			'contactos' => 'id_contactos',
			'movimientos' => 'id_movi',
			'documentos' => 'id',
			'mantenimientos' => 'id_mtto',
			'calibraciones' => 'id',
			'unidades' => 'id_unidades',
			'marcas' => 'id_marca',
			'marca_modelo' => 'id_mmodelo',
			'accesorios' => 'id',
			'ciudades' => 'id_ciudad',
			'imagenes' => 'id',
			'verificacion' => 'id',
			'fichatecnica' => 'id',
			'docu_modelo' => 'id',
			'tipo_relacion' => 'id_tiporelac',
			'tipo_dispositivo' => 'id_tipodispo',
			'tipo_documento' => 'id_tipodoc',
			'tipo_iden' => 'id_iden',
			'tipo_razon_social' => 'id',
			'tipo_contacto' => 'id_tipo_contacto',
			'tipo_mobiliario' => 'id',
			'tipo_estado_dispo' => 'id',
			'grupo_dispo' => 'id_grupo_dispo',
			'tipo_movimiento' => 'id',
			'tipo_estado_movi' => 'id',
			'tipo_estado_verifica' => 'id',
			'tipo_mtto' => 'id',
			'tipo_calibracion' => 'id',
			'tipo_grupo_mobilia' => 'id_gru_mo',
		);

		if(!isset($sql_from[$table_name])) return false;

		$from = ($skip_joins ? "`{$table_name}`" : $sql_from[$table_name]);

		if($skip_permissions) return $from . ' WHERE 1=1';

		// mm: build the query based on current member's permissions
		$perm = getTablePermissions($table_name);
		if($perm[2] == 1) { // view owner only
			$from .= ", membership_userrecords WHERE `{$table_name}`.`{$pkey[$table_name]}`=membership_userrecords.pkValue and membership_userrecords.tableName='{$table_name}' and lcase(membership_userrecords.memberID)='" . getLoggedMemberID() . "'";
		}elseif($perm[2] == 2) { // view group only
			$from .= ", membership_userrecords WHERE `{$table_name}`.`{$pkey[$table_name]}`=membership_userrecords.pkValue and membership_userrecords.tableName='{$table_name}' and membership_userrecords.groupID='" . getLoggedGroupID() . "'";
		}elseif($perm[2] == 3) { // view all
			$from .= ' WHERE 1=1';
		}else{ // view none
			return false;
		}

		return $from;
	}

	#########################################################

	function get_joined_record($table, $id, $skip_permissions = false) {
		$sql_fields = get_sql_fields($table);
		$sql_from = get_sql_from($table, $skip_permissions);

		if(!$sql_fields || !$sql_from) return false;

		$pk = getPKFieldName($table);
		if(!$pk) return false;

		$safe_id = makeSafe($id, false);
		$sql = "SELECT {$sql_fields} FROM {$sql_from} AND `{$table}`.`{$pk}`='{$safe_id}'";
		$eo['silentErrors'] = true;
		$res = sql($sql, $eo);
		if($row = db_fetch_assoc($res)) return $row;

		return false;
	}

	#########################################################

	function get_defaults($table) {
		/* array of tables and their fields, with default values (or empty), excluding automatic values */
		$defaults = array(
			'dispositivos' => array(
				'id_dispo' => '',
				'foto' => '',
				'code' => '',
				'codigo' => '',
				'serial' => '',
				'tipo_dispositivo' => '',
				'marca' => '',
				'modelo' => '',
				'ubicacion' => '',
				'ubicacion_abre' => '',
				'fecha_ingreso' => '',
				'estado' => '',
				'descripcion' => '',
				'creado' => '',
				'creado1' => '',
				'editado' => '',
				'creado_por' => '',
				'editado_por' => ''
			),
			'mobiliario' => array(
				'id_mobiliario' => '',
				'codigo' => '',
				'grupo' => '',
				'tipo_mobiliario' => '',
				'fecha_ingreso' => '',
				'unidad' => '',
				'uni_abreviado' => '',
				'descripcion' => '',
				'accesorios' => '',
				'foto' => '',
				'creado' => '',
				'creado_por' => '',
				'editado' => '',
				'editado_por' => ''
			),
			'compras' => array(
				'id_compras' => '',
				'tipo_dispo' => '',
				'serial' => '',
				'marca' => '',
				'modelo' => '',
				'referencia' => '',
				'fecha_compra' => '',
				'proveedor' => '',
				'identi' => '',
				'tel_provededor' => '',
				'ciudad_origen' => '',
				'fecha_factura' => '',
				'factura' => '',
				'precio' => '',
				'comprado_por' => '',
				'garantia' => '',
				'descripcion' => '',
				'foto' => '',
				'soporte' => '',
				'ficha_tecnica' => '',
				'otro_documento' => '',
				'comentarios' => '',
				'reg_creado' => '',
				'update' => '',
				'creado_por' => '',
				'editado_por' => '',
				'creado' => '',
				'editado' => ''
			),
			'contactos' => array(
				'id_contactos' => '',
				'tipo_contacto' => '',
				'tipo_iden' => '',
				'identificacion' => '',
				'nombre' => '',
				'ciudad' => '',
				'telefono' => '',
				'direccion' => '',
				'tipo_relacion' => '',
				'nota' => '',
				'reg_creado' => '',
				'update' => '',
				'creado_por' => '',
				'editado_por' => '',
				'creado' => '',
				'editado' => ''
			),
			'movimientos' => array(
				'id_movi' => '',
				'tipo_movimiento' => '',
				'codigo_dispo' => '',
				'serial_dispo' => '',
				'tipo_dispo' => '',
				'origen' => '',
				'fecha_movimiento' => '',
				'destino' => '',
				'responsable' => '',
				'telefono_respon' => '',
				'estado_movi' => '',
				'comentarios' => '',
				'documento' => '',
				'creado' => '',
				'creado_por' => '',
				'update' => '',
				'editado_por' => '',
				'editado' => ''
			),
			'documentos' => array(
				'id' => '',
				'dispo_codigo' => '',
				'dispo_serial' => '',
				'tipo_docu' => '',
				'enlace' => '',
				'carga' => '',
				'descripcion' => '',
				'fecha_carga' => '',
				'timestamp' => '',
				'update' => '',
				'creado_por' => '',
				'editado_por' => '',
				'creado' => '',
				'editado' => ''
			),
			'mantenimientos' => array(
				'id_mtto' => '',
				'codigo' => '',
				'serial' => '',
				'fecha_mtto' => '',
				'tipo_mtto' => '',
				'responsable' => '',
				'id_responsable' => '',
				'documento' => '',
				'timestamp' => '',
				'update' => '',
				'creado_por' => '',
				'editado_por' => '',
				'creado' => '',
				'editado' => ''
			),
			'calibraciones' => array(
				'id' => '',
				'codigo' => '',
				'serial' => '',
				'tipo_calibracion' => '',
				'fecha_calibra' => '',
				'fecha_expiracion' => '',
				'empresa' => '',
				'documento' => '',
				'descripcion' => '',
				'timestamp' => '',
				'update' => '',
				'creado_por' => '',
				'editado_por' => ''
			),
			'unidades' => array(
				'id_unidades' => '',
				'nombre' => '',
				'abreviado' => '',
				'ciudad' => '',
				'telefono' => '',
				'razon_social' => '',
				'logo' => ''
			),
			'marcas' => array(
				'id_marca' => '',
				'marca' => '',
				'descripcion' => ''
			),
			'marca_modelo' => array(
				'id_mmodelo' => '',
				'marca' => '',
				'modelo' => '',
				'img' => '',
				'fabricante' => '',
				'descripcion' => ''
			),
			'accesorios' => array(
				'id' => '',
				'id_dispo' => '',
				'iddispo' => '',
				'Accesorio' => '',
				'fecha_asignacion' => '',
				'comentarios' => ''
			),
			'ciudades' => array(
				'id_ciudad' => '',
				'ciudad' => '',
				'descripcion' => ''
			),
			'imagenes' => array(
				'id' => '',
				'imagen' => '',
				'tipo_dispo' => '',
				'marca' => '',
				'modelo' => '',
				'descripcion' => ''
			),
			'verificacion' => array(
				'id' => '',
				'codigo' => '',
				'tipo_dispo' => '',
				'serial' => '',
				'ubicacion' => '',
				'verificacion' => '',
				'comentario' => '',
				'verifi_por' => '',
				'fecha_verifi' => ''
			),
			'fichatecnica' => array(
				'id' => '',
				'dispo_codigo' => '',
				'dispo_serial' => '',
				'hardware' => '',
				'software' => '',
				'enlace' => '',
				'carga' => '',
				'fecha_carga' => '',
				'creado' => '',
				'creado_por' => '',
				'editado' => '',
				'editado_por' => ''
			),
			'docu_modelo' => array(
				'id' => '',
				'tipo_dispo' => '',
				'marca' => '',
				'modelo' => '',
				'tipo_documento' => '',
				'documento' => '',
				'descripcion' => '',
				'creado' => '',
				'creado_por' => '',
				'editado' => '',
				'editado_por' => ''
			),
			'tipo_relacion' => array(
				'id_tiporelac' => '',
				'tipo_relac' => '',
				'descripcion' => ''
			),
			'tipo_dispositivo' => array(
				'id_tipodispo' => '',
				'tipo_dispositivo' => '',
				'grupo_dispo' => '',
				'prefijo' => '',
				'descripcion' => ''
			),
			'tipo_documento' => array(
				'id_tipodoc' => '',
				'tipo_doc' => '',
				'descripcion' => ''
			),
			'tipo_iden' => array(
				'id_iden' => '',
				'tipo_iden' => '',
				'tipo_iden_abre' => ''
			),
			'tipo_razon_social' => array(
				'id' => '',
				'razon_social' => '',
				'nit' => '',
				'descripcion' => ''
			),
			'tipo_contacto' => array(
				'id_tipo_contacto' => '',
				'tipo_contacto' => '',
				'descripcion' => ''
			),
			'tipo_mobiliario' => array(
				'id' => '',
				'grupo_mobi' => '',
				'tipo_mobiliario' => '',
				'descripcion' => ''
			),
			'tipo_estado_dispo' => array(
				'id' => '',
				'estado_dispo' => '',
				'descripcion' => ''
			),
			'grupo_dispo' => array(
				'id_grupo_dispo' => '',
				'grupo_dispo' => '',
				'descripcion' => ''
			),
			'tipo_movimiento' => array(
				'id' => '',
				'tipo_movimiento' => '',
				'descripcion' => ''
			),
			'tipo_estado_movi' => array(
				'id' => '',
				'estado_movi' => '',
				'descripcion' => ''
			),
			'tipo_estado_verifica' => array(
				'id' => '',
				'estado_verificado' => '',
				'descripcion' => ''
			),
			'tipo_mtto' => array(
				'id' => '',
				'tipo_mtto' => '',
				'descripcion' => ''
			),
			'tipo_calibracion' => array(
				'id' => '',
				'tipo_calibracion' => '',
				'descripcion' => ''
			),
			'tipo_grupo_mobilia' => array(
				'id_gru_mo' => '',
				'grupo' => '',
				'prefijo' => '',
				'descripcion' => ''
			)
		);

		return isset($defaults[$table]) ? $defaults[$table] : array();
	}

	#########################################################

    function logInMember(){
		$redir = 'index.php';
		if($_POST['signIn'] != ''){
			if(isset($_POST['signIn']) && !empty($_POST['signIn'])){
				if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
				//your site secret key
				$secret = '6LdI59EZAAAAAHFJrUumF-I7JalhnUuWQBwkg6IM';
				//get verify response data
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
				$responseData = json_decode($verifyResponse);
					if($responseData->success == true){

						if($_POST['username'] != '' && $_POST['password'] != '') {
							$username = makeSafe(strtolower($_POST['username']));
							$hash = sqlValue("select passMD5 from membership_users where lcase(memberID)='{$username}' and isApproved=1 and isBanned=0");
							$password = $_POST['password'];

							if(password_match($password, $hash)) {
								$_SESSION['memberID'] = $username;
								$_SESSION['memberGroupID'] = sqlValue("SELECT `groupID` FROM `membership_users` WHERE LCASE(`memberID`)='{$username}'");

									if($_POST['rememberMe'] == 1) {
										RememberMe::login($username);
									}else{
										RememberMe::delete();
										}
										
									// harden user's password hash
									password_harden($username, $password, $hash);

									// hook: login_ok
									if(function_exists('login_ok')) {
										$args=array();
											if(!$redir=login_ok(getMemberInfo(), $args)) {
												$redir='index.php';
											}
									}	
								redirect($redir);
								exit;
							}
						}
						
						// hook: login_failed
						if(function_exists('login_failed')) {
						$args=array();
						login_failed(array(
						'username' => $_POST['username'],
						'password' => $_POST['password'],
						'IP' => $_SERVER['REMOTE_ADDR']
						), $args);
						}

						if(!headers_sent()) header('HTTP/1.0 403 Forbidden');
						redirect("index.php?loginFailed=1");
						exit;
						
					}
					
				} else {
					$_SESSION['custom_err_msg']= "Please click on reCAPTCHA to login";
					if(!headers_sent()) header('HTTP/1.0 403 Forbidden');
					redirect("index.php?loginFailed=1");
					exit;
					}
			}		
		}		
		// do we have a JWT auth header? 
			jwt_check_login();

			if(!empty($_SESSION['memberID']) && !empty($_SESSION['memberGroupID'])) return;

			// check if a rememberMe cookie exists and sign in user if so 
			if(RememberMe::check()) {
			$username = makeSafe(strtolower(RememberMe::user()));
			$_SESSION['memberID'] = $username;
			$_SESSION['memberGroupID'] = sqlValue("SELECT `groupID` FROM `membership_users` WHERE LCASE(`memberID`)='{$username}'");
			}
	}

	#########################################################

	function htmlUserBar() {
		global $adminConfig, $Translation;
		if(!defined('PREPEND_PATH')) define('PREPEND_PATH', '');

		ob_start();
		$home_page = (basename($_SERVER['PHP_SELF'])=='index.php' ? true : false);

		?>
		<nav class="navbar navbar-default navbar-fixed-top hidden-print" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- application title is obtained from the name besides the yellow database icon in AppGini, use underscores for spaces -->
				<a class="navbar-brand" href="<?php echo PREPEND_PATH; ?>index.php"><i class="glyphicon glyphicon-home"></i> INVENTARIO</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<?php if(!$home_page) { ?>
						<?php echo NavMenus(); ?>
					<?php } ?>
				</ul>

				<?php if(getLoggedAdmin()) { ?>
					<ul class="nav navbar-nav">
						<a href="<?php echo PREPEND_PATH; ?>admin/pageHome.php" class="btn btn-danger navbar-btn hidden-xs" title="<?php echo html_attr($Translation['admin area']); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo $Translation['admin area']; ?></a>
						<a href="<?php echo PREPEND_PATH; ?>admin/pageHome.php" class="btn btn-danger navbar-btn visible-xs btn-lg" title="<?php echo html_attr($Translation['admin area']); ?>"><i class="glyphicon glyphicon-cog"></i> <?php echo $Translation['admin area']; ?></a>
					</ul>
				<?php } ?>

				<?php if(!$_GET['signIn'] && !$_GET['loginFailed']) { ?>
					<?php if(getLoggedMemberID() == $adminConfig['anonymousMember']) { ?>
						<p class="navbar-text navbar-right">&nbsp;</p>
						<a href="<?php echo PREPEND_PATH; ?>index.php?signIn=1" class="btn btn-success navbar-btn navbar-right"><?php echo $Translation['sign in']; ?></a>
						<p class="navbar-text navbar-right">
							<?php echo $Translation['not signed in']; ?>
						</p>
					<?php }else{ ?>
						<ul class="nav navbar-nav navbar-right hidden-xs" style="min-width: 330px;">
							<a class="btn navbar-btn btn-default" href="<?php echo PREPEND_PATH; ?>index.php?signOut=1"><i class="glyphicon glyphicon-log-out"></i> <?php echo $Translation['sign out']; ?></a>
							<p class="navbar-text">
								<?php echo $Translation['signed as']; ?> <strong><a href="<?php echo PREPEND_PATH; ?>membership_profile.php" class="navbar-link"><?php echo getLoggedMemberID(); ?></a></strong>
							</p>
						</ul>
						<ul class="nav navbar-nav visible-xs">
							<a class="btn navbar-btn btn-default btn-lg visible-xs" href="<?php echo PREPEND_PATH; ?>index.php?signOut=1"><i class="glyphicon glyphicon-log-out"></i> <?php echo $Translation['sign out']; ?></a>
							<p class="navbar-text text-center">
								<?php echo $Translation['signed as']; ?> <strong><a href="<?php echo PREPEND_PATH; ?>membership_profile.php" class="navbar-link"><?php echo getLoggedMemberID(); ?></a></strong>
							</p>
						</ul>
						<script>
							/* periodically check if user is still signed in */
							setInterval(function() {
								$j.ajax({
									url: '<?php echo PREPEND_PATH; ?>ajax_check_login.php',
									success: function(username) {
										if(!username.length) window.location = '<?php echo PREPEND_PATH; ?>index.php?signIn=1';
									}
								});
							}, 60000);
						</script>
					<?php } ?>
				<?php } ?>
			</div>
		</nav>
		<?php

		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	#########################################################

	function showNotifications($msg = '', $class = '', $fadeout = true) {
		global $Translation;

		$notify_template_no_fadeout = '<div id="%%ID%%" class="alert alert-dismissable %%CLASS%%" style="display: none; padding-top: 6px; padding-bottom: 6px;">' .
					'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' .
					'%%MSG%%</div>' .
					'<script> jQuery(function() { /* */ jQuery("#%%ID%%").show("slow"); }); </script>'."\n";
		$notify_template = '<div id="%%ID%%" class="alert %%CLASS%%" style="display: none; padding-top: 6px; padding-bottom: 6px;">%%MSG%%</div>' .
					'<script>' .
						'jQuery(function() {' .
							'jQuery("#%%ID%%").show("slow", function() {' .
								'setTimeout(function() { /* */ jQuery("#%%ID%%").hide("slow"); }, 4000);' .
							'});' .
						'});' .
					'</script>'."\n";

		if(!$msg) { // if no msg, use url to detect message to display
			if($_REQUEST['record-added-ok'] != '') {
				$msg = $Translation['new record saved'];
				$class = 'alert-success';
			}elseif($_REQUEST['record-added-error'] != '') {
				$msg = $Translation['Couldn\'t save the new record'];
				$class = 'alert-danger';
				$fadeout = false;
			}elseif($_REQUEST['record-updated-ok'] != '') {
				$msg = $Translation['record updated'];
				$class = 'alert-success';
			}elseif($_REQUEST['record-updated-error'] != '') {
				$msg = $Translation['Couldn\'t save changes to the record'];
				$class = 'alert-danger';
				$fadeout = false;
			}elseif($_REQUEST['record-deleted-ok'] != '') {
				$msg = $Translation['The record has been deleted successfully'];
				$class = 'alert-success';
				$fadeout = false;
			}elseif($_REQUEST['record-deleted-error'] != '') {
				$msg = $Translation['Couldn\'t delete this record'];
				$class = 'alert-danger';
				$fadeout = false;
			}else{
				return '';
			}
		}
		$id = 'notification-' . rand();

		$out = ($fadeout ? $notify_template : $notify_template_no_fadeout);
		$out = str_replace('%%ID%%', $id, $out);
		$out = str_replace('%%MSG%%', $msg, $out);
		$out = str_replace('%%CLASS%%', $class, $out);

		return $out;
	}

	#########################################################

	function parseMySQLDate($date, $altDate) {
		// is $date valid?
		if(preg_match("/^\d{4}-\d{1,2}-\d{1,2}$/", trim($date))) {
			return trim($date);
		}

		if($date != '--' && preg_match("/^\d{4}-\d{1,2}-\d{1,2}$/", trim($altDate))) {
			return trim($altDate);
		}

		if($date != '--' && $altDate && intval($altDate)==$altDate) {
			return @date('Y-m-d', @time() + ($altDate >= 1 ? $altDate - 1 : $altDate) * 86400);
		}

		return '';
	}

	#########################################################

	function parseCode($code, $isInsert=true, $rawData=false) {
		if($isInsert) {
			$arrCodes=array(
				'<%%creatorusername%%>' => $_SESSION['memberID'],
				'<%%creatorgroupid%%>' => $_SESSION['memberGroupID'],
				'<%%creatorip%%>' => $_SERVER['REMOTE_ADDR'],
				'<%%creatorgroup%%>' => sqlValue("select name from membership_groups where groupID='{$_SESSION['memberGroupID']}'"),

				'<%%creationdate%%>' => ($rawData ? @date('Y-m-d') : @date('j/n/Y')),
				'<%%creationtime%%>' => ($rawData ? @date('H:i:s') : @date('h:i:s a')),
				'<%%creationdatetime%%>' => ($rawData ? @date('Y-m-d H:i:s') : @date('j/n/Y h:i:s a')),
				'<%%creationtimestamp%%>' => ($rawData ? @date('Y-m-d H:i:s') : @time())
			);
		}else{
			$arrCodes=array(
				'<%%editorusername%%>' => $_SESSION['memberID'],
				'<%%editorgroupid%%>' => $_SESSION['memberGroupID'],
				'<%%editorip%%>' => $_SERVER['REMOTE_ADDR'],
				'<%%editorgroup%%>' => sqlValue("select name from membership_groups where groupID='{$_SESSION['memberGroupID']}'"),

				'<%%editingdate%%>' => ($rawData ? @date('Y-m-d') : @date('j/n/Y')),
				'<%%editingtime%%>' => ($rawData ? @date('H:i:s') : @date('h:i:s a')),
				'<%%editingdatetime%%>' => ($rawData ? @date('Y-m-d H:i:s') : @date('j/n/Y h:i:s a')),
				'<%%editingtimestamp%%>' => ($rawData ? @date('Y-m-d H:i:s') : @time())
			);
		}

		$pc=str_ireplace(array_keys($arrCodes), array_values($arrCodes), $code);

		return $pc;
	}

	#########################################################

	function addFilter($index, $filterAnd, $filterField, $filterOperator, $filterValue) {
		// validate input
		if($index < 1 || $index > 80 || !is_int($index)) return false;
		if($filterAnd != 'or')   $filterAnd = 'and';
		$filterField = intval($filterField);

		/* backward compatibility */
		if(in_array($filterOperator, $GLOBALS['filter_operators'])) {
			$filterOperator = array_search($filterOperator, $GLOBALS['filter_operators']);
		}

		if(!in_array($filterOperator, array_keys($GLOBALS['filter_operators']))) {
			$filterOperator = 'like';
		}

		if(!$filterField) {
			$filterOperator = '';
			$filterValue = '';
		}

		$_REQUEST['FilterAnd'][$index] = $filterAnd;
		$_REQUEST['FilterField'][$index] = $filterField;
		$_REQUEST['FilterOperator'][$index] = $filterOperator;
		$_REQUEST['FilterValue'][$index] = $filterValue;

		return true;
	}

	#########################################################

	function clearFilters() {
		for($i=1; $i<=80; $i++) {
			addFilter($i, '', 0, '', '');
		}
	}

	#########################################################

	if(!function_exists('str_ireplace')) {
		function str_ireplace($search, $replace, $subject) {
			$ret=$subject;
			if(is_array($search)) {
				for($i=0; $i<count($search); $i++) {
					$ret=str_ireplace($search[$i], $replace[$i], $ret);
				}
			}else{
				$ret=preg_replace('/'.preg_quote($search, '/').'/i', $replace, $ret);
			}

			return $ret;
		} 
	} 

	#########################################################

	/**
	* Loads a given view from the templates folder, passing the given data to it
	* @param $view the name of a php file (without extension) to be loaded from the 'templates' folder
	* @param $the_data_to_pass_to_the_view (optional) associative array containing the data to pass to the view
	* @return the output of the parsed view as a string
	*/
	function loadView($view, $the_data_to_pass_to_the_view=false) {
		global $Translation;

		$view = dirname(__FILE__)."/templates/$view.php";
		if(!is_file($view)) return false;

		if(is_array($the_data_to_pass_to_the_view)) {
			foreach($the_data_to_pass_to_the_view as $k => $v)
				$$k = $v;
		}
		unset($the_data_to_pass_to_the_view, $k, $v);

		ob_start();
		@include($view);
		$out=ob_get_contents();
		ob_end_clean();

		return $out;
	}

	#########################################################

	/**
	* Loads a table template from the templates folder, passing the given data to it
	* @param $table_name the name of the table whose template is to be loaded from the 'templates' folder
	* @param $the_data_to_pass_to_the_table associative array containing the data to pass to the table template
	* @return the output of the parsed table template as a string
	*/
	function loadTable($table_name, $the_data_to_pass_to_the_table = array()) {
		$dont_load_header = $the_data_to_pass_to_the_table['dont_load_header'];
		$dont_load_footer = $the_data_to_pass_to_the_table['dont_load_footer'];

		$header = $table = $footer = '';

		if(!$dont_load_header) {
			// try to load tablename-header
			if(!($header = loadView("{$table_name}-header", $the_data_to_pass_to_the_table))) {
				$header = loadView('table-common-header', $the_data_to_pass_to_the_table);
			}
		}

		$table = loadView($table_name, $the_data_to_pass_to_the_table);

		if(!$dont_load_footer) {
			// try to load tablename-footer
			if(!($footer = loadView("{$table_name}-footer", $the_data_to_pass_to_the_table))) {
				$footer = loadView('table-common-footer', $the_data_to_pass_to_the_table);
			}
		}

		return "{$header}{$table}{$footer}";
	}

	#########################################################

	function filterDropdownBy($filterable, $filterers, $parentFilterers, $parentPKField, $parentCaption, $parentTable, &$filterableCombo) {
		$filterersArray = explode(',', $filterers);
		$parentFilterersArray = explode(',', $parentFilterers);
		$parentFiltererList = '`' . implode('`, `', $parentFilterersArray) . '`';
		$res=sql("SELECT `$parentPKField`, $parentCaption, $parentFiltererList FROM `$parentTable` ORDER BY 2", $eo);
		$filterableData = array();
		while($row=db_fetch_row($res)) {
			$filterableData[$row[0]] = $row[1];
			$filtererIndex = 0;
			foreach($filterersArray as $filterer) {
				$filterableDataByFilterer[$filterer][$row[$filtererIndex + 2]][$row[0]] = $row[1];
				$filtererIndex++;
			}
			$row[0] = addslashes($row[0]);
			$row[1] = addslashes($row[1]);
			$jsonFilterableData .= "\"{$row[0]}\":\"{$row[1]}\",";
		}
		$jsonFilterableData .= '}';
		$jsonFilterableData = '{'.str_replace(',}', '}', $jsonFilterableData);     
		$filterJS = "\nvar {$filterable}_data = $jsonFilterableData;";

		foreach($filterersArray as $filterer) {
			if(is_array($filterableDataByFilterer[$filterer])) foreach($filterableDataByFilterer[$filterer] as $filtererItem => $filterableItem) {
				$jsonFilterableDataByFilterer[$filterer] .= '"'.addslashes($filtererItem).'":{';
				foreach($filterableItem as $filterableItemID => $filterableItemData) {
					$jsonFilterableDataByFilterer[$filterer] .= '"'.addslashes($filterableItemID).'":"'.addslashes($filterableItemData).'",';
				}
				$jsonFilterableDataByFilterer[$filterer] .= '},';
			}
			$jsonFilterableDataByFilterer[$filterer] .= '}';
			$jsonFilterableDataByFilterer[$filterer] = '{'.str_replace(',}', '}', $jsonFilterableDataByFilterer[$filterer]);

			$filterJS.="\n\n// code for filtering {$filterable} by {$filterer}\n";
			$filterJS.="\nvar {$filterable}_data_by_{$filterer} = {$jsonFilterableDataByFilterer[$filterer]}; ";
			$filterJS.="\nvar selected_{$filterable} = \$j('#{$filterable}').val();";
			$filterJS.="\nvar {$filterable}_change_by_{$filterer} = function() {";
			$filterJS.="\n\t$('{$filterable}').options.length=0;";
			$filterJS.="\n\t$('{$filterable}').options[0] = new Option();";
			$filterJS.="\n\tif(\$j('#{$filterer}').val()) {";
			$filterJS.="\n\t\tfor({$filterable}_item in {$filterable}_data_by_{$filterer}[\$j('#{$filterer}').val()]) {";
			$filterJS.="\n\t\t\t$('{$filterable}').options[$('{$filterable}').options.length] = new Option(";
			$filterJS.="\n\t\t\t\t{$filterable}_data_by_{$filterer}[\$j('#{$filterer}').val()][{$filterable}_item],";
			$filterJS.="\n\t\t\t\t{$filterable}_item,";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false),";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false)";
			$filterJS.="\n\t\t\t);";
			$filterJS.="\n\t\t}";
			$filterJS.="\n\t}else{";
			$filterJS.="\n\t\tfor({$filterable}_item in {$filterable}_data) {";
			$filterJS.="\n\t\t\t$('{$filterable}').options[$('{$filterable}').options.length] = new Option(";
			$filterJS.="\n\t\t\t\t{$filterable}_data[{$filterable}_item],";
			$filterJS.="\n\t\t\t\t{$filterable}_item,";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false),";
			$filterJS.="\n\t\t\t\t({$filterable}_item == selected_{$filterable} ? true : false)";
			$filterJS.="\n\t\t\t);";
			$filterJS.="\n\t\t}";
			$filterJS.="\n\t\tif(selected_{$filterable} && selected_{$filterable} == \$j('#{$filterable}').val()) {";
			$filterJS.="\n\t\t\tfor({$filterer}_item in {$filterable}_data_by_{$filterer}) {";
			$filterJS.="\n\t\t\t\tfor({$filterable}_item in {$filterable}_data_by_{$filterer}[{$filterer}_item]) {";
			$filterJS.="\n\t\t\t\t\tif({$filterable}_item == selected_{$filterable}) {";
			$filterJS.="\n\t\t\t\t\t\t$('{$filterer}').value = {$filterer}_item;";
			$filterJS.="\n\t\t\t\t\t\tbreak;";
			$filterJS.="\n\t\t\t\t\t}";
			$filterJS.="\n\t\t\t\t}";
			$filterJS.="\n\t\t\t\tif({$filterable}_item == selected_{$filterable}) break;";
			$filterJS.="\n\t\t\t}";
			$filterJS.="\n\t\t}";
			$filterJS.="\n\t}";
			$filterJS.="\n\t$('{$filterable}').highlight();";
			$filterJS.="\n};";
			$filterJS.="\n$('{$filterer}').observe('change', function() { /* */ window.setTimeout({$filterable}_change_by_{$filterer}, 25); });";
			$filterJS.="\n";
		}

		$filterableCombo = new Combo;
		$filterableCombo->ListType = 0;
		$filterableCombo->ListItem = array_slice(array_values($filterableData), 0, 10);
		$filterableCombo->ListData = array_slice(array_keys($filterableData), 0, 10);
		$filterableCombo->SelectName = $filterable;
		$filterableCombo->AllowNull = true;

		return $filterJS;
	}

	#########################################################
	function br2nl($text) {
		return  preg_replace('/\<br(\s*)?\/?\>/i', "\n", $text);
	}

	#########################################################

	if(!function_exists('htmlspecialchars_decode')) {
		function htmlspecialchars_decode($string, $quote_style = ENT_COMPAT) {
			return strtr($string, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
		}
	}

	#########################################################

	function entitiesToUTF8($input) {
		return preg_replace_callback('/(&#[0-9]+;)/', '_toUTF8', $input);
	}

	function _toUTF8($m) {
		if(function_exists('mb_convert_encoding')) {
			return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
		}else{
			return $m[1];
		}
	}

	#########################################################

	function func_get_args_byref() {
		if(!function_exists('debug_backtrace')) return false;

		$trace = debug_backtrace();
		return $trace[1]['args'];
	}

	#########################################################

	function permissions_sql($table, $level = 'all') {
		if(!in_array($level, array('user', 'group'))) { $level = 'all'; }
		$perm = getTablePermissions($table);
		$from = '';
		$where = '';
		$pk = getPKFieldName($table);

		if($perm[2] == 1 || ($perm[2] > 1 && $level == 'user')) { // view owner only
			$from = 'membership_userrecords';
			$where = "(`$table`.`$pk`=membership_userrecords.pkValue and membership_userrecords.tableName='$table' and lcase(membership_userrecords.memberID)='".getLoggedMemberID()."')";
		}elseif($perm[2] == 2 || ($perm[2] > 2 && $level == 'group')) { // view group only
			$from = 'membership_userrecords';
			$where = "(`$table`.`$pk`=membership_userrecords.pkValue and membership_userrecords.tableName='$table' and membership_userrecords.groupID='".getLoggedGroupID()."')";
		}elseif($perm[2] == 3) { // view all
			// no further action
		}elseif($perm[2] == 0) { // view none
			return false;
		}

		return array('where' => $where, 'from' => $from, 0 => $where, 1 => $from);
	}

	#########################################################

	function error_message($msg, $back_url = '', $full_page = true) {
		$curr_dir = dirname(__FILE__);
		global $Translation;

		ob_start();

		if($full_page) include_once($curr_dir . '/header.php');

		echo '<div class="panel panel-danger">';
			echo '<div class="panel-heading"><h3 class="panel-title">' . $Translation['error:'] . '</h3></div>';
			echo '<div class="panel-body"><p class="text-danger">' . $msg . '</p>';
			if($back_url !== false) { // explicitly passing false suppresses the back link completely
				echo '<div class="text-center">';
				if($back_url) {
					echo '<a href="' . $back_url . '" class="btn btn-danger btn-lg vspacer-lg"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['< back'] . '</a>';
				}else{
					echo '<a href="#" class="btn btn-danger btn-lg vspacer-lg" onclick="history.go(-1); return false;"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['< back'] . '</a>';
				}
				echo '</div>';
			}
			echo '</div>';
		echo '</div>';

		if($full_page) include_once($curr_dir . '/footer.php');

		$out = ob_get_contents();
		ob_end_clean();

		return $out;
	}

	#########################################################

	function toMySQLDate($formattedDate, $sep = datalist_date_separator, $ord = datalist_date_format) {
		// extract date elements
		$de=explode($sep, $formattedDate);
		$mySQLDate=intval($de[strpos($ord, 'Y')]).'-'.intval($de[strpos($ord, 'm')]).'-'.intval($de[strpos($ord, 'd')]);
		return $mySQLDate;
	}

	#########################################################

	function reIndex(&$arr) {
		$i=1;
		foreach($arr as $n=>$v) {
			$arr2[$i]=$n;
			$i++;
		}
		return $arr2;
	}

	#########################################################

	function get_embed($provider, $url, $max_width = '', $max_height = '', $retrieve = 'html') {
		global $Translation;
		if(!$url) return '';

		$providers = array(
			'youtube' => array('oembed' => 'http://www.youtube.com/oembed?'),
			'googlemap' => array('oembed' => '', 'regex' => '/^http.*\.google\..*maps/i')
		);

		if(!isset($providers[$provider])) {
			return '<div class="text-danger">' . $Translation['invalid provider'] . '</div>';
		}

		if(isset($providers[$provider]['regex']) && !preg_match($providers[$provider]['regex'], $url)) {
			return '<div class="text-danger">' . $Translation['invalid url'] . '</div>';
		}

		if($providers[$provider]['oembed']) {
			$oembed = $providers[$provider]['oembed'] . 'url=' . urlencode($url) . "&maxwidth={$max_width}&maxheight={$max_height}&format=json";
			$data_json = request_cache($oembed);

			$data = json_decode($data_json, true);
			if($data === null) {
				/* an error was returned rather than a json string */
				if($retrieve == 'html') return "<div class=\"text-danger\">{$data_json}\n<!-- {$oembed} --></div>";
				return '';
			}

			return (isset($data[$retrieve]) ? $data[$retrieve] : $data['html']);
		}

		/* special cases (where there is no oEmbed provider) */
		if($provider == 'googlemap') return get_embed_googlemap($url, $max_width, $max_height, $retrieve);

		return '<div class="text-danger">Invalid provider!</div>';
	}

	#########################################################

	function get_embed_googlemap($url, $max_width = '', $max_height = '', $retrieve = 'html') {
		global $Translation;
		$url_parts = parse_url($url);
		$coords_regex = '/-?\d+(\.\d+)?[,+]-?\d+(\.\d+)?(,\d{1,2}z)?/'; /* https://stackoverflow.com/questions/2660201 */

		if(preg_match($coords_regex, $url_parts['path'] . '?' . $url_parts['query'], $m)) {
			list($lat, $long, $zoom) = explode(',', $m[0]);
			$zoom = intval($zoom);
			if(!$zoom) $zoom = 10; /* default zoom */
			if(!$max_height) $max_height = 360;
			if(!$max_width) $max_width = 480;

			$api_key = '';
			$embed_url = "https://www.google.com/maps/embed/v1/view?key={$api_key}&center={$lat},{$long}&zoom={$zoom}&maptype=roadmap";
			$thumbnail_url = "https://maps.googleapis.com/maps/api/staticmap?key={$api_key}&center={$lat},{$long}&zoom={$zoom}&maptype=roadmap&size={$max_width}x{$max_height}";

			if($retrieve == 'html') {
				return "<iframe width=\"{$max_width}\" height=\"{$max_height}\" frameborder=\"0\" style=\"border:0\" src=\"{$embed_url}\"></iframe>";
			}else{
				return $thumbnail_url;
			}
		}else{
			return '<div class="text-danger">' . $Translation['cant retrieve coordinates from url'] . '</div>';
		}
	}

	#########################################################

	function request_cache($request, $force_fetch = false) {
		$max_cache_lifetime = 7 * 86400; /* max cache lifetime in seconds before refreshing from source */

		/* membership_cache table exists? if not, create it */
		static $cache_table_exists = false;
		if(!$cache_table_exists && !$force_fetch) {
			$te = sqlValue("show tables like 'membership_cache'");
			if(!$te) {
				if(!sql("CREATE TABLE `membership_cache` (`request` VARCHAR(100) NOT NULL, `request_ts` INT, `response` TEXT NOT NULL, PRIMARY KEY (`request`))", $eo)) {
					/* table can't be created, so force fetching request */
					return request_cache($request, true);
				}
			}
			$cache_table_exists = true;
		}

		/* retrieve response from cache if exists */
		if(!$force_fetch) {
			$res = sql("select response, request_ts from membership_cache where request='" . md5($request) . "'", $eo);
			if(!$row = db_fetch_array($res)) return request_cache($request, true);

			$response = $row[0];
			$response_ts = $row[1];
			if($response_ts < time() - $max_cache_lifetime) return request_cache($request, true);
		}

		/* if no response in cache, issue a request */
		if(!$response || $force_fetch) {
			$response = @file_get_contents($request);
			if($response === false) {
				$error = error_get_last();
				$error_message = preg_replace('/.*: (.*)/', '$1', $error['message']);
				return $error_message;
			}elseif($cache_table_exists) {
				/* store response in cache */
				$ts = time();
				sql("replace into membership_cache set request='" . md5($request) . "', request_ts='{$ts}', response='" . makeSafe($response, false) . "'", $eo);
			}
		}

		return $response;
	}

	#########################################################

	function check_record_permission($table, $id, $perm = 'view') {
		if($perm != 'edit' && $perm != 'delete') $perm = 'view';

		$perms = getTablePermissions($table);
		if(!$perms[$perm]) return false;

		$safe_id = makeSafe($id);
		$safe_table = makeSafe($table);

		if($perms[$perm] == 1) { // own records only
			$username = getLoggedMemberID();
			$owner = sqlValue("select memberID from membership_userrecords where tableName='{$safe_table}' and pkValue='{$safe_id}'");
			if($owner == $username) return true;
		}elseif($perms[$perm] == 2) { // group records
			$group_id = getLoggedGroupID();
			$owner_group_id = sqlValue("select groupID from membership_userrecords where tableName='{$safe_table}' and pkValue='{$safe_id}'");
			if($owner_group_id == $group_id) return true;
		}elseif($perms[$perm] == 3) { // all records
			return true;
		}

		return false;
	}

	#########################################################

	function NavMenus($options = array()) {
		if(!defined('PREPEND_PATH')) define('PREPEND_PATH', '');
		global $Translation;
		$prepend_path = PREPEND_PATH;

		/* default options */
		if(empty($options)) {
			$options = array(
				'tabs' => 7
			);
		}

		$table_group_name = array_keys(get_table_groups()); /* 0 => group1, 1 => group2 .. */
		/* if only one group named 'None', set to translation of 'select a table' */
		if((count($table_group_name) == 1 && $table_group_name[0] == 'None') || count($table_group_name) < 1) $table_group_name[0] = $Translation['select a table'];
		$table_group_index = array_flip($table_group_name); /* group1 => 0, group2 => 1 .. */
		$menu = array_fill(0, count($table_group_name), '');

		$t = time();
		$arrTables = getTableList();
		if(is_array($arrTables)) {
			foreach($arrTables as $tn => $tc) {
				/* ---- list of tables where hide link in nav menu is set ---- */
				$tChkHL = array_search($tn, array('marcas'));

				/* ---- list of tables where filter first is set ---- */
				$tChkFF = array_search($tn, array());
				if($tChkFF !== false && $tChkFF !== null) {
					$searchFirst = '&Filter_x=1';
				}else{
					$searchFirst = '';
				}

				/* when no groups defined, $table_group_index['None'] is NULL, so $menu_index is still set to 0 */
				$menu_index = intval($table_group_index[$tc[3]]);
				if(!$tChkHL && $tChkHL !== 0) $menu[$menu_index] .= "<li><a href=\"{$prepend_path}{$tn}_view.php?t={$t}{$searchFirst}\"><img src=\"{$prepend_path}" . ($tc[2] ? $tc[2] : 'blank.gif') . "\" height=\"32\"> {$tc[0]}</a></li>";
			}
		}

		// custom nav links, as defined in "hooks/links-navmenu.php" 
		global $navLinks;
		if(is_array($navLinks)) {
			$memberInfo = getMemberInfo();
			$links_added = array();
			foreach($navLinks as $link) {
				if(!isset($link['url']) || !isset($link['title'])) continue;
				if($memberInfo['admin'] || @in_array($memberInfo['group'], $link['groups']) || @in_array('*', $link['groups'])) {
					$menu_index = intval($link['table_group']);
					if(!$links_added[$menu_index]) $menu[$menu_index] .= '<li class="divider"></li>';

					/* add prepend_path to custom links if they aren't absolute links */
					if(!preg_match('/^(http|\/\/)/i', $link['url'])) $link['url'] = $prepend_path . $link['url'];
					if(!preg_match('/^(http|\/\/)/i', $link['icon']) && $link['icon']) $link['icon'] = $prepend_path . $link['icon'];

					$menu[$menu_index] .= "<li><a href=\"{$link['url']}\"><img src=\"" . ($link['icon'] ? $link['icon'] : "{$prepend_path}blank.gif") . "\" height=\"32\"> {$link['title']}</a></li>";
					$links_added[$menu_index]++;
				}
			}
		}

		$menu_wrapper = '';
		for($i = 0; $i < count($menu); $i++) {
			$menu_wrapper .= <<<EOT
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">{$table_group_name[$i]} <b class="caret"></b></a>
					<ul class="dropdown-menu" role="menu">{$menu[$i]}</ul>
				</li>
EOT;
		}

		return $menu_wrapper;
	}

	#########################################################

	function StyleSheet() {
		if(!defined('PREPEND_PATH')) define('PREPEND_PATH', '');
		$prepend_path = PREPEND_PATH;

		$css_links = <<<EOT

			<link rel="stylesheet" href="{$prepend_path}resources/initializr/css/bootstrap.css">
			<link rel="stylesheet" href="{$prepend_path}resources/lightbox/css/lightbox.css" media="screen">
			<link rel="stylesheet" href="{$prepend_path}resources/select2/select2.css" media="screen">
			<link rel="stylesheet" href="{$prepend_path}resources/timepicker/bootstrap-timepicker.min.css" media="screen">
			<link rel="stylesheet" href="{$prepend_path}dynamic.css.php">
EOT;

		return $css_links;
	}

	#########################################################

	function getUploadDir($dir) {
		global $Translation;

		if($dir=="") {
			$dir=$Translation['ImageFolder'];
		}

		if(substr($dir, -1)!="/") {
			$dir.="/";
		}

		return $dir;
	}

	#########################################################

	function PrepareUploadedFile($FieldName, $MaxSize, $FileTypes = 'jpg|jpeg|gif|png', $NoRename = false, $dir = '') {
		global $Translation;
		$f = $_FILES[$FieldName];
		if($f['error'] == 4 || !$f['name']) return '';

		$dir = getUploadDir($dir);

		/* get php.ini upload_max_filesize in bytes */
		$php_upload_size_limit = trim(ini_get('upload_max_filesize'));
		$last = strtolower($php_upload_size_limit[strlen($php_upload_size_limit) - 1]);
		switch($last) {
			case 'g':
				$php_upload_size_limit *= 1024;
			case 'm':
				$php_upload_size_limit *= 1024;
			case 'k':
				$php_upload_size_limit *= 1024;
		}

		$MaxSize = min($MaxSize, $php_upload_size_limit);

		if($f['size'] > $MaxSize || $f['error']) {
			echo error_message(str_replace('<MaxSize>', intval($MaxSize / 1024), $Translation['file too large']));
			exit;
		}
		if(!preg_match('/\.(' . $FileTypes . ')$/i', $f['name'], $ft)) {
			echo error_message(str_replace('<FileTypes>', str_replace('|', ', ', $FileTypes), $Translation['invalid file type']));
			exit;
		}

		$name = str_replace(' ', '_', $f['name']);
		if(!$NoRename) $name = substr(md5(microtime() . rand(0, 100000)), -17) . $ft[0];

		if(!file_exists($dir)) @mkdir($dir, 0777);

		if(!@move_uploaded_file($f['tmp_name'], $dir . $name)) {
			echo error_message("Couldn't save the uploaded file. Try chmoding the upload folder '{$dir}' to 777.");
			exit;
		}

		@chmod($dir . $name, 0666);
		return $name;
	}

	#########################################################

	function get_home_links($homeLinks, $default_classes, $tgroup = '') {
		if(!is_array($homeLinks) || !count($homeLinks)) return '';

		$memberInfo = getMemberInfo();

		ob_start();
		foreach($homeLinks as $link) {
			if(!isset($link['url']) || !isset($link['title'])) continue;
			if($tgroup != $link['table_group'] && $tgroup != '*') continue;

			/* fall-back classes if none defined */
			if(!$link['grid_column_classes']) $link['grid_column_classes'] = $default_classes['grid_column'];
			if(!$link['panel_classes']) $link['panel_classes'] = $default_classes['panel'];
			if(!$link['link_classes']) $link['link_classes'] = $default_classes['link'];

			if($memberInfo['admin'] || @in_array($memberInfo['group'], $link['groups']) || @in_array('*', $link['groups'])) {
				?>
				<div class="col-xs-12 <?php echo $link['grid_column_classes']; ?>">
					<div class="panel <?php echo $link['panel_classes']; ?>">
						<div class="panel-body">
							<a class="btn btn-block btn-lg <?php echo $link['link_classes']; ?>" title="<?php echo preg_replace("/&amp;(#[0-9]+|[a-z]+);/i", "&$1;", html_attr(strip_tags($link['description']))); ?>" href="<?php echo $link['url']; ?>"><?php echo ($link['icon'] ? '<img src="' . $link['icon'] . '">' : ''); ?><strong><?php echo $link['title']; ?></strong></a>
							<div class="panel-body-description"><?php echo $link['description']; ?></div>
						</div>
					</div>
				</div>
				<?php
			}
		}

		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	#########################################################

	function quick_search_html($search_term, $label, $separate_dv = true) {
		global $Translation;

		$safe_search = html_attr($search_term);
		$safe_label = html_attr($label);
		$safe_clear_label = html_attr($Translation['Reset Filters']);

		if($separate_dv) {
			$reset_selection = "document.myform.SelectedID.value = '';";
		}else{
			$reset_selection = "document.myform.writeAttribute('novalidate', 'novalidate');";
		}
		$reset_selection .= ' document.myform.NoDV.value=1; return true;';

		$html = <<<EOT
		<div class="input-group" id="quick-search">
			<input type="text" id="SearchString" name="SearchString" value="{$safe_search}" class="form-control" placeholder="{$safe_label}">
			<span class="input-group-btn">
				<button name="Search_x" value="1" id="Search" type="submit" onClick="{$reset_selection}" class="btn btn-default" title="{$safe_label}"><i class="glyphicon glyphicon-search"></i></button>
				<button name="ClearQuickSearch" value="1" id="ClearQuickSearch" type="submit" onClick="\$j('#SearchString').val(''); {$reset_selection}" class="btn btn-default" title="{$safe_clear_label}"><i class="glyphicon glyphicon-remove-circle"></i></button>
			</span>
		</div>
EOT;
		return $html;
	}

	#########################################################

