<?php
// This script and data application were generated by AppGini 5.96
// Download AppGini for free from https://bigprof.com/appgini/download/

	$currDir = dirname(__FILE__);
	include_once("{$currDir}/lib.php");
	@include_once("{$currDir}/hooks/grupo_dispo.php");
	include_once("{$currDir}/grupo_dispo_dml.php");

	// mm: can the current member access this page?
	$perm = getTablePermissions('grupo_dispo');
	if(!$perm['access']) {
		echo error_message($Translation['tableAccessDenied']);
		exit;
	}

	$x = new DataList;
	$x->TableName = 'grupo_dispo';

	// Fields that can be displayed in the table view
	$x->QueryFieldsTV = [
		"`grupo_dispo`.`id_grupo_dispo`" => "id_grupo_dispo",
		"`grupo_dispo`.`grupo_dispo`" => "grupo_dispo",
		"`grupo_dispo`.`descripcion`" => "descripcion",
	];
	// mapping incoming sort by requests to actual query fields
	$x->SortFields = [
		1 => '`grupo_dispo`.`id_grupo_dispo`',
		2 => 2,
		3 => 3,
	];

	// Fields that can be displayed in the csv file
	$x->QueryFieldsCSV = [
		"`grupo_dispo`.`id_grupo_dispo`" => "id_grupo_dispo",
		"`grupo_dispo`.`grupo_dispo`" => "grupo_dispo",
		"`grupo_dispo`.`descripcion`" => "descripcion",
	];
	// Fields that can be filtered
	$x->QueryFieldsFilters = [
		"`grupo_dispo`.`id_grupo_dispo`" => "ID",
		"`grupo_dispo`.`grupo_dispo`" => "Grupo del dispositivo",
		"`grupo_dispo`.`descripcion`" => "Descripcion",
	];

	// Fields that can be quick searched
	$x->QueryFieldsQS = [
		"`grupo_dispo`.`id_grupo_dispo`" => "id_grupo_dispo",
		"`grupo_dispo`.`grupo_dispo`" => "grupo_dispo",
		"`grupo_dispo`.`descripcion`" => "descripcion",
	];

	// Lookup fields that can be used as filterers
	$x->filterers = [];

	$x->QueryFrom = "`grupo_dispo` ";
	$x->QueryWhere = '';
	$x->QueryOrder = '';

	$x->AllowSelection = 1;
	$x->HideTableView = ($perm['view'] == 0 ? 1 : 0);
	$x->AllowDelete = $perm['delete'];
	$x->AllowMassDelete = (getLoggedAdmin() !== false);
	$x->AllowInsert = $perm['insert'];
	$x->AllowUpdate = $perm['edit'];
	$x->SeparateDV = 1;
	$x->AllowDeleteOfParents = 0;
	$x->AllowFilters = (getLoggedAdmin() !== false);
	$x->AllowSavingFilters = (getLoggedAdmin() !== false);
	$x->AllowSorting = 1;
	$x->AllowNavigation = 1;
	$x->AllowPrinting = 1;
	$x->AllowPrintingDV = 1;
	$x->AllowCSV = 1;
	$x->RecordsPerPage = 10;
	$x->QuickSearch = 1;
	$x->QuickSearchText = $Translation['quick search'];
	$x->ScriptFileName = 'grupo_dispo_view.php';
	$x->RedirectAfterInsert = 'grupo_dispo_view.php?SelectedID=#ID#';
	$x->TableTitle = 'Grupo del dispositivo';
	$x->TableIcon = 'resources/table_icons/cog.png';
	$x->PrimaryKey = '`grupo_dispo`.`id_grupo_dispo`';

	$x->ColWidth = [150, 150, ];
	$x->ColCaption = ['Grupo del dispositivo', 'Descripcion', ];
	$x->ColFieldName = ['grupo_dispo', 'descripcion', ];
	$x->ColNumber  = [2, 3, ];

	// template paths below are based on the app main directory
	$x->Template = 'templates/grupo_dispo_templateTV.html';
	$x->SelectedTemplate = 'templates/grupo_dispo_templateTVS.html';
	$x->TemplateDV = 'templates/grupo_dispo_templateDV.html';
	$x->TemplateDVP = 'templates/grupo_dispo_templateDVP.html';

	$x->ShowTableHeader = 1;
	$x->TVClasses = "";
	$x->DVClasses = "";
	$x->HasCalculatedFields = false;
	$x->AllowConsoleLog = false;
	$x->AllowDVNavigation = true;

	// mm: build the query based on current member's permissions
	$DisplayRecords = $_REQUEST['DisplayRecords'];
	if(!in_array($DisplayRecords, ['user', 'group'])) { $DisplayRecords = 'all'; }
	if($perm['view'] == 1 || ($perm['view'] > 1 && $DisplayRecords == 'user' && !$_REQUEST['NoFilter_x'])) { // view owner only
		$x->QueryFrom .= ', `membership_userrecords`';
		$x->QueryWhere = "WHERE `grupo_dispo`.`id_grupo_dispo`=`membership_userrecords`.`pkValue` AND `membership_userrecords`.`tableName`='grupo_dispo' AND LCASE(`membership_userrecords`.`memberID`)='" . getLoggedMemberID() . "'";
	} elseif($perm['view'] == 2 || ($perm['view'] > 2 && $DisplayRecords == 'group' && !$_REQUEST['NoFilter_x'])) { // view group only
		$x->QueryFrom .= ', `membership_userrecords`';
		$x->QueryWhere = "WHERE `grupo_dispo`.`id_grupo_dispo`=`membership_userrecords`.`pkValue` AND `membership_userrecords`.`tableName`='grupo_dispo' AND `membership_userrecords`.`groupID`='" . getLoggedGroupID() . "'";
	} elseif($perm['view'] == 3) { // view all
		// no further action
	} elseif($perm['view'] == 0) { // view none
		$x->QueryFields = ['Not enough permissions' => 'NEP'];
		$x->QueryFrom = '`grupo_dispo`';
		$x->QueryWhere = '';
		$x->DefaultSortField = '';
	}
	// hook: grupo_dispo_init
	$render = true;
	if(function_exists('grupo_dispo_init')) {
		$args = [];
		$render = grupo_dispo_init($x, getMemberInfo(), $args);
	}

	if($render) $x->Render();

	// hook: grupo_dispo_header
	$headerCode = '';
	if(function_exists('grupo_dispo_header')) {
		$args = [];
		$headerCode = grupo_dispo_header($x->ContentType, getMemberInfo(), $args);
	}

	if(!$headerCode) {
		include_once("{$currDir}/header.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/header.php");
		echo str_replace('<%%HEADER%%>', ob_get_clean(), $headerCode);
	}

	echo $x->HTML;

	// hook: grupo_dispo_footer
	$footerCode = '';
	if(function_exists('grupo_dispo_footer')) {
		$args = [];
		$footerCode = grupo_dispo_footer($x->ContentType, getMemberInfo(), $args);
	}

	if(!$footerCode) {
		include_once("{$currDir}/footer.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/footer.php");
		echo str_replace('<%%FOOTER%%>', ob_get_clean(), $footerCode);
	}
