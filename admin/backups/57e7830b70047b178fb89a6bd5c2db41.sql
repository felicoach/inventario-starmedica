-- MySQL dump 10.13  Distrib 5.6.32-78.1, for Linux (x86_64)
--
-- Host: localhost    Database: starmedi_bd_equipos
-- ------------------------------------------------------
-- Server version	5.6.32-78.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesorios`
--

DROP TABLE IF EXISTS `accesorios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesorios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iddispo` int(10) unsigned DEFAULT NULL,
  `Accesorio` int(10) unsigned NOT NULL,
  `fecha_asignacion` date NOT NULL,
  `comentarios` text,
  `id_dispo` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iddispo` (`iddispo`),
  KEY `Accesorio` (`Accesorio`),
  KEY `id_dispo` (`id_dispo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesorios`
--

LOCK TABLES `accesorios` WRITE;
/*!40000 ALTER TABLE `accesorios` DISABLE KEYS */;
/*!40000 ALTER TABLE `accesorios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calibraciones`
--

DROP TABLE IF EXISTS `calibraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calibraciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` int(10) unsigned NOT NULL,
  `serial` int(10) unsigned DEFAULT NULL,
  `tipo_calibracion` int(10) unsigned NOT NULL,
  `fecha_calibra` date NOT NULL,
  `fecha_expiracion` date DEFAULT NULL,
  `empresa` int(10) unsigned NOT NULL,
  `documento` varchar(40) NOT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  KEY `codigo` (`codigo`),
  KEY `tipo_calibracion` (`tipo_calibracion`),
  KEY `empresa` (`empresa`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calibraciones`
--

LOCK TABLES `calibraciones` WRITE;
/*!40000 ALTER TABLE `calibraciones` DISABLE KEYS */;
INSERT INTO `calibraciones` VALUES (1,26,26,2,'2020-09-10','2020-09-30',6,'mtto12345.docx','10/9/2020 04:51:05 pm',NULL,'lgarcia',NULL,NULL);
/*!40000 ALTER TABLE `calibraciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudades` (
  `id_ciudad` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_ciudad`),
  UNIQUE KEY `ciudad_unique` (`ciudad`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudades`
--

LOCK TABLES `ciudades` WRITE;
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` VALUES (1,'CALI',NULL),(2,'BUGA',NULL),(3,'TULUA',NULL),(4,'PALMIRA',NULL),(5,'BOGOTA',NULL),(6,'CALIFORNIA',NULL),(7,'CHINA',NULL);
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigoserial`
--

DROP TABLE IF EXISTS `codigoserial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codigoserial` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` int(10) unsigned DEFAULT NULL,
  `serial` int(10) unsigned DEFAULT NULL,
  `tipo_dispo` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `codigo` (`codigo`),
  KEY `serial` (`serial`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigoserial`
--

LOCK TABLES `codigoserial` WRITE;
/*!40000 ALTER TABLE `codigoserial` DISABLE KEYS */;
/*!40000 ALTER TABLE `codigoserial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compras` (
  `id_compras` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` int(10) unsigned NOT NULL,
  `serial` varchar(40) DEFAULT NULL,
  `proveedor` int(10) unsigned NOT NULL,
  `identi` int(10) unsigned DEFAULT NULL,
  `tel_provededor` int(10) unsigned DEFAULT NULL,
  `ciudad_origen` int(10) unsigned DEFAULT NULL,
  `fecha_factura` date DEFAULT NULL,
  `factura` varchar(40) NOT NULL,
  `precio` int(11) DEFAULT NULL,
  `garantia` date DEFAULT NULL,
  `fecha_compra` date DEFAULT NULL,
  `comentarios` text,
  `reg_creado` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `soporte` varchar(40) DEFAULT NULL,
  `field19` varchar(40) DEFAULT NULL,
  `comprado_por` int(10) unsigned NOT NULL,
  `tipo_dispo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned DEFAULT NULL,
  `modelo` int(10) unsigned DEFAULT NULL,
  `referencia` varchar(40) DEFAULT NULL,
  `ficha_tecnica` varchar(40) DEFAULT NULL,
  `otro_documento` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` varchar(40) DEFAULT NULL,
  `creado1` datetime NOT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field27` varchar(40) DEFAULT NULL,
  `field28` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_compras`),
  KEY `codigo` (`codigo`),
  KEY `proveedor` (`proveedor`),
  KEY `ciudad_origen` (`ciudad_origen`),
  KEY `comprado_por` (`comprado_por`),
  KEY `tipo_dispo` (`tipo_dispo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` VALUES (2,57,'57',4,4,4,7,'2019-11-14','DMID191114-16',3800000,'2020-08-10','2019-11-14','Factura de compra China','10/12/2019 06:57:48 pm','26/12/2019 06:22:55 pm','felipem','felipem',NULL,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-12-10 00:00:00',NULL,NULL,NULL,NULL),(3,58,'58',4,4,4,7,'2019-11-14','DMID191114-16',3800000,'2020-08-10','2019-11-14','Factura de compra China','10/12/2019 06:57:57 pm','10/12/2019 06:58:24 pm','felipem','felipem','788a619b9dac4720e.pdf',NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-12-10 00:00:00',NULL,NULL,NULL,NULL),(4,223,'223',1,1,1,1,'2020-02-12','TP-46268',1799000,'2021-02-12','2020-02-12','<br>','19/2/2020 11:57:09 am',NULL,'felipem',NULL,NULL,NULL,3,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-19 00:00:00',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactos`
--

DROP TABLE IF EXISTS `contactos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactos` (
  `id_contactos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_contacto` int(10) unsigned NOT NULL,
  `tipo_iden` int(10) unsigned NOT NULL,
  `identificacion` varchar(40) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `ciudad` int(10) unsigned NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `direccion` text,
  `tipo_relacion` int(10) unsigned NOT NULL,
  `nota` text,
  `reg_creado` datetime DEFAULT NULL,
  `update` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `creado1` datetime NOT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field15` varchar(40) DEFAULT NULL,
  `field16` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_contactos`),
  UNIQUE KEY `identificacion_unique` (`identificacion`),
  KEY `tipo_contacto` (`tipo_contacto`),
  KEY `tipo_iden` (`tipo_iden`),
  KEY `ciudad` (`ciudad`),
  KEY `tipo_relacion` (`tipo_relacion`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactos`
--

LOCK TABLES `contactos` WRITE;
/*!40000 ALTER TABLE `contactos` DISABLE KEYS */;
INSERT INTO `contactos` VALUES (1,2,2,'900355222-7','TIENDAS TECNOPLAZA SAS',1,'4851150','LOCAL 203 CENTRO COMERCIAL LA PASARELA AVENIDA 5A N NRO 23DN - 68',1,NULL,'0000-00-00 00:00:00',NULL,'vilmao',NULL,'2019-11-27 00:00:00',NULL,NULL,NULL,NULL),(2,1,1,'465465','WILLIAM IBARRA',1,'23423',NULL,2,NULL,'0000-00-00 00:00:00',NULL,'vilmao',NULL,'2019-11-27 00:00:00',NULL,NULL,NULL,NULL),(3,1,1,'1061718393','FELIPE MOLANO',1,'3135507417','Cra 53#98-91,apto 704-3',2,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00','felipem','felipem','2019-11-29 00:00:00',NULL,NULL,NULL,NULL),(4,2,3,'510663','DMSoftware',7,'+86','Rm 202,2nd Floor,No.604-1,Guangshan 2nd Rd,Tianhe District, Guangzhou,Guangdong,China 510663',1,'Proveedor DMS\r\nContacto Don Goh','0000-00-00 00:00:00','0000-00-00 00:00:00','felipem','felipem','2019-12-10 00:00:00',NULL,NULL,NULL,NULL),(5,1,1,'1002971312','JUAN CAMILO ZAMBRANO',1,'3185541002','LA REFORMA',3,'APOYO EN TECNOLOGIA','0000-00-00 00:00:00',NULL,'camiloz',NULL,'2020-02-29 00:00:00',NULL,NULL,NULL,NULL),(6,1,1,'3452345','DIEGO TRIANA',2,'63456345',NULL,3,'Biomedico','2020-09-10 16:45:23',NULL,'lgarcia',NULL,'0000-00-00 00:00:00','2020-09-10 16:45:23',NULL,NULL,NULL);
/*!40000 ALTER TABLE `contactos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivos`
--

DROP TABLE IF EXISTS `dispositivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivos` (
  `id_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(4) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `ubicacion` int(10) unsigned NOT NULL,
  `ubicacion_abre` int(10) unsigned DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) unsigned NOT NULL,
  `creado2` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) unsigned DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado1` datetime DEFAULT NULL,
  PRIMARY KEY (`id_dispo`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  UNIQUE KEY `serial_unique` (`serial`),
  UNIQUE KEY `codigo` (`codigo`),
  KEY `tipo_dispositivo` (`tipo_dispositivo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `ubicacion` (`ubicacion`),
  KEY `estado` (`estado`)
) ENGINE=MyISAM AUTO_INCREMENT=258 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivos`
--

LOCK TABLES `dispositivos` WRITE;
/*!40000 ALTER TABLE `dispositivos` DISABLE KEYS */;
INSERT INTO `dispositivos` VALUES (1,'a0c7a87f98ff4c423.jpg','101','3A-7125',1,1,1,6,6,'2018-11-20',1,'27/11/2019 09:37:37 am','11/5/2020 09:08:01 am','felipem','felipem','<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span>',1,'HOLTER 101','2019-11-27 00:00:00',NULL),(2,'8710a41018a7c4e42.jpg','102','3A-11605',1,1,1,1,1,'2019-11-27',1,'27/11/2019 09:40:39 am','11/5/2020 09:10:06 am','felipem','felipem','<br>',1,'HOLTER 102','2019-11-27 00:00:00',NULL),(3,'6630f469154d3320d.jpg','103','3A-8045',1,1,1,3,3,'2019-12-27',1,NULL,'11/5/2020 09:12:40 am',NULL,'felipem','Tenia el serial 3A-8044, pero se cambia a 3A-8045',1,'HOLTER 103','2020-03-05 00:00:00',NULL),(4,'796ef58bbcaf815d5.jpg','104','3A-11606',1,1,1,2,2,'2019-12-27',1,NULL,'11/5/2020 09:13:20 am',NULL,'felipem','<br>',1,'HOLTER 104','2020-03-05 00:00:00',NULL),(5,'a1ca49f2da0d3858a.jpg','105','3A-11931',1,1,1,3,3,'2019-12-27',1,NULL,'11/5/2020 09:13:59 am',NULL,'felipem','Antes tenia el 3A-1039',1,'HOLTER 105','2020-03-05 00:00:00',NULL),(6,'fa31345b2dec3f75d.jpg','106','3A-11607',1,1,1,2,2,'2019-12-27',1,NULL,'11/5/2020 09:14:20 am',NULL,'felipem','Aparece con 3a-11934',1,'HOLTER 106','2020-03-05 00:00:00',NULL),(7,'62f8f3afd6062cf73.jpg','107','3A-11608',1,1,1,7,7,NULL,1,NULL,'11/5/2020 09:14:43 am',NULL,'felipem','<br>',1,'HOLTER 107','2020-03-05 00:00:00',NULL),(8,'16a714e4f58af1930.jpg','108','3A-2151',1,1,1,6,6,'2019-12-27',1,NULL,'11/5/2020 09:15:00 am',NULL,'felipem','Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial',1,'HOLTER 108','2020-03-05 00:00:00',NULL),(9,'fa647e5ab77632692.jpg','109','3A-9421',1,1,1,13,13,'2019-12-03',1,NULL,'10/9/2020 04:55:26 pm',NULL,'superadmin','<br>',1,'HOLTER 109','2020-03-05 00:00:00',NULL),(10,'476bc82bf3bed4616.jpg','110','3A-9310',1,1,1,6,6,'2018-11-20',1,NULL,'11/5/2020 09:15:48 am',NULL,'felipem','Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.',1,'HOLTER 110','2020-03-05 00:00:00',NULL),(11,'a456dd57854fcd34f.jpg','111','3A-11609',1,1,1,7,7,NULL,1,NULL,'11/5/2020 09:16:04 am',NULL,'felipem','<br>',1,'HOLTER 111','2020-03-05 00:00:00',NULL),(12,'e79c8397c6fa85698.jpg','112','3A-12178',1,1,1,2,2,'2019-12-27',1,NULL,'11/5/2020 09:16:23 am',NULL,'felipem','Aparece con 130 pero es 112&nbsp;',1,'HOLTER 112','2020-03-05 00:00:00',NULL),(13,'79766d196e19eb1db.jpg','113','3A-9311',1,1,1,2,2,'2019-12-27',1,NULL,'11/5/2020 09:16:40 am',NULL,'felipem','Aparece con 3A-11948',1,'HOLTER 113','2020-03-05 00:00:00',NULL),(14,'1c1558589faf656c3.jpg','114','3A-11815',1,1,1,6,6,'2020-03-04',1,NULL,'11/5/2020 09:16:56 am',NULL,'felipem','Actualizo, se encuentra en FHSJ, funcionamiento normal 04-Marzo-2020, Camilo.',1,'HOLTER 114','2020-03-05 00:00:00',NULL),(15,'a47482f69e6bb1445.jpg','115','3A-05295',1,1,1,7,7,'2019-12-27',3,NULL,'27/12/2019 11:59:48 am',NULL,'felipem','Equipo fundido en desfibrilacion según estado de las tarjetas',1,'HOLTER 115','2020-03-05 00:00:00',NULL),(16,'fad32fe2f96f1b6f2.jpg','116','3A-8940',1,1,1,6,6,'2020-03-04',1,NULL,'11/5/2020 09:17:48 am',NULL,'felipem','Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.',1,'HOLTER 116','2020-03-05 00:00:00',NULL),(17,'f6216dd5ac51a2f82.jpg','117','3A-9484',1,1,1,1,1,'2019-12-03',1,NULL,'11/5/2020 09:18:07 am',NULL,'felipem','<br>',1,'HOLTER 117','2020-03-05 00:00:00',NULL),(18,'6b9ff54089c40cbf3.jpg','118','3A-9486',1,1,1,4,4,NULL,1,NULL,'11/5/2020 09:18:24 am',NULL,'felipem','<br>',1,'HOLTER 118','2020-03-05 00:00:00',NULL),(19,'ad6939e30d7026eee.jpg','119','3A-9572',1,1,1,6,6,'2020-03-17',1,NULL,'11/5/2020 09:23:03 am',NULL,'felipem','Actualizo, funciona normal en FHSJ 17/Marzo/2020 Camilo.',1,'HOLTER 119','2020-03-05 00:00:00',NULL),(20,'636978098692e0616.jpg','120','3A-9487',1,1,1,2,2,'2019-12-27',1,NULL,'27/12/2019 06:11:01 am',NULL,'felipem','<br>',1,'HOLTER 120','2020-03-05 00:00:00',NULL),(21,'05353fef66bf34245.jpg','121','3A-8946',1,1,1,4,4,'2020-03-06',1,NULL,'6/3/2020 01:12:23 pm',NULL,'camiloz','Actualizo, funcionamiento normal, se cambia la etiqueta del código y del serial 06-Marzo-2020, Camilo.',1,'HOLTER 121','2020-03-05 00:00:00',NULL),(22,'753003a921f5f02ec.jpg','122','3A-9485',1,1,1,4,4,'2020-03-06',1,NULL,'6/3/2020 02:04:04 pm',NULL,'camiloz','Actualizo, funcionamiento normal HTU, se cambió la etiqueta del código y del serial, 06/Marzo/2020.',1,'HOLTER 122','2020-03-05 00:00:00',NULL),(23,'5cc88d4b86424bb7d.jpg','123','3A-1993',1,1,1,6,6,'2020-03-04',1,NULL,'9/3/2020 03:45:45 pm',NULL,'camiloz','Antes holter 112, Felipe.<div><br><div>El Holter 123 realmente se encuentra en FHSJ, pendiente revisar el clon en HIDC 04-Marzo-2020, Camilo.</div></div>',1,'HOLTER 123','2020-03-05 00:00:00',NULL),(24,'b89cb8d899578dd8b.jpg','124','3A-10318',1,1,1,5,5,'2019-12-27',1,NULL,'27/12/2019 06:26:34 am','','felipem','Aparece con 3a-9311 en csf',1,'HOLTER 124','2020-03-05 00:00:00',NULL),(25,'2b4b8d4d10dddbe8b.jpg','125','3A-11816',1,1,1,4,4,'2020-03-06',1,NULL,'6/3/2020 02:05:19 pm',NULL,'camiloz','Aparece con el serial 3A -7125, el cual es el serial del HOLTER 101, se cambian etiquetas y queda con el serial&nbsp; 3A-11816. 06/Marzo/2020. Camilo',1,'HOLTER 125','2020-03-05 00:00:00',NULL),(26,'06b68e149d1227503.jpg','126','3A-8044',1,1,1,13,13,'2019-12-03',1,NULL,'10/9/2020 04:33:31 pm',NULL,'felipem','Antes 3A-11818',1,'HOLTER 126','2020-03-05 00:00:00',NULL),(27,'f4964486301b6afe2.jpg','127','3A-12175',1,1,1,5,5,'2019-12-27',1,NULL,'6/3/2020 03:57:55 pm',NULL,'camiloz','Actualizo, funcionamiento normal CSF, 06/Marzo/2020, Camilo.',1,'HOLTER 127','2020-03-05 00:00:00',NULL),(28,'d179e9e2972b91703.jpg','128','3A-12176',1,1,1,5,5,'2019-12-27',1,NULL,'6/3/2020 04:08:07 pm',NULL,'camiloz','Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo',1,'HOLTER 128','2020-03-05 00:00:00',NULL),(29,'9e12d7e5ca84cd0f4.jpg','129','3A-11940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:29:09 am',NULL,'felipem',NULL,NULL,'HOLTER 129','2020-03-05 00:00:00',NULL),(57,'c11ec5b51107d1e95.jpg','130','3A-11295',1,1,1,9,9,'2019-11-29',1,'29/11/2019 04:45:35 pm','5/12/2019 11:02:44 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL,'HOLTER 130','2019-11-29 00:00:00',NULL),(31,'3cc42f4af7a84575e.jpg','201','US97804887',3,3,3,5,5,'2020-03-06',1,NULL,'11/5/2020 09:24:51 am',NULL,'felipem','Aparecía en Bodega, realmente se encuentra en CSF, Funciona normalmente, 06/Marzo/2020, Camilo.',3,'ECO 201','2020-03-05 00:00:00',NULL),(32,'536dec1c5345e2b59.jpg','202','US97806979',3,3,3,6,6,NULL,1,NULL,'29/11/2019 01:11:49 am',NULL,'felipem',NULL,NULL,'ECO 202','2020-03-05 00:00:00',NULL),(33,'2edf9e5e9f0adf0b0.jpg','203','3728A01811',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:12:32 am',NULL,'felipem',NULL,NULL,'ECO 203','2020-03-05 00:00:00',NULL),(34,'b35b0af60ed58ef47.jpg','204','US50210876',3,3,3,3,3,'2020-03-02',1,NULL,'21/3/2020 12:44:16 pm',NULL,'camiloz','Aparecía en Bodega, realmente está en HROB. 02/Marzo/2020 Camilo.<div><br></div><div>Actualizo, el día de hoy 17/Marzo/2020 se lleva equipo desde HROB (Palmira) a FHSJ (Buga), Camilo.</div>',3,'ECO 204','2020-03-05 00:00:00',NULL),(35,'d6bb8df0f60c48ab0.jpg','205','3728A00382',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:19:52 am',NULL,'felipem',NULL,NULL,'ECO 205','2020-03-05 00:00:00',NULL),(36,'620450dbf61c4c19f.jpg','206','9708A09160',3,3,3,7,7,'2020-03-05',2,NULL,'5/3/2020 10:10:07 pm',NULL,'camiloz','Actualizo, Aparecía en HTU, realmente está en Bodega. 05/Marzo/2020. Camilo',3,'ECO 206','2020-03-05 00:00:00',NULL),(37,'41736a0baf4cb45e3.jpg','207','71674',3,6,5,7,7,NULL,1,NULL,'29/11/2019 01:24:01 am',NULL,'felipem',NULL,NULL,'ECO 207','2020-03-05 00:00:00',NULL),(38,'0eb9fdc48d520b1a8.jpg','208','001838VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:29 am',NULL,'felipem',NULL,NULL,'ECO 208','2020-03-05 00:00:00',NULL),(39,'f7c6ea863e2c80a47.jpg','209','055133VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:16 am',NULL,'felipem',NULL,NULL,'ECO 209','2020-03-05 00:00:00',NULL),(40,'c86621e1a393abd5e.jpg','210','3728A01073',3,3,3,4,4,'2019-12-05',1,NULL,'21/3/2020 01:28:19 pm',NULL,'camiloz','Actualizo, Funciona normalmente en HTU, 21/Marzo/2020, Camilo.',3,'ECO 210','2020-03-05 00:00:00',NULL),(41,'158d01f499364579d.jpg','211','USO0211434',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:06 am',NULL,'felipem',NULL,NULL,'ECO 211','2020-03-05 00:00:00',NULL),(42,'387a137ebf96710b6.jpg','212','US97807671',3,3,3,2,2,'2020-02-26',1,NULL,'2/3/2020 03:24:26 pm',NULL,'camiloz','<br>',3,'ECO 212','2020-03-05 00:00:00',NULL),(43,'5ed931c87e7ad07f3.jpg','213','9708A04535',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:34 am',NULL,'felipem',NULL,NULL,'ECO 213','2020-03-05 00:00:00',NULL),(44,'e624e9cfa97691728.jpg','214','3728A00160',3,3,3,7,7,'2019-12-04',2,NULL,'29/2/2020 11:07:09 am',NULL,'camiloz','No se encuentra en FHSJ',3,'ECO 214','2020-03-05 00:00:00',NULL),(45,'54d451022df8c30e6.jpg','215','US97703545',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:07 am',NULL,'felipem',NULL,NULL,'ECO 215','2020-03-05 00:00:00',NULL),(46,'0b54a448e7a5a93aa.jpg','216','US97808948',3,3,3,6,6,'2020-03-04',1,NULL,'4/3/2020 06:45:35 pm',NULL,'camiloz','<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>',3,'ECO 216','2020-03-05 00:00:00',NULL),(47,'630053d26916342ca.jpg','217','US97800318',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:57 am',NULL,'felipem',NULL,NULL,'ECO 217','2020-03-05 00:00:00',NULL),(48,'9972d0e70d8ca4ca6.jpg','218','3728A02302',3,3,3,6,6,'2020-02-20',3,NULL,'17/3/2020 06:33:16 pm',NULL,'camiloz','Actualizado, se encontraba en UCI río se deja en FHSJ 20-02-2018<div><br></div><div>Actualizo, equipo fuera de servicio, en reparación, 04/Marzo/2020, Camilo.<br><div><br></div></div>',3,'ECO 218','2020-03-05 00:00:00',NULL),(49,'2e9dc0bf01f8da61f.jpg','219','US80210739',3,3,3,1,1,'2019-12-03',1,NULL,'29/2/2020 09:31:18 pm',NULL,'camiloz','<br>',3,'ECO 219','2020-03-05 00:00:00',NULL),(50,'6e5eaa081a347bc98.jpg','220','US97703205',3,3,3,1,1,'2019-12-03',1,NULL,'3/12/2019 11:27:58 am',NULL,'felipem','<br>',NULL,'ECO 220','2020-03-05 00:00:00',NULL),(51,'10b8abd62f6418d47.jpg','221','US97804106',3,3,3,3,3,'2020-03-02',1,NULL,'21/3/2020 12:43:04 pm',NULL,'camiloz','Actualizo, Funciona normalmete en HROB, 02/Marzo/2020, Camilo.',3,'ECO 221','2020-03-05 00:00:00',NULL),(52,'b6efed45a482c8c76.jpg','222','US97804535',3,3,3,7,7,'2020-03-05',2,NULL,'5/3/2020 10:19:31 pm',NULL,'camiloz','Actualizo, está en Bodega inactivo 05/Marzo/2020. Camilo',3,'ECO 222','2020-03-05 00:00:00',NULL),(53,'326d48038544d0081.jpg','223','US70421029',3,3,3,5,5,'2020-03-06',1,NULL,'6/3/2020 03:23:21 pm',NULL,'camiloz','Aparecía en Bodega, realmente se encuentra en Clínica San Francisco 06/Marzo/2020, Camilo',3,'ECO 223','2020-03-05 00:00:00',NULL),(54,'2114d2a34043479d1.jpg','224','US97703258',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:26:47 am',NULL,'felipem',NULL,NULL,'ECO 224','2020-03-05 00:00:00',NULL),(55,'99c80bd69f2b7d401.jpg','225','USD0211891',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:06:31 am',NULL,'felipem','Verificar código puesto en el equipo&nbsp;',NULL,'ECO 225','2020-03-05 00:00:00',NULL),(56,'a4df3e5a8cd507c06.jpg','621','JCN0GR05Z985517',11,7,6,7,7,'2019-08-07',1,'27/11/2019 06:30:03 pm','22/5/2020 12:38:37 pm','vilmao','felipem','<br>',6,'PC 621','2019-11-27 00:00:00',NULL),(58,'2fb3b0f1c5f8919a5.jpg','131','3A-11287',1,1,1,9,9,'2019-11-29',1,'29/11/2019 05:06:08 pm','5/12/2019 11:03:20 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL,'HOLTER 131','2019-11-29 00:00:00',NULL),(59,'982c34a6a71597fe5.jpg','132','3A-11293',1,1,1,1,1,'2019-12-03',1,'3/12/2019 12:38:32 am','5/12/2019 11:04:07 pm','felipem','felipem','Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL,'HOLTER 132','2019-12-03 00:00:00',NULL),(60,'5c9c7e2c36eb50b7f.jpg','133','3A-11285',1,1,1,1,1,'2019-11-29',1,'3/12/2019 12:41:03 am','5/12/2019 11:04:28 pm','felipem','felipem','Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL,'HOLTER 133','2019-12-03 00:00:00',NULL),(61,'f19510876e2eb19cd.jpeg','551','IP309100004',2,2,7,3,3,NULL,3,NULL,'5/12/2019 12:01:35 am',NULL,'felipem','Equipo dañado&nbsp;',NULL,'MAPA 551','2020-03-05 00:00:00',NULL),(62,'1d40088c027c6481a.jpeg','552','00087513',2,8,8,6,6,'2019-12-04',1,NULL,'4/12/2019 11:59:19 pm',NULL,'felipem','<br>',NULL,'MAPA 552','2020-03-05 00:00:00',NULL),(63,NULL,'553','140206005',2,2,7,3,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 553','2020-03-05 00:00:00',NULL),(64,NULL,'554','IP1602200009',2,2,2,7,7,'2019-12-27',3,NULL,'27/12/2019 12:48:25 pm',NULL,'felipem','flm',2,'MAPA 554','2020-03-05 00:00:00',NULL),(65,NULL,'555','IP1602200106',2,2,2,6,6,'2020-03-04',1,NULL,'4/3/2020 06:40:43 pm',NULL,'camiloz','<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>',2,'MAPA 555','2020-03-05 00:00:00',NULL),(66,NULL,'556','IP1605200325',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 556','2020-03-05 00:00:00',NULL),(67,NULL,'557','IP1602200057',2,2,2,4,4,'2020-03-06',1,NULL,'6/3/2020 02:25:21 pm',NULL,'camiloz','Actualizo, funciona normalmente en HTU, se coloca nueva etiqueta con código y serial. 06/Marzo/2020, Camilo',2,'MAPA 557','2020-03-05 00:00:00',NULL),(68,NULL,'558','IP1602200170',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 558','2020-03-05 00:00:00',NULL),(69,NULL,'559','IP1602200295',2,2,2,7,7,NULL,1,NULL,'5/12/2019 09:41:05 am',NULL,'felipem','<br>',NULL,'MAPA 559','2020-03-05 00:00:00',NULL),(70,NULL,'560','IP1602200047',2,2,2,6,6,'2020-03-17',1,NULL,'17/3/2020 06:15:26 pm',NULL,'camiloz','Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.',2,'MAPA 560','2020-03-05 00:00:00',NULL),(71,NULL,'561','IP1602200018',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 561','2020-03-05 00:00:00',NULL),(72,'781ea01118ca82192.jpeg','562','17030100040',2,2,2,6,6,'2019-12-04',1,NULL,'17/3/2020 06:17:46 pm',NULL,'camiloz','Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.',2,'MAPA 562','2020-03-05 00:00:00',NULL),(73,NULL,'564','IP1701400086',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 564','2020-03-05 00:00:00',NULL),(74,NULL,'565','IP1701400310',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 565','2020-03-05 00:00:00',NULL),(75,NULL,'566','IP1701400267',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 566','2020-03-05 00:00:00',NULL),(76,NULL,'567','IP1701400116',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 567','2020-03-05 00:00:00',NULL),(77,NULL,'568','IP1701400067',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 568','2020-03-05 00:00:00',NULL),(78,'f0babaaecbf90b169.jpeg','569','17030100024',2,2,2,7,7,'2019-12-27',3,NULL,'27/12/2019 12:50:46 pm',NULL,'felipem','Equipo dañado&nbsp;',2,'MAPA 569','2020-03-05 00:00:00',NULL),(79,NULL,'570','17030100023',2,2,2,10,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 570','2020-03-05 00:00:00',NULL),(80,'7cdb97b607ea37f77.jpeg','571','17030100042',2,2,2,1,1,'2019-12-03',2,NULL,'29/2/2020 09:28:08 pm',NULL,'camiloz','Se encuentra en mantenimiento en la Bodega',2,'MAPA 571','2020-03-05 00:00:00',NULL),(81,NULL,'572','17030100190',2,2,2,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 572','2020-03-05 00:00:00',NULL),(82,'3bd9b5e8c99cc1012.jpeg','573','17030100192',2,2,2,1,1,'2019-12-03',1,NULL,'29/2/2020 09:23:43 pm',NULL,'camiloz','<br>',2,'MAPA 573','2020-03-05 00:00:00',NULL),(83,NULL,'574','17030100036',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 574','2020-03-05 00:00:00',NULL),(84,NULL,'575','17030100031',2,2,2,4,4,'2019-12-27',1,NULL,'8/10/2020 03:32:56 pm',NULL,'superadmin','Actualizo, Funcionamiento normal CSF, 06/Marzo/2020, Camilo.',2,'MAPA 575','2020-03-05 00:00:00',NULL),(85,NULL,'576','17030100016',2,2,2,3,3,'2019-12-27',1,NULL,'27/12/2019 11:36:25 am',NULL,'felipem','<br>',2,'MAPA 576','2020-03-05 00:00:00',NULL),(86,NULL,'577','17030100188',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 577','2020-03-05 00:00:00',NULL),(87,'dbd1a242bfc1e2cbf.jpeg','578','17030100055',2,2,2,1,1,'2019-12-03',1,NULL,'29/2/2020 11:20:10 pm',NULL,'camiloz','El dispositivo opera normalmente en el HIDC',2,'MAPA 578','2020-03-05 00:00:00',NULL),(88,NULL,'579','17030100033',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:55:48 pm',NULL,'felipem','<br>',2,'MAPA 579','2020-03-05 00:00:00',NULL),(89,NULL,'580','17030100037',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 580','2020-03-05 00:00:00',NULL),(90,NULL,'581','17030100029',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:53:47 pm',NULL,'felipem','<br>',2,'MAPA 581','2020-03-05 00:00:00',NULL),(91,NULL,'582','17030100030',2,2,2,4,4,NULL,1,NULL,'8/10/2020 03:15:37 pm',NULL,'superadmin','<br>',2,'MAPA 582','2020-03-05 00:00:00',NULL),(92,NULL,'583','17030100027',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:56:35 pm',NULL,'felipem','<br>',2,'MAPA 583','2020-03-05 00:00:00',NULL),(93,NULL,'584','17030100052',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:56:09 pm',NULL,'felipem','<br>',2,'MAPA 584','2020-03-05 00:00:00',NULL),(94,NULL,'585','17030100032',2,2,2,3,3,'2019-12-27',1,NULL,'27/12/2019 11:35:49 am',NULL,'felipem','flm',2,'MAPA 585','2020-03-05 00:00:00',NULL),(95,NULL,'586','17030100021',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 586','2020-03-05 00:00:00',NULL),(96,NULL,'587','17030100041',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 587','2020-03-05 00:00:00',NULL),(97,NULL,'588','17030100174',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 588','2020-03-05 00:00:00',NULL),(98,NULL,'589','17030100184',2,2,2,6,6,'2020-03-04',1,NULL,'4/3/2020 06:37:55 pm',NULL,'camiloz','<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>',2,'MAPA 589','2020-03-05 00:00:00',NULL),(99,NULL,'251','404262WX8',13,0,0,0,0,NULL,0,NULL,'29/2/2020 10:41:48 am',NULL,'camiloz','GE , 3Sc-RS',0,'TR 251','2020-03-05 00:00:00',NULL),(100,NULL,'252','333407WX5',13,0,0,0,0,NULL,0,NULL,'29/2/2020 10:42:06 am',NULL,'camiloz','GE , 3Sc-RS',0,'TR 252','2020-03-05 00:00:00',NULL),(101,NULL,'253','US97310163',13,3,25,7,7,'2020-03-03',2,NULL,'3/3/2020 05:17:34 pm',NULL,'camiloz','SE ENCUENTRA EN LA REFORMA',25,'TR 253','2020-03-05 00:00:00',NULL),(102,NULL,'254','US99N01008',13,0,0,0,0,NULL,0,NULL,'29/2/2020 10:43:00 am',NULL,'camiloz','HEWLETT PACKARD , S3',0,'TR 254','2020-03-05 00:00:00',NULL),(103,NULL,'255','02M7J5',13,0,0,0,0,NULL,0,NULL,'29/2/2020 10:43:32 am',NULL,'camiloz','HEWLETT PACKARD , 11-3L',0,'TR 255','2020-03-05 00:00:00',NULL),(104,NULL,'256','03DNL7',13,3,20,3,3,'2020-02-26',1,NULL,'12/5/2020 12:31:53 am',NULL,'felipem','PHILIPS , IPx-7',20,'TR 256','2020-03-05 00:00:00',NULL),(105,NULL,'257','US99N01942',13,3,16,1,1,'2020-05-07',1,NULL,'7/5/2020 12:01:48 pm',NULL,'felipem','SIUI , 21311A',16,'TR 257','2020-03-05 00:00:00',NULL),(106,NULL,'258','3709A00256',13,3,17,2,2,NULL,1,NULL,'26/2/2020 05:03:57 pm',NULL,'camiloz','HEWLETT PACKARD , 21350A',17,'TR 258','2020-03-05 00:00:00',NULL),(107,NULL,'259','US97404881',13,0,0,0,0,NULL,0,NULL,'29/2/2020 09:38:43 pm',NULL,'camiloz','PHILIPS , T62110',0,'TR 259','2020-03-05 00:00:00',NULL),(108,NULL,'260','US99300683',13,3,24,7,7,'2020-03-03',2,NULL,'3/3/2020 04:52:49 pm',NULL,'camiloz','SE ENCUENTRA EN LA REFORMA',24,'TR 260','2020-03-05 00:00:00',NULL),(109,NULL,'261','US97302745',13,3,17,6,6,'2020-02-26',1,NULL,'26/2/2020 08:46:42 pm',NULL,'camiloz','HEWLETT PACKARD , 21350A',17,'TR 261','2020-03-05 00:00:00',NULL),(110,NULL,'262','US99N04288',13,3,16,6,6,'2020-02-26',1,NULL,'26/2/2020 08:49:04 pm',NULL,'camiloz','PHILIPS&nbsp;',16,'TR 262','2020-03-05 00:00:00',NULL),(111,NULL,'263','02M7PY',13,3,20,4,4,'2020-02-26',1,NULL,'6/3/2020 11:41:46 am',NULL,'camiloz','Actualizo, funciona normalmente en HTU, 06/Marzo/2020. Camilo',20,'TR 263','2020-03-05 00:00:00',NULL),(112,NULL,'264','3630A00395',13,0,0,0,0,NULL,0,NULL,'29/2/2020 09:38:54 pm',NULL,'camiloz','HEWLETT PACKARD , 21367A',0,'TR 264','2020-03-05 00:00:00',NULL),(113,NULL,'265','18ZHQQ',13,0,0,0,0,NULL,0,NULL,'29/2/2020 09:39:01 pm',NULL,'camiloz','PHILIPS , 21369A',0,'TR 265','2020-03-05 00:00:00',NULL),(114,NULL,'266','02QDYM',13,3,16,6,6,'2020-02-26',1,NULL,'26/2/2020 08:58:19 pm',NULL,'camiloz','PHILIPS , 21311A',16,'TR 266','2020-03-05 00:00:00',NULL),(115,NULL,'267','142585PD9',13,0,0,0,0,NULL,0,NULL,'29/2/2020 09:39:08 pm',NULL,'camiloz','GE , 6S-RS',0,'TR 267','2020-03-05 00:00:00',NULL),(116,NULL,'268','US99300678',13,0,0,0,0,NULL,0,NULL,'29/2/2020 09:39:14 pm',NULL,'camiloz','HEWLETT PACKARD , 21321A',0,'TR 268','2020-03-05 00:00:00',NULL),(117,NULL,'269','US99N03966',13,3,16,6,6,'2020-02-26',1,NULL,'26/2/2020 08:50:48 pm',NULL,'camiloz','PHILLIPS , 21311A',16,'TR 269','2020-03-05 00:00:00',NULL),(118,NULL,'270','US97403824',13,0,0,0,0,NULL,0,NULL,'29/2/2020 09:39:21 pm',NULL,'camiloz','HEWLETT PACKARD ,',0,'TR 270','2020-03-05 00:00:00',NULL),(119,NULL,'271','3716A00792',13,3,26,7,7,'2020-02-29',1,NULL,'6/5/2020 06:09:03 pm',NULL,'felipem','Estaba en el HIDC, el dia 4 de Marzo se llevará para el HMC',26,'TR 271','2020-03-05 00:00:00',NULL),(120,NULL,'272','US99N04681',13,3,16,1,1,'2020-02-29',1,NULL,'29/2/2020 11:12:35 pm',NULL,'camiloz','PHILIPS , 21311A',16,'TR 272','2020-03-05 00:00:00',NULL),(121,NULL,'301','20161031/BTT02/00293',5,1,10,7,7,'2020-03-11',2,NULL,'11/3/2020 02:54:20 am',NULL,'camiloz','DMS 300-BTT02',10,'ECG 301','2020-03-05 00:00:00',NULL),(122,NULL,'302','20161031/BTT02/01743',5,0,0,0,0,NULL,0,NULL,'15/10/2020 12:52:01 pm',NULL,'superadmin','DMS 300-BTT02',0,'ECG 302','2020-03-05 00:00:00',NULL),(123,NULL,'303','20161031/BTT02/01729',5,1,10,6,6,'2020-03-17',1,NULL,'17/3/2020 04:42:57 pm',NULL,'camiloz','DMS 300-BTT02',10,'ECG 303','2020-03-05 00:00:00',NULL),(124,NULL,'304','20140721/BBT02/00290',5,1,10,1,1,'2020-02-29',1,NULL,'29/2/2020 12:00:24 pm',NULL,'camiloz','DMS 300-BTT02',10,'ECG 304','2020-03-05 00:00:00',NULL),(125,NULL,'305','D1081A0753',0,0,0,0,0,NULL,0,NULL,'15/10/2020 12:54:59 pm',NULL,'superadmin','CONTEC 8000',0,NULL,'2020-03-05 00:00:00',NULL),(126,NULL,'401','DG401',15,9,9,1,1,'2019-12-10',1,NULL,'29/2/2020 10:30:39 am',NULL,'camiloz',NULL,9,'DG 401','2020-03-05 00:00:00',NULL),(127,NULL,'402','DG402',15,9,9,6,6,'2020-02-07',3,NULL,'18/2/2020 10:17:56 pm',NULL,'felipem','EQUIPO DAÑADO',9,'DG 402','2020-03-05 00:00:00',NULL),(128,'18a13cd3f35261938.jpg','403','180403',15,9,9,6,6,'2019-12-10',1,NULL,'20/2/2020 05:27:40 pm',NULL,'felipem','Usado también para Uci Fatima&nbsp;',9,'DG 403','2020-03-05 00:00:00',NULL),(129,NULL,'404','DG404',15,9,9,4,4,'2020-02-07',1,NULL,'6/3/2020 09:57:03 am',NULL,'camiloz','Actualizo, funcionamiento normal HTU, 06/Marzo/2020, Camilo.',9,'DG 404','2020-03-05 00:00:00',NULL),(130,NULL,'405','DG405',15,9,9,3,3,'2020-03-02',1,NULL,'21/3/2020 12:46:24 pm',NULL,'camiloz','Estaba con la ubicación de la CSF pero realmente está en uso en HROB. 02-03-2020 Camilo.<div><br></div><div>Se verifica nuevamente y se establece ingreso en HROB 04-03-2020. Felipe&nbsp;</div>',9,'DG 405','2020-03-05 00:00:00',NULL),(131,NULL,'406','DG406',15,9,9,6,6,'2020-03-17',1,NULL,'21/3/2020 01:00:38 pm',NULL,'camiloz','Aparecía en HTU, realmente se encuentra en HROB, pendiente localizar el dispositivo de este registro.<div><br></div><div>Actualizo, el día 17/Marzo/2020 se llevó este equipo desde HROB (Palmira) a FHSJ (Buga).</div>',9,'DG 406','2020-03-05 00:00:00',NULL),(132,NULL,'407','180407',15,9,9,6,6,'2018-06-08',1,NULL,'18/2/2020 10:20:51 pm',NULL,'felipem','FUNDACION HOSPITAL SANJOSE, Marcado',9,'DG 407','2020-03-05 00:00:00',NULL),(133,NULL,'451','B97K125',4,0,0,0,0,NULL,0,NULL,'4/3/2020 09:52:53 am',NULL,'camiloz','ZOLL , PD 1200 ,  ,',0,'DES 451','2020-03-05 00:00:00',NULL),(134,NULL,'452','B97K12587',4,13,21,7,7,'2020-07-07',3,NULL,'7/7/2020 07:28:15 pm',NULL,'camiloz','En reparaciòn 07/Julio/2020.',21,'DES 452','2020-03-05 00:00:00','2020-07-07 19:26:21'),(135,NULL,'453','B97K12574',4,13,21,6,6,'2020-02-26',1,NULL,'26/2/2020 09:08:18 pm',NULL,'camiloz','ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE',21,'DES 453','2020-03-05 00:00:00',NULL),(136,NULL,'454','98278',4,4,27,3,3,'2020-03-04',1,NULL,'4/3/2020 10:06:47 am',NULL,'camiloz','Funciona normalmente en el HROB',27,'DES 454','2020-03-05 00:00:00',NULL),(137,NULL,'455','12129094',4,0,0,0,0,NULL,0,NULL,'4/3/2020 09:53:13 am',NULL,'camiloz','ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER',0,'DES 455','2020-03-05 00:00:00',NULL),(138,NULL,'456','12127290',4,4,19,2,2,'2020-02-26',1,NULL,'11/5/2020 09:04:01 am',NULL,'felipem','ARTEMA , IP 22 , CALI , HMC',19,'DES 456','2020-03-05 00:00:00',NULL),(139,NULL,'457','05126132',4,23,35,7,7,'2020-07-07',3,NULL,'7/7/2020 07:23:12 pm',NULL,'camiloz','En reparaciòn 07/Julio/2020 Camilo.',35,'DES 457','2020-03-05 00:00:00','2020-07-07 19:18:13'),(140,NULL,'458','90427',4,4,27,4,4,'2020-03-06',1,NULL,'6/3/2020 09:20:09 am',NULL,'camiloz','Actualizo, funcionamiento normal HTU, 06-Marzo-2020',27,'DES 458','2020-03-05 00:00:00',NULL),(141,NULL,'601','A7V87A',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:52:28 am',NULL,'camiloz','COMPAQ ,  , COMPAQ ,',0,'PC 601','2020-03-05 00:00:00',NULL),(142,NULL,'602','HN3F91QC100398',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:52:47 am',NULL,'camiloz','SAMSUNG ,  , INTEGRADO ,',0,'PC 602','2020-03-05 00:00:00',NULL),(143,NULL,'603','U082MA00',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:53:22 am',NULL,'camiloz','DELL ,  , INTEGRADO ,',0,'PC 603','2020-03-05 00:00:00',NULL),(144,NULL,'604','CLON',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:53:39 am',NULL,'camiloz','INTEL ,  , JANUS ,',0,'PC 604','2020-03-05 00:00:00',NULL),(145,NULL,'606','CS02589545',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:53:50 am',NULL,'camiloz','LENOVO ,  , INTEGRADO ,',0,'PC 606','2020-03-05 00:00:00',NULL),(146,NULL,'607','VS81211172',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:54:17 am',NULL,'camiloz','LENOVO ,  , INTEGRADO ,',0,'PC 607','2020-03-05 00:00:00',NULL),(147,NULL,'608','6MC2190SSX',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:25:58 pm',NULL,'camiloz','LENOVO ,  , INTEGRADO ,',0,'PC 608','2020-03-05 00:00:00',NULL),(148,NULL,'609','3CR2221DW9',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:26:15 pm',NULL,'camiloz','COMPAQ ,  , COMPAQ ,',0,'PC 609','2020-03-05 00:00:00',NULL),(149,NULL,'610','VS81225546',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:26:24 pm',NULL,'camiloz','LENOVO ,  , INTEGRADO ,',0,'PC 610','2020-03-05 00:00:00',NULL),(150,NULL,'611','5CB40840JJ',11,0,0,0,0,NULL,0,NULL,'29/2/2020 10:58:27 am',NULL,'camiloz','HP ,  , INTEGRADO ,',0,'PC 611','2020-03-05 00:00:00',NULL),(151,NULL,'612','HYXS91LD200746',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:26:32 pm',NULL,'camiloz','SAMSUNG ,  , INTEGRADO ,',0,'PC 612','2020-03-05 00:00:00',NULL),(152,NULL,'636','5CM31902QS',11,0,0,0,0,NULL,0,NULL,'15/10/2020 12:16:59 pm',NULL,'superadmin','HP ,  , INTEGRADO ,',0,'PC 636','2020-03-05 00:00:00',NULL),(153,NULL,'614','12052393514',11,0,0,0,0,NULL,0,NULL,'4/3/2020 10:12:06 am',NULL,'camiloz','LG ,  , JONUS ,',0,'PC 614','2020-03-05 00:00:00',NULL),(154,NULL,'615','335255',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:28:40 pm',NULL,'camiloz','COMPAQ ,  , AOPEN ,',0,'PC 615','2020-03-05 00:00:00',NULL),(155,NULL,'617','5CD440256Q',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:28:58 pm',NULL,'camiloz','HP ,  , HP ,',0,'PC 617','2020-03-05 00:00:00',NULL),(156,NULL,'618','CS01429411',11,10,11,3,3,'2020-03-04',1,NULL,'4/3/2020 10:18:49 am',NULL,'camiloz','LENOVO, TODO EN UNO',11,'PC 618','2020-03-05 00:00:00',NULL),(157,NULL,'619','PF0N6RLY',11,0,0,0,0,NULL,0,NULL,'29/2/2020 11:30:45 pm',NULL,'camiloz','LENOVO ,  , NA ,',0,'PC 619','2020-03-05 00:00:00',NULL),(158,'bcdf768ef42798802.jpg','620','CS02593439',11,10,11,1,1,'2019-12-10',1,NULL,'11/3/2020 01:42:49 pm',NULL,'camiloz','LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div><div>02-21-2020 se traslada a hidc</div>',11,'PC 620','2020-03-05 00:00:00',NULL),(159,NULL,'701','10-10-10',7,14,22,6,6,'2020-02-26',1,NULL,'17/3/2020 04:50:56 pm',NULL,'camiloz','PERFECT 10 , PERFECT 10&nbsp;',22,'BT 701','2020-03-05 00:00:00',NULL),(160,NULL,'702','201409026161',7,0,0,0,0,NULL,0,NULL,'29/2/2020 11:15:59 pm',NULL,'camiloz','SPORT FITNESS , JS-5000B-1 ,  ,',0,'BT 702','2020-03-05 00:00:00',NULL),(161,NULL,'704','SN: 270765 JC',7,0,0,0,0,NULL,0,NULL,'29/2/2020 11:16:10 pm',NULL,'camiloz',',  ,  ,',0,'BT 704','2020-03-05 00:00:00',NULL),(162,NULL,'705','201312044116',7,12,18,3,3,'2020-03-04',1,NULL,'4/3/2020 09:48:58 am',NULL,'camiloz','SPORT FITNESS , JS-5000B-1',18,'BT 705','2020-03-05 00:00:00',NULL),(163,NULL,'706','201409026192',7,12,18,2,2,'2020-02-26',1,NULL,'2/3/2020 03:48:35 pm',NULL,'camiloz','SPORT FITNESS , JS-5000B-1&nbsp;',18,'BT 706','2020-03-05 00:00:00',NULL),(164,NULL,'708','201509043123',7,0,0,0,0,NULL,0,NULL,'29/2/2020 11:16:35 pm',NULL,'camiloz','SPORT FITNESS ,  ,  ,',0,'BT 708','2020-03-05 00:00:00',NULL),(165,NULL,'710','201509043152',7,12,18,1,1,'2020-02-29',1,NULL,'29/2/2020 11:16:50 pm',NULL,'camiloz','SPORT FITNESS , JS-5000B-1 , HIDC ,',18,'BT 710','2020-03-05 00:00:00',NULL),(166,NULL,'751','20140625/BTT/01068',6,1,10,5,5,'2020-03-06',1,NULL,'15/10/2020 12:49:23 pm',NULL,'superadmin','Actualizo, funciona normalmente en CSF 06/Marzo/2020 Camilo.&nbsp;',10,'STR 751','2020-03-05 00:00:00',NULL),(167,NULL,'752','20140625/BTT/01107',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:34:44 pm',NULL,'camiloz','DMS 300-BTT01 , 20140625/BTR/01107 ,  ,',0,'STR 752','2020-03-05 00:00:00',NULL),(168,NULL,'753','20160725/BTT/01304',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:34:56 pm',NULL,'camiloz','DMS 300-BTT01 , 20160725/BTR/01304 ,  ,',0,'STR 753','2020-03-05 00:00:00',NULL),(169,NULL,'754','20160104/BTT/01253',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:35:03 pm',NULL,'camiloz','DMS 300-BTT01 , 20160104/BTR/01253 ,  ,',0,'STR 754','2020-03-05 00:00:00',NULL),(170,NULL,'755','20141201/BTT/01123',6,1,10,2,2,'2020-02-26',1,NULL,'2/3/2020 03:54:16 pm',NULL,'camiloz','DMS 300-BTT01 , 20141201/BTR/01123',10,'STR 755','2020-03-05 00:00:00',NULL),(171,NULL,'756','20160725/BTT/01307',6,1,10,9,9,'2019-12-10',1,NULL,'29/2/2020 09:37:01 pm',NULL,'camiloz','DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>',10,'STR 756','2020-03-05 00:00:00',NULL),(172,NULL,'757','20150129/BTT/01136',6,1,10,1,1,'2020-02-29',1,NULL,'29/2/2020 09:33:32 pm',NULL,'camiloz','DMS 300-BTT01 , 20150129/BTR/01136 ,&nbsp;',10,'STR 757','2020-03-05 00:00:00',NULL),(173,NULL,'758','20160104/BTR/01253',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:35:25 pm',NULL,'camiloz','DMS 300-BTT01 ,  ,  ,',0,'STR 758','2020-03-05 00:00:00',NULL),(174,NULL,'759','4712AU3681E',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:35:41 pm',NULL,'camiloz','QRSCardUSB , NA ,  ,',0,'STR 759','2020-03-05 00:00:00',NULL),(175,NULL,'760','SN BTR01-343',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:35:51 pm',NULL,'camiloz','DMS 300-BTT01 ,  ,  ,',0,'STR 760','2020-03-05 00:00:00',NULL),(176,NULL,'761','SM 311SN: D111E0004',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:36:01 pm',NULL,'camiloz','DMS300-BTT03 ,  ,  ,',0,'STR 761','2020-03-05 00:00:00',NULL),(177,NULL,'762','SN: 20140721/BTT02/00290',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:36:08 pm',NULL,'camiloz','DMS300-BTT02 ,  ,  ,',0,'STR 762','2020-03-05 00:00:00',NULL),(178,NULL,'763','20160725/BTT/01299',6,0,0,0,0,NULL,0,NULL,'29/2/2020 09:36:17 pm',NULL,'camiloz','DMS 300-BTT01 , 20160725/BTR/01299 ,  ,',0,'STR 763','2020-03-05 00:00:00',NULL),(179,NULL,'901','SM 10',0,0,0,0,0,NULL,0,NULL,'7/5/2020 04:09:27 pm',NULL,'felipem','KINGSTON , 8 GB , BUGA , HSJ',0,'HOL 901','2020-03-05 00:00:00',NULL),(180,NULL,'902','SM 11',0,0,0,0,0,NULL,0,NULL,'7/5/2020 04:09:33 pm',NULL,'felipem','KINGSTON , 8 GB , BUGA , HSJ',0,'HOL 902','2020-03-05 00:00:00',NULL),(181,NULL,'903','SM 903',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 8 GB , CALI , ADMIN.',NULL,'HOL 903','2020-03-05 00:00:00',NULL),(182,NULL,'904','SM 13',0,0,0,0,0,NULL,0,NULL,'8/5/2020 08:32:11 am',NULL,'felipem','KINGSTON , 8 GB , TULUA , HTU',0,'HOL 904','2020-03-05 00:00:00',NULL),(183,NULL,'905','SM 14',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:09:07 pm',NULL,'superadmin','KINGSTON , 8 GB , CASA , CASA',42,'USB 905','2020-03-05 00:00:00',NULL),(184,NULL,'906','SM 15',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:10:25 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA , TECNOLOGIA',42,'USB 906','2020-03-05 00:00:00',NULL),(185,NULL,'907','SM 16',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:12:03 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 907','2020-03-05 00:00:00',NULL),(186,NULL,'908','SM 17',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:12:30 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 908','2020-03-05 00:00:00',NULL),(187,NULL,'909','SM 18',16,25,42,6,6,'2020-10-15',3,NULL,'15/10/2020 01:13:04 pm',NULL,'superadmin','KINGSTON , 16 GB , BUGA , HSJ',42,'USB 909','2020-03-05 00:00:00',NULL),(188,NULL,'910','SM 19',16,25,42,6,6,'2020-10-15',3,NULL,'15/10/2020 01:14:31 pm',NULL,'superadmin','KINGSTON , 16 GB , BUGA , HSJ',42,'USB 910','2020-03-05 00:00:00',NULL),(189,NULL,'911','SM 20',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:15:03 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 911','2020-03-05 00:00:00',NULL),(190,NULL,'912','SM 21',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:15:28 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 912','2020-03-05 00:00:00',NULL),(191,NULL,'913','SM 22',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:15:59 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 913','2020-03-05 00:00:00',NULL),(192,NULL,'914','SM 23',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:08:25 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 914','2020-03-05 00:00:00',NULL),(193,NULL,'915','SM24',16,25,42,7,7,'2020-10-15',3,NULL,'15/10/2020 01:07:42 pm',NULL,'superadmin','KINGSTON , 16 GB , CASA ,',42,'USB 915','2020-03-05 00:00:00',NULL),(194,NULL,'801','VND3J28585',16,25,42,5,5,'2020-10-15',3,NULL,'15/10/2020 01:06:39 pm',NULL,'superadmin','IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO',42,'USB 801','2020-03-05 00:00:00',NULL),(195,NULL,'802','CN26RBK063',16,25,42,3,3,NULL,3,NULL,'15/10/2020 01:05:57 pm',NULL,'superadmin','OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO',42,'USB 802','2020-03-05 00:00:00',NULL),(196,NULL,'803','CN22SBTFP',16,25,42,3,3,'2020-10-15',3,NULL,'15/10/2020 01:05:09 pm',NULL,'superadmin','OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO',42,'USB 803','2020-03-05 00:00:00',NULL),(197,NULL,'804','SE8Y011865',16,25,42,2,2,'2020-10-15',3,NULL,'15/10/2020 01:04:29 pm',NULL,'superadmin','EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO',42,'USB 804','2020-03-05 00:00:00',NULL),(198,NULL,'805','SE8Y018164',16,25,42,3,3,'2020-10-15',1,NULL,'15/10/2020 01:03:39 pm',NULL,'superadmin','Sin verificacion<div><br></div><div><br></div>',42,'USB 805','2020-03-05 00:00:00',NULL),(199,'d7dffdba6db4acaa5.jpg','134','3A-11289',1,1,1,4,4,'2019-12-05',1,'5/12/2019 10:59:48 pm','8/10/2020 02:46:19 pm','felipem','superadmin','Estuche, Cable<span style=\"font-size: 0.857em;\">, China</span><span style=\"font-size: 0.857em;\">&nbsp;Noviembre 2019, Actualizo</span><span style=\"font-size: 0.857em;\">, funciona normalmente, HTU 06/Marzo/2020, Camilo.</span>',1,'HOLTER 134','2019-12-05 00:00:00',NULL),(200,'0517ef6974ab1463b.jpg','135','3A-11288',1,1,1,5,5,'2019-12-11',1,'5/12/2019 11:01:16 pm','27/12/2019 11:15:52 am','felipem','felipem','Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>',1,'HOLTER 135','2019-12-05 00:00:00',NULL),(201,'c830bd855b5933e47.jpg','136','3A-11286',1,1,1,5,5,'2019-12-11',1,'5/12/2019 11:06:13 pm','27/12/2019 11:06:57 am','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',1,'HOLTER 136','2019-12-05 00:00:00',NULL),(202,'9cd28935c6a69d0a3.jpg','137','3A-11292',1,1,1,4,4,'2019-12-05',1,'5/12/2019 11:07:54 pm','10/12/2019 06:44:08 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL,'HOLTER 137','2019-12-05 00:00:00',NULL),(203,'ec24083a0b7fcd5a2.jpg','138','3A-11291',1,1,1,4,4,'2019-12-09',1,'5/12/2019 11:08:49 pm','10/12/2019 06:41:34 pm','felipem','felipem','<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>',NULL,'HOLTER 138','2019-12-05 00:00:00',NULL),(204,'caeae04f26da4d82b.jpg','139','3A-11290',1,1,1,5,5,'2019-12-05',1,'5/12/2019 11:09:40 pm','6/3/2020 03:54:04 pm','felipem','camiloz','<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\"><br></span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo.</span></div>',1,'HOLTER 139','2019-12-05 00:00:00',NULL),(205,NULL,'PRUE','123456',10,7,6,7,7,'2019-12-27',2,'27/12/2019 09:54:45 am',NULL,'bodega.felipe',NULL,'<br>',6,'CAB PRUE','2019-12-27 00:00:00',NULL),(206,NULL,'590','19120200001',2,2,12,7,7,'2019-12-27',1,'27/12/2019 01:58:38 pm','27/12/2019 02:20:23 pm','felipem','felipem','<br>',12,'MAPA 590','2019-12-27 00:00:00',NULL),(207,NULL,'591','19120200002',2,2,12,7,7,'2019-12-27',1,'27/12/2019 02:01:17 pm',NULL,'felipem',NULL,'Noviembre 2019 China',12,'MAPA 591','2019-12-27 00:00:00',NULL),(208,NULL,'592','19120200010',2,2,2,4,4,'2019-12-27',1,NULL,'8/10/2020 02:58:54 pm',NULL,'superadmin','<br>',2,'MAPA 592','2020-03-05 00:00:00',NULL),(209,NULL,'593','19120200012',2,2,2,4,4,'2020-03-06',1,NULL,'6/3/2020 02:16:38 pm',NULL,'camiloz','Actualizo, el equipo se encontraba en bodega, ahora funciona normalmente en HTU, 06/Marzo/2020, Camilo.',2,'MAPA 593','2020-03-05 00:00:00',NULL),(210,NULL,'594','19120200022',2,2,2,7,NULL,'0000-00-00',1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 594','2020-03-05 00:00:00',NULL),(211,NULL,'595','19120200023',2,2,2,7,NULL,'0000-00-00',1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 595','2020-03-05 00:00:00',NULL),(212,NULL,'596','19120200027',2,2,2,7,NULL,'0000-00-00',1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 596','2020-03-05 00:00:00',NULL),(213,NULL,'597','19120200028',2,2,2,7,NULL,'0000-00-00',1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 597','2020-03-05 00:00:00',NULL),(214,NULL,'598','19120200029',2,2,2,7,NULL,'0000-00-00',1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 598','2020-03-05 00:00:00',NULL),(215,NULL,'599','19120200030',2,2,2,7,NULL,'0000-00-00',1,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA 599','2020-03-05 00:00:00',NULL),(216,NULL,'140','3A-11294',1,1,1,9,9,'2020-02-04',1,'4/2/2020 11:01:33 am',NULL,'felipem',NULL,'China 5 de 15 enero 2020',1,'HOLTER 140','2020-02-04 00:00:00',NULL),(217,NULL,'141','3A-11300',1,1,1,9,9,'2020-02-04',1,'4/2/2020 11:02:23 am','4/2/2020 11:02:39 am','felipem','felipem','<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>',1,'HOLTER 141','2020-02-04 00:00:00',NULL),(218,NULL,'142','3A-11301',1,1,1,9,9,'2020-02-04',1,'4/2/2020 11:03:28 am',NULL,'felipem',NULL,'<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>',1,'HOLTER 142','2020-02-04 00:00:00',NULL),(219,NULL,'143','3A-11302',1,1,1,9,9,'2020-02-04',1,'4/2/2020 11:04:38 am',NULL,'felipem',NULL,'<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>',1,'HOLTER 143','2020-02-04 00:00:00',NULL),(220,NULL,'144','3A-11304',1,1,1,9,9,'2020-02-04',1,'4/2/2020 11:08:00 am',NULL,'felipem',NULL,'<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>',1,'HOLTER 144','2020-02-04 00:00:00',NULL),(221,NULL,'408','DG408',15,9,9,4,4,'2020-02-18',1,'18/2/2020 10:21:55 pm','6/3/2020 10:10:04 am','felipem','camiloz','Actualizo, Funciona normalmente en HTU, 06/Marzo/2020, Camilo.',9,'DG 408','2020-02-18 00:00:00',NULL),(222,NULL,'409','DG409',15,9,9,6,6,'2016-02-08',3,'18/2/2020 10:23:46 pm',NULL,'felipem',NULL,'No funciona',9,'DG 409','2020-02-18 00:00:00',NULL),(223,NULL,'622','MP1FZWXE',11,10,15,11,11,'2020-02-18',1,'19/2/2020 11:54:09 am',NULL,'felipem',NULL,'<br>',15,'PC 622','2020-02-19 00:00:00',NULL),(224,NULL,'410','DG410',15,9,9,5,5,'2020-02-21',1,'20/2/2020 05:41:52 pm','4/3/2020 11:07:12 am','felipem','felipem','Equipo comprado de segunda enviado 20-02-2020 por flm para csf.<div>Equipo reemplazado por el DG 414</div>',9,'DG 410','2020-02-20 00:00:00',NULL),(225,NULL,'413','DG413',15,9,9,5,5,'2020-02-26',1,'26/2/2020 05:13:35 pm','6/3/2020 10:04:17 am','camiloz','camiloz','Funcionamiento normal',9,'DG 413','2020-02-26 00:00:00',NULL),(226,NULL,'411','DG411',15,9,9,1,1,'2020-02-29',1,'29/2/2020 10:15:52 am',NULL,'camiloz',NULL,'Tenia el codigo DG 407 se le otorga el codigo DG 411, Camilo.<div><br></div>',9,'DG 411','2020-02-29 00:00:00',NULL),(227,NULL,'412','DG412',15,9,9,2,2,'2020-02-29',1,'29/2/2020 10:32:18 am','29/2/2020 10:35:41 am','camiloz','camiloz','PENDIENTE POR CAMBIAR CODIGO DEL 401 AL 412',9,'DG 412','2020-02-29 00:00:00',NULL),(228,NULL,'414','DG414',15,9,9,5,5,'2020-03-04',3,'4/3/2020 11:09:00 am','4/3/2020 11:14:57 am','felipem','felipem','El equipo no prende, completamente dañado',9,'DG 414','2020-03-04 00:00:00',NULL),(229,NULL,'1001','X5NQ011045',17,17,28,1,1,'2020-03-05',1,'5/3/2020 11:16:49 am',NULL,'felipem',NULL,'Ingreso nueva 05-03-1989',28,'SM 1001','2020-03-05 00:00:00',NULL),(230,NULL,'703','201509043107',7,12,18,4,4,'2020-03-06',1,'6/3/2020 08:57:47 am','6/3/2020 09:03:00 am','camiloz','camiloz','Se trajo desde Palmira por Luis García',18,'BT 703','2020-03-06 00:00:00',NULL),(231,NULL,'605','303NDWE33345',11,18,29,4,4,'2020-03-06',1,'6/3/2020 11:23:42 am',NULL,'camiloz',NULL,'<br>',29,'PC 605','2020-03-06 00:00:00',NULL),(232,NULL,'546','SM546',18,19,30,4,4,'2020-03-06',1,'6/3/2020 12:15:29 pm','6/3/2020 12:17:05 pm','camiloz','camiloz','Funcionamiento normal HTU, 06/Marzo/2020, Camilo.',30,'SM 546','2020-03-06 00:00:00',NULL),(233,NULL,'917','SM 917',20,20,31,4,4,'2020-03-06',1,'6/3/2020 12:40:16 pm','12/5/2020 12:35:12 am','camiloz','felipem','<br>',31,'SM 917','2020-03-06 00:00:00',NULL),(234,NULL,'508','1078535',19,21,32,4,4,'2020-03-06',1,'6/3/2020 12:52:36 pm','6/3/2020 12:54:30 pm','camiloz','camiloz','Actualizado, Funcionamiento normal, 06/Marzo/2020, Camilo.',32,'SM 508','2020-03-06 00:00:00',NULL),(235,NULL,'916','SM 916',20,20,31,2,2,'2020-02-26',1,'21/3/2020 01:15:27 pm','12/5/2020 12:33:51 am','camiloz','felipem','Actualizo, el dispositivo funciona normalmente en HMC, 21/03/2020, Camilo.',31,'SM 916','2020-03-21 00:00:00',NULL),(236,NULL,'273','03CX6P',13,3,33,1,1,'2020-05-06',1,'6/5/2020 06:19:31 pm','6/5/2020 06:21:55 pm','felipem','felipem','<br>',33,'TR 273','2020-05-06 00:00:00',NULL),(237,NULL,'274','US99N01847',13,3,16,1,1,'2020-05-07',1,'7/5/2020 11:57:32 am','7/5/2020 11:58:43 am','felipem','felipem','<br>',16,'TR 274','2020-05-07 00:00:00',NULL),(238,NULL,'1002','IHTD56VD1',21,22,34,1,1,'2020-05-08',1,'8/5/2020 08:54:23 am',NULL,'felipem',NULL,'SJUG7749AA',34,'CEL 1002','2020-05-08 00:00:00',NULL),(239,NULL,'629','CS02781268',11,10,11,4,4,'2020-05-22',1,'22/5/2020 01:27:33 pm',NULL,'felipem',NULL,'<br>',11,'PC 629','2020-05-22 00:00:00',NULL),(240,NULL,'626','MP12PNNW',11,10,36,4,4,'2020-05-22',1,'22/5/2020 01:55:36 pm','26/5/2020 09:03:41 pm','felipem','felipem','Utilizado para ECG',36,'PC 626','2020-05-22 00:00:00',NULL),(241,NULL,'635','MP189C54',11,10,37,4,4,NULL,1,'22/5/2020 02:53:45 pm',NULL,'felipem',NULL,'<br>',37,'PC 635','2020-05-22 00:00:00',NULL),(242,NULL,'628','5CM3340JQV',11,24,38,4,4,'2020-05-22',1,'22/5/2020 03:15:52 pm','24/6/2020 08:38:03 pm','felipem','felipem','<div>Función: HOLTER Y MAPA</div><div><div><br></div></div>',38,'PC 628','2020-05-22 00:00:00',NULL),(243,NULL,'713','L1317121',18,19,30,9,9,'2019-11-01',1,NULL,'15/10/2020 12:20:49 pm','lgarcia','superadmin','EQUIPO PROCEDENTE DE HFPS',30,'SM 713','2020-09-28 16:18:07','2020-09-28 16:10:48'),(244,NULL,'351','00267',10,1,39,7,7,'2020-10-01',1,NULL,'1/10/2020 04:44:36 pm','superadmin','superadmin','<br>',39,'CAB 351','2020-10-01 16:40:44',NULL),(245,NULL,'352','00272',10,1,39,7,7,'2020-10-01',1,NULL,NULL,'superadmin',NULL,'<br>',39,'CAB 352','2020-10-01 16:46:00',NULL),(246,NULL,'550','17030100017',2,2,2,4,4,'2020-10-08',1,NULL,NULL,'superadmin',NULL,'<br>',2,'MAPA 550','2020-10-08 15:08:34',NULL),(247,NULL,'613','MXL3162BRT',11,24,40,4,4,'2020-09-22',1,NULL,NULL,'superadmin',NULL,'<br>',40,'PC 613','2020-10-15 12:25:44',NULL),(248,NULL,'275','US30432955',13,3,45,4,4,'2020-10-15',1,NULL,NULL,'superadmin',NULL,'Sonda trasesofagica',45,'TR 275','2020-10-15 16:45:56',NULL),(249,NULL,'276','US99N02863',13,3,16,4,4,'2020-10-15',1,NULL,NULL,'superadmin',NULL,'<br>',16,'TR 276','2020-10-15 16:47:56',NULL),(250,NULL,'711','201606060064',7,12,18,4,4,'2020-10-15',1,NULL,NULL,'superadmin',NULL,'<br>',18,'BT 711','2020-10-15 17:07:24',NULL),(251,NULL,'815','UKTY002906',17,17,46,4,4,'2020-10-15',1,NULL,NULL,'superadmin',NULL,'<br>',46,'SM 815','2020-10-15 17:41:09',NULL),(252,NULL,'816','X5E8006892',17,17,47,4,4,'2020-10-15',1,NULL,NULL,'superadmin',NULL,'<br>',47,'SM 816','2020-10-15 17:45:41',NULL),(253,NULL,'817','SE8Y017621',17,17,41,4,4,'2020-10-15',1,NULL,'15/10/2020 05:49:24 pm','superadmin','superadmin','<br>',41,'SM 817','2020-10-15 17:48:07',NULL),(254,NULL,'818','SE8Y017614',17,17,41,4,4,'2020-10-15',1,NULL,'15/10/2020 05:51:49 pm','superadmin','superadmin','<br>',41,'SM 818','2020-10-15 17:50:16',NULL),(255,NULL,'145','145',23,26,44,7,7,NULL,4,NULL,'15/10/2020 06:00:57 pm','superadmin','superadmin','HOLTER',44,'LIBRE 145','2020-10-15 17:55:00',NULL),(256,NULL,'146','146',23,26,44,7,7,NULL,4,NULL,'15/10/2020 06:00:08 pm','superadmin','superadmin','HOLTER',44,'LIBRE 146','2020-10-15 17:56:35',NULL),(257,NULL,'147','147',23,26,44,7,7,NULL,4,NULL,'15/10/2020 06:00:21 pm','superadmin','superadmin','HOLTER',44,'LIBRE 147','2020-10-15 17:57:25',NULL);
/*!40000 ALTER TABLE `dispositivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivos_backup_20191127095023`
--

DROP TABLE IF EXISTS `dispositivos_backup_20191127095023`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivos_backup_20191127095023` (
  `id_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `ubicacion` int(10) unsigned NOT NULL,
  `ubicacion_abre` int(10) unsigned DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) unsigned NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_dispo`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  UNIQUE KEY `serial_unique` (`serial`),
  KEY `tipo_dispositivo` (`tipo_dispositivo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `ubicacion` (`ubicacion`),
  KEY `estado` (`estado`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivos_backup_20191127095023`
--

LOCK TABLES `dispositivos_backup_20191127095023` WRITE;
/*!40000 ALTER TABLE `dispositivos_backup_20191127095023` DISABLE KEYS */;
INSERT INTO `dispositivos_backup_20191127095023` VALUES (1,'a0c7a87f98ff4c423.jpg','HOLTER101','3A-7125',1,1,1,6,6,'2018-11-20',1,'27/11/2019 09:37:37 am',NULL,'felipem',NULL),(2,'8710a41018a7c4e42.jpg','HOLTER102','3A-11605',1,1,1,7,7,'2019-11-27',1,'27/11/2019 09:40:39 am',NULL,'felipem',NULL);
/*!40000 ALTER TABLE `dispositivos_backup_20191127095023` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivos_backup_20191127101832`
--

DROP TABLE IF EXISTS `dispositivos_backup_20191127101832`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivos_backup_20191127101832` (
  `id_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `ubicacion` int(10) unsigned NOT NULL,
  `ubicacion_abre` int(10) unsigned DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) unsigned NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_dispo`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  UNIQUE KEY `serial_unique` (`serial`),
  KEY `tipo_dispositivo` (`tipo_dispositivo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `ubicacion` (`ubicacion`),
  KEY `estado` (`estado`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivos_backup_20191127101832`
--

LOCK TABLES `dispositivos_backup_20191127101832` WRITE;
/*!40000 ALTER TABLE `dispositivos_backup_20191127101832` DISABLE KEYS */;
INSERT INTO `dispositivos_backup_20191127101832` VALUES (1,'a0c7a87f98ff4c423.jpg','HOLTER101','3A-7125',1,1,1,6,6,'2018-11-20',1,'27/11/2019 09:37:37 am',NULL,'felipem',NULL),(2,'8710a41018a7c4e42.jpg','HOLTER102','3A-11605',1,1,1,7,7,'2019-11-27',1,'27/11/2019 09:40:39 am',NULL,'felipem',NULL),(3,'6630f469154d3320d.jpg','HOLTER 103','3A-8044',1,1,1,7,7,NULL,1,NULL,'27/11/2019 09:51:07 am',NULL,'felipem'),(4,NULL,'HOLTER 104','3A-11606',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(5,NULL,'HOLTER 105','3A-1039',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(6,NULL,'HOLTER 106','3A-11607',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(7,NULL,'HOLTER 107','3A-11608',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(8,NULL,'HOLTER 108','3A-2151',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(9,NULL,'HOLTER 109','3A-9421',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(10,NULL,'HOLTER 110','3A-9310',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(11,NULL,'HOLTER 111','3A-11609',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(12,NULL,'HOLTER 112','R1',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(13,NULL,'HOLTER 113','3A-9311',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(14,NULL,'HOLTER 114','3A-11815',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(15,NULL,'HOLTER 115','3A-9573',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(16,NULL,'HOLTER 116','3A-8940',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(17,NULL,'HOLTER 117','3A-9484',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(18,NULL,'HOLTER 118','3A-9486',1,1,1,4,NULL,NULL,1,NULL,NULL,NULL,NULL),(19,NULL,'HOLTER 119','3A-9572',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(20,NULL,'HOLTER 120','3A-9487',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(21,NULL,'HOLTER 121','3A-8946',1,1,1,4,NULL,NULL,1,NULL,NULL,NULL,NULL),(22,NULL,'HOLTER 122','3A-9485',1,1,1,4,NULL,NULL,1,NULL,NULL,NULL,NULL),(23,NULL,'HOLTER 123','3A-1993',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(24,NULL,'HOLTER 124','3A-10318',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(25,NULL,'HOLTER 125','3A-11816',1,1,1,4,NULL,NULL,1,NULL,NULL,NULL,NULL),(26,NULL,'HOLTER 126','3A-11818',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(27,NULL,'HOLTER 127','3A-12175',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(28,NULL,'HOLTER 128','3A-12176',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL),(29,NULL,'HOLTER 129','3A-11940',1,1,1,6,NULL,NULL,1,NULL,NULL,NULL,NULL),(30,NULL,'HOLTER 130','3A-12178',1,1,1,7,NULL,NULL,1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `dispositivos_backup_20191127101832` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivos_backup_20191203080334`
--

DROP TABLE IF EXISTS `dispositivos_backup_20191203080334`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivos_backup_20191203080334` (
  `id_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `ubicacion` int(10) unsigned NOT NULL,
  `ubicacion_abre` int(10) unsigned DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) unsigned NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_dispo`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  UNIQUE KEY `serial_unique` (`serial`),
  KEY `tipo_dispositivo` (`tipo_dispositivo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `ubicacion` (`ubicacion`),
  KEY `estado` (`estado`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivos_backup_20191203080334`
--

LOCK TABLES `dispositivos_backup_20191203080334` WRITE;
/*!40000 ALTER TABLE `dispositivos_backup_20191203080334` DISABLE KEYS */;
INSERT INTO `dispositivos_backup_20191203080334` VALUES (1,'a0c7a87f98ff4c423.jpg','HOLTER 101','3A-7125',1,1,1,6,6,'2018-11-20',1,'27/11/2019 09:37:37 am','27/11/2019 11:06:28 am','felipem','felipem',NULL),(2,'8710a41018a7c4e42.jpg','HOLTER 102','3A-11605',1,1,1,7,7,'2019-11-27',1,'27/11/2019 09:40:39 am','27/11/2019 11:06:42 am','felipem','felipem',NULL),(3,'6630f469154d3320d.jpg','HOLTER 103','3A-8044',1,1,1,7,7,NULL,1,NULL,'27/11/2019 09:51:07 am',NULL,'felipem',NULL),(4,'796ef58bbcaf815d5.jpg','HOLTER 104','3A-11606',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:20:47 am',NULL,'felipem',NULL),(5,'a1ca49f2da0d3858a.jpg','HOLTER 105','3A-1039',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:21:01 am',NULL,'felipem',NULL),(6,'fa31345b2dec3f75d.jpg','HOLTER 106','3A-11607',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:06 am',NULL,'felipem',NULL),(7,'62f8f3afd6062cf73.jpg','HOLTER 107','3A-11608',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:41 am',NULL,'felipem',NULL),(8,'16a714e4f58af1930.jpg','HOLTER 108','3A-2151',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:46 am',NULL,'felipem',NULL),(9,'fa647e5ab77632692.jpg','HOLTER 109','3A-9421',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:50 am',NULL,'felipem',NULL),(10,'476bc82bf3bed4616.jpg','HOLTER 110','3A-9310',1,1,1,6,6,'2018-11-20',1,NULL,'27/11/2019 10:46:21 am',NULL,'felipem',NULL),(11,'a456dd57854fcd34f.jpg','HOLTER 111','3A-11609',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:00 am',NULL,'felipem',NULL),(12,'e79c8397c6fa85698.jpg','HOLTER 112','3A-12178',1,1,1,6,6,NULL,1,NULL,'29/11/2019 01:14:34 am',NULL,'felipem',NULL),(13,'79766d196e19eb1db.jpg','HOLTER 113','3A-9311',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:11 am',NULL,'felipem',NULL),(14,'1c1558589faf656c3.jpg','HOLTER 114','3A-11815',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:27:18 am',NULL,'felipem',NULL),(15,'a47482f69e6bb1445.jpg','HOLTER 115','3A-9573',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:56 am',NULL,'felipem',NULL),(16,'fad32fe2f96f1b6f2.jpg','HOLTER 116','3A-8940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:01 am',NULL,'felipem',NULL),(17,'f6216dd5ac51a2f82.jpg','HOLTER 117','3A-9484',1,1,1,10,10,'2019-12-04',1,NULL,'3/12/2019 12:43:08 am',NULL,'felipem','Esruche<div>Cable viejo</div>'),(18,'6b9ff54089c40cbf3.jpg','HOLTER 118','3A-9486',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:12 am',NULL,'felipem',NULL),(19,'ad6939e30d7026eee.jpg','HOLTER 119','3A-9572',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:21 am',NULL,'felipem',NULL),(20,'636978098692e0616.jpg','HOLTER 120','3A-9487',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:23 am',NULL,'felipem',NULL),(21,'05353fef66bf34245.jpg','HOLTER 121','3A-8946',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:27 am',NULL,'felipem',NULL),(22,'753003a921f5f02ec.jpg','HOLTER 122','3A-9485',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:32 am',NULL,'felipem',NULL),(23,'5cc88d4b86424bb7d.jpg','HOLTER 123','3A-1993',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:37 am',NULL,'felipem',NULL),(24,'b89cb8d899578dd8b.jpg','HOLTER 124','3A-10318',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:41 am',NULL,'felipem',NULL),(25,'2b4b8d4d10dddbe8b.jpg','HOLTER 125','3A-11816',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:47 am',NULL,'felipem',NULL),(26,'06b68e149d1227503.jpg','HOLTER 126','3A-11818',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:52 am',NULL,'felipem',NULL),(27,'f4964486301b6afe2.jpg','HOLTER 127','3A-12175',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:58 am',NULL,'felipem',NULL),(28,'d179e9e2972b91703.jpg','HOLTER 128','3A-12176',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:29:03 am',NULL,'felipem',NULL),(29,'9e12d7e5ca84cd0f4.jpg','HOLTER 129','3A-11940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:29:09 am',NULL,'felipem',NULL),(57,NULL,'HOLTER130','3A-11295',1,1,1,9,9,'2019-11-29',1,'29/11/2019 04:45:35 pm',NULL,'felipem',NULL,NULL),(31,'3cc42f4af7a84575e.jpg','ECO 201','US97804887',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:28:42 am',NULL,'felipem',NULL),(32,'536dec1c5345e2b59.jpg','ECO 202','US97806979',3,3,3,6,6,NULL,1,NULL,'29/11/2019 01:11:49 am',NULL,'felipem',NULL),(33,'2edf9e5e9f0adf0b0.jpg','ECO 203','3728A01811',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:12:32 am',NULL,'felipem',NULL),(34,'b35b0af60ed58ef47.jpg','ECO 204','US50210876',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:13:01 am',NULL,'felipem',NULL),(35,'d6bb8df0f60c48ab0.jpg','ECO 205','3728A00382',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:19:52 am',NULL,'felipem',NULL),(36,'620450dbf61c4c19f.jpg','ECO 206','9708A09160',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:20:17 am',NULL,'felipem',NULL),(37,'41736a0baf4cb45e3.jpg','ECO 207','71674',3,6,5,7,7,NULL,1,NULL,'29/11/2019 01:24:01 am',NULL,'felipem',NULL),(38,'0eb9fdc48d520b1a8.jpg','ECO 208','001838VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:29 am',NULL,'felipem',NULL),(39,'f7c6ea863e2c80a47.jpg','ECO 209','055133VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:16 am',NULL,'felipem',NULL),(40,'c86621e1a393abd5e.jpg','ECO 210','3728A01073',3,3,3,4,4,NULL,1,NULL,'29/11/2019 01:20:39 am',NULL,'felipem',NULL),(41,'158d01f499364579d.jpg','ECO 211','USO0211434',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:06 am',NULL,'felipem',NULL),(42,'387a137ebf96710b6.jpg','ECO 212','US97807671',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:46 am',NULL,'felipem',NULL),(43,'5ed931c87e7ad07f3.jpg','ECO 213','9708A04535',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:34 am',NULL,'felipem',NULL),(44,'e624e9cfa97691728.jpg','ECO 214','3728A00160',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:26:23 am',NULL,'felipem',NULL),(45,'54d451022df8c30e6.jpg','ECO 215','US97703545',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:07 am',NULL,'felipem',NULL),(46,NULL,'ECO 216','US97808948',3,3,3,6,6,NULL,1,NULL,'29/11/2019 01:27:21 am',NULL,'felipem',NULL),(47,'630053d26916342ca.jpg','ECO 217','US97800318',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:57 am',NULL,'felipem',NULL),(48,'9972d0e70d8ca4ca6.jpg','ECO 218','3728A02302',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:47 am',NULL,'felipem',NULL),(49,'2e9dc0bf01f8da61f.jpg','ECO 219','US80210739',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:31 am',NULL,'felipem',NULL),(50,'6e5eaa081a347bc98.jpg','ECO 220','US97703205',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:18 am',NULL,'felipem',NULL),(51,'10b8abd62f6418d47.jpg','ECO 221','US97804106',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:05 am',NULL,'felipem',NULL),(52,'b6efed45a482c8c76.jpg','ECO 222','US97804535',3,3,3,7,7,NULL,1,NULL,'28/11/2019 10:37:14 am',NULL,'lgarcia',NULL),(53,'326d48038544d0081.jpg','ECO 223','US70421029',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:24:53 am',NULL,'felipem',NULL),(54,'2114d2a34043479d1.jpg','ECO 224','US97703258',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:26:47 am',NULL,'felipem',NULL),(55,'99c80bd69f2b7d401.jpg','ECO 225','USD0211891',3,3,3,4,4,NULL,1,NULL,'29/11/2019 01:24:41 am',NULL,'felipem',NULL),(56,'a4df3e5a8cd507c06.jpg','PC621','JCN0GR05Z985517',12,7,6,1,1,'2019-08-07',1,'27/11/2019 06:30:03 pm','27/11/2019 06:41:53 pm','vilmao','vilmao',NULL),(58,NULL,'HOLTER 131','3A-11287',1,1,1,9,9,'2019-11-29',1,'29/11/2019 05:06:08 pm',NULL,'felipem',NULL,NULL),(59,'982c34a6a71597fe5.jpg','HOLTER 132','3A-11293',1,1,1,7,7,'2019-11-29',1,'3/12/2019 12:38:32 am','3/12/2019 12:38:53 am','felipem','felipem','Con estuche y cable de paciente&nbsp;'),(60,'5c9c7e2c36eb50b7f.jpg','HOLTER 133','3A-11285',1,1,1,7,7,'2019-11-29',1,'3/12/2019 12:41:03 am',NULL,'felipem',NULL,'Estuche&nbsp;<div>Cable paciente&nbsp;</div>');
/*!40000 ALTER TABLE `dispositivos_backup_20191203080334` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivos_backup_20191205094300`
--

DROP TABLE IF EXISTS `dispositivos_backup_20191205094300`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivos_backup_20191205094300` (
  `id_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `ubicacion` int(10) unsigned NOT NULL,
  `ubicacion_abre` int(10) unsigned DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) unsigned NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_dispo`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  UNIQUE KEY `serial_unique` (`serial`),
  KEY `tipo_dispositivo` (`tipo_dispositivo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `ubicacion` (`ubicacion`),
  KEY `estado` (`estado`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivos_backup_20191205094300`
--

LOCK TABLES `dispositivos_backup_20191205094300` WRITE;
/*!40000 ALTER TABLE `dispositivos_backup_20191205094300` DISABLE KEYS */;
INSERT INTO `dispositivos_backup_20191205094300` VALUES (1,'a0c7a87f98ff4c423.jpg','HOLTER 101','3A-7125',1,1,1,6,6,'2018-11-20',1,'27/11/2019 09:37:37 am','27/11/2019 11:06:28 am','felipem','felipem',NULL),(2,'8710a41018a7c4e42.jpg','HOLTER 102','3A-11605',1,1,1,1,1,'2019-11-27',1,'27/11/2019 09:40:39 am','3/12/2019 08:18:57 am','felipem','felipem','<br>'),(3,'6630f469154d3320d.jpg','HOLTER 103','3333',1,1,1,7,7,'2019-12-03',3,NULL,'3/12/2019 08:14:22 am',NULL,'felipem','<br>'),(4,'796ef58bbcaf815d5.jpg','HOLTER 104','3A-11606',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:20:47 am',NULL,'felipem',NULL),(5,'a1ca49f2da0d3858a.jpg','HOLTER 105','3A-1039',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:21:01 am',NULL,'felipem',NULL),(6,'fa31345b2dec3f75d.jpg','HOLTER 106','3A-11607',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:06 am',NULL,'felipem',NULL),(7,'62f8f3afd6062cf73.jpg','HOLTER 107','3A-11608',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:41 am',NULL,'felipem',NULL),(8,'16a714e4f58af1930.jpg','HOLTER 108','3A-2151',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:46 am',NULL,'felipem',NULL),(9,'fa647e5ab77632692.jpg','HOLTER 109','3A-9421',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 11:14:08 am',NULL,'felipem','<br>'),(10,'476bc82bf3bed4616.jpg','HOLTER 110','3A-9310',1,1,1,6,6,'2018-11-20',1,NULL,'27/11/2019 10:46:21 am',NULL,'felipem',NULL),(11,'a456dd57854fcd34f.jpg','HOLTER 111','3A-11609',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:00 am',NULL,'felipem',NULL),(12,'e79c8397c6fa85698.jpg','HOLTER 112','3A-12178',1,1,1,6,6,NULL,1,NULL,'29/11/2019 01:14:34 am',NULL,'felipem',NULL),(13,'79766d196e19eb1db.jpg','HOLTER 113','3A-9311',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:11 am',NULL,'felipem',NULL),(14,'1c1558589faf656c3.jpg','HOLTER 114','3A-11815',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:27:18 am',NULL,'felipem',NULL),(15,'a47482f69e6bb1445.jpg','HOLTER 115','3A-9573',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:56 am',NULL,'felipem',NULL),(16,'fad32fe2f96f1b6f2.jpg','HOLTER 116','3A-8940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:01 am',NULL,'felipem',NULL),(17,'f6216dd5ac51a2f82.jpg','HOLTER 117','3A-9484',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 01:38:12 pm',NULL,'felipem','Esruche<div>Cable viejo</div>'),(18,'6b9ff54089c40cbf3.jpg','HOLTER 118','3A-9486',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:12 am',NULL,'felipem',NULL),(19,'ad6939e30d7026eee.jpg','HOLTER 119','3A-9572',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:21 am',NULL,'felipem',NULL),(20,'636978098692e0616.jpg','HOLTER 120','3A-9487',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:23 am',NULL,'felipem',NULL),(21,'05353fef66bf34245.jpg','HOLTER 121','3A-8946',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:27 am',NULL,'felipem',NULL),(22,'753003a921f5f02ec.jpg','HOLTER 122','3A-9485',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:32 am',NULL,'felipem',NULL),(23,'5cc88d4b86424bb7d.jpg','HOLTER 123','3A-1993',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 11:03:57 am',NULL,'felipem','Antes holter 112'),(24,'b89cb8d899578dd8b.jpg','HOLTER 124','3A-10318',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:41 am',NULL,'felipem',NULL),(25,'2b4b8d4d10dddbe8b.jpg','HOLTER 125','3A-11816',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:47 am',NULL,'felipem',NULL),(26,'06b68e149d1227503.jpg','HOLTER 126','3A-8044',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 10:42:01 am',NULL,'felipem','Antes 3A-11818'),(27,'f4964486301b6afe2.jpg','HOLTER 127','3A-12175',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:28:58 am',NULL,'felipem',NULL),(28,'d179e9e2972b91703.jpg','HOLTER 128','3A-12176',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:29:03 am',NULL,'felipem',NULL),(29,'9e12d7e5ca84cd0f4.jpg','HOLTER 129','3A-11940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:29:09 am',NULL,'felipem',NULL),(57,'c11ec5b51107d1e95.jpg','HOLTER130','3A-11295',1,1,1,9,9,'2019-11-29',1,'29/11/2019 04:45:35 pm','5/12/2019 09:39:43 am','felipem','felipem','<br>'),(31,'3cc42f4af7a84575e.jpg','ECO 201','US97804887',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:28:42 am',NULL,'felipem',NULL),(32,'536dec1c5345e2b59.jpg','ECO 202','US97806979',3,3,3,6,6,NULL,1,NULL,'29/11/2019 01:11:49 am',NULL,'felipem',NULL),(33,'2edf9e5e9f0adf0b0.jpg','ECO 203','3728A01811',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:12:32 am',NULL,'felipem',NULL),(34,'b35b0af60ed58ef47.jpg','ECO 204','US50210876',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:13:01 am',NULL,'felipem',NULL),(35,'d6bb8df0f60c48ab0.jpg','ECO 205','3728A00382',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:19:52 am',NULL,'felipem',NULL),(36,'620450dbf61c4c19f.jpg','ECO 206','9708A09160',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:04:44 am',NULL,'felipem','<br>'),(37,'41736a0baf4cb45e3.jpg','ECO 207','71674',3,6,5,7,7,NULL,1,NULL,'29/11/2019 01:24:01 am',NULL,'felipem',NULL),(38,'0eb9fdc48d520b1a8.jpg','ECO 208','001838VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:29 am',NULL,'felipem',NULL),(39,'f7c6ea863e2c80a47.jpg','ECO 209','055133VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:16 am',NULL,'felipem',NULL),(40,'c86621e1a393abd5e.jpg','ECO 210','3728A01073',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:03:57 am',NULL,'felipem','<br>'),(41,'158d01f499364579d.jpg','ECO 211','USO0211434',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:06 am',NULL,'felipem',NULL),(42,'387a137ebf96710b6.jpg','ECO 212','US97807671',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:46 am',NULL,'felipem',NULL),(43,'5ed931c87e7ad07f3.jpg','ECO 213','9708A04535',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:34 am',NULL,'felipem',NULL),(44,'e624e9cfa97691728.jpg','ECO 214','3728A00160',3,3,3,6,6,'2019-12-04',1,NULL,'4/12/2019 11:54:30 pm',NULL,'felipem','<br>'),(45,'54d451022df8c30e6.jpg','ECO 215','US97703545',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:07 am',NULL,'felipem',NULL),(46,'0b54a448e7a5a93aa.jpg','ECO 216','US97808948',3,3,3,6,6,NULL,1,NULL,'4/12/2019 11:56:52 pm',NULL,'felipem','<br>'),(47,'630053d26916342ca.jpg','ECO 217','US97800318',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:57 am',NULL,'felipem',NULL),(48,'9972d0e70d8ca4ca6.jpg','ECO 218','3728A02302',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:47 am',NULL,'felipem',NULL),(49,'2e9dc0bf01f8da61f.jpg','ECO 219','US80210739',3,3,3,1,1,'2019-12-03',1,NULL,'3/12/2019 11:30:51 am',NULL,'felipem','<br>'),(50,'6e5eaa081a347bc98.jpg','ECO 220','US97703205',3,3,3,1,1,'2019-12-03',1,NULL,'3/12/2019 11:27:58 am',NULL,'felipem','<br>'),(51,'10b8abd62f6418d47.jpg','ECO 221','US97804106',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:05 am',NULL,'felipem',NULL),(52,'b6efed45a482c8c76.jpg','ECO 222','US97804535',3,3,3,7,7,NULL,1,NULL,'28/11/2019 10:37:14 am',NULL,'lgarcia',NULL),(53,'326d48038544d0081.jpg','ECO 223','US70421029',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:24:53 am',NULL,'felipem',NULL),(54,'2114d2a34043479d1.jpg','ECO 224','US97703258',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:26:47 am',NULL,'felipem',NULL),(55,'99c80bd69f2b7d401.jpg','ECO 225','USD0211891',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:06:31 am',NULL,'felipem','Verificar código puesto en el equipo&nbsp;'),(56,'a4df3e5a8cd507c06.jpg','PC621','JCN0GR05Z985517',12,7,6,1,1,'2019-08-07',1,'27/11/2019 06:30:03 pm','27/11/2019 06:41:53 pm','vilmao','vilmao',NULL),(58,'2fb3b0f1c5f8919a5.jpg','HOLTER 131','3A-11287',1,1,1,9,9,'2019-11-29',1,'29/11/2019 05:06:08 pm','5/12/2019 09:40:19 am','felipem','felipem','<br>'),(59,'982c34a6a71597fe5.jpg','HOLTER 132','3A-11293',1,1,1,1,1,'2019-12-03',1,'3/12/2019 12:38:32 am','3/12/2019 01:46:53 pm','felipem','felipem','Con estuche y cable de paciente&nbsp;'),(60,'5c9c7e2c36eb50b7f.jpg','HOLTER 133','3A-11285',1,1,1,1,1,'2019-11-29',1,'3/12/2019 12:41:03 am','3/12/2019 01:43:50 pm','felipem','felipem','Estuche&nbsp;<div>Cable paciente&nbsp;</div>'),(61,'f19510876e2eb19cd.jpeg','MAPA 551','IP309100004',2,2,7,3,3,NULL,3,NULL,'5/12/2019 12:01:35 am',NULL,'felipem','Equipo dañado&nbsp;'),(62,'1d40088c027c6481a.jpeg','MAPA 552','00087513',2,8,8,6,6,'2019-12-04',1,NULL,'4/12/2019 11:59:19 pm',NULL,'felipem','<br>'),(63,NULL,'MAPA 553','140206005',2,2,7,3,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(64,NULL,'MAPA 554','IP1602200009',2,2,2,7,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(65,NULL,'MAPA 555','IP1602200106',2,2,2,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(66,NULL,'MAPA 556','IP1605200325',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(67,NULL,'MAPA 557','IP1602200057',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(68,NULL,'MAPA 558','IP1602200170',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(69,NULL,'MAPA 559','IP1602200295',2,2,2,7,7,NULL,1,NULL,'5/12/2019 09:41:05 am',NULL,'felipem','<br>'),(70,NULL,'MAPA 560','IP1602200047',2,2,2,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(71,NULL,'MAPA 561','IP1602200018',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(72,'781ea01118ca82192.jpeg','MAPA 562','17030100040',2,2,2,6,6,'2019-12-04',1,NULL,'4/12/2019 11:59:54 pm',NULL,'felipem','<br>'),(73,NULL,'MAPA 564','IP1701400086',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(74,NULL,'MAPA 565','IP1701400310',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(75,NULL,'MAPA 566','IP1701400267',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(76,NULL,'MAPA 567','IP1701400116',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(77,NULL,'MAPA 568','IP1701400067',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(78,'f0babaaecbf90b169.jpeg','MAPA 569','17030100024',2,2,2,3,3,'2019-12-05',3,NULL,'5/12/2019 12:02:36 am',NULL,'felipem','Equipo dañado&nbsp;'),(79,NULL,'MAPA 570','17030100023',2,2,2,10,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(80,'7cdb97b607ea37f77.jpeg','MAPA 571','17030100042',2,2,2,1,1,'2019-12-03',1,NULL,'3/12/2019 11:19:56 am',NULL,'felipem','<br>'),(81,NULL,'MAPA 572','17030100190',2,2,2,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(82,'3bd9b5e8c99cc1012.jpeg','MAPA 573','17030100192',2,2,2,1,1,'2019-12-03',1,NULL,'3/12/2019 11:22:30 am',NULL,'felipem','<br>'),(83,NULL,'MAPA 574','17030100036',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(84,NULL,'MAPA 575','17030100031',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(85,NULL,'MAPA 576','17030100016',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(86,NULL,'MAPA 577','17030100188',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(87,'dbd1a242bfc1e2cbf.jpeg','MAPA 578','17030100055',2,2,2,1,1,'2019-12-03',1,NULL,'3/12/2019 11:22:58 am',NULL,'felipem','<br>'),(88,NULL,'MAPA 579','17030100033',2,2,2,7,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL),(89,NULL,'MAPA 580','17030100037',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(90,NULL,'MAPA 581','17030100029',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(91,NULL,'MAPA 582','17030100030',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(92,NULL,'MAPA 583','17030100027',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(93,NULL,'MAPA 584','17030100052',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(94,NULL,'MAPA 585','17030100032',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(95,NULL,'MAPA 586','17030100021',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(96,NULL,'MAPA 587','17030100041',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(97,NULL,'MAPA 588','17030100174',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),(98,NULL,'MAPA 589','17030100184',2,2,2,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `dispositivos_backup_20191205094300` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dispositivos_backup_20191227141506`
--

DROP TABLE IF EXISTS `dispositivos_backup_20191227141506`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dispositivos_backup_20191227141506` (
  `id_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `ubicacion` int(10) unsigned NOT NULL,
  `ubicacion_abre` int(10) unsigned DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) unsigned NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_dispo`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  UNIQUE KEY `serial_unique` (`serial`),
  KEY `tipo_dispositivo` (`tipo_dispositivo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `ubicacion` (`ubicacion`),
  KEY `estado` (`estado`)
) ENGINE=MyISAM AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dispositivos_backup_20191227141506`
--

LOCK TABLES `dispositivos_backup_20191227141506` WRITE;
/*!40000 ALTER TABLE `dispositivos_backup_20191227141506` DISABLE KEYS */;
INSERT INTO `dispositivos_backup_20191227141506` VALUES (1,'a0c7a87f98ff4c423.jpg','HOLTER 101','3A-7125',1,1,1,6,6,'2018-11-20',1,'27/11/2019 09:37:37 am','27/11/2019 11:06:28 am','felipem','felipem',NULL,NULL),(2,'8710a41018a7c4e42.jpg','HOLTER 102','3A-11605',1,1,1,1,1,'2019-11-27',1,'27/11/2019 09:40:39 am','3/12/2019 08:18:57 am','felipem','felipem','<br>',NULL),(3,'6630f469154d3320d.jpg','HOLTER 103','3A-8045',1,1,1,3,3,'2019-12-27',1,NULL,'27/12/2019 11:35:13 am',NULL,'felipem','Tenia el serial 3A-8044, pero se cambia a 3A-8045',1),(4,'796ef58bbcaf815d5.jpg','HOLTER 104','3A-11606',1,1,1,2,2,'2019-12-27',1,NULL,'27/12/2019 06:11:49 am',NULL,'felipem','<br>',1),(5,'a1ca49f2da0d3858a.jpg','HOLTER 105','3A-11931',1,1,1,3,3,'2019-12-27',1,NULL,'27/12/2019 11:50:10 am',NULL,'felipem','Antes tenia el 3A-1039',1),(6,'fa31345b2dec3f75d.jpg','HOLTER 106','3A-11607',1,1,1,2,2,'2019-12-27',1,NULL,'27/12/2019 06:08:41 am',NULL,'felipem','Aparece con 3a-11934',1),(7,'62f8f3afd6062cf73.jpg','HOLTER 107','3A-11608',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:26:41 am',NULL,'felipem',NULL,NULL),(8,'16a714e4f58af1930.jpg','HOLTER 108','3A-2151',1,1,1,6,6,'2019-12-27',1,NULL,'27/12/2019 11:23:38 am',NULL,'felipem','Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial',1),(9,'fa647e5ab77632692.jpg','HOLTER 109','3A-9421',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 11:14:08 am',NULL,'felipem','<br>',NULL),(10,'476bc82bf3bed4616.jpg','HOLTER 110','3A-9310',1,1,1,6,6,'2018-11-20',1,NULL,'27/11/2019 10:46:21 am',NULL,'felipem',NULL,NULL),(11,'a456dd57854fcd34f.jpg','HOLTER 111','3A-11609',1,1,1,7,7,NULL,1,NULL,'27/11/2019 10:27:00 am',NULL,'felipem',NULL,NULL),(12,'e79c8397c6fa85698.jpg','HOLTER 112','3A-12178',1,1,1,2,2,'2019-12-27',1,NULL,'27/12/2019 06:05:51 am',NULL,'felipem','Aparece con 130 pero es 112&nbsp;',1),(13,'79766d196e19eb1db.jpg','HOLTER 113','3A-9311',1,1,1,2,2,'2019-12-27',1,NULL,'27/12/2019 06:10:07 am',NULL,'felipem','Aparece con 3A-11948',1),(14,'1c1558589faf656c3.jpg','HOLTER 114','3A-11815',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:27:18 am',NULL,'felipem',NULL,NULL),(15,'a47482f69e6bb1445.jpg','HOLTER 115','3A-05295',1,1,1,7,7,'2019-12-27',3,NULL,'27/12/2019 11:59:48 am',NULL,'felipem','Equipo fundido en desfibrilacion según estado de las tarjetas',1),(16,'fad32fe2f96f1b6f2.jpg','HOLTER 116','3A-8940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:01 am',NULL,'felipem',NULL,NULL),(17,'f6216dd5ac51a2f82.jpg','HOLTER 117','3A-9484',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 01:38:12 pm',NULL,'felipem','Esruche<div>Cable viejo</div>',NULL),(18,'6b9ff54089c40cbf3.jpg','HOLTER 118','3A-9486',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:12 am',NULL,'felipem',NULL,NULL),(19,'ad6939e30d7026eee.jpg','HOLTER 119','3A-9572',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:28:21 am',NULL,'felipem',NULL,NULL),(20,'636978098692e0616.jpg','HOLTER 120','3A-9487',1,1,1,2,2,'2019-12-27',1,NULL,'27/12/2019 06:11:01 am',NULL,'felipem','<br>',1),(21,'05353fef66bf34245.jpg','HOLTER 121','3A-8946',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:27 am',NULL,'felipem',NULL,NULL),(22,'753003a921f5f02ec.jpg','HOLTER 122','3A-9485',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:32 am',NULL,'felipem',NULL,NULL),(23,'5cc88d4b86424bb7d.jpg','HOLTER 123','3A-1993',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 11:03:57 am',NULL,'felipem','Antes holter 112',NULL),(24,'b89cb8d899578dd8b.jpg','HOLTER 124','3A-10318',1,1,1,5,5,'2019-12-27',1,NULL,'27/12/2019 06:26:34 am',NULL,'felipem','Aparece con 3a-9311 en csf',1),(25,'2b4b8d4d10dddbe8b.jpg','HOLTER 125','3A-11816',1,1,1,4,4,NULL,1,NULL,'27/11/2019 10:28:47 am',NULL,'felipem',NULL,NULL),(26,'06b68e149d1227503.jpg','HOLTER 126','3A-8044',1,1,1,1,1,'2019-12-03',1,NULL,'3/12/2019 10:42:01 am',NULL,'felipem','Antes 3A-11818',NULL),(27,'f4964486301b6afe2.jpg','HOLTER 127','3A-12175',1,1,1,5,5,'2019-12-27',1,NULL,'27/12/2019 06:20:21 am',NULL,'felipem','<br>',1),(28,'d179e9e2972b91703.jpg','HOLTER 128','3A-12176',1,1,1,5,5,'2019-12-27',1,NULL,'27/12/2019 06:23:06 am',NULL,'felipem','<br>',1),(29,'9e12d7e5ca84cd0f4.jpg','HOLTER 129','3A-11940',1,1,1,6,6,NULL,1,NULL,'27/11/2019 10:29:09 am',NULL,'felipem',NULL,NULL),(57,'c11ec5b51107d1e95.jpg','HOLTER 130','3A-11295',1,1,1,9,9,'2019-11-29',1,'29/11/2019 04:45:35 pm','5/12/2019 11:02:44 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL),(31,'3cc42f4af7a84575e.jpg','ECO 201','US97804887',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:28:42 am',NULL,'felipem',NULL,NULL),(32,'536dec1c5345e2b59.jpg','ECO 202','US97806979',3,3,3,6,6,NULL,1,NULL,'29/11/2019 01:11:49 am',NULL,'felipem',NULL,NULL),(33,'2edf9e5e9f0adf0b0.jpg','ECO 203','3728A01811',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:12:32 am',NULL,'felipem',NULL,NULL),(34,'b35b0af60ed58ef47.jpg','ECO 204','US50210876',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:13:01 am',NULL,'felipem',NULL,NULL),(35,'d6bb8df0f60c48ab0.jpg','ECO 205','3728A00382',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:19:52 am',NULL,'felipem',NULL,NULL),(36,'620450dbf61c4c19f.jpg','ECO 206','9708A09160',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:04:44 am',NULL,'felipem','<br>',NULL),(37,'41736a0baf4cb45e3.jpg','ECO 207','71674',3,6,5,7,7,NULL,1,NULL,'29/11/2019 01:24:01 am',NULL,'felipem',NULL,NULL),(38,'0eb9fdc48d520b1a8.jpg','ECO 208','001838VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:29 am',NULL,'felipem',NULL,NULL),(39,'f7c6ea863e2c80a47.jpg','ECO 209','055133VI',3,5,4,7,7,NULL,1,NULL,'29/11/2019 01:24:16 am',NULL,'felipem',NULL,NULL),(40,'c86621e1a393abd5e.jpg','ECO 210','3728A01073',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:03:57 am',NULL,'felipem','<br>',NULL),(41,'158d01f499364579d.jpg','ECO 211','USO0211434',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:06 am',NULL,'felipem',NULL,NULL),(42,'387a137ebf96710b6.jpg','ECO 212','US97807671',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:21:46 am',NULL,'felipem',NULL,NULL),(43,'5ed931c87e7ad07f3.jpg','ECO 213','9708A04535',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:34 am',NULL,'felipem',NULL,NULL),(44,'e624e9cfa97691728.jpg','ECO 214','3728A00160',3,3,3,6,6,'2019-12-04',1,NULL,'4/12/2019 11:54:30 pm',NULL,'felipem','<br>',NULL),(45,'54d451022df8c30e6.jpg','ECO 215','US97703545',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:07 am',NULL,'felipem',NULL,NULL),(46,'0b54a448e7a5a93aa.jpg','ECO 216','US97808948',3,3,3,6,6,NULL,1,NULL,'4/12/2019 11:56:52 pm',NULL,'felipem','<br>',NULL),(47,'630053d26916342ca.jpg','ECO 217','US97800318',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:22:57 am',NULL,'felipem',NULL,NULL),(48,'9972d0e70d8ca4ca6.jpg','ECO 218','3728A02302',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:47 am',NULL,'felipem',NULL,NULL),(49,'2e9dc0bf01f8da61f.jpg','ECO 219','US80210739',3,3,3,1,1,'2019-12-03',1,NULL,'3/12/2019 11:30:51 am',NULL,'felipem','<br>',NULL),(50,'6e5eaa081a347bc98.jpg','ECO 220','US97703205',3,3,3,1,1,'2019-12-03',1,NULL,'3/12/2019 11:27:58 am',NULL,'felipem','<br>',NULL),(51,'10b8abd62f6418d47.jpg','ECO 221','US97804106',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:25:05 am',NULL,'felipem',NULL,NULL),(52,'b6efed45a482c8c76.jpg','ECO 222','US97804535',3,3,3,7,7,NULL,1,NULL,'28/11/2019 10:37:14 am',NULL,'lgarcia',NULL,NULL),(53,'326d48038544d0081.jpg','ECO 223','US70421029',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:24:53 am',NULL,'felipem',NULL,NULL),(54,'2114d2a34043479d1.jpg','ECO 224','US97703258',3,3,3,7,7,NULL,1,NULL,'29/11/2019 01:26:47 am',NULL,'felipem',NULL,NULL),(55,'99c80bd69f2b7d401.jpg','ECO 225','USD0211891',3,3,3,4,4,'2019-12-05',1,NULL,'5/12/2019 12:06:31 am',NULL,'felipem','Verificar código puesto en el equipo&nbsp;',NULL),(56,'a4df3e5a8cd507c06.jpg','PC 621','JCN0GR05Z985517',12,7,6,1,1,'2019-08-07',1,'27/11/2019 06:30:03 pm','26/12/2019 06:24:23 pm','vilmao','felipem','<br>',6),(58,'2fb3b0f1c5f8919a5.jpg','HOLTER 131','3A-11287',1,1,1,9,9,'2019-11-29',1,'29/11/2019 05:06:08 pm','5/12/2019 11:03:20 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL),(59,'982c34a6a71597fe5.jpg','HOLTER 132','3A-11293',1,1,1,1,1,'2019-12-03',1,'3/12/2019 12:38:32 am','5/12/2019 11:04:07 pm','felipem','felipem','Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL),(60,'5c9c7e2c36eb50b7f.jpg','HOLTER 133','3A-11285',1,1,1,1,1,'2019-11-29',1,'3/12/2019 12:41:03 am','5/12/2019 11:04:28 pm','felipem','felipem','Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL),(61,'f19510876e2eb19cd.jpeg','MAPA 551','IP309100004',2,2,7,3,3,NULL,3,NULL,'5/12/2019 12:01:35 am',NULL,'felipem','Equipo dañado&nbsp;',NULL),(62,'1d40088c027c6481a.jpeg','MAPA 552','00087513',2,8,8,6,6,'2019-12-04',1,NULL,'4/12/2019 11:59:19 pm',NULL,'felipem','<br>',NULL),(63,NULL,'MAPA 553','140206005',2,2,7,3,NULL,NULL,2,NULL,NULL,NULL,NULL,NULL,NULL),(64,NULL,'MAPA 554','IP1602200009',2,2,2,7,7,'2019-12-27',3,NULL,'27/12/2019 12:48:25 pm',NULL,'felipem','flm',2),(65,NULL,'MAPA 555','IP1602200106',2,2,2,7,7,NULL,1,NULL,'27/12/2019 12:06:27 pm',NULL,'felipem','<br>',2),(66,NULL,'MAPA 556','IP1605200325',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(67,NULL,'MAPA 557','IP1602200057',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(68,NULL,'MAPA 558','IP1602200170',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(69,NULL,'MAPA 559','IP1602200295',2,2,2,7,7,NULL,1,NULL,'5/12/2019 09:41:05 am',NULL,'felipem','<br>',NULL),(70,NULL,'MAPA 560','IP1602200047',2,2,2,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(71,NULL,'MAPA 561','IP1602200018',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(72,'781ea01118ca82192.jpeg','MAPA 562','17030100040',2,2,2,6,6,'2019-12-04',1,NULL,'4/12/2019 11:59:54 pm',NULL,'felipem','<br>',NULL),(73,NULL,'MAPA 564','IP1701400086',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(74,NULL,'MAPA 565','IP1701400310',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(75,NULL,'MAPA 566','IP1701400267',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(76,NULL,'MAPA 567','IP1701400116',2,2,2,5,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(77,NULL,'MAPA 568','IP1701400067',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(78,'f0babaaecbf90b169.jpeg','MAPA 569','17030100024',2,2,2,7,7,'2019-12-27',3,NULL,'27/12/2019 12:50:46 pm',NULL,'felipem','Equipo dañado&nbsp;',2),(79,NULL,'MAPA 570','17030100023',2,2,2,10,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(80,'7cdb97b607ea37f77.jpeg','MAPA 571','17030100042',2,2,2,1,1,'2019-12-03',1,NULL,'3/12/2019 11:19:56 am',NULL,'felipem','<br>',NULL),(81,NULL,'MAPA 572','17030100190',2,2,2,2,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(82,'3bd9b5e8c99cc1012.jpeg','MAPA 573','17030100192',2,2,2,1,1,'2019-12-03',1,NULL,'3/12/2019 11:22:30 am',NULL,'felipem','<br>',NULL),(83,NULL,'MAPA 574','17030100036',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(84,NULL,'MAPA 575','17030100031',2,2,2,5,5,'2019-12-27',1,NULL,'27/12/2019 05:59:45 am',NULL,'felipem','<br>',2),(85,NULL,'MAPA 576','17030100016',2,2,2,3,3,'2019-12-27',1,NULL,'27/12/2019 11:36:25 am',NULL,'felipem','<br>',2),(86,NULL,'MAPA 577','17030100188',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(87,'dbd1a242bfc1e2cbf.jpeg','MAPA 578','17030100055',2,2,2,1,1,'2019-12-03',1,NULL,'3/12/2019 11:22:58 am',NULL,'felipem','<br>',NULL),(88,NULL,'MAPA 579','17030100033',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:55:48 pm',NULL,'felipem','<br>',2),(89,NULL,'MAPA 580','17030100037',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(90,NULL,'MAPA 581','17030100029',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:53:47 pm',NULL,'felipem','<br>',2),(91,NULL,'MAPA 582','17030100030',2,2,2,5,5,NULL,1,NULL,'27/12/2019 06:00:59 am',NULL,'felipem','<br>',2),(92,NULL,'MAPA 583','17030100027',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:56:35 pm',NULL,'felipem','<br>',2),(93,NULL,'MAPA 584','17030100052',2,2,2,9,9,'2019-12-27',1,NULL,'27/12/2019 12:56:09 pm',NULL,'felipem','<br>',2),(94,NULL,'MAPA 585','17030100032',2,2,2,3,3,'2019-12-27',1,NULL,'27/12/2019 11:35:49 am',NULL,'felipem','flm',2),(95,NULL,'MAPA 586','17030100021',2,2,2,7,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(96,NULL,'MAPA 587','17030100041',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(97,NULL,'MAPA 588','17030100174',2,2,2,4,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(98,NULL,'MAPA 589','17030100184',2,2,2,6,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL),(99,NULL,'TR 251','404262WX8',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'GE , 3Sc-RS',NULL),(100,NULL,'TR 252','333407WX5',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'GE , 3Sc-RS',NULL),(101,NULL,'TR 253','US97310163',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 21330A',NULL),(102,NULL,'TR 254','US99N01008',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , S3',NULL),(103,NULL,'TR 255','02M7J5',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 11-3L',NULL),(104,NULL,'TR 256','03DNL7',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , IPx-7',NULL),(105,NULL,'TR 257','US99N01942',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SIUI , 21311A',NULL),(106,NULL,'TR 258','3709A00256',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 213550A',NULL),(107,NULL,'TR 259','US97404881',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , T62110',NULL),(108,NULL,'TR 260','US99300683',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 11-3L',NULL),(109,NULL,'TR 261','US97302745',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 21350A',NULL),(110,NULL,'TR 262','US99N04288',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , ',NULL),(111,NULL,'TR 263','02M7PY',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , IPX-7',NULL),(112,NULL,'TR 264','3630A00395',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 21367A',NULL),(113,NULL,'TR 265','18ZHQQ',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , 21369A',NULL),(114,NULL,'TR 266','02QDYM',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , 21311A',NULL),(115,NULL,'TR 267','142585PD9',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'GE , 6S-RS',NULL),(116,NULL,'TR 268','US99300678',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 21321A',NULL),(117,NULL,'TR 269','US99N03966',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILLIPS , 21311A',NULL),(118,NULL,'TR 270','US97403824',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , ',NULL),(119,NULL,'TR 271','3716A00792',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HEWLETT PACKARD , 21369A',NULL),(120,NULL,'TR 272','US99N04681',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PHILIPS , 21311A',NULL),(121,NULL,'ECG 301','20161031/BTT02/00293',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT02',NULL),(122,NULL,'ECG 302','20161031/BTT02/01743',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT02',NULL),(123,NULL,'ECG 303','20161031/BTT02/01729',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT02',NULL),(124,NULL,'ECG 304','20140721/BBT02/00290',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT02',NULL),(125,NULL,'ECG 305','D1081A0753',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'CONTEC 8000',NULL),(126,NULL,'DG 401','180401',15,9,9,7,7,'2019-12-10',1,NULL,'10/12/2019 12:30:54 am',NULL,'felipem','HOSPITAL ISAIAS DUARTE CANCINO',NULL),(127,NULL,'DG 402','180402',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(128,'18a13cd3f35261938.jpg','DG 403','180403',15,9,9,9,9,'2019-12-10',1,NULL,'10/12/2019 12:38:09 am',NULL,'felipem','<br>',NULL),(129,NULL,'DG 404','180404',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(130,NULL,'DG 405','180405',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(131,NULL,'DG 406','180406',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL),(132,NULL,'DG 407','180407',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HOSPITAL ISAIAS DUARTE CANCINO',NULL),(133,NULL,'DES 451','B97K125',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'ZOLL , PD 1200 ,  , ',NULL),(134,NULL,'DES 452','B97K12587',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'ZOLL , PD 1200 , YUMBO , SUMEDICA',NULL),(135,NULL,'DES 453','B97K12574',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE',NULL),(136,NULL,'DES 454','98278',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,' , PIC 30 , PALMIRA , HOSPITAL RAUL OREJUELA BUENO',NULL),(137,NULL,'DES 455','12129094',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER',NULL),(138,NULL,'DES 456','12127290',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'ARTEMA , IP 22 , CALI , HIDC',NULL),(139,NULL,'DES 457','5126132',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'ARTEMA ,  , CARDIO AID 200 , HIDC',NULL),(140,NULL,'DES 458','90427',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'WELCHALLYN , PIC 30 , TULUA , HOSPITAL TTOMAS URIBE',NULL),(141,NULL,'PC601','A7V87A',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'COMPAQ ,  , COMPAQ , ',NULL),(142,NULL,'PC602','HN3F91QC100398',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SAMSUNG ,  , INTEGRADO , ',NULL),(143,NULL,'PC603','U082MA00',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DELL ,  , INTEGRADO , ',NULL),(144,NULL,'PC604','CLON',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'INTEL ,  , JANUS , ',NULL),(145,NULL,'PC606','CS02589545',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LENOVO ,  , INTEGRADO , ',NULL),(146,NULL,'PC607','VS81211172',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LENOVO ,  , INTEGRADO , ',NULL),(147,NULL,'PC608','6MC2190SSX',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LENOVO ,  , INTEGRADO , ',NULL),(148,NULL,'PC609','3CR2221DW9',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'COMPAQ ,  , COMPAQ , ',NULL),(149,NULL,'PC610','VS81225546',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LENOVO ,  , INTEGRADO , ',NULL),(150,NULL,'PC611','5CB40840JJ',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HP ,  , INTEGRADO , ',NULL),(151,NULL,'PC612','HYXS91LD200746',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SAMSUNG ,  , INTEGRADO , ',NULL),(152,NULL,'PC613','5CM31902QS',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HP ,  , INTEGRADO , ',NULL),(153,NULL,'PC614','',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LG ,  , JONUS , ',NULL),(154,NULL,'PC615','335255',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'COMPAQ ,  , AOPEN , ',NULL),(155,NULL,'PC617','5CD440256Q',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'HP ,  , HP , ',NULL),(156,NULL,'PC618','CS01429411',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LENOVO ,  , INTEGRADO , ',NULL),(157,NULL,'PC619','PF0N6RLY',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'LENOVO ,  , NA , ',NULL),(158,'bcdf768ef42798802.jpg','PC620','CS02593439',11,10,11,9,9,'2019-12-10',1,NULL,'10/12/2019 04:15:46 pm',NULL,'felipem','LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div>',NULL),(159,NULL,'BT701','SN: 10-10-10',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'PERFECT 10 , PERFECT 10 ,  , ',NULL),(160,NULL,'BT702','201409026161',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SPORT FITNESS , JS-5000B-1 ,  , ',NULL),(161,NULL,'BT704','SN: 270765 JC',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,' ,  ,  , ',NULL),(162,NULL,'BT705','201312044116',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SPORT FITNESS , JS-5000B-1 ,  , ',NULL),(163,NULL,'BT706','201409026192',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SPORT FITNESS , JS-5000B-1 ,  , ',NULL),(164,NULL,'BT708','201509043123',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SPORT FITNESS ,  ,  , ',NULL),(165,NULL,'BT710','201509043152',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'SPORT FITNESS , JS-5000B-1 , HIDC , ',NULL),(166,NULL,'STR751','20140625/BTT/01068',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20140625/BTR/01068 ,  , ',NULL),(167,NULL,'STR752','20140625/BTT/01107',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20140625/BTR/01107 ,  , ',NULL),(168,NULL,'STR753','20160725/BTT/01304',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20160725/BTR/01304 ,  , ',NULL),(169,NULL,'STR754','20160104/BTT/01253',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20160104/BTR/01253 ,  , ',NULL),(170,NULL,'STR755','20141201/BTT/01123',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20141201/BTR/01123 ,  , ',NULL),(171,NULL,'STR756','20160725/BTT/01307',6,1,10,9,9,'2019-12-10',1,NULL,'10/12/2019 09:46:57 am',NULL,'felipem','DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>',NULL),(172,NULL,'STR757','20150129/BTT/01136',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20150129/BTR/01136 ,  , ',NULL),(173,NULL,'STR758','20160104/BTR/01253',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 ,  ,  , ',NULL),(174,NULL,'STR759','4712AU3681E',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'QRSCardUSB , NA ,  , ',NULL),(175,NULL,'STR760','SN BTR01-343',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 ,  ,  , ',NULL),(176,NULL,'STR761','SM 311SN: D111E0004',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS300-BTT03 ,  ,  , ',NULL),(177,NULL,'STR762','SN: 20140721/BTT02/00290',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS300-BTT02 ,  ,  , ',NULL),(178,NULL,'STR763','20160725/BTT/01299',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'DMS 300-BTT01 , 20160725/BTR/01299 ,  , ',NULL),(179,NULL,'SM901','SM 10',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 8 GB , BUGA , HSJ',NULL),(180,NULL,'SM902','SM 11',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 8 GB , BUGA , HSJ',NULL),(181,NULL,'SM903','SM 12',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 8 GB , CALI , ADMIN.',NULL),(182,NULL,'SM904','SM 13',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 8 GB , TULUA , HTU',NULL),(183,NULL,'SM905','SM 14',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 8 GB , CASA , CASA',NULL),(184,NULL,'SM906','SM 15',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , TECNOLOGIA',NULL),(185,NULL,'SM907','SM 16',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(186,NULL,'SM908','SM 17',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(187,NULL,'SM909','SM 18',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , BUGA , HSJ',NULL),(188,NULL,'SM910','SM 19',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , BUGA , HSJ',NULL),(189,NULL,'SM911','SM 20',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(190,NULL,'SM912','SM 21',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(191,NULL,'SM913','SM 22',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(192,NULL,'SM914','SM 23',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(193,NULL,'SM915','SM 24',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'KINGSTON , 16 GB , CASA , ',NULL),(194,NULL,'SM801','VND3J28585',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO',NULL),(195,NULL,'SM802','CN26RBK063',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO',NULL),(196,NULL,'SM803','CN22SBTFP',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO',NULL),(197,NULL,'SM804','SE8Y011865',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO',NULL),(198,NULL,'SM805','SE8Y018164',0,0,0,0,NULL,NULL,0,NULL,NULL,NULL,NULL,'EPSON WORKFORCE ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO',NULL),(199,'d7dffdba6db4acaa5.jpg','HOLTER 134','3A-11289',1,1,1,4,4,'2019-12-05',2,'5/12/2019 10:59:48 pm','10/12/2019 06:38:50 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>',NULL),(200,'0517ef6974ab1463b.jpg','HOLTER 135','3A-11288',1,1,1,5,5,'2019-12-11',1,'5/12/2019 11:01:16 pm','27/12/2019 11:15:52 am','felipem','felipem','Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>',1),(201,'c830bd855b5933e47.jpg','HOLTER 136','3A-11286',1,1,1,5,5,'2019-12-11',1,'5/12/2019 11:06:13 pm','27/12/2019 11:06:57 am','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',1),(202,'9cd28935c6a69d0a3.jpg','HOLTER 137','3A-11292',1,1,1,4,4,'2019-12-05',1,'5/12/2019 11:07:54 pm','10/12/2019 06:44:08 pm','felipem','felipem','Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>',NULL),(203,'ec24083a0b7fcd5a2.jpg','HOLTER 138','3A-11291',1,1,1,4,4,'2019-12-09',1,'5/12/2019 11:08:49 pm','10/12/2019 06:41:34 pm','felipem','felipem','<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>',NULL),(204,'caeae04f26da4d82b.jpg','HOLTER 139','3A-11290',1,1,1,5,5,'2019-12-05',1,'5/12/2019 11:09:40 pm','27/12/2019 11:09:07 am','felipem','felipem','<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>',1),(205,NULL,'PRUEBA1','123456',10,7,6,7,7,'2019-12-27',2,'27/12/2019 09:54:45 am',NULL,'bodega.felipe',NULL,'<br>',6),(206,NULL,'MAPA 590','191202000',2,2,12,7,7,'2019-12-27',1,'27/12/2019 01:58:38 pm',NULL,'felipem',NULL,'<br>',12),(207,NULL,'MAPA 591','19120200002',2,2,12,7,7,'2019-12-27',1,'27/12/2019 02:01:17 pm',NULL,'felipem',NULL,'Noviembre 2019 China',12);
/*!40000 ALTER TABLE `dispositivos_backup_20191227141506` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docu_modelo`
--

DROP TABLE IF EXISTS `docu_modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docu_modelo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_dispo` int(10) unsigned DEFAULT NULL,
  `marca` int(10) unsigned DEFAULT NULL,
  `modelo` int(10) unsigned DEFAULT NULL,
  `tipo_documento` int(10) unsigned DEFAULT NULL,
  `documento` varchar(40) DEFAULT NULL,
  `descripcion` varchar(40) DEFAULT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_dispo` (`tipo_dispo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`),
  KEY `tipo_documento` (`tipo_documento`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docu_modelo`
--

LOCK TABLES `docu_modelo` WRITE;
/*!40000 ALTER TABLE `docu_modelo` DISABLE KEYS */;
/*!40000 ALTER TABLE `docu_modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentos`
--

DROP TABLE IF EXISTS `documentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dispo_codigo` int(10) unsigned NOT NULL,
  `dispo_serial` int(10) unsigned DEFAULT NULL,
  `tipo_docu` int(10) unsigned NOT NULL,
  `carga` varchar(40) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_carga` varchar(40) DEFAULT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `creado1` datetime NOT NULL,
  `enlace` varchar(40) DEFAULT NULL,
  `creado` timestamp NULL DEFAULT NULL,
  `editado` timestamp NULL DEFAULT NULL,
  `field13` varchar(40) DEFAULT NULL,
  `field14` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dispo_codigo` (`dispo_codigo`),
  KEY `tipo_docu` (`tipo_docu`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentos`
--

LOCK TABLES `documentos` WRITE;
/*!40000 ALTER TABLE `documentos` DISABLE KEYS */;
INSERT INTO `documentos` VALUES (1,56,56,1,'carta_la_capellana.docx','Factura','27/11/2019','1574898656',NULL,'vilmao',NULL,'0000-00-00 00:00:00',NULL,NULL,NULL,NULL,NULL),(2,243,243,2,'-HOJA_DE_VIDA-ASPIRADOR.pdf','-','28/9/2020','1601328288',NULL,'lgarcia',NULL,'0000-00-00 00:00:00',NULL,'2020-09-28 21:24:48',NULL,NULL,NULL);
/*!40000 ALTER TABLE `documentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fichatecnica`
--

DROP TABLE IF EXISTS `fichatecnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fichatecnica` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dispo_codigo` int(10) unsigned NOT NULL,
  `dispo_serial` int(10) unsigned DEFAULT NULL,
  `hardware` text,
  `software` text,
  `enlace` text,
  `carga` varchar(40) DEFAULT NULL,
  `fecha_carga` date DEFAULT NULL,
  `creado` timestamp NULL DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dispo_codigo` (`dispo_codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fichatecnica`
--

LOCK TABLES `fichatecnica` WRITE;
/*!40000 ALTER TABLE `fichatecnica` DISABLE KEYS */;
INSERT INTO `fichatecnica` VALUES (1,242,242,'<span style=\"font-size: 11.998px;\">Sistema Operativo:&nbsp;Windows 7 Ultimate 64-bit (6.1, Build 7601) Service</span><div style=\"font-size: 11.998px;\">Memoria RAM: 4G</div><div style=\"font-size: 11.998px;\">Procesador:&nbsp;AMD E1-1500 APU with Radeon(tm) HD Graphics (2 CPUs), ~1.5GHz</div><div style=\"font-size: 11.998px;\">Disco Duro: 1TB</div>','<div style=\"\">Office 365<br>CardioScan<br>ABPM50<br></div>',NULL,NULL,'2020-06-24','2020-06-25 01:37:35','felipem','2020-06-24 20:38:32','felipem');
/*!40000 ALTER TABLE `fichatecnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo_dispo`
--

DROP TABLE IF EXISTS `grupo_dispo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo_dispo` (
  `id_grupo_dispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grupo_dispo` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_grupo_dispo`),
  UNIQUE KEY `grupo_dispo_unique` (`grupo_dispo`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo_dispo`
--

LOCK TABLES `grupo_dispo` WRITE;
/*!40000 ALTER TABLE `grupo_dispo` DISABLE KEYS */;
INSERT INTO `grupo_dispo` VALUES (1,'ACCESORIO','Accesorio del dispositivo'),(2,'REPUESTO','Repuesto para el dispositivo'),(3,'EQUIPO','Equipo o dispositivo');
/*!40000 ALTER TABLE `grupo_dispo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenes`
--

DROP TABLE IF EXISTS `imagenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagen` varchar(40) NOT NULL,
  `tipo_dispo` int(10) unsigned NOT NULL,
  `marca` int(10) unsigned NOT NULL,
  `modelo` int(10) unsigned NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_dispo` (`tipo_dispo`),
  KEY `marca` (`marca`),
  KEY `modelo` (`modelo`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenes`
--

LOCK TABLES `imagenes` WRITE;
/*!40000 ALTER TABLE `imagenes` DISABLE KEYS */;
INSERT INTO `imagenes` VALUES (1,'d0f94ddf2d0c66e91.jpg',1,1,1,'Holter'),(2,'0f3cd7645201167b9.jpg',3,3,3,'PHILIPS'),(3,'3c7ab71c1d907fca8.jpg',15,9,9,'Digitalizador para ecografo'),(4,'1ad1671dda11eda6d.jpg',6,1,10,NULL),(5,'72a4aa7397261433b.jpg',11,10,11,'MODELO 10160');
/*!40000 ALTER TABLE `imagenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mantenimientos`
--

DROP TABLE IF EXISTS `mantenimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mantenimientos` (
  `id_mtto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` int(10) unsigned NOT NULL,
  `serial` int(10) unsigned DEFAULT NULL,
  `fecha_mtto` date NOT NULL,
  `tipo_mtto` int(10) unsigned DEFAULT NULL,
  `responsable` int(10) unsigned DEFAULT NULL,
  `id_responsable` int(10) unsigned DEFAULT NULL,
  `documento` varchar(40) NOT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field13` varchar(40) DEFAULT NULL,
  `field14` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_mtto`),
  KEY `codigo` (`codigo`),
  KEY `tipo_mtto` (`tipo_mtto`),
  KEY `responsable` (`responsable`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mantenimientos`
--

LOCK TABLES `mantenimientos` WRITE;
/*!40000 ALTER TABLE `mantenimientos` DISABLE KEYS */;
INSERT INTO `mantenimientos` VALUES (1,70,70,'2020-02-19',1,3,3,'desktop-ic-aio-a340.jpg','0000-00-00 00:00:00',NULL,'felipem',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `mantenimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca_modelo`
--

DROP TABLE IF EXISTS `marca_modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca_modelo` (
  `id_mmodelo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marca` int(10) unsigned NOT NULL,
  `modelo` varchar(40) NOT NULL,
  `fabricante` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `img` varchar(40) NOT NULL,
  `field6` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_mmodelo`),
  UNIQUE KEY `modelo_unique` (`modelo`),
  KEY `marca` (`marca`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca_modelo`
--

LOCK TABLES `marca_modelo` WRITE;
/*!40000 ALTER TABLE `marca_modelo` DISABLE KEYS */;
INSERT INTO `marca_modelo` VALUES (1,1,'300-3A','DMSoftware','<br>','13f4515ec1b290738.jpg',NULL),(2,2,'ABPM50','CONTEC MEDICAL','<br>','c5a4e52ffd1371037.png',NULL),(3,3,'SONOS5500','PHILIPS','<br>','b8a5974b09f495570.jpg',NULL),(4,5,'VIVID-i','GE','<br>','3dd6d629302078b98.jpg',NULL),(5,6,'ACUSON CYPRESS','SIMENS','<br>','34f8b1277a9587f5c.jpg',NULL),(6,7,'X407U','ASUS','<br>','7c374d1d63b19e374.jpg',NULL),(7,2,'CONTEC 8000','CONTEC MEDICAL','<br>','d1f4078ebe073796e.jpg',NULL),(8,8,'BRAVO 24H','SUNTECH','<br>','4968e54c14cc0374d.jpg',NULL),(9,9,'VIDEO CAPTURE','PINNACLE','<br>','17faafb62b0451cd3.jpg',NULL),(10,1,'DMS-300BTT/BTR','DMSoftware','<br>','59ac2dd651d16fa03.jpg',NULL),(11,10,'TODO EN UNO C260','LENOVO','<br>','0071cada5585eced1.jpg',NULL),(12,2,'PM50','CONTEC MEDICAL','MONITOR DE PRESIÓN ARTERIAL CON SPO','2886d30c54ea0035d.jpg',NULL),(13,1,'SOFTWARE CARDIOSCAN 76A','CONTEC MEDICAL','SOFTWARE HOLTER CARDIOSCAN 76A','a4aa682eab47aabbf.jpg',NULL),(14,1,'SOFTWARE CARDIOSCAN SATELLITE','CONTEC MEDICAL','CARDIOSCAN SATELLITE 2020','a224ed359a40d1c73.jpg',NULL),(15,10,'AIO A340','LENOVO','COMPUTADOR TODO EN UNO CORE i5 G8','79225158bd18a3b6f.jpg',NULL),(16,3,'21311A','PHILIPS','Transductor o sonda de referencia s3','f92192ee36757b969.jpg',NULL),(17,3,'21350A','PHILIPS','Transductor s8 pediátrico','9cdcca9829b39bf0c.jpg',NULL),(18,12,'JS-5000B-1','SPORT FITNESS','<br>','3a59617ab28a1a4f9.jpg',NULL),(19,4,'IP22','ARTEMA','<br>','0ccecf6da6df9d8ef.jpg',NULL),(20,3,'IPX-7','PHILIPS','<br>','f6d31817f8eed03c7.jpg',NULL),(21,13,'PD 1200','ZOLL MEDICAL','<br>','b9551239478536bcc.jpg',NULL),(22,14,'PERFECT10','PERFECT10','<br>','a4431b8946b1671e6.jpg',NULL),(23,15,'ASPIRE Z1-601','ACER','Equipo de computo todo en uno&nbsp;','fdcae9ccffb776572.jpg',NULL),(24,3,'21356A','HEWLETT PACKARD','HEWLETT PACKARD','6da7ac27c53ce45bb.jpg',NULL),(25,3,'21330A','HEWLETT PACKARD','HEWLETT PACKARD','ebd011d0abb9f9004.jpg',NULL),(26,3,'21369A','HEWLETT PACKARD','HEWLETT PACKARD','f524cae427b8d7d95.jpg',NULL),(27,4,'PIC 30','WELCH ALLYN','<br>','193661f9297760a25.jpg',NULL),(28,17,'L5190','EPSON','L5190 impresora multifuncional con scanner, fax, WiFi, pantalla LCD color&nbsp;','2367c21ed40024791.jpg',NULL),(29,18,'19EN33SA','LG','Pantalla LG','df5ab3fadf89e4a1e.jpg',NULL),(30,19,'SXT-5A','SMAF','<br>','f7c3b97bd92243612.jpg',NULL),(31,20,'TX628','ZKTeco','<br>','3abb6580f800a94a4.jpg',NULL),(32,21,'FP-960','NIPRO CORPORATION','<br>','ebfdad944b71e9446.jpg',NULL),(33,3,'T6210','PHILIPS','SONDA TRASESOFAGICA&nbsp;','dd847adc7bce4c1d8.jpg',NULL),(34,22,'XT1601','MOTOROLA','<div>Nombre del sistema operativo Android</div><div>Versión del sistema operativo 6.0.1 Marshmallow</div><div>Año de lanzamiento 2016</div><div>Peso 137 g</div><div>Altura 144.4 mm</div><div>Ancho 72 mm</div><div>Profundidad 9.9 mm</div><div>Pantalla táctil Sí</div><div>Tamaño de la pantalla 5 \"</div><div>Resolución de la pantalla 720 px x 1280 px</div><div>Píxeles por pulgada 294 ppi</div><div>Tecnología de pantalla IPS</div><div>Cámara Sí</div><div>Cantidad de cámaras traseras 1</div><div>Resolución de la cámara trasera principal 8 Mpx</div><div>Resolución de video de la cámara trasera 1920 px x 1080 px</div><div>Apertura del diafragma de la cámara trasera f 2.2</div><div>Cantidad de cámaras frontales 1</div><div>Resolución de la cámara frontal principal 5 Mpx</div><div>Resolución de video de la cámara frontal 1920 px x 1080 px</div><div>Apertura del diafragma de la cámara frontal f 2.2</div><div>Flash en la cámara frontal No</div><div>Red 4G/LTE</div><div>Cantidad de ranuras para tarjeta SIM 2</div><div>Tamaños de tarjeta SIM compatibles Micro-SIM</div><div>Tipos de tarjeta de memoria microSD</div><div>Capacidad máxima de la tarjeta de memoria 256 GB</div><div>Modelo del procesador Snapdragon 410</div><div>Modelos de CPU 4x1.2 GHz Cortex-A53</div><div>Cantidad de núcleos del procesador 4</div><div>Velocidad del procesador 1.2 GHz</div><div>Modelo de GPU Adreno 306</div><div>Teclado QWERTY físico No</div><div>Conector USB Sí</div><div>Wi-Fi Sí</div><div>GPS Sí</div><div>Bluetooth Sí</div><div>Mini HDMI No</div><div>Radio Sí</div><div>Lector de huella digital No</div><div>Reconocimiento facial Sí</div><div>Acelerómetro Sí</div><div>Sensor de proximidad Sí</div><div>Giroscopio No</div><div>Resistente a salpicaduras No</div><div>Resistente al agua No</div><div>A prueba de agua No</div><div>Tipo de batería Polímero de litio</div><div>Capacidad de la batería 2800 mAh</div><div>Batería removible Sí</div>','f7462b97dfea33400.jpg',NULL),(35,23,'CARDIO AID 200-B','ARTEMA','Desfibrilador ARTEMA','8df4132e249af5884.jpg',NULL),(36,10,'80MJ','LENOVO','COMPUTADOR PORTÁTIL&nbsp;','116a5c5700bde702d.jpg',NULL),(37,10,'80UC',NULL,'LENOVO IDEAPAD 110-14ISK','b6cfcf85b37de3e60.jpg',NULL),(38,24,'18-1311la','HP','HP 18 All in One PC','2f1b64c7b710a4d6d.jpg',NULL),(39,1,'HDMI-USB','DMS','Cable HDMI-USB para Holter 300-3A','e569ccd3f6c17cbd4.jpg',NULL),(40,24,'COMPAQ ELITE 8300','HP','Computador compaq corporativo','ea0da6ce4eacee00b.jpg',NULL),(41,17,'M205','Epson','<br>','747d772870e46040b.png',NULL),(42,25,'16 GB','Kingston','<br>','7e9dcd983a3881a42.jpg',NULL),(43,25,'8 GB','Kingston','<br>','319dab040f1ed6c76.jpg',NULL),(44,26,'NA','NA','<br>','75566e0e6e50b25e2.gif',NULL),(45,3,'21378A','PHILIPS','Sonda trasesofagica','ed6e962d9f505b765.jpg',NULL),(46,17,'M200','EPSON','<br>','7a4747319b8736f47.png',NULL),(47,17,'L3150','EPSON','<br>','f4700cdac1b796249.jpg',NULL);
/*!40000 ALTER TABLE `marca_modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcas` (
  `id_marca` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marca` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_marca`),
  UNIQUE KEY `marca_unique` (`marca`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (1,'DMS',NULL),(2,'CONTEC',NULL),(3,'PHILIPS',NULL),(4,'WELCH ALLYN',NULL),(5,'GENERAL ELECTRIC',NULL),(6,'SIMENS',NULL),(7,'ASUS',NULL),(8,'SUNTECH',NULL),(9,'PINNACLE',NULL),(10,'LENOVO','COMPUTADORES LENOVO'),(12,'SPORT FITNESS',NULL),(13,'ZOLL',NULL),(14,'PERFECT10',NULL),(15,'ACER',NULL),(16,'SIUI',NULL),(17,'EPSON',NULL),(18,'LG',NULL),(19,'SMAF',NULL),(20,'ZKTECO',NULL),(21,'NIPRO',NULL),(22,'MOTOROLA',NULL),(23,'ARTEMA',NULL),(24,'HP','HP PARA OFICINA'),(25,'KINGSTON','Memoria USB'),(26,'NA',NULL);
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_grouppermissions`
--

DROP TABLE IF EXISTS `membership_grouppermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_grouppermissions` (
  `permissionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupID` int(10) unsigned DEFAULT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `allowInsert` tinyint(4) NOT NULL DEFAULT '0',
  `allowView` tinyint(4) NOT NULL DEFAULT '0',
  `allowEdit` tinyint(4) NOT NULL DEFAULT '0',
  `allowDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permissionID`),
  UNIQUE KEY `groupID_tableName` (`groupID`,`tableName`)
) ENGINE=MyISAM AUTO_INCREMENT=496 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_grouppermissions`
--

LOCK TABLES `membership_grouppermissions` WRITE;
/*!40000 ALTER TABLE `membership_grouppermissions` DISABLE KEYS */;
INSERT INTO `membership_grouppermissions` VALUES (1,2,'dispositivos',1,3,3,3),(2,2,'compras',1,3,3,3),(3,2,'contactos',1,3,3,3),(4,2,'movimientos',1,3,3,3),(5,2,'documentos',1,3,3,3),(6,2,'mantenimientos',1,3,3,3),(7,2,'calibraciones',1,3,3,3),(8,2,'mobiliario',1,3,3,3),(9,2,'unidades',1,3,3,3),(10,2,'marcas',1,3,3,3),(11,2,'marca_modelo',1,3,3,3),(12,2,'accesorios',1,3,3,3),(13,2,'ciudades',1,3,3,3),(14,2,'tipo_relacion',1,3,3,3),(15,2,'tipo_dispositivo',1,3,3,3),(16,2,'tipo_documento',1,3,3,3),(17,2,'tipo_iden',1,3,3,3),(18,2,'tipo_razon_social',1,3,3,3),(19,2,'tipo_contacto',1,3,3,3),(20,2,'tipo_mobiliario',1,3,3,3),(21,2,'tipo_estado_dispo',1,3,3,3),(22,2,'grupo_dispo',1,3,3,3),(23,2,'tipo_movimiento',1,3,3,3),(24,2,'tipo_estado_movi',1,3,3,3),(25,2,'tipo_estado_verifica',1,3,3,3),(26,2,'tipo_mtto',1,3,3,3),(27,2,'tipo_calibracion',1,3,3,3),(28,2,'imagenes',1,3,3,3),(494,3,'tipo_calibracion',0,3,0,0),(493,3,'tipo_mtto',0,3,0,0),(492,3,'tipo_estado_verifica',0,3,0,0),(491,3,'tipo_estado_movi',0,3,0,0),(490,3,'tipo_movimiento',0,3,0,0),(489,3,'grupo_dispo',0,3,0,0),(488,3,'tipo_estado_dispo',1,3,0,0),(487,3,'tipo_mobiliario',0,3,0,0),(486,3,'tipo_contacto',0,3,0,0),(485,3,'tipo_razon_social',0,3,0,0),(484,3,'tipo_iden',0,3,0,0),(483,3,'tipo_documento',1,3,3,0),(482,3,'tipo_dispositivo',1,3,0,0),(481,3,'tipo_relacion',0,3,0,0),(480,3,'docu_modelo',0,0,0,0),(479,3,'fichatecnica',0,0,0,0),(478,3,'verificacion',1,0,0,0),(477,3,'imagenes',1,3,0,0),(476,3,'ciudades',0,3,0,0),(475,3,'accesorios',1,3,3,2),(474,3,'marca_modelo',1,3,0,0),(473,3,'marcas',1,3,0,0),(472,3,'unidades',0,3,0,1),(145,4,'documentos',1,3,2,2),(146,4,'mantenimientos',1,3,2,2),(147,4,'calibraciones',1,3,2,2),(148,4,'mobiliario',1,3,2,2),(149,4,'unidades',0,3,0,0),(150,4,'marcas',1,3,2,2),(151,4,'marca_modelo',1,3,2,2),(152,4,'accesorios',1,3,2,2),(153,4,'ciudades',1,3,2,2),(154,4,'tipo_relacion',0,3,0,0),(155,4,'tipo_dispositivo',0,3,0,0),(156,4,'tipo_documento',0,3,0,0),(157,4,'tipo_iden',0,0,0,0),(158,4,'tipo_razon_social',0,3,0,0),(159,4,'tipo_contacto',0,0,0,0),(160,4,'tipo_mobiliario',0,0,0,0),(161,4,'tipo_estado_dispo',0,3,0,0),(162,4,'grupo_dispo',0,0,0,0),(163,4,'tipo_movimiento',0,3,0,0),(144,4,'movimientos',1,3,2,2),(143,4,'contactos',1,3,2,2),(142,4,'compras',1,3,2,2),(141,4,'dispositivos',1,3,2,2),(164,4,'tipo_estado_movi',0,3,0,0),(165,4,'tipo_estado_verifica',0,3,0,0),(166,4,'tipo_mtto',0,3,0,0),(167,4,'tipo_calibracion',0,3,0,0),(168,4,'imagenes',1,3,2,2),(216,5,'tipo_mobiliario',1,2,2,2),(215,5,'tipo_contacto',1,2,2,2),(214,5,'tipo_razon_social',1,2,2,2),(213,5,'tipo_iden',1,2,2,2),(212,5,'tipo_documento',1,2,2,2),(211,5,'tipo_dispositivo',1,2,2,2),(210,5,'tipo_relacion',1,2,2,2),(209,5,'ciudades',1,2,2,2),(208,5,'accesorios',1,2,2,2),(207,5,'marca_modelo',1,2,2,2),(206,5,'marcas',1,2,2,2),(205,5,'unidades',1,2,2,2),(204,5,'mobiliario',1,2,2,2),(203,5,'calibraciones',1,2,2,2),(202,5,'mantenimientos',1,2,2,2),(201,5,'documentos',1,2,2,2),(200,5,'movimientos',1,2,2,2),(199,5,'contactos',1,2,2,2),(198,5,'compras',1,2,2,2),(197,5,'dispositivos',1,2,2,2),(217,5,'tipo_estado_dispo',1,2,2,2),(218,5,'grupo_dispo',1,2,2,2),(219,5,'tipo_movimiento',1,2,2,2),(220,5,'tipo_estado_movi',1,2,2,2),(221,5,'tipo_estado_verifica',1,2,2,2),(222,5,'tipo_mtto',1,2,2,2),(223,5,'tipo_calibracion',1,2,2,2),(224,5,'imagenes',1,2,2,2),(471,3,'calibraciones',1,3,3,1),(470,3,'mantenimientos',1,3,3,1),(469,3,'documentos',1,3,3,2),(467,3,'contactos',1,3,3,1),(468,3,'movimientos',1,3,3,2),(393,6,'tipo_calibracion',0,3,0,0),(392,6,'tipo_mtto',0,3,0,0),(391,6,'tipo_estado_verifica',0,3,0,0),(390,6,'tipo_estado_movi',0,3,0,0),(389,6,'tipo_movimiento',0,3,0,0),(388,6,'grupo_dispo',0,3,0,0),(387,6,'tipo_estado_dispo',0,3,0,0),(386,6,'tipo_mobiliario',0,3,0,0),(385,6,'tipo_contacto',0,3,0,0),(384,6,'tipo_razon_social',0,3,0,0),(383,6,'tipo_iden',0,3,0,0),(382,6,'tipo_documento',0,3,0,0),(381,6,'tipo_dispositivo',0,3,0,0),(380,6,'tipo_relacion',0,3,0,0),(379,6,'ciudades',0,3,0,0),(378,6,'accesorios',0,3,0,0),(377,6,'marca_modelo',0,3,0,0),(376,6,'marcas',0,3,0,0),(370,6,'movimientos',1,3,0,0),(371,6,'documentos',0,3,0,0),(372,6,'mantenimientos',0,3,0,0),(373,6,'calibraciones',0,3,0,0),(374,6,'mobiliario',0,3,0,0),(375,6,'unidades',0,3,0,0),(369,6,'contactos',0,3,0,0),(368,6,'compras',0,3,0,0),(367,6,'dispositivos',0,3,0,0),(365,2,'codigoserial',1,3,3,3),(366,2,'verificacion',1,3,3,3),(394,6,'imagenes',0,3,0,0),(395,6,'codigoserial',0,0,0,0),(396,6,'verificacion',1,3,3,3),(397,2,'tipo_grupo_mobilia',1,3,3,3),(398,2,'docu_modelo',1,3,3,3),(399,2,'fichatecnica',1,3,3,3),(466,3,'compras',1,3,3,1),(465,3,'mobiliario',1,3,3,1),(464,3,'dispositivos',1,3,0,0),(432,7,'dispositivos',0,3,0,0),(433,7,'mobiliario',0,0,0,0),(434,7,'compras',0,0,0,0),(435,7,'contactos',0,0,0,0),(436,7,'movimientos',0,0,0,0),(437,7,'documentos',1,3,3,3),(438,7,'mantenimientos',0,0,0,0),(439,7,'calibraciones',0,0,0,0),(440,7,'unidades',0,0,0,0),(441,7,'marcas',0,0,0,0),(442,7,'marca_modelo',0,0,0,0),(443,7,'accesorios',0,0,0,0),(444,7,'ciudades',0,0,0,0),(445,7,'imagenes',0,0,0,0),(446,7,'verificacion',0,0,0,0),(447,7,'fichatecnica',0,0,0,0),(448,7,'docu_modelo',0,0,0,0),(449,7,'tipo_relacion',0,0,0,0),(450,7,'tipo_dispositivo',0,0,0,0),(451,7,'tipo_documento',0,0,0,0),(452,7,'tipo_iden',0,0,0,0),(453,7,'tipo_razon_social',0,0,0,0),(454,7,'tipo_contacto',0,0,0,0),(455,7,'tipo_mobiliario',0,0,0,0),(456,7,'tipo_estado_dispo',0,0,0,0),(457,7,'grupo_dispo',0,0,0,0),(458,7,'tipo_movimiento',0,0,0,0),(459,7,'tipo_estado_movi',0,0,0,0),(460,7,'tipo_estado_verifica',0,0,0,0),(461,7,'tipo_mtto',0,0,0,0),(462,7,'tipo_calibracion',0,0,0,0),(463,7,'tipo_grupo_mobilia',0,0,0,0),(495,3,'tipo_grupo_mobilia',0,0,0,0);
/*!40000 ALTER TABLE `membership_grouppermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_groups`
--

DROP TABLE IF EXISTS `membership_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_groups` (
  `groupID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text,
  `allowSignup` tinyint(4) DEFAULT NULL,
  `needsApproval` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`groupID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_groups`
--

LOCK TABLES `membership_groups` WRITE;
/*!40000 ALTER TABLE `membership_groups` DISABLE KEYS */;
INSERT INTO `membership_groups` VALUES (1,'anonymous','Anonymous group created automatically on 2019-11-26',0,0),(2,'Admins','Admin group created automatically on 2019-11-26',0,1),(3,'APOYO EN TECNOLOGÍA','Apoyo en Auditoria y seguimiento de equipos',0,1),(4,'COMPRAS','Registros de compras',0,1),(5,'BODEGA','Bodega de insumos',0,1),(6,'ENFERMEROS','Apoyo administrativo del personal de enfermería.',1,1),(7,'INGENIEROS','Ingenieros Biomedicos',0,1);
/*!40000 ALTER TABLE `membership_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_userpermissions`
--

DROP TABLE IF EXISTS `membership_userpermissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_userpermissions` (
  `permissionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `memberID` varchar(100) NOT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `allowInsert` tinyint(4) NOT NULL DEFAULT '0',
  `allowView` tinyint(4) NOT NULL DEFAULT '0',
  `allowEdit` tinyint(4) NOT NULL DEFAULT '0',
  `allowDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permissionID`),
  UNIQUE KEY `memberID_tableName` (`memberID`,`tableName`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_userpermissions`
--

LOCK TABLES `membership_userpermissions` WRITE;
/*!40000 ALTER TABLE `membership_userpermissions` DISABLE KEYS */;
INSERT INTO `membership_userpermissions` VALUES (80,'camiloz','tipo_estado_movi',0,3,0,0),(79,'camiloz','tipo_movimiento',0,3,0,0),(78,'camiloz','grupo_dispo',0,3,0,0),(77,'camiloz','tipo_estado_dispo',0,3,0,0),(76,'camiloz','tipo_mobiliario',0,3,0,0),(75,'camiloz','tipo_contacto',0,3,0,0),(74,'camiloz','tipo_razon_social',0,3,0,0),(73,'camiloz','tipo_iden',0,3,0,0),(72,'camiloz','tipo_documento',1,3,3,0),(71,'camiloz','tipo_dispositivo',1,3,3,0),(70,'camiloz','tipo_relacion',0,3,0,0),(69,'camiloz','ciudades',1,3,3,1),(68,'camiloz','accesorios',1,3,3,2),(67,'camiloz','marca_modelo',1,3,3,3),(66,'camiloz','marcas',1,3,3,3),(65,'camiloz','unidades',1,3,3,1),(64,'camiloz','mobiliario',1,3,3,1),(63,'camiloz','calibraciones',1,3,3,1),(62,'camiloz','mantenimientos',1,3,3,1),(61,'camiloz','documentos',1,3,3,2),(60,'camiloz','movimientos',1,3,3,2),(59,'camiloz','contactos',1,3,3,1),(58,'camiloz','compras',1,3,3,1),(57,'camiloz','dispositivos',1,3,3,1),(81,'camiloz','tipo_estado_verifica',0,3,0,0),(82,'camiloz','tipo_mtto',0,3,0,0),(83,'camiloz','tipo_calibracion',0,3,0,0),(84,'camiloz','imagenes',0,3,0,0);
/*!40000 ALTER TABLE `membership_userpermissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_userrecords`
--

DROP TABLE IF EXISTS `membership_userrecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_userrecords` (
  `recID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tableName` varchar(100) DEFAULT NULL,
  `pkValue` varchar(255) DEFAULT NULL,
  `memberID` varchar(100) DEFAULT NULL,
  `dateAdded` bigint(20) unsigned DEFAULT NULL,
  `dateUpdated` bigint(20) unsigned DEFAULT NULL,
  `groupID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`recID`),
  UNIQUE KEY `tableName_pkValue` (`tableName`,`pkValue`(150)),
  KEY `pkValue` (`pkValue`),
  KEY `tableName` (`tableName`),
  KEY `memberID` (`memberID`),
  KEY `groupID` (`groupID`)
) ENGINE=MyISAM AUTO_INCREMENT=640 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_userrecords`
--

LOCK TABLES `membership_userrecords` WRITE;
/*!40000 ALTER TABLE `membership_userrecords` DISABLE KEYS */;
INSERT INTO `membership_userrecords` VALUES (1,'marcas','1','felipem',1574829510,1590176307,2),(2,'marcas','2','felipem',1574829531,1590176396,2),(3,'marcas','3','felipem',1574829584,1574829584,2),(4,'marca_modelo','1','felipem',1574829694,1577399542,2),(5,'marca_modelo','2','felipem',1574829745,1577399923,2),(6,'marcas','4','felipem',1574829808,1583334227,2),(7,'ciudades','1','felipem',1574830554,1574830554,2),(8,'ciudades','2','felipem',1574830609,1574830609,2),(9,'ciudades','3','felipem',1574830626,1574830626,2),(10,'ciudades','4','felipem',1574830643,1574830643,2),(11,'tipo_relacion','1','felipem',1574830693,1574830693,2),(12,'tipo_relacion','2','felipem',1574830752,1574830752,2),(13,'tipo_documento','1','felipem',1574830863,1574830863,2),(14,'tipo_documento','2','felipem',1574830895,1574830895,2),(15,'tipo_documento','3','felipem',1574830953,1574830953,2),(16,'tipo_documento','4','felipem',1574831031,1574831031,2),(17,'tipo_documento','5','felipem',1574831081,1574831081,2),(18,'tipo_iden','1','felipem',1574834160,1574834160,2),(19,'tipo_iden','2','felipem',1574834252,1574834252,2),(20,'tipo_razon_social','1','felipem',1574834321,1574834321,2),(21,'tipo_razon_social','2','felipem',1574834373,1574834373,2),(22,'tipo_razon_social','3','felipem',1574834447,1574834447,2),(23,'tipo_movimiento','1','felipem',1574834517,1574834517,2),(24,'tipo_movimiento','2','felipem',1574834562,1574834562,2),(25,'tipo_estado_movi','1','felipem',1574834603,1574834603,2),(26,'tipo_estado_movi','2','felipem',1574834630,1574834630,2),(27,'tipo_estado_verifica','1','felipem',1574834704,1574834704,2),(28,'tipo_estado_verifica','2','felipem',1574834759,1574834759,2),(29,'tipo_mtto','1','felipem',1574834807,1574834807,2),(30,'tipo_mtto','2','felipem',1574834842,1574834842,2),(31,'tipo_calibracion','1','felipem',1574834902,1574839002,2),(32,'tipo_documento','6','felipem',1574834955,1574834955,2),(33,'tipo_documento','7','felipem',1574834975,1574834975,2),(34,'tipo_documento','8','felipem',1574835018,1574835018,2),(35,'tipo_documento','9','felipem',1574835065,1574835065,2),(36,'unidades','1','felipem',1574835252,1574835542,2),(37,'unidades','2','felipem',1574835293,1574835592,2),(38,'unidades','3','felipem',1574835336,1574835797,2),(39,'unidades','4','felipem',1574835373,1574835698,2),(40,'unidades','5','felipem',1574835421,1574835906,2),(41,'unidades','6','felipem',1574835472,1574835645,2),(42,'tipo_relacion','3','felipem',1574838334,1574838334,2),(43,'grupo_dispo','1','felipem',1574838397,1574838397,2),(44,'grupo_dispo','2','felipem',1574838417,1574838417,2),(45,'grupo_dispo','3','felipem',1574838483,1574838483,2),(46,'tipo_dispositivo','1','felipem',1574838498,1602785992,2),(47,'tipo_dispositivo','2','felipem',1574838508,1602786002,2),(48,'tipo_dispositivo','3','felipem',1574838519,1602786011,2),(49,'tipo_dispositivo','4','felipem',1574838530,1602786210,2),(50,'tipo_dispositivo','5','felipem',1574838549,1602786033,2),(51,'tipo_dispositivo','6','felipem',1574838571,1602786040,2),(52,'tipo_dispositivo','7','felipem',1574838584,1589264571,2),(53,'tipo_dispositivo','8','felipem',1574838598,1589264632,2),(54,'tipo_dispositivo','9','felipem',1574838609,1589264626,2),(55,'tipo_dispositivo','10','felipem',1574838654,1602785911,2),(56,'tipo_contacto','1','felipem',1574838724,1574838724,2),(57,'tipo_contacto','2','felipem',1574838738,1574838738,2),(58,'tipo_mobiliario','1','felipem',1574838781,1574838781,2),(59,'tipo_mobiliario','2','felipem',1574838792,1574838792,2),(60,'tipo_estado_dispo','1','felipem',1574838831,1574838831,2),(61,'tipo_estado_dispo','2','felipem',1574838892,1574838892,2),(62,'tipo_calibracion','2','felipem',1574839025,1574839025,2),(63,'imagenes','1','felipem',1574849750,1574849750,2),(64,'tipo_dispositivo','11','felipem',1574864502,1602781981,2),(581,'dispositivos','239','felipem',1590172053,1590172053,2),(66,'dispositivos','1','felipem',1574865457,1589206081,2),(67,'unidades','7','felipem',1574865611,1574865611,2),(68,'dispositivos','2','felipem',1574865639,1589206206,2),(69,'dispositivos','3','felipem',1574866234,1589206360,2),(70,'dispositivos','4','felipem',1574866234,1589206400,2),(71,'dispositivos','5','felipem',1574866234,1589206439,2),(72,'dispositivos','6','felipem',1574866234,1589206460,2),(73,'dispositivos','7','felipem',1574866234,1589206483,2),(74,'dispositivos','8','felipem',1574866234,1589206500,2),(75,'dispositivos','9','felipem',1574866234,1599774926,2),(76,'dispositivos','10','felipem',1574866234,1589206548,2),(77,'dispositivos','11','felipem',1574866234,1589206564,2),(78,'dispositivos','12','felipem',1574866234,1589206583,2),(79,'dispositivos','13','felipem',1574866234,1589206600,2),(80,'dispositivos','14','felipem',1574866234,1589206616,2),(81,'dispositivos','15','felipem',1574866234,1577465988,2),(82,'dispositivos','16','felipem',1574866234,1589206668,2),(83,'dispositivos','17','felipem',1574866234,1589206687,2),(84,'dispositivos','18','felipem',1574866234,1589206704,2),(85,'dispositivos','19','felipem',1574866234,1589206983,2),(86,'dispositivos','20','felipem',1574866234,1577445062,2),(87,'dispositivos','21','felipem',1574866234,1583518343,2),(88,'dispositivos','22','felipem',1574866234,1583521444,2),(89,'dispositivos','23','felipem',1574866234,1583783145,2),(90,'dispositivos','24','felipem',1574866234,1577445994,2),(91,'dispositivos','25','felipem',1574866234,1583521519,2),(92,'dispositivos','26','felipem',1574866234,1599773611,2),(93,'dispositivos','27','felipem',1574866234,1583528275,2),(94,'dispositivos','28','felipem',1574866234,1583528887,2),(95,'dispositivos','29','felipem',1574866234,1574868549,2),(141,'unidades','9','felipem',1575063919,1575063919,2),(97,'marca_modelo','3','felipem',1574866475,1577399943,2),(98,'marcas','5','felipem',1574866578,1574866578,2),(99,'marca_modelo','4','felipem',1574866600,1577400000,2),(100,'marcas','6','felipem',1574866615,1574866615,2),(101,'marca_modelo','5','felipem',1574866630,1577400114,2),(102,'dispositivos','31','felipem',1574867923,1589207091,2),(103,'dispositivos','32','felipem',1574867923,1575007909,2),(104,'dispositivos','33','felipem',1574867923,1575007952,2),(105,'dispositivos','34','felipem',1574867923,1584809056,2),(106,'dispositivos','35','felipem',1574867923,1575008392,2),(107,'dispositivos','36','felipem',1574867923,1583464207,2),(108,'dispositivos','37','felipem',1574867923,1575008641,2),(109,'dispositivos','38','felipem',1574867923,1575008669,2),(110,'dispositivos','39','felipem',1574867923,1575008656,2),(111,'dispositivos','40','felipem',1574867923,1584811699,2),(112,'dispositivos','41','felipem',1574867923,1575008466,2),(113,'dispositivos','42','felipem',1574867923,1583180666,2),(114,'dispositivos','43','felipem',1574867923,1575008554,2),(115,'dispositivos','44','felipem',1574867923,1582992429,2),(116,'dispositivos','45','felipem',1574867923,1575008527,2),(117,'dispositivos','46','felipem',1574867923,1583365535,2),(118,'dispositivos','47','felipem',1574867923,1575008577,2),(119,'dispositivos','48','felipem',1574867923,1584484396,2),(120,'dispositivos','49','felipem',1574867923,1583029878,2),(121,'dispositivos','50','felipem',1574867923,1575390478,2),(122,'dispositivos','51','felipem',1574867923,1584808984,2),(123,'dispositivos','52','felipem',1574867923,1583464771,2),(124,'dispositivos','53','felipem',1574867923,1583526201,2),(125,'dispositivos','54','felipem',1574867923,1575008807,2),(126,'dispositivos','55','felipem',1574867923,1575522391,2),(127,'imagenes','2','felipem',1574868316,1574868316,2),(128,'ciudades','5','felipem',1574869823,1574869823,2),(129,'ciudades','6','felipem',1574869842,1574869842,2),(130,'ciudades','7','felipem',1574869849,1574869849,2),(131,'marcas','7','felipem',1574895268,1574895268,2),(132,'marca_modelo','6','felipem',1574895347,1577400170,2),(133,'unidades','8','vilmao',1574897330,1574897330,3),(134,'dispositivos','56','vilmao',1574897403,1590169117,3),(135,'contactos','1','vilmao',1574897672,1574897672,3),(582,'marca_modelo','36','felipem',1590173659,1590173659,2),(137,'contactos','2','vilmao',1574898334,1574898334,3),(138,'movimientos','1','vilmao',1574898368,1574898610,3),(139,'movimientos','2','vilmao',1574898574,1574898574,3),(140,'documentos','1','vilmao',1574898656,1574898656,3),(142,'dispositivos','57','felipem',1575063935,1575604964,2),(143,'movimientos','3','felipem',1575064319,1575064476,2),(144,'contactos','3','felipem',1575064426,1575604417,2),(145,'dispositivos','58','felipem',1575065168,1575605000,2),(146,'movimientos','4','felipem',1575065215,1575065215,2),(147,'dispositivos','59','felipem',1575351512,1575605047,2),(148,'dispositivos','60','felipem',1575351663,1575605068,2),(149,'unidades','10','felipem',1575351742,1575351742,2),(150,'movimientos','5','felipem',1575351866,1575398311,2),(151,'movimientos','6','felipem',1575351956,1575398354,2),(152,'marca_modelo','7','felipem',1575377974,1577401079,2),(153,'marcas','8','felipem',1575378007,1575378007,2),(154,'marca_modelo','8','felipem',1575378054,1577401155,2),(155,'dispositivos','61','felipem',1575378241,1575522095,2),(156,'dispositivos','62','felipem',1575378241,1575521959,2),(157,'dispositivos','63','felipem',1575378241,1575378241,2),(158,'dispositivos','64','felipem',1575378241,1577468905,2),(159,'dispositivos','65','felipem',1575378241,1583365243,2),(160,'dispositivos','66','felipem',1575378241,1575378241,2),(161,'dispositivos','67','felipem',1575378241,1583522721,2),(162,'dispositivos','68','felipem',1575378241,1575378241,2),(163,'dispositivos','69','felipem',1575378241,1575556865,2),(164,'dispositivos','70','felipem',1575378241,1584483326,2),(165,'dispositivos','71','felipem',1575378241,1575378241,2),(166,'dispositivos','72','felipem',1575378241,1584483466,2),(167,'dispositivos','73','felipem',1575378241,1575378241,2),(168,'dispositivos','74','felipem',1575378241,1575378241,2),(169,'dispositivos','75','felipem',1575378241,1575378241,2),(170,'dispositivos','76','felipem',1575378241,1575378241,2),(171,'dispositivos','77','felipem',1575378241,1575378241,2),(172,'dispositivos','78','felipem',1575378241,1577469046,2),(173,'dispositivos','79','felipem',1575378241,1575378241,2),(174,'dispositivos','80','felipem',1575378241,1583029688,2),(175,'dispositivos','81','felipem',1575378241,1575378241,2),(176,'dispositivos','82','felipem',1575378241,1583029423,2),(177,'dispositivos','83','felipem',1575378241,1575378241,2),(178,'dispositivos','84','felipem',1575378241,1602189176,2),(179,'dispositivos','85','felipem',1575378241,1577464585,2),(180,'dispositivos','86','felipem',1575378241,1575378241,2),(181,'dispositivos','87','felipem',1575378241,1583036410,2),(182,'dispositivos','88','felipem',1575378241,1577469348,2),(183,'dispositivos','89','felipem',1575378241,1575378241,2),(184,'dispositivos','90','felipem',1575378241,1577469227,2),(185,'dispositivos','91','felipem',1575378241,1602188137,2),(186,'dispositivos','92','felipem',1575378241,1577469395,2),(187,'dispositivos','93','felipem',1575378241,1577469369,2),(188,'dispositivos','94','felipem',1575378241,1577464549,2),(189,'dispositivos','95','felipem',1575378241,1575378241,2),(190,'dispositivos','96','felipem',1575378241,1575378241,2),(191,'dispositivos','97','felipem',1575378241,1575378241,2),(192,'dispositivos','98','felipem',1575378241,1583365075,2),(193,'tipo_estado_dispo','3','felipem',1575378727,1575378727,2),(194,'movimientos','7','felipem',1575389101,1583364723,2),(195,'movimientos','8','felipem',1575389208,1575389208,2),(196,'movimientos','9','felipem',1575389401,1575389401,2),(197,'movimientos','10','felipem',1575389413,1575389442,2),(198,'movimientos','11','felipem',1575389518,1575389518,2),(199,'movimientos','12','felipem',1575389522,1575389599,2),(200,'movimientos','13','felipem',1575389687,1575389687,2),(201,'movimientos','14','felipem',1575389692,1575389721,2),(202,'movimientos','15','felipem',1575389892,1575389923,2),(203,'movimientos','16','felipem',1575390057,1575390133,2),(204,'movimientos','17','felipem',1575390067,1575390067,2),(205,'movimientos','18','felipem',1575390402,1575390402,2),(206,'movimientos','19','felipem',1575390415,1575390455,2),(207,'movimientos','20','felipem',1575398679,1575398679,2),(208,'movimientos','21','felipem',1575398690,1575398714,2),(209,'movimientos','22','felipem',1575398751,1575398751,2),(210,'movimientos','23','felipem',1575398754,1575398780,2),(211,'dispositivos','99','felipem',1575556995,1582990908,2),(212,'dispositivos','100','felipem',1575556995,1582990926,2),(213,'dispositivos','101','felipem',1575556995,1583273854,2),(214,'dispositivos','102','felipem',1575556995,1582990980,2),(215,'dispositivos','103','felipem',1575556995,1582991012,2),(216,'dispositivos','104','felipem',1575556995,1589261513,2),(217,'dispositivos','105','felipem',1575556995,1588870908,2),(218,'dispositivos','106','felipem',1575556995,1582754637,2),(219,'dispositivos','107','felipem',1575556995,1583030323,2),(220,'dispositivos','108','felipem',1575556995,1583272369,2),(221,'dispositivos','109','felipem',1575556995,1582768002,2),(222,'dispositivos','110','felipem',1575556995,1582768144,2),(223,'dispositivos','111','felipem',1575556995,1583512906,2),(224,'dispositivos','112','felipem',1575556995,1583030334,2),(225,'dispositivos','113','felipem',1575556995,1583030341,2),(226,'dispositivos','114','felipem',1575556995,1582768699,2),(227,'dispositivos','115','felipem',1575556995,1583030348,2),(228,'dispositivos','116','felipem',1575556995,1583030354,2),(229,'dispositivos','117','felipem',1575556995,1582768248,2),(230,'dispositivos','118','felipem',1575556995,1583030361,2),(231,'dispositivos','119','felipem',1575556995,1588806543,2),(232,'dispositivos','120','felipem',1575556995,1583035955,2),(233,'dispositivos','121','felipem',1575556995,1583909660,2),(234,'dispositivos','122','felipem',1575556995,1602784321,2),(235,'dispositivos','123','felipem',1575556995,1584477777,2),(236,'dispositivos','124','felipem',1575556995,1582995624,2),(237,'dispositivos','125','felipem',1575556995,1602784499,2),(238,'dispositivos','126','felipem',1575556995,1582990239,2),(239,'dispositivos','127','felipem',1575556995,1582082276,2),(240,'dispositivos','128','felipem',1575556995,1582237660,2),(241,'dispositivos','129','felipem',1575556995,1583506623,2),(242,'dispositivos','130','felipem',1575556995,1584809184,2),(243,'dispositivos','131','felipem',1575556995,1584810038,2),(244,'dispositivos','132','felipem',1575556995,1582082451,2),(245,'dispositivos','133','felipem',1575556995,1583333573,2),(246,'dispositivos','134','felipem',1575556995,1594168095,2),(247,'dispositivos','135','felipem',1575556995,1582769298,2),(248,'dispositivos','136','felipem',1575556995,1583334407,2),(249,'dispositivos','137','felipem',1575556995,1583333593,2),(250,'dispositivos','138','felipem',1575556995,1589205841,2),(251,'dispositivos','139','felipem',1575556995,1594167792,2),(252,'dispositivos','140','felipem',1575556995,1583504409,2),(253,'dispositivos','141','felipem',1575556995,1582991548,2),(254,'dispositivos','142','felipem',1575556995,1582991567,2),(255,'dispositivos','143','felipem',1575556995,1582991602,2),(256,'dispositivos','144','felipem',1575556995,1582991619,2),(257,'dispositivos','145','felipem',1575556995,1582991630,2),(258,'dispositivos','146','felipem',1575556995,1582991657,2),(259,'dispositivos','147','felipem',1575556995,1583036758,2),(260,'dispositivos','148','felipem',1575556995,1583036775,2),(261,'dispositivos','149','felipem',1575556995,1583036784,2),(262,'dispositivos','150','felipem',1575556995,1582991907,2),(263,'dispositivos','151','felipem',1575556995,1583036792,2),(264,'dispositivos','152','felipem',1575556995,1602782219,2),(265,'dispositivos','153','felipem',1575556995,1583334726,2),(266,'dispositivos','154','felipem',1575556995,1583036920,2),(267,'dispositivos','155','felipem',1575556995,1583036938,2),(268,'dispositivos','156','felipem',1575556995,1583335129,2),(269,'dispositivos','157','felipem',1575556995,1583037045,2),(270,'dispositivos','158','felipem',1575556995,1583948569,2),(271,'dispositivos','159','felipem',1575556995,1584478256,2),(272,'dispositivos','160','felipem',1575556995,1583036159,2),(273,'dispositivos','161','felipem',1575556995,1583036170,2),(274,'dispositivos','162','felipem',1575556995,1583333338,2),(275,'dispositivos','163','felipem',1575556995,1583182115,2),(276,'dispositivos','164','felipem',1575556995,1583036195,2),(277,'dispositivos','165','felipem',1575556995,1583036210,2),(278,'dispositivos','166','felipem',1575556995,1602784163,2),(279,'dispositivos','167','felipem',1575556995,1583030084,2),(280,'dispositivos','168','felipem',1575556995,1583030096,2),(281,'dispositivos','169','felipem',1575556995,1583030103,2),(282,'dispositivos','170','felipem',1575556995,1583182456,2),(283,'dispositivos','171','felipem',1575556995,1583030221,2),(284,'dispositivos','172','felipem',1575556995,1583030012,2),(285,'dispositivos','173','felipem',1575556995,1583030125,2),(286,'dispositivos','174','felipem',1575556995,1583030141,2),(287,'dispositivos','175','felipem',1575556995,1583030151,2),(288,'dispositivos','176','felipem',1575556995,1583030161,2),(289,'dispositivos','177','felipem',1575556995,1583030168,2),(290,'dispositivos','178','felipem',1575556995,1583030177,2),(291,'dispositivos','179','felipem',1575556995,1588885767,2),(292,'dispositivos','180','felipem',1575556995,1588885773,2),(293,'dispositivos','181','felipem',1575556995,1575556995,2),(294,'dispositivos','182','felipem',1575556995,1588944731,2),(295,'dispositivos','183','felipem',1575556995,1602785347,2),(296,'dispositivos','184','felipem',1575556995,1602785425,2),(297,'dispositivos','185','felipem',1575556995,1602785523,2),(298,'dispositivos','186','felipem',1575556995,1602785550,2),(299,'dispositivos','187','felipem',1575556995,1602785584,2),(300,'dispositivos','188','felipem',1575556995,1602785671,2),(301,'dispositivos','189','felipem',1575556995,1602785703,2),(302,'dispositivos','190','felipem',1575556995,1602785728,2),(303,'dispositivos','191','felipem',1575556995,1602785759,2),(304,'dispositivos','192','felipem',1575556995,1602785305,2),(305,'dispositivos','193','felipem',1575556995,1602785262,2),(306,'dispositivos','194','felipem',1575556995,1602785199,2),(307,'dispositivos','195','felipem',1575556995,1602785157,2),(308,'dispositivos','196','felipem',1575556995,1602785109,2),(309,'dispositivos','197','felipem',1575556995,1602785069,2),(310,'dispositivos','198','felipem',1575556995,1602785019,2),(311,'dispositivos','199','felipem',1575604788,1602186379,2),(312,'dispositivos','200','felipem',1575604876,1577463352,2),(313,'dispositivos','201','felipem',1575605173,1577462817,2),(314,'dispositivos','202','felipem',1575605274,1576021448,2),(315,'dispositivos','203','felipem',1575605329,1576021294,2),(316,'dispositivos','204','felipem',1575605380,1583528044,2),(317,'tipo_dispositivo','13','felipem',1575608022,1602785839,2),(318,'tipo_dispositivo','14','felipem',1575608065,1589264613,2),(319,'tipo_dispositivo','15','felipem',1575608099,1602785845,2),(320,'tipo_dispositivo','16','felipem',1575608195,1602786124,2),(321,'tipo_dispositivo','17','felipem',1575608209,1602786172,2),(322,'marcas','9','felipem',1575955734,1575955804,2),(323,'marca_modelo','9','felipem',1575955782,1577400361,2),(324,'imagenes','3','felipem',1575956131,1575956131,2),(325,'marca_modelo','10','felipem',1575989158,1577400316,2),(326,'imagenes','4','felipem',1576011693,1576011693,2),(327,'marcas','10','felipem',1576012278,1590176273,2),(328,'marca_modelo','11','felipem',1576012373,1577402451,2),(329,'imagenes','5','felipem',1576012597,1576012597,2),(330,'movimientos','24','felipem',1576021182,1576021182,2),(331,'movimientos','25','felipem',1576021189,1576021223,2),(332,'movimientos','26','felipem',1576021351,1576021351,2),(333,'movimientos','27','felipem',1576021357,1576021393,2),(334,'movimientos','28','felipem',1576021488,1576021488,2),(335,'movimientos','29','felipem',1576021568,1576021607,2),(336,'tipo_iden','3','felipem',1576021965,1576021965,2),(337,'contactos','4','felipem',1576022106,1576022121,2),(338,'compras','2','felipem',1576022268,1577402575,2),(339,'compras','3','felipem',1576022277,1576022304,2),(340,'marca_modelo','12','felipem',1577400398,1577400940,2),(341,'dispositivos','205','bodega.felipe',1577458485,1577458485,5),(342,'movimientos','30','felipem',1577462666,1577462666,2),(343,'movimientos','31','felipem',1577462670,1577462702,2),(344,'movimientos','32','felipem',1577462855,1577462855,2),(345,'movimientos','33','felipem',1577462859,1577462889,2),(346,'movimientos','34','felipem',1577462994,1577462994,2),(347,'movimientos','35','felipem',1577463010,1577463027,2),(348,'movimientos','36','felipem',1577469270,1577469270,2),(349,'movimientos','37','felipem',1577469277,1577469296,2),(350,'movimientos','38','felipem',1577469582,1577469582,2),(351,'dispositivos','206','felipem',1577473118,1577474423,2),(352,'dispositivos','207','felipem',1577473277,1577473277,2),(353,'dispositivos','208','felipem',1577474118,1602187134,2),(354,'dispositivos','209','felipem',1577474118,1583522198,2),(355,'dispositivos','210','felipem',1577474118,1577474118,2),(356,'dispositivos','211','felipem',1577474118,1577474118,2),(357,'dispositivos','212','felipem',1577474118,1577474118,2),(358,'dispositivos','213','felipem',1577474118,1577474118,2),(359,'dispositivos','214','felipem',1577474118,1577474118,2),(360,'dispositivos','215','felipem',1577474118,1577474118,2),(361,'marca_modelo','13','felipem',1577761250,1577761250,2),(362,'marca_modelo','14','felipem',1577761291,1577761291,2),(363,'dispositivos','216','felipem',1580832093,1580832093,2),(364,'dispositivos','217','felipem',1580832143,1580832159,2),(365,'dispositivos','218','felipem',1580832208,1580832208,2),(366,'dispositivos','219','felipem',1580832278,1580832278,2),(367,'dispositivos','220','felipem',1580832480,1580832480,2),(368,'movimientos','39','felipem',1580832555,1580832555,2),(369,'movimientos','40','felipem',1580832560,1580832572,2),(370,'movimientos','41','felipem',1580832576,1580832583,2),(371,'movimientos','42','felipem',1580832587,1580832606,2),(372,'movimientos','43','felipem',1580832608,1580832629,2),(373,'dispositivos','221','felipem',1582082515,1583507404,2),(374,'dispositivos','222','felipem',1582082626,1582082626,2),(375,'marca_modelo','15','felipem',1582131046,1582131114,2),(376,'unidades','11','felipem',1582131201,1582131201,2),(377,'dispositivos','223','felipem',1582131249,1582131249,2),(378,'compras','4','felipem',1582131429,1582131429,2),(379,'movimientos','44','felipem',1582132200,1582132200,2),(380,'mantenimientos','1','felipem',1582132389,1582132389,2),(381,'movimientos','45','felipem',1582231965,1582231965,2),(382,'marca_modelo','16','felipem',1582237925,1582238033,2),(383,'marca_modelo','17','felipem',1582238255,1582754378,2),(384,'dispositivos','224','felipem',1582238512,1583338032,2),(386,'dispositivos','225','camiloz',1582755215,1583507057,3),(387,'marcas','12','camiloz',1582757737,1582757743,3),(388,'marca_modelo','18','camiloz',1582758125,1582758125,3),(389,'marca_modelo','19','camiloz',1582763298,1582763347,3),(390,'marca_modelo','20','camiloz',1582767359,1582767382,3),(391,'marcas','13','camiloz',1582769072,1582769075,3),(392,'marca_modelo','21','camiloz',1582769242,1582769248,3),(393,'marcas','14','camiloz',1582769445,1582769448,3),(394,'marca_modelo','22','camiloz',1582772003,1584478372,3),(395,'dispositivos','226','camiloz',1582989352,1582989352,3),(396,'contactos','5','camiloz',1582989584,1582989584,3),(397,'movimientos','46','camiloz',1582989688,1582989688,3),(398,'movimientos','47','camiloz',1582989773,1582989773,3),(399,'movimientos','48','camiloz',1582990170,1582990170,3),(400,'dispositivos','227','camiloz',1582990338,1582990541,3),(401,'movimientos','49','camiloz',1582990415,1582990707,3),(402,'marcas','15','camiloz',1582991305,1582991305,3),(403,'marca_modelo','23','camiloz',1582991379,1582991379,3),(404,'movimientos','50','camiloz',1582994275,1583035939,3),(405,'movimientos','51','camiloz',1582994526,1582994535,3),(406,'movimientos','52','camiloz',1582994981,1582994981,3),(407,'movimientos','53','camiloz',1582995585,1582995585,3),(408,'movimientos','54','camiloz',1583024440,1583029648,3),(413,'movimientos','59','camiloz',1583029865,1583029869,3),(410,'movimientos','56','camiloz',1583025135,1583025135,3),(414,'movimientos','60','camiloz',1583180592,1583180604,3),(415,'movimientos','61','camiloz',1583180902,1583180916,3),(416,'movimientos','62','camiloz',1583181944,1583181944,3),(417,'movimientos','63','camiloz',1583182372,1583182388,3),(418,'movimientos','64','camiloz',1583200265,1583200991,3),(419,'movimientos','65','camiloz',1583200747,1583200747,3),(420,'movimientos','66','camiloz',1583201372,1583201372,3),(421,'movimientos','67','camiloz',1583201509,1583201509,3),(422,'movimientos','68','camiloz',1583201860,1583201860,3),(423,'movimientos','69','camiloz',1583201973,1583201983,3),(424,'movimientos','70','camiloz',1583202582,1583202582,3),(425,'movimientos','71','camiloz',1583202831,1583202890,3),(426,'marcas','16','camiloz',1583241334,1583241334,3),(427,'marca_modelo','24','camiloz',1583269790,1583272304,3),(428,'movimientos','72','camiloz',1583271547,1583271547,3),(429,'marca_modelo','25','camiloz',1583273673,1583273696,3),(430,'movimientos','73','camiloz',1583273788,1583273788,3),(431,'marca_modelo','26','camiloz',1583289906,1583289909,3),(432,'movimientos','74','camiloz',1583290210,1583290210,3),(433,'movimientos','75','camiloz',1583290406,1583290414,3),(434,'movimientos','76','camiloz',1583333311,1583333311,3),(435,'movimientos','77','camiloz',1583333455,1583333455,3),(436,'marca_modelo','27','camiloz',1583334203,1583334203,3),(437,'movimientos','78','camiloz',1583334401,1583334401,3),(438,'movimientos','79','camiloz',1583335083,1583335083,3),(439,'dispositivos','228','felipem',1583338140,1583338497,2),(440,'movimientos','80','felipem',1583338399,1583338399,2),(441,'movimientos','81','camiloz',1583363529,1583363532,3),(442,'movimientos','82','camiloz',1583363799,1583363799,3),(443,'movimientos','83','camiloz',1583363979,1583363979,3),(444,'movimientos','84','camiloz',1583364574,1583364574,3),(445,'movimientos','85','camiloz',1583364654,1583364654,3),(446,'movimientos','86','camiloz',1583364901,1583364904,3),(447,'movimientos','87','camiloz',1583365060,1583365063,3),(448,'movimientos','88','camiloz',1583365233,1583365237,3),(449,'movimientos','89','camiloz',1583365526,1583365529,3),(450,'marcas','17','felipem',1583424616,1583424616,2),(451,'marca_modelo','28','felipem',1583424817,1583424817,2),(452,'dispositivos','229','felipem',1583425009,1583425009,2),(453,'movimientos','90','camiloz',1583464281,1583464281,3),(454,'movimientos','91','camiloz',1583464371,1583464386,3),(455,'movimientos','92','camiloz',1583464667,1583464667,3),(456,'dispositivos','230','camiloz',1583503067,1583503380,3),(457,'movimientos','93','camiloz',1583503149,1583503432,3),(458,'movimientos','94','camiloz',1583504271,1583504271,3),(459,'movimientos','95','camiloz',1583505115,1583505159,3),(460,'movimientos','96','camiloz',1583505629,1583505629,3),(461,'movimientos','97','camiloz',1583506591,1583506609,3),(462,'movimientos','98','camiloz',1583506779,1583506779,3),(463,'movimientos','99','camiloz',1583507355,1583507355,3),(464,'marcas','18','camiloz',1583511521,1583511521,3),(465,'marca_modelo','29','camiloz',1583511769,1583511769,3),(466,'dispositivos','231','camiloz',1583511822,1583511822,3),(467,'movimientos','100','camiloz',1583512768,1583512768,3),(468,'tipo_dispositivo','18','camiloz',1583513874,1601327255,3),(469,'marcas','19','camiloz',1583514014,1583514014,3),(470,'marca_modelo','30','camiloz',1583514233,1583514441,3),(471,'dispositivos','232','camiloz',1583514929,1583515025,3),(472,'movimientos','101','camiloz',1583514995,1583514995,3),(473,'tipo_dispositivo','19','camiloz',1583515183,1589264656,3),(474,'tipo_dispositivo','20','camiloz',1583515824,1589264421,3),(475,'marcas','20','camiloz',1583515895,1583516330,3),(476,'marca_modelo','31','camiloz',1583516351,1583516351,3),(477,'dispositivos','233','camiloz',1583516416,1589261712,3),(478,'marcas','21','camiloz',1583516693,1583516693,3),(479,'marca_modelo','32','camiloz',1583517016,1583517016,3),(480,'dispositivos','234','camiloz',1583517156,1583517270,3),(481,'movimientos','102','camiloz',1583517250,1583517250,3),(482,'movimientos','103','camiloz',1583518338,1583518338,3),(483,'movimientos','104','camiloz',1583522068,1583522068,3),(484,'movimientos','105','camiloz',1583522149,1583522149,3),(485,'movimientos','106','camiloz',1583522452,1583522452,3),(486,'movimientos','107','camiloz',1583526034,1583526060,3),(487,'movimientos','108','camiloz',1583526136,1583526136,3),(488,'movimientos','109','camiloz',1583526739,1583526739,3),(489,'movimientos','110','camiloz',1583528255,1583528255,3),(490,'movimientos','111','camiloz',1583528862,1583528862,3),(491,'movimientos','112','camiloz',1583529201,1583529201,3),(492,'movimientos','113','camiloz',1583529301,1583529301,3),(493,'movimientos','114','camiloz',1583948451,1583948451,3),(494,'movimientos','115','camiloz',1583948540,1583948540,3),(495,'movimientos','116','camiloz',1584378707,1584378713,3),(496,'movimientos','117','camiloz',1584467133,1584467155,3),(497,'movimientos','118','camiloz',1584477747,1584477754,3),(498,'movimientos','119','camiloz',1584478236,1584478236,3),(499,'movimientos','120','camiloz',1584482638,1584482642,3),(500,'movimientos','121','camiloz',1584483314,1584483320,3),(501,'movimientos','122','camiloz',1584483453,1584483453,3),(502,'movimientos','123','camiloz',1584484360,1584484360,3),(503,'movimientos','124','camiloz',1584484547,1584484547,3),(504,'movimientos','125','camiloz',1584809503,1584809759,3),(505,'movimientos','126','camiloz',1584809603,1584809743,3),(506,'dispositivos','235','camiloz',1584810927,1589261631,3),(507,'movimientos','127','camiloz',1584810976,1584811014,3),(508,'movimientos','128','camiloz',1584811554,1584811554,3),(509,'verificacion','1','felipem',1588196147,1588196147,2),(510,'verificacion','2','crueda',1588274706,1588274706,6),(511,'verificacion','3','crueda',1588274745,1588274745,6),(512,'verificacion','4','crueda',1588274771,1588274771,6),(513,'verificacion','5','crueda',1588274812,1588274812,6),(514,'verificacion','6','hrestrepo',1588276519,1588276519,6),(515,'verificacion','7','hrestrepo',1588276561,1588276561,6),(516,'verificacion','8','hrestrepo',1588276643,1588276643,6),(517,'verificacion','9','hrestrepo',1588276811,1588276811,6),(518,'verificacion','10','hrestrepo',1588276868,1588276868,6),(519,'verificacion','11','hrestrepo',1588277267,1588277267,6),(520,'verificacion','12','hrestrepo',1588277305,1588277305,6),(521,'verificacion','13','hrestrepo',1588277384,1588277384,6),(522,'verificacion','14','hrestrepo',1588277433,1588277433,6),(523,'verificacion','15','hrestrepo',1588277496,1588277496,6),(524,'verificacion','16','hrestrepo',1588277719,1588277719,6),(525,'verificacion','17','crueda',1588277721,1588277721,6),(526,'verificacion','18','hrestrepo',1588277756,1588277756,6),(527,'verificacion','19','crueda',1588277772,1588277772,6),(528,'verificacion','20','crueda',1588277846,1588277846,6),(529,'verificacion','21','hrestrepo',1588277938,1588277938,6),(530,'verificacion','22','crueda',1588277955,1588277955,6),(531,'verificacion','23','hrestrepo',1588278098,1588278098,6),(532,'verificacion','24','hrestrepo',1588278307,1588278307,6),(533,'verificacion','25','crueda',1588278674,1588278770,6),(534,'verificacion','26','hrestrepo',1588278879,1588278879,6),(535,'verificacion','27','jvilla',1588429793,1588429793,6),(536,'verificacion','28','jvilla',1588430315,1588430315,6),(537,'verificacion','29','jvilla',1588430354,1588430354,6),(538,'verificacion','30','jvilla',1588430400,1588430400,6),(539,'verificacion','31','jvilla',1588430532,1588430532,6),(540,'verificacion','32','jvilla',1588431259,1588431259,6),(541,'verificacion','33','jvilla',1588431305,1588431305,6),(542,'verificacion','34','jvilla',1588431444,1588431444,6),(543,'verificacion','35','jvilla',1588431471,1588431471,6),(544,'verificacion','36','jvilla',1588432813,1588432813,6),(545,'verificacion','37','jvilla',1588433135,1588433135,6),(546,'verificacion','38','jvilla',1588433227,1588433227,6),(547,'verificacion','39','jvilla',1588433271,1588433271,6),(548,'verificacion','40','jvilla',1588433298,1588433298,6),(549,'verificacion','41','jvilla',1588433374,1588433374,6),(550,'verificacion','42','jvilla',1588433419,1588433419,6),(551,'movimientos','129','felipem',1588806157,1588806157,2),(552,'unidades','12','felipem',1588806324,1588806324,2),(553,'movimientos','130','felipem',1588806419,1588806422,2),(554,'movimientos','131','felipem',1588806528,1588806528,2),(555,'marca_modelo','33','felipem',1588807139,1588807139,2),(556,'dispositivos','236','felipem',1588807171,1588807315,2),(557,'movimientos','132','felipem',1588807223,1588807223,2),(558,'movimientos','133','felipem',1588807477,1588807570,2),(559,'dispositivos','237','felipem',1588870652,1588870723,2),(560,'movimientos','134','felipem',1588870714,1588870714,2),(561,'movimientos','135','felipem',1588870899,1588870899,2),(562,'tipo_dispositivo','21','felipem',1588885347,1602781202,2),(563,'marcas','22','felipem',1588885386,1588885386,2),(564,'marca_modelo','34','felipem',1588885612,1588885612,2),(565,'marcas','23','felipem',1588940327,1588940327,2),(566,'marca_modelo','35','felipem',1588940513,1594167340,2),(567,'verificacion','43','ibet',1588945435,1588945435,6),(568,'verificacion','44','ibet',1588945506,1588945506,6),(569,'verificacion','45','ibet',1588945922,1588945922,6),(570,'dispositivos','238','felipem',1588946063,1588946063,2),(571,'verificacion','46','felipem',1588946088,1588946088,2),(572,'verificacion','47','ibet',1588946203,1588946203,6),(573,'verificacion','48','ibet',1588946424,1588946424,6),(574,'verificacion','49','ibet',1588946894,1588946894,6),(575,'verificacion','50','ibet',1588947008,1588947008,6),(576,'verificacion','51','ibet',1588947098,1588947098,6),(577,'verificacion','52','ibet',1588947220,1588947220,6),(578,'verificacion','53','ibet',1588947360,1588947360,6),(579,'verificacion','54','ibet',1588947431,1588947431,6),(580,'verificacion','55','ibet',1588948491,1588948491,6),(583,'dispositivos','240','felipem',1590173736,1590545021,2),(584,'marca_modelo','37','felipem',1590176256,1590176256,2),(585,'dispositivos','241','felipem',1590177225,1590177225,2),(586,'marcas','24','felipem',1590178187,1590178195,2),(587,'marca_modelo','38','felipem',1590178359,1590178518,2),(588,'dispositivos','242','felipem',1590178552,1593049083,2),(589,'tipo_documento','10','felipem',1593013355,1593013355,2),(590,'fichatecnica','1','felipem',1593049055,1593049112,2),(591,'movimientos','136','camiloz',1594167656,1594167676,3),(592,'movimientos','137','camiloz',1594168052,1594168057,3),(593,'unidades','13','felipem',1599773478,1599773478,2),(594,'movimientos','138','felipem',1599773594,1599773594,2),(595,'contactos','6','lgarcia',1599774323,1599774323,3),(596,'calibraciones','1','lgarcia',1599774665,1599774665,3),(597,'movimientos','139','superadmin',1599775006,1599775006,2),(599,'dispositivos','243','lgarcia',1601327887,1602782449,3),(600,'documentos','2','lgarcia',1601328288,1601328288,3),(601,'marca_modelo','39','superadmin',1601587345,1601587345,2),(602,'dispositivos','244','superadmin',1601588444,1601588676,2),(603,'movimientos','140','superadmin',1601588594,1601588594,2),(604,'dispositivos','245','superadmin',1601588760,1601588760,2),(605,'movimientos','141','superadmin',1601588818,1601588818,2),(606,'verificacion','56','superadmin',1602186025,1602186025,2),(607,'verificacion','57','superadmin',1602186078,1602186078,2),(608,'verificacion','58','superadmin',1602186123,1602186123,2),(609,'verificacion','59','superadmin',1602186213,1602186213,2),(610,'verificacion','60','superadmin',1602187164,1602187164,2),(611,'verificacion','61','superadmin',1602187226,1602187383,2),(612,'dispositivos','246','superadmin',1602187714,1602187714,2),(613,'verificacion','62','superadmin',1602187731,1602187731,2),(614,'verificacion','63','superadmin',1602188156,1602188156,2),(615,'verificacion','64','superadmin',1602189203,1602189203,2),(616,'tipo_dispositivo','23','superadmin',1602782313,1602782313,2),(617,'marca_modelo','40','superadmin',1602782668,1602782668,2),(618,'dispositivos','247','superadmin',1602782744,1602782744,2),(619,'marca_modelo','41','superadmin',1602782983,1602783950,2),(620,'marcas','25','superadmin',1602784828,1602784828,2),(621,'marca_modelo','42','superadmin',1602784869,1602784869,2),(622,'marca_modelo','43','superadmin',1602784930,1602784930,2),(623,'verificacion','65','superadmin',1602790571,1602790571,2),(624,'marcas','26','superadmin',1602793582,1602793582,2),(625,'marca_modelo','44','superadmin',1602793681,1602802768,2),(626,'marca_modelo','45','superadmin',1602798203,1602798203,2),(627,'dispositivos','248','superadmin',1602798356,1602798356,2),(628,'dispositivos','249','superadmin',1602798476,1602798476,2),(629,'dispositivos','250','superadmin',1602799644,1602799644,2),(630,'marca_modelo','46','superadmin',1602801618,1602801634,2),(631,'dispositivos','251','superadmin',1602801669,1602801669,2),(632,'marca_modelo','47','superadmin',1602801879,1602801879,2),(633,'dispositivos','252','superadmin',1602801941,1602801941,2),(634,'dispositivos','253','superadmin',1602802087,1602802164,2),(635,'dispositivos','254','superadmin',1602802216,1602802309,2),(636,'tipo_estado_dispo','4','superadmin',1602802489,1602802489,2),(637,'dispositivos','255','superadmin',1602802500,1602802857,2),(638,'dispositivos','256','superadmin',1602802595,1602802808,2),(639,'dispositivos','257','superadmin',1602802645,1602802821,2);
/*!40000 ALTER TABLE `membership_userrecords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_users`
--

DROP TABLE IF EXISTS `membership_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_users` (
  `memberID` varchar(100) NOT NULL,
  `passMD5` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) unsigned DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) unsigned DEFAULT NULL,
  `flags` text,
  PRIMARY KEY (`memberID`),
  KEY `groupID` (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_users`
--

LOCK TABLES `membership_users` WRITE;
/*!40000 ALTER TABLE `membership_users` DISABLE KEYS */;
INSERT INTO `membership_users` VALUES ('guest',NULL,NULL,'2019-11-26',1,0,1,NULL,NULL,NULL,NULL,'Anonymous member created automatically on 2019-11-26',NULL,NULL,NULL),('superadmin','$2y$10$AuuyvEFPljmpdlg6J7sZSeNNFcGgLKdfyOZ1kS5oZ75fe7RphdMde','tecnologia@starmedica.com.co','2019-11-26',2,0,1,NULL,NULL,NULL,NULL,'Admin member created automatically on 2019-11-26\nRecord updated automatically on 2019-12-02\nRecord updated automatically on 2020-09-10',NULL,NULL,NULL),('lgarcia','$2y$10$E.acyF5gBBk8yJFqh4ahzeIPa6z7wtWlPkCB1a6dA.ZT2oGAuZ68.','cardio.auditoria@gmail.com','2019-11-27',3,0,1,'Luis Garcia','313','C','','',NULL,NULL,NULL),('vilmao','$2y$10$A6o/En7F23Qq/RjY8Gn4IuJLENkzWd1dVQZnT1Y8g2V5y/PV8W8R2','gerencia@starmedica.com.co','2019-11-27',3,0,1,'Vilma Ocampo','','','','',NULL,NULL,NULL),('luzeidy','$2y$10$lDpcGchI.5PN8u8gX6hblegSwqnPwIxUS5xlF0Q1q42kmDkpiOeyK','cardio.financiera@gmail.com','2019-12-02',4,0,1,'Luzeidy Zambrano','','Cali','','',NULL,NULL,NULL),('william','$2y$10$/TcJ0L.NNLnPWVDlLAgg4uwbWBVpWqolhApdNIcBc7oHbMUvLLvCC','cardio.procesos@gmail.com','2019-12-02',4,0,1,'William Ibarra','Compras y Procesos','Cali','Starmedica','',NULL,NULL,NULL),('bodega.felipe','$2y$10$8uumdourCA3AZ9yS8mdQAOEDwy0AE9.V1ImVFvdZNMj4fWxoj2U/K','tecnologia@starmedica.com.co','2019-12-27',5,0,1,'Felipe Molano','Bodega','Cali','STARMEDICA','',NULL,NULL,NULL),('camiloz','$2y$10$H7zFRIMfoGfQSR4Wtm4wietmX5Wztg9HYvyKYam5fn4CzG9PZmuJC','camilomunoz1064@gmail.com','2020-02-12',3,0,1,'Camilo Zambrano','Tecnologia','Cali','Starmedica','',NULL,NULL,NULL),('jvilla','$2y$10$fAUMILtSTM9gC3A7L9G1/.UTSwp2ZGk2mgZI14BUUPB.zSHxbSfGu','hidc@starmedica.com.co','2020-04-28',6,0,1,'Jorge Luis Villa','HIDC','','Starmedica','FLM',NULL,NULL,NULL),('druiz','$2y$10$zl250b5ZfAtxDoAU37amRe9cV9b1SqziT74LvId5oS9E6F6xDnqNS','hidc@starmedica.com.co','2020-04-28',6,0,1,'David Alexander Ruiz','','Cali','','member signed up through the registration form.',NULL,NULL,NULL),('crueda','$2y$10$g7TKPlMqaNRPOcNKddCjC.fXNVAysgqODdevfLMws0csBPY88uCN.','Htu@starmedica.com.co','2020-04-29',6,0,1,'Claudia Rueda','','','','',NULL,NULL,NULL),('jpalacios','$2y$10$MSS1F9nx885qv1C.phGJUuhQwcdG5FeKHL5posipPwp7yXpRLvOQ.','hfps@starmedica.com.co','2020-04-29',6,0,1,'Janeth Palacios','','','','member signed up through the registration form.',NULL,NULL,NULL),('aospina','$2y$10$CgtKty23b7MvRPlMaLHL2.E1jyzKEXYnD5UBgnmxW6JHjXrVFsVd6','hrob@starmedica.com.co','2020-04-29',6,0,1,'Andres Ospina','','','','member signed up through the registration form.',NULL,NULL,NULL),('yulianar','$2y$10$8odv2jQN88ULJENhSAceQeI/P4.SUL.kPhp/zAG1uShGMPlzVN0gu','administracion@starmedica.com.co','2020-04-29',6,0,1,'Yuliana Roa','','','','member signed up through the registration form.',NULL,NULL,NULL),('aida','$2y$10$G5yEj/vTKX5uONwQ3/AZOO.7B7G/blGSuyc8hlMha2p8.lb0AxArq','adm@starmedica.com.co','2020-04-29',6,0,1,'Aida Milena','','','','member signed up through the registration form.',NULL,NULL,NULL),('sgiraldo','$2y$10$oJB2ufw.JB0Kp0SNrfjrs.uwWwnSTmLN.g.Q46QRrWeNOKWfod3l.','Htu@starmedica.com.co','2020-04-29',6,0,1,'Samir Giraldo','','','','member signed up through the registration form.',NULL,NULL,NULL),('mmartinez','$2y$10$Ov/yHx.lawxWeR1qe8kmC.VYzzuLUwJnirowwhov0QhtkvrR4bL6K','fhsj@starmedica.com.co','2020-04-29',6,0,1,'Mary Martinez','','','','member signed up through the registration form.',NULL,NULL,NULL),('srendon','$2y$10$/i66ZuKkE/ntGxsnplW2tei6wu5CAl8zV4sN4HJyDeOO.osbUfSmC','fhsj@starmedica.com.co','2020-04-29',6,0,1,'Sebastian Rendon','','','','member signed up through the registration form.',NULL,NULL,NULL),('diegoh','$2y$10$de5wPgEICRaZkwxfJbK/4uW651uMMhDkZBlelc0DZpL7fA.6jqi2O','fhsj@starmedica.com.co','2020-04-29',6,0,1,'Diego Hernan','','','','member signed up through the registration form.',NULL,NULL,NULL),('jvalencia','$2y$10$n8oZGX1OnN.e6PFCvPj9s.e0i2sAlO8dmo1ekd2m/m5485yusKDrG','fhsj@starmedica.com.co','2020-04-29',6,0,1,'Javier Francisco Valencia','','','','member signed up through the registration form.',NULL,NULL,NULL),('mnieto','$2y$10$lv./nAtshgFyFG9S/CAiEOXEQzOLP7PMDkaHgjaL.VtoDwqgNZou6','holter.tulua@gmail.com','2020-04-29',6,0,1,'Monica Nieto','','','','member signed up through the registration form.',NULL,NULL,NULL),('dvalencia','$2y$10$XZj.n9BTG0nxk7toXSNVxuELX.Fc6qXnrF40NbnM4pu.G/9ytzEIa','danielavalenciapaz@gmail.com','2020-04-29',6,0,1,'Daniela Valencia','','','','member signed up through the registration form.',NULL,NULL,NULL),('ccastro','$2y$10$MXXE4w9Hh8P4zRH75NzaX.sgYW9KdP8wLrDLsbxSCH0jBXE.aVkAe','Htu@starmedica.com.co','2020-04-29',6,0,1,'Cesar Andres Castro','','','','member signed up through the registration form.',NULL,NULL,NULL),('wibarra','$2y$10$cXsANUC26QkJ7IaTSQYfT.zMnpkpRqoA40Bn/IkV/khO/swbZC/52','cardio.procesos@gmail.com','2020-04-29',6,0,1,'William Ibarra','','','','member signed up through the registration form.',NULL,NULL,NULL),('hrestrepo','$2y$10$TfvQeTirXQWULOzm0l9YqOsVrkLvkPc5eZWOJcEPa3OjLS4/ig7DO','Htu@starmedica.com.co','2020-04-29',6,0,1,'Harold Restrepo','','','','member signed up through the registration form.',NULL,NULL,NULL),('jesusv','$2y$10$F/wiNTWif6co3wRHiU3N3.VI.r9Um5U8Lj308T15zBY7RCCvNDttq','tecnologia@starmedica.com.co','2020-04-29',6,0,1,'Jesus Davida Varela','','Cali','','member signed up through the registration form.',NULL,NULL,NULL),('carlosc','$2y$10$IL5qlrI8g93W/GempahDS.QghlbheYM229q1YaFecIku1W0RDaRee','tecnologia@starmedica.com.co','2020-04-29',6,0,1,'Carlos Carvajal','','Cali','','member signed up through the registration form.',NULL,NULL,NULL),('ibet','$2y$10$6hnxQZ7nk4Y6QroQh05cCOhXI2YCyMwoQJFX.0EWAoMxYJnnJNHGS','tecnologia@starmedica.com.co','2020-04-29',6,0,1,'Ibet Toro','','Cali','','member signed up through the registration form.',NULL,NULL,NULL),('wilmarh','$2y$10$KpCQE8v6HKJ.oVKCwBjXfun57tTbTBMyieiSgwFsB2tHDxHDg9YI2','tecnologia@starmedica.com.co','2020-04-29',6,0,1,'Wilmar Hernán mamian','','Cali','','member signed up through the registration form.',NULL,NULL,NULL),('fvalencia','$2y$10$HJ429dj/3nnqr8yffKmOrO6zyCoQP7vu9M8RO.Vt.PjPiVfXcVJ0u','hmc@starmedica.com.co','2020-04-29',6,0,1,'Frank Valencia','','','','member signed up through the registration form.',NULL,NULL,NULL),('jcastro','$2y$10$EraHgKTE1CbyrdJm9jzve.2eJ744kfWheoJoMwPAkkb9gIeb5uYAO','cardio.calidad2@gmail.com','2020-05-05',6,0,1,'Johana Castro','','','','Flm',NULL,NULL,NULL);
/*!40000 ALTER TABLE `membership_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membership_usersessions`
--

DROP TABLE IF EXISTS `membership_usersessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membership_usersessions` (
  `memberID` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `agent` varchar(100) NOT NULL,
  `expiry_ts` int(10) unsigned NOT NULL,
  UNIQUE KEY `memberID_token_agent` (`memberID`,`token`,`agent`),
  KEY `memberID` (`memberID`),
  KEY `expiry_ts` (`expiry_ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membership_usersessions`
--

LOCK TABLES `membership_usersessions` WRITE;
/*!40000 ALTER TABLE `membership_usersessions` DISABLE KEYS */;
INSERT INTO `membership_usersessions` VALUES ('superadmin','jdMr5kXzXgTU3SVD95tkbVVjrfJeXf','0thGWAe7EWWgjKPGOHExjvVYir3gI5',1604177805),('superadmin','FSBQ1w6DIVlelDqQb4F10yH3ePj4I8','YJ1Xd2jQNMFm3pkaVnam4rCCC00MCh',1604180154),('superadmin','YQukMIL2DAz6vkYXGWSXYuBsHfUPZU','PMt3qymGV38kmNbhwkEHZIqoBr0XRF',1605367011);
/*!40000 ALTER TABLE `membership_usersessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobiliario`
--

DROP TABLE IF EXISTS `mobiliario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobiliario` (
  `id_mobiliario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(40) NOT NULL,
  `tipo_mobiliario` int(10) unsigned NOT NULL,
  `fecha_ingreso` varchar(40) NOT NULL,
  `unidad` int(10) unsigned DEFAULT NULL,
  `uni_abreviado` int(10) unsigned DEFAULT NULL,
  `descripcion` text,
  `accesorios` text,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `grupo` int(10) unsigned DEFAULT NULL,
  `foto` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_mobiliario`),
  UNIQUE KEY `codigo_unique` (`codigo`),
  KEY `tipo_mobiliario` (`tipo_mobiliario`),
  KEY `unidad` (`unidad`),
  KEY `grupo` (`grupo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobiliario`
--

LOCK TABLES `mobiliario` WRITE;
/*!40000 ALTER TABLE `mobiliario` DISABLE KEYS */;
/*!40000 ALTER TABLE `mobiliario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movimientos`
--

DROP TABLE IF EXISTS `movimientos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movimientos` (
  `id_movi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_movimiento` int(10) unsigned NOT NULL,
  `codigo_dispo` int(10) unsigned NOT NULL,
  `serial_dispo` int(10) unsigned DEFAULT NULL,
  `tipo_dispo` int(10) unsigned DEFAULT NULL,
  `origen` int(10) unsigned NOT NULL,
  `fecha_movimiento` date NOT NULL,
  `destino` int(10) unsigned NOT NULL,
  `responsable` int(10) unsigned NOT NULL,
  `telefono_respon` int(10) unsigned DEFAULT NULL,
  `estado_movi` int(10) unsigned DEFAULT NULL,
  `verifica` int(10) unsigned DEFAULT NULL,
  `estado_verifica` int(10) unsigned NOT NULL,
  `comentarios` text,
  `creado2` varchar(40) DEFAULT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `documento` varchar(40) DEFAULT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field18` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_movi`),
  KEY `tipo_movimiento` (`tipo_movimiento`),
  KEY `codigo_dispo` (`codigo_dispo`),
  KEY `origen` (`origen`),
  KEY `destino` (`destino`),
  KEY `responsable` (`responsable`),
  KEY `estado_movi` (`estado_movi`),
  KEY `verifica` (`verifica`),
  KEY `estado_verifica` (`estado_verifica`)
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movimientos`
--

LOCK TABLES `movimientos` WRITE;
/*!40000 ALTER TABLE `movimientos` DISABLE KEYS */;
INSERT INTO `movimientos` VALUES (1,1,56,56,56,1,'2019-11-27',8,2,2,1,NULL,1,'Ok','27/11/2019 06:46:08 pm','27/11/2019 06:46:08 pm','27/11/2019 06:50:10 pm','vilmao','vilmao',NULL,'2019-11-27 00:00:00',NULL,NULL),(2,2,56,56,56,1,'2019-11-27',8,2,2,2,56,2,'ok','27/11/2019 06:49:34 pm','27/11/2019 06:49:34 pm',NULL,'vilmao',NULL,NULL,'2019-11-27 00:00:00',NULL,NULL),(3,2,57,57,57,7,'2019-11-29',9,3,3,1,57,2,'Nuevo','29/11/2019 04:51:59 pm','29/11/2019 04:51:59 pm','29/11/2019 04:54:36 pm','felipem','felipem',NULL,'2019-11-29 00:00:00',NULL,NULL),(4,1,58,58,58,7,'2019-11-29',9,3,3,1,58,2,'Nuevo','29/11/2019 05:06:55 pm','29/11/2019 05:06:55 pm',NULL,'felipem',NULL,NULL,'2019-11-29 00:00:00',NULL,NULL),(5,1,17,17,17,10,'2019-12-03',1,3,3,1,NULL,1,'Dispositivo trasladado a HIDC, se ha realizado un mantenimiento y limpieza','3/12/2019 12:44:26 am','3/12/2019 12:44:26 am','3/12/2019 01:38:31 pm','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(6,2,17,17,17,10,'2019-12-03',1,3,3,2,17,2,'Recibido en la unidad','3/12/2019 12:45:56 am','3/12/2019 12:45:56 am','3/12/2019 01:39:14 pm','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(7,1,23,23,23,6,'2019-12-03',1,3,3,1,23,1,'Update 3 de diciembre','3/12/2019 11:05:01 am','3/12/2019 11:05:01 am','4/3/2020 06:32:03 pm','felipem','camiloz',NULL,'2019-12-03 00:00:00',NULL,NULL),(8,2,23,23,23,6,'2019-12-03',1,3,3,2,23,2,'Recibido','3/12/2019 11:06:48 am','3/12/2019 11:06:48 am',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(9,1,26,26,26,6,'2019-12-03',1,3,3,NULL,NULL,1,NULL,'3/12/2019 11:10:01 am','3/12/2019 11:10:01 am',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(10,2,26,26,26,6,'2019-12-03',1,3,3,2,26,2,NULL,'3/12/2019 11:10:13 am','3/12/2019 11:10:13 am','3/12/2019 11:10:42 am','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(11,1,2,2,2,7,'2019-12-03',1,3,3,1,NULL,1,NULL,'3/12/2019 11:11:58 am','3/12/2019 11:11:58 am',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(12,2,2,2,2,7,'2019-12-03',1,3,3,1,2,2,NULL,'3/12/2019 11:12:02 am','3/12/2019 11:12:02 am','3/12/2019 11:13:19 am','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(13,1,9,9,9,7,'2019-12-03',1,3,3,1,NULL,1,NULL,'3/12/2019 11:14:47 am','3/12/2019 11:14:47 am',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(14,2,9,9,9,7,'2019-12-03',1,3,3,1,9,2,NULL,'3/12/2019 11:14:52 am','3/12/2019 11:14:52 am','3/12/2019 11:15:21 am','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(15,2,80,80,80,7,'2019-12-03',1,3,3,1,80,2,NULL,'3/12/2019 11:18:12 am','3/12/2019 11:18:12 am','3/12/2019 11:18:43 am','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(16,2,82,82,82,7,'2019-12-03',1,3,3,1,82,2,NULL,'3/12/2019 11:20:57 am','3/12/2019 11:20:57 am','3/12/2019 11:22:13 am','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(17,1,82,82,82,7,'2019-12-03',1,3,3,1,NULL,1,NULL,'3/12/2019 11:21:07 am','3/12/2019 11:21:07 am',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(18,1,50,50,50,7,'2019-12-03',1,3,3,1,NULL,1,NULL,'3/12/2019 11:26:42 am','3/12/2019 11:26:42 am',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(19,2,50,50,50,7,'2019-12-03',1,3,3,1,50,2,NULL,'3/12/2019 11:26:55 am','3/12/2019 11:26:55 am','3/12/2019 11:27:35 am','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(20,1,60,60,60,7,'2019-12-03',1,3,3,1,NULL,1,NULL,'3/12/2019 01:44:39 pm','3/12/2019 01:44:39 pm',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(21,2,60,60,60,7,'2019-12-03',1,3,3,1,60,2,NULL,'3/12/2019 01:44:50 pm','3/12/2019 01:44:50 pm','3/12/2019 01:45:14 pm','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(22,1,59,59,59,7,'2019-12-03',1,3,3,1,NULL,1,NULL,'3/12/2019 01:45:51 pm','3/12/2019 01:45:51 pm',NULL,'felipem',NULL,NULL,'2019-12-03 00:00:00',NULL,NULL),(23,2,59,59,59,7,'2019-12-03',1,3,3,2,59,2,NULL,'3/12/2019 01:45:54 pm','3/12/2019 01:45:54 pm','3/12/2019 01:46:20 pm','felipem','felipem',NULL,'2019-12-03 00:00:00',NULL,NULL),(24,1,199,199,199,7,'2019-12-10',4,3,3,1,NULL,1,'Nuevo, enviado con HR','10/12/2019 06:39:42 pm','10/12/2019 06:39:42 pm',NULL,'felipem',NULL,NULL,'2019-12-10 00:00:00',NULL,NULL),(25,2,199,199,199,7,'2019-12-10',4,3,3,2,199,2,'Nuevo, enviado con HR','10/12/2019 06:39:49 pm','10/12/2019 06:39:49 pm','10/12/2019 06:40:23 pm','felipem','felipem',NULL,'2019-12-10 00:00:00',NULL,NULL),(26,1,203,203,203,7,'2019-12-09',4,3,3,1,NULL,1,'Holter nuevo enviado con HR','10/12/2019 06:42:31 pm','10/12/2019 06:42:31 pm',NULL,'felipem',NULL,NULL,'2019-12-10 00:00:00',NULL,NULL),(27,2,203,203,203,7,'2019-12-09',4,3,3,2,203,2,'Recibido en HTU','10/12/2019 06:42:37 pm','10/12/2019 06:42:37 pm','10/12/2019 06:43:13 pm','felipem','felipem',NULL,'2019-12-10 00:00:00',NULL,NULL),(28,1,202,202,202,7,'2019-12-09',4,3,3,1,NULL,1,'Nuevo enviado con HR','10/12/2019 06:44:48 pm','10/12/2019 06:44:48 pm',NULL,'felipem',NULL,NULL,'2019-12-10 00:00:00',NULL,NULL),(29,2,202,202,202,7,'2019-12-09',4,3,3,2,202,2,'Recibído en la unidad','10/12/2019 06:46:08 pm','10/12/2019 06:46:08 pm','10/12/2019 06:46:47 pm','felipem','felipem',NULL,'2019-12-10 00:00:00',NULL,NULL),(30,1,200,200,200,7,'2019-12-05',5,3,3,1,NULL,1,'<br>','27/12/2019 11:04:26 am','27/12/2019 11:04:26 am',NULL,'felipem',NULL,NULL,'2019-12-27 00:00:00',NULL,NULL),(31,2,200,200,200,7,'2019-12-05',5,3,3,2,200,2,'<br>','27/12/2019 11:04:29 am','27/12/2019 11:04:29 am','27/12/2019 11:05:02 am','felipem','felipem',NULL,'2019-12-27 00:00:00',NULL,NULL),(32,1,201,201,201,7,'2019-12-05',5,3,3,1,NULL,1,'<br>','27/12/2019 11:07:35 am','27/12/2019 11:07:35 am',NULL,'felipem',NULL,NULL,'2019-12-27 00:00:00',NULL,NULL),(33,1,201,201,201,7,'2019-12-05',5,3,3,2,201,2,'<br>','27/12/2019 11:07:39 am','27/12/2019 11:07:39 am','27/12/2019 11:08:09 am','felipem','felipem',NULL,'2019-12-27 00:00:00',NULL,NULL),(34,1,204,204,204,7,'2019-12-05',5,3,3,1,NULL,1,'<br>','27/12/2019 11:09:54 am','27/12/2019 11:09:54 am',NULL,'felipem',NULL,NULL,'2019-12-27 00:00:00',NULL,NULL),(35,2,204,204,204,7,'2019-12-05',5,3,3,2,204,2,'<br>','27/12/2019 11:10:10 am','27/12/2019 11:10:10 am','27/12/2019 11:10:27 am','felipem','felipem',NULL,'2019-12-27 00:00:00',NULL,NULL),(36,1,90,90,90,7,'2019-12-16',9,3,3,1,NULL,1,'<br>','27/12/2019 12:54:30 pm','27/12/2019 12:54:30 pm',NULL,'felipem',NULL,NULL,'2019-12-27 00:00:00',NULL,NULL),(37,2,90,90,90,7,'2019-12-16',9,3,3,2,90,2,'<br>','27/12/2019 12:54:37 pm','27/12/2019 12:54:37 pm','27/12/2019 12:54:56 pm','felipem','felipem',NULL,'2019-12-27 00:00:00',NULL,NULL),(38,1,88,88,88,1,'2019-12-09',9,3,3,1,88,2,'<br>','27/12/2019 12:59:42 pm','27/12/2019 12:59:42 pm',NULL,'felipem',NULL,NULL,'2019-12-27 00:00:00',NULL,NULL),(39,1,216,216,216,7,'2020-02-04',9,3,3,1,NULL,1,NULL,'4/2/2020 11:09:15 am','4/2/2020 11:09:15 am',NULL,'felipem',NULL,NULL,'2020-02-04 00:00:00',NULL,NULL),(40,1,217,217,217,7,'2020-02-04',9,3,3,1,NULL,1,'<br>','4/2/2020 11:09:20 am','4/2/2020 11:09:20 am','4/2/2020 11:09:32 am','felipem','felipem',NULL,'2020-02-04 00:00:00',NULL,NULL),(41,1,218,218,218,7,'2020-02-04',9,3,3,1,NULL,1,'<br>','4/2/2020 11:09:36 am','4/2/2020 11:09:36 am','4/2/2020 11:09:43 am','felipem','felipem',NULL,'2020-02-04 00:00:00',NULL,NULL),(42,1,219,219,219,7,'2020-02-04',9,3,3,1,NULL,1,'<br>','4/2/2020 11:09:47 am','4/2/2020 11:09:47 am','4/2/2020 11:10:06 am','felipem','felipem',NULL,'2020-02-04 00:00:00',NULL,NULL),(43,1,220,220,220,7,'2020-02-04',9,3,3,1,NULL,1,'<br>','4/2/2020 11:10:08 am','4/2/2020 11:10:08 am','4/2/2020 11:10:29 am','felipem','felipem',NULL,'2020-02-04 00:00:00',NULL,NULL),(44,1,223,223,223,11,'2020-02-19',1,3,3,1,NULL,1,'<br>','19/2/2020 12:10:00 pm','19/2/2020 12:10:00 pm',NULL,'felipem',NULL,NULL,'2020-02-19 00:00:00',NULL,NULL),(45,1,44,44,44,7,'2020-02-20',7,3,3,1,44,2,'Equipo sacado de FHSJ','20/2/2020 03:52:45 pm','20/2/2020 03:52:45 pm',NULL,'felipem',NULL,NULL,'2020-02-20 00:00:00',NULL,NULL),(46,1,226,226,226,1,'2020-02-29',1,5,5,1,NULL,1,'El equipo era el DG 407 y se le otorga el DG 411, CAMILO','29/2/2020 10:21:28 am','29/2/2020 10:21:28 am',NULL,'camiloz',NULL,NULL,'2020-02-29 00:00:00',NULL,NULL),(47,2,226,226,226,1,'2020-02-29',1,5,5,2,226,2,'Equipo en funcionamiento normal','29/2/2020 10:22:53 am','29/2/2020 10:22:53 am',NULL,'camiloz',NULL,NULL,'2020-02-29 00:00:00',NULL,NULL),(48,1,126,126,126,1,'2020-02-29',1,5,5,2,126,2,'SE MODIFICARÁ EL DIGITALIZADOR DEL HMC','29/2/2020 10:29:30 am','29/2/2020 10:29:30 am',NULL,'camiloz',NULL,NULL,'2020-02-29 00:00:00',NULL,NULL),(49,1,227,227,227,2,'2020-02-29',2,5,5,2,227,2,'PENDIENTE POR CAMBIAR CÓDIGO DEL 401 AL 412','29/2/2020 10:33:35 am','29/2/2020 10:33:35 am','29/2/2020 10:38:27 am','camiloz','camiloz',NULL,'2020-02-29 00:00:00',NULL,NULL),(50,1,120,120,120,1,'2020-02-29',1,5,5,2,120,2,'El transductor funciona normalmente en el HIDC','29/2/2020 11:37:55 am','29/2/2020 11:37:55 am','29/2/2020 11:12:19 pm','camiloz','camiloz',NULL,'2020-02-29 00:00:00',NULL,NULL),(51,1,165,165,165,1,'2020-02-29',1,5,5,2,165,2,'La trotadora funciona normalmente en el HIDC','29/2/2020 11:42:06 am','29/2/2020 11:42:06 am','29/2/2020 11:42:15 am','camiloz','camiloz',NULL,'2020-02-29 00:00:00',NULL,NULL),(52,1,172,172,172,1,'2020-02-29',1,5,5,2,172,2,'El dispositivo se encuentra en funcionamiento normal en el HIDC','29/2/2020 11:49:41 am','29/2/2020 11:49:41 am',NULL,'camiloz',NULL,NULL,'2020-02-29 00:00:00',NULL,NULL),(53,1,124,124,124,1,'2020-02-29',1,5,5,2,124,2,'Dispositivo funcionando normalmente en el HIDC','29/2/2020 11:59:45 am','29/2/2020 11:59:45 am',NULL,'camiloz',NULL,NULL,'2020-02-29 00:00:00',NULL,NULL),(54,1,80,80,80,1,'2020-02-29',7,5,5,1,80,2,'El equipo sale de la unidad por motivo de revisión y mantenimiento, Camilo.','29/2/2020 08:00:40 pm','29/2/2020 08:00:40 pm','29/2/2020 09:27:28 pm','camiloz','camiloz',NULL,'2020-02-29 00:00:00',NULL,NULL),(59,1,49,49,49,1,'2020-02-29',1,5,5,2,49,2,'El equipo funciona normalmente','29/2/2020 09:31:05 pm','29/2/2020 09:31:05 pm','29/2/2020 09:31:09 pm','camiloz','camiloz',NULL,'2020-02-29 00:00:00',NULL,NULL),(56,1,87,87,87,1,'2020-02-29',1,5,5,2,87,2,'El dispositivo funciona normalmente','29/2/2020 08:12:15 pm','29/2/2020 08:12:15 pm',NULL,'camiloz',NULL,NULL,'2020-02-29 00:00:00',NULL,NULL),(60,1,42,42,42,2,'2020-02-26',2,5,5,2,42,2,'<br>','2/3/2020 03:23:12 pm','2/3/2020 03:23:12 pm','2/3/2020 03:23:24 pm','camiloz','camiloz',NULL,'2020-03-02 00:00:00',NULL,NULL),(61,1,138,138,138,2,'2020-02-26',2,5,5,2,138,2,'<br>','2/3/2020 03:28:22 pm','2/3/2020 03:28:22 pm','2/3/2020 03:28:36 pm','camiloz','camiloz',NULL,'2020-03-02 00:00:00',NULL,NULL),(62,1,163,163,163,2,'2020-02-26',2,5,5,2,163,2,'<br>','2/3/2020 03:45:44 pm','2/3/2020 03:45:44 pm',NULL,'camiloz',NULL,NULL,'2020-03-02 00:00:00',NULL,NULL),(63,1,170,170,170,2,'2020-02-26',2,5,5,2,170,2,'<br>','2/3/2020 03:52:52 pm','2/3/2020 03:52:52 pm','2/3/2020 03:53:08 pm','camiloz','camiloz',NULL,'2020-03-02 00:00:00',NULL,NULL),(64,1,130,130,130,5,'2020-03-02',3,5,5,1,130,1,'Pendiente ubicar el digitalizador que salió de la Clínica San Francisco por reparación','2/3/2020 08:51:05 pm','2/3/2020 08:51:05 pm','2/3/2020 09:03:11 pm','camiloz','camiloz',NULL,'2020-03-02 00:00:00',NULL,NULL),(65,1,130,130,130,5,'2020-03-02',3,5,5,1,130,2,NULL,'2/3/2020 08:59:07 pm','2/3/2020 08:59:07 pm',NULL,'camiloz',NULL,NULL,'2020-03-02 00:00:00',NULL,NULL),(66,1,51,51,51,7,'2020-03-02',3,5,5,1,51,1,'<br>','2/3/2020 09:09:32 pm','2/3/2020 09:09:32 pm',NULL,'camiloz',NULL,NULL,'2020-03-02 00:00:00',NULL,NULL),(67,1,51,51,51,7,'2020-03-02',3,5,5,1,51,2,'<br>','2/3/2020 09:11:49 pm','2/3/2020 09:11:49 pm',NULL,'camiloz',NULL,NULL,'2020-03-02 00:00:00',NULL,NULL),(68,1,34,34,34,7,'2020-03-02',3,5,5,1,34,1,'<br>','2/3/2020 09:17:40 pm','2/3/2020 09:17:40 pm',NULL,'camiloz',NULL,NULL,'2020-03-02 00:00:00',NULL,NULL),(69,1,34,34,34,7,'2020-03-02',3,5,5,1,34,2,'<br>','2/3/2020 09:19:33 pm','2/3/2020 09:19:33 pm','2/3/2020 09:19:43 pm','camiloz','camiloz',NULL,'2020-03-02 00:00:00',NULL,NULL),(70,1,131,131,131,4,'2020-03-02',3,5,5,1,131,1,'<br>','2/3/2020 09:29:42 pm','2/3/2020 09:29:42 pm',NULL,'camiloz',NULL,NULL,'2020-03-02 00:00:00',NULL,NULL),(71,1,131,131,131,4,'2020-03-02',3,5,5,1,131,2,'<br>','2/3/2020 09:33:51 pm','2/3/2020 09:33:51 pm','2/3/2020 09:34:50 pm','camiloz','camiloz',NULL,'2020-03-02 00:00:00',NULL,NULL),(72,1,108,108,108,7,'2020-03-03',7,5,5,2,108,2,'Se encuentra en la Reforma','3/3/2020 04:39:07 pm','3/3/2020 04:39:07 pm',NULL,'camiloz',NULL,NULL,'2020-03-03 00:00:00',NULL,NULL),(73,1,101,101,101,7,'2020-03-03',7,5,5,2,101,2,'<br>','3/3/2020 05:16:28 pm','3/3/2020 05:16:28 pm',NULL,'camiloz',NULL,NULL,'2020-03-03 00:00:00',NULL,NULL),(74,1,119,119,119,1,'2020-02-29',1,5,5,2,119,2,'Esta en el HIDC','3/3/2020 09:50:10 pm','3/3/2020 09:50:10 pm',NULL,'camiloz',NULL,NULL,'2020-03-03 00:00:00',NULL,NULL),(75,1,119,119,119,1,'2020-03-04',2,5,5,1,119,2,'El Doctor Ruiz lleva el transductor del HIDC al HMC','3/3/2020 09:53:26 pm','3/3/2020 09:53:26 pm','3/3/2020 09:53:34 pm','camiloz','camiloz',NULL,'2020-03-03 00:00:00',NULL,NULL),(76,1,162,162,162,3,'2020-03-04',3,5,5,2,162,2,'Funciona normalmente en el HROB','4/3/2020 09:48:31 am','4/3/2020 09:48:31 am',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(77,1,104,104,104,3,'2020-02-26',3,5,5,2,104,2,'<br>','4/3/2020 09:50:55 am','4/3/2020 09:50:55 am',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(78,1,136,136,136,3,'2020-03-04',3,5,5,2,136,2,'<br>','4/3/2020 10:06:41 am','4/3/2020 10:06:41 am',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(79,1,156,156,156,3,'2020-03-04',3,5,5,2,156,2,'<br>','4/3/2020 10:18:03 am','4/3/2020 10:18:03 am',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(80,1,130,130,130,5,'2020-03-04',3,3,3,2,130,2,'Se verifica en HROB&nbsp;','4/3/2020 11:13:19 am','4/3/2020 11:13:19 am',NULL,'felipem',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(81,1,14,14,14,6,'2020-03-04',6,5,5,2,14,2,'<br>','4/3/2020 06:12:09 pm','4/3/2020 06:12:09 pm','4/3/2020 06:12:12 pm','camiloz','camiloz',NULL,'2020-03-04 00:00:00',NULL,NULL),(82,1,16,16,16,6,'2020-03-04',6,5,5,2,16,2,'<br>','4/3/2020 06:16:39 pm','4/3/2020 06:16:39 pm',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(83,1,10,10,10,6,'2020-03-04',6,5,5,2,10,2,'<br>','4/3/2020 06:19:39 pm','4/3/2020 06:19:39 pm',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(84,1,23,23,23,1,'2020-03-04',6,5,5,1,23,1,'<br>','4/3/2020 06:29:34 pm','4/3/2020 06:29:34 pm',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(85,2,23,23,23,1,'2020-03-04',6,5,5,2,23,2,'<br>','4/3/2020 06:30:54 pm','4/3/2020 06:30:54 pm',NULL,'camiloz',NULL,NULL,'2020-03-04 00:00:00',NULL,NULL),(86,1,1,1,1,6,'2020-03-04',6,5,5,2,1,2,'<br>','4/3/2020 06:35:01 pm','4/3/2020 06:35:01 pm','4/3/2020 06:35:04 pm','camiloz','camiloz',NULL,'2020-03-04 00:00:00',NULL,NULL),(87,1,98,98,98,6,'2020-03-04',6,5,5,2,98,2,'<br>','4/3/2020 06:37:40 pm','4/3/2020 06:37:40 pm','4/3/2020 06:37:43 pm','camiloz','camiloz',NULL,'2020-03-04 00:00:00',NULL,NULL),(88,1,65,65,65,6,'2020-03-04',6,5,5,2,65,2,'<br>','4/3/2020 06:40:33 pm','4/3/2020 06:40:33 pm','4/3/2020 06:40:37 pm','camiloz','camiloz',NULL,'2020-03-04 00:00:00',NULL,NULL),(89,1,46,46,46,6,'2020-03-04',6,5,5,2,46,2,'<br>','4/3/2020 06:45:26 pm','4/3/2020 06:45:26 pm','4/3/2020 06:45:29 pm','camiloz','camiloz',NULL,'2020-03-04 00:00:00',NULL,NULL),(90,1,36,36,36,4,'2020-03-05',7,5,5,1,36,1,'<br>','5/3/2020 10:11:21 pm','5/3/2020 10:11:21 pm',NULL,'camiloz',NULL,NULL,'2020-03-05 00:00:00',NULL,NULL),(91,1,36,36,36,4,'2020-03-05',7,5,5,1,36,2,'<br>','5/3/2020 10:12:51 pm','5/3/2020 10:12:51 pm','5/3/2020 10:13:06 pm','camiloz','camiloz',NULL,'2020-03-05 00:00:00',NULL,NULL),(92,1,52,52,52,7,'2020-03-05',7,5,5,2,52,2,'<br>','5/3/2020 10:17:47 pm','5/3/2020 10:17:47 pm',NULL,'camiloz',NULL,NULL,'2020-03-05 00:00:00',NULL,NULL),(93,1,230,230,230,4,'2020-03-06',4,5,5,2,230,2,'<br>','6/3/2020 08:59:09 am','6/3/2020 08:59:09 am','6/3/2020 09:03:52 am','camiloz','camiloz',NULL,'2020-03-06 00:00:00',NULL,NULL),(94,1,140,140,140,4,'2020-03-06',4,5,5,2,140,2,'<br>','6/3/2020 09:17:51 am','6/3/2020 09:17:51 am',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(95,1,25,25,25,4,'2020-03-06',4,5,5,2,25,2,'<br>','6/3/2020 09:31:55 am','6/3/2020 09:31:55 am','6/3/2020 09:32:39 am','camiloz','camiloz',NULL,'2020-03-06 00:00:00',NULL,NULL),(96,1,22,22,22,4,'2020-03-06',4,5,5,2,22,2,'<br>','6/3/2020 09:40:29 am','6/3/2020 09:40:29 am',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(97,1,129,129,129,4,'2020-03-06',4,5,5,2,129,2,'<br>','6/3/2020 09:56:31 am','6/3/2020 09:56:31 am','6/3/2020 09:56:49 am','camiloz','camiloz',NULL,'2020-03-06 00:00:00',NULL,NULL),(98,1,225,225,225,5,'2020-02-26',5,5,5,2,225,2,'<br>','6/3/2020 09:59:39 am','6/3/2020 09:59:39 am',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(99,1,221,221,221,4,'2020-03-06',4,5,5,2,221,2,'<br>','6/3/2020 10:09:15 am','6/3/2020 10:09:15 am',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(100,1,111,111,111,4,'2020-03-06',4,5,5,2,111,2,'<br>','6/3/2020 11:39:28 am','6/3/2020 11:39:28 am',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(101,1,232,232,232,4,'2020-03-06',4,5,5,2,232,2,'<br>','6/3/2020 12:16:35 pm','6/3/2020 12:16:35 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(102,1,234,234,234,4,'2020-03-06',4,5,5,2,234,2,'<br>','6/3/2020 12:54:10 pm','6/3/2020 12:54:10 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(103,1,21,21,21,4,'2020-03-06',4,5,5,2,21,2,'<br>','6/3/2020 01:12:18 pm','6/3/2020 01:12:18 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(104,1,209,209,209,7,'2020-03-06',4,5,5,1,209,1,'<br>','6/3/2020 02:14:28 pm','6/3/2020 02:14:28 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(105,1,209,209,209,7,'2020-03-06',4,5,5,1,209,2,'<br>','6/3/2020 02:15:49 pm','6/3/2020 02:15:49 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(106,1,67,67,67,4,'2020-03-06',4,5,5,2,67,2,'<br>','6/3/2020 02:20:52 pm','6/3/2020 02:20:52 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(107,1,53,53,53,7,'2020-03-06',5,5,5,1,53,1,'<br>','6/3/2020 03:20:34 pm','6/3/2020 03:20:34 pm','6/3/2020 03:21:00 pm','camiloz','camiloz',NULL,'2020-03-06 00:00:00',NULL,NULL),(108,1,53,53,53,7,'2020-03-06',5,5,5,1,53,2,'<br>','6/3/2020 03:22:16 pm','6/3/2020 03:22:16 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(109,1,84,84,84,5,'2020-03-06',5,5,5,2,84,2,'<br>','6/3/2020 03:32:19 pm','6/3/2020 03:32:19 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(110,1,27,27,27,5,'2020-03-06',5,5,5,2,27,2,'<br>','6/3/2020 03:57:35 pm','6/3/2020 03:57:35 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(111,1,28,28,28,5,'2020-03-06',5,5,5,2,28,2,'<br>','6/3/2020 04:07:42 pm','6/3/2020 04:07:42 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(112,1,31,31,31,7,'2020-03-06',5,5,5,1,31,1,'<br>','6/3/2020 04:13:21 pm','6/3/2020 04:13:21 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(113,1,31,31,31,7,'2020-03-06',5,5,5,1,31,2,'<br>','6/3/2020 04:15:01 pm','6/3/2020 04:15:01 pm',NULL,'camiloz',NULL,NULL,'2020-03-06 00:00:00',NULL,NULL),(114,1,158,158,158,10,'2019-02-21',1,3,3,1,158,1,'<br>','11/3/2020 01:40:51 pm','11/3/2020 01:40:51 pm',NULL,'camiloz',NULL,NULL,'2020-03-11 00:00:00',NULL,NULL),(115,1,158,158,158,10,'2020-02-21',1,3,3,1,158,2,'<br>','11/3/2020 01:42:20 pm','11/3/2020 01:42:20 pm',NULL,'camiloz',NULL,NULL,'2020-03-11 00:00:00',NULL,NULL),(116,1,166,166,166,5,'2020-03-06',5,5,5,1,166,2,'<br>','16/3/2020 01:11:47 pm','16/3/2020 01:11:47 pm','16/3/2020 01:11:53 pm','camiloz','camiloz',NULL,'2020-03-16 00:00:00',NULL,NULL),(117,1,34,34,34,3,'2020-03-17',6,5,5,1,34,1,'<br>','17/3/2020 01:45:33 pm','17/3/2020 01:45:33 pm','17/3/2020 01:45:55 pm','camiloz','camiloz','IMG_20200317_124457.jpg','2020-03-17 00:00:00',NULL,NULL),(118,1,123,123,123,6,'2020-03-17',6,5,5,2,123,2,'<br>','17/3/2020 04:42:27 pm','17/3/2020 04:42:27 pm','17/3/2020 04:42:34 pm','camiloz','camiloz',NULL,'2020-03-17 00:00:00',NULL,NULL),(119,1,159,159,159,6,'2020-03-17',6,5,5,2,159,2,'<br>','17/3/2020 04:50:36 pm','17/3/2020 04:50:36 pm',NULL,'camiloz',NULL,NULL,'2020-03-17 00:00:00',NULL,NULL),(120,1,19,19,19,6,'2020-03-17',6,5,5,2,19,2,'<br>','17/3/2020 06:03:58 pm','17/3/2020 06:03:58 pm','17/3/2020 06:04:02 pm','camiloz','camiloz',NULL,'2020-03-17 00:00:00',NULL,NULL),(121,1,70,70,70,6,'2020-03-17',6,5,5,2,70,2,'<br>','17/3/2020 06:15:14 pm','17/3/2020 06:15:14 pm','17/3/2020 06:15:20 pm','camiloz','camiloz',NULL,'2020-03-17 00:00:00',NULL,NULL),(122,1,72,72,72,6,'2020-03-17',6,5,5,2,72,2,'<br>','17/3/2020 06:17:33 pm','17/3/2020 06:17:33 pm',NULL,'camiloz',NULL,NULL,'2020-03-17 00:00:00',NULL,NULL),(123,1,48,48,48,6,'2020-03-04',6,5,5,2,48,2,'<br>','17/3/2020 06:32:40 pm','17/3/2020 06:32:40 pm',NULL,'camiloz',NULL,NULL,'2020-03-17 00:00:00',NULL,NULL),(124,1,34,34,34,3,'2020-03-17',6,5,5,1,34,2,'<br>','17/3/2020 06:35:47 pm','17/3/2020 06:35:47 pm',NULL,'camiloz',NULL,NULL,'2020-03-17 00:00:00',NULL,NULL),(125,1,131,131,131,3,'2020-03-17',6,5,5,1,131,1,'<span style=\"font-size: 11.998px;\">Se envió junto con el Ecógrafo ECO 204.</span><br>','21/3/2020 12:51:43 pm','21/3/2020 12:51:43 pm','21/3/2020 12:55:59 pm','camiloz','camiloz',NULL,'2020-03-21 00:00:00',NULL,NULL),(126,1,131,131,131,3,'2020-03-17',6,5,5,1,131,2,'Se envió junto con el Ecógrafo ECO 204.','21/3/2020 12:53:23 pm','21/3/2020 12:53:23 pm','21/3/2020 12:55:43 pm','camiloz','camiloz',NULL,'2020-03-21 00:00:00',NULL,NULL),(127,1,235,235,235,2,'2020-03-21',2,5,5,2,235,2,'<br>','21/3/2020 01:16:16 pm','21/3/2020 01:16:16 pm','21/3/2020 01:16:54 pm','camiloz','camiloz',NULL,'2020-03-21 00:00:00',NULL,NULL),(128,1,40,40,40,4,'2020-03-21',4,5,5,2,40,2,'<br>','21/3/2020 01:25:54 pm','21/3/2020 01:25:54 pm',NULL,'camiloz',NULL,NULL,'2020-03-21 00:00:00',NULL,NULL),(129,1,9,9,9,6,'2020-05-06',7,3,3,1,9,2,'Se verifica en HIDC pero no se encuentra, no existe reporte de traslado ni correo de por medio. Hoy se traslada a bodega con el fin de encontrar su ubicación.&nbsp;','6/5/2020 06:02:37 pm','6/5/2020 06:02:37 pm',NULL,'felipem',NULL,NULL,'2020-05-06 00:00:00',NULL,NULL),(130,1,56,56,56,1,'2020-05-06',12,3,3,1,56,2,'ENTREGADO A SOL PARA SU TRABAJO DE PROGRAMACION','6/5/2020 06:06:59 pm','6/5/2020 06:06:59 pm','6/5/2020 06:07:02 pm','felipem','felipem',NULL,'2020-05-06 00:00:00',NULL,NULL),(131,1,119,119,119,1,'2020-05-06',7,3,3,1,119,2,'EN ESPERA DE ACTUALIZACIÓN DE ESTADO Y UBICACIÓN&nbsp;','6/5/2020 06:08:48 pm','6/5/2020 06:08:48 pm',NULL,'felipem',NULL,NULL,'2020-05-06 00:00:00',NULL,NULL),(132,1,236,236,236,7,'2020-05-06',1,3,3,1,236,2,'REGISTRADO POR CORREO ELECTRÓNICO&nbsp;','6/5/2020 06:20:23 pm','6/5/2020 06:20:23 pm',NULL,'felipem',NULL,NULL,'2020-05-06 00:00:00',NULL,NULL),(133,1,120,120,120,1,'2020-05-06',1,3,3,1,120,2,'EN ESPERA DE VERIFICACIÓN PORQUE EL EQUIPO AL PARECER SE ENCUENTRA EN LA SALA DE COVID','6/5/2020 06:24:37 pm','6/5/2020 06:24:37 pm','6/5/2020 06:26:10 pm','felipem','felipem',NULL,'2020-05-06 00:00:00',NULL,NULL),(134,1,237,237,237,7,'2020-05-06',1,3,3,1,237,2,'VERIFICADO POR CORREO ELECTRÓNICO EN HIDC','7/5/2020 11:58:34 am','7/5/2020 11:58:34 am',NULL,'felipem',NULL,NULL,'2020-05-07 00:00:00',NULL,NULL),(135,1,105,105,105,7,'2020-05-07',1,3,3,1,105,2,'VERIFICADO POR CORREO','7/5/2020 12:01:39 pm','7/5/2020 12:01:39 pm',NULL,'felipem',NULL,NULL,'2020-05-07 00:00:00',NULL,NULL),(136,1,139,139,139,7,'2020-07-07',7,5,5,1,NULL,0,'Se envìa a reparaciòn.',NULL,NULL,'7/7/2020 07:21:16 pm','camiloz','camiloz',NULL,'7/7/2020 07:20:56 pm','2020-07-07 19:21:16',NULL),(137,1,134,134,134,7,'2020-07-07',7,5,5,1,NULL,0,'Se lleva a reparaciòn.',NULL,NULL,'7/7/2020 07:27:37 pm','camiloz','camiloz',NULL,'7/7/2020 07:27:32 pm','2020-07-07 19:27:37',NULL),(138,1,26,26,26,1,'2020-09-01',13,3,3,2,NULL,0,'Se envia a medisun esensa',NULL,NULL,NULL,'felipem',NULL,NULL,'10/9/2020 04:33:14 pm',NULL,NULL),(139,1,9,9,9,1,'2020-09-10',13,3,3,1,NULL,0,'Yo envié Felipe&nbsp;',NULL,NULL,NULL,'superadmin',NULL,'15997749791773665205687412259682.jpg','10/9/2020 04:56:46 pm',NULL,NULL),(140,1,244,244,244,7,'2020-10-01',6,3,3,1,NULL,0,'Enviado con el Dr Huberth Ruiz',NULL,NULL,NULL,'superadmin',NULL,NULL,'1/10/2020 04:43:14 pm',NULL,NULL),(141,1,245,245,245,7,'2020-10-01',6,3,3,1,NULL,0,'Enviado con el Dr Huberth Ruiz',NULL,NULL,NULL,'superadmin',NULL,NULL,'1/10/2020 04:46:58 pm',NULL,NULL);
/*!40000 ALTER TABLE `movimientos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_calibracion`
--

DROP TABLE IF EXISTS `tipo_calibracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_calibracion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_calibracion` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo_calibracion_unique` (`tipo_calibracion`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_calibracion`
--

LOCK TABLES `tipo_calibracion` WRITE;
/*!40000 ALTER TABLE `tipo_calibracion` DISABLE KEYS */;
INSERT INTO `tipo_calibracion` VALUES (1,'CALIBRACIÓN PROGRAMADA','Calibración realizada en periodos determinados por la ficha técnica del equipo'),(2,'CALIBRACION CORRECTIVA','Calibración realizada a demanda del dispositivo');
/*!40000 ALTER TABLE `tipo_calibracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_contacto`
--

DROP TABLE IF EXISTS `tipo_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_contacto` (
  `id_tipo_contacto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_contacto` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_tipo_contacto`),
  UNIQUE KEY `tipo_contacto_unique` (`tipo_contacto`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_contacto`
--

LOCK TABLES `tipo_contacto` WRITE;
/*!40000 ALTER TABLE `tipo_contacto` DISABLE KEYS */;
INSERT INTO `tipo_contacto` VALUES (1,'PERSONA','Persona natural'),(2,'EMPRESA','Persona jurídica o empresa');
/*!40000 ALTER TABLE `tipo_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_dispositivo`
--

DROP TABLE IF EXISTS `tipo_dispositivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_dispositivo` (
  `id_tipodispo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_dispositivo` varchar(40) NOT NULL,
  `grupo_dispo` int(10) unsigned NOT NULL,
  `prefijo` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_tipodispo`),
  UNIQUE KEY `tipo_dispositivo_unique` (`tipo_dispositivo`),
  KEY `grupo_dispo` (`grupo_dispo`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_dispositivo`
--

LOCK TABLES `tipo_dispositivo` WRITE;
/*!40000 ALTER TABLE `tipo_dispositivo` DISABLE KEYS */;
INSERT INTO `tipo_dispositivo` VALUES (1,'HOLTER',3,'HOLTER','DESDE 101 - HASTA 200'),(2,'MAPA',3,'MAPA','DESDE 551 - HASTA 600'),(3,'ECOGRAFO',3,'ECO','DESDE 201 - HASTA 250'),(4,'DESFIBRILADOR',3,'DES','DESDE 451 - HASTA 500'),(5,'ECG',3,'ECG','DESDE 301 - HASTA 350'),(6,'STRESS TEST',3,'STR','DESDE 751 - HASTA 800'),(7,'BANDA TROTADORA',3,'BT','Banda trotadora de cualquier versión'),(8,'TENSIOMETRO',3,'SM',NULL),(9,'FONENDOSCOPIO',3,'SM',NULL),(10,'CABLE ECG',1,'CAB','DESDE 351 - HASTA 400'),(11,'COMPUTADOR',3,'PC','DESDE 601\r\nHSTA 700\r\nComputadores todo en uno, Computadores portatiles, Laptop, computadores de mesa, computadores de escritorio, computadores grandes con solo la pantalla, computadores con torre minuatura.'),(13,'TRANSDUCTOR',1,'TR','DESDE 251 - HASTA 300'),(14,'ELECTROCARDIOGRAFO',3,'ECG','Electrocardiógrafos'),(15,'DIGITALIZADOR',1,'DG','DESDE 401 - HASTA 450'),(16,'MEMORIA USB',1,'USB','DESDE 901 - HASTA 950'),(17,'IMPRESORA',1,'SM','DESDE 801 - HASTA 850'),(18,'ASPIRADOR',3,'SM','Dispositivo de absorción o aspirador'),(19,'BOMBA DE INFUSIÓN',3,'SM',NULL),(20,'LECTOR DE HUELLA',3,'SM',NULL),(21,'TELÉFONO CELULAR',3,'CEL',NULL),(23,'SIN ASIGNAR',3,'LIBRE','Numero sin asignar');
/*!40000 ALTER TABLE `tipo_dispositivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `id_tipodoc` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_doc` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_tipodoc`),
  UNIQUE KEY `tipo_doc_unique` (`tipo_doc`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` VALUES (1,'FACTURA','Factura de compra'),(2,'HOJA DE VIDA','Hoja de vida del dispositivo'),(3,'REGISTRO INVIMA','Registro invima'),(4,'CRONOGRAMA DE MANTENIMIENTO','Cronograma de mantenimiento'),(5,'DECLARACIÓN DE IMPORTACION','Declaración de importación'),(6,'FICHA TÉCNICA','Ficha técnica o documento de propiedades técnicas del equipo o dispositivo'),(7,'MANUAL DE USUARIO','Manual de usuario'),(8,'MANUAL DE SERVICIO','Manual de servicio del dispositivo'),(9,'PROTOCOLO DE LIMPIEZA Y DESINFECCIÓN','Protocolo, guía o manual deimpieza del equipo'),(10,'DXDIAG','Documento de información del sistema');
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estado_dispo`
--

DROP TABLE IF EXISTS `tipo_estado_dispo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estado_dispo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado_dispo` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estado_dispo`
--

LOCK TABLES `tipo_estado_dispo` WRITE;
/*!40000 ALTER TABLE `tipo_estado_dispo` DISABLE KEYS */;
INSERT INTO `tipo_estado_dispo` VALUES (1,'ACTIVO','Dispositivo funcionando correctamente'),(2,'INACTIVO','Dispositivo en verificación y/o reparación'),(3,'FUERA DE SERVICIO','Equipo dañado'),(4,'SIN ASIGNAR',NULL);
/*!40000 ALTER TABLE `tipo_estado_dispo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estado_movi`
--

DROP TABLE IF EXISTS `tipo_estado_movi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estado_movi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado_movi` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `estado_movi_unique` (`estado_movi`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estado_movi`
--

LOCK TABLES `tipo_estado_movi` WRITE;
/*!40000 ALTER TABLE `tipo_estado_movi` DISABLE KEYS */;
INSERT INTO `tipo_estado_movi` VALUES (1,'ENVIADO','Dispositivo enviado'),(2,'RECIBIDO','Dispositivo recibido');
/*!40000 ALTER TABLE `tipo_estado_movi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estado_verifica`
--

DROP TABLE IF EXISTS `tipo_estado_verifica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estado_verifica` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `estado_verificado` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `estado_verificado_unique` (`estado_verificado`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estado_verifica`
--

LOCK TABLES `tipo_estado_verifica` WRITE;
/*!40000 ALTER TABLE `tipo_estado_verifica` DISABLE KEYS */;
INSERT INTO `tipo_estado_verifica` VALUES (1,'ENVIADO - En espera','Cuando el dispositivo de ha enviado y se encuentra en espera de la recepción'),(2,'RECIBIDO - Verificado','Cuando el dispositivo ha sido recibido y verificado, se debe ingresar a actualizar el registro de la nueva ubicación en el dispositivo');
/*!40000 ALTER TABLE `tipo_estado_verifica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_grupo_mobilia`
--

DROP TABLE IF EXISTS `tipo_grupo_mobilia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_grupo_mobilia` (
  `id_gru_mo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grupo` varchar(40) DEFAULT NULL,
  `prefijo` varchar(40) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_gru_mo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_grupo_mobilia`
--

LOCK TABLES `tipo_grupo_mobilia` WRITE;
/*!40000 ALTER TABLE `tipo_grupo_mobilia` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_grupo_mobilia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_iden`
--

DROP TABLE IF EXISTS `tipo_iden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_iden` (
  `id_iden` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_iden` varchar(40) NOT NULL,
  `tipo_iden_abre` varchar(40) NOT NULL,
  PRIMARY KEY (`id_iden`),
  UNIQUE KEY `tipo_iden_unique` (`tipo_iden`),
  UNIQUE KEY `tipo_iden_abre_unique` (`tipo_iden_abre`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_iden`
--

LOCK TABLES `tipo_iden` WRITE;
/*!40000 ALTER TABLE `tipo_iden` DISABLE KEYS */;
INSERT INTO `tipo_iden` VALUES (1,'Cédula de ciudadanía','CC'),(2,'Número de identificación tributaria','NIT'),(3,'Identificación','ID');
/*!40000 ALTER TABLE `tipo_iden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_mobiliario`
--

DROP TABLE IF EXISTS `tipo_mobiliario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_mobiliario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_mobiliario` varchar(40) NOT NULL,
  `descripcion` text,
  `grupo_mobi` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grupo_mobi` (`grupo_mobi`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_mobiliario`
--

LOCK TABLES `tipo_mobiliario` WRITE;
/*!40000 ALTER TABLE `tipo_mobiliario` DISABLE KEYS */;
INSERT INTO `tipo_mobiliario` VALUES (1,'MESA ESCRITORIO','todo tipo de mesa o escritorio',NULL),(2,'SILLA','Todo tipo de sillas',NULL);
/*!40000 ALTER TABLE `tipo_mobiliario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_movimiento`
--

DROP TABLE IF EXISTS `tipo_movimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_movimiento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_movimiento` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo_movimiento_unique` (`tipo_movimiento`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_movimiento`
--

LOCK TABLES `tipo_movimiento` WRITE;
/*!40000 ALTER TABLE `tipo_movimiento` DISABLE KEYS */;
INSERT INTO `tipo_movimiento` VALUES (1,'ENVÍO','Envío del dispositivo'),(2,'RECEPCIÓN','Recepción del dispositivo');
/*!40000 ALTER TABLE `tipo_movimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_mtto`
--

DROP TABLE IF EXISTS `tipo_mtto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_mtto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_mtto` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipo_mtto_unique` (`tipo_mtto`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_mtto`
--

LOCK TABLES `tipo_mtto` WRITE;
/*!40000 ALTER TABLE `tipo_mtto` DISABLE KEYS */;
INSERT INTO `tipo_mtto` VALUES (1,'PREVENTIVO','Mantenimiento preventivo'),(2,'CORRECTIVO','Mantenimiento correctivo, cambio de piezas o repuestos');
/*!40000 ALTER TABLE `tipo_mtto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_razon_social`
--

DROP TABLE IF EXISTS `tipo_razon_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_razon_social` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `razon_social` varchar(40) NOT NULL,
  `nit` varchar(40) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `razon_social_unique` (`razon_social`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_razon_social`
--

LOCK TABLES `tipo_razon_social` WRITE;
/*!40000 ALTER TABLE `tipo_razon_social` DISABLE KEYS */;
INSERT INTO `tipo_razon_social` VALUES (1,'STARMEDICA SAS','900.462.329-4','Starmedica SAS'),(2,'STARMEDICAL VALLE SAS','900.713.421-2','STARMEDICAL VALLE SAS'),(3,'HUBERTH RUIZ HENAO','76304351-2','Hubert Ruiz empresa');
/*!40000 ALTER TABLE `tipo_razon_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_relacion`
--

DROP TABLE IF EXISTS `tipo_relacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_relacion` (
  `id_tiporelac` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_relac` varchar(40) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_tiporelac`),
  UNIQUE KEY `tipo_relac_unique` (`tipo_relac`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_relacion`
--

LOCK TABLES `tipo_relacion` WRITE;
/*!40000 ALTER TABLE `tipo_relacion` DISABLE KEYS */;
INSERT INTO `tipo_relacion` VALUES (1,'PROVEEDOR','Proveedor de productos o servicios'),(2,'MIEMBRO','Miembro de la empresa'),(3,'ASISTENTE','Asistente de apoyo');
/*!40000 ALTER TABLE `tipo_relacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades` (
  `id_unidades` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `abreviado` varchar(40) NOT NULL,
  `ciudad` int(10) unsigned DEFAULT NULL,
  `telefono` varchar(40) DEFAULT NULL,
  `razon_social` int(10) unsigned DEFAULT NULL,
  `logo` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_unidades`),
  UNIQUE KEY `nombre_unique` (`nombre`),
  UNIQUE KEY `abreviado_unique` (`abreviado`),
  KEY `ciudad` (`ciudad`),
  KEY `razon_social` (`razon_social`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidades`
--

LOCK TABLES `unidades` WRITE;
/*!40000 ALTER TABLE `unidades` DISABLE KEYS */;
INSERT INTO `unidades` VALUES (1,'HOSPITAL ISAIAS DUARTE CANCINO','HIDC',1,'3148835680',1,NULL),(2,'HOSPITAL MARIO CORREA','HMC',1,'3135507451',1,NULL),(3,'HOSPITAL RAUL OREJUELA BUENO','HROB',4,'3147281502',2,NULL),(4,'HOSPITAL TOMAS URIBE','HTU',3,'3127927840',1,NULL),(5,'CLÍNICA SAN FRANCISCO','CSF',3,'3166261217',1,NULL),(6,'FUNDACIÓN HOSPITAL SAN JOSÉ','FHSJ',2,'3135495504',1,NULL),(7,'BODEGA','BDG',1,NULL,NULL,NULL),(8,'ÁREA DE PROGRAMACIÓN','SOL BECERRA',1,NULL,2,NULL),(9,'PROFAMILIA VIVA','PRO',1,NULL,1,NULL),(10,'HOSPITAL FRANCISCO DE PAULA SANTANDER','HFPS',1,NULL,1,NULL),(11,'OFICINA PRINCIPAL LA REFORMA','SM REFORMA',1,'3144110208',1,NULL),(12,'DOTACIÓN','DO',1,NULL,1,NULL),(13,'MEDISUN ESENSA','SUME',1,NULL,1,NULL);
/*!40000 ALTER TABLE `unidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificacion`
--

DROP TABLE IF EXISTS `verificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` int(10) unsigned DEFAULT NULL,
  `tipo_dispo` int(10) unsigned DEFAULT NULL,
  `serial` int(10) unsigned DEFAULT NULL,
  `ubicacion` int(10) unsigned DEFAULT NULL,
  `verificacion` varchar(40) NOT NULL,
  `comentario` text,
  `verifi_por` varchar(40) DEFAULT NULL,
  `fecha_verifi` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `codigo` (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificacion`
--

LOCK TABLES `verificacion` WRITE;
/*!40000 ALTER TABLE `verificacion` DISABLE KEYS */;
INSERT INTO `verificacion` VALUES (1,1,1,1,1,'1',NULL,'felipem','29/4/2020 04:35:47 pm'),(2,18,18,18,18,'1',NULL,'crueda','30/4/2020 02:25:06 pm'),(3,21,21,21,21,'1',NULL,'crueda','30/4/2020 02:25:45 pm'),(4,22,22,22,22,'1',NULL,'crueda','30/4/2020 02:26:11 pm'),(5,25,25,25,25,'1',NULL,'crueda','30/4/2020 02:26:52 pm'),(6,199,199,199,199,'1',NULL,'hrestrepo','30/4/2020 02:55:19 pm'),(7,202,202,202,202,'1',NULL,'hrestrepo','30/4/2020 02:56:01 pm'),(8,203,203,203,203,'1',NULL,'hrestrepo','30/4/2020 02:57:23 pm'),(9,67,67,67,67,'1',NULL,'hrestrepo','30/4/2020 03:00:11 pm'),(10,68,68,68,68,'1',NULL,'hrestrepo','30/4/2020 03:01:08 pm'),(11,89,89,89,89,'1',NULL,'hrestrepo','30/4/2020 03:07:47 pm'),(12,96,96,96,96,'1',NULL,'hrestrepo','30/4/2020 03:08:25 pm'),(13,97,97,97,97,'1',NULL,'hrestrepo','30/4/2020 03:09:44 pm'),(14,86,86,86,86,'1',NULL,'hrestrepo','30/4/2020 03:10:33 pm'),(15,209,209,209,209,'1',NULL,'hrestrepo','30/4/2020 03:11:36 pm'),(16,129,129,129,129,'1',NULL,'hrestrepo','30/4/2020 03:15:19 pm'),(17,234,234,234,234,'1',NULL,'crueda','30/4/2020 03:15:21 pm'),(18,221,221,221,221,'1',NULL,'hrestrepo','30/4/2020 03:15:56 pm'),(19,233,233,233,233,'1',NULL,'crueda','30/4/2020 03:16:12 pm'),(20,232,232,232,232,'1',NULL,'crueda','30/4/2020 03:17:26 pm'),(21,140,140,140,140,'1',NULL,'hrestrepo','30/4/2020 03:18:58 pm'),(22,231,231,231,231,'1',NULL,'crueda','30/4/2020 03:19:15 pm'),(23,111,111,111,111,'1',NULL,'hrestrepo','30/4/2020 03:21:38 pm'),(24,40,40,40,40,'1',NULL,'hrestrepo','30/4/2020 03:25:07 pm'),(25,230,230,230,230,'1','esta banda con ese numero no esta en la unidad le envió al correo numero y serial de las dos bandas que manejamos aca en la unidad','crueda','30/4/2020 03:31:14 pm'),(26,55,55,55,55,'1',NULL,'hrestrepo','30/4/2020 03:34:39 pm'),(27,2,2,2,2,'1','VERIFICADO.','jvilla','2/5/2020 09:29:53 am'),(28,9,9,9,9,'1','SE REALIZA VERIFICACIÓN DE ESTE DISPOSITIVO Y SE EVIDENCIA QUE NO SE ENCUENTRA EN LA UNIDAD.','jvilla','2/5/2020 09:38:35 am'),(29,17,17,17,17,'1','VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 09:39:14 am'),(30,26,26,26,26,'1','VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 09:40:00 am'),(31,49,49,49,49,'1','VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 09:42:12 am'),(32,59,59,59,59,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 09:54:19 am'),(33,60,60,60,60,'1','EQUIPO VERIFICADO CORRECTAMENTE','jvilla','2/5/2020 09:55:05 am'),(34,82,82,82,82,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 09:57:24 am'),(35,87,87,87,87,'1','EQUIPO VERIFICADO CORRECTAMENTE','jvilla','2/5/2020 09:57:51 am'),(36,124,124,124,124,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:20:13 am'),(37,158,158,158,158,'1','EQUIPO  VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:25:35 am'),(38,165,165,165,165,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:27:07 am'),(39,172,172,172,172,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:27:51 am'),(40,226,226,226,226,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:28:18 am'),(41,229,229,229,229,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:29:34 am'),(42,80,80,80,80,'1','EQUIPO VERIFICADO CORRECTAMENTE.','jvilla','2/5/2020 10:30:19 am'),(43,57,57,57,57,'1','EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 08:43:55 am'),(44,58,58,58,58,'1','EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 08:45:06 am'),(45,88,88,88,88,'1','ESTE EQUIPO NO SE ENCUENTRA EN LA UNIDAD POR ESTE MOTIVO NO SE PUDO VERIFICAR.','ibet','8/5/2020 08:52:02 am'),(46,238,238,238,238,'1',NULL,'felipem','8/5/2020 08:54:48 am'),(47,90,90,90,90,'1','EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 08:56:43 am'),(48,93,93,93,93,'1','EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:00:24 am'),(49,171,171,171,171,'1','ESTE EQUIPO NO SE ENCUENTRA EN LA UNIDAD POR ESTE MOTIVO NO SE PUDO VERIFICAR.','ibet','8/5/2020 09:08:14 am'),(50,216,216,216,216,'1','ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:10:08 am'),(51,217,217,217,217,'1','ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:11:38 am'),(52,218,218,218,218,'1','ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:13:40 am'),(53,219,219,219,219,'1','ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:16:00 am'),(54,220,220,220,220,'1','ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:17:11 am'),(55,92,92,92,92,'1','ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.','ibet','8/5/2020 09:34:51 am'),(56,203,203,203,203,'1','Flm','superadmin','8/10/2020 02:40:25 pm'),(57,202,202,202,202,'1','Flm','superadmin','8/10/2020 02:41:18 pm'),(58,21,21,21,21,'1',NULL,'superadmin','8/10/2020 02:42:03 pm'),(59,25,25,25,25,'1','Flm','superadmin','8/10/2020 02:43:33 pm'),(60,208,208,208,208,'1','Verificado hoy dentro de la unidad HTU, MAPA CON SPO','superadmin','8/10/2020 02:59:24 pm'),(61,209,209,209,209,'1','Verificado en HTU, equipo con SPO','superadmin','8/10/2020 03:00:26 pm'),(62,246,246,246,246,'1','flm','superadmin','8/10/2020 03:08:51 pm'),(63,91,91,91,91,'1','flm','superadmin','8/10/2020 03:15:56 pm'),(64,84,84,84,84,'1','flm','superadmin','8/10/2020 03:33:23 pm'),(65,230,230,230,230,'1','Se ha marcado el con pirograbador','superadmin','15/10/2020 02:36:11 pm');
/*!40000 ALTER TABLE `verificacion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-15 18:01:48
