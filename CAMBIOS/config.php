<?php
	$dbServer = 'localhost';
	$dbUsername = 'starmedi_star';
	$dbPassword = 'cardio12345';
	$dbDatabase = 'starmedi_bd_equipos';
	$appURI = '';
	$host = 'inventario.starmedica.co';

	$adminConfig = array(
		'adminUsername' => "felipem",
		'adminPassword' => "\$2y\$10\$rmdod31npGwE.1/ItHglDuki0KwcwqhHh6mOT6Q3WIYUfkxDboRFy",
		'notifyAdminNewMembers' => "0",
		'defaultSignUp' => "0",
		'anonymousGroup' => "anonymous",
		'anonymousMember' => "guest",
		'groupsPerPage' => "10",
		'membersPerPage' => "10",
		'recordsPerPage' => "10",
		'custom1' => "Nombre Completo",
		'custom2' => "Grupo",
		'custom3' => "Ciudad",
		'custom4' => "Empresa",
		'MySQLDateFormat' => "%d/%m/%Y",
		'PHPDateFormat' => "j/n/Y",
		'PHPDateTimeFormat' => "d/m/Y, h:i a",
		'senderName' => "Membership management",
		'senderEmail' => "tecnologia@starmedica.com.co",
		'approvalSubject' => "Tu acceso a Inventario Starmedica ha sido aprobado",
		'approvalMessage' => "Hola,\r\n\r\nTu acceso a Inventario Starmedica ha sido aprobado:\r\nhttp://inventario.starmedica.co\r\n\r\nTu contraseña inicial es 12345\r\n\r\nSaludos,\r\nAdmin",
		'hide_twitter_feed' => "1",
		'maintenance_mode_message' => "<b>Our website is currently down for maintenance</b><br>\r\nWe expect to be back in a couple hours. Thanks for your patience.",
		'mail_function' => "mail",
		'smtp_server' => "",
		'smtp_encryption' => "",
		'smtp_port' => "25",
		'smtp_user' => "",
		'smtp_pass' => ""
	);