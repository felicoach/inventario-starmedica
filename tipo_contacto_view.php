<?php
// This script and data application were generated by AppGini 5.96
// Download AppGini for free from https://bigprof.com/appgini/download/

	$currDir = dirname(__FILE__);
	include_once("{$currDir}/lib.php");
	@include_once("{$currDir}/hooks/tipo_contacto.php");
	include_once("{$currDir}/tipo_contacto_dml.php");

	// mm: can the current member access this page?
	$perm = getTablePermissions('tipo_contacto');
	if(!$perm['access']) {
		echo error_message($Translation['tableAccessDenied']);
		exit;
	}

	$x = new DataList;
	$x->TableName = 'tipo_contacto';

	// Fields that can be displayed in the table view
	$x->QueryFieldsTV = [
		"`tipo_contacto`.`id_tipo_contacto`" => "id_tipo_contacto",
		"`tipo_contacto`.`tipo_contacto`" => "tipo_contacto",
		"`tipo_contacto`.`descripcion`" => "descripcion",
	];
	// mapping incoming sort by requests to actual query fields
	$x->SortFields = [
		1 => '`tipo_contacto`.`id_tipo_contacto`',
		2 => 2,
		3 => 3,
	];

	// Fields that can be displayed in the csv file
	$x->QueryFieldsCSV = [
		"`tipo_contacto`.`id_tipo_contacto`" => "id_tipo_contacto",
		"`tipo_contacto`.`tipo_contacto`" => "tipo_contacto",
		"`tipo_contacto`.`descripcion`" => "descripcion",
	];
	// Fields that can be filtered
	$x->QueryFieldsFilters = [
		"`tipo_contacto`.`id_tipo_contacto`" => "ID",
		"`tipo_contacto`.`tipo_contacto`" => "Tipo de contacto",
		"`tipo_contacto`.`descripcion`" => "Descripcion",
	];

	// Fields that can be quick searched
	$x->QueryFieldsQS = [
		"`tipo_contacto`.`id_tipo_contacto`" => "id_tipo_contacto",
		"`tipo_contacto`.`tipo_contacto`" => "tipo_contacto",
		"`tipo_contacto`.`descripcion`" => "descripcion",
	];

	// Lookup fields that can be used as filterers
	$x->filterers = [];

	$x->QueryFrom = "`tipo_contacto` ";
	$x->QueryWhere = '';
	$x->QueryOrder = '';

	$x->AllowSelection = 1;
	$x->HideTableView = ($perm['view'] == 0 ? 1 : 0);
	$x->AllowDelete = $perm['delete'];
	$x->AllowMassDelete = (getLoggedAdmin() !== false);
	$x->AllowInsert = $perm['insert'];
	$x->AllowUpdate = $perm['edit'];
	$x->SeparateDV = 1;
	$x->AllowDeleteOfParents = 0;
	$x->AllowFilters = (getLoggedAdmin() !== false);
	$x->AllowSavingFilters = (getLoggedAdmin() !== false);
	$x->AllowSorting = 1;
	$x->AllowNavigation = 1;
	$x->AllowPrinting = 1;
	$x->AllowPrintingDV = 1;
	$x->AllowCSV = 1;
	$x->RecordsPerPage = 10;
	$x->QuickSearch = 1;
	$x->QuickSearchText = $Translation['quick search'];
	$x->ScriptFileName = 'tipo_contacto_view.php';
	$x->RedirectAfterInsert = 'tipo_contacto_view.php?SelectedID=#ID#';
	$x->TableTitle = 'Tipo de contacto';
	$x->TableIcon = 'resources/table_icons/cog_add.png';
	$x->PrimaryKey = '`tipo_contacto`.`id_tipo_contacto`';

	$x->ColWidth = [150, 150, ];
	$x->ColCaption = ['Tipo de contacto', 'Descripcion', ];
	$x->ColFieldName = ['tipo_contacto', 'descripcion', ];
	$x->ColNumber  = [2, 3, ];

	// template paths below are based on the app main directory
	$x->Template = 'templates/tipo_contacto_templateTV.html';
	$x->SelectedTemplate = 'templates/tipo_contacto_templateTVS.html';
	$x->TemplateDV = 'templates/tipo_contacto_templateDV.html';
	$x->TemplateDVP = 'templates/tipo_contacto_templateDVP.html';

	$x->ShowTableHeader = 1;
	$x->TVClasses = "";
	$x->DVClasses = "";
	$x->HasCalculatedFields = false;
	$x->AllowConsoleLog = false;
	$x->AllowDVNavigation = true;

	// mm: build the query based on current member's permissions
	$DisplayRecords = $_REQUEST['DisplayRecords'];
	if(!in_array($DisplayRecords, ['user', 'group'])) { $DisplayRecords = 'all'; }
	if($perm['view'] == 1 || ($perm['view'] > 1 && $DisplayRecords == 'user' && !$_REQUEST['NoFilter_x'])) { // view owner only
		$x->QueryFrom .= ', `membership_userrecords`';
		$x->QueryWhere = "WHERE `tipo_contacto`.`id_tipo_contacto`=`membership_userrecords`.`pkValue` AND `membership_userrecords`.`tableName`='tipo_contacto' AND LCASE(`membership_userrecords`.`memberID`)='" . getLoggedMemberID() . "'";
	} elseif($perm['view'] == 2 || ($perm['view'] > 2 && $DisplayRecords == 'group' && !$_REQUEST['NoFilter_x'])) { // view group only
		$x->QueryFrom .= ', `membership_userrecords`';
		$x->QueryWhere = "WHERE `tipo_contacto`.`id_tipo_contacto`=`membership_userrecords`.`pkValue` AND `membership_userrecords`.`tableName`='tipo_contacto' AND `membership_userrecords`.`groupID`='" . getLoggedGroupID() . "'";
	} elseif($perm['view'] == 3) { // view all
		// no further action
	} elseif($perm['view'] == 0) { // view none
		$x->QueryFields = ['Not enough permissions' => 'NEP'];
		$x->QueryFrom = '`tipo_contacto`';
		$x->QueryWhere = '';
		$x->DefaultSortField = '';
	}
	// hook: tipo_contacto_init
	$render = true;
	if(function_exists('tipo_contacto_init')) {
		$args = [];
		$render = tipo_contacto_init($x, getMemberInfo(), $args);
	}

	if($render) $x->Render();

	// hook: tipo_contacto_header
	$headerCode = '';
	if(function_exists('tipo_contacto_header')) {
		$args = [];
		$headerCode = tipo_contacto_header($x->ContentType, getMemberInfo(), $args);
	}

	if(!$headerCode) {
		include_once("{$currDir}/header.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/header.php");
		echo str_replace('<%%HEADER%%>', ob_get_clean(), $headerCode);
	}

	echo $x->HTML;

	// hook: tipo_contacto_footer
	$footerCode = '';
	if(function_exists('tipo_contacto_footer')) {
		$args = [];
		$footerCode = tipo_contacto_footer($x->ContentType, getMemberInfo(), $args);
	}

	if(!$footerCode) {
		include_once("{$currDir}/footer.php"); 
	} else {
		ob_start();
		include_once("{$currDir}/footer.php");
		echo str_replace('<%%FOOTER%%>', ob_get_clean(), $footerCode);
	}
