<?php

// Data functions (insert, update, delete, form) for table imagenes

// This script and data application were generated by AppGini 5.92
// Download AppGini for free from https://bigprof.com/appgini/download/

function imagenes_insert(&$error_message = '') {
	global $Translation;

	// mm: can member insert record?
	$arrPerm = getTablePermissions('imagenes');
	if(!$arrPerm['insert']) return false;

	$data = [
		'imagen' => Request::fileUpload('imagen', [
			'maxSize' => 2048000,
			'types' => 'jpg|jpeg|gif|png',
			'noRename' => false,
			'dir' => '',
			'success' => function($name, $selected_id) {
				createThumbnail($name, getThumbnailSpecs('imagenes', 'imagen', 'tv'));
			},
			'failure' => function($selected_id, $fileRemoved) {
				if(!isset($_REQUEST['SelectedID'])) return '';

				/* for empty upload fields, when saving a copy of an existing record, copy the original upload field */
				return existing_value('imagenes', 'imagen', $_REQUEST['SelectedID']);
			},
		]),
		'tipo_dispo' => Request::val('tipo_dispo', ''),
		'marca' => Request::val('marca', ''),
		'modelo' => Request::val('modelo', ''),
		'descripcion' => Request::val('descripcion', ''),
	];

	if($data['imagen'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Imagen': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	if($data['tipo_dispo'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Tipo dispo': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	if($data['marca'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Marca': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	if($data['modelo'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Modelo': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}

	// hook: imagenes_before_insert
	if(function_exists('imagenes_before_insert')) {
		$args = [];
		if(!imagenes_before_insert($data, getMemberInfo(), $args)) {
			if(isset($args['error_message'])) $error_message = $args['error_message'];
			return false;
		}
	}

	$error = '';
	// set empty fields to NULL
	$data = array_map(function($v) { return ($v === '' ? NULL : $v); }, $data);
	insert('imagenes', backtick_keys_once($data), $error);
	if($error)
		die("{$error}<br><a href=\"#\" onclick=\"history.go(-1);\">{$Translation['< back']}</a>");

	$recID = db_insert_id(db_link());

	update_calc_fields('imagenes', $recID, calculated_fields()['imagenes']);

	// hook: imagenes_after_insert
	if(function_exists('imagenes_after_insert')) {
		$res = sql("SELECT * FROM `imagenes` WHERE `id`='" . makeSafe($recID, false) . "' LIMIT 1", $eo);
		if($row = db_fetch_assoc($res)) {
			$data = array_map('makeSafe', $row);
		}
		$data['selectedID'] = makeSafe($recID, false);
		$args=[];
		if(!imagenes_after_insert($data, getMemberInfo(), $args)) { return $recID; }
	}

	// mm: save ownership data
	set_record_owner('imagenes', $recID, getLoggedMemberID());

	// if this record is a copy of another record, copy children if applicable
	if(!empty($_REQUEST['SelectedID'])) imagenes_copy_children($recID, $_REQUEST['SelectedID']);

	return $recID;
}

function imagenes_copy_children($destination_id, $source_id) {
	global $Translation;
	$requests = []; // array of curl handlers for launching insert requests
	$eo = ['silentErrors' => true];
	$uploads_dir = realpath(dirname(__FILE__) . '/../' . $Translation['ImageFolder']);
	$safe_sid = makeSafe($source_id);

	// launch requests, asynchronously
	curl_batch($requests);
}

function imagenes_delete($selected_id, $AllowDeleteOfParents = false, $skipChecks = false) {
	// insure referential integrity ...
	global $Translation;
	$selected_id = makeSafe($selected_id);

	// mm: can member delete record?
	if(!check_record_permission('imagenes', $selected_id, 'delete')) {
		return $Translation['You don\'t have enough permissions to delete this record'];
	}

	// hook: imagenes_before_delete
	if(function_exists('imagenes_before_delete')) {
		$args = [];
		if(!imagenes_before_delete($selected_id, $skipChecks, getMemberInfo(), $args))
			return $Translation['Couldn\'t delete this record'] . (
				!empty($args['error_message']) ?
					'<div class="text-bold">' . strip_tags($args['error_message']) . '</div>'
					: '' 
			);
	}

	sql("DELETE FROM `imagenes` WHERE `id`='{$selected_id}'", $eo);

	// hook: imagenes_after_delete
	if(function_exists('imagenes_after_delete')) {
		$args = [];
		imagenes_after_delete($selected_id, getMemberInfo(), $args);
	}

	// mm: delete ownership data
	sql("DELETE FROM `membership_userrecords` WHERE `tableName`='imagenes' AND `pkValue`='{$selected_id}'", $eo);
}

function imagenes_update(&$selected_id, &$error_message = '') {
	global $Translation;

	// mm: can member edit record?
	if(!check_record_permission('imagenes', $selected_id, 'edit')) return false;

	$data = [
		'imagen' => Request::fileUpload('imagen', [
			'maxSize' => 2048000,
			'types' => 'jpg|jpeg|gif|png',
			'noRename' => false,
			'dir' => '',
			'id' => $selected_id,
			'success' => function($name, $selected_id) {
				createThumbnail($name, getThumbnailSpecs('imagenes', 'imagen', 'tv'));
			},
			'failure' => function($selected_id, $fileRemoved) {
				if($fileRemoved) return '';
				return existing_value('imagenes', 'imagen', $selected_id);
			},
		]),
		'tipo_dispo' => Request::val('tipo_dispo', ''),
		'marca' => Request::val('marca', ''),
		'modelo' => Request::val('modelo', ''),
		'descripcion' => Request::val('descripcion', ''),
	];

	if($data['imagen'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Imagen': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	if($data['tipo_dispo'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Tipo dispo': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	if($data['marca'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Marca': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	if($data['modelo'] === '') {
		echo StyleSheet() . "\n\n<div class=\"alert alert-danger\">{$Translation['error:']} 'Modelo': {$Translation['field not null']}<br><br>";
		echo '<a href="" onclick="history.go(-1); return false;">' . $Translation['< back'] . '</a></div>';
		exit;
	}
	// get existing values
	$old_data = getRecord('imagenes', $selected_id);
	if(is_array($old_data)) {
		$old_data = array_map('makeSafe', $old_data);
		$old_data['selectedID'] = makeSafe($selected_id);
	}

	$data['selectedID'] = makeSafe($selected_id);

	// hook: imagenes_before_update
	if(function_exists('imagenes_before_update')) {
		$args = ['old_data' => $old_data];
		if(!imagenes_before_update($data, getMemberInfo(), $args)) {
			if(isset($args['error_message'])) $error_message = $args['error_message'];
			return false;
		}
	}

	$set = $data; unset($set['selectedID']);
	foreach ($set as $field => $value) {
		$set[$field] = ($value !== '' && $value !== NULL) ? $value : NULL;
	}

	if(!update(
		'imagenes', 
		backtick_keys_once($set), 
		['`id`' => $selected_id], 
		$error_message
	)) {
		echo $error_message;
		echo '<a href="imagenes_view.php?SelectedID=' . urlencode($selected_id) . "\">{$Translation['< back']}</a>";
		exit;
	}


	$eo = ['silentErrors' => true];

	update_calc_fields('imagenes', $data['selectedID'], calculated_fields()['imagenes']);

	// hook: imagenes_after_update
	if(function_exists('imagenes_after_update')) {
		$res = sql("SELECT * FROM `imagenes` WHERE `id`='{$data['selectedID']}' LIMIT 1", $eo);
		if($row = db_fetch_assoc($res)) $data = array_map('makeSafe', $row);

		$data['selectedID'] = $data['id'];
		$args = ['old_data' => $old_data];
		if(!imagenes_after_update($data, getMemberInfo(), $args)) return;
	}

	// mm: update ownership data
	sql("UPDATE `membership_userrecords` SET `dateUpdated`='" . time() . "' WHERE `tableName`='imagenes' AND `pkValue`='" . makeSafe($selected_id) . "'", $eo);
}

function imagenes_form($selected_id = '', $AllowUpdate = 1, $AllowInsert = 1, $AllowDelete = 1, $ShowCancel = 0, $TemplateDV = '', $TemplateDVP = '') {
	// function to return an editable form for a table records
	// and fill it with data of record whose ID is $selected_id. If $selected_id
	// is empty, an empty form is shown, with only an 'Add New'
	// button displayed.

	global $Translation;

	// mm: get table permissions
	$arrPerm = getTablePermissions('imagenes');
	if(!$arrPerm['insert'] && $selected_id=='') { return ''; }
	$AllowInsert = ($arrPerm['insert'] ? true : false);
	// print preview?
	$dvprint = false;
	if($selected_id && $_REQUEST['dvprint_x'] != '') {
		$dvprint = true;
	}

	$filterer_tipo_dispo = thisOr($_REQUEST['filterer_tipo_dispo'], '');
	$filterer_marca = thisOr($_REQUEST['filterer_marca'], '');
	$filterer_modelo = thisOr($_REQUEST['filterer_modelo'], '');

	// populate filterers, starting from children to grand-parents
	if($filterer_modelo && !$filterer_marca) $filterer_marca = sqlValue("select marca from marca_modelo where id_mmodelo='" . makeSafe($filterer_modelo) . "'");

	// unique random identifier
	$rnd1 = ($dvprint ? rand(1000000, 9999999) : '');
	// combobox: tipo_dispo
	$combo_tipo_dispo = new DataCombo;
	// combobox: marca
	$combo_marca = new DataCombo;
	// combobox: modelo, filterable by: marca
	$combo_modelo = new DataCombo;

	if($selected_id) {
		// mm: check member permissions
		if(!$arrPerm['view']) return '';

		// mm: who is the owner?
		$ownerGroupID = sqlValue("SELECT `groupID` FROM `membership_userrecords` WHERE `tableName`='imagenes' AND `pkValue`='" . makeSafe($selected_id) . "'");
		$ownerMemberID = sqlValue("SELECT LCASE(`memberID`) FROM `membership_userrecords` WHERE `tableName`='imagenes' AND `pkValue`='" . makeSafe($selected_id) . "'");

		if($arrPerm['view'] == 1 && getLoggedMemberID() != $ownerMemberID) return '';
		if($arrPerm['view'] == 2 && getLoggedGroupID() != $ownerGroupID) return '';

		// can edit?
		$AllowUpdate = 0;
		if(($arrPerm['edit'] == 1 && $ownerMemberID == getLoggedMemberID()) || ($arrPerm['edit'] == 2 && $ownerGroupID == getLoggedGroupID()) || $arrPerm['edit'] == 3) {
			$AllowUpdate = 1;
		}

		$res = sql("SELECT * FROM `imagenes` WHERE `id`='" . makeSafe($selected_id) . "'", $eo);
		if(!($row = db_fetch_array($res))) {
			return error_message($Translation['No records found'], 'imagenes_view.php', false);
		}
		$combo_tipo_dispo->SelectedData = $row['tipo_dispo'];
		$combo_marca->SelectedData = $row['marca'];
		$combo_modelo->SelectedData = $row['modelo'];
		$urow = $row; /* unsanitized data */
		$hc = new CI_Input();
		$row = $hc->xss_clean($row); /* sanitize data */
	} else {
		$combo_tipo_dispo->SelectedData = $filterer_tipo_dispo;
		$combo_marca->SelectedData = $filterer_marca;
		$combo_modelo->SelectedData = $filterer_modelo;
	}
	$combo_tipo_dispo->HTML = '<span id="tipo_dispo-container' . $rnd1 . '"></span><input type="hidden" name="tipo_dispo" id="tipo_dispo' . $rnd1 . '" value="' . html_attr($combo_tipo_dispo->SelectedData) . '">';
	$combo_tipo_dispo->MatchText = '<span id="tipo_dispo-container-readonly' . $rnd1 . '"></span><input type="hidden" name="tipo_dispo" id="tipo_dispo' . $rnd1 . '" value="' . html_attr($combo_tipo_dispo->SelectedData) . '">';
	$combo_marca->HTML = '<span id="marca-container' . $rnd1 . '"></span><input type="hidden" name="marca" id="marca' . $rnd1 . '" value="' . html_attr($combo_marca->SelectedData) . '">';
	$combo_marca->MatchText = '<span id="marca-container-readonly' . $rnd1 . '"></span><input type="hidden" name="marca" id="marca' . $rnd1 . '" value="' . html_attr($combo_marca->SelectedData) . '">';
	$combo_modelo->HTML = '<span id="modelo-container' . $rnd1 . '"></span><input type="hidden" name="modelo" id="modelo' . $rnd1 . '" value="' . html_attr($combo_modelo->SelectedData) . '">';
	$combo_modelo->MatchText = '<span id="modelo-container-readonly' . $rnd1 . '"></span><input type="hidden" name="modelo" id="modelo' . $rnd1 . '" value="' . html_attr($combo_modelo->SelectedData) . '">';

	ob_start();
	?>

	<script>
		// initial lookup values
		AppGini.current_tipo_dispo__RAND__ = { text: "", value: "<?php echo addslashes($selected_id ? $urow['tipo_dispo'] : $filterer_tipo_dispo); ?>"};
		AppGini.current_marca__RAND__ = { text: "", value: "<?php echo addslashes($selected_id ? $urow['marca'] : $filterer_marca); ?>"};
		AppGini.current_modelo__RAND__ = { text: "", value: "<?php echo addslashes($selected_id ? $urow['modelo'] : $filterer_modelo); ?>"};

		jQuery(function() {
			setTimeout(function() {
				if(typeof(tipo_dispo_reload__RAND__) == 'function') tipo_dispo_reload__RAND__();
				if(typeof(marca_reload__RAND__) == 'function') marca_reload__RAND__();
				<?php echo (!$AllowUpdate || $dvprint ? 'if(typeof(modelo_reload__RAND__) == \'function\') modelo_reload__RAND__(AppGini.current_marca__RAND__.value);' : ''); ?>
			}, 10); /* we need to slightly delay client-side execution of the above code to allow AppGini.ajaxCache to work */
		});
		function tipo_dispo_reload__RAND__() {
		<?php if(($AllowUpdate || $AllowInsert) && !$dvprint) { ?>

			$j("#tipo_dispo-container__RAND__").select2({
				/* initial default value */
				initSelection: function(e, c) {
					$j.ajax({
						url: 'ajax_combo.php',
						dataType: 'json',
						data: { id: AppGini.current_tipo_dispo__RAND__.value, t: 'imagenes', f: 'tipo_dispo' },
						success: function(resp) {
							c({
								id: resp.results[0].id,
								text: resp.results[0].text
							});
							$j('[name="tipo_dispo"]').val(resp.results[0].id);
							$j('[id=tipo_dispo-container-readonly__RAND__]').html('<span id="tipo_dispo-match-text">' + resp.results[0].text + '</span>');
							if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=tipo_dispositivo_view_parent]').hide(); } else { $j('.btn[id=tipo_dispositivo_view_parent]').show(); }


							if(typeof(tipo_dispo_update_autofills__RAND__) == 'function') tipo_dispo_update_autofills__RAND__();
						}
					});
				},
				width: '100%',
				formatNoMatches: function(term) { /* */ return '<?php echo addslashes($Translation['No matches found!']); ?>'; },
				minimumResultsForSearch: 5,
				loadMorePadding: 200,
				ajax: {
					url: 'ajax_combo.php',
					dataType: 'json',
					cache: true,
					data: function(term, page) { /* */ return { s: term, p: page, t: 'imagenes', f: 'tipo_dispo' }; },
					results: function(resp, page) { /* */ return resp; }
				},
				escapeMarkup: function(str) { /* */ return str; }
			}).on('change', function(e) {
				AppGini.current_tipo_dispo__RAND__.value = e.added.id;
				AppGini.current_tipo_dispo__RAND__.text = e.added.text;
				$j('[name="tipo_dispo"]').val(e.added.id);
				if(e.added.id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=tipo_dispositivo_view_parent]').hide(); } else { $j('.btn[id=tipo_dispositivo_view_parent]').show(); }


				if(typeof(tipo_dispo_update_autofills__RAND__) == 'function') tipo_dispo_update_autofills__RAND__();
			});

			if(!$j("#tipo_dispo-container__RAND__").length) {
				$j.ajax({
					url: 'ajax_combo.php',
					dataType: 'json',
					data: { id: AppGini.current_tipo_dispo__RAND__.value, t: 'imagenes', f: 'tipo_dispo' },
					success: function(resp) {
						$j('[name="tipo_dispo"]').val(resp.results[0].id);
						$j('[id=tipo_dispo-container-readonly__RAND__]').html('<span id="tipo_dispo-match-text">' + resp.results[0].text + '</span>');
						if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=tipo_dispositivo_view_parent]').hide(); } else { $j('.btn[id=tipo_dispositivo_view_parent]').show(); }

						if(typeof(tipo_dispo_update_autofills__RAND__) == 'function') tipo_dispo_update_autofills__RAND__();
					}
				});
			}

		<?php } else { ?>

			$j.ajax({
				url: 'ajax_combo.php',
				dataType: 'json',
				data: { id: AppGini.current_tipo_dispo__RAND__.value, t: 'imagenes', f: 'tipo_dispo' },
				success: function(resp) {
					$j('[id=tipo_dispo-container__RAND__], [id=tipo_dispo-container-readonly__RAND__]').html('<span id="tipo_dispo-match-text">' + resp.results[0].text + '</span>');
					if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=tipo_dispositivo_view_parent]').hide(); } else { $j('.btn[id=tipo_dispositivo_view_parent]').show(); }

					if(typeof(tipo_dispo_update_autofills__RAND__) == 'function') tipo_dispo_update_autofills__RAND__();
				}
			});
		<?php } ?>

		}
		function marca_reload__RAND__() {
		<?php if(($AllowUpdate || $AllowInsert) && !$dvprint) { ?>

			$j("#marca-container__RAND__").select2({
				/* initial default value */
				initSelection: function(e, c) {
					$j.ajax({
						url: 'ajax_combo.php',
						dataType: 'json',
						data: { id: AppGini.current_marca__RAND__.value, t: 'imagenes', f: 'marca' },
						success: function(resp) {
							c({
								id: resp.results[0].id,
								text: resp.results[0].text
							});
							$j('[name="marca"]').val(resp.results[0].id);
							$j('[id=marca-container-readonly__RAND__]').html('<span id="marca-match-text">' + resp.results[0].text + '</span>');
							if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marcas_view_parent]').hide(); } else { $j('.btn[id=marcas_view_parent]').show(); }

						if(typeof(modelo_reload__RAND__) == 'function') modelo_reload__RAND__(AppGini.current_marca__RAND__.value);

							if(typeof(marca_update_autofills__RAND__) == 'function') marca_update_autofills__RAND__();
						}
					});
				},
				width: '100%',
				formatNoMatches: function(term) { /* */ return '<?php echo addslashes($Translation['No matches found!']); ?>'; },
				minimumResultsForSearch: 5,
				loadMorePadding: 200,
				ajax: {
					url: 'ajax_combo.php',
					dataType: 'json',
					cache: true,
					data: function(term, page) { /* */ return { s: term, p: page, t: 'imagenes', f: 'marca' }; },
					results: function(resp, page) { /* */ return resp; }
				},
				escapeMarkup: function(str) { /* */ return str; }
			}).on('change', function(e) {
				AppGini.current_marca__RAND__.value = e.added.id;
				AppGini.current_marca__RAND__.text = e.added.text;
				$j('[name="marca"]').val(e.added.id);
				if(e.added.id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marcas_view_parent]').hide(); } else { $j('.btn[id=marcas_view_parent]').show(); }

						if(typeof(modelo_reload__RAND__) == 'function') modelo_reload__RAND__(AppGini.current_marca__RAND__.value);

				if(typeof(marca_update_autofills__RAND__) == 'function') marca_update_autofills__RAND__();
			});

			if(!$j("#marca-container__RAND__").length) {
				$j.ajax({
					url: 'ajax_combo.php',
					dataType: 'json',
					data: { id: AppGini.current_marca__RAND__.value, t: 'imagenes', f: 'marca' },
					success: function(resp) {
						$j('[name="marca"]').val(resp.results[0].id);
						$j('[id=marca-container-readonly__RAND__]').html('<span id="marca-match-text">' + resp.results[0].text + '</span>');
						if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marcas_view_parent]').hide(); } else { $j('.btn[id=marcas_view_parent]').show(); }

						if(typeof(marca_update_autofills__RAND__) == 'function') marca_update_autofills__RAND__();
					}
				});
			}

		<?php } else { ?>

			$j.ajax({
				url: 'ajax_combo.php',
				dataType: 'json',
				data: { id: AppGini.current_marca__RAND__.value, t: 'imagenes', f: 'marca' },
				success: function(resp) {
					$j('[id=marca-container__RAND__], [id=marca-container-readonly__RAND__]').html('<span id="marca-match-text">' + resp.results[0].text + '</span>');
					if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marcas_view_parent]').hide(); } else { $j('.btn[id=marcas_view_parent]').show(); }

					if(typeof(marca_update_autofills__RAND__) == 'function') marca_update_autofills__RAND__();
				}
			});
		<?php } ?>

		}
		function modelo_reload__RAND__(filterer_marca) {
		<?php if(($AllowUpdate || $AllowInsert) && !$dvprint) { ?>

			$j("#modelo-container__RAND__").select2({
				/* initial default value */
				initSelection: function(e, c) {
					$j.ajax({
						url: 'ajax_combo.php',
						dataType: 'json',
						data: { filterer_marca: filterer_marca, id: AppGini.current_modelo__RAND__.value, t: 'imagenes', f: 'modelo' },
						success: function(resp) {
							c({
								id: resp.results[0].id,
								text: resp.results[0].text
							});
							$j('[name="modelo"]').val(resp.results[0].id);
							$j('[id=modelo-container-readonly__RAND__]').html('<span id="modelo-match-text">' + resp.results[0].text + '</span>');
							if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marca_modelo_view_parent]').hide(); } else { $j('.btn[id=marca_modelo_view_parent]').show(); }


							if(typeof(modelo_update_autofills__RAND__) == 'function') modelo_update_autofills__RAND__();
						}
					});
				},
				width: '100%',
				formatNoMatches: function(term) { /* */ return '<?php echo addslashes($Translation['No matches found!']); ?>'; },
				minimumResultsForSearch: 5,
				loadMorePadding: 200,
				ajax: {
					url: 'ajax_combo.php',
					dataType: 'json',
					cache: true,
					data: function(term, page) { /* */ return { filterer_marca: filterer_marca, s: term, p: page, t: 'imagenes', f: 'modelo' }; },
					results: function(resp, page) { /* */ return resp; }
				},
				escapeMarkup: function(str) { /* */ return str; }
			}).on('change', function(e) {
				AppGini.current_modelo__RAND__.value = e.added.id;
				AppGini.current_modelo__RAND__.text = e.added.text;
				$j('[name="modelo"]').val(e.added.id);
				if(e.added.id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marca_modelo_view_parent]').hide(); } else { $j('.btn[id=marca_modelo_view_parent]').show(); }


				if(typeof(modelo_update_autofills__RAND__) == 'function') modelo_update_autofills__RAND__();
			});

			if(!$j("#modelo-container__RAND__").length) {
				$j.ajax({
					url: 'ajax_combo.php',
					dataType: 'json',
					data: { id: AppGini.current_modelo__RAND__.value, t: 'imagenes', f: 'modelo' },
					success: function(resp) {
						$j('[name="modelo"]').val(resp.results[0].id);
						$j('[id=modelo-container-readonly__RAND__]').html('<span id="modelo-match-text">' + resp.results[0].text + '</span>');
						if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marca_modelo_view_parent]').hide(); } else { $j('.btn[id=marca_modelo_view_parent]').show(); }

						if(typeof(modelo_update_autofills__RAND__) == 'function') modelo_update_autofills__RAND__();
					}
				});
			}

		<?php } else { ?>

			$j.ajax({
				url: 'ajax_combo.php',
				dataType: 'json',
				data: { id: AppGini.current_modelo__RAND__.value, t: 'imagenes', f: 'modelo' },
				success: function(resp) {
					$j('[id=modelo-container__RAND__], [id=modelo-container-readonly__RAND__]').html('<span id="modelo-match-text">' + resp.results[0].text + '</span>');
					if(resp.results[0].id == '<?php echo empty_lookup_value; ?>') { $j('.btn[id=marca_modelo_view_parent]').hide(); } else { $j('.btn[id=marca_modelo_view_parent]').show(); }

					if(typeof(modelo_update_autofills__RAND__) == 'function') modelo_update_autofills__RAND__();
				}
			});
		<?php } ?>

		}
	</script>
	<?php

	$lookups = str_replace('__RAND__', $rnd1, ob_get_contents());
	ob_end_clean();


	// code for template based detail view forms

	// open the detail view template
	if($dvprint) {
		$template_file = is_file("./{$TemplateDVP}") ? "./{$TemplateDVP}" : './templates/imagenes_templateDVP.html';
		$templateCode = @file_get_contents($template_file);
	} else {
		$template_file = is_file("./{$TemplateDV}") ? "./{$TemplateDV}" : './templates/imagenes_templateDV.html';
		$templateCode = @file_get_contents($template_file);
	}

	// process form title
	$templateCode = str_replace('<%%DETAIL_VIEW_TITLE%%>', 'Imagene details', $templateCode);
	$templateCode = str_replace('<%%RND1%%>', $rnd1, $templateCode);
	$templateCode = str_replace('<%%EMBEDDED%%>', ($_REQUEST['Embedded'] ? 'Embedded=1' : ''), $templateCode);
	// process buttons
	if($AllowInsert) {
		if(!$selected_id) $templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-success" id="insert" name="insert_x" value="1" onclick="return imagenes_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save New'] . '</button>', $templateCode);
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="insert" name="insert_x" value="1" onclick="return imagenes_validateData();"><i class="glyphicon glyphicon-plus-sign"></i> ' . $Translation['Save As Copy'] . '</button>', $templateCode);
	} else {
		$templateCode = str_replace('<%%INSERT_BUTTON%%>', '', $templateCode);
	}

	// 'Back' button action
	if($_REQUEST['Embedded']) {
		$backAction = 'AppGini.closeParentModal(); return false;';
	} else {
		$backAction = '$j(\'form\').eq(0).attr(\'novalidate\', \'novalidate\'); document.myform.reset(); return true;';
	}

	if($selected_id) {
		if(!$_REQUEST['Embedded']) $templateCode = str_replace('<%%DVPRINT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="dvprint" name="dvprint_x" value="1" onclick="$j(\'form\').eq(0).prop(\'novalidate\', true); document.myform.reset(); return true;" title="' . html_attr($Translation['Print Preview']) . '"><i class="glyphicon glyphicon-print"></i> ' . $Translation['Print Preview'] . '</button>', $templateCode);
		if($AllowUpdate) {
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '<button type="submit" class="btn btn-success btn-lg" id="update" name="update_x" value="1" onclick="return imagenes_validateData();" title="' . html_attr($Translation['Save Changes']) . '"><i class="glyphicon glyphicon-ok"></i> ' . $Translation['Save Changes'] . '</button>', $templateCode);
		} else {
			$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		}
		if(($arrPerm[4]==1 && $ownerMemberID==getLoggedMemberID()) || ($arrPerm[4]==2 && $ownerGroupID==getLoggedGroupID()) || $arrPerm[4]==3) { // allow delete?
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '<button type="submit" class="btn btn-danger" id="delete" name="delete_x" value="1" onclick="return confirm(\'' . $Translation['are you sure?'] . '\');" title="' . html_attr($Translation['Delete']) . '"><i class="glyphicon glyphicon-trash"></i> ' . $Translation['Delete'] . '</button>', $templateCode);
		} else {
			$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		}
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>', $templateCode);
	} else {
		$templateCode = str_replace('<%%UPDATE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DELETE_BUTTON%%>', '', $templateCode);
		$templateCode = str_replace('<%%DESELECT_BUTTON%%>', ($ShowCancel ? '<button type="submit" class="btn btn-default" id="deselect" name="deselect_x" value="1" onclick="' . $backAction . '" title="' . html_attr($Translation['Back']) . '"><i class="glyphicon glyphicon-chevron-left"></i> ' . $Translation['Back'] . '</button>' : ''), $templateCode);
	}

	// set records to read only if user can't insert new records and can't edit current record
	if(($selected_id && !$AllowUpdate && !$AllowInsert) || (!$selected_id && !$AllowInsert)) {
		$jsReadOnly .= "\tjQuery('#imagen').replaceWith('<div class=\"form-control-static\" id=\"imagen\">' + (jQuery('#imagen').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('#tipo_dispo').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#tipo_dispo_caption').prop('disabled', true).css({ color: '#555', backgroundColor: 'white' });\n";
		$jsReadOnly .= "\tjQuery('#marca').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#marca_caption').prop('disabled', true).css({ color: '#555', backgroundColor: 'white' });\n";
		$jsReadOnly .= "\tjQuery('#modelo').prop('disabled', true).css({ color: '#555', backgroundColor: '#fff' });\n";
		$jsReadOnly .= "\tjQuery('#modelo_caption').prop('disabled', true).css({ color: '#555', backgroundColor: 'white' });\n";
		$jsReadOnly .= "\tjQuery('#descripcion').replaceWith('<div class=\"form-control-static\" id=\"descripcion\">' + (jQuery('#descripcion').val() || '') + '</div>');\n";
		$jsReadOnly .= "\tjQuery('.select2-container').hide();\n";

		$noUploads = true;
	} elseif($AllowInsert) {
		$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', true);"; // temporarily disable form change handler
			$jsEditable .= "\tjQuery('form').eq(0).data('already_changed', false);"; // re-enable form change handler
	}

	// process combos
	$templateCode = str_replace('<%%COMBO(tipo_dispo)%%>', $combo_tipo_dispo->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(tipo_dispo)%%>', $combo_tipo_dispo->MatchText, $templateCode);
	$templateCode = str_replace('<%%URLCOMBOTEXT(tipo_dispo)%%>', urlencode($combo_tipo_dispo->MatchText), $templateCode);
	$templateCode = str_replace('<%%COMBO(marca)%%>', $combo_marca->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(marca)%%>', $combo_marca->MatchText, $templateCode);
	$templateCode = str_replace('<%%URLCOMBOTEXT(marca)%%>', urlencode($combo_marca->MatchText), $templateCode);
	$templateCode = str_replace('<%%COMBO(modelo)%%>', $combo_modelo->HTML, $templateCode);
	$templateCode = str_replace('<%%COMBOTEXT(modelo)%%>', $combo_modelo->MatchText, $templateCode);
	$templateCode = str_replace('<%%URLCOMBOTEXT(modelo)%%>', urlencode($combo_modelo->MatchText), $templateCode);

	/* lookup fields array: 'lookup field name' => array('parent table name', 'lookup field caption') */
	$lookup_fields = array('tipo_dispo' => array('tipo_dispositivo', 'Tipo dispo'), 'marca' => array('marcas', 'Marca'), 'modelo' => array('marca_modelo', 'Modelo'), );
	foreach($lookup_fields as $luf => $ptfc) {
		$pt_perm = getTablePermissions($ptfc[0]);

		// process foreign key links
		if($pt_perm['view'] || $pt_perm['edit']) {
			$templateCode = str_replace("<%%PLINK({$luf})%%>", '<button type="button" class="btn btn-default view_parent hspacer-md" id="' . $ptfc[0] . '_view_parent" title="' . html_attr($Translation['View'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-eye-open"></i></button>', $templateCode);
		}

		// if user has insert permission to parent table of a lookup field, put an add new button
		if($pt_perm['insert'] && !$_REQUEST['Embedded']) {
			$templateCode = str_replace("<%%ADDNEW({$ptfc[0]})%%>", '<button type="button" class="btn btn-success add_new_parent hspacer-md" id="' . $ptfc[0] . '_add_new" title="' . html_attr($Translation['Add New'] . ' ' . $ptfc[1]) . '"><i class="glyphicon glyphicon-plus-sign"></i></button>', $templateCode);
		}
	}

	// process images
	$templateCode = str_replace('<%%UPLOADFILE(id)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(imagen)%%>', ($noUploads ? '' : "<div>{$Translation['upload image']}</div>" . '<i class="glyphicon glyphicon-remove text-danger clear-upload hidden"></i> <input type="file" name="imagen" id="imagen" data-filetypes="jpg|jpeg|gif|png" data-maxsize="2048000" accept=".jpg,.jpeg,.gif,.png">'), $templateCode);
	if($AllowUpdate && $row['imagen'] != '') {
		$templateCode = str_replace('<%%REMOVEFILE(imagen)%%>', '<br><input type="checkbox" name="imagen_remove" id="imagen_remove" value="1"> <label for="imagen_remove" style="color: red; font-weight: bold;">'.$Translation['remove image'].'</label>', $templateCode);
	} else {
		$templateCode = str_replace('<%%REMOVEFILE(imagen)%%>', '', $templateCode);
	}
	$templateCode = str_replace('<%%UPLOADFILE(tipo_dispo)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(marca)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(modelo)%%>', '', $templateCode);
	$templateCode = str_replace('<%%UPLOADFILE(descripcion)%%>', '', $templateCode);

	// process values
	if($selected_id) {
		if( $dvprint) $templateCode = str_replace('<%%VALUE(id)%%>', safe_html($urow['id']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(id)%%>', html_attr($row['id']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(id)%%>', urlencode($urow['id']), $templateCode);
		$row['imagen'] = ($row['imagen'] != '' ? $row['imagen'] : 'blank.gif');
		if( $dvprint) $templateCode = str_replace('<%%VALUE(imagen)%%>', safe_html($urow['imagen']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(imagen)%%>', html_attr($row['imagen']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(imagen)%%>', urlencode($urow['imagen']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(tipo_dispo)%%>', safe_html($urow['tipo_dispo']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(tipo_dispo)%%>', html_attr($row['tipo_dispo']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(tipo_dispo)%%>', urlencode($urow['tipo_dispo']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(marca)%%>', safe_html($urow['marca']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(marca)%%>', html_attr($row['marca']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(marca)%%>', urlencode($urow['marca']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(modelo)%%>', safe_html($urow['modelo']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(modelo)%%>', html_attr($row['modelo']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(modelo)%%>', urlencode($urow['modelo']), $templateCode);
		if( $dvprint) $templateCode = str_replace('<%%VALUE(descripcion)%%>', safe_html($urow['descripcion']), $templateCode);
		if(!$dvprint) $templateCode = str_replace('<%%VALUE(descripcion)%%>', html_attr($row['descripcion']), $templateCode);
		$templateCode = str_replace('<%%URLVALUE(descripcion)%%>', urlencode($urow['descripcion']), $templateCode);
	} else {
		$templateCode = str_replace('<%%VALUE(id)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(id)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(imagen)%%>', 'blank.gif', $templateCode);
		$templateCode = str_replace('<%%VALUE(tipo_dispo)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(tipo_dispo)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(marca)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(marca)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(modelo)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(modelo)%%>', urlencode(''), $templateCode);
		$templateCode = str_replace('<%%VALUE(descripcion)%%>', '', $templateCode);
		$templateCode = str_replace('<%%URLVALUE(descripcion)%%>', urlencode(''), $templateCode);
	}

	// process translations
	foreach($Translation as $symbol=>$trans) {
		$templateCode = str_replace("<%%TRANSLATION($symbol)%%>", $trans, $templateCode);
	}

	// clear scrap
	$templateCode = str_replace('<%%', '<!-- ', $templateCode);
	$templateCode = str_replace('%%>', ' -->', $templateCode);

	// hide links to inaccessible tables
	if($_REQUEST['dvprint_x'] == '') {
		$templateCode .= "\n\n<script>\$j(function() {\n";
		$arrTables = getTableList();
		foreach($arrTables as $name => $caption) {
			$templateCode .= "\t\$j('#{$name}_link').removeClass('hidden');\n";
			$templateCode .= "\t\$j('#xs_{$name}_link').removeClass('hidden');\n";
		}

		$templateCode .= $jsReadOnly;
		$templateCode .= $jsEditable;

		if(!$selected_id) {
		}

		$templateCode.="\n});</script>\n";
	}

	// ajaxed auto-fill fields
	$templateCode .= '<script>';
	$templateCode .= '$j(function() {';


	$templateCode.="});";
	$templateCode.="</script>";
	$templateCode .= $lookups;

	// handle enforced parent values for read-only lookup fields

	// don't include blank images in lightbox gallery
	$templateCode = preg_replace('/blank.gif" data-lightbox=".*?"/', 'blank.gif"', $templateCode);

	// don't display empty email links
	$templateCode=preg_replace('/<a .*?href="mailto:".*?<\/a>/', '', $templateCode);

	/* default field values */
	$rdata = $jdata = get_defaults('imagenes');
	if($selected_id) {
		$jdata = get_joined_record('imagenes', $selected_id);
		if($jdata === false) $jdata = get_defaults('imagenes');
		$rdata = $row;
	}
	$templateCode .= loadView('imagenes-ajax-cache', array('rdata' => $rdata, 'jdata' => $jdata));

	// hook: imagenes_dv
	if(function_exists('imagenes_dv')) {
		$args=[];
		imagenes_dv(($selected_id ? $selected_id : FALSE), getMemberInfo(), $templateCode, $args);
	}

	return $templateCode;
}