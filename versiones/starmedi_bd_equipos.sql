-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-04-2021 a las 15:15:56
-- Versión del servidor: 5.6.32-78.1
-- Versión de PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `starmedi_bd_equipos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorios`
--

CREATE TABLE `accesorios` (
  `id` int(10) UNSIGNED NOT NULL,
  `iddispo` int(10) UNSIGNED DEFAULT NULL,
  `Accesorio` int(10) UNSIGNED NOT NULL,
  `fecha_asignacion` date NOT NULL,
  `comentarios` text,
  `id_dispo` int(10) UNSIGNED DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `tipo_accesorio` int(10) UNSIGNED DEFAULT NULL,
  `nombre_accesorio` varchar(60) NOT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `codigo_accesorio` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorio_dispo_relacion`
--

CREATE TABLE `accesorio_dispo_relacion` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_accesorio` int(10) UNSIGNED DEFAULT NULL,
  `id_dispo` int(10) UNSIGNED DEFAULT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `marca_modelo` int(10) UNSIGNED DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acceso_remoto`
--

CREATE TABLE `acceso_remoto` (
  `id` int(10) UNSIGNED NOT NULL,
  `unidad` int(10) UNSIGNED NOT NULL,
  `ciudad` int(10) UNSIGNED DEFAULT NULL,
  `servicio` text,
  `anydesk` varchar(40) NOT NULL,
  `acceso` varchar(40) NOT NULL,
  `ultima_verifica` date NOT NULL,
  `comentario` text,
  `creado` datetime DEFAULT NULL,
  `teamviewer` varchar(40) DEFAULT NULL,
  `acceso2` varchar(40) DEFAULT NULL,
  `codigo_equipo` int(10) UNSIGNED DEFAULT NULL,
  `equipo` int(10) UNSIGNED DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `acceso_remoto`
--

INSERT INTO `acceso_remoto` (`id`, `unidad`, `ciudad`, `servicio`, `anydesk`, `acceso`, `ultima_verifica`, `comentario`, `creado`, `teamviewer`, `acceso2`, `codigo_equipo`, `equipo`, `creado_por`, `editado`, `editado_por`) VALUES
(1, 4, 4, 'RECEPCION', '356313947', 'SM123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, 247, 247, NULL, '2020-12-11 16:53:48', 'andresl'),
(2, 4, 4, 'HOLTER, MAPA', '621216946', 'sm123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 4, 4, 'TRANSCRIPCION', '302698685', 'sm123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, NULL, NULL, 'andresl', NULL, NULL),
(4, 2, 2, 'HOLTER, MAPA, CONSULTA, TRANSCRIPCION, GOOGLE DRIVE', '104301309', 'sm123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, 149, 149, 'andresl', '2020-12-14 14:09:49', 'andresl'),
(5, 2, 2, 'TRANSCRIPCION', '401481429', 'sm123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, 581, 581, 'andresl', '2020-12-14 14:11:18', 'andresl'),
(6, 1, 1, 'HOLTER, MAPA, TRANSCRIPCION, RECEPCION', '681721923', 'sm123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, NULL, NULL, 'andresl', NULL, NULL),
(7, 1, 1, 'HOLTER, MAPA, TRANSCRIPCION', '203786438', 'sm123456', '2020-12-10', NULL, '2020-12-10 00:00:00', NULL, NULL, NULL, NULL, 'andresl', NULL, NULL),
(8, 4, 4, 'TRANSCRIPCION', '815962374', 'sm123456', '2020-12-11', NULL, '2020-12-11 00:00:00', NULL, NULL, 586, 586, 'andresl', '2020-12-11 16:49:54', 'andresl'),
(9, 14, 14, 'GOOGLE DRIVE', '771040954', 'sm123456', '2021-01-04', 'PC 677', '2021-01-04 00:00:00', NULL, NULL, NULL, NULL, 'felipem', '2021-01-04 15:09:02', 'felipem'),
(10, 1, 1, 'HOLTER, MAPA, GOOGLE DRIVE', '799371434', 'sm123456', '2021-01-04', NULL, '2021-01-04 00:00:00', NULL, NULL, NULL, NULL, 'felipem', '2021-01-04 16:55:51', 'felipem'),
(11, 6, 6, NULL, '837195367', 'sm123456', '2021-01-13', NULL, '2021-01-13 00:00:00', '1 559 932 424', '12345678', NULL, NULL, 'felipem', '2021-01-13 16:59:14', 'felipem');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appgini_csv_import_jobs`
--

CREATE TABLE `appgini_csv_import_jobs` (
  `id` varchar(40) NOT NULL,
  `memberID` varchar(100) NOT NULL,
  `config` text,
  `insert_ts` int(11) DEFAULT NULL,
  `last_update_ts` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT '99999999',
  `done` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `appgini_csv_import_jobs`
--

INSERT INTO `appgini_csv_import_jobs` (`id`, `memberID`, `config`, `insert_ts`, `last_update_ts`, `total`, `done`) VALUES
('5fb73fca3d9c78.73099585', 'felipem', '{\"table\":\"dispositivos\",\"fieldSeparator\":\",\",\"fieldWrapper\":\"\\\"\",\"updateExistingRecords\":false,\"rowFilterCallback\":null,\"rowTransformCallback\":null,\"csvFile\":\"\\/home4\\/starmedi\\/inventario.starmedica.co\\/import-csv\\/be0e07ea6555bf430.csv\",\"csvHasHeaderRow\":false,\"csvMap\":[],\"moreOptions\":[],\"fastMode\":false,\"recordsPerInsert\":100,\"cleanCsvFileReady\":false,\"totalRecords\":0,\"importedCount\":0,\"importedCountSkip\":0,\"importedCountInsert\":0,\"importedCountUpdate\":0,\"lastInsertId\":null,\"importFinished\":false,\"pkFieldName\":\"id_dispo\",\"pkAutoInc\":true}', 1605844938, 1605844940, 0, 0),
('5fb74255af7090.57386417', 'felipem', '{\"table\":\"dispositivos\",\"fieldSeparator\":\",\",\"fieldWrapper\":\"\\\"\",\"updateExistingRecords\":false,\"rowFilterCallback\":null,\"rowTransformCallback\":null,\"csvFile\":\"\\/home4\\/starmedi\\/inventario.starmedica.co\\/import-csv\\/7ef7f47cb96cfc661.csv\",\"csvHasHeaderRow\":false,\"csvMap\":[],\"moreOptions\":[],\"fastMode\":false,\"recordsPerInsert\":100,\"cleanCsvFileReady\":false,\"totalRecords\":0,\"importedCount\":0,\"importedCountSkip\":0,\"importedCountInsert\":0,\"importedCountUpdate\":0,\"lastInsertId\":null,\"importFinished\":false,\"pkFieldName\":\"id_dispo\",\"pkAutoInc\":true}', 1605845589, 1605845591, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calibraciones`
--

CREATE TABLE `calibraciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` int(10) UNSIGNED NOT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `tipo_calibracion` int(10) UNSIGNED NOT NULL,
  `fecha_calibra` date NOT NULL,
  `fecha_expiracion` date NOT NULL,
  `empresa` int(10) UNSIGNED NOT NULL,
  `documento` varchar(40) NOT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `telefono` int(10) UNSIGNED DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `calibraciones`
--

INSERT INTO `calibraciones` (`id`, `codigo`, `serial`, `tipo_calibracion`, `fecha_calibra`, `fecha_expiracion`, `empresa`, `documento`, `timestamp`, `update`, `creado_por`, `editado_por`, `descripcion`, `telefono`, `creado`, `editado`) VALUES
(1, 26, 26, 2, '2020-09-10', '2020-09-30', 6, 'mtto12345.docx', '10/9/2020 04:51:05 pm', NULL, 'lgarcia', NULL, NULL, NULL, NULL, NULL),
(2, 232, 232, 1, '2017-05-19', '2017-05-19', 7, 'CALIBRACION_2017.pdf', '6/11/2020 11:49:24 am', NULL, 'lgarcia', NULL, NULL, NULL, NULL, NULL),
(3, 232, 232, 1, '2019-07-20', '2020-07-20', 7, 'CALIBRACION_2019.pdf', '6/11/2020 11:51:56 am', NULL, 'lgarcia', NULL, NULL, NULL, NULL, NULL),
(4, 232, 232, 1, '2020-10-10', '2021-10-10', 7, 'CALIBRACION_2020.pdf', '6/11/2020 11:53:02 am', '6/11/2020 11:53:16 am', 'lgarcia', 'lgarcia', NULL, NULL, NULL, NULL),
(5, 258, 258, 1, '2019-08-29', '2020-08-29', 7, 'CALIBRACION-_08-2019.pdf', '9/11/2020 09:34:57 pm', NULL, 'lgarcia', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `id_ciudad` int(10) UNSIGNED NOT NULL,
  `ciudad` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ciudades`
--

INSERT INTO `ciudades` (`id_ciudad`, `ciudad`, `descripcion`) VALUES
(1, 'CALI', NULL),
(2, 'BUGA', NULL),
(3, 'TULUA', NULL),
(4, 'PALMIRA', NULL),
(5, 'BOGOTA', NULL),
(6, 'CALIFORNIA', NULL),
(7, 'CHINA', NULL),
(8, 'GUACARI', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigoserial`
--

CREATE TABLE `codigoserial` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` int(10) UNSIGNED DEFAULT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compras` int(10) UNSIGNED NOT NULL,
  `codigo` int(10) UNSIGNED NOT NULL,
  `serial` varchar(40) DEFAULT NULL,
  `proveedor` int(10) UNSIGNED NOT NULL,
  `identi` int(10) UNSIGNED DEFAULT NULL,
  `tel_provededor` int(10) UNSIGNED DEFAULT NULL,
  `ciudad_origen` int(10) UNSIGNED DEFAULT NULL,
  `fecha_factura` date DEFAULT NULL,
  `factura` varchar(40) NOT NULL,
  `precio` int(11) DEFAULT NULL,
  `garantia` date DEFAULT NULL,
  `fecha_compra` date DEFAULT NULL,
  `comentarios` text,
  `reg_creado` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `soporte` varchar(40) DEFAULT NULL,
  `field19` varchar(40) DEFAULT NULL,
  `comprado_por` int(10) UNSIGNED NOT NULL,
  `tipo_dispo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED DEFAULT NULL,
  `modelo` int(10) UNSIGNED DEFAULT NULL,
  `referencia` text,
  `ficha_tecnica` varchar(40) DEFAULT NULL,
  `otro_documento` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` varchar(40) DEFAULT NULL,
  `creado1` datetime NOT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field27` varchar(40) DEFAULT NULL,
  `field28` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consentimieto`
--

CREATE TABLE `consentimieto` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perfil` int(10) UNSIGNED DEFAULT NULL,
  `identificacion` int(10) UNSIGNED DEFAULT NULL,
  `acepto` varchar(40) NOT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id_contactos` int(10) UNSIGNED NOT NULL,
  `tipo_contacto` int(10) UNSIGNED NOT NULL,
  `tipo_iden` int(10) UNSIGNED NOT NULL,
  `identificacion` varchar(40) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `ciudad` int(10) UNSIGNED NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `direccion` text,
  `tipo_relacion` int(10) UNSIGNED NOT NULL,
  `nota` text NOT NULL,
  `reg_creado` datetime DEFAULT NULL,
  `update` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `creado1` datetime NOT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field15` varchar(40) DEFAULT NULL,
  `field16` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id_contactos`, `tipo_contacto`, `tipo_iden`, `identificacion`, `nombre`, `ciudad`, `telefono`, `direccion`, `tipo_relacion`, `nota`, `reg_creado`, `update`, `creado_por`, `editado_por`, `creado1`, `creado`, `editado`, `field15`, `field16`) VALUES
(1, 2, 2, '900355222-7', 'TIENDAS TECNOPLAZA SAS', 1, '4851150', 'LOCAL 203 CENTRO COMERCIAL LA PASARELA AVENIDA 5A N NRO 23DN - 68', 1, '', '0000-00-00 00:00:00', NULL, 'vilmao', NULL, '2019-11-27 00:00:00', NULL, NULL, NULL, NULL),
(2, 1, 1, '465465', 'WILLIAM IBARRA', 1, '23423', NULL, 2, '', '0000-00-00 00:00:00', NULL, 'vilmao', NULL, '2019-11-27 00:00:00', NULL, NULL, NULL, NULL),
(3, 1, 1, '1061718393', 'FELIPE MOLANO', 1, '3135507417', 'Cra 53#98-91,apto 704-3', 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'felipem', 'felipem', '2019-11-29 00:00:00', NULL, NULL, NULL, NULL),
(4, 2, 3, '510663', 'DMSoftware', 7, '+86', 'Rm 202,2nd Floor,No.604-1,Guangshan 2nd Rd,Tianhe District, Guangzhou,Guangdong,China 510663', 1, 'Proveedor DMS\r\nContacto Don Goh', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'felipem', 'felipem', '2019-12-10 00:00:00', NULL, NULL, NULL, NULL),
(5, 1, 1, '1002971312', 'JUAN CAMILO ZAMBRANO', 1, '3185541002', 'LA REFORMA', 3, 'APOYO EN TECNOLOGIA', '0000-00-00 00:00:00', NULL, 'camiloz', NULL, '2020-02-29 00:00:00', NULL, NULL, NULL, NULL),
(6, 1, 1, '3452345', 'DIEGO TRIANA', 2, '63456345', NULL, 3, 'Biomedico', '2020-09-10 16:45:23', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-09-10 16:45:23', NULL, NULL, NULL),
(7, 2, 2, '901.246.811.1', 'CEMECEO', 1, '3217822331', 'CALLE 13 # 1-22', 1, '', '2020-11-04 14:23:10', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-04 14:23:10', NULL, NULL, NULL),
(8, 1, 1, '1148207969', 'ANDRES LOPEZ', 1, '3187617857', 'Cra 53 oeste # 13-13', 3, '', '2020-11-17 12:51:31', NULL, 'andresl', NULL, '0000-00-00 00:00:00', '2020-11-17 12:51:31', NULL, NULL, NULL),
(9, 1, 1, '14886979', 'LUIS ALBERTO SOLARTE', 5, '3144590404', NULL, 3, 'Ingeniero de mantenimiento correctivo y preventivo de ecógrafos', '2020-11-20 14:26:09', NULL, 'felipem', 'felipem', '0000-00-00 00:00:00', '2020-11-20 14:26:09', '2020-11-20 14:27:41', NULL, NULL),
(10, 1, 1, '6322231', 'LUIS GARCIA', 8, '3157522872', NULL, 2, 'JEFE DE ENFERMEROS<div>STARMEDICA<br><div><br></div></div>', '2020-12-02 17:15:44', NULL, 'felipem', NULL, '0000-00-00 00:00:00', '2020-12-02 17:15:44', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `costos_mtto`
--

CREATE TABLE `costos_mtto` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_mtto` int(10) UNSIGNED DEFAULT NULL,
  `precio` decimal(10,2) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `descripcion` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `costos_mtto`
--

INSERT INTO `costos_mtto` (`id`, `id_mtto`, `precio`, `cantidad`, `descripcion`, `creado`, `creado_por`) VALUES
(1, 41, 150000.00, 1, 'manguera con conector, cambio de manguera, transportes a san fernando', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos`
--

CREATE TABLE `dispositivos` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(4) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED DEFAULT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date NOT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado2` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado1` datetime DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos`
--

INSERT INTO `dispositivos` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', '101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span>', 1, 'HOLTER 101', '2019-11-27 00:00:00', NULL, NULL),
(2, '8710a41018a7c4e42.jpg', '102', '3A-11605', 1, 1, 1, 7, 7, '2020-12-03', 2, '27/11/2019 09:40:39 am', '2021-04-19 15:01:14', 'felipem', 'a.lopez', '<br>', 1, 'HOLTER 102', '2019-11-27 00:00:00', NULL, NULL),
(3, '6630f469154d3320d.jpg', '103', '3A-8045', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '2021-03-08 09:36:33', NULL, 'a.lopez', 'Tenia el serial 3A-8044, pero se cambia a 3A-8045', 1, 'HOLTER 103', '2020-03-05 00:00:00', NULL, NULL),
(4, '796ef58bbcaf815d5.jpg', '104', '3A-11609', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '2021-04-08 15:12:59', NULL, 'a.lopez', '<br>', 1, 'HOLTER 104', '2020-03-05 00:00:00', NULL, NULL),
(5, 'a1ca49f2da0d3858a.jpg', '105', '3A-11931', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes tenia el 3A-1039', 1, 'HOLTER 105', '2020-03-05 00:00:00', NULL, NULL),
(6, 'fa31345b2dec3f75d.jpg', '106', '3A-11934', 1, 1, 1, 1, 1, '2019-12-27', 1, NULL, '2021-04-14 10:03:16', NULL, 'a.lopez', 'Aparece con 3a-11934', 1, 'HOLTER 106', '2020-03-05 00:00:00', NULL, NULL),
(7, '62f8f3afd6062cf73.jpg', '107', '3A-11608', 1, 1, 1, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 107', '2020-03-05 00:00:00', NULL, NULL),
(8, '16a714e4f58af1930.jpg', '108', '3A-2151', 1, 1, 1, 6, 6, '2019-12-27', 1, NULL, '2021-01-29 12:54:03', NULL, 'a.lopez', 'Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial', 1, 'HOLTER 108', '2020-03-05 00:00:00', NULL, NULL),
(9, 'fa647e5ab77632692.jpg', '109', '3A-9421', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '2021-03-01 10:04:32', NULL, 'a.lopez', '<br>', 1, 'HOLTER 109', '2020-03-05 00:00:00', NULL, NULL),
(10, '476bc82bf3bed4616.jpg', '110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 110', '2020-03-05 00:00:00', NULL, NULL),
(11, 'a456dd57854fcd34f.jpg', '111', '3A-116090', 1, 1, 1, 7, 7, '2021-04-08', 2, NULL, '2021-04-08 15:10:40', NULL, 'a.lopez', '<br>', 1, 'HOLTER 111', '2020-03-05 00:00:00', NULL, NULL),
(12, 'e79c8397c6fa85698.jpg', '112', '3A-12178', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 130 pero es 112&nbsp;', 1, 'HOLTER 112', '2020-03-05 00:00:00', NULL, NULL),
(13, '79766d196e19eb1db.jpg', '113', '3A-9311', 1, 1, 1, 17, 2, '2019-12-27', 1, NULL, '2021-01-25 13:43:18', NULL, 'camiloz', 'Aparece con 3A-11948', 1, 'HOLTER 113', '2020-03-05 00:00:00', NULL, NULL),
(14, '1c1558589faf656c3.jpg', '114', '3A-11815', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, se encuentra en FHSJ, funcionamiento normal 04-Marzo-2020, Camilo.', 1, 'HOLTER 114', '2020-03-05 00:00:00', NULL, NULL),
(15, 'a47482f69e6bb1445.jpg', '115', '3A-05295', 1, 1, 1, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo fundido en desfibrilacion según estado de las tarjetas', 1, 'HOLTER 115', '2020-03-05 00:00:00', NULL, NULL),
(16, 'fad32fe2f96f1b6f2.jpg', '116', '3A-8940', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 116', '2020-03-05 00:00:00', NULL, NULL),
(17, 'f6216dd5ac51a2f82.jpg', '117', '3A-9484', 1, 1, 1, 7, 7, '2019-12-03', 1, NULL, '2021-04-19 11:56:29', NULL, 'a.lopez', '<br>', 1, 'HOLTER 117', '2020-03-05 00:00:00', NULL, NULL),
(18, '6b9ff54089c40cbf3.jpg', '118', '3A-9486', 1, 1, 1, 4, 4, '2020-11-28', 1, NULL, '2020-11-28 12:01:53', NULL, 'felipem', '<br>', 1, 'HOLTER 118', '2020-03-05 00:00:00', NULL, NULL),
(19, 'ad6939e30d7026eee.jpg', '119', '3A-9572', 1, 1, 1, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funciona normal en FHSJ 17/Marzo/2020 Camilo.', 1, 'HOLTER 119', '2020-03-05 00:00:00', NULL, NULL),
(20, '636978098692e0616.jpg', '120', '3A-9487', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '2021-04-11 12:48:52', NULL, 'a.lopez', '<br>', 1, 'HOLTER 120', '2020-03-05 00:00:00', NULL, NULL),
(21, '05353fef66bf34245.jpg', '121', '3A-8946', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal, se cambia la etiqueta del código y del serial 06-Marzo-2020, Camilo.', 1, 'HOLTER 121', '2020-03-05 00:00:00', NULL, NULL),
(22, '753003a921f5f02ec.jpg', '122', '3A-9485', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, se cambió la etiqueta del código y del serial, 06/Marzo/2020.', 1, 'HOLTER 122', '2020-03-05 00:00:00', NULL, NULL),
(23, '5cc88d4b86424bb7d.jpg', '123', '3A-1993', 1, 1, 1, 13, 13, '2020-03-04', 1, NULL, '2021-03-01 10:10:27', NULL, 'a.lopez', 'Antes holter 112, Felipe.<div><br><div>El Holter 123 realmente se encuentra en FHSJ, pendiente revisar el clon en HIDC 04-Marzo-2020, Camilo.</div></div>', 1, 'HOLTER 123', '2020-03-05 00:00:00', NULL, 300000.00),
(24, 'b89cb8d899578dd8b.jpg', '124', '3A-10318', 1, 1, 1, 7, 7, '2019-12-27', 2, NULL, '2021-02-01 11:28:10', '', 'a.lopez', 'Aparece con 3a-9311 en csf', 1, 'HOLTER 124', '2020-03-05 00:00:00', NULL, NULL),
(25, '2b4b8d4d10dddbe8b.jpg', '125', '3A-11816', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparece con el serial 3A -7125, el cual es el serial del HOLTER 101, se cambian etiquetas y queda con el serial&nbsp; 3A-11816. 06/Marzo/2020. Camilo', 1, 'HOLTER 125', '2020-03-05 00:00:00', NULL, NULL),
(26, '06b68e149d1227503.jpg', '126', '3A-8044', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '2021-03-01 09:54:18', NULL, 'a.lopez', 'Antes 3A-11818', 1, 'HOLTER 126', '2020-03-05 00:00:00', NULL, NULL),
(27, 'f4964486301b6afe2.jpg', '127', '3A-12175', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 1, 'HOLTER 127', '2020-03-05 00:00:00', NULL, NULL),
(28, 'd179e9e2972b91703.jpg', '128', '3A-12176', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo', 1, 'HOLTER 128', '2020-03-05 00:00:00', NULL, NULL),
(29, '9e12d7e5ca84cd0f4.jpg', '129', '3A-11940', 1, 1, 1, 6, 6, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'HOLTER 129', '2020-03-05 00:00:00', NULL, NULL),
(57, 'c11ec5b51107d1e95.jpg', '130', '3A-11295', 1, 1, 1, 1, 1, '2020-11-23', 1, '29/11/2019 04:45:35 pm', '2021-04-14 10:15:35', 'felipem', 'a.lopez', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 130', '2019-11-29 00:00:00', NULL, NULL),
(31, '3cc42f4af7a84575e.jpg', '201', 'US97804887', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparecía en Bodega, realmente se encuentra en CSF, Funciona normalmente, 06/Marzo/2020, Camilo.', 3, 'ECO 201', '2020-03-05 00:00:00', NULL, NULL),
(32, '536dec1c5345e2b59.jpg', '202', 'US97806979', 3, 3, 3, 6, 6, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 202', '2020-03-05 00:00:00', NULL, NULL),
(33, '2edf9e5e9f0adf0b0.jpg', '203', '3728A01811', 3, 3, 3, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 203', '2020-03-05 00:00:00', NULL, NULL),
(34, 'b35b0af60ed58ef47.jpg', '204', 'US50210876', 3, 3, 3, 6, 6, '2020-12-04', 1, NULL, '2020-12-04 09:18:44', NULL, 'andresl', 'Aparecía en Bodega, realmente está en HROB. 02/Marzo/2020 Camilo.<div><br></div><div>Actualizo, el día de hoy 17/Marzo/2020 se lleva equipo desde HROB (Palmira) a FHSJ (Buga), Camilo.</div>', 3, 'ECO 204', '2020-03-05 00:00:00', NULL, NULL),
(35, 'd6bb8df0f60c48ab0.jpg', '205', '3728A00382', 3, 3, 3, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 205', '2020-03-05 00:00:00', NULL, NULL),
(36, '620450dbf61c4c19f.jpg', '206', '9708A09160', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Aparecía en HTU, realmente está en Bodega. 05/Marzo/2020. Camilo', 3, 'ECO 206', '2020-03-05 00:00:00', NULL, NULL),
(37, '41736a0baf4cb45e3.jpg', '207', '71674', 3, 6, 5, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 207', '2020-03-05 00:00:00', NULL, NULL),
(38, '0eb9fdc48d520b1a8.jpg', '208', '001838VI', 3, 5, 4, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 208', '2020-03-05 00:00:00', NULL, NULL),
(39, 'f7c6ea863e2c80a47.jpg', '209', '055133VI', 3, 5, 4, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 209', '2020-03-05 00:00:00', NULL, NULL),
(40, 'c86621e1a393abd5e.jpg', '210', '3728A01073', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmente en HTU, 21/Marzo/2020, Camilo.', 3, 'ECO 210', '2020-03-05 00:00:00', NULL, NULL),
(41, '158d01f499364579d.jpg', '211', 'USO0211434', 3, 3, 3, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 211', '2020-03-05 00:00:00', NULL, NULL),
(42, '387a137ebf96710b6.jpg', '212', 'US97807671', 3, 3, 3, 2, 2, '2020-02-26', 1, NULL, '2021-04-13 11:40:06', NULL, 'a.lopez', '<br>', 3, 'ECO 212', '2020-03-05 00:00:00', NULL, NULL),
(43, '5ed931c87e7ad07f3.jpg', '213', '9708A04535', 3, 3, 3, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 213', '2020-03-05 00:00:00', NULL, NULL),
(44, 'e624e9cfa97691728.jpg', '214', '3728A00160', 3, 3, 3, 7, 7, '2019-12-04', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'No se encuentra en FHSJ', 3, 'ECO 214', '2020-03-05 00:00:00', NULL, NULL),
(45, '54d451022df8c30e6.jpg', '215', 'US97703545', 3, 3, 3, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 215', '2020-03-05 00:00:00', NULL, NULL),
(46, '0b54a448e7a5a93aa.jpg', '216', 'US97808948', 3, 3, 3, 3, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 3, 'ECO 216', '2020-03-05 00:00:00', NULL, NULL),
(47, '630053d26916342ca.jpg', '217', 'US97800318', 3, 3, 3, 7, 7, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 217', '2020-03-05 00:00:00', NULL, NULL),
(48, '9972d0e70d8ca4ca6.jpg', '218', '3728A02302', 3, 3, 3, 6, 6, '2020-02-20', 1, NULL, '2021-04-16 10:12:36', NULL, 'a.lopez', 'Actualizado, se encontraba en UCI río se deja en FHSJ 20-02-2018<div><br></div><div>Actualizo, equipo fuera de servicio, en reparación, 04/Marzo/2020, Camilo.<br><div><br></div></div>', 3, 'ECO 218', '2020-03-05 00:00:00', NULL, NULL),
(49, '2e9dc0bf01f8da61f.jpg', '219', 'US80210739', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '2021-04-14 09:58:22', NULL, 'a.lopez', '<br>', 3, 'ECO 219', '2020-03-05 00:00:00', NULL, NULL),
(50, '6e5eaa081a347bc98.jpg', '220', 'US97703205', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '2021-04-14 10:02:04', NULL, 'a.lopez', '<br>', 3, 'ECO 220', '2020-03-05 00:00:00', NULL, NULL),
(51, '10b8abd62f6418d47.jpg', '221', 'US97804106', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmete en HROB, 02/Marzo/2020, Camilo.', 3, 'ECO 221', '2020-03-05 00:00:00', NULL, NULL),
(52, 'b6efed45a482c8c76.jpg', '222', 'US97804535', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, está en Bodega inactivo 05/Marzo/2020. Camilo', 3, 'ECO 222', '2020-03-05 00:00:00', NULL, NULL),
(53, '326d48038544d0081.jpg', '223', 'US70421029', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente se encuentra en Clínica San Francisco 06/Marzo/2020, Camilo', 3, 'ECO 223', '2020-03-05 00:00:00', NULL, NULL),
(54, '2114d2a34043479d1.jpg', '224', 'US97703258', 3, 3, 3, 7, 7, '2020-11-17', 1, NULL, '2021-04-13 18:46:42', NULL, 'a.lopez', NULL, 3, 'ECO 224', '2020-03-05 00:00:00', NULL, NULL),
(55, '99c80bd69f2b7d401.jpg', '225', 'USD0211891', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Verificar código puesto en el equipo&nbsp;', NULL, 'ECO 225', '2020-03-05 00:00:00', NULL, NULL),
(56, 'a4df3e5a8cd507c06.jpg', '621', 'JCN0GR05Z985517', 11, 7, 6, 7, 7, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '0000-00-00 00:00:00', 'vilmao', 'felipem', '<br>', 6, 'PC 621', '2019-11-27 00:00:00', NULL, NULL),
(58, '2fb3b0f1c5f8919a5.jpg', '131', '3A-11287', 1, 1, 1, 7, 7, '2019-11-29', 1, '29/11/2019 05:06:08 pm', '2021-02-16 13:42:22', 'felipem', 'a.lopez', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 131', '2019-11-29 00:00:00', NULL, NULL),
(59, '982c34a6a71597fe5.jpg', '132', '3A-11293', 1, 1, 1, 1, 13, '2019-12-03', 1, '3/12/2019 12:38:32 am', '2021-03-01 10:19:43', 'felipem', 'a.lopez', 'Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 132', '2019-12-03 00:00:00', NULL, NULL),
(60, '5c9c7e2c36eb50b7f.jpg', '133', '3A-11285', 1, 1, 1, 1, 1, '2019-11-29', 1, '3/12/2019 12:41:03 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 133', '2019-12-03 00:00:00', NULL, NULL),
(61, 'f19510876e2eb19cd.jpeg', '551', 'IP309100004', 2, 2, 7, 3, 3, '0000-00-00', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', NULL, 'MAPA 551', '2020-03-05 00:00:00', NULL, NULL),
(62, '1d40088c027c6481a.jpeg', '552', '00087513', 2, 8, 8, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 552', '2020-03-05 00:00:00', NULL, NULL),
(63, NULL, '553', '140206005', 2, 2, 7, 3, NULL, '0000-00-00', 2, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 553', '2020-03-05 00:00:00', NULL, NULL),
(64, NULL, '554', 'IP1602200009', 2, 2, 2, 7, 7, '2019-12-27', 2, NULL, '2021-04-19 11:52:02', NULL, 'a.lopez', 'flm', 2, 'MAPA 554', '2020-03-05 00:00:00', NULL, NULL),
(66, NULL, '556', 'IP1605200325', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 556', '2020-03-05 00:00:00', NULL, NULL),
(67, NULL, '557', 'IP1602200057', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en HTU, se coloca nueva etiqueta con código y serial. 06/Marzo/2020, Camilo', 2, 'MAPA 557', '2020-03-05 00:00:00', NULL, NULL),
(68, NULL, '558', 'IP1602200170', 2, 2, 2, 4, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 558', '2020-03-05 00:00:00', NULL, NULL),
(69, NULL, '559', 'IP1602200295', 2, 30, 2, 9, 9, '2020-11-28', 1, NULL, '2020-12-01 17:13:02', NULL, 'andresl', '<br>', 2, 'MAPA 559', '2020-03-05 00:00:00', NULL, NULL),
(70, NULL, '560', 'IP1602200047', 2, 2, 2, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 560', '2020-03-05 00:00:00', NULL, NULL),
(71, NULL, '561', 'IP1602200018', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 561', '2020-03-05 00:00:00', NULL, NULL),
(72, '781ea01118ca82192.jpeg', '562', '17030100040', 2, 2, 2, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 562', '2020-03-05 00:00:00', NULL, NULL),
(73, NULL, '564', 'IP1701400086', 2, 2, 2, 5, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 564', '2020-03-05 00:00:00', NULL, NULL),
(74, NULL, '565', 'IP1701400310', 2, 2, 2, 5, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 565', '2020-03-05 00:00:00', NULL, NULL),
(75, NULL, '566', 'IP1701400267', 2, 2, 2, 5, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 566', '2020-03-05 00:00:00', NULL, NULL),
(76, NULL, '567', 'IP1701400116', 2, 2, 2, 5, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 567', '2020-03-05 00:00:00', NULL, NULL),
(77, NULL, '568', 'IP1701400067', 2, 2, 2, 4, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 568', '2020-03-05 00:00:00', NULL, NULL),
(78, 'f0babaaecbf90b169.jpeg', '569', '17030100024', 2, 2, 2, 7, 7, '2019-12-27', 2, NULL, '2021-04-19 11:49:55', NULL, 'a.lopez', 'Equipo dañado&nbsp;', 2, 'MAPA 569', '2020-03-05 00:00:00', NULL, NULL),
(79, NULL, '570', '17030100023', 2, 2, 2, 10, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 570', '2020-03-05 00:00:00', NULL, NULL),
(80, '7cdb97b607ea37f77.jpeg', '571', '17030100042', 2, 2, 2, 1, 1, '2019-12-03', 2, NULL, '2021-04-14 10:06:11', NULL, 'a.lopez', 'Se encuentra en mantenimiento en la Bodega', 2, 'MAPA 571', '2020-03-05 00:00:00', NULL, NULL),
(81, NULL, '572', '17030100190', 2, 2, 2, 2, 2, '2020-11-23', 1, NULL, '2020-11-23 08:20:10', NULL, 'andresl', NULL, 2, 'MAPA 572', '2020-03-05 00:00:00', NULL, NULL),
(82, '3bd9b5e8c99cc1012.jpeg', '573', '17030100192', 2, 2, 2, 7, 1, '2019-12-03', 1, NULL, '2021-04-13 09:02:39', NULL, 'a.lopez', '<br>', 2, 'MAPA 573', '2020-03-05 00:00:00', NULL, NULL),
(83, NULL, '574', '17030100036', 2, 2, 2, 9, 9, '2020-11-19', 1, NULL, '2020-11-19 14:52:16', NULL, 'andresl', NULL, 2, 'MAPA 574', '2020-03-05 00:00:00', NULL, NULL),
(84, NULL, '575', '17030100031', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, Funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 2, 'MAPA 575', '2020-03-05 00:00:00', NULL, NULL),
(85, NULL, '576', '17030100016', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '2021-03-08 09:46:42', NULL, 'a.lopez', '<br>', 2, 'MAPA 576', '2020-03-05 00:00:00', NULL, NULL),
(86, NULL, '577', '17030100188', 2, 2, 2, 8, 8, '2021-04-19', 3, NULL, '2021-04-19 11:43:40', NULL, 'a.lopez', NULL, 2, 'MAPA 577', '2020-03-05 00:00:00', NULL, NULL),
(87, 'dbd1a242bfc1e2cbf.jpeg', '578', '17030100055', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '2021-04-14 10:05:06', NULL, 'a.lopez', 'El dispositivo opera normalmente en el HIDC', 2, 'MAPA 578', '2020-03-05 00:00:00', NULL, NULL),
(88, NULL, '579', '17030100033', 2, 2, 2, 7, 7, '2019-12-27', 1, NULL, '2020-12-01 13:46:22', NULL, 'felipem', '<br>', 2, 'MAPA 579', '2020-03-05 00:00:00', NULL, NULL),
(89, NULL, '580', '17030100037', 2, 2, 2, 4, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 580', '2020-03-05 00:00:00', NULL, NULL),
(90, NULL, '581', '17030100029', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '2020-11-19 14:23:27', NULL, 'andresl', '<br>', 2, 'MAPA 581', '2020-03-05 00:00:00', NULL, NULL),
(91, NULL, '582', '17030100030', 2, 2, 2, 4, 4, '0000-00-00', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 582', '2020-03-05 00:00:00', NULL, NULL),
(92, NULL, '583', '17030100027', 2, 2, 2, 7, 7, '2019-12-27', 2, NULL, '2021-04-19 11:54:14', NULL, 'a.lopez', '<br>', 2, 'MAPA 583', '2020-03-05 00:00:00', NULL, NULL),
(93, NULL, '584', '17030100052', 2, 2, 2, 1, 1, '2019-12-27', 1, NULL, '2021-04-13 08:55:20', NULL, 'a.lopez', '<br>', 2, 'MAPA 584', '2020-03-05 00:00:00', NULL, NULL),
(94, NULL, '585', '17030100032', 2, 2, 2, 7, 7, '2019-12-27', 2, NULL, '2021-04-19 11:49:15', NULL, 'a.lopez', 'flm', 2, 'MAPA 585', '2020-03-05 00:00:00', NULL, NULL),
(95, NULL, '586', '17030100021', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 586', '2020-03-05 00:00:00', NULL, NULL),
(96, NULL, '587', '17030100041', 2, 2, 2, 11, 11, '2019-12-19', 2, NULL, '2021-03-02 12:29:06', NULL, 'a.lopez', NULL, 2, 'MAPA 587', '2020-03-05 00:00:00', NULL, NULL),
(97, NULL, '588', '17030100174', 2, 2, 2, 11, 17, '2020-11-28', 2, NULL, '2021-01-25 13:13:39', NULL, 'camiloz', NULL, 2, 'MAPA 588', '2020-03-05 00:00:00', NULL, NULL),
(98, NULL, '589', '17030100184', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 589', '2020-03-05 00:00:00', NULL, NULL),
(99, NULL, '251', '404262WX8', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 251', '2020-03-05 00:00:00', NULL, NULL),
(100, NULL, '252', '333407WX5', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 252', '2020-03-05 00:00:00', NULL, NULL),
(101, NULL, '253', 'US97310163', 13, 3, 25, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 25, 'TR 253', '2020-03-05 00:00:00', NULL, NULL),
(102, NULL, '254', 'US99N01008', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , S3', 0, 'TR 254', '2020-03-05 00:00:00', NULL, NULL),
(103, NULL, '255', '02M7J5', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 11-3L', 0, 'TR 255', '2020-03-05 00:00:00', NULL, NULL),
(104, NULL, '256', '03DNL7', 13, 3, 20, 3, 3, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'PHILIPS , IPx-7', 20, 'TR 256', '2020-03-05 00:00:00', NULL, NULL),
(105, NULL, '257', 'US99N01942', 13, 3, 16, 1, 1, '2020-05-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'SIUI , 21311A', 16, 'TR 257', '2020-03-05 00:00:00', NULL, NULL),
(106, NULL, '258', '3709A00256', 13, 3, 17, 2, 2, '2020-11-24', 1, NULL, '2021-04-09 13:52:07', NULL, 'a.lopez', 'HEWLETT PACKARD , 21350A', 17, 'TR 258', '2020-03-05 00:00:00', NULL, NULL),
(107, NULL, '259', 'US97404881', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , T62110', 0, 'TR 259', '2020-03-05 00:00:00', NULL, NULL),
(108, NULL, '260', 'US99300683', 13, 3, 24, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 24, 'TR 260', '2020-03-05 00:00:00', NULL, NULL),
(109, NULL, '261', 'US97302745', 13, 3, 17, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 261', '2020-03-05 00:00:00', NULL, NULL),
(110, NULL, '262', 'US99N04288', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS&nbsp;', 16, 'TR 262', '2020-03-05 00:00:00', NULL, NULL),
(111, NULL, '263', '02M7PY', 13, 3, 20, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:43:01', NULL, 'felipem', 'Actualizo, funciona normalmente en HTU, 06/Marzo/2020. Camilo', 20, 'TR 263', '2020-03-05 00:00:00', NULL, NULL),
(112, NULL, '264', '3630A00395', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21367A', 0, 'TR 264', '2020-03-05 00:00:00', NULL, NULL),
(113, NULL, '265', '18ZHQQ', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21369A', 0, 'TR 265', '2020-03-05 00:00:00', NULL, NULL),
(114, NULL, '266', '02QDYM', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 266', '2020-03-05 00:00:00', NULL, NULL),
(115, NULL, '267', '142585PD9', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 6S-RS', 0, 'TR 267', '2020-03-05 00:00:00', NULL, NULL),
(116, NULL, '268', 'US99300678', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21321A', 0, 'TR 268', '2020-03-05 00:00:00', NULL, NULL),
(117, NULL, '269', 'US99N03966', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILLIPS , 21311A', 16, 'TR 269', '2020-03-05 00:00:00', NULL, NULL),
(118, NULL, '270', 'US97403824', 13, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD ,', 0, 'TR 270', '2020-03-05 00:00:00', NULL, NULL),
(119, NULL, '271', '3716A00792', 13, 3, 26, 7, 7, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Estaba en el HIDC, el dia 4 de Marzo se llevará para el HMC', 26, 'TR 271', '2020-03-05 00:00:00', NULL, NULL),
(120, NULL, '272', 'US99N04681', 13, 3, 16, 7, 7, '2020-02-29', 2, NULL, '2020-11-30 09:33:23', NULL, 'andresl', 'PHILIPS , 21311A', 16, 'TR 272', '2020-03-05 00:00:00', NULL, NULL),
(121, NULL, '301', '20161031/BTT02/00293', 5, 1, 10, 7, 7, '2020-03-11', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 301', '2020-03-05 00:00:00', NULL, NULL),
(122, NULL, '302', '20160606/BTT02/01370', 5, 1, 48, 3, 3, '2018-11-12', 1, NULL, '2020-11-20 14:22:25', NULL, 'felipem', 'DMS 300-BTT02', 48, 'ECG 302', '2020-03-05 00:00:00', NULL, NULL),
(123, NULL, '303', '20161031/BTT02/01729', 5, 1, 10, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 303', '2020-03-05 00:00:00', NULL, NULL),
(124, NULL, '304', '20140721/BBT02/00290', 5, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 304', '2020-03-05 00:00:00', NULL, NULL),
(125, NULL, '305', 'D1081A0753', 0, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'CONTEC 8000', 0, NULL, '2020-03-05 00:00:00', NULL, NULL),
(126, NULL, '401', 'DG401', 15, 9, 9, 2, 2, '2019-12-10', 1, NULL, '2021-04-13 12:20:50', NULL, 'a.lopez', NULL, 9, 'DG 401', '2020-03-05 00:00:00', NULL, NULL),
(127, NULL, '402', 'DG402', 15, 9, 9, 6, 6, '2020-02-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'EQUIPO DAÑADO', 9, 'DG 402', '2020-03-05 00:00:00', NULL, NULL),
(128, '18a13cd3f35261938.jpg', '403', '180403', 15, 9, 9, 1, 1, '2020-11-17', 1, NULL, '2021-04-14 10:24:47', NULL, 'a.lopez', 'Usado también para Uci Fatima&nbsp;', 9, 'DG 403', '2020-03-05 00:00:00', NULL, NULL),
(129, NULL, '404', 'DG404', 15, 9, 9, 4, 4, '2020-02-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 9, 'DG 404', '2020-03-05 00:00:00', NULL, NULL),
(130, NULL, '405', 'DG405', 15, 9, 9, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Estaba con la ubicación de la CSF pero realmente está en uso en HROB. 02-03-2020 Camilo.<div><br></div><div>Se verifica nuevamente y se establece ingreso en HROB 04-03-2020. Felipe&nbsp;</div>', 9, 'DG 405', '2020-03-05 00:00:00', NULL, NULL),
(131, NULL, '406', 'DG406', 15, 9, 9, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en HTU, realmente se encuentra en HROB, pendiente localizar el dispositivo de este registro.<div><br></div><div>Actualizo, el día 17/Marzo/2020 se llevó este equipo desde HROB (Palmira) a FHSJ (Buga).</div>', 9, 'DG 406', '2020-03-05 00:00:00', NULL, NULL),
(132, NULL, '407', '180407', 15, 9, 9, 6, 6, '2018-06-08', 1, NULL, '2021-03-09 15:32:14', NULL, 'a.lopez', 'FUNDACION HOSPITAL SANJOSE, Marcado', 9, 'DG 407', '2020-03-05 00:00:00', NULL, NULL),
(133, NULL, '451', 'B97K125', 4, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 ,  ,', 0, 'DES 451', '2020-03-05 00:00:00', NULL, NULL),
(134, NULL, '452', 'B97K12587', 4, 13, 21, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020.', 21, 'DES 452', '2020-03-05 00:00:00', '2020-07-07 19:26:21', NULL),
(135, NULL, '453', 'B97K12574', 4, 13, 21, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE', 21, 'DES 453', '2020-03-05 00:00:00', NULL, NULL),
(136, NULL, '454', '98278', 4, 4, 27, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Funciona normalmente en el HROB', 27, 'DES 454', '2020-03-05 00:00:00', NULL, NULL),
(137, NULL, '455', '12129094', 4, 23, 59, 7, 7, '2020-12-02', 3, NULL, '2020-12-02 08:49:38', NULL, 'felipem', 'ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER', 59, 'DES 455', '2020-03-05 00:00:00', NULL, NULL),
(138, NULL, '456', '82399', 4, 28, 19, 2, 2, '2020-02-26', 2, NULL, '2021-04-11 13:13:54', NULL, 'a.lopez', 'ARTEMA , IP 22 , CALI , HMC', 19, 'DES 456', '2020-03-05 00:00:00', NULL, NULL),
(139, NULL, '457', '05126132', 4, 23, 35, 7, 7, '2020-07-07', 3, NULL, '2021-04-13 10:43:23', NULL, 'a.lopez', 'En reparaciòn 07/Julio/2020 Camilo.', 35, 'DES 457', '2020-03-05 00:00:00', '2020-07-07 19:18:13', NULL),
(140, NULL, '458', '90427', 4, 4, 27, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06-Marzo-2020', 27, 'DES 458', '2020-03-05 00:00:00', NULL, NULL),
(141, NULL, '601', 'A7V87A', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 601', '2020-03-05 00:00:00', NULL, NULL),
(142, NULL, '602', 'HN3F91QC100398', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 602', '2020-03-05 00:00:00', NULL, NULL),
(143, NULL, '603', 'U082MA00', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DELL ,  , INTEGRADO ,', 0, 'PC 603', '2020-03-05 00:00:00', NULL, NULL),
(144, NULL, '604', 'CLON', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'INTEL ,  , JANUS ,', 0, 'PC 604', '2020-03-05 00:00:00', NULL, NULL),
(145, NULL, '606', 'CS02589545', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 606', '2020-03-05 00:00:00', NULL, NULL),
(146, NULL, '607', 'VS81211172', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 607', '2020-03-05 00:00:00', NULL, NULL),
(147, NULL, '608', '6MC2190SSX', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 608', '2020-03-05 00:00:00', NULL, NULL),
(148, NULL, '609', '3CR2221DW9', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 609', '2020-03-05 00:00:00', NULL, NULL),
(149, NULL, '610', 'VS81225546', 11, 10, 11, 2, 2, '2020-11-24', 1, NULL, '2020-11-24 11:05:21', NULL, 'andresl', 'LENOVO ,  , INTEGRADO ,', 11, 'PC 610', '2020-03-05 00:00:00', NULL, NULL),
(150, NULL, '611', '5CB40840JJ', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , INTEGRADO ,', 0, 'PC 611', '2020-03-05 00:00:00', NULL, NULL),
(151, NULL, '612', 'HYXS91LD200746', 11, 0, 0, 6, 6, '2020-02-14', 1, NULL, '2021-04-06 17:39:40', NULL, 'a.lopez', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 612', '2020-03-05 00:00:00', NULL, NULL),
(152, NULL, '636', '5CM31902QS', 11, 0, 0, 12, 12, '2020-03-25', 1, NULL, '2021-04-06 17:43:20', NULL, 'a.lopez', 'HP ,  , INTEGRADO ,', 0, 'PC 636', '2020-03-05 00:00:00', NULL, NULL),
(153, NULL, '614', '12052393514', 11, 0, 0, 6, 6, '2020-01-17', 1, NULL, '2021-04-06 17:36:02', NULL, 'a.lopez', 'LG ,  , JONUS ,', 0, 'PC 614', '2020-03-05 00:00:00', NULL, NULL),
(154, NULL, '615', '335255', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , AOPEN ,', 0, 'PC 615', '2020-03-05 00:00:00', NULL, NULL),
(155, NULL, '617', '5CD440256Q', 11, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , HP ,', 0, 'PC 617', '2020-03-05 00:00:00', NULL, NULL),
(156, NULL, '618', 'CS01429411', 11, 10, 11, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO, TODO EN UNO', 11, 'PC 618', '2020-03-05 00:00:00', NULL, NULL),
(157, NULL, '619', 'PF0N6RLY', 11, 10, 36, 9, 9, '2020-12-01', 1, NULL, '2020-12-01 16:38:54', NULL, 'andresl', 'LENOVO ,  , NA ,', 36, 'PC 619', '2020-03-05 00:00:00', NULL, NULL),
(158, 'bcdf768ef42798802.jpg', '620', 'CS02593439', 11, 10, 11, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div><div>02-21-2020 se traslada a hidc</div>', 11, 'PC 620', '2020-03-05 00:00:00', NULL, NULL),
(159, NULL, '701', '10-10-10', 7, 14, 22, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PERFECT 10 , PERFECT 10&nbsp;', 22, 'BT 701', '2020-03-05 00:00:00', NULL, NULL),
(160, NULL, '702', '201409026161', 7, 12, 18, 9, 9, '2020-12-01', 1, NULL, '2020-12-01 16:23:41', NULL, 'andresl', 'SPORT FITNESS , JS-5000B-1 ,  ,', 18, 'BT 702', '2020-03-05 00:00:00', NULL, NULL),
(161, NULL, '704', 'SN: 270765 JC', 7, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', ',  ,  ,', 0, 'BT 704', '2020-03-05 00:00:00', NULL, NULL),
(162, NULL, '705', '201312044116', 7, 12, 18, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1', 18, 'BT 705', '2020-03-05 00:00:00', NULL, NULL),
(163, NULL, '706', '201409026192', 7, 12, 18, 2, 2, '2020-02-26', 1, NULL, '2020-11-24 10:47:36', NULL, 'andresl', 'SPORT FITNESS , JS-5000B-1&nbsp;', 18, 'BT 706', '2020-03-05 00:00:00', NULL, NULL),
(164, NULL, '708', '201509043123', 7, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS ,  ,  ,', 0, 'BT 708', '2020-03-05 00:00:00', NULL, NULL),
(165, NULL, '710', '201509043152', 7, 12, 18, 1, 1, '2020-02-29', 1, NULL, '2021-04-14 10:06:44', NULL, 'a.lopez', 'SPORT FITNESS , JS-5000B-1 , HIDC ,', 18, 'BT 710', '2020-03-05 00:00:00', NULL, NULL),
(166, NULL, '751', '20140625/BTT/01068', 6, 1, 10, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, funciona normalmente en CSF 06/Marzo/2020 Camilo.&nbsp;', 10, 'STR 751', '2020-03-05 00:00:00', NULL, NULL),
(167, NULL, '752', '20140625/BTT/01107', 6, 1, 57, 4, 4, '2020-11-28', 1, NULL, '2020-11-28 13:09:19', NULL, 'felipem', 'DMS 300-BTT01 , 20140625/BTR/01107 ,  ,', 57, 'STR 752', '2020-03-05 00:00:00', NULL, NULL),
(168, NULL, '753', '20160725/BTT/01304', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01304 ,  ,', 0, 'STR 753', '2020-03-05 00:00:00', NULL, NULL),
(169, NULL, '754', '20160104/BTT/01253', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160104/BTR/01253 ,  ,', 0, 'STR 754', '2020-03-05 00:00:00', NULL, NULL),
(170, NULL, '755', '20141201/BTT/01123', 6, 1, 10, 17, 9, '2020-02-26', 1, NULL, '2021-01-25 14:47:47', NULL, 'camiloz', 'DMS 300-BTT01 , 20141201/BTR/01123', 10, 'STR 755', '2020-03-05 00:00:00', NULL, NULL),
(171, NULL, '756', '20160725/BTT/01307', 6, 1, 10, 7, 7, '2019-12-10', 1, NULL, '2020-12-07 15:57:20', NULL, 'andresl', 'DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>', 10, 'STR 756', '2020-03-05 00:00:00', NULL, NULL),
(172, NULL, '757', '20150129/BTT/01136', 6, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20150129/BTR/01136 ,&nbsp;', 10, 'STR 757', '2020-03-05 00:00:00', NULL, NULL),
(173, NULL, '758', '20160104/BTR/01253', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 758', '2020-03-05 00:00:00', NULL, NULL),
(174, NULL, '759', '4712AU3681E', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'QRSCardUSB , NA ,  ,', 0, 'STR 759', '2020-03-05 00:00:00', NULL, NULL),
(175, NULL, '760', 'SN BTR01-343', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 760', '2020-03-05 00:00:00', NULL, NULL),
(176, NULL, '761', 'SM 311SN: D111E0004', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT03 ,  ,  ,', 0, 'STR 761', '2020-03-05 00:00:00', NULL, NULL),
(177, NULL, '762', 'SN: 20140721/BTT02/00290', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT02 ,  ,  ,', 0, 'STR 762', '2020-03-05 00:00:00', NULL, NULL),
(178, NULL, '763', '20160725/BTT/01299', 6, 0, 0, 0, 0, '0000-00-00', 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01299 ,  ,', 0, 'STR 763', '2020-03-05 00:00:00', NULL, NULL),
(183, NULL, '905', 'SM 14', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 8 GB , CASA , CASA', 42, 'USB 905', '2020-03-05 00:00:00', NULL, NULL),
(184, NULL, '906', 'SM 15', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA , TECNOLOGIA', 42, 'USB 906', '2020-03-05 00:00:00', NULL, NULL),
(185, NULL, '907', 'SM 16', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 907', '2020-03-05 00:00:00', NULL, NULL),
(186, NULL, '908', 'SM 17', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 908', '2020-03-05 00:00:00', NULL, NULL),
(187, NULL, '909', 'SM 18', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 909', '2020-03-05 00:00:00', NULL, NULL),
(188, NULL, '910', 'SM 19', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 910', '2020-03-05 00:00:00', NULL, NULL),
(189, NULL, '911', 'SM 20', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 911', '2020-03-05 00:00:00', NULL, NULL),
(190, NULL, '912', 'SM 21', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 912', '2020-03-05 00:00:00', NULL, NULL),
(191, NULL, '913', 'SM 22', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 913', '2020-03-05 00:00:00', NULL, NULL),
(192, NULL, '914', 'SM 23', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 914', '2020-03-05 00:00:00', NULL, NULL),
(193, NULL, '915', 'SM24', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 915', '2020-03-05 00:00:00', NULL, NULL),
(194, NULL, '901', 'VND3J28585', 16, 25, 42, 5, 5, '2020-10-15', 3, NULL, '2020-11-20 00:32:14', NULL, 'felipem', 'IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO', 42, 'USB 901', '2020-03-05 00:00:00', NULL, NULL),
(195, NULL, '902', 'CN26RBK063', 16, 25, 42, 3, 3, '2019-06-18', 3, NULL, '2020-11-20 00:32:56', NULL, 'felipem', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 902', '2020-03-05 00:00:00', NULL, NULL),
(196, NULL, '903', 'CN22SBTFP', 16, 25, 42, 3, 3, '2020-10-15', 3, NULL, '2020-11-20 00:34:13', NULL, 'felipem', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 903', '2020-03-05 00:00:00', NULL, NULL),
(197, NULL, '904', 'SE8Y011865', 16, 25, 42, 2, 2, '2020-10-15', 3, NULL, '2020-11-24 13:03:58', NULL, 'andresl', 'EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO', 42, 'USB 904', '2020-03-05 00:00:00', NULL, NULL),
(198, NULL, '918', 'SE8Y018164', 16, 25, 42, 3, 3, '2020-10-15', 1, NULL, '2020-11-20 00:36:26', NULL, 'felipem', 'Sin verificacion<div><br></div><div><br></div>', 42, 'USB 918', '2020-03-05 00:00:00', NULL, NULL),
(199, 'd7dffdba6db4acaa5.jpg', '134', '3A-11289', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 10:59:48 pm', '0000-00-00 00:00:00', 'felipem', 'superadmin', 'Estuche, Cable<span style=\"font-size: 0.857em;\">, China</span><span style=\"font-size: 0.857em;\">&nbsp;Noviembre 2019, Actualizo</span><span style=\"font-size: 0.857em;\">, funciona normalmente, HTU 06/Marzo/2020, Camilo.</span>', 1, 'HOLTER 134', '2019-12-05 00:00:00', NULL, NULL),
(200, '0517ef6974ab1463b.jpg', '135', '3A-11288', 1, 1, 1, 17, 17, '2019-12-11', 2, '5/12/2019 11:01:16 pm', '2021-01-25 14:37:45', 'felipem', 'camiloz', 'Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>', 1, 'HOLTER 135', '2019-12-05 00:00:00', NULL, NULL),
(201, 'c830bd855b5933e47.jpg', '136', '3A-11286', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:06:13 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 136', '2019-12-05 00:00:00', NULL, NULL),
(202, '9cd28935c6a69d0a3.jpg', '137', '3A-11292', 1, 1, 1, 17, 4, '2019-12-05', 2, '5/12/2019 11:07:54 pm', '2021-01-25 13:49:25', 'felipem', 'camiloz', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 137', '2019-12-05 00:00:00', NULL, NULL),
(203, 'ec24083a0b7fcd5a2.jpg', '138', '3A-11291', 1, 1, 1, 4, 4, '2019-12-09', 1, '5/12/2019 11:08:49 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>', NULL, 'HOLTER 138', '2019-12-05 00:00:00', NULL, NULL),
(204, 'caeae04f26da4d82b.jpg', '139', '3A-11290', 1, 1, 1, 5, 5, '2019-12-05', 1, '5/12/2019 11:09:40 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\"><br></span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo.</span></div>', 1, 'HOLTER 139', '2019-12-05 00:00:00', NULL, NULL),
(206, NULL, '590', '19120200001', 2, 2, 12, 9, 9, '2020-11-19', 1, '27/12/2019 01:58:38 pm', '2020-11-19 15:03:15', 'felipem', 'andresl', '<br>', 12, 'MAPA 590', '2019-12-27 00:00:00', NULL, NULL),
(207, NULL, '591', '19120200002', 2, 2, 12, 7, 7, '2019-12-27', 1, '27/12/2019 02:01:17 pm', NULL, 'felipem', NULL, 'Noviembre 2019 China', 12, 'MAPA 591', '2019-12-27 00:00:00', NULL, NULL),
(208, NULL, '555', '19120200010', 2, 2, 2, 11, 17, '2019-12-27', 2, NULL, '2021-03-02 12:27:50', NULL, 'a.lopez', '<br>', 2, 'MAPA 555', '2020-03-05 00:00:00', NULL, NULL),
(209, NULL, '593', '19120200012', 2, 2, 2, 7, 7, '2020-03-06', 2, NULL, '2021-04-19 11:50:55', NULL, 'a.lopez', 'Actualizo, el equipo se encontraba en bodega, ahora funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 2, 'MAPA 593', '2020-03-05 00:00:00', NULL, NULL),
(210, NULL, '594', '19120200022', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 594', '2020-03-05 00:00:00', NULL, NULL),
(211, NULL, '595', '19120200023', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 595', '2020-03-05 00:00:00', NULL, NULL),
(212, NULL, '596', '19120200027', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 596', '2020-03-05 00:00:00', NULL, NULL),
(213, NULL, '597', '19120200028', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 597', '2020-03-05 00:00:00', NULL, NULL),
(214, NULL, '598', '19120200029', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 598', '2020-03-05 00:00:00', NULL, NULL),
(215, NULL, '599', '19120200030', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 599', '2020-03-05 00:00:00', NULL, NULL),
(216, NULL, '140', '3A-11294', 1, 1, 1, 17, 9, '2020-02-04', 1, '4/2/2020 11:01:33 am', '2021-01-25 13:30:12', 'felipem', 'camiloz', 'China 5 de 15 enero 2020', 1, 'HOLTER 140', '2020-02-04 00:00:00', NULL, NULL),
(217, NULL, '141', '3A-11300', 1, 1, 1, 7, 17, '2020-02-04', 2, '4/2/2020 11:02:23 am', '2021-04-19 14:56:14', 'felipem', 'a.lopez', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 141', '2020-02-04 00:00:00', NULL, NULL),
(218, NULL, '142', '3A-11301', 1, 1, 1, 1, 1, '2020-02-04', 1, '4/2/2020 11:03:28 am', '2021-04-13 11:36:41', 'felipem', 'a.lopez', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 142', '2020-02-04 00:00:00', NULL, NULL),
(219, NULL, '143', '3A-11302', 1, 1, 1, 4, 17, '2020-02-04', 1, '4/2/2020 11:04:38 am', '2021-01-25 13:34:36', 'felipem', 'camiloz', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 143', '2020-02-04 00:00:00', NULL, NULL),
(220, NULL, '144', '3A-11304', 1, 1, 1, 7, 9, '2020-02-04', 1, '4/2/2020 11:08:00 am', '2020-11-19 15:00:27', 'felipem', 'andresl', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 144', '2020-02-04 00:00:00', NULL, NULL),
(221, NULL, '408', 'DG408', 15, 9, 9, 4, 4, '2020-02-18', 1, '18/2/2020 10:21:55 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', 'Actualizo, Funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 9, 'DG 408', '2020-02-18 00:00:00', NULL, NULL),
(222, NULL, '409', 'DG409', 15, 9, 9, 6, 6, '2016-02-08', 3, '18/2/2020 10:23:46 pm', NULL, 'felipem', NULL, 'No funciona', 9, 'DG 409', '2020-02-18 00:00:00', NULL, NULL),
(223, NULL, '622', 'MP1FZWXE', 11, 10, 15, 11, 11, '2020-02-18', 1, '19/2/2020 11:54:09 am', NULL, 'felipem', NULL, '<br>', 15, 'PC 622', '2020-02-19 00:00:00', NULL, NULL),
(224, NULL, '410', 'DG410', 15, 9, 9, 5, 5, '2020-02-21', 1, '20/2/2020 05:41:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Equipo comprado de segunda enviado 20-02-2020 por flm para csf.<div>Equipo reemplazado por el DG 414</div>', 9, 'DG 410', '2020-02-20 00:00:00', NULL, NULL),
(225, NULL, '413', 'DG413', 15, 9, 9, 5, 5, '2020-02-26', 1, '26/2/2020 05:13:35 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal', 9, 'DG 413', '2020-02-26 00:00:00', NULL, NULL),
(226, NULL, '411', '180411', 15, 9, 9, 1, 1, '2020-02-29', 1, '29/2/2020 10:15:52 am', '2021-04-14 10:17:59', 'camiloz', 'a.lopez', 'Tenia el codigo DG 407 se le otorga el codigo DG 411, Camilo.<div><br></div>', 9, 'DG 411', '2020-02-29 00:00:00', NULL, NULL),
(227, NULL, '412', 'DG412', 15, 9, 9, 2, 2, '2020-02-29', 1, '29/2/2020 10:32:18 am', '2021-04-11 12:45:46', 'camiloz', 'a.lopez', 'PENDIENTE POR CAMBIAR CODIGO DEL 401 AL 412', 9, 'DG 412', '2020-02-29 00:00:00', NULL, NULL),
(228, NULL, '414', 'DG414', 15, 9, 9, 5, 5, '2020-03-04', 3, '4/3/2020 11:09:00 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'El equipo no prende, completamente dañado', 9, 'DG 414', '2020-03-04 00:00:00', NULL, NULL),
(229, NULL, '801', 'X5NQ011045', 17, 17, 28, 1, 1, '2020-03-05', 1, '5/3/2020 11:16:49 am', '2020-11-20 00:44:30', 'felipem', 'felipem', 'Ingreso nueva 05-03-1989', 28, 'SM 801', '2020-03-05 00:00:00', NULL, NULL),
(230, NULL, '703', '201509043107', 7, 12, 18, 4, 4, '2020-03-06', 1, '6/3/2020 08:57:47 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Se trajo desde Palmira por Luis García', 18, 'BT 703', '2020-03-06 00:00:00', NULL, NULL),
(231, NULL, '605', '303NDWE33345', 11, 18, 29, 4, 4, '2020-03-06', 1, '6/3/2020 11:23:42 am', '2021-02-23 11:32:16', 'camiloz', 'a.lopez', '<br>', 29, 'PC 605', '2020-03-06 00:00:00', NULL, NULL),
(232, NULL, '546', 'L14.16.081', 18, 19, 30, 4, 4, '2020-03-06', 1, '6/3/2020 12:15:29 pm', '2020-11-28 13:03:51', 'camiloz', 'felipem', 'Funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 30, 'SM 546', '2020-03-06 00:00:00', NULL, NULL),
(233, NULL, '917', 'SM 917', 20, 20, 31, 4, 4, '2020-03-06', 1, '6/3/2020 12:40:16 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', '<br>', 31, 'SM 917', '2020-03-06 00:00:00', NULL, NULL),
(234, NULL, '508', '1078535', 19, 21, 32, 4, 4, '2020-03-06', 1, '6/3/2020 12:52:36 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Actualizado, Funcionamiento normal, 06/Marzo/2020, Camilo.', 32, 'SM 508', '2020-03-06 00:00:00', NULL, NULL),
(235, NULL, '916', 'SM 916', 20, 20, 31, 7, 7, '2020-02-26', 2, '21/3/2020 01:15:27 pm', '2020-11-26 13:27:06', 'camiloz', 'andresl', 'Actualizo, el dispositivo funciona normalmente en HMC, 21/03/2020, Camilo.', 31, 'SM 916', '2020-03-21 00:00:00', NULL, NULL),
(236, NULL, '273', '03CX6P', 13, 3, 33, 1, 1, '2020-05-06', 1, '6/5/2020 06:19:31 pm', '2021-04-13 09:53:38', 'felipem', 'a.lopez', '<br>', 33, 'TR 273', '2020-05-06 00:00:00', NULL, NULL),
(237, NULL, '274', 'US99N01847', 13, 3, 16, 7, 7, '2020-05-07', 2, '7/5/2020 11:57:32 am', '2020-11-30 09:17:52', 'felipem', 'andresl', '<br>', 16, 'TR 274', '2020-05-07 00:00:00', NULL, NULL),
(238, NULL, '1002', 'IHTD56VD1', 21, 22, 34, 1, 1, '2020-05-08', 1, '8/5/2020 08:54:23 am', NULL, 'felipem', NULL, 'SJUG7749AA', 34, 'CEL 1002', '2020-05-08 00:00:00', NULL, NULL),
(239, NULL, '629', 'CS02781268', 11, 10, 11, 4, 4, '2020-05-22', 1, '22/5/2020 01:27:33 pm', '2021-02-23 12:18:39', 'felipem', 'a.lopez', '<br>', 11, 'PC 629', '2020-05-22 00:00:00', NULL, NULL),
(240, NULL, '626', 'MP12PNNW', 11, 10, 36, 4, 4, '2020-05-22', 1, '22/5/2020 01:55:36 pm', '2021-02-23 11:37:25', 'felipem', 'a.lopez', 'Utilizado para ECG', 36, 'PC 626', '2020-05-22 00:00:00', NULL, NULL),
(241, NULL, '635', 'MP189C54', 11, 10, 37, 4, 4, '2020-11-28', 1, '22/5/2020 02:53:45 pm', '2021-02-23 11:29:48', 'felipem', 'a.lopez', '<br>', 37, 'PC 635', '2020-05-22 00:00:00', NULL, NULL),
(242, NULL, '628', '5CM3340JQV', 11, 24, 38, 4, 4, '2020-05-22', 1, '22/5/2020 03:15:52 pm', '2021-02-23 11:31:36', 'felipem', 'a.lopez', '<div>Función: HOLTER Y MAPA</div><div><div><br></div></div>', 38, 'PC 628', '2020-05-22 00:00:00', NULL, NULL);
INSERT INTO `dispositivos` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(243, NULL, '713', 'L1317121', 18, 19, 30, 9, 9, '2019-11-01', 1, NULL, '2020-12-07 15:44:14', 'lgarcia', 'andresl', 'EQUIPO PROCEDENTE DE HFPS', 30, 'SM 713', '2020-09-28 16:18:07', '2020-09-28 16:10:48', NULL),
(244, NULL, '351', '00267', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 39, 'CAB 351', '2020-10-01 16:40:44', NULL, NULL),
(245, NULL, '352', '00272', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, NULL, 'superadmin', NULL, '<br>', 39, 'CAB 352', '2020-10-01 16:46:00', NULL, NULL),
(246, NULL, '550', '17030100017', 2, 2, 2, 4, 4, '2020-10-08', 1, NULL, NULL, 'superadmin', NULL, '<br>', 2, 'MAPA 550', '2020-10-08 15:08:34', NULL, NULL),
(247, NULL, '613', 'MXL3162BRT', 11, 24, 40, 4, 4, '2020-09-22', 1, NULL, '2021-02-23 11:28:09', 'superadmin', 'a.lopez', '<br>', 40, 'PC 613', '2020-10-15 12:25:44', NULL, NULL),
(248, NULL, '275', 'US30432955', 13, 3, 45, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, 'Sonda trasesofagica', 45, 'TR 275', '2020-10-15 16:45:56', NULL, NULL),
(249, NULL, '276', 'US99N02863', 13, 3, 16, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 16, 'TR 276', '2020-10-15 16:47:56', NULL, NULL),
(250, NULL, '711', '201606060064', 7, 12, 18, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 18, 'BT 711', '2020-10-15 17:07:24', NULL, NULL),
(251, NULL, '815', 'UKTY002906', 17, 17, 46, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 46, 'SM 815', '2020-10-15 17:41:09', NULL, NULL),
(252, NULL, '816', 'X5E8006892', 17, 17, 47, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 47, 'SM 816', '2020-10-15 17:45:41', NULL, NULL),
(253, NULL, '817', 'SE8Y017621', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 817', '2020-10-15 17:48:07', NULL, NULL),
(254, NULL, '818', 'SE8Y017614', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 818', '2020-10-15 17:50:16', NULL, NULL),
(255, NULL, '145', '145', 1, 26, 44, 7, 7, '2021-01-25', 4, NULL, '2021-01-25 17:55:12', 'superadmin', 'felipem', 'HOLTER', 44, 'HOLTER 145', '2020-10-15 17:55:00', NULL, NULL),
(256, NULL, '146', '146', 1, 26, 44, 7, 7, '2021-01-25', 4, NULL, '2021-01-25 17:54:24', 'superadmin', 'felipem', 'HOLTER', 44, 'HOLTER 146', '2020-10-15 17:56:35', NULL, NULL),
(257, NULL, '147', '147', 1, 26, 44, 7, 7, '2020-11-19', 4, NULL, '2021-01-25 17:53:51', 'superadmin', 'felipem', 'HOLTER', 44, 'HOLTER 147', '2020-10-15 17:57:25', NULL, NULL),
(258, NULL, '526', 'OL1019199', 18, 19, 30, 3, 3, '2019-08-19', 1, NULL, '2020-11-20 00:19:46', 'lgarcia', 'felipem', NULL, 30, 'SM 526', '2020-11-09 21:32:07', NULL, NULL),
(311, NULL, '199', '199', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, 'HOLTER', NULL, 'HOLTER 199', NULL, NULL, NULL),
(336, NULL, '250', '250', 3, 3, 3, 11, 16, '2021-02-18', 1, NULL, '2021-02-19 10:44:39', NULL, 'a.lopez', NULL, 3, 'ECO 250', NULL, NULL, NULL),
(360, NULL, '300', 'US99N03069', 13, 3, 16, 1, 1, '2021-04-13', 1, NULL, '2021-04-13 10:54:25', NULL, 'a.lopez', NULL, 16, 'TR 300', NULL, NULL, NULL),
(405, NULL, '350', '350', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 350', NULL, NULL, NULL),
(453, NULL, '400', '400', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 400', NULL, NULL, NULL),
(489, NULL, '450', '450', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 450', NULL, NULL, NULL),
(531, NULL, '500', '500', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 500', NULL, NULL, NULL),
(555, NULL, '525', '525', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 525', NULL, NULL, NULL),
(580, NULL, '600', '600', 2, 26, 44, 16, 16, '2020-11-20', 4, NULL, '2020-11-20 00:15:19', NULL, 'felipem', NULL, 44, 'MAPA 600', NULL, NULL, NULL),
(654, NULL, '700', '41455837477', 11, 33, 56, 4, 16, '2020-12-29', 1, NULL, '2020-12-29 14:47:42', NULL, 'andresl', NULL, 56, 'PC 700', NULL, NULL, NULL),
(694, NULL, '750', '750', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 750', NULL, NULL, NULL),
(731, NULL, '800', '800', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 800', NULL, NULL, NULL),
(822, NULL, '900', 'X5NQ070467', 17, 17, 28, 6, 6, '2020-07-24', 1, NULL, '2021-03-11 14:19:57', NULL, 'a.lopez', NULL, 28, 'SM 900', NULL, NULL, NULL),
(904, NULL, '999', '999', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 999', NULL, NULL, NULL),
(1003, NULL, '1100', '1100', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1100', NULL, NULL, NULL),
(953, NULL, '1050', '1050', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1050', NULL, NULL, NULL),
(260, NULL, '148', '3A-11948', 1, 1, 1, 2, 2, '2020-11-23', 1, NULL, '2021-04-09 13:50:24', NULL, 'a.lopez', NULL, 1, 'HOLTER 148', NULL, NULL, NULL),
(261, NULL, '149', '149', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 149', NULL, NULL, NULL),
(262, NULL, '150', '150', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 150', NULL, NULL, NULL),
(263, NULL, '151', '151', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 151', NULL, NULL, NULL),
(264, NULL, '152', '152', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 152', NULL, NULL, NULL),
(265, NULL, '153', '153', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 153', NULL, NULL, NULL),
(266, NULL, '154', '154', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 154', NULL, NULL, NULL),
(267, NULL, '155', '155', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 155', NULL, NULL, NULL),
(268, NULL, '156', '156', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 156', NULL, NULL, NULL),
(269, NULL, '157', '157', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 157', NULL, NULL, NULL),
(270, NULL, '158', '158', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 158', NULL, NULL, NULL),
(271, NULL, '159', '159', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 159', NULL, NULL, NULL),
(272, NULL, '160', '160', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 160', NULL, NULL, NULL),
(273, NULL, '161', '161', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 161', NULL, NULL, NULL),
(274, NULL, '162', '162', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 162', NULL, NULL, NULL),
(275, NULL, '163', '163', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 163', NULL, NULL, NULL),
(276, NULL, '164', '164', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 164', NULL, NULL, NULL),
(277, NULL, '165', '165', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 165', NULL, NULL, NULL),
(278, NULL, '166', '166', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 166', NULL, NULL, NULL),
(279, NULL, '167', '167', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 167', NULL, NULL, NULL),
(280, NULL, '168', '168', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 168', NULL, NULL, NULL),
(281, NULL, '169', '169', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 169', NULL, NULL, NULL),
(282, NULL, '170', '170', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 170', NULL, NULL, NULL),
(283, NULL, '171', '171', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 171', NULL, NULL, NULL),
(284, NULL, '172', '172', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 172', NULL, NULL, NULL),
(285, NULL, '173', '173', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 173', NULL, NULL, NULL),
(286, NULL, '174', '174', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 174', NULL, NULL, NULL),
(287, NULL, '175', '175', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 175', NULL, NULL, NULL),
(288, NULL, '176', '176', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 176', NULL, NULL, NULL),
(289, NULL, '177', '177', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 177', NULL, NULL, NULL),
(290, NULL, '178', '178', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 178', NULL, NULL, NULL),
(291, NULL, '179', '179', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 179', NULL, NULL, NULL),
(292, NULL, '180', '180', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 180', NULL, NULL, NULL),
(293, NULL, '181', '181', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 181', NULL, NULL, NULL),
(294, NULL, '182', '182', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 182', NULL, NULL, NULL),
(295, NULL, '183', '183', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 183', NULL, NULL, NULL),
(296, NULL, '184', '184', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 184', NULL, NULL, NULL),
(297, NULL, '185', '185', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 185', NULL, NULL, NULL),
(298, NULL, '186', '186', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 186', NULL, NULL, NULL),
(299, NULL, '187', '187', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 187', NULL, NULL, NULL),
(300, NULL, '188', '188', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 188', NULL, NULL, NULL),
(301, NULL, '189', '189', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 189', NULL, NULL, NULL),
(302, NULL, '190', '190', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 190', NULL, NULL, NULL),
(303, NULL, '191', '191', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 191', NULL, NULL, NULL),
(304, NULL, '192', '192', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 192', NULL, NULL, NULL),
(305, NULL, '193', '193', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 193', NULL, NULL, NULL),
(306, NULL, '194', '194', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 194', NULL, NULL, NULL),
(307, NULL, '195', '195', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 195', NULL, NULL, NULL),
(308, NULL, '196', '196', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 196', NULL, NULL, NULL),
(309, NULL, '197', '197', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 197', NULL, NULL, NULL),
(310, NULL, '198', '198', 1, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'HOLTER 198', NULL, NULL, NULL),
(312, NULL, '226', '226', 3, 3, 3, 4, 4, '2020-11-28', 1, NULL, '2020-11-28 10:22:58', NULL, 'andresl', NULL, 3, 'ECO 226', NULL, NULL, NULL),
(313, NULL, '227', '227', 3, 3, 58, 14, 14, '2020-12-01', 1, NULL, '2021-03-01 16:26:14', NULL, 'a.lopez', NULL, 58, 'ECO 227', NULL, NULL, NULL),
(314, NULL, '228', '3728A0080', 3, 38, 72, 2, 16, '2021-04-08', 1, NULL, '2021-04-08 15:16:41', NULL, 'a.lopez', NULL, 72, 'ECO 228', NULL, NULL, NULL),
(315, NULL, '229', '229', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 229', NULL, NULL, NULL),
(316, NULL, '230', '230', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 230', NULL, NULL, NULL),
(317, NULL, '231', '231', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 231', NULL, NULL, NULL),
(318, NULL, '232', '232', 3, 3, 3, 13, 13, '2020-12-03', 1, NULL, '2020-12-03 16:15:06', NULL, 'andresl', NULL, 3, 'ECO 232', NULL, NULL, NULL),
(319, NULL, '233', '233', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 233', NULL, NULL, NULL),
(320, NULL, '234', '234', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 234', NULL, NULL, NULL),
(321, NULL, '235', '235', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 235', NULL, NULL, NULL),
(322, NULL, '236', '236', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 236', NULL, NULL, NULL),
(323, NULL, '237', '237', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 237', NULL, NULL, NULL),
(324, NULL, '238', '238', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 238', NULL, NULL, NULL),
(325, NULL, '239', '239', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 239', NULL, NULL, NULL),
(326, NULL, '240', '240', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 240', NULL, NULL, NULL),
(327, NULL, '241', '241', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 241', NULL, NULL, NULL),
(328, NULL, '242', '242', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 242', NULL, NULL, NULL),
(329, NULL, '243', '243', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 243', NULL, NULL, NULL),
(330, NULL, '244', '244', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 244', NULL, NULL, NULL),
(331, NULL, '245', '245', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 245', NULL, NULL, NULL),
(332, NULL, '246', '246', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 246', NULL, NULL, NULL),
(333, NULL, '247', '247', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 247', NULL, NULL, NULL),
(334, NULL, '248', '248', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 248', NULL, NULL, NULL),
(335, NULL, '249', '249', 3, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECO 249', NULL, NULL, NULL),
(337, NULL, '277', '277', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 277', NULL, NULL, NULL),
(338, NULL, '278', '278', 13, 3, 16, 4, 4, '2020-11-28', 1, NULL, '2020-11-28 12:58:58', NULL, 'andresl', NULL, 16, 'TR 278', NULL, NULL, NULL),
(339, NULL, '279', '279', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 279', NULL, NULL, NULL),
(340, NULL, '280', '280', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 280', NULL, NULL, NULL),
(341, NULL, '281', '281', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 281', NULL, NULL, NULL),
(342, NULL, '282', '282', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 282', NULL, NULL, NULL),
(343, NULL, '283', '283', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 283', NULL, NULL, NULL),
(344, NULL, '284', '284', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 284', NULL, NULL, NULL),
(345, NULL, '285', '285', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 285', NULL, NULL, NULL),
(346, NULL, '286', '286', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 286', NULL, NULL, NULL),
(347, NULL, '287', '287', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 287', NULL, NULL, NULL),
(348, NULL, '288', '288', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 288', NULL, NULL, NULL),
(349, NULL, '289', '289', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 289', NULL, NULL, NULL),
(350, NULL, '290', '02YVT9', 13, 3, 20, 2, 2, '2020-08-14', 1, NULL, '2021-04-09 13:56:31', NULL, 'a.lopez', NULL, 20, 'TR 290', NULL, NULL, NULL),
(351, NULL, '291', '291', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 291', NULL, NULL, NULL),
(352, NULL, '292', '0384TM', 13, 3, 20, 13, 13, '2020-12-03', 1, NULL, '2020-12-03 16:19:20', NULL, 'andresl', NULL, 20, 'TR 292', NULL, NULL, NULL),
(353, NULL, '293', '293', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 293', NULL, NULL, NULL),
(354, NULL, '294', '294', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 294', NULL, NULL, NULL),
(355, NULL, '295', '295', 13, 3, 20, 1, 1, '2020-09-16', 1, NULL, '2021-04-13 11:25:44', NULL, 'a.lopez', NULL, 20, 'TR 295', NULL, NULL, NULL),
(356, NULL, '296', '296', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 296', NULL, NULL, NULL),
(357, NULL, '297', '297', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 297', NULL, NULL, NULL),
(358, NULL, '298', '298', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 298', NULL, NULL, NULL),
(359, NULL, '299', '299', 13, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'TR 299', NULL, NULL, NULL),
(361, NULL, '306', '20161031/BTT02/01743', 5, 26, 44, 16, 16, '2020-11-20', 4, NULL, '2020-11-20 14:23:21', NULL, 'felipem', NULL, 44, 'ECG 306', NULL, NULL, NULL),
(362, NULL, '307', '307', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 307', NULL, NULL, NULL),
(363, NULL, '308', '308', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 308', NULL, NULL, NULL),
(364, NULL, '309', '309', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 309', NULL, NULL, NULL),
(365, NULL, '310', '310', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 310', NULL, NULL, NULL),
(366, NULL, '311', '311', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 311', NULL, NULL, NULL),
(367, NULL, '312', '312', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 312', NULL, NULL, NULL),
(368, NULL, '313', '313', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 313', NULL, NULL, NULL),
(369, NULL, '314', '314', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 314', NULL, NULL, NULL),
(370, NULL, '315', '315', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 315', NULL, NULL, NULL),
(371, NULL, '316', '316', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 316', NULL, NULL, NULL),
(372, NULL, '317', '317', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 317', NULL, NULL, NULL),
(373, NULL, '318', '318', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 318', NULL, NULL, NULL),
(374, NULL, '319', '319', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 319', NULL, NULL, NULL),
(375, NULL, '320', '320', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 320', NULL, NULL, NULL),
(376, NULL, '321', '321', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 321', NULL, NULL, NULL),
(377, NULL, '322', '322', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 322', NULL, NULL, NULL),
(378, NULL, '323', '323', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 323', NULL, NULL, NULL),
(379, NULL, '324', '324', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 324', NULL, NULL, NULL),
(380, NULL, '325', '325', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 325', NULL, NULL, NULL),
(381, NULL, '326', '326', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 326', NULL, NULL, NULL),
(382, NULL, '327', '327', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 327', NULL, NULL, NULL),
(383, NULL, '328', '328', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 328', NULL, NULL, NULL),
(384, NULL, '329', '329', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 329', NULL, NULL, NULL),
(385, NULL, '330', '330', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 330', NULL, NULL, NULL),
(386, NULL, '331', '331', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 331', NULL, NULL, NULL),
(387, NULL, '332', '332', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 332', NULL, NULL, NULL),
(388, NULL, '333', '333', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 333', NULL, NULL, NULL),
(389, NULL, '334', '334', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 334', NULL, NULL, NULL),
(390, NULL, '335', '335', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 335', NULL, NULL, NULL),
(391, NULL, '336', '336', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 336', NULL, NULL, NULL),
(392, NULL, '337', '337', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 337', NULL, NULL, NULL),
(393, NULL, '338', '338', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 338', NULL, NULL, NULL),
(394, NULL, '339', '339', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 339', NULL, NULL, NULL),
(395, NULL, '340', '340', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 340', NULL, NULL, NULL),
(396, NULL, '341', '341', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 341', NULL, NULL, NULL),
(397, NULL, '342', '342', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 342', NULL, NULL, NULL),
(398, NULL, '343', '343', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 343', NULL, NULL, NULL),
(399, NULL, '344', '344', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 344', NULL, NULL, NULL),
(400, NULL, '345', '345', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 345', NULL, NULL, NULL),
(401, NULL, '346', '346', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 346', NULL, NULL, NULL),
(402, NULL, '347', '347', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 347', NULL, NULL, NULL),
(403, NULL, '348', '348', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 348', NULL, NULL, NULL),
(404, NULL, '349', '349', 5, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'ECG 349', NULL, NULL, NULL),
(406, NULL, '353', '353', 10, 1, 10, 1, 1, '2020-11-25', 1, NULL, '2020-11-25 10:36:15', NULL, 'andresl', NULL, 10, 'CAB 353', NULL, NULL, NULL),
(407, NULL, '354', '354', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 354', NULL, NULL, NULL),
(408, NULL, '355', '355', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 355', NULL, NULL, NULL),
(409, NULL, '356', '356', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 356', NULL, NULL, NULL),
(410, NULL, '357', '357', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 357', NULL, NULL, NULL),
(411, NULL, '358', '358', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 358', NULL, NULL, NULL),
(412, NULL, '359', '359', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 359', NULL, NULL, NULL),
(413, NULL, '360', '360', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 360', NULL, NULL, NULL),
(414, NULL, '361', '361', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 361', NULL, NULL, NULL),
(415, NULL, '362', '362', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 362', NULL, NULL, NULL),
(416, NULL, '363', '363', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 363', NULL, NULL, NULL),
(417, NULL, '364', '364', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 364', NULL, NULL, NULL),
(418, NULL, '365', '365', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 365', NULL, NULL, NULL),
(419, NULL, '366', '366', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 366', NULL, NULL, NULL),
(420, NULL, '367', '367', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 367', NULL, NULL, NULL),
(421, NULL, '368', '368', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 368', NULL, NULL, NULL),
(422, NULL, '369', '369', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 369', NULL, NULL, NULL),
(423, NULL, '370', '370', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 370', NULL, NULL, NULL),
(424, NULL, '371', '371', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 371', NULL, NULL, NULL),
(425, NULL, '372', '372', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 372', NULL, NULL, NULL),
(426, NULL, '373', '373', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 373', NULL, NULL, NULL),
(427, NULL, '374', '374', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 374', NULL, NULL, NULL),
(428, NULL, '375', '375', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 375', NULL, NULL, NULL),
(429, NULL, '376', '376', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 376', NULL, NULL, NULL),
(430, NULL, '377', '377', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 377', NULL, NULL, NULL),
(431, NULL, '378', '378', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 378', NULL, NULL, NULL),
(432, NULL, '379', '379', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 379', NULL, NULL, NULL),
(433, NULL, '380', '380', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 380', NULL, NULL, NULL),
(434, NULL, '381', '381', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 381', NULL, NULL, NULL),
(435, NULL, '382', '382', 10, 3, 51, 2, 2, '2020-11-24', 1, NULL, '2020-11-24 12:30:06', NULL, 'andresl', NULL, 51, 'CAB 382', NULL, NULL, NULL),
(436, NULL, '383', '383', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 383', NULL, NULL, NULL),
(437, NULL, '384', '384', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 384', NULL, NULL, NULL),
(438, NULL, '385', '385', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 385', NULL, NULL, NULL),
(439, NULL, '386', '386', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 386', NULL, NULL, NULL),
(440, NULL, '387', '387', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 387', NULL, NULL, NULL),
(441, NULL, '388', '388', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 388', NULL, NULL, NULL),
(442, NULL, '389', '389', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 389', NULL, NULL, NULL),
(443, NULL, '390', '390', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 390', NULL, NULL, NULL),
(444, NULL, '391', '391', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 391', NULL, NULL, NULL),
(445, NULL, '392', '392', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 392', NULL, NULL, NULL),
(446, NULL, '393', '393', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 393', NULL, NULL, NULL),
(447, NULL, '394', '394', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 394', NULL, NULL, NULL),
(448, NULL, '395', '395', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 395', NULL, NULL, NULL),
(449, NULL, '396', '396', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 396', NULL, NULL, NULL),
(450, NULL, '397', '397', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 397', NULL, NULL, NULL),
(451, NULL, '398', '398', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 398', NULL, NULL, NULL),
(452, NULL, '399', '399', 10, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CAB 399', NULL, NULL, NULL),
(454, NULL, '415', 'DG 415', 15, 9, 9, 7, 7, '2020-11-20', 1, NULL, '2020-11-20 11:19:45', NULL, 'felipem', NULL, 9, 'DG 415', NULL, NULL, NULL),
(455, NULL, '416', '416', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 416', NULL, NULL, NULL),
(456, NULL, '417', '417', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 417', NULL, NULL, NULL),
(457, NULL, '418', '418', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 418', NULL, NULL, NULL),
(458, NULL, '419', '419', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 419', NULL, NULL, NULL),
(459, NULL, '420', '420', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 420', NULL, NULL, NULL),
(460, NULL, '421', '421', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 421', NULL, NULL, NULL),
(461, NULL, '422', '422', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 422', NULL, NULL, NULL),
(462, NULL, '423', '423', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 423', NULL, NULL, NULL),
(463, NULL, '424', '424', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 424', NULL, NULL, NULL),
(464, NULL, '425', '425', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 425', NULL, NULL, NULL),
(465, NULL, '426', '426', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 426', NULL, NULL, NULL),
(466, NULL, '427', '427', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 427', NULL, NULL, NULL),
(467, NULL, '428', '428', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 428', NULL, NULL, NULL),
(468, NULL, '429', '429', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 429', NULL, NULL, NULL),
(469, NULL, '430', '430', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 430', NULL, NULL, NULL),
(470, NULL, '431', '431', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 431', NULL, NULL, NULL),
(471, NULL, '432', '432', 15, 9, 9, 2, 2, '2021-02-12', 1, NULL, '2021-04-09 08:38:51', NULL, 'a.lopez', NULL, 9, 'DG 432', NULL, NULL, NULL),
(472, NULL, '433', '433', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 433', NULL, NULL, NULL),
(473, NULL, '434', '434', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 434', NULL, NULL, NULL),
(474, NULL, '435', '435', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 435', NULL, NULL, NULL),
(475, NULL, '436', '436', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 436', NULL, NULL, NULL),
(476, NULL, '437', '437', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 437', NULL, NULL, NULL),
(477, NULL, '438', '438', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 438', NULL, NULL, NULL),
(478, NULL, '439', '439', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 439', NULL, NULL, NULL),
(479, NULL, '440', '440', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 440', NULL, NULL, NULL),
(480, NULL, '441', '441', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 441', NULL, NULL, NULL),
(481, NULL, '442', '442', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 442', NULL, NULL, NULL),
(482, NULL, '443', '443', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 443', NULL, NULL, NULL),
(483, NULL, '444', '444', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 444', NULL, NULL, NULL),
(484, NULL, '445', '445', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 445', NULL, NULL, NULL),
(485, NULL, '446', '446', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 446', NULL, NULL, NULL),
(486, NULL, '447', '447', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 447', NULL, NULL, NULL),
(487, NULL, '448', '448', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 448', NULL, NULL, NULL),
(488, NULL, '449', '449', 15, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DG 449', NULL, NULL, NULL),
(490, NULL, '459', '459', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 459', NULL, NULL, NULL),
(491, NULL, '460', '460', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 460', NULL, NULL, NULL),
(492, NULL, '461', '461', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 461', NULL, NULL, NULL),
(493, NULL, '462', '462', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 462', NULL, NULL, NULL),
(494, NULL, '463', '463', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 463', NULL, NULL, NULL),
(495, NULL, '464', '464', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 464', NULL, NULL, NULL),
(496, NULL, '465', '465', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 465', NULL, NULL, NULL),
(497, NULL, '466', '466', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 466', NULL, NULL, NULL),
(498, NULL, '467', '467', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 467', NULL, NULL, NULL),
(499, NULL, '468', '468', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 468', NULL, NULL, NULL),
(500, NULL, '469', '469', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 469', NULL, NULL, NULL),
(501, NULL, '470', '470', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 470', NULL, NULL, NULL),
(502, NULL, '471', '471', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 471', NULL, NULL, NULL),
(503, NULL, '472', '472', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 472', NULL, NULL, NULL),
(504, NULL, '473', '473', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 473', NULL, NULL, NULL),
(505, NULL, '474', '474', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 474', NULL, NULL, NULL),
(506, NULL, '475', '475', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 475', NULL, NULL, NULL),
(507, NULL, '476', '476', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 476', NULL, NULL, NULL),
(508, NULL, '477', '477', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 477', NULL, NULL, NULL),
(509, NULL, '478', '478', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 478', NULL, NULL, NULL),
(510, NULL, '479', '479', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 479', NULL, NULL, NULL),
(511, NULL, '480', '480', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 480', NULL, NULL, NULL),
(512, NULL, '481', '481', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 481', NULL, NULL, NULL),
(513, NULL, '482', '482', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 482', NULL, NULL, NULL),
(514, NULL, '483', '12127290', 4, 23, 35, 1, 1, '2021-04-13', 1, NULL, '2021-04-13 10:42:03', NULL, 'a.lopez', NULL, 35, 'DES 483', NULL, NULL, NULL),
(515, NULL, '484', '484', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 484', NULL, NULL, NULL),
(516, NULL, '485', '485', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 485', NULL, NULL, NULL),
(517, NULL, '486', '486', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 486', NULL, NULL, NULL),
(518, NULL, '487', '487', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 487', NULL, NULL, NULL),
(519, NULL, '488', '488', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 488', NULL, NULL, NULL),
(520, NULL, '489', '489', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 489', NULL, NULL, NULL),
(521, NULL, '490', '490', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 490', NULL, NULL, NULL),
(522, NULL, '491', '491', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 491', NULL, NULL, NULL),
(523, NULL, '492', '492', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 492', NULL, NULL, NULL),
(524, NULL, '493', '493', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 493', NULL, NULL, NULL),
(525, NULL, '494', '494', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 494', NULL, NULL, NULL),
(526, NULL, '495', '495', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 495', NULL, NULL, NULL),
(527, NULL, '496', '496', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 496', NULL, NULL, NULL),
(528, NULL, '497', '497', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 497', NULL, NULL, NULL),
(529, NULL, '498', '498', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 498', NULL, NULL, NULL),
(530, NULL, '499', '499', 4, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'DES 499', NULL, NULL, NULL),
(532, NULL, '501', '501', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 501', NULL, NULL, NULL),
(533, NULL, '502', '502', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 502', NULL, NULL, NULL),
(534, NULL, '503', '503', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 503', NULL, NULL, NULL),
(535, NULL, '504', '504', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 504', NULL, NULL, NULL),
(536, NULL, '505', '505', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 505', NULL, NULL, NULL),
(537, NULL, '506', '506', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 506', NULL, NULL, NULL),
(538, NULL, '507', '507', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 507', NULL, NULL, NULL),
(539, NULL, '509', '509', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 509', NULL, NULL, NULL),
(540, NULL, '510', '510', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 510', NULL, NULL, NULL),
(541, NULL, '511', '511', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 511', NULL, NULL, NULL),
(542, NULL, '512', '512', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 512', NULL, NULL, NULL),
(543, NULL, '513', '513', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 513', NULL, NULL, NULL),
(544, NULL, '514', '514', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 514', NULL, NULL, NULL),
(545, NULL, '515', '515', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 515', NULL, NULL, NULL),
(546, NULL, '516', '516', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 516', NULL, NULL, NULL),
(547, NULL, '517', '517', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 517', NULL, NULL, NULL),
(548, NULL, '518', '518', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 518', NULL, NULL, NULL),
(549, NULL, '519', '519', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 519', NULL, NULL, NULL),
(550, NULL, '520', '520', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 520', NULL, NULL, NULL),
(551, NULL, '521', '521', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 521', NULL, NULL, NULL),
(552, NULL, '522', '522', 19, 20, 31, 4, 4, '2020-11-28', 1, NULL, '2020-11-28 12:32:55', NULL, 'andresl', NULL, 31, 'SM 522', NULL, NULL, NULL),
(553, NULL, '523', '523', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 523', NULL, NULL, NULL),
(554, NULL, '524', '524', 19, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 524', NULL, NULL, NULL),
(557, NULL, '527', '527', 18, 19, 30, 2, 2, '2020-11-23', 1, NULL, '2020-11-26 12:57:11', NULL, 'andresl', NULL, 30, 'SM 527', NULL, NULL, NULL),
(558, NULL, '528', '528', 20, 20, 31, 2, 2, '2020-11-23', 1, NULL, '2020-11-23 10:52:46', NULL, 'andresl', NULL, 31, 'SM 528', NULL, NULL, NULL),
(559, NULL, '529', '529', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 529', NULL, NULL, NULL),
(560, NULL, '530', '530', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 530', NULL, NULL, NULL),
(561, NULL, '531', '531', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 531', NULL, NULL, NULL),
(562, NULL, '532', '532', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 532', NULL, NULL, NULL),
(563, NULL, '533', '533', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 533', NULL, NULL, NULL),
(564, NULL, '534', '534', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 534', NULL, NULL, NULL),
(565, NULL, '535', '535', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 535', NULL, NULL, NULL),
(566, NULL, '536', '536', 20, 20, 31, 6, 6, '2020-12-04', 1, NULL, '2020-12-04 09:42:51', NULL, 'andresl', NULL, 31, 'SM 536', NULL, NULL, NULL),
(567, NULL, '537', '537', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 537', NULL, NULL, NULL),
(568, NULL, '538', '538', 18, 26, 44, 4, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 538', NULL, NULL, NULL),
(569, NULL, '539', '539', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 539', NULL, NULL, NULL),
(570, NULL, '540', '540', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 540', NULL, NULL, NULL),
(571, NULL, '541', '541', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 541', NULL, NULL, NULL),
(572, NULL, '542', '542', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 542', NULL, NULL, NULL),
(573, NULL, '543', '543', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 543', NULL, NULL, NULL),
(574, NULL, '544', '544', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 544', NULL, NULL, NULL),
(575, NULL, '545', '545', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 545', NULL, NULL, NULL),
(576, NULL, '547', '547', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 547', NULL, NULL, NULL),
(577, NULL, '548', '548', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 548', NULL, NULL, NULL),
(578, NULL, '549', '549', 18, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 549', NULL, NULL, NULL),
(579, NULL, '563', '563', 2, 26, 44, 16, 16, '2020-11-20', 4, NULL, '2020-11-20 00:13:22', NULL, 'felipem', NULL, 44, 'MAPA 563', NULL, NULL, NULL),
(581, NULL, '616', 'PB-P1WRA', 11, 10, 49, 2, 2, '2020-11-23', 1, NULL, '2020-11-23 18:25:00', NULL, 'felipem', NULL, 49, 'PC 616', NULL, NULL, NULL),
(582, NULL, '623', '623', 11, 24, 40, 1, 1, '2020-11-30', 1, NULL, '2020-11-30 10:39:53', NULL, 'andresl', NULL, 40, 'PC 623', NULL, NULL, NULL),
(583, NULL, '624', '624', 11, 10, 36, 1, 1, '2020-11-30', 1, NULL, '2020-11-30 10:56:31', NULL, 'andresl', NULL, 36, 'PC 624', NULL, NULL, NULL),
(584, NULL, '625', '625', 11, 24, 40, 1, 1, '2020-11-30', 1, NULL, '2020-11-30 10:57:40', NULL, 'andresl', NULL, 40, 'PC 625', NULL, NULL, NULL),
(585, NULL, '627', '43329862585', 11, 33, 56, 4, 4, '2020-12-28', 1, NULL, '2020-12-03 09:24:50', NULL, 'andresl', NULL, 56, 'PC 627', NULL, NULL, NULL),
(586, NULL, '630', '43229868586', 11, 24, 40, 4, 4, '2020-11-28', 1, NULL, '2021-02-23 11:57:33', NULL, 'a.lopez', NULL, 40, 'PC 630', NULL, NULL, NULL),
(587, NULL, '631', '303NDWE33346', 11, 24, 40, 4, 4, '2020-11-28', 1, NULL, '2021-02-23 11:40:03', NULL, 'a.lopez', NULL, 40, 'PC 631', NULL, NULL, NULL),
(588, NULL, '632', '632', 11, 24, 40, 1, 1, '2020-11-25', 1, NULL, '2020-11-25 12:27:27', NULL, 'andresl', NULL, 40, 'PC 632', NULL, NULL, NULL),
(589, NULL, '633', 'PF14W248', 11, 10, 62, 17, 16, '2021-01-06', 1, NULL, '2021-01-08 11:01:49', NULL, 'andresl', NULL, 62, 'PC 633', NULL, NULL, NULL),
(590, NULL, '634', '634', 11, 10, 52, 14, 16, '2021-01-22', 1, NULL, '2021-01-22 17:06:28', NULL, 'andresl', NULL, 52, 'PC 634', NULL, NULL, NULL),
(591, NULL, '637', 'PB-0FBNK', 11, 10, 66, 15, 15, '2021-01-26', 1, NULL, '2021-01-26 10:35:12', NULL, 'a.lopez', NULL, 66, 'PC 637', NULL, NULL, NULL),
(592, NULL, '638', '5CD6194JPT', 11, 24, 70, 6, 17, '2021-02-23', 1, NULL, '2021-04-06 17:32:23', NULL, 'a.lopez', NULL, 70, 'PC 638', NULL, NULL, NULL),
(593, NULL, '639', '639', 11, 29, 71, 6, 6, '2021-03-11', 1, NULL, '2021-04-06 17:27:22', NULL, 'a.lopez', NULL, 71, 'PC 639', NULL, NULL, NULL),
(594, NULL, '640', '640', 11, 10, 66, 17, 17, '2021-02-23', 1, NULL, '2021-02-23 10:46:12', NULL, 'a.lopez', NULL, 66, 'PC 640', NULL, NULL, NULL),
(595, NULL, '641', '641', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 641', NULL, NULL, NULL),
(596, NULL, '642', '642', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 642', NULL, NULL, NULL),
(597, NULL, '643', '643', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 643', NULL, NULL, NULL),
(598, NULL, '644', '644', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 644', NULL, NULL, NULL),
(599, NULL, '645', '645', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 645', NULL, NULL, NULL),
(600, NULL, '646', '646', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 646', NULL, NULL, NULL),
(601, NULL, '647', '647', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 647', NULL, NULL, NULL),
(602, NULL, '648', '648', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 648', NULL, NULL, NULL),
(603, NULL, '649', '649', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 649', NULL, NULL, NULL),
(604, NULL, '650', '650', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 650', NULL, NULL, NULL),
(605, NULL, '651', '651', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 651', NULL, NULL, NULL),
(606, NULL, '652', '652', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 652', NULL, NULL, NULL),
(607, NULL, '653', '653', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 653', NULL, NULL, NULL),
(608, NULL, '654', '654', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 654', NULL, NULL, NULL),
(609, NULL, '655', '655', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 655', NULL, NULL, NULL),
(610, NULL, '656', '656', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 656', NULL, NULL, NULL),
(611, NULL, '657', '657', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 657', NULL, NULL, NULL),
(612, NULL, '658', '658', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 658', NULL, NULL, NULL);
INSERT INTO `dispositivos` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(613, NULL, '659', '659', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 659', NULL, NULL, NULL),
(614, NULL, '660', '660', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 660', NULL, NULL, NULL),
(615, NULL, '661', 'MXL20222MJ', 11, 33, 56, 17, 16, '2021-01-01', 1, NULL, '2021-01-08 13:06:12', NULL, 'andresl', NULL, 56, 'PC 661', NULL, NULL, NULL),
(616, NULL, '662', 'MJ01DCEM', 11, 33, 56, 17, 16, '2021-01-01', 1, NULL, '2021-01-08 13:05:20', NULL, 'andresl', NULL, 56, 'PC 662', NULL, NULL, NULL),
(617, NULL, '663', 'MJ01DC30', 11, 33, 56, 17, 16, '2021-01-01', 1, NULL, '2021-01-08 13:11:20', NULL, 'andresl', NULL, 56, 'PC 663', NULL, NULL, NULL),
(618, NULL, '664', '664', 11, 26, 44, 17, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 664', NULL, NULL, NULL),
(619, NULL, '665', '665', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 665', NULL, NULL, NULL),
(620, NULL, '666', 'YJ00V45N', 11, 10, 11, 17, 17, '2021-01-01', 1, NULL, '2021-01-08 12:05:32', NULL, 'andresl', NULL, 11, 'PC 666', NULL, NULL, NULL),
(621, NULL, '667', '667', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 667', NULL, NULL, NULL),
(622, NULL, '668', 'MJ01JR2S', 11, 33, 56, 17, 17, '2021-01-01', 1, NULL, '2021-01-08 12:05:47', NULL, 'andresl', NULL, 56, 'PC 668', NULL, NULL, NULL),
(623, NULL, '669', '669', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 669', NULL, NULL, NULL),
(624, NULL, '670', '670', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 670', NULL, NULL, NULL),
(625, NULL, '671', '671', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 671', NULL, NULL, NULL),
(626, NULL, '672', '672', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 672', NULL, NULL, NULL),
(627, NULL, '673', '673', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 673', NULL, NULL, NULL),
(628, NULL, '674', '674', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 674', NULL, NULL, NULL),
(629, NULL, '675', '675', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 675', NULL, NULL, NULL),
(630, NULL, '676', 'C4K6L72', 11, 33, 60, 17, 17, '2020-12-15', 1, NULL, '2020-12-15 17:20:14', NULL, 'andresl', NULL, 60, 'PC 676', NULL, NULL, NULL),
(631, NULL, '677', 'CS02896995', 11, 10, 11, 13, 13, '2020-12-03', 1, NULL, '2020-12-03 15:59:26', NULL, 'andresl', NULL, 11, 'PC 677', NULL, NULL, NULL),
(632, NULL, '678', '678', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 678', NULL, NULL, NULL),
(633, NULL, '679', '0585BU6', 11, 10, 52, 7, 7, '2020-11-28', 1, NULL, '2020-11-28 12:31:31', NULL, 'felipem', NULL, 52, 'PC 679', NULL, NULL, NULL),
(634, NULL, '680', 'MP10ZRAP', 11, 10, 11, 6, 16, '2020-09-12', 2, NULL, '2021-03-11 13:27:54', NULL, 'a.lopez', NULL, 11, 'PC 680', NULL, NULL, NULL),
(635, NULL, '681', '681', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 681', NULL, NULL, NULL),
(636, NULL, '682', '682', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 682', NULL, NULL, NULL),
(637, NULL, '683', '683', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 683', NULL, NULL, NULL),
(638, NULL, '684', '684', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 684', NULL, NULL, NULL),
(639, NULL, '685', '685', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 685', NULL, NULL, NULL),
(640, NULL, '686', '686', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 686', NULL, NULL, NULL),
(641, NULL, '687', '687', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 687', NULL, NULL, NULL),
(642, NULL, '688', '688', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 688', NULL, NULL, NULL),
(643, NULL, '689', '689', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 689', NULL, NULL, NULL),
(644, NULL, '690', '690', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 690', NULL, NULL, NULL),
(645, NULL, '691', '691', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 691', NULL, NULL, NULL),
(646, NULL, '692', '692', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 692', NULL, NULL, NULL),
(647, NULL, '693', '693', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 693', NULL, NULL, NULL),
(648, NULL, '694', '694', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 694', NULL, NULL, NULL),
(649, NULL, '695', '695', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 695', NULL, NULL, NULL),
(650, NULL, '696', '696', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 696', NULL, NULL, NULL),
(651, NULL, '697', '697', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 697', NULL, NULL, NULL),
(652, NULL, '698', '698', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 698', NULL, NULL, NULL),
(653, NULL, '699', '699', 11, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'PC 699', NULL, NULL, NULL),
(655, NULL, '707', '707', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 707', NULL, NULL, NULL),
(656, NULL, '709', '709', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 709', NULL, NULL, NULL),
(657, NULL, '712', '712', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 712', NULL, NULL, NULL),
(658, NULL, '714', '714', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 714', NULL, NULL, NULL),
(659, NULL, '715', '715', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 715', NULL, NULL, NULL),
(660, NULL, '716', '716', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 716', NULL, NULL, NULL),
(661, NULL, '717', '717', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 717', NULL, NULL, NULL),
(662, NULL, '718', '718', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 718', NULL, NULL, NULL),
(663, NULL, '719', '719', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 719', NULL, NULL, NULL),
(664, NULL, '720', '720', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 720', NULL, NULL, NULL),
(665, NULL, '721', '721', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 721', NULL, NULL, NULL),
(666, NULL, '722', '722', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 722', NULL, NULL, NULL),
(667, NULL, '723', '723', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 723', NULL, NULL, NULL),
(668, NULL, '724', '724', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 724', NULL, NULL, NULL),
(669, NULL, '725', '725', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 725', NULL, NULL, NULL),
(670, NULL, '726', '726', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 726', NULL, NULL, NULL),
(671, NULL, '727', '727', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 727', NULL, NULL, NULL),
(672, NULL, '728', '728', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 728', NULL, NULL, NULL),
(673, NULL, '729', '729', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 729', NULL, NULL, NULL),
(674, NULL, '730', '730', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 730', NULL, NULL, NULL),
(675, NULL, '731', '731', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 731', NULL, NULL, NULL),
(676, NULL, '732', '732', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 732', NULL, NULL, NULL),
(677, NULL, '733', '733', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 733', NULL, NULL, NULL),
(678, NULL, '734', '734', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 734', NULL, NULL, NULL),
(679, NULL, '735', '20140118', 7, 12, 18, 13, 13, '2020-12-03', 1, NULL, '2021-03-01 13:57:40', NULL, 'a.lopez', NULL, 18, 'BT 735', NULL, NULL, NULL),
(680, NULL, '736', '736', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 736', NULL, NULL, NULL),
(681, NULL, '737', '737', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 737', NULL, NULL, NULL),
(682, NULL, '738', '738', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 738', NULL, NULL, NULL),
(683, NULL, '739', '739', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 739', NULL, NULL, NULL),
(684, NULL, '740', '740', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 740', NULL, NULL, NULL),
(685, NULL, '741', '741', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 741', NULL, NULL, NULL),
(686, NULL, '742', '742', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 742', NULL, NULL, NULL),
(687, NULL, '743', '743', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 743', NULL, NULL, NULL),
(688, NULL, '744', '744', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 744', NULL, NULL, NULL),
(689, NULL, '745', '745', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 745', NULL, NULL, NULL),
(690, NULL, '746', '746', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 746', NULL, NULL, NULL),
(691, NULL, '747', '747', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 747', NULL, NULL, NULL),
(692, NULL, '748', '748', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 748', NULL, NULL, NULL),
(693, NULL, '749', '749', 7, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'BT 749', NULL, NULL, NULL),
(695, NULL, '764', '764', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 764', NULL, NULL, NULL),
(696, NULL, '765', '765', 6, 3, 20, 3, 16, '2021-01-09', 4, NULL, '2021-03-02 15:37:32', NULL, 'a.lopez', NULL, 20, 'STR 765', NULL, NULL, NULL),
(697, NULL, '766', '766', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 766', NULL, NULL, NULL),
(698, NULL, '767', '767', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 767', NULL, NULL, NULL),
(699, NULL, '768', '768', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 768', NULL, NULL, NULL),
(700, NULL, '769', '769', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 769', NULL, NULL, NULL),
(701, NULL, '770', '770', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 770', NULL, NULL, NULL),
(702, NULL, '771', '771', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 771', NULL, NULL, NULL),
(703, NULL, '772', '772', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 772', NULL, NULL, NULL),
(704, NULL, '773', '773', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 773', NULL, NULL, NULL),
(705, NULL, '774', '774', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 774', NULL, NULL, NULL),
(706, NULL, '775', '775', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 775', NULL, NULL, NULL),
(707, NULL, '776', '776', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 776', NULL, NULL, NULL),
(708, NULL, '777', '777', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 777', NULL, NULL, NULL),
(709, NULL, '778', '778', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 778', NULL, NULL, NULL),
(710, NULL, '779', '779', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 779', NULL, NULL, NULL),
(711, NULL, '780', '780', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 780', NULL, NULL, NULL),
(712, NULL, '781', '781', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 781', NULL, NULL, NULL),
(713, NULL, '782', '782', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 782', NULL, NULL, NULL),
(714, NULL, '783', '783', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 783', NULL, NULL, NULL),
(715, NULL, '784', '784', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 784', NULL, NULL, NULL),
(716, NULL, '785', '785', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 785', NULL, NULL, NULL),
(717, NULL, '786', '786', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 786', NULL, NULL, NULL),
(718, NULL, '787', '787', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 787', NULL, NULL, NULL),
(719, NULL, '788', '788', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 788', NULL, NULL, NULL),
(720, NULL, '789', '789', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 789', NULL, NULL, NULL),
(721, NULL, '790', '790', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 790', NULL, NULL, NULL),
(722, NULL, '791', '791', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 791', NULL, NULL, NULL),
(723, NULL, '792', '792', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 792', NULL, NULL, NULL),
(724, NULL, '793', '793', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 793', NULL, NULL, NULL),
(725, NULL, '794', '794', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 794', NULL, NULL, NULL),
(726, NULL, '795', '795', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 795', NULL, NULL, NULL),
(727, NULL, '796', '796', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 796', NULL, NULL, NULL),
(728, NULL, '797', '797', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 797', NULL, NULL, NULL),
(729, NULL, '798', '798', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 798', NULL, NULL, NULL),
(730, NULL, '799', '799', 6, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'STR 799', NULL, NULL, NULL),
(732, NULL, '806', 'CN09A3S1NN', 17, 24, 63, 17, 16, '2021-01-01', 1, NULL, '2021-01-08 13:28:59', NULL, 'andresl', NULL, 63, 'SM 806', NULL, NULL, NULL),
(733, NULL, '807', 'CN09A3S23J', 17, 24, 63, 17, 17, '2021-01-01', 1, NULL, '2021-01-08 13:39:52', NULL, 'andresl', NULL, 63, 'SM 807', NULL, NULL, NULL),
(734, NULL, '808', '808', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 808', NULL, NULL, NULL),
(735, NULL, '809', '809', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 809', NULL, NULL, NULL),
(736, NULL, '810', '810', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 810', NULL, NULL, NULL),
(737, NULL, '811', '811', 17, 17, 41, 6, 6, '2020-12-04', 3, NULL, '2020-12-04 09:28:17', NULL, 'andresl', NULL, 41, 'SM 811', NULL, NULL, NULL),
(738, NULL, '812', '812', 17, 17, 41, 6, 6, '2020-12-04', 3, NULL, '2020-12-04 09:31:53', NULL, 'andresl', NULL, 41, 'SM 812', NULL, NULL, NULL),
(739, NULL, '813', '813', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 813', NULL, NULL, NULL),
(740, NULL, '814', '814', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 814', NULL, NULL, NULL),
(741, NULL, '819', 'SM 819', 17, 17, 54, 4, 4, '2020-11-28', 1, NULL, '2020-12-02 16:55:06', NULL, 'andresl', NULL, 54, 'SM 819', NULL, NULL, NULL),
(742, NULL, '820', '820', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 820', NULL, NULL, NULL),
(743, NULL, '821', '821', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 821', NULL, NULL, NULL),
(744, NULL, '822', '822', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 822', NULL, NULL, NULL),
(745, NULL, '823', '823', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 823', NULL, NULL, NULL),
(746, NULL, '824', '824', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 824', NULL, NULL, NULL),
(747, NULL, '825', '825', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 825', NULL, NULL, NULL),
(748, NULL, '826', '826', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 826', NULL, NULL, NULL),
(749, NULL, '827', '827', 17, 17, 41, 1, 1, '2020-11-30', 1, NULL, '2020-11-30 09:59:39', NULL, 'andresl', NULL, 41, 'SM 827', NULL, NULL, NULL),
(750, NULL, '828', '828', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 828', NULL, NULL, NULL),
(751, NULL, '829', '829', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 829', NULL, NULL, NULL),
(752, NULL, '830', '830', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 830', NULL, NULL, NULL),
(753, NULL, '831', '831', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 831', NULL, NULL, NULL),
(754, NULL, '832', '832', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 832', NULL, NULL, NULL),
(755, NULL, '833', '833', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 833', NULL, NULL, NULL),
(756, NULL, '834', '834', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 834', NULL, NULL, NULL),
(757, NULL, '835', '835', 17, 17, 41, 9, 9, '2020-12-01', 1, NULL, '2020-12-01 17:15:13', NULL, 'andresl', NULL, 41, 'SM 835', NULL, NULL, NULL),
(758, NULL, '836', '836', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 836', NULL, NULL, NULL),
(759, NULL, '837', '837', 17, 32, 64, 17, 16, '2021-01-01', 1, NULL, '2021-01-08 13:39:38', NULL, 'andresl', NULL, 64, 'SM 837', NULL, NULL, NULL),
(760, NULL, '838', '838', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 838', NULL, NULL, NULL),
(761, NULL, '839', '839', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 839', NULL, NULL, NULL),
(762, NULL, '840', '840', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 840', NULL, NULL, NULL),
(763, NULL, '841', '841', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 841', NULL, NULL, NULL),
(764, NULL, '842', '842', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 842', NULL, NULL, NULL),
(765, NULL, '843', '843', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 843', NULL, NULL, NULL),
(766, NULL, '844', '844', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 844', NULL, NULL, NULL),
(767, NULL, '845', '845', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 845', NULL, NULL, NULL),
(768, NULL, '846', '846', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 846', NULL, NULL, NULL),
(769, NULL, '847', '847', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 847', NULL, NULL, NULL),
(770, NULL, '848', '848', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 848', NULL, NULL, NULL),
(771, NULL, '849', '849', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 849', NULL, NULL, NULL),
(772, NULL, '850', '850', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 850', NULL, NULL, NULL),
(773, NULL, '851', '851', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 851', NULL, NULL, NULL),
(774, NULL, '852', '852', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 852', NULL, NULL, NULL),
(775, NULL, '853', '853', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 853', NULL, NULL, NULL),
(776, NULL, '854', '854', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 854', NULL, NULL, NULL),
(777, NULL, '855', '855', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 855', NULL, NULL, NULL),
(778, NULL, '856', '856', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 856', NULL, NULL, NULL),
(779, NULL, '857', '857', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 857', NULL, NULL, NULL),
(780, NULL, '858', '858', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 858', NULL, NULL, NULL),
(781, NULL, '859', '859', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 859', NULL, NULL, NULL),
(782, NULL, '860', '860', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 860', NULL, NULL, NULL),
(783, NULL, '861', '861', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 861', NULL, NULL, NULL),
(784, NULL, '862', '862', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 862', NULL, NULL, NULL),
(785, NULL, '863', '863', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 863', NULL, NULL, NULL),
(786, NULL, '864', '864', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 864', NULL, NULL, NULL),
(787, NULL, '865', '865', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 865', NULL, NULL, NULL),
(788, NULL, '866', '866', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 866', NULL, NULL, NULL),
(789, NULL, '867', '867', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 867', NULL, NULL, NULL),
(790, NULL, '868', '868', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 868', NULL, NULL, NULL),
(791, NULL, '869', '869', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 869', NULL, NULL, NULL),
(792, NULL, '870', '870', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 870', NULL, NULL, NULL),
(793, NULL, '871', '871', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 871', NULL, NULL, NULL),
(794, NULL, '872', '872', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 872', NULL, NULL, NULL),
(795, NULL, '873', '873', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 873', NULL, NULL, NULL),
(796, NULL, '874', '874', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 874', NULL, NULL, NULL),
(797, NULL, '875', '875', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 875', NULL, NULL, NULL),
(798, NULL, '876', '876', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 876', NULL, NULL, NULL),
(799, NULL, '877', '877', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 877', NULL, NULL, NULL),
(800, NULL, '878', '878', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 878', NULL, NULL, NULL),
(801, NULL, '879', '879', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 879', NULL, NULL, NULL),
(802, NULL, '880', '880', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 880', NULL, NULL, NULL),
(803, NULL, '881', '881', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 881', NULL, NULL, NULL),
(804, NULL, '882', '882', 17, 17, 46, 3, 16, '1920-08-11', 1, NULL, '2021-03-16 11:36:08', NULL, 'a.lopez', NULL, 46, 'SM 882', NULL, NULL, NULL),
(805, NULL, '883', '883', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 883', NULL, NULL, NULL),
(806, NULL, '884', '884', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 884', NULL, NULL, NULL),
(807, NULL, '885', '885', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 885', NULL, NULL, NULL),
(808, NULL, '886', '886', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 886', NULL, NULL, NULL),
(809, NULL, '887', '887', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 887', NULL, NULL, NULL),
(810, NULL, '888', '888', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 888', NULL, NULL, NULL),
(811, NULL, '889', '889', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 889', NULL, NULL, NULL),
(812, NULL, '890', '890', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 890', NULL, NULL, NULL),
(813, NULL, '891', '891', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 891', NULL, NULL, NULL),
(814, NULL, '892', '892', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 892', NULL, NULL, NULL),
(815, NULL, '893', '893', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 893', NULL, NULL, NULL),
(816, NULL, '894', '894', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 894', NULL, NULL, NULL),
(817, NULL, '895', '895', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 895', NULL, NULL, NULL),
(818, NULL, '896', '896', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 896', NULL, NULL, NULL),
(819, NULL, '897', '897', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 897', NULL, NULL, NULL),
(820, NULL, '898', '898', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 898', NULL, NULL, NULL),
(821, NULL, '899', '899', 17, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'SM 899', NULL, NULL, NULL),
(824, NULL, '919', '919', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 919', NULL, NULL, NULL),
(825, NULL, '920', '920', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 920', NULL, NULL, NULL),
(826, NULL, '921', '921', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 921', NULL, NULL, NULL),
(827, NULL, '922', '922', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 922', NULL, NULL, NULL),
(828, NULL, '923', '923', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 923', NULL, NULL, NULL),
(829, NULL, '924', '924', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 924', NULL, NULL, NULL),
(830, NULL, '925', '925', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 925', NULL, NULL, NULL),
(831, NULL, '926', '926', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 926', NULL, NULL, NULL),
(832, NULL, '927', '927', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 927', NULL, NULL, NULL),
(833, NULL, '928', '928', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 928', NULL, NULL, NULL),
(834, NULL, '929', '929', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 929', NULL, NULL, NULL),
(835, NULL, '930', '930', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 930', NULL, NULL, NULL),
(836, NULL, '931', '931', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 931', NULL, NULL, NULL),
(837, NULL, '932', '932', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 932', NULL, NULL, NULL),
(838, NULL, '933', '933', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 933', NULL, NULL, NULL),
(839, NULL, '934', '934', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 934', NULL, NULL, NULL),
(840, NULL, '935', '935', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 935', NULL, NULL, NULL),
(841, NULL, '936', '936', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 936', NULL, NULL, NULL),
(842, NULL, '937', '937', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 937', NULL, NULL, NULL),
(843, NULL, '938', '938', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 938', NULL, NULL, NULL),
(844, NULL, '939', '939', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 939', NULL, NULL, NULL),
(845, NULL, '940', '940', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 940', NULL, NULL, NULL),
(846, NULL, '941', '941', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 941', NULL, NULL, NULL),
(847, NULL, '942', '942', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 942', NULL, NULL, NULL),
(848, NULL, '943', '943', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 943', NULL, NULL, NULL),
(849, NULL, '944', '944', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 944', NULL, NULL, NULL),
(850, NULL, '945', '945', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 945', NULL, NULL, NULL),
(851, NULL, '946', '946', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 946', NULL, NULL, NULL),
(852, NULL, '947', '947', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 947', NULL, NULL, NULL),
(853, NULL, '948', '948', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 948', NULL, NULL, NULL),
(854, NULL, '949', '949', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 949', NULL, NULL, NULL),
(855, NULL, '950', '950', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 950', NULL, NULL, NULL),
(856, NULL, '951', '951', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 951', NULL, NULL, NULL),
(857, NULL, '952', '952', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 952', NULL, NULL, NULL),
(858, NULL, '953', '953', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 953', NULL, NULL, NULL),
(859, NULL, '954', '954', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 954', NULL, NULL, NULL),
(860, NULL, '955', '955', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 955', NULL, NULL, NULL),
(861, NULL, '956', '956', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 956', NULL, NULL, NULL),
(862, NULL, '957', '957', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 957', NULL, NULL, NULL),
(863, NULL, '958', '958', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 958', NULL, NULL, NULL),
(864, NULL, '959', '959', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 959', NULL, NULL, NULL),
(865, NULL, '960', '960', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 960', NULL, NULL, NULL),
(866, NULL, '961', '961', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 961', NULL, NULL, NULL),
(867, NULL, '962', '962', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 962', NULL, NULL, NULL),
(868, NULL, '963', '963', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 963', NULL, NULL, NULL),
(869, NULL, '964', '964', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 964', NULL, NULL, NULL),
(870, NULL, '965', '965', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 965', NULL, NULL, NULL),
(871, NULL, '966', '966', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 966', NULL, NULL, NULL),
(872, NULL, '967', '967', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 967', NULL, NULL, NULL),
(873, NULL, '968', '968', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 968', NULL, NULL, NULL),
(874, NULL, '969', '969', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 969', NULL, NULL, NULL),
(875, NULL, '970', '970', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 970', NULL, NULL, NULL),
(876, NULL, '971', '971', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 971', NULL, NULL, NULL),
(877, NULL, '972', '972', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 972', NULL, NULL, NULL),
(878, NULL, '973', '973', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 973', NULL, NULL, NULL),
(879, NULL, '974', '974', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 974', NULL, NULL, NULL),
(880, NULL, '975', '975', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 975', NULL, NULL, NULL),
(881, NULL, '976', '976', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 976', NULL, NULL, NULL),
(882, NULL, '977', '977', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 977', NULL, NULL, NULL),
(883, NULL, '978', '978', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 978', NULL, NULL, NULL),
(884, NULL, '979', '979', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 979', NULL, NULL, NULL),
(885, NULL, '980', '980', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 980', NULL, NULL, NULL),
(886, NULL, '981', '981', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 981', NULL, NULL, NULL),
(887, NULL, '982', '982', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 982', NULL, NULL, NULL),
(888, NULL, '983', '983', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 983', NULL, NULL, NULL),
(889, NULL, '984', '984', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 984', NULL, NULL, NULL),
(890, NULL, '985', '985', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 985', NULL, NULL, NULL),
(891, NULL, '986', '986', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 986', NULL, NULL, NULL),
(892, NULL, '987', '987', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 987', NULL, NULL, NULL),
(893, NULL, '988', '988', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 988', NULL, NULL, NULL),
(894, NULL, '989', '989', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 989', NULL, NULL, NULL),
(895, NULL, '990', '990', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 990', NULL, NULL, NULL),
(896, NULL, '991', '991', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 991', NULL, NULL, NULL),
(897, NULL, '992', '992', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 992', NULL, NULL, NULL),
(898, NULL, '993', '993', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 993', NULL, NULL, NULL),
(899, NULL, '994', '994', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 994', NULL, NULL, NULL),
(900, NULL, '995', '995', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 995', NULL, NULL, NULL),
(901, NULL, '996', '996', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 996', NULL, NULL, NULL),
(902, NULL, '997', '997', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 997', NULL, NULL, NULL),
(903, NULL, '998', '998', 16, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'USB 998', NULL, NULL, NULL),
(905, NULL, '1000', 'E2008070733', 31, 35, 61, 17, 16, '2021-01-08', 1, NULL, '2021-01-08 10:11:00', NULL, 'andresl', NULL, 61, 'UPS 1000', NULL, NULL, NULL),
(906, NULL, '1003', '1003', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1003', NULL, NULL, NULL),
(907, NULL, '1004', '1004', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1004', NULL, NULL, NULL),
(908, NULL, '1005', '1005', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1005', NULL, NULL, NULL),
(909, NULL, '1006', '1006', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1006', NULL, NULL, NULL),
(910, NULL, '1007', '1007', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1007', NULL, NULL, NULL),
(911, NULL, '1008', '1008', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1008', NULL, NULL, NULL),
(912, NULL, '1009', '1009', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1009', NULL, NULL, NULL),
(913, NULL, '1010', '1010', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1010', NULL, NULL, NULL),
(914, NULL, '1011', '1011', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1011', NULL, NULL, NULL),
(915, NULL, '1012', '1012', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1012', NULL, NULL, NULL),
(916, NULL, '1013', '1013', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1013', NULL, NULL, NULL),
(917, NULL, '1014', '1014', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1014', NULL, NULL, NULL),
(918, NULL, '1015', '1015', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1015', NULL, NULL, NULL),
(919, NULL, '1016', '1016', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1016', NULL, NULL, NULL),
(920, NULL, '1017', '1017', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1017', NULL, NULL, NULL),
(921, NULL, '1018', '1018', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1018', NULL, NULL, NULL),
(922, NULL, '1019', '1019', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1019', NULL, NULL, NULL),
(923, NULL, '1020', '0197N', 8, 37, 67, 4, 16, '2021-01-27', 1, NULL, '2021-01-27 13:06:56', NULL, 'felipem', NULL, 67, 'SM 1020', NULL, NULL, NULL),
(924, NULL, '1021', '0197A', 8, 37, 67, 4, 16, '2021-01-27', 1, NULL, '2021-01-27 13:09:04', NULL, 'felipem', NULL, 67, 'SM 1021', NULL, NULL, NULL),
(925, NULL, '1022', '0197', 8, 37, 67, 16, 16, '2021-01-27', 1, NULL, '2021-01-27 15:33:54', NULL, 'felipem', NULL, 67, 'SM 1022', NULL, NULL, NULL),
(926, NULL, '1023', '1023', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1023', NULL, NULL, NULL),
(927, NULL, '1024', '1024', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1024', NULL, NULL, NULL),
(928, NULL, '1025', '1025', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1025', NULL, NULL, NULL),
(929, NULL, '1026', '1026', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1026', NULL, NULL, NULL),
(930, NULL, '1027', '1027', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1027', NULL, NULL, NULL),
(931, NULL, '1028', '1028', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1028', NULL, NULL, NULL),
(932, NULL, '1029', '1029', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1029', NULL, NULL, NULL),
(933, NULL, '1030', '19T30M28A01616', 33, 39, 68, 4, 4, '2021-02-23', 1, NULL, '2021-03-01 11:48:24', NULL, 'a.lopez', NULL, 68, 'WIFI 1030', NULL, NULL, NULL),
(934, NULL, '1031', '19T30M28A01598', 33, 39, 68, 7, 7, '2021-02-23', 1, NULL, '2021-02-23 10:27:50', NULL, 'felipem', NULL, 68, 'WIFI 1031', NULL, NULL, NULL),
(935, NULL, '1032', '19T30M28A01061', 33, 39, 68, 8, 16, '2021-02-23', 1, NULL, '2021-02-23 10:28:34', NULL, 'felipem', NULL, 68, 'WIFI 1032', NULL, NULL, NULL),
(936, NULL, '1033', '1033', 21, 27, 50, 2, 2, '2020-11-24', 1, NULL, '2020-11-24 11:43:04', NULL, 'andresl', NULL, 50, 'CEL 1033', NULL, NULL, NULL),
(937, NULL, '1034', '19T30M28A01159', 33, 39, 68, 17, 17, '2021-02-23', 1, NULL, '2021-02-23 10:49:21', NULL, 'a.lopez', NULL, 68, 'WIFI 1034', NULL, NULL, NULL),
(938, NULL, '1035', '1035', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1035', NULL, NULL, NULL),
(939, NULL, '1036', '1036', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1036', NULL, NULL, NULL),
(940, NULL, '1037', '1037', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1037', NULL, NULL, NULL),
(941, NULL, '1038', '1038', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1038', NULL, NULL, NULL),
(942, NULL, '1039', '1039', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1039', NULL, NULL, NULL),
(943, NULL, '1040', 'GM171580', 32, 36, 65, 17, 16, '2021-01-25', 2, NULL, '2021-01-25 18:07:03', NULL, 'felipem', NULL, 65, 'THG 1040', NULL, NULL, NULL),
(944, NULL, '1041', '1041', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1041', NULL, NULL, NULL),
(945, NULL, '1042', '1042', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1042', NULL, NULL, NULL),
(946, NULL, '1043', '1043', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1043', NULL, NULL, NULL),
(947, NULL, '1044', '1044', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1044', NULL, NULL, NULL),
(948, NULL, '1045', '1045', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1045', NULL, NULL, NULL),
(949, NULL, '1046', '1046', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1046', NULL, NULL, NULL),
(950, NULL, '1047', '1047', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1047', NULL, NULL, NULL),
(951, NULL, '1048', '1048', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1048', NULL, NULL, NULL),
(952, NULL, '1049', '1049', 21, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'CEL 1049', NULL, NULL, NULL),
(954, NULL, '1051', 'E2008070734', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:49:56', NULL, 'felipem', NULL, 61, 'UPS 1051', NULL, NULL, NULL),
(955, NULL, '1052', 'E2008070735', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:49:51', NULL, 'felipem', NULL, 61, 'UPS 1052', NULL, NULL, NULL),
(956, NULL, '1053', 'E2008070736', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:49:59', NULL, 'felipem', NULL, 61, 'UPS 1053', NULL, NULL, NULL),
(957, NULL, '1054', 'E2008070781', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:21:54', NULL, 'felipem', NULL, 61, 'UPS 1054', NULL, NULL, NULL),
(958, NULL, '1055', 'E2008070782', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:21:47', NULL, 'felipem', NULL, 61, 'UPS 1055', NULL, NULL, NULL),
(959, NULL, '1056', 'E2008070783', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:21:40', NULL, 'felipem', NULL, 61, 'UPS 1056', NULL, NULL, NULL),
(960, NULL, '1057', 'E2008070784', 31, 35, 61, 17, 17, '2021-01-08', 1, NULL, '2021-02-23 09:21:35', NULL, 'felipem', NULL, 61, 'UPS 1057', NULL, NULL, NULL),
(961, NULL, '1058', '1058', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1058', NULL, NULL, NULL),
(962, NULL, '1059', '1059', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1059', NULL, NULL, NULL),
(963, NULL, '1060', '1060', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1060', NULL, NULL, NULL),
(964, NULL, '1061', '1061', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1061', NULL, NULL, NULL),
(965, NULL, '1062', '1062', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1062', NULL, NULL, NULL),
(966, NULL, '1063', '1063', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1063', NULL, NULL, NULL),
(967, NULL, '1064', '1064', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1064', NULL, NULL, NULL),
(968, NULL, '1065', '1065', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1065', NULL, NULL, NULL),
(969, NULL, '1066', '1066', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1066', NULL, NULL, NULL),
(970, NULL, '1067', '1067', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1067', NULL, NULL, NULL),
(971, NULL, '1068', '1068', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1068', NULL, NULL, NULL),
(972, NULL, '1069', '1069', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1069', NULL, NULL, NULL),
(973, NULL, '1070', '1070', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1070', NULL, NULL, NULL),
(974, NULL, '1071', '1071', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1071', NULL, NULL, NULL),
(975, NULL, '1072', '1072', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1072', NULL, NULL, NULL),
(976, NULL, '1073', '1073', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1073', NULL, NULL, NULL),
(977, NULL, '1074', '1074', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1074', NULL, NULL, NULL),
(978, NULL, '1075', '1075', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1075', NULL, NULL, NULL),
(979, NULL, '1076', '1076', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1076', NULL, NULL, NULL),
(980, NULL, '1077', '1077', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1077', NULL, NULL, NULL),
(981, NULL, '1078', '1078', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1078', NULL, NULL, NULL),
(982, NULL, '1079', '1079', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1079', NULL, NULL, NULL),
(983, NULL, '1080', '1080', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1080', NULL, NULL, NULL),
(984, NULL, '1081', '1081', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1081', NULL, NULL, NULL),
(985, NULL, '1082', '1082', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1082', NULL, NULL, NULL),
(986, NULL, '1083', '1083', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1083', NULL, NULL, NULL),
(987, NULL, '1084', '1084', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1084', NULL, NULL, NULL),
(988, NULL, '1085', '1085', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1085', NULL, NULL, NULL),
(989, NULL, '1086', '1086', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1086', NULL, NULL, NULL),
(990, NULL, '1087', '1087', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1087', NULL, NULL, NULL),
(991, NULL, '1088', '1088', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1088', NULL, NULL, NULL),
(992, NULL, '1089', '1089', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1089', NULL, NULL, NULL),
(993, NULL, '1090', '1090', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1090', NULL, NULL, NULL),
(994, NULL, '1091', '1091', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1091', NULL, NULL, NULL),
(995, NULL, '1092', '1092', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1092', NULL, NULL, NULL),
(996, NULL, '1093', '1093', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1093', NULL, NULL, NULL),
(997, NULL, '1094', '1094', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1094', NULL, NULL, NULL);
INSERT INTO `dispositivos` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(998, NULL, '1095', '1095', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1095', NULL, NULL, NULL),
(999, NULL, '1096', '1096', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1096', NULL, NULL, NULL),
(1000, NULL, '1097', '1097', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1097', NULL, NULL, NULL),
(1001, NULL, '1098', '1098', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1098', NULL, NULL, NULL),
(1002, NULL, '1099', '1099', 23, 26, 44, 16, NULL, '0000-00-00', 4, NULL, NULL, NULL, NULL, NULL, NULL, 'LIBRE 1099', NULL, NULL, NULL),
(1004, NULL, '1001', '1001', 34, 40, 69, 7, 7, '2020-11-20', 4, NULL, '2021-02-23 10:19:44', 'felipem', 'felipem', NULL, 69, 'SM 1001', '2020-11-20 00:38:26', NULL, NULL),
(1005, NULL, '802', '802', 17, 17, 41, 1, 1, '2020-11-20', 1, NULL, '2020-11-30 10:47:48', 'felipem', 'andresl', NULL, 41, 'SM 802', '2020-11-20 00:39:20', NULL, NULL),
(1006, NULL, '803', 'X7GP082765', 17, 17, 47, 13, 13, '2020-11-20', 1, NULL, '2020-12-03 16:09:47', 'felipem', 'andresl', NULL, 47, 'SM 803', '2020-11-20 00:39:57', NULL, NULL),
(1007, NULL, '804', '804', 17, 17, 41, 2, 2, '2020-11-20', 1, NULL, '2020-11-24 10:58:52', 'felipem', 'andresl', NULL, 41, 'SM 804', '2020-11-20 00:40:27', NULL, 550000.00),
(1008, NULL, '805', '805', 19, 21, 32, 1, 1, '2020-11-20', 1, NULL, '2021-04-14 09:09:26', 'felipem', 'a.lopez', NULL, 32, 'SM 805', '2020-11-20 00:40:56', NULL, NULL),
(1009, NULL, '200', '200', 1, 26, 44, 16, 16, '2020-11-20', 4, NULL, '2021-02-15 16:12:23', 'felipem', 'felipem', NULL, 44, 'HOLTER 200', '2020-11-20 00:48:05', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20191127095023`
--

CREATE TABLE `dispositivos_backup_20191127095023` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20191127095023`
--

INSERT INTO `dispositivos_backup_20191127095023` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado`, `editado`, `creado_por`, `editado_por`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', 'HOLTER101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', NULL, 'felipem', NULL),
(2, '8710a41018a7c4e42.jpg', 'HOLTER102', '3A-11605', 1, 1, 1, 7, 7, '2019-11-27', 1, '27/11/2019 09:40:39 am', NULL, 'felipem', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20191127101832`
--

CREATE TABLE `dispositivos_backup_20191127101832` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20191127101832`
--

INSERT INTO `dispositivos_backup_20191127101832` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado`, `editado`, `creado_por`, `editado_por`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', 'HOLTER101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', NULL, 'felipem', NULL),
(2, '8710a41018a7c4e42.jpg', 'HOLTER102', '3A-11605', 1, 1, 1, 7, 7, '2019-11-27', 1, '27/11/2019 09:40:39 am', NULL, 'felipem', NULL),
(3, '6630f469154d3320d.jpg', 'HOLTER 103', '3A-8044', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 09:51:07 am', NULL, 'felipem'),
(4, NULL, 'HOLTER 104', '3A-11606', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(5, NULL, 'HOLTER 105', '3A-1039', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(6, NULL, 'HOLTER 106', '3A-11607', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(7, NULL, 'HOLTER 107', '3A-11608', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(8, NULL, 'HOLTER 108', '3A-2151', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(9, NULL, 'HOLTER 109', '3A-9421', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(10, NULL, 'HOLTER 110', '3A-9310', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(11, NULL, 'HOLTER 111', '3A-11609', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(12, NULL, 'HOLTER 112', 'R1', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(13, NULL, 'HOLTER 113', '3A-9311', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(14, NULL, 'HOLTER 114', '3A-11815', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(15, NULL, 'HOLTER 115', '3A-9573', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(16, NULL, 'HOLTER 116', '3A-8940', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(17, NULL, 'HOLTER 117', '3A-9484', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(18, NULL, 'HOLTER 118', '3A-9486', 1, 1, 1, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(19, NULL, 'HOLTER 119', '3A-9572', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(20, NULL, 'HOLTER 120', '3A-9487', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(21, NULL, 'HOLTER 121', '3A-8946', 1, 1, 1, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(22, NULL, 'HOLTER 122', '3A-9485', 1, 1, 1, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(23, NULL, 'HOLTER 123', '3A-1993', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(24, NULL, 'HOLTER 124', '3A-10318', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(25, NULL, 'HOLTER 125', '3A-11816', 1, 1, 1, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(26, NULL, 'HOLTER 126', '3A-11818', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(27, NULL, 'HOLTER 127', '3A-12175', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(28, NULL, 'HOLTER 128', '3A-12176', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(29, NULL, 'HOLTER 129', '3A-11940', 1, 1, 1, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(30, NULL, 'HOLTER 130', '3A-12178', 1, 1, 1, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20191203080334`
--

CREATE TABLE `dispositivos_backup_20191203080334` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20191203080334`
--

INSERT INTO `dispositivos_backup_20191203080334` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado`, `editado`, `creado_por`, `editado_por`, `descripcion`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', 'HOLTER 101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '27/11/2019 11:06:28 am', 'felipem', 'felipem', NULL),
(2, '8710a41018a7c4e42.jpg', 'HOLTER 102', '3A-11605', 1, 1, 1, 7, 7, '2019-11-27', 1, '27/11/2019 09:40:39 am', '27/11/2019 11:06:42 am', 'felipem', 'felipem', NULL),
(3, '6630f469154d3320d.jpg', 'HOLTER 103', '3A-8044', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 09:51:07 am', NULL, 'felipem', NULL),
(4, '796ef58bbcaf815d5.jpg', 'HOLTER 104', '3A-11606', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:20:47 am', NULL, 'felipem', NULL),
(5, 'a1ca49f2da0d3858a.jpg', 'HOLTER 105', '3A-1039', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:21:01 am', NULL, 'felipem', NULL),
(6, 'fa31345b2dec3f75d.jpg', 'HOLTER 106', '3A-11607', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:06 am', NULL, 'felipem', NULL),
(7, '62f8f3afd6062cf73.jpg', 'HOLTER 107', '3A-11608', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:41 am', NULL, 'felipem', NULL),
(8, '16a714e4f58af1930.jpg', 'HOLTER 108', '3A-2151', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:46 am', NULL, 'felipem', NULL),
(9, 'fa647e5ab77632692.jpg', 'HOLTER 109', '3A-9421', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:50 am', NULL, 'felipem', NULL),
(10, '476bc82bf3bed4616.jpg', 'HOLTER 110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '27/11/2019 10:46:21 am', NULL, 'felipem', NULL),
(11, 'a456dd57854fcd34f.jpg', 'HOLTER 111', '3A-11609', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:00 am', NULL, 'felipem', NULL),
(12, 'e79c8397c6fa85698.jpg', 'HOLTER 112', '3A-12178', 1, 1, 1, 6, 6, NULL, 1, NULL, '29/11/2019 01:14:34 am', NULL, 'felipem', NULL),
(13, '79766d196e19eb1db.jpg', 'HOLTER 113', '3A-9311', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:11 am', NULL, 'felipem', NULL),
(14, '1c1558589faf656c3.jpg', 'HOLTER 114', '3A-11815', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:27:18 am', NULL, 'felipem', NULL),
(15, 'a47482f69e6bb1445.jpg', 'HOLTER 115', '3A-9573', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:56 am', NULL, 'felipem', NULL),
(16, 'fad32fe2f96f1b6f2.jpg', 'HOLTER 116', '3A-8940', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:01 am', NULL, 'felipem', NULL),
(17, 'f6216dd5ac51a2f82.jpg', 'HOLTER 117', '3A-9484', 1, 1, 1, 10, 10, '2019-12-04', 1, NULL, '3/12/2019 12:43:08 am', NULL, 'felipem', 'Esruche<div>Cable viejo</div>'),
(18, '6b9ff54089c40cbf3.jpg', 'HOLTER 118', '3A-9486', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:12 am', NULL, 'felipem', NULL),
(19, 'ad6939e30d7026eee.jpg', 'HOLTER 119', '3A-9572', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:21 am', NULL, 'felipem', NULL),
(20, '636978098692e0616.jpg', 'HOLTER 120', '3A-9487', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:23 am', NULL, 'felipem', NULL),
(21, '05353fef66bf34245.jpg', 'HOLTER 121', '3A-8946', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:27 am', NULL, 'felipem', NULL),
(22, '753003a921f5f02ec.jpg', 'HOLTER 122', '3A-9485', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:32 am', NULL, 'felipem', NULL),
(23, '5cc88d4b86424bb7d.jpg', 'HOLTER 123', '3A-1993', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:37 am', NULL, 'felipem', NULL),
(24, 'b89cb8d899578dd8b.jpg', 'HOLTER 124', '3A-10318', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:41 am', NULL, 'felipem', NULL),
(25, '2b4b8d4d10dddbe8b.jpg', 'HOLTER 125', '3A-11816', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:47 am', NULL, 'felipem', NULL),
(26, '06b68e149d1227503.jpg', 'HOLTER 126', '3A-11818', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:52 am', NULL, 'felipem', NULL),
(27, 'f4964486301b6afe2.jpg', 'HOLTER 127', '3A-12175', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:58 am', NULL, 'felipem', NULL),
(28, 'd179e9e2972b91703.jpg', 'HOLTER 128', '3A-12176', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:29:03 am', NULL, 'felipem', NULL),
(29, '9e12d7e5ca84cd0f4.jpg', 'HOLTER 129', '3A-11940', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:29:09 am', NULL, 'felipem', NULL),
(57, NULL, 'HOLTER130', '3A-11295', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 04:45:35 pm', NULL, 'felipem', NULL, NULL),
(31, '3cc42f4af7a84575e.jpg', 'ECO 201', 'US97804887', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:28:42 am', NULL, 'felipem', NULL),
(32, '536dec1c5345e2b59.jpg', 'ECO 202', 'US97806979', 3, 3, 3, 6, 6, NULL, 1, NULL, '29/11/2019 01:11:49 am', NULL, 'felipem', NULL),
(33, '2edf9e5e9f0adf0b0.jpg', 'ECO 203', '3728A01811', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:12:32 am', NULL, 'felipem', NULL),
(34, 'b35b0af60ed58ef47.jpg', 'ECO 204', 'US50210876', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:13:01 am', NULL, 'felipem', NULL),
(35, 'd6bb8df0f60c48ab0.jpg', 'ECO 205', '3728A00382', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:19:52 am', NULL, 'felipem', NULL),
(36, '620450dbf61c4c19f.jpg', 'ECO 206', '9708A09160', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:20:17 am', NULL, 'felipem', NULL),
(37, '41736a0baf4cb45e3.jpg', 'ECO 207', '71674', 3, 6, 5, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:01 am', NULL, 'felipem', NULL),
(38, '0eb9fdc48d520b1a8.jpg', 'ECO 208', '001838VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:29 am', NULL, 'felipem', NULL),
(39, 'f7c6ea863e2c80a47.jpg', 'ECO 209', '055133VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:16 am', NULL, 'felipem', NULL),
(40, 'c86621e1a393abd5e.jpg', 'ECO 210', '3728A01073', 3, 3, 3, 4, 4, NULL, 1, NULL, '29/11/2019 01:20:39 am', NULL, 'felipem', NULL),
(41, '158d01f499364579d.jpg', 'ECO 211', 'USO0211434', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:21:06 am', NULL, 'felipem', NULL),
(42, '387a137ebf96710b6.jpg', 'ECO 212', 'US97807671', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:21:46 am', NULL, 'felipem', NULL),
(43, '5ed931c87e7ad07f3.jpg', 'ECO 213', '9708A04535', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:34 am', NULL, 'felipem', NULL),
(44, 'e624e9cfa97691728.jpg', 'ECO 214', '3728A00160', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:26:23 am', NULL, 'felipem', NULL),
(45, '54d451022df8c30e6.jpg', 'ECO 215', 'US97703545', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:07 am', NULL, 'felipem', NULL),
(46, NULL, 'ECO 216', 'US97808948', 3, 3, 3, 6, 6, NULL, 1, NULL, '29/11/2019 01:27:21 am', NULL, 'felipem', NULL),
(47, '630053d26916342ca.jpg', 'ECO 217', 'US97800318', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:57 am', NULL, 'felipem', NULL),
(48, '9972d0e70d8ca4ca6.jpg', 'ECO 218', '3728A02302', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:47 am', NULL, 'felipem', NULL),
(49, '2e9dc0bf01f8da61f.jpg', 'ECO 219', 'US80210739', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:31 am', NULL, 'felipem', NULL),
(50, '6e5eaa081a347bc98.jpg', 'ECO 220', 'US97703205', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:18 am', NULL, 'felipem', NULL),
(51, '10b8abd62f6418d47.jpg', 'ECO 221', 'US97804106', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:05 am', NULL, 'felipem', NULL),
(52, 'b6efed45a482c8c76.jpg', 'ECO 222', 'US97804535', 3, 3, 3, 7, 7, NULL, 1, NULL, '28/11/2019 10:37:14 am', NULL, 'lgarcia', NULL),
(53, '326d48038544d0081.jpg', 'ECO 223', 'US70421029', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:53 am', NULL, 'felipem', NULL),
(54, '2114d2a34043479d1.jpg', 'ECO 224', 'US97703258', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:26:47 am', NULL, 'felipem', NULL),
(55, '99c80bd69f2b7d401.jpg', 'ECO 225', 'USD0211891', 3, 3, 3, 4, 4, NULL, 1, NULL, '29/11/2019 01:24:41 am', NULL, 'felipem', NULL),
(56, 'a4df3e5a8cd507c06.jpg', 'PC621', 'JCN0GR05Z985517', 12, 7, 6, 1, 1, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '27/11/2019 06:41:53 pm', 'vilmao', 'vilmao', NULL),
(58, NULL, 'HOLTER 131', '3A-11287', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 05:06:08 pm', NULL, 'felipem', NULL, NULL),
(59, '982c34a6a71597fe5.jpg', 'HOLTER 132', '3A-11293', 1, 1, 1, 7, 7, '2019-11-29', 1, '3/12/2019 12:38:32 am', '3/12/2019 12:38:53 am', 'felipem', 'felipem', 'Con estuche y cable de paciente&nbsp;'),
(60, '5c9c7e2c36eb50b7f.jpg', 'HOLTER 133', '3A-11285', 1, 1, 1, 7, 7, '2019-11-29', 1, '3/12/2019 12:41:03 am', NULL, 'felipem', NULL, 'Estuche&nbsp;<div>Cable paciente&nbsp;</div>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20191205094300`
--

CREATE TABLE `dispositivos_backup_20191205094300` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20191205094300`
--

INSERT INTO `dispositivos_backup_20191205094300` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado`, `editado`, `creado_por`, `editado_por`, `descripcion`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', 'HOLTER 101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '27/11/2019 11:06:28 am', 'felipem', 'felipem', NULL),
(2, '8710a41018a7c4e42.jpg', 'HOLTER 102', '3A-11605', 1, 1, 1, 1, 1, '2019-11-27', 1, '27/11/2019 09:40:39 am', '3/12/2019 08:18:57 am', 'felipem', 'felipem', '<br>'),
(3, '6630f469154d3320d.jpg', 'HOLTER 103', '3333', 1, 1, 1, 7, 7, '2019-12-03', 3, NULL, '3/12/2019 08:14:22 am', NULL, 'felipem', '<br>'),
(4, '796ef58bbcaf815d5.jpg', 'HOLTER 104', '3A-11606', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:20:47 am', NULL, 'felipem', NULL),
(5, 'a1ca49f2da0d3858a.jpg', 'HOLTER 105', '3A-1039', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:21:01 am', NULL, 'felipem', NULL),
(6, 'fa31345b2dec3f75d.jpg', 'HOLTER 106', '3A-11607', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:06 am', NULL, 'felipem', NULL),
(7, '62f8f3afd6062cf73.jpg', 'HOLTER 107', '3A-11608', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:41 am', NULL, 'felipem', NULL),
(8, '16a714e4f58af1930.jpg', 'HOLTER 108', '3A-2151', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:46 am', NULL, 'felipem', NULL),
(9, 'fa647e5ab77632692.jpg', 'HOLTER 109', '3A-9421', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:14:08 am', NULL, 'felipem', '<br>'),
(10, '476bc82bf3bed4616.jpg', 'HOLTER 110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '27/11/2019 10:46:21 am', NULL, 'felipem', NULL),
(11, 'a456dd57854fcd34f.jpg', 'HOLTER 111', '3A-11609', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:00 am', NULL, 'felipem', NULL),
(12, 'e79c8397c6fa85698.jpg', 'HOLTER 112', '3A-12178', 1, 1, 1, 6, 6, NULL, 1, NULL, '29/11/2019 01:14:34 am', NULL, 'felipem', NULL),
(13, '79766d196e19eb1db.jpg', 'HOLTER 113', '3A-9311', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:11 am', NULL, 'felipem', NULL),
(14, '1c1558589faf656c3.jpg', 'HOLTER 114', '3A-11815', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:27:18 am', NULL, 'felipem', NULL),
(15, 'a47482f69e6bb1445.jpg', 'HOLTER 115', '3A-9573', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:56 am', NULL, 'felipem', NULL),
(16, 'fad32fe2f96f1b6f2.jpg', 'HOLTER 116', '3A-8940', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:01 am', NULL, 'felipem', NULL),
(17, 'f6216dd5ac51a2f82.jpg', 'HOLTER 117', '3A-9484', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 01:38:12 pm', NULL, 'felipem', 'Esruche<div>Cable viejo</div>'),
(18, '6b9ff54089c40cbf3.jpg', 'HOLTER 118', '3A-9486', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:12 am', NULL, 'felipem', NULL),
(19, 'ad6939e30d7026eee.jpg', 'HOLTER 119', '3A-9572', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:21 am', NULL, 'felipem', NULL),
(20, '636978098692e0616.jpg', 'HOLTER 120', '3A-9487', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:23 am', NULL, 'felipem', NULL),
(21, '05353fef66bf34245.jpg', 'HOLTER 121', '3A-8946', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:27 am', NULL, 'felipem', NULL),
(22, '753003a921f5f02ec.jpg', 'HOLTER 122', '3A-9485', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:32 am', NULL, 'felipem', NULL),
(23, '5cc88d4b86424bb7d.jpg', 'HOLTER 123', '3A-1993', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:03:57 am', NULL, 'felipem', 'Antes holter 112'),
(24, 'b89cb8d899578dd8b.jpg', 'HOLTER 124', '3A-10318', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:41 am', NULL, 'felipem', NULL),
(25, '2b4b8d4d10dddbe8b.jpg', 'HOLTER 125', '3A-11816', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:47 am', NULL, 'felipem', NULL),
(26, '06b68e149d1227503.jpg', 'HOLTER 126', '3A-8044', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 10:42:01 am', NULL, 'felipem', 'Antes 3A-11818'),
(27, 'f4964486301b6afe2.jpg', 'HOLTER 127', '3A-12175', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:28:58 am', NULL, 'felipem', NULL),
(28, 'd179e9e2972b91703.jpg', 'HOLTER 128', '3A-12176', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:29:03 am', NULL, 'felipem', NULL),
(29, '9e12d7e5ca84cd0f4.jpg', 'HOLTER 129', '3A-11940', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:29:09 am', NULL, 'felipem', NULL),
(57, 'c11ec5b51107d1e95.jpg', 'HOLTER130', '3A-11295', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 04:45:35 pm', '5/12/2019 09:39:43 am', 'felipem', 'felipem', '<br>'),
(31, '3cc42f4af7a84575e.jpg', 'ECO 201', 'US97804887', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:28:42 am', NULL, 'felipem', NULL),
(32, '536dec1c5345e2b59.jpg', 'ECO 202', 'US97806979', 3, 3, 3, 6, 6, NULL, 1, NULL, '29/11/2019 01:11:49 am', NULL, 'felipem', NULL),
(33, '2edf9e5e9f0adf0b0.jpg', 'ECO 203', '3728A01811', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:12:32 am', NULL, 'felipem', NULL),
(34, 'b35b0af60ed58ef47.jpg', 'ECO 204', 'US50210876', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:13:01 am', NULL, 'felipem', NULL),
(35, 'd6bb8df0f60c48ab0.jpg', 'ECO 205', '3728A00382', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:19:52 am', NULL, 'felipem', NULL),
(36, '620450dbf61c4c19f.jpg', 'ECO 206', '9708A09160', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '5/12/2019 12:04:44 am', NULL, 'felipem', '<br>'),
(37, '41736a0baf4cb45e3.jpg', 'ECO 207', '71674', 3, 6, 5, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:01 am', NULL, 'felipem', NULL),
(38, '0eb9fdc48d520b1a8.jpg', 'ECO 208', '001838VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:29 am', NULL, 'felipem', NULL),
(39, 'f7c6ea863e2c80a47.jpg', 'ECO 209', '055133VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:16 am', NULL, 'felipem', NULL),
(40, 'c86621e1a393abd5e.jpg', 'ECO 210', '3728A01073', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '5/12/2019 12:03:57 am', NULL, 'felipem', '<br>'),
(41, '158d01f499364579d.jpg', 'ECO 211', 'USO0211434', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:21:06 am', NULL, 'felipem', NULL),
(42, '387a137ebf96710b6.jpg', 'ECO 212', 'US97807671', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:21:46 am', NULL, 'felipem', NULL),
(43, '5ed931c87e7ad07f3.jpg', 'ECO 213', '9708A04535', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:34 am', NULL, 'felipem', NULL),
(44, 'e624e9cfa97691728.jpg', 'ECO 214', '3728A00160', 3, 3, 3, 6, 6, '2019-12-04', 1, NULL, '4/12/2019 11:54:30 pm', NULL, 'felipem', '<br>'),
(45, '54d451022df8c30e6.jpg', 'ECO 215', 'US97703545', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:07 am', NULL, 'felipem', NULL),
(46, '0b54a448e7a5a93aa.jpg', 'ECO 216', 'US97808948', 3, 3, 3, 6, 6, NULL, 1, NULL, '4/12/2019 11:56:52 pm', NULL, 'felipem', '<br>'),
(47, '630053d26916342ca.jpg', 'ECO 217', 'US97800318', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:57 am', NULL, 'felipem', NULL),
(48, '9972d0e70d8ca4ca6.jpg', 'ECO 218', '3728A02302', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:47 am', NULL, 'felipem', NULL),
(49, '2e9dc0bf01f8da61f.jpg', 'ECO 219', 'US80210739', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:30:51 am', NULL, 'felipem', '<br>'),
(50, '6e5eaa081a347bc98.jpg', 'ECO 220', 'US97703205', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:27:58 am', NULL, 'felipem', '<br>'),
(51, '10b8abd62f6418d47.jpg', 'ECO 221', 'US97804106', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:05 am', NULL, 'felipem', NULL),
(52, 'b6efed45a482c8c76.jpg', 'ECO 222', 'US97804535', 3, 3, 3, 7, 7, NULL, 1, NULL, '28/11/2019 10:37:14 am', NULL, 'lgarcia', NULL),
(53, '326d48038544d0081.jpg', 'ECO 223', 'US70421029', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:53 am', NULL, 'felipem', NULL),
(54, '2114d2a34043479d1.jpg', 'ECO 224', 'US97703258', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:26:47 am', NULL, 'felipem', NULL),
(55, '99c80bd69f2b7d401.jpg', 'ECO 225', 'USD0211891', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '5/12/2019 12:06:31 am', NULL, 'felipem', 'Verificar código puesto en el equipo&nbsp;'),
(56, 'a4df3e5a8cd507c06.jpg', 'PC621', 'JCN0GR05Z985517', 12, 7, 6, 1, 1, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '27/11/2019 06:41:53 pm', 'vilmao', 'vilmao', NULL),
(58, '2fb3b0f1c5f8919a5.jpg', 'HOLTER 131', '3A-11287', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 05:06:08 pm', '5/12/2019 09:40:19 am', 'felipem', 'felipem', '<br>'),
(59, '982c34a6a71597fe5.jpg', 'HOLTER 132', '3A-11293', 1, 1, 1, 1, 1, '2019-12-03', 1, '3/12/2019 12:38:32 am', '3/12/2019 01:46:53 pm', 'felipem', 'felipem', 'Con estuche y cable de paciente&nbsp;'),
(60, '5c9c7e2c36eb50b7f.jpg', 'HOLTER 133', '3A-11285', 1, 1, 1, 1, 1, '2019-11-29', 1, '3/12/2019 12:41:03 am', '3/12/2019 01:43:50 pm', 'felipem', 'felipem', 'Estuche&nbsp;<div>Cable paciente&nbsp;</div>'),
(61, 'f19510876e2eb19cd.jpeg', 'MAPA 551', 'IP309100004', 2, 2, 7, 3, 3, NULL, 3, NULL, '5/12/2019 12:01:35 am', NULL, 'felipem', 'Equipo dañado&nbsp;'),
(62, '1d40088c027c6481a.jpeg', 'MAPA 552', '00087513', 2, 8, 8, 6, 6, '2019-12-04', 1, NULL, '4/12/2019 11:59:19 pm', NULL, 'felipem', '<br>'),
(63, NULL, 'MAPA 553', '140206005', 2, 2, 7, 3, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL),
(64, NULL, 'MAPA 554', 'IP1602200009', 2, 2, 2, 7, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL),
(65, NULL, 'MAPA 555', 'IP1602200106', 2, 2, 2, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(66, NULL, 'MAPA 556', 'IP1605200325', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(67, NULL, 'MAPA 557', 'IP1602200057', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(68, NULL, 'MAPA 558', 'IP1602200170', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(69, NULL, 'MAPA 559', 'IP1602200295', 2, 2, 2, 7, 7, NULL, 1, NULL, '5/12/2019 09:41:05 am', NULL, 'felipem', '<br>'),
(70, NULL, 'MAPA 560', 'IP1602200047', 2, 2, 2, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(71, NULL, 'MAPA 561', 'IP1602200018', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(72, '781ea01118ca82192.jpeg', 'MAPA 562', '17030100040', 2, 2, 2, 6, 6, '2019-12-04', 1, NULL, '4/12/2019 11:59:54 pm', NULL, 'felipem', '<br>'),
(73, NULL, 'MAPA 564', 'IP1701400086', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(74, NULL, 'MAPA 565', 'IP1701400310', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(75, NULL, 'MAPA 566', 'IP1701400267', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(76, NULL, 'MAPA 567', 'IP1701400116', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(77, NULL, 'MAPA 568', 'IP1701400067', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(78, 'f0babaaecbf90b169.jpeg', 'MAPA 569', '17030100024', 2, 2, 2, 3, 3, '2019-12-05', 3, NULL, '5/12/2019 12:02:36 am', NULL, 'felipem', 'Equipo dañado&nbsp;'),
(79, NULL, 'MAPA 570', '17030100023', 2, 2, 2, 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(80, '7cdb97b607ea37f77.jpeg', 'MAPA 571', '17030100042', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:19:56 am', NULL, 'felipem', '<br>'),
(81, NULL, 'MAPA 572', '17030100190', 2, 2, 2, 2, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(82, '3bd9b5e8c99cc1012.jpeg', 'MAPA 573', '17030100192', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:22:30 am', NULL, 'felipem', '<br>'),
(83, NULL, 'MAPA 574', '17030100036', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(84, NULL, 'MAPA 575', '17030100031', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(85, NULL, 'MAPA 576', '17030100016', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(86, NULL, 'MAPA 577', '17030100188', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(87, 'dbd1a242bfc1e2cbf.jpeg', 'MAPA 578', '17030100055', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:22:58 am', NULL, 'felipem', '<br>'),
(88, NULL, 'MAPA 579', '17030100033', 2, 2, 2, 7, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL),
(89, NULL, 'MAPA 580', '17030100037', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(90, NULL, 'MAPA 581', '17030100029', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(91, NULL, 'MAPA 582', '17030100030', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(92, NULL, 'MAPA 583', '17030100027', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(93, NULL, 'MAPA 584', '17030100052', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(94, NULL, 'MAPA 585', '17030100032', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(95, NULL, 'MAPA 586', '17030100021', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(96, NULL, 'MAPA 587', '17030100041', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(97, NULL, 'MAPA 588', '17030100174', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(98, NULL, 'MAPA 589', '17030100184', 2, 2, 2, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20191227141506`
--

CREATE TABLE `dispositivos_backup_20191227141506` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(40) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado` varchar(40) DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20191227141506`
--

INSERT INTO `dispositivos_backup_20191227141506` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', 'HOLTER 101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '27/11/2019 11:06:28 am', 'felipem', 'felipem', NULL, NULL),
(2, '8710a41018a7c4e42.jpg', 'HOLTER 102', '3A-11605', 1, 1, 1, 1, 1, '2019-11-27', 1, '27/11/2019 09:40:39 am', '3/12/2019 08:18:57 am', 'felipem', 'felipem', '<br>', NULL),
(3, '6630f469154d3320d.jpg', 'HOLTER 103', '3A-8045', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '27/12/2019 11:35:13 am', NULL, 'felipem', 'Tenia el serial 3A-8044, pero se cambia a 3A-8045', 1),
(4, '796ef58bbcaf815d5.jpg', 'HOLTER 104', '3A-11606', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '27/12/2019 06:11:49 am', NULL, 'felipem', '<br>', 1),
(5, 'a1ca49f2da0d3858a.jpg', 'HOLTER 105', '3A-11931', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '27/12/2019 11:50:10 am', NULL, 'felipem', 'Antes tenia el 3A-1039', 1),
(6, 'fa31345b2dec3f75d.jpg', 'HOLTER 106', '3A-11607', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '27/12/2019 06:08:41 am', NULL, 'felipem', 'Aparece con 3a-11934', 1),
(7, '62f8f3afd6062cf73.jpg', 'HOLTER 107', '3A-11608', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:26:41 am', NULL, 'felipem', NULL, NULL),
(8, '16a714e4f58af1930.jpg', 'HOLTER 108', '3A-2151', 1, 1, 1, 6, 6, '2019-12-27', 1, NULL, '27/12/2019 11:23:38 am', NULL, 'felipem', 'Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial', 1),
(9, 'fa647e5ab77632692.jpg', 'HOLTER 109', '3A-9421', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:14:08 am', NULL, 'felipem', '<br>', NULL),
(10, '476bc82bf3bed4616.jpg', 'HOLTER 110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '27/11/2019 10:46:21 am', NULL, 'felipem', NULL, NULL),
(11, 'a456dd57854fcd34f.jpg', 'HOLTER 111', '3A-11609', 1, 1, 1, 7, 7, NULL, 1, NULL, '27/11/2019 10:27:00 am', NULL, 'felipem', NULL, NULL),
(12, 'e79c8397c6fa85698.jpg', 'HOLTER 112', '3A-12178', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '27/12/2019 06:05:51 am', NULL, 'felipem', 'Aparece con 130 pero es 112&nbsp;', 1),
(13, '79766d196e19eb1db.jpg', 'HOLTER 113', '3A-9311', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '27/12/2019 06:10:07 am', NULL, 'felipem', 'Aparece con 3A-11948', 1),
(14, '1c1558589faf656c3.jpg', 'HOLTER 114', '3A-11815', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:27:18 am', NULL, 'felipem', NULL, NULL),
(15, 'a47482f69e6bb1445.jpg', 'HOLTER 115', '3A-05295', 1, 1, 1, 7, 7, '2019-12-27', 3, NULL, '27/12/2019 11:59:48 am', NULL, 'felipem', 'Equipo fundido en desfibrilacion según estado de las tarjetas', 1),
(16, 'fad32fe2f96f1b6f2.jpg', 'HOLTER 116', '3A-8940', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:01 am', NULL, 'felipem', NULL, NULL),
(17, 'f6216dd5ac51a2f82.jpg', 'HOLTER 117', '3A-9484', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 01:38:12 pm', NULL, 'felipem', 'Esruche<div>Cable viejo</div>', NULL),
(18, '6b9ff54089c40cbf3.jpg', 'HOLTER 118', '3A-9486', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:12 am', NULL, 'felipem', NULL, NULL),
(19, 'ad6939e30d7026eee.jpg', 'HOLTER 119', '3A-9572', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:28:21 am', NULL, 'felipem', NULL, NULL),
(20, '636978098692e0616.jpg', 'HOLTER 120', '3A-9487', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '27/12/2019 06:11:01 am', NULL, 'felipem', '<br>', 1),
(21, '05353fef66bf34245.jpg', 'HOLTER 121', '3A-8946', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:27 am', NULL, 'felipem', NULL, NULL),
(22, '753003a921f5f02ec.jpg', 'HOLTER 122', '3A-9485', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:32 am', NULL, 'felipem', NULL, NULL),
(23, '5cc88d4b86424bb7d.jpg', 'HOLTER 123', '3A-1993', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:03:57 am', NULL, 'felipem', 'Antes holter 112', NULL),
(24, 'b89cb8d899578dd8b.jpg', 'HOLTER 124', '3A-10318', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '27/12/2019 06:26:34 am', NULL, 'felipem', 'Aparece con 3a-9311 en csf', 1),
(25, '2b4b8d4d10dddbe8b.jpg', 'HOLTER 125', '3A-11816', 1, 1, 1, 4, 4, NULL, 1, NULL, '27/11/2019 10:28:47 am', NULL, 'felipem', NULL, NULL),
(26, '06b68e149d1227503.jpg', 'HOLTER 126', '3A-8044', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 10:42:01 am', NULL, 'felipem', 'Antes 3A-11818', NULL),
(27, 'f4964486301b6afe2.jpg', 'HOLTER 127', '3A-12175', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '27/12/2019 06:20:21 am', NULL, 'felipem', '<br>', 1),
(28, 'd179e9e2972b91703.jpg', 'HOLTER 128', '3A-12176', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '27/12/2019 06:23:06 am', NULL, 'felipem', '<br>', 1),
(29, '9e12d7e5ca84cd0f4.jpg', 'HOLTER 129', '3A-11940', 1, 1, 1, 6, 6, NULL, 1, NULL, '27/11/2019 10:29:09 am', NULL, 'felipem', NULL, NULL),
(57, 'c11ec5b51107d1e95.jpg', 'HOLTER 130', '3A-11295', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 04:45:35 pm', '5/12/2019 11:02:44 pm', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL),
(31, '3cc42f4af7a84575e.jpg', 'ECO 201', 'US97804887', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:28:42 am', NULL, 'felipem', NULL, NULL),
(32, '536dec1c5345e2b59.jpg', 'ECO 202', 'US97806979', 3, 3, 3, 6, 6, NULL, 1, NULL, '29/11/2019 01:11:49 am', NULL, 'felipem', NULL, NULL),
(33, '2edf9e5e9f0adf0b0.jpg', 'ECO 203', '3728A01811', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:12:32 am', NULL, 'felipem', NULL, NULL),
(34, 'b35b0af60ed58ef47.jpg', 'ECO 204', 'US50210876', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:13:01 am', NULL, 'felipem', NULL, NULL),
(35, 'd6bb8df0f60c48ab0.jpg', 'ECO 205', '3728A00382', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:19:52 am', NULL, 'felipem', NULL, NULL),
(36, '620450dbf61c4c19f.jpg', 'ECO 206', '9708A09160', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '5/12/2019 12:04:44 am', NULL, 'felipem', '<br>', NULL),
(37, '41736a0baf4cb45e3.jpg', 'ECO 207', '71674', 3, 6, 5, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:01 am', NULL, 'felipem', NULL, NULL),
(38, '0eb9fdc48d520b1a8.jpg', 'ECO 208', '001838VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:29 am', NULL, 'felipem', NULL, NULL),
(39, 'f7c6ea863e2c80a47.jpg', 'ECO 209', '055133VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:16 am', NULL, 'felipem', NULL, NULL),
(40, 'c86621e1a393abd5e.jpg', 'ECO 210', '3728A01073', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '5/12/2019 12:03:57 am', NULL, 'felipem', '<br>', NULL),
(41, '158d01f499364579d.jpg', 'ECO 211', 'USO0211434', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:21:06 am', NULL, 'felipem', NULL, NULL),
(42, '387a137ebf96710b6.jpg', 'ECO 212', 'US97807671', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:21:46 am', NULL, 'felipem', NULL, NULL),
(43, '5ed931c87e7ad07f3.jpg', 'ECO 213', '9708A04535', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:34 am', NULL, 'felipem', NULL, NULL),
(44, 'e624e9cfa97691728.jpg', 'ECO 214', '3728A00160', 3, 3, 3, 6, 6, '2019-12-04', 1, NULL, '4/12/2019 11:54:30 pm', NULL, 'felipem', '<br>', NULL),
(45, '54d451022df8c30e6.jpg', 'ECO 215', 'US97703545', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:07 am', NULL, 'felipem', NULL, NULL),
(46, '0b54a448e7a5a93aa.jpg', 'ECO 216', 'US97808948', 3, 3, 3, 6, 6, NULL, 1, NULL, '4/12/2019 11:56:52 pm', NULL, 'felipem', '<br>', NULL),
(47, '630053d26916342ca.jpg', 'ECO 217', 'US97800318', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:22:57 am', NULL, 'felipem', NULL, NULL),
(48, '9972d0e70d8ca4ca6.jpg', 'ECO 218', '3728A02302', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:47 am', NULL, 'felipem', NULL, NULL),
(49, '2e9dc0bf01f8da61f.jpg', 'ECO 219', 'US80210739', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:30:51 am', NULL, 'felipem', '<br>', NULL),
(50, '6e5eaa081a347bc98.jpg', 'ECO 220', 'US97703205', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:27:58 am', NULL, 'felipem', '<br>', NULL),
(51, '10b8abd62f6418d47.jpg', 'ECO 221', 'US97804106', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:25:05 am', NULL, 'felipem', NULL, NULL),
(52, 'b6efed45a482c8c76.jpg', 'ECO 222', 'US97804535', 3, 3, 3, 7, 7, NULL, 1, NULL, '28/11/2019 10:37:14 am', NULL, 'lgarcia', NULL, NULL),
(53, '326d48038544d0081.jpg', 'ECO 223', 'US70421029', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:24:53 am', NULL, 'felipem', NULL, NULL),
(54, '2114d2a34043479d1.jpg', 'ECO 224', 'US97703258', 3, 3, 3, 7, 7, NULL, 1, NULL, '29/11/2019 01:26:47 am', NULL, 'felipem', NULL, NULL),
(55, '99c80bd69f2b7d401.jpg', 'ECO 225', 'USD0211891', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '5/12/2019 12:06:31 am', NULL, 'felipem', 'Verificar código puesto en el equipo&nbsp;', NULL),
(56, 'a4df3e5a8cd507c06.jpg', 'PC 621', 'JCN0GR05Z985517', 12, 7, 6, 1, 1, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '26/12/2019 06:24:23 pm', 'vilmao', 'felipem', '<br>', 6),
(58, '2fb3b0f1c5f8919a5.jpg', 'HOLTER 131', '3A-11287', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 05:06:08 pm', '5/12/2019 11:03:20 pm', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL),
(59, '982c34a6a71597fe5.jpg', 'HOLTER 132', '3A-11293', 1, 1, 1, 1, 1, '2019-12-03', 1, '3/12/2019 12:38:32 am', '5/12/2019 11:04:07 pm', 'felipem', 'felipem', 'Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL),
(60, '5c9c7e2c36eb50b7f.jpg', 'HOLTER 133', '3A-11285', 1, 1, 1, 1, 1, '2019-11-29', 1, '3/12/2019 12:41:03 am', '5/12/2019 11:04:28 pm', 'felipem', 'felipem', 'Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL),
(61, 'f19510876e2eb19cd.jpeg', 'MAPA 551', 'IP309100004', 2, 2, 7, 3, 3, NULL, 3, NULL, '5/12/2019 12:01:35 am', NULL, 'felipem', 'Equipo dañado&nbsp;', NULL),
(62, '1d40088c027c6481a.jpeg', 'MAPA 552', '00087513', 2, 8, 8, 6, 6, '2019-12-04', 1, NULL, '4/12/2019 11:59:19 pm', NULL, 'felipem', '<br>', NULL),
(63, NULL, 'MAPA 553', '140206005', 2, 2, 7, 3, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(64, NULL, 'MAPA 554', 'IP1602200009', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '27/12/2019 12:48:25 pm', NULL, 'felipem', 'flm', 2),
(65, NULL, 'MAPA 555', 'IP1602200106', 2, 2, 2, 7, 7, NULL, 1, NULL, '27/12/2019 12:06:27 pm', NULL, 'felipem', '<br>', 2),
(66, NULL, 'MAPA 556', 'IP1605200325', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(67, NULL, 'MAPA 557', 'IP1602200057', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(68, NULL, 'MAPA 558', 'IP1602200170', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(69, NULL, 'MAPA 559', 'IP1602200295', 2, 2, 2, 7, 7, NULL, 1, NULL, '5/12/2019 09:41:05 am', NULL, 'felipem', '<br>', NULL),
(70, NULL, 'MAPA 560', 'IP1602200047', 2, 2, 2, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(71, NULL, 'MAPA 561', 'IP1602200018', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(72, '781ea01118ca82192.jpeg', 'MAPA 562', '17030100040', 2, 2, 2, 6, 6, '2019-12-04', 1, NULL, '4/12/2019 11:59:54 pm', NULL, 'felipem', '<br>', NULL),
(73, NULL, 'MAPA 564', 'IP1701400086', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(74, NULL, 'MAPA 565', 'IP1701400310', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(75, NULL, 'MAPA 566', 'IP1701400267', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(76, NULL, 'MAPA 567', 'IP1701400116', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(77, NULL, 'MAPA 568', 'IP1701400067', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'f0babaaecbf90b169.jpeg', 'MAPA 569', '17030100024', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '27/12/2019 12:50:46 pm', NULL, 'felipem', 'Equipo dañado&nbsp;', 2),
(79, NULL, 'MAPA 570', '17030100023', 2, 2, 2, 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(80, '7cdb97b607ea37f77.jpeg', 'MAPA 571', '17030100042', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:19:56 am', NULL, 'felipem', '<br>', NULL),
(81, NULL, 'MAPA 572', '17030100190', 2, 2, 2, 2, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(82, '3bd9b5e8c99cc1012.jpeg', 'MAPA 573', '17030100192', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:22:30 am', NULL, 'felipem', '<br>', NULL),
(83, NULL, 'MAPA 574', '17030100036', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(84, NULL, 'MAPA 575', '17030100031', 2, 2, 2, 5, 5, '2019-12-27', 1, NULL, '27/12/2019 05:59:45 am', NULL, 'felipem', '<br>', 2),
(85, NULL, 'MAPA 576', '17030100016', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '27/12/2019 11:36:25 am', NULL, 'felipem', '<br>', 2),
(86, NULL, 'MAPA 577', '17030100188', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'dbd1a242bfc1e2cbf.jpeg', 'MAPA 578', '17030100055', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '3/12/2019 11:22:58 am', NULL, 'felipem', '<br>', NULL),
(88, NULL, 'MAPA 579', '17030100033', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '27/12/2019 12:55:48 pm', NULL, 'felipem', '<br>', 2),
(89, NULL, 'MAPA 580', '17030100037', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(90, NULL, 'MAPA 581', '17030100029', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '27/12/2019 12:53:47 pm', NULL, 'felipem', '<br>', 2),
(91, NULL, 'MAPA 582', '17030100030', 2, 2, 2, 5, 5, NULL, 1, NULL, '27/12/2019 06:00:59 am', NULL, 'felipem', '<br>', 2),
(92, NULL, 'MAPA 583', '17030100027', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '27/12/2019 12:56:35 pm', NULL, 'felipem', '<br>', 2),
(93, NULL, 'MAPA 584', '17030100052', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '27/12/2019 12:56:09 pm', NULL, 'felipem', '<br>', 2),
(94, NULL, 'MAPA 585', '17030100032', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '27/12/2019 11:35:49 am', NULL, 'felipem', 'flm', 2),
(95, NULL, 'MAPA 586', '17030100021', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(96, NULL, 'MAPA 587', '17030100041', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(97, NULL, 'MAPA 588', '17030100174', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(98, NULL, 'MAPA 589', '17030100184', 2, 2, 2, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(99, NULL, 'TR 251', '404262WX8', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'GE , 3Sc-RS', NULL),
(100, NULL, 'TR 252', '333407WX5', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'GE , 3Sc-RS', NULL),
(101, NULL, 'TR 253', 'US97310163', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 21330A', NULL),
(102, NULL, 'TR 254', 'US99N01008', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , S3', NULL),
(103, NULL, 'TR 255', '02M7J5', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 11-3L', NULL),
(104, NULL, 'TR 256', '03DNL7', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , IPx-7', NULL),
(105, NULL, 'TR 257', 'US99N01942', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SIUI , 21311A', NULL),
(106, NULL, 'TR 258', '3709A00256', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 213550A', NULL),
(107, NULL, 'TR 259', 'US97404881', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , T62110', NULL),
(108, NULL, 'TR 260', 'US99300683', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 11-3L', NULL),
(109, NULL, 'TR 261', 'US97302745', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 21350A', NULL),
(110, NULL, 'TR 262', 'US99N04288', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , ', NULL),
(111, NULL, 'TR 263', '02M7PY', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , IPX-7', NULL),
(112, NULL, 'TR 264', '3630A00395', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 21367A', NULL),
(113, NULL, 'TR 265', '18ZHQQ', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , 21369A', NULL),
(114, NULL, 'TR 266', '02QDYM', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , 21311A', NULL),
(115, NULL, 'TR 267', '142585PD9', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'GE , 6S-RS', NULL),
(116, NULL, 'TR 268', 'US99300678', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 21321A', NULL),
(117, NULL, 'TR 269', 'US99N03966', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILLIPS , 21311A', NULL),
(118, NULL, 'TR 270', 'US97403824', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , ', NULL),
(119, NULL, 'TR 271', '3716A00792', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HEWLETT PACKARD , 21369A', NULL),
(120, NULL, 'TR 272', 'US99N04681', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PHILIPS , 21311A', NULL),
(121, NULL, 'ECG 301', '20161031/BTT02/00293', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT02', NULL),
(122, NULL, 'ECG 302', '20161031/BTT02/01743', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT02', NULL),
(123, NULL, 'ECG 303', '20161031/BTT02/01729', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT02', NULL),
(124, NULL, 'ECG 304', '20140721/BBT02/00290', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT02', NULL),
(125, NULL, 'ECG 305', 'D1081A0753', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'CONTEC 8000', NULL),
(126, NULL, 'DG 401', '180401', 15, 9, 9, 7, 7, '2019-12-10', 1, NULL, '10/12/2019 12:30:54 am', NULL, 'felipem', 'HOSPITAL ISAIAS DUARTE CANCINO', NULL),
(127, NULL, 'DG 402', '180402', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(128, '18a13cd3f35261938.jpg', 'DG 403', '180403', 15, 9, 9, 9, 9, '2019-12-10', 1, NULL, '10/12/2019 12:38:09 am', NULL, 'felipem', '<br>', NULL),
(129, NULL, 'DG 404', '180404', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(130, NULL, 'DG 405', '180405', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(131, NULL, 'DG 406', '180406', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(132, NULL, 'DG 407', '180407', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HOSPITAL ISAIAS DUARTE CANCINO', NULL),
(133, NULL, 'DES 451', 'B97K125', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'ZOLL , PD 1200 ,  , ', NULL),
(134, NULL, 'DES 452', 'B97K12587', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'ZOLL , PD 1200 , YUMBO , SUMEDICA', NULL),
(135, NULL, 'DES 453', 'B97K12574', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE', NULL),
(136, NULL, 'DES 454', '98278', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, ' , PIC 30 , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', NULL),
(137, NULL, 'DES 455', '12129094', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER', NULL),
(138, NULL, 'DES 456', '12127290', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'ARTEMA , IP 22 , CALI , HIDC', NULL),
(139, NULL, 'DES 457', '5126132', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'ARTEMA ,  , CARDIO AID 200 , HIDC', NULL),
(140, NULL, 'DES 458', '90427', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'WELCHALLYN , PIC 30 , TULUA , HOSPITAL TTOMAS URIBE', NULL),
(141, NULL, 'PC601', 'A7V87A', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'COMPAQ ,  , COMPAQ , ', NULL),
(142, NULL, 'PC602', 'HN3F91QC100398', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SAMSUNG ,  , INTEGRADO , ', NULL),
(143, NULL, 'PC603', 'U082MA00', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DELL ,  , INTEGRADO , ', NULL),
(144, NULL, 'PC604', 'CLON', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'INTEL ,  , JANUS , ', NULL),
(145, NULL, 'PC606', 'CS02589545', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LENOVO ,  , INTEGRADO , ', NULL),
(146, NULL, 'PC607', 'VS81211172', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LENOVO ,  , INTEGRADO , ', NULL),
(147, NULL, 'PC608', '6MC2190SSX', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LENOVO ,  , INTEGRADO , ', NULL),
(148, NULL, 'PC609', '3CR2221DW9', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'COMPAQ ,  , COMPAQ , ', NULL),
(149, NULL, 'PC610', 'VS81225546', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LENOVO ,  , INTEGRADO , ', NULL),
(150, NULL, 'PC611', '5CB40840JJ', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HP ,  , INTEGRADO , ', NULL),
(151, NULL, 'PC612', 'HYXS91LD200746', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SAMSUNG ,  , INTEGRADO , ', NULL),
(152, NULL, 'PC613', '5CM31902QS', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HP ,  , INTEGRADO , ', NULL),
(153, NULL, 'PC614', '', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LG ,  , JONUS , ', NULL),
(154, NULL, 'PC615', '335255', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'COMPAQ ,  , AOPEN , ', NULL),
(155, NULL, 'PC617', '5CD440256Q', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'HP ,  , HP , ', NULL),
(156, NULL, 'PC618', 'CS01429411', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LENOVO ,  , INTEGRADO , ', NULL),
(157, NULL, 'PC619', 'PF0N6RLY', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'LENOVO ,  , NA , ', NULL),
(158, 'bcdf768ef42798802.jpg', 'PC620', 'CS02593439', 11, 10, 11, 9, 9, '2019-12-10', 1, NULL, '10/12/2019 04:15:46 pm', NULL, 'felipem', 'LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div>', NULL),
(159, NULL, 'BT701', 'SN: 10-10-10', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'PERFECT 10 , PERFECT 10 ,  , ', NULL),
(160, NULL, 'BT702', '201409026161', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SPORT FITNESS , JS-5000B-1 ,  , ', NULL),
(161, NULL, 'BT704', 'SN: 270765 JC', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, ' ,  ,  , ', NULL),
(162, NULL, 'BT705', '201312044116', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SPORT FITNESS , JS-5000B-1 ,  , ', NULL),
(163, NULL, 'BT706', '201409026192', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SPORT FITNESS , JS-5000B-1 ,  , ', NULL),
(164, NULL, 'BT708', '201509043123', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SPORT FITNESS ,  ,  , ', NULL),
(165, NULL, 'BT710', '201509043152', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'SPORT FITNESS , JS-5000B-1 , HIDC , ', NULL),
(166, NULL, 'STR751', '20140625/BTT/01068', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20140625/BTR/01068 ,  , ', NULL),
(167, NULL, 'STR752', '20140625/BTT/01107', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20140625/BTR/01107 ,  , ', NULL),
(168, NULL, 'STR753', '20160725/BTT/01304', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20160725/BTR/01304 ,  , ', NULL),
(169, NULL, 'STR754', '20160104/BTT/01253', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20160104/BTR/01253 ,  , ', NULL),
(170, NULL, 'STR755', '20141201/BTT/01123', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20141201/BTR/01123 ,  , ', NULL),
(171, NULL, 'STR756', '20160725/BTT/01307', 6, 1, 10, 9, 9, '2019-12-10', 1, NULL, '10/12/2019 09:46:57 am', NULL, 'felipem', 'DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>', NULL),
(172, NULL, 'STR757', '20150129/BTT/01136', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20150129/BTR/01136 ,  , ', NULL),
(173, NULL, 'STR758', '20160104/BTR/01253', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 ,  ,  , ', NULL),
(174, NULL, 'STR759', '4712AU3681E', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'QRSCardUSB , NA ,  , ', NULL),
(175, NULL, 'STR760', 'SN BTR01-343', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 ,  ,  , ', NULL),
(176, NULL, 'STR761', 'SM 311SN: D111E0004', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS300-BTT03 ,  ,  , ', NULL),
(177, NULL, 'STR762', 'SN: 20140721/BTT02/00290', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS300-BTT02 ,  ,  , ', NULL),
(178, NULL, 'STR763', '20160725/BTT/01299', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'DMS 300-BTT01 , 20160725/BTR/01299 ,  , ', NULL),
(179, NULL, 'SM901', 'SM 10', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , BUGA , HSJ', NULL),
(180, NULL, 'SM902', 'SM 11', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , BUGA , HSJ', NULL),
(181, NULL, 'SM903', 'SM 12', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , CALI , ADMIN.', NULL),
(182, NULL, 'SM904', 'SM 13', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , TULUA , HTU', NULL),
(183, NULL, 'SM905', 'SM 14', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , CASA , CASA', NULL),
(184, NULL, 'SM906', 'SM 15', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , TECNOLOGIA', NULL),
(185, NULL, 'SM907', 'SM 16', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(186, NULL, 'SM908', 'SM 17', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(187, NULL, 'SM909', 'SM 18', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , BUGA , HSJ', NULL),
(188, NULL, 'SM910', 'SM 19', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , BUGA , HSJ', NULL),
(189, NULL, 'SM911', 'SM 20', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(190, NULL, 'SM912', 'SM 21', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(191, NULL, 'SM913', 'SM 22', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(192, NULL, 'SM914', 'SM 23', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(193, NULL, 'SM915', 'SM 24', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 16 GB , CASA , ', NULL),
(194, NULL, 'SM801', 'VND3J28585', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO', NULL),
(195, NULL, 'SM802', 'CN26RBK063', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', NULL),
(196, NULL, 'SM803', 'CN22SBTFP', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', NULL),
(197, NULL, 'SM804', 'SE8Y011865', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO', NULL),
(198, NULL, 'SM805', 'SE8Y018164', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'EPSON WORKFORCE ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', NULL),
(199, 'd7dffdba6db4acaa5.jpg', 'HOLTER 134', '3A-11289', 1, 1, 1, 4, 4, '2019-12-05', 2, '5/12/2019 10:59:48 pm', '10/12/2019 06:38:50 pm', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>', NULL),
(200, '0517ef6974ab1463b.jpg', 'HOLTER 135', '3A-11288', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:01:16 pm', '27/12/2019 11:15:52 am', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>', 1),
(201, 'c830bd855b5933e47.jpg', 'HOLTER 136', '3A-11286', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:06:13 pm', '27/12/2019 11:06:57 am', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1),
(202, '9cd28935c6a69d0a3.jpg', 'HOLTER 137', '3A-11292', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 11:07:54 pm', '10/12/2019 06:44:08 pm', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL),
(203, 'ec24083a0b7fcd5a2.jpg', 'HOLTER 138', '3A-11291', 1, 1, 1, 4, 4, '2019-12-09', 1, '5/12/2019 11:08:49 pm', '10/12/2019 06:41:34 pm', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>', NULL),
(204, 'caeae04f26da4d82b.jpg', 'HOLTER 139', '3A-11290', 1, 1, 1, 5, 5, '2019-12-05', 1, '5/12/2019 11:09:40 pm', '27/12/2019 11:09:07 am', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>', 1),
(205, NULL, 'PRUEBA1', '123456', 10, 7, 6, 7, 7, '2019-12-27', 2, '27/12/2019 09:54:45 am', NULL, 'bodega.felipe', NULL, '<br>', 6),
(206, NULL, 'MAPA 590', '191202000', 2, 2, 12, 7, 7, '2019-12-27', 1, '27/12/2019 01:58:38 pm', NULL, 'felipem', NULL, '<br>', 12),
(207, NULL, 'MAPA 591', '19120200002', 2, 2, 12, 7, 7, '2019-12-27', 1, '27/12/2019 02:01:17 pm', NULL, 'felipem', NULL, 'Noviembre 2019 China', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20201119230142`
--

CREATE TABLE `dispositivos_backup_20201119230142` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(4) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado2` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado1` datetime DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20201119230142`
--

INSERT INTO `dispositivos_backup_20201119230142` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', '101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span>', 1, 'HOLTER 101', '2019-11-27 00:00:00', NULL, NULL),
(2, '8710a41018a7c4e42.jpg', '102', '3A-11605', 1, 1, 1, 1, 1, '2019-11-27', 1, '27/11/2019 09:40:39 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 1, 'HOLTER 102', '2019-11-27 00:00:00', NULL, NULL),
(3, '6630f469154d3320d.jpg', '103', '3A-8045', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Tenia el serial 3A-8044, pero se cambia a 3A-8045', 1, 'HOLTER 103', '2020-03-05 00:00:00', NULL, NULL),
(4, '796ef58bbcaf815d5.jpg', '104', '3A-11606', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 104', '2020-03-05 00:00:00', NULL, NULL),
(5, 'a1ca49f2da0d3858a.jpg', '105', '3A-11931', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes tenia el 3A-1039', 1, 'HOLTER 105', '2020-03-05 00:00:00', NULL, NULL),
(6, 'fa31345b2dec3f75d.jpg', '106', '3A-11607', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 3a-11934', 1, 'HOLTER 106', '2020-03-05 00:00:00', NULL, NULL),
(7, '62f8f3afd6062cf73.jpg', '107', '3A-11608', 1, 1, 1, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 107', '2020-03-05 00:00:00', NULL, NULL),
(8, '16a714e4f58af1930.jpg', '108', '3A-2151', 1, 1, 1, 6, 6, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial', 1, 'HOLTER 108', '2020-03-05 00:00:00', NULL, NULL),
(9, 'fa647e5ab77632692.jpg', '109', '3A-9421', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 1, 'HOLTER 109', '2020-03-05 00:00:00', NULL, NULL),
(10, '476bc82bf3bed4616.jpg', '110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 110', '2020-03-05 00:00:00', NULL, NULL),
(11, 'a456dd57854fcd34f.jpg', '111', '3A-11609', 1, 1, 1, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 111', '2020-03-05 00:00:00', NULL, NULL),
(12, 'e79c8397c6fa85698.jpg', '112', '3A-12178', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 130 pero es 112&nbsp;', 1, 'HOLTER 112', '2020-03-05 00:00:00', NULL, NULL),
(13, '79766d196e19eb1db.jpg', '113', '3A-9311', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 3A-11948', 1, 'HOLTER 113', '2020-03-05 00:00:00', NULL, NULL),
(14, '1c1558589faf656c3.jpg', '114', '3A-11815', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, se encuentra en FHSJ, funcionamiento normal 04-Marzo-2020, Camilo.', 1, 'HOLTER 114', '2020-03-05 00:00:00', NULL, NULL),
(15, 'a47482f69e6bb1445.jpg', '115', '3A-05295', 1, 1, 1, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo fundido en desfibrilacion según estado de las tarjetas', 1, 'HOLTER 115', '2020-03-05 00:00:00', NULL, NULL),
(16, 'fad32fe2f96f1b6f2.jpg', '116', '3A-8940', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 116', '2020-03-05 00:00:00', NULL, NULL),
(17, 'f6216dd5ac51a2f82.jpg', '117', '3A-9484', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 117', '2020-03-05 00:00:00', NULL, NULL),
(18, '6b9ff54089c40cbf3.jpg', '118', '3A-9486', 1, 1, 1, 4, 4, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 118', '2020-03-05 00:00:00', NULL, NULL),
(19, 'ad6939e30d7026eee.jpg', '119', '3A-9572', 1, 1, 1, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funciona normal en FHSJ 17/Marzo/2020 Camilo.', 1, 'HOLTER 119', '2020-03-05 00:00:00', NULL, NULL),
(20, '636978098692e0616.jpg', '120', '3A-9487', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 120', '2020-03-05 00:00:00', NULL, NULL),
(21, '05353fef66bf34245.jpg', '121', '3A-8946', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal, se cambia la etiqueta del código y del serial 06-Marzo-2020, Camilo.', 1, 'HOLTER 121', '2020-03-05 00:00:00', NULL, NULL),
(22, '753003a921f5f02ec.jpg', '122', '3A-9485', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, se cambió la etiqueta del código y del serial, 06/Marzo/2020.', 1, 'HOLTER 122', '2020-03-05 00:00:00', NULL, NULL),
(23, '5cc88d4b86424bb7d.jpg', '123', '3A-1993', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Antes holter 112, Felipe.<div><br><div>El Holter 123 realmente se encuentra en FHSJ, pendiente revisar el clon en HIDC 04-Marzo-2020, Camilo.</div></div>', 1, 'HOLTER 123', '2020-03-05 00:00:00', NULL, NULL),
(24, 'b89cb8d899578dd8b.jpg', '124', '3A-10318', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', '', 'felipem', 'Aparece con 3a-9311 en csf', 1, 'HOLTER 124', '2020-03-05 00:00:00', NULL, NULL),
(25, '2b4b8d4d10dddbe8b.jpg', '125', '3A-11816', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparece con el serial 3A -7125, el cual es el serial del HOLTER 101, se cambian etiquetas y queda con el serial&nbsp; 3A-11816. 06/Marzo/2020. Camilo', 1, 'HOLTER 125', '2020-03-05 00:00:00', NULL, NULL),
(26, '06b68e149d1227503.jpg', '126', '3A-8044', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes 3A-11818', 1, 'HOLTER 126', '2020-03-05 00:00:00', NULL, NULL),
(27, 'f4964486301b6afe2.jpg', '127', '3A-12175', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 1, 'HOLTER 127', '2020-03-05 00:00:00', NULL, NULL),
(28, 'd179e9e2972b91703.jpg', '128', '3A-12176', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo', 1, 'HOLTER 128', '2020-03-05 00:00:00', NULL, NULL),
(29, '9e12d7e5ca84cd0f4.jpg', '129', '3A-11940', 1, 1, 1, 6, 6, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'HOLTER 129', '2020-03-05 00:00:00', NULL, NULL),
(57, 'c11ec5b51107d1e95.jpg', '130', '3A-11295', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 04:45:35 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 130', '2019-11-29 00:00:00', NULL, NULL),
(31, '3cc42f4af7a84575e.jpg', '201', 'US97804887', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparecía en Bodega, realmente se encuentra en CSF, Funciona normalmente, 06/Marzo/2020, Camilo.', 3, 'ECO 201', '2020-03-05 00:00:00', NULL, NULL),
(32, '536dec1c5345e2b59.jpg', '202', 'US97806979', 3, 3, 3, 6, 6, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 202', '2020-03-05 00:00:00', NULL, NULL),
(33, '2edf9e5e9f0adf0b0.jpg', '203', '3728A01811', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 203', '2020-03-05 00:00:00', NULL, NULL),
(34, 'b35b0af60ed58ef47.jpg', '204', 'US50210876', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente está en HROB. 02/Marzo/2020 Camilo.<div><br></div><div>Actualizo, el día de hoy 17/Marzo/2020 se lleva equipo desde HROB (Palmira) a FHSJ (Buga), Camilo.</div>', 3, 'ECO 204', '2020-03-05 00:00:00', NULL, NULL),
(35, 'd6bb8df0f60c48ab0.jpg', '205', '3728A00382', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 205', '2020-03-05 00:00:00', NULL, NULL),
(36, '620450dbf61c4c19f.jpg', '206', '9708A09160', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Aparecía en HTU, realmente está en Bodega. 05/Marzo/2020. Camilo', 3, 'ECO 206', '2020-03-05 00:00:00', NULL, NULL),
(37, '41736a0baf4cb45e3.jpg', '207', '71674', 3, 6, 5, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 207', '2020-03-05 00:00:00', NULL, NULL),
(38, '0eb9fdc48d520b1a8.jpg', '208', '001838VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 208', '2020-03-05 00:00:00', NULL, NULL),
(39, 'f7c6ea863e2c80a47.jpg', '209', '055133VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 209', '2020-03-05 00:00:00', NULL, NULL),
(40, 'c86621e1a393abd5e.jpg', '210', '3728A01073', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmente en HTU, 21/Marzo/2020, Camilo.', 3, 'ECO 210', '2020-03-05 00:00:00', NULL, NULL),
(41, '158d01f499364579d.jpg', '211', 'USO0211434', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 211', '2020-03-05 00:00:00', NULL, NULL),
(42, '387a137ebf96710b6.jpg', '212', 'US97807671', 3, 3, 3, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 3, 'ECO 212', '2020-03-05 00:00:00', NULL, NULL),
(43, '5ed931c87e7ad07f3.jpg', '213', '9708A04535', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 213', '2020-03-05 00:00:00', NULL, NULL),
(44, 'e624e9cfa97691728.jpg', '214', '3728A00160', 3, 3, 3, 7, 7, '2019-12-04', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'No se encuentra en FHSJ', 3, 'ECO 214', '2020-03-05 00:00:00', NULL, NULL),
(45, '54d451022df8c30e6.jpg', '215', 'US97703545', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 215', '2020-03-05 00:00:00', NULL, NULL),
(46, '0b54a448e7a5a93aa.jpg', '216', 'US97808948', 3, 3, 3, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 3, 'ECO 216', '2020-03-05 00:00:00', NULL, NULL),
(47, '630053d26916342ca.jpg', '217', 'US97800318', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 217', '2020-03-05 00:00:00', NULL, NULL),
(48, '9972d0e70d8ca4ca6.jpg', '218', '3728A02302', 3, 3, 3, 6, 6, '2020-02-20', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizado, se encontraba en UCI río se deja en FHSJ 20-02-2018<div><br></div><div>Actualizo, equipo fuera de servicio, en reparación, 04/Marzo/2020, Camilo.<br><div><br></div></div>', 3, 'ECO 218', '2020-03-05 00:00:00', NULL, NULL),
(49, '2e9dc0bf01f8da61f.jpg', '219', 'US80210739', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 3, 'ECO 219', '2020-03-05 00:00:00', NULL, NULL),
(50, '6e5eaa081a347bc98.jpg', '220', 'US97703205', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'ECO 220', '2020-03-05 00:00:00', NULL, NULL),
(51, '10b8abd62f6418d47.jpg', '221', 'US97804106', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmete en HROB, 02/Marzo/2020, Camilo.', 3, 'ECO 221', '2020-03-05 00:00:00', NULL, NULL),
(52, 'b6efed45a482c8c76.jpg', '222', 'US97804535', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, está en Bodega inactivo 05/Marzo/2020. Camilo', 3, 'ECO 222', '2020-03-05 00:00:00', NULL, NULL),
(53, '326d48038544d0081.jpg', '223', 'US70421029', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente se encuentra en Clínica San Francisco 06/Marzo/2020, Camilo', 3, 'ECO 223', '2020-03-05 00:00:00', NULL, NULL),
(54, '2114d2a34043479d1.jpg', '224', 'US97703258', 3, 3, 3, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:27:42', NULL, 'felipem', NULL, 3, 'ECO 224', '2020-03-05 00:00:00', NULL, NULL),
(55, '99c80bd69f2b7d401.jpg', '225', 'USD0211891', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Verificar código puesto en el equipo&nbsp;', NULL, 'ECO 225', '2020-03-05 00:00:00', NULL, NULL),
(56, 'a4df3e5a8cd507c06.jpg', '621', 'JCN0GR05Z985517', 11, 7, 6, 7, 7, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '0000-00-00 00:00:00', 'vilmao', 'felipem', '<br>', 6, 'PC 621', '2019-11-27 00:00:00', NULL, NULL),
(58, '2fb3b0f1c5f8919a5.jpg', '131', '3A-11287', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 05:06:08 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 131', '2019-11-29 00:00:00', NULL, NULL),
(59, '982c34a6a71597fe5.jpg', '132', '3A-11293', 1, 1, 1, 1, 1, '2019-12-03', 1, '3/12/2019 12:38:32 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 132', '2019-12-03 00:00:00', NULL, NULL),
(60, '5c9c7e2c36eb50b7f.jpg', '133', '3A-11285', 1, 1, 1, 1, 1, '2019-11-29', 1, '3/12/2019 12:41:03 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 133', '2019-12-03 00:00:00', NULL, NULL),
(61, 'f19510876e2eb19cd.jpeg', '551', 'IP309100004', 2, 2, 7, 3, 3, NULL, 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', NULL, 'MAPA 551', '2020-03-05 00:00:00', NULL, NULL),
(62, '1d40088c027c6481a.jpeg', '552', '00087513', 2, 8, 8, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 552', '2020-03-05 00:00:00', NULL, NULL),
(63, NULL, '553', '140206005', 2, 2, 7, 3, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 553', '2020-03-05 00:00:00', NULL, NULL),
(64, NULL, '554', 'IP1602200009', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'flm', 2, 'MAPA 554', '2020-03-05 00:00:00', NULL, NULL),
(65, NULL, '555', 'IP1602200106', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 555', '2020-03-05 00:00:00', NULL, NULL),
(66, NULL, '556', 'IP1605200325', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 556', '2020-03-05 00:00:00', NULL, NULL),
(67, NULL, '557', 'IP1602200057', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en HTU, se coloca nueva etiqueta con código y serial. 06/Marzo/2020, Camilo', 2, 'MAPA 557', '2020-03-05 00:00:00', NULL, NULL),
(68, NULL, '558', 'IP1602200170', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 558', '2020-03-05 00:00:00', NULL, NULL),
(69, NULL, '559', 'IP1602200295', 2, 2, 2, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 559', '2020-03-05 00:00:00', NULL, NULL),
(70, NULL, '560', 'IP1602200047', 2, 2, 2, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 560', '2020-03-05 00:00:00', NULL, NULL),
(71, NULL, '561', 'IP1602200018', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 561', '2020-03-05 00:00:00', NULL, NULL),
(72, '781ea01118ca82192.jpeg', '562', '17030100040', 2, 2, 2, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 562', '2020-03-05 00:00:00', NULL, NULL),
(73, NULL, '564', 'IP1701400086', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 564', '2020-03-05 00:00:00', NULL, NULL),
(74, NULL, '565', 'IP1701400310', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 565', '2020-03-05 00:00:00', NULL, NULL),
(75, NULL, '566', 'IP1701400267', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 566', '2020-03-05 00:00:00', NULL, NULL),
(76, NULL, '567', 'IP1701400116', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 567', '2020-03-05 00:00:00', NULL, NULL),
(77, NULL, '568', 'IP1701400067', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 568', '2020-03-05 00:00:00', NULL, NULL),
(78, 'f0babaaecbf90b169.jpeg', '569', '17030100024', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', 2, 'MAPA 569', '2020-03-05 00:00:00', NULL, NULL),
(79, NULL, '570', '17030100023', 2, 2, 2, 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 570', '2020-03-05 00:00:00', NULL, NULL),
(80, '7cdb97b607ea37f77.jpeg', '571', '17030100042', 2, 2, 2, 1, 1, '2019-12-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Se encuentra en mantenimiento en la Bodega', 2, 'MAPA 571', '2020-03-05 00:00:00', NULL, NULL),
(81, NULL, '572', '17030100190', 2, 2, 2, 2, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 572', '2020-03-05 00:00:00', NULL, NULL),
(82, '3bd9b5e8c99cc1012.jpeg', '573', '17030100192', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 2, 'MAPA 573', '2020-03-05 00:00:00', NULL, NULL),
(83, NULL, '574', '17030100036', 2, 2, 2, 9, 9, '2020-11-19', 1, NULL, '2020-11-19 14:52:16', NULL, 'andresl', NULL, 2, 'MAPA 574', '2020-03-05 00:00:00', NULL, NULL),
(84, NULL, '575', '17030100031', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, Funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 2, 'MAPA 575', '2020-03-05 00:00:00', NULL, NULL),
(85, NULL, '576', '17030100016', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 576', '2020-03-05 00:00:00', NULL, NULL),
(86, NULL, '577', '17030100188', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 577', '2020-03-05 00:00:00', NULL, NULL),
(87, 'dbd1a242bfc1e2cbf.jpeg', '578', '17030100055', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'El dispositivo opera normalmente en el HIDC', 2, 'MAPA 578', '2020-03-05 00:00:00', NULL, NULL),
(88, NULL, '579', '17030100033', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 579', '2020-03-05 00:00:00', NULL, NULL),
(89, NULL, '580', '17030100037', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 580', '2020-03-05 00:00:00', NULL, NULL),
(90, NULL, '581', '17030100029', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '2020-11-19 14:23:27', NULL, 'andresl', '<br>', 2, 'MAPA 581', '2020-03-05 00:00:00', NULL, NULL),
(91, NULL, '582', '17030100030', 2, 2, 2, 4, 4, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 582', '2020-03-05 00:00:00', NULL, NULL),
(92, NULL, '583', '17030100027', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 583', '2020-03-05 00:00:00', NULL, NULL),
(93, NULL, '584', '17030100052', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 584', '2020-03-05 00:00:00', NULL, NULL),
(94, NULL, '585', '17030100032', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'flm', 2, 'MAPA 585', '2020-03-05 00:00:00', NULL, NULL),
(95, NULL, '586', '17030100021', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 586', '2020-03-05 00:00:00', NULL, NULL),
(96, NULL, '587', '17030100041', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 587', '2020-03-05 00:00:00', NULL, NULL),
(97, NULL, '588', '17030100174', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 588', '2020-03-05 00:00:00', NULL, NULL),
(98, NULL, '589', '17030100184', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 589', '2020-03-05 00:00:00', NULL, NULL),
(99, NULL, '251', '404262WX8', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 251', '2020-03-05 00:00:00', NULL, NULL),
(100, NULL, '252', '333407WX5', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 252', '2020-03-05 00:00:00', NULL, NULL),
(101, NULL, '253', 'US97310163', 13, 3, 25, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 25, 'TR 253', '2020-03-05 00:00:00', NULL, NULL),
(102, NULL, '254', 'US99N01008', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , S3', 0, 'TR 254', '2020-03-05 00:00:00', NULL, NULL),
(103, NULL, '255', '02M7J5', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 11-3L', 0, 'TR 255', '2020-03-05 00:00:00', NULL, NULL),
(104, NULL, '256', '03DNL7', 13, 3, 20, 3, 3, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'PHILIPS , IPx-7', 20, 'TR 256', '2020-03-05 00:00:00', NULL, NULL),
(105, NULL, '257', 'US99N01942', 13, 3, 16, 1, 1, '2020-05-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'SIUI , 21311A', 16, 'TR 257', '2020-03-05 00:00:00', NULL, NULL),
(106, NULL, '258', '3709A00256', 13, 3, 17, 2, 2, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 258', '2020-03-05 00:00:00', NULL, NULL),
(107, NULL, '259', 'US97404881', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , T62110', 0, 'TR 259', '2020-03-05 00:00:00', NULL, NULL),
(108, NULL, '260', 'US99300683', 13, 3, 24, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 24, 'TR 260', '2020-03-05 00:00:00', NULL, NULL),
(109, NULL, '261', 'US97302745', 13, 3, 17, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 261', '2020-03-05 00:00:00', NULL, NULL),
(110, NULL, '262', 'US99N04288', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS&nbsp;', 16, 'TR 262', '2020-03-05 00:00:00', NULL, NULL),
(111, NULL, '263', '02M7PY', 13, 3, 20, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:43:01', NULL, 'felipem', 'Actualizo, funciona normalmente en HTU, 06/Marzo/2020. Camilo', 20, 'TR 263', '2020-03-05 00:00:00', NULL, NULL),
(112, NULL, '264', '3630A00395', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21367A', 0, 'TR 264', '2020-03-05 00:00:00', NULL, NULL),
(113, NULL, '265', '18ZHQQ', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21369A', 0, 'TR 265', '2020-03-05 00:00:00', NULL, NULL),
(114, NULL, '266', '02QDYM', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 266', '2020-03-05 00:00:00', NULL, NULL),
(115, NULL, '267', '142585PD9', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 6S-RS', 0, 'TR 267', '2020-03-05 00:00:00', NULL, NULL),
(116, NULL, '268', 'US99300678', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21321A', 0, 'TR 268', '2020-03-05 00:00:00', NULL, NULL),
(117, NULL, '269', 'US99N03966', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILLIPS , 21311A', 16, 'TR 269', '2020-03-05 00:00:00', NULL, NULL),
(118, NULL, '270', 'US97403824', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD ,', 0, 'TR 270', '2020-03-05 00:00:00', NULL, NULL),
(119, NULL, '271', '3716A00792', 13, 3, 26, 7, 7, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Estaba en el HIDC, el dia 4 de Marzo se llevará para el HMC', 26, 'TR 271', '2020-03-05 00:00:00', NULL, NULL),
(120, NULL, '272', 'US99N04681', 13, 3, 16, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 272', '2020-03-05 00:00:00', NULL, NULL),
(121, NULL, '301', '20161031/BTT02/00293', 5, 1, 10, 7, 7, '2020-03-11', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 301', '2020-03-05 00:00:00', NULL, NULL),
(122, NULL, '302', '20161031/BTT02/01743', 5, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'DMS 300-BTT02', 0, 'ECG 302', '2020-03-05 00:00:00', NULL, NULL),
(123, NULL, '303', '20161031/BTT02/01729', 5, 1, 10, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 303', '2020-03-05 00:00:00', NULL, NULL),
(124, NULL, '304', '20140721/BBT02/00290', 5, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 304', '2020-03-05 00:00:00', NULL, NULL),
(125, NULL, '305', 'D1081A0753', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'CONTEC 8000', 0, NULL, '2020-03-05 00:00:00', NULL, NULL),
(126, NULL, '401', 'DG401', 15, 9, 9, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', NULL, 9, 'DG 401', '2020-03-05 00:00:00', NULL, NULL),
(127, NULL, '402', 'DG402', 15, 9, 9, 6, 6, '2020-02-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'EQUIPO DAÑADO', 9, 'DG 402', '2020-03-05 00:00:00', NULL, NULL),
(128, '18a13cd3f35261938.jpg', '403', '180403', 15, 9, 9, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:37:31', NULL, 'felipem', 'Usado también para Uci Fatima&nbsp;', 9, 'DG 403', '2020-03-05 00:00:00', NULL, NULL),
(129, NULL, '404', 'DG404', 15, 9, 9, 4, 4, '2020-02-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 9, 'DG 404', '2020-03-05 00:00:00', NULL, NULL),
(130, NULL, '405', 'DG405', 15, 9, 9, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Estaba con la ubicación de la CSF pero realmente está en uso en HROB. 02-03-2020 Camilo.<div><br></div><div>Se verifica nuevamente y se establece ingreso en HROB 04-03-2020. Felipe&nbsp;</div>', 9, 'DG 405', '2020-03-05 00:00:00', NULL, NULL),
(131, NULL, '406', 'DG406', 15, 9, 9, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en HTU, realmente se encuentra en HROB, pendiente localizar el dispositivo de este registro.<div><br></div><div>Actualizo, el día 17/Marzo/2020 se llevó este equipo desde HROB (Palmira) a FHSJ (Buga).</div>', 9, 'DG 406', '2020-03-05 00:00:00', NULL, NULL),
(132, NULL, '407', '180407', 15, 9, 9, 6, 6, '2018-06-08', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'FUNDACION HOSPITAL SANJOSE, Marcado', 9, 'DG 407', '2020-03-05 00:00:00', NULL, NULL),
(133, NULL, '451', 'B97K125', 4, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 ,  ,', 0, 'DES 451', '2020-03-05 00:00:00', NULL, NULL),
(134, NULL, '452', 'B97K12587', 4, 13, 21, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020.', 21, 'DES 452', '2020-03-05 00:00:00', '2020-07-07 19:26:21', NULL),
(135, NULL, '453', 'B97K12574', 4, 13, 21, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE', 21, 'DES 453', '2020-03-05 00:00:00', NULL, NULL),
(136, NULL, '454', '98278', 4, 4, 27, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Funciona normalmente en el HROB', 27, 'DES 454', '2020-03-05 00:00:00', NULL, NULL),
(137, NULL, '455', '12129094', 4, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER', 0, 'DES 455', '2020-03-05 00:00:00', NULL, NULL),
(138, NULL, '456', '12127290', 4, 4, 19, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'ARTEMA , IP 22 , CALI , HMC', 19, 'DES 456', '2020-03-05 00:00:00', NULL, NULL),
(139, NULL, '457', '05126132', 4, 23, 35, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020 Camilo.', 35, 'DES 457', '2020-03-05 00:00:00', '2020-07-07 19:18:13', NULL),
(140, NULL, '458', '90427', 4, 4, 27, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06-Marzo-2020', 27, 'DES 458', '2020-03-05 00:00:00', NULL, NULL),
(141, NULL, '601', 'A7V87A', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 601', '2020-03-05 00:00:00', NULL, NULL),
(142, NULL, '602', 'HN3F91QC100398', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 602', '2020-03-05 00:00:00', NULL, NULL),
(143, NULL, '603', 'U082MA00', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DELL ,  , INTEGRADO ,', 0, 'PC 603', '2020-03-05 00:00:00', NULL, NULL),
(144, NULL, '604', 'CLON', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'INTEL ,  , JANUS ,', 0, 'PC 604', '2020-03-05 00:00:00', NULL, NULL),
(145, NULL, '606', 'CS02589545', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 606', '2020-03-05 00:00:00', NULL, NULL),
(146, NULL, '607', 'VS81211172', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 607', '2020-03-05 00:00:00', NULL, NULL),
(147, NULL, '608', '6MC2190SSX', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 608', '2020-03-05 00:00:00', NULL, NULL),
(148, NULL, '609', '3CR2221DW9', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 609', '2020-03-05 00:00:00', NULL, NULL),
(149, NULL, '610', 'VS81225546', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 610', '2020-03-05 00:00:00', NULL, NULL),
(150, NULL, '611', '5CB40840JJ', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , INTEGRADO ,', 0, 'PC 611', '2020-03-05 00:00:00', NULL, NULL),
(151, NULL, '612', 'HYXS91LD200746', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 612', '2020-03-05 00:00:00', NULL, NULL),
(152, NULL, '636', '5CM31902QS', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'HP ,  , INTEGRADO ,', 0, 'PC 636', '2020-03-05 00:00:00', NULL, NULL),
(153, NULL, '614', '12052393514', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LG ,  , JONUS ,', 0, 'PC 614', '2020-03-05 00:00:00', NULL, NULL),
(154, NULL, '615', '335255', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , AOPEN ,', 0, 'PC 615', '2020-03-05 00:00:00', NULL, NULL),
(155, NULL, '617', '5CD440256Q', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , HP ,', 0, 'PC 617', '2020-03-05 00:00:00', NULL, NULL),
(156, NULL, '618', 'CS01429411', 11, 10, 11, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO, TODO EN UNO', 11, 'PC 618', '2020-03-05 00:00:00', NULL, NULL),
(157, NULL, '619', 'PF0N6RLY', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NA ,', 0, 'PC 619', '2020-03-05 00:00:00', NULL, NULL),
(158, 'bcdf768ef42798802.jpg', '620', 'CS02593439', 11, 10, 11, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div><div>02-21-2020 se traslada a hidc</div>', 11, 'PC 620', '2020-03-05 00:00:00', NULL, NULL),
(159, NULL, '701', '10-10-10', 7, 14, 22, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PERFECT 10 , PERFECT 10&nbsp;', 22, 'BT 701', '2020-03-05 00:00:00', NULL, NULL),
(160, NULL, '702', '201409026161', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1 ,  ,', 0, 'BT 702', '2020-03-05 00:00:00', NULL, NULL),
(161, NULL, '704', 'SN: 270765 JC', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', ',  ,  ,', 0, 'BT 704', '2020-03-05 00:00:00', NULL, NULL),
(162, NULL, '705', '201312044116', 7, 12, 18, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1', 18, 'BT 705', '2020-03-05 00:00:00', NULL, NULL),
(163, NULL, '706', '201409026192', 7, 12, 18, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1&nbsp;', 18, 'BT 706', '2020-03-05 00:00:00', NULL, NULL),
(164, NULL, '708', '201509043123', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS ,  ,  ,', 0, 'BT 708', '2020-03-05 00:00:00', NULL, NULL),
(165, NULL, '710', '201509043152', 7, 12, 18, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1 , HIDC ,', 18, 'BT 710', '2020-03-05 00:00:00', NULL, NULL),
(166, NULL, '751', '20140625/BTT/01068', 6, 1, 10, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, funciona normalmente en CSF 06/Marzo/2020 Camilo.&nbsp;', 10, 'STR 751', '2020-03-05 00:00:00', NULL, NULL),
(167, NULL, '752', '20140625/BTT/01107', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20140625/BTR/01107 ,  ,', 0, 'STR 752', '2020-03-05 00:00:00', NULL, NULL),
(168, NULL, '753', '20160725/BTT/01304', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01304 ,  ,', 0, 'STR 753', '2020-03-05 00:00:00', NULL, NULL),
(169, NULL, '754', '20160104/BTT/01253', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160104/BTR/01253 ,  ,', 0, 'STR 754', '2020-03-05 00:00:00', NULL, NULL),
(170, NULL, '755', '20141201/BTT/01123', 6, 1, 10, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20141201/BTR/01123', 10, 'STR 755', '2020-03-05 00:00:00', NULL, NULL),
(171, NULL, '756', '20160725/BTT/01307', 6, 1, 10, 9, 9, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>', 10, 'STR 756', '2020-03-05 00:00:00', NULL, NULL),
(172, NULL, '757', '20150129/BTT/01136', 6, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20150129/BTR/01136 ,&nbsp;', 10, 'STR 757', '2020-03-05 00:00:00', NULL, NULL),
(173, NULL, '758', '20160104/BTR/01253', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 758', '2020-03-05 00:00:00', NULL, NULL),
(174, NULL, '759', '4712AU3681E', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'QRSCardUSB , NA ,  ,', 0, 'STR 759', '2020-03-05 00:00:00', NULL, NULL),
(175, NULL, '760', 'SN BTR01-343', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 760', '2020-03-05 00:00:00', NULL, NULL),
(176, NULL, '761', 'SM 311SN: D111E0004', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT03 ,  ,  ,', 0, 'STR 761', '2020-03-05 00:00:00', NULL, NULL),
(177, NULL, '762', 'SN: 20140721/BTT02/00290', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT02 ,  ,  ,', 0, 'STR 762', '2020-03-05 00:00:00', NULL, NULL),
(178, NULL, '763', '20160725/BTT/01299', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01299 ,  ,', 0, 'STR 763', '2020-03-05 00:00:00', NULL, NULL),
(179, NULL, '901', 'SM 10', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , BUGA , HSJ', 0, 'HOL 901', '2020-03-05 00:00:00', NULL, NULL),
(180, NULL, '902', 'SM 11', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , BUGA , HSJ', 0, 'HOL 902', '2020-03-05 00:00:00', NULL, NULL),
(181, NULL, '903', 'SM 903', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , CALI , ADMIN.', NULL, 'HOL 903', '2020-03-05 00:00:00', NULL, NULL),
(182, NULL, '904', 'SM 13', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , TULUA , HTU', 0, 'HOL 904', '2020-03-05 00:00:00', NULL, NULL),
(183, NULL, '905', 'SM 14', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 8 GB , CASA , CASA', 42, 'USB 905', '2020-03-05 00:00:00', NULL, NULL),
(184, NULL, '906', 'SM 15', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA , TECNOLOGIA', 42, 'USB 906', '2020-03-05 00:00:00', NULL, NULL),
(185, NULL, '907', 'SM 16', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 907', '2020-03-05 00:00:00', NULL, NULL),
(186, NULL, '908', 'SM 17', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 908', '2020-03-05 00:00:00', NULL, NULL),
(187, NULL, '909', 'SM 18', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 909', '2020-03-05 00:00:00', NULL, NULL),
(188, NULL, '910', 'SM 19', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 910', '2020-03-05 00:00:00', NULL, NULL),
(189, NULL, '911', 'SM 20', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 911', '2020-03-05 00:00:00', NULL, NULL),
(190, NULL, '912', 'SM 21', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 912', '2020-03-05 00:00:00', NULL, NULL),
(191, NULL, '913', 'SM 22', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 913', '2020-03-05 00:00:00', NULL, NULL),
(192, NULL, '914', 'SM 23', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 914', '2020-03-05 00:00:00', NULL, NULL),
(193, NULL, '915', 'SM24', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 915', '2020-03-05 00:00:00', NULL, NULL),
(194, NULL, '801', 'VND3J28585', 16, 25, 42, 5, 5, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO', 42, 'USB 801', '2020-03-05 00:00:00', NULL, NULL),
(195, NULL, '802', 'CN26RBK063', 16, 25, 42, 3, 3, NULL, 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 802', '2020-03-05 00:00:00', NULL, NULL),
(196, NULL, '803', 'CN22SBTFP', 16, 25, 42, 3, 3, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 803', '2020-03-05 00:00:00', NULL, NULL),
(197, NULL, '804', 'SE8Y011865', 16, 25, 42, 2, 2, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO', 42, 'USB 804', '2020-03-05 00:00:00', NULL, NULL),
(198, NULL, '805', 'SE8Y018164', 16, 25, 42, 3, 3, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Sin verificacion<div><br></div><div><br></div>', 42, 'USB 805', '2020-03-05 00:00:00', NULL, NULL),
(199, 'd7dffdba6db4acaa5.jpg', '134', '3A-11289', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 10:59:48 pm', '0000-00-00 00:00:00', 'felipem', 'superadmin', 'Estuche, Cable<span style=\"font-size: 0.857em;\">, China</span><span style=\"font-size: 0.857em;\">&nbsp;Noviembre 2019, Actualizo</span><span style=\"font-size: 0.857em;\">, funciona normalmente, HTU 06/Marzo/2020, Camilo.</span>', 1, 'HOLTER 134', '2019-12-05 00:00:00', NULL, NULL),
(200, '0517ef6974ab1463b.jpg', '135', '3A-11288', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:01:16 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>', 1, 'HOLTER 135', '2019-12-05 00:00:00', NULL, NULL),
(201, 'c830bd855b5933e47.jpg', '136', '3A-11286', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:06:13 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 136', '2019-12-05 00:00:00', NULL, NULL),
(202, '9cd28935c6a69d0a3.jpg', '137', '3A-11292', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 11:07:54 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 137', '2019-12-05 00:00:00', NULL, NULL),
(203, 'ec24083a0b7fcd5a2.jpg', '138', '3A-11291', 1, 1, 1, 4, 4, '2019-12-09', 1, '5/12/2019 11:08:49 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>', NULL, 'HOLTER 138', '2019-12-05 00:00:00', NULL, NULL),
(204, 'caeae04f26da4d82b.jpg', '139', '3A-11290', 1, 1, 1, 5, 5, '2019-12-05', 1, '5/12/2019 11:09:40 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\"><br></span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo.</span></div>', 1, 'HOLTER 139', '2019-12-05 00:00:00', NULL, NULL),
(205, NULL, 'PRUE', '123456', 10, 7, 6, 7, 7, '2019-12-27', 2, '27/12/2019 09:54:45 am', NULL, 'bodega.felipe', NULL, '<br>', 6, 'CAB PRUE', '2019-12-27 00:00:00', NULL, NULL),
(206, NULL, '590', '19120200001', 2, 2, 12, 9, 9, '2020-11-19', 1, '27/12/2019 01:58:38 pm', '2020-11-19 15:03:15', 'felipem', 'andresl', '<br>', 12, 'MAPA 590', '2019-12-27 00:00:00', NULL, NULL),
(207, NULL, '591', '19120200002', 2, 2, 12, 7, 7, '2019-12-27', 1, '27/12/2019 02:01:17 pm', NULL, 'felipem', NULL, 'Noviembre 2019 China', 12, 'MAPA 591', '2019-12-27 00:00:00', NULL, NULL),
(208, NULL, '592', '19120200010', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 592', '2020-03-05 00:00:00', NULL, NULL),
(209, NULL, '593', '19120200012', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, el equipo se encontraba en bodega, ahora funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 2, 'MAPA 593', '2020-03-05 00:00:00', NULL, NULL),
(210, NULL, '594', '19120200022', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 594', '2020-03-05 00:00:00', NULL, NULL),
(211, NULL, '595', '19120200023', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 595', '2020-03-05 00:00:00', NULL, NULL),
(212, NULL, '596', '19120200027', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 596', '2020-03-05 00:00:00', NULL, NULL),
(213, NULL, '597', '19120200028', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 597', '2020-03-05 00:00:00', NULL, NULL),
(214, NULL, '598', '19120200029', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 598', '2020-03-05 00:00:00', NULL, NULL),
(215, NULL, '599', '19120200030', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 599', '2020-03-05 00:00:00', NULL, NULL),
(216, NULL, '140', '3A-11294', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:01:33 am', NULL, 'felipem', NULL, 'China 5 de 15 enero 2020', 1, 'HOLTER 140', '2020-02-04 00:00:00', NULL, NULL),
(217, NULL, '141', '3A-11300', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:02:23 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 141', '2020-02-04 00:00:00', NULL, NULL),
(218, NULL, '142', '3A-11301', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:03:28 am', NULL, 'felipem', NULL, '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 142', '2020-02-04 00:00:00', NULL, NULL),
(219, NULL, '143', '3A-11302', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:04:38 am', NULL, 'felipem', NULL, '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 143', '2020-02-04 00:00:00', NULL, NULL),
(220, NULL, '144', '3A-11304', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:08:00 am', '2020-11-19 15:00:27', 'felipem', 'andresl', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 144', '2020-02-04 00:00:00', NULL, NULL),
(221, NULL, '408', 'DG408', 15, 9, 9, 4, 4, '2020-02-18', 1, '18/2/2020 10:21:55 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', 'Actualizo, Funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 9, 'DG 408', '2020-02-18 00:00:00', NULL, NULL),
(222, NULL, '409', 'DG409', 15, 9, 9, 6, 6, '2016-02-08', 3, '18/2/2020 10:23:46 pm', NULL, 'felipem', NULL, 'No funciona', 9, 'DG 409', '2020-02-18 00:00:00', NULL, NULL),
(223, NULL, '622', 'MP1FZWXE', 11, 10, 15, 11, 11, '2020-02-18', 1, '19/2/2020 11:54:09 am', NULL, 'felipem', NULL, '<br>', 15, 'PC 622', '2020-02-19 00:00:00', NULL, NULL),
(224, NULL, '410', 'DG410', 15, 9, 9, 5, 5, '2020-02-21', 1, '20/2/2020 05:41:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Equipo comprado de segunda enviado 20-02-2020 por flm para csf.<div>Equipo reemplazado por el DG 414</div>', 9, 'DG 410', '2020-02-20 00:00:00', NULL, NULL),
(225, NULL, '413', 'DG413', 15, 9, 9, 5, 5, '2020-02-26', 1, '26/2/2020 05:13:35 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal', 9, 'DG 413', '2020-02-26 00:00:00', NULL, NULL),
(226, NULL, '411', 'DG411', 15, 9, 9, 1, 1, '2020-02-29', 1, '29/2/2020 10:15:52 am', NULL, 'camiloz', NULL, 'Tenia el codigo DG 407 se le otorga el codigo DG 411, Camilo.<div><br></div>', 9, 'DG 411', '2020-02-29 00:00:00', NULL, NULL),
(227, NULL, '412', 'DG412', 15, 9, 9, 2, 2, '2020-02-29', 1, '29/2/2020 10:32:18 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'PENDIENTE POR CAMBIAR CODIGO DEL 401 AL 412', 9, 'DG 412', '2020-02-29 00:00:00', NULL, NULL),
(228, NULL, '414', 'DG414', 15, 9, 9, 5, 5, '2020-03-04', 3, '4/3/2020 11:09:00 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'El equipo no prende, completamente dañado', 9, 'DG 414', '2020-03-04 00:00:00', NULL, NULL),
(229, NULL, '1001', 'X5NQ011045', 17, 17, 28, 1, 1, '2020-03-05', 1, '5/3/2020 11:16:49 am', NULL, 'felipem', NULL, 'Ingreso nueva 05-03-1989', 28, 'SM 1001', '2020-03-05 00:00:00', NULL, NULL),
(230, NULL, '703', '201509043107', 7, 12, 18, 4, 4, '2020-03-06', 1, '6/3/2020 08:57:47 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Se trajo desde Palmira por Luis García', 18, 'BT 703', '2020-03-06 00:00:00', NULL, NULL),
(231, NULL, '605', '303NDWE33345', 11, 18, 29, 4, 4, '2020-03-06', 1, '6/3/2020 11:23:42 am', NULL, 'camiloz', NULL, '<br>', 29, 'PC 605', '2020-03-06 00:00:00', NULL, NULL),
(232, NULL, '546', 'SM546', 18, 19, 30, 4, 4, '2020-03-06', 1, '6/3/2020 12:15:29 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 30, 'SM 546', '2020-03-06 00:00:00', NULL, NULL),
(233, NULL, '917', 'SM 917', 20, 20, 31, 4, 4, '2020-03-06', 1, '6/3/2020 12:40:16 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', '<br>', 31, 'SM 917', '2020-03-06 00:00:00', NULL, NULL),
(234, NULL, '508', '1078535', 19, 21, 32, 4, 4, '2020-03-06', 1, '6/3/2020 12:52:36 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Actualizado, Funcionamiento normal, 06/Marzo/2020, Camilo.', 32, 'SM 508', '2020-03-06 00:00:00', NULL, NULL),
(235, NULL, '916', 'SM 916', 20, 20, 31, 2, 2, '2020-02-26', 1, '21/3/2020 01:15:27 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', 'Actualizo, el dispositivo funciona normalmente en HMC, 21/03/2020, Camilo.', 31, 'SM 916', '2020-03-21 00:00:00', NULL, NULL),
(236, NULL, '273', '03CX6P', 13, 3, 33, 1, 1, '2020-05-06', 1, '6/5/2020 06:19:31 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 33, 'TR 273', '2020-05-06 00:00:00', NULL, NULL),
(237, NULL, '274', 'US99N01847', 13, 3, 16, 1, 1, '2020-05-07', 1, '7/5/2020 11:57:32 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 16, 'TR 274', '2020-05-07 00:00:00', NULL, NULL),
(238, NULL, '1002', 'IHTD56VD1', 21, 22, 34, 1, 1, '2020-05-08', 1, '8/5/2020 08:54:23 am', NULL, 'felipem', NULL, 'SJUG7749AA', 34, 'CEL 1002', '2020-05-08 00:00:00', NULL, NULL),
(239, NULL, '629', 'CS02781268', 11, 10, 11, 4, 4, '2020-05-22', 1, '22/5/2020 01:27:33 pm', NULL, 'felipem', NULL, '<br>', 11, 'PC 629', '2020-05-22 00:00:00', NULL, NULL),
(240, NULL, '626', 'MP12PNNW', 11, 10, 36, 4, 4, '2020-05-22', 1, '22/5/2020 01:55:36 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Utilizado para ECG', 36, 'PC 626', '2020-05-22 00:00:00', NULL, NULL);
INSERT INTO `dispositivos_backup_20201119230142` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(241, NULL, '635', 'MP189C54', 11, 10, 37, 4, 4, NULL, 1, '22/5/2020 02:53:45 pm', NULL, 'felipem', NULL, '<br>', 37, 'PC 635', '2020-05-22 00:00:00', NULL, NULL),
(242, NULL, '628', '5CM3340JQV', 11, 24, 38, 4, 4, '2020-05-22', 1, '22/5/2020 03:15:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<div>Función: HOLTER Y MAPA</div><div><div><br></div></div>', 38, 'PC 628', '2020-05-22 00:00:00', NULL, NULL),
(243, NULL, '713', 'L1317121', 18, 19, 30, 9, 9, '2019-11-01', 1, NULL, '0000-00-00 00:00:00', 'lgarcia', 'superadmin', 'EQUIPO PROCEDENTE DE HFPS', 30, 'SM 713', '2020-09-28 16:18:07', '2020-09-28 16:10:48', NULL),
(244, NULL, '351', '00267', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 39, 'CAB 351', '2020-10-01 16:40:44', NULL, NULL),
(245, NULL, '352', '00272', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, NULL, 'superadmin', NULL, '<br>', 39, 'CAB 352', '2020-10-01 16:46:00', NULL, NULL),
(246, NULL, '550', '17030100017', 2, 2, 2, 4, 4, '2020-10-08', 1, NULL, NULL, 'superadmin', NULL, '<br>', 2, 'MAPA 550', '2020-10-08 15:08:34', NULL, NULL),
(247, NULL, '613', 'MXL3162BRT', 11, 24, 40, 4, 4, '2020-09-22', 1, NULL, NULL, 'superadmin', NULL, '<br>', 40, 'PC 613', '2020-10-15 12:25:44', NULL, NULL),
(248, NULL, '275', 'US30432955', 13, 3, 45, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, 'Sonda trasesofagica', 45, 'TR 275', '2020-10-15 16:45:56', NULL, NULL),
(249, NULL, '276', 'US99N02863', 13, 3, 16, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 16, 'TR 276', '2020-10-15 16:47:56', NULL, NULL),
(250, NULL, '711', '201606060064', 7, 12, 18, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 18, 'BT 711', '2020-10-15 17:07:24', NULL, NULL),
(251, NULL, '815', 'UKTY002906', 17, 17, 46, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 46, 'SM 815', '2020-10-15 17:41:09', NULL, NULL),
(252, NULL, '816', 'X5E8006892', 17, 17, 47, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 47, 'SM 816', '2020-10-15 17:45:41', NULL, NULL),
(253, NULL, '817', 'SE8Y017621', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 817', '2020-10-15 17:48:07', NULL, NULL),
(254, NULL, '818', 'SE8Y017614', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 818', '2020-10-15 17:50:16', NULL, NULL),
(255, NULL, '145', '145', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 145', '2020-10-15 17:55:00', NULL, NULL),
(256, NULL, '146', '146', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 146', '2020-10-15 17:56:35', NULL, NULL),
(257, NULL, '147', '147', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 147', '2020-10-15 17:57:25', NULL, NULL),
(258, NULL, 'SM', 'OL1019199', 18, 19, 30, 3, 3, '2019-08-19', 1, NULL, NULL, 'lgarcia', NULL, NULL, 30, 'SM SM', '2020-11-09 21:32:07', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20201119231714`
--

CREATE TABLE `dispositivos_backup_20201119231714` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(4) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado2` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado1` datetime DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20201119231714`
--

INSERT INTO `dispositivos_backup_20201119231714` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', '101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span>', 1, 'HOLTER 101', '2019-11-27 00:00:00', NULL, NULL),
(2, '8710a41018a7c4e42.jpg', '102', '3A-11605', 1, 1, 1, 1, 1, '2019-11-27', 1, '27/11/2019 09:40:39 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 1, 'HOLTER 102', '2019-11-27 00:00:00', NULL, NULL),
(3, '6630f469154d3320d.jpg', '103', '3A-8045', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Tenia el serial 3A-8044, pero se cambia a 3A-8045', 1, 'HOLTER 103', '2020-03-05 00:00:00', NULL, NULL),
(4, '796ef58bbcaf815d5.jpg', '104', '3A-11606', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 104', '2020-03-05 00:00:00', NULL, NULL),
(5, 'a1ca49f2da0d3858a.jpg', '105', '3A-11931', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes tenia el 3A-1039', 1, 'HOLTER 105', '2020-03-05 00:00:00', NULL, NULL),
(6, 'fa31345b2dec3f75d.jpg', '106', '3A-11607', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 3a-11934', 1, 'HOLTER 106', '2020-03-05 00:00:00', NULL, NULL),
(7, '62f8f3afd6062cf73.jpg', '107', '3A-11608', 1, 1, 1, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 107', '2020-03-05 00:00:00', NULL, NULL),
(8, '16a714e4f58af1930.jpg', '108', '3A-2151', 1, 1, 1, 6, 6, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial', 1, 'HOLTER 108', '2020-03-05 00:00:00', NULL, NULL),
(9, 'fa647e5ab77632692.jpg', '109', '3A-9421', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 1, 'HOLTER 109', '2020-03-05 00:00:00', NULL, NULL),
(10, '476bc82bf3bed4616.jpg', '110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 110', '2020-03-05 00:00:00', NULL, NULL),
(11, 'a456dd57854fcd34f.jpg', '111', '3A-11609', 1, 1, 1, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 111', '2020-03-05 00:00:00', NULL, NULL),
(12, 'e79c8397c6fa85698.jpg', '112', '3A-12178', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 130 pero es 112&nbsp;', 1, 'HOLTER 112', '2020-03-05 00:00:00', NULL, NULL),
(13, '79766d196e19eb1db.jpg', '113', '3A-9311', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 3A-11948', 1, 'HOLTER 113', '2020-03-05 00:00:00', NULL, NULL),
(14, '1c1558589faf656c3.jpg', '114', '3A-11815', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, se encuentra en FHSJ, funcionamiento normal 04-Marzo-2020, Camilo.', 1, 'HOLTER 114', '2020-03-05 00:00:00', NULL, NULL),
(15, 'a47482f69e6bb1445.jpg', '115', '3A-05295', 1, 1, 1, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo fundido en desfibrilacion según estado de las tarjetas', 1, 'HOLTER 115', '2020-03-05 00:00:00', NULL, NULL),
(16, 'fad32fe2f96f1b6f2.jpg', '116', '3A-8940', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 116', '2020-03-05 00:00:00', NULL, NULL),
(17, 'f6216dd5ac51a2f82.jpg', '117', '3A-9484', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 117', '2020-03-05 00:00:00', NULL, NULL),
(18, '6b9ff54089c40cbf3.jpg', '118', '3A-9486', 1, 1, 1, 4, 4, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 118', '2020-03-05 00:00:00', NULL, NULL),
(19, 'ad6939e30d7026eee.jpg', '119', '3A-9572', 1, 1, 1, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funciona normal en FHSJ 17/Marzo/2020 Camilo.', 1, 'HOLTER 119', '2020-03-05 00:00:00', NULL, NULL),
(20, '636978098692e0616.jpg', '120', '3A-9487', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 120', '2020-03-05 00:00:00', NULL, NULL),
(21, '05353fef66bf34245.jpg', '121', '3A-8946', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal, se cambia la etiqueta del código y del serial 06-Marzo-2020, Camilo.', 1, 'HOLTER 121', '2020-03-05 00:00:00', NULL, NULL),
(22, '753003a921f5f02ec.jpg', '122', '3A-9485', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, se cambió la etiqueta del código y del serial, 06/Marzo/2020.', 1, 'HOLTER 122', '2020-03-05 00:00:00', NULL, NULL),
(23, '5cc88d4b86424bb7d.jpg', '123', '3A-1993', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Antes holter 112, Felipe.<div><br><div>El Holter 123 realmente se encuentra en FHSJ, pendiente revisar el clon en HIDC 04-Marzo-2020, Camilo.</div></div>', 1, 'HOLTER 123', '2020-03-05 00:00:00', NULL, NULL),
(24, 'b89cb8d899578dd8b.jpg', '124', '3A-10318', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', '', 'felipem', 'Aparece con 3a-9311 en csf', 1, 'HOLTER 124', '2020-03-05 00:00:00', NULL, NULL),
(25, '2b4b8d4d10dddbe8b.jpg', '125', '3A-11816', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparece con el serial 3A -7125, el cual es el serial del HOLTER 101, se cambian etiquetas y queda con el serial&nbsp; 3A-11816. 06/Marzo/2020. Camilo', 1, 'HOLTER 125', '2020-03-05 00:00:00', NULL, NULL),
(26, '06b68e149d1227503.jpg', '126', '3A-8044', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes 3A-11818', 1, 'HOLTER 126', '2020-03-05 00:00:00', NULL, NULL),
(27, 'f4964486301b6afe2.jpg', '127', '3A-12175', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 1, 'HOLTER 127', '2020-03-05 00:00:00', NULL, NULL),
(28, 'd179e9e2972b91703.jpg', '128', '3A-12176', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo', 1, 'HOLTER 128', '2020-03-05 00:00:00', NULL, NULL),
(29, '9e12d7e5ca84cd0f4.jpg', '129', '3A-11940', 1, 1, 1, 6, 6, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'HOLTER 129', '2020-03-05 00:00:00', NULL, NULL),
(57, 'c11ec5b51107d1e95.jpg', '130', '3A-11295', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 04:45:35 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 130', '2019-11-29 00:00:00', NULL, NULL),
(31, '3cc42f4af7a84575e.jpg', '201', 'US97804887', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparecía en Bodega, realmente se encuentra en CSF, Funciona normalmente, 06/Marzo/2020, Camilo.', 3, 'ECO 201', '2020-03-05 00:00:00', NULL, NULL),
(32, '536dec1c5345e2b59.jpg', '202', 'US97806979', 3, 3, 3, 6, 6, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 202', '2020-03-05 00:00:00', NULL, NULL),
(33, '2edf9e5e9f0adf0b0.jpg', '203', '3728A01811', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 203', '2020-03-05 00:00:00', NULL, NULL),
(34, 'b35b0af60ed58ef47.jpg', '204', 'US50210876', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente está en HROB. 02/Marzo/2020 Camilo.<div><br></div><div>Actualizo, el día de hoy 17/Marzo/2020 se lleva equipo desde HROB (Palmira) a FHSJ (Buga), Camilo.</div>', 3, 'ECO 204', '2020-03-05 00:00:00', NULL, NULL),
(35, 'd6bb8df0f60c48ab0.jpg', '205', '3728A00382', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 205', '2020-03-05 00:00:00', NULL, NULL),
(36, '620450dbf61c4c19f.jpg', '206', '9708A09160', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Aparecía en HTU, realmente está en Bodega. 05/Marzo/2020. Camilo', 3, 'ECO 206', '2020-03-05 00:00:00', NULL, NULL),
(37, '41736a0baf4cb45e3.jpg', '207', '71674', 3, 6, 5, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 207', '2020-03-05 00:00:00', NULL, NULL),
(38, '0eb9fdc48d520b1a8.jpg', '208', '001838VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 208', '2020-03-05 00:00:00', NULL, NULL),
(39, 'f7c6ea863e2c80a47.jpg', '209', '055133VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 209', '2020-03-05 00:00:00', NULL, NULL),
(40, 'c86621e1a393abd5e.jpg', '210', '3728A01073', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmente en HTU, 21/Marzo/2020, Camilo.', 3, 'ECO 210', '2020-03-05 00:00:00', NULL, NULL),
(41, '158d01f499364579d.jpg', '211', 'USO0211434', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 211', '2020-03-05 00:00:00', NULL, NULL),
(42, '387a137ebf96710b6.jpg', '212', 'US97807671', 3, 3, 3, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 3, 'ECO 212', '2020-03-05 00:00:00', NULL, NULL),
(43, '5ed931c87e7ad07f3.jpg', '213', '9708A04535', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 213', '2020-03-05 00:00:00', NULL, NULL),
(44, 'e624e9cfa97691728.jpg', '214', '3728A00160', 3, 3, 3, 7, 7, '2019-12-04', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'No se encuentra en FHSJ', 3, 'ECO 214', '2020-03-05 00:00:00', NULL, NULL),
(45, '54d451022df8c30e6.jpg', '215', 'US97703545', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 215', '2020-03-05 00:00:00', NULL, NULL),
(46, '0b54a448e7a5a93aa.jpg', '216', 'US97808948', 3, 3, 3, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 3, 'ECO 216', '2020-03-05 00:00:00', NULL, NULL),
(47, '630053d26916342ca.jpg', '217', 'US97800318', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 217', '2020-03-05 00:00:00', NULL, NULL),
(48, '9972d0e70d8ca4ca6.jpg', '218', '3728A02302', 3, 3, 3, 6, 6, '2020-02-20', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizado, se encontraba en UCI río se deja en FHSJ 20-02-2018<div><br></div><div>Actualizo, equipo fuera de servicio, en reparación, 04/Marzo/2020, Camilo.<br><div><br></div></div>', 3, 'ECO 218', '2020-03-05 00:00:00', NULL, NULL),
(49, '2e9dc0bf01f8da61f.jpg', '219', 'US80210739', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 3, 'ECO 219', '2020-03-05 00:00:00', NULL, NULL),
(50, '6e5eaa081a347bc98.jpg', '220', 'US97703205', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'ECO 220', '2020-03-05 00:00:00', NULL, NULL),
(51, '10b8abd62f6418d47.jpg', '221', 'US97804106', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmete en HROB, 02/Marzo/2020, Camilo.', 3, 'ECO 221', '2020-03-05 00:00:00', NULL, NULL),
(52, 'b6efed45a482c8c76.jpg', '222', 'US97804535', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, está en Bodega inactivo 05/Marzo/2020. Camilo', 3, 'ECO 222', '2020-03-05 00:00:00', NULL, NULL),
(53, '326d48038544d0081.jpg', '223', 'US70421029', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente se encuentra en Clínica San Francisco 06/Marzo/2020, Camilo', 3, 'ECO 223', '2020-03-05 00:00:00', NULL, NULL),
(54, '2114d2a34043479d1.jpg', '224', 'US97703258', 3, 3, 3, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:27:42', NULL, 'felipem', NULL, 3, 'ECO 224', '2020-03-05 00:00:00', NULL, NULL),
(55, '99c80bd69f2b7d401.jpg', '225', 'USD0211891', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Verificar código puesto en el equipo&nbsp;', NULL, 'ECO 225', '2020-03-05 00:00:00', NULL, NULL),
(56, 'a4df3e5a8cd507c06.jpg', '621', 'JCN0GR05Z985517', 11, 7, 6, 7, 7, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '0000-00-00 00:00:00', 'vilmao', 'felipem', '<br>', 6, 'PC 621', '2019-11-27 00:00:00', NULL, NULL),
(58, '2fb3b0f1c5f8919a5.jpg', '131', '3A-11287', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 05:06:08 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 131', '2019-11-29 00:00:00', NULL, NULL),
(59, '982c34a6a71597fe5.jpg', '132', '3A-11293', 1, 1, 1, 1, 1, '2019-12-03', 1, '3/12/2019 12:38:32 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 132', '2019-12-03 00:00:00', NULL, NULL),
(60, '5c9c7e2c36eb50b7f.jpg', '133', '3A-11285', 1, 1, 1, 1, 1, '2019-11-29', 1, '3/12/2019 12:41:03 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 133', '2019-12-03 00:00:00', NULL, NULL),
(61, 'f19510876e2eb19cd.jpeg', '551', 'IP309100004', 2, 2, 7, 3, 3, NULL, 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', NULL, 'MAPA 551', '2020-03-05 00:00:00', NULL, NULL),
(62, '1d40088c027c6481a.jpeg', '552', '00087513', 2, 8, 8, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 552', '2020-03-05 00:00:00', NULL, NULL),
(63, NULL, '553', '140206005', 2, 2, 7, 3, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 553', '2020-03-05 00:00:00', NULL, NULL),
(64, NULL, '554', 'IP1602200009', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'flm', 2, 'MAPA 554', '2020-03-05 00:00:00', NULL, NULL),
(65, NULL, '555', 'IP1602200106', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 555', '2020-03-05 00:00:00', NULL, NULL),
(66, NULL, '556', 'IP1605200325', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 556', '2020-03-05 00:00:00', NULL, NULL),
(67, NULL, '557', 'IP1602200057', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en HTU, se coloca nueva etiqueta con código y serial. 06/Marzo/2020, Camilo', 2, 'MAPA 557', '2020-03-05 00:00:00', NULL, NULL),
(68, NULL, '558', 'IP1602200170', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 558', '2020-03-05 00:00:00', NULL, NULL),
(69, NULL, '559', 'IP1602200295', 2, 2, 2, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 559', '2020-03-05 00:00:00', NULL, NULL),
(70, NULL, '560', 'IP1602200047', 2, 2, 2, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 560', '2020-03-05 00:00:00', NULL, NULL),
(71, NULL, '561', 'IP1602200018', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 561', '2020-03-05 00:00:00', NULL, NULL),
(72, '781ea01118ca82192.jpeg', '562', '17030100040', 2, 2, 2, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 562', '2020-03-05 00:00:00', NULL, NULL),
(73, NULL, '564', 'IP1701400086', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 564', '2020-03-05 00:00:00', NULL, NULL),
(74, NULL, '565', 'IP1701400310', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 565', '2020-03-05 00:00:00', NULL, NULL),
(75, NULL, '566', 'IP1701400267', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 566', '2020-03-05 00:00:00', NULL, NULL),
(76, NULL, '567', 'IP1701400116', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 567', '2020-03-05 00:00:00', NULL, NULL),
(77, NULL, '568', 'IP1701400067', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 568', '2020-03-05 00:00:00', NULL, NULL),
(78, 'f0babaaecbf90b169.jpeg', '569', '17030100024', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', 2, 'MAPA 569', '2020-03-05 00:00:00', NULL, NULL),
(79, NULL, '570', '17030100023', 2, 2, 2, 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 570', '2020-03-05 00:00:00', NULL, NULL),
(80, '7cdb97b607ea37f77.jpeg', '571', '17030100042', 2, 2, 2, 1, 1, '2019-12-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Se encuentra en mantenimiento en la Bodega', 2, 'MAPA 571', '2020-03-05 00:00:00', NULL, NULL),
(81, NULL, '572', '17030100190', 2, 2, 2, 2, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 572', '2020-03-05 00:00:00', NULL, NULL),
(82, '3bd9b5e8c99cc1012.jpeg', '573', '17030100192', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 2, 'MAPA 573', '2020-03-05 00:00:00', NULL, NULL),
(83, NULL, '574', '17030100036', 2, 2, 2, 9, 9, '2020-11-19', 1, NULL, '2020-11-19 14:52:16', NULL, 'andresl', NULL, 2, 'MAPA 574', '2020-03-05 00:00:00', NULL, NULL),
(84, NULL, '575', '17030100031', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, Funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 2, 'MAPA 575', '2020-03-05 00:00:00', NULL, NULL),
(85, NULL, '576', '17030100016', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 576', '2020-03-05 00:00:00', NULL, NULL),
(86, NULL, '577', '17030100188', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 577', '2020-03-05 00:00:00', NULL, NULL),
(87, 'dbd1a242bfc1e2cbf.jpeg', '578', '17030100055', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'El dispositivo opera normalmente en el HIDC', 2, 'MAPA 578', '2020-03-05 00:00:00', NULL, NULL),
(88, NULL, '579', '17030100033', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 579', '2020-03-05 00:00:00', NULL, NULL),
(89, NULL, '580', '17030100037', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 580', '2020-03-05 00:00:00', NULL, NULL),
(90, NULL, '581', '17030100029', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '2020-11-19 14:23:27', NULL, 'andresl', '<br>', 2, 'MAPA 581', '2020-03-05 00:00:00', NULL, NULL),
(91, NULL, '582', '17030100030', 2, 2, 2, 4, 4, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 582', '2020-03-05 00:00:00', NULL, NULL),
(92, NULL, '583', '17030100027', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 583', '2020-03-05 00:00:00', NULL, NULL),
(93, NULL, '584', '17030100052', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 584', '2020-03-05 00:00:00', NULL, NULL),
(94, NULL, '585', '17030100032', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'flm', 2, 'MAPA 585', '2020-03-05 00:00:00', NULL, NULL),
(95, NULL, '586', '17030100021', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 586', '2020-03-05 00:00:00', NULL, NULL),
(96, NULL, '587', '17030100041', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 587', '2020-03-05 00:00:00', NULL, NULL),
(97, NULL, '588', '17030100174', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 588', '2020-03-05 00:00:00', NULL, NULL),
(98, NULL, '589', '17030100184', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 589', '2020-03-05 00:00:00', NULL, NULL),
(99, NULL, '251', '404262WX8', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 251', '2020-03-05 00:00:00', NULL, NULL),
(100, NULL, '252', '333407WX5', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 252', '2020-03-05 00:00:00', NULL, NULL),
(101, NULL, '253', 'US97310163', 13, 3, 25, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 25, 'TR 253', '2020-03-05 00:00:00', NULL, NULL),
(102, NULL, '254', 'US99N01008', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , S3', 0, 'TR 254', '2020-03-05 00:00:00', NULL, NULL),
(103, NULL, '255', '02M7J5', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 11-3L', 0, 'TR 255', '2020-03-05 00:00:00', NULL, NULL),
(104, NULL, '256', '03DNL7', 13, 3, 20, 3, 3, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'PHILIPS , IPx-7', 20, 'TR 256', '2020-03-05 00:00:00', NULL, NULL),
(105, NULL, '257', 'US99N01942', 13, 3, 16, 1, 1, '2020-05-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'SIUI , 21311A', 16, 'TR 257', '2020-03-05 00:00:00', NULL, NULL),
(106, NULL, '258', '3709A00256', 13, 3, 17, 2, 2, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 258', '2020-03-05 00:00:00', NULL, NULL),
(107, NULL, '259', 'US97404881', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , T62110', 0, 'TR 259', '2020-03-05 00:00:00', NULL, NULL),
(108, NULL, '260', 'US99300683', 13, 3, 24, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 24, 'TR 260', '2020-03-05 00:00:00', NULL, NULL),
(109, NULL, '261', 'US97302745', 13, 3, 17, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 261', '2020-03-05 00:00:00', NULL, NULL),
(110, NULL, '262', 'US99N04288', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS&nbsp;', 16, 'TR 262', '2020-03-05 00:00:00', NULL, NULL),
(111, NULL, '263', '02M7PY', 13, 3, 20, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:43:01', NULL, 'felipem', 'Actualizo, funciona normalmente en HTU, 06/Marzo/2020. Camilo', 20, 'TR 263', '2020-03-05 00:00:00', NULL, NULL),
(112, NULL, '264', '3630A00395', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21367A', 0, 'TR 264', '2020-03-05 00:00:00', NULL, NULL),
(113, NULL, '265', '18ZHQQ', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21369A', 0, 'TR 265', '2020-03-05 00:00:00', NULL, NULL),
(114, NULL, '266', '02QDYM', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 266', '2020-03-05 00:00:00', NULL, NULL),
(115, NULL, '267', '142585PD9', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 6S-RS', 0, 'TR 267', '2020-03-05 00:00:00', NULL, NULL),
(116, NULL, '268', 'US99300678', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21321A', 0, 'TR 268', '2020-03-05 00:00:00', NULL, NULL),
(117, NULL, '269', 'US99N03966', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILLIPS , 21311A', 16, 'TR 269', '2020-03-05 00:00:00', NULL, NULL),
(118, NULL, '270', 'US97403824', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD ,', 0, 'TR 270', '2020-03-05 00:00:00', NULL, NULL),
(119, NULL, '271', '3716A00792', 13, 3, 26, 7, 7, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Estaba en el HIDC, el dia 4 de Marzo se llevará para el HMC', 26, 'TR 271', '2020-03-05 00:00:00', NULL, NULL),
(120, NULL, '272', 'US99N04681', 13, 3, 16, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 272', '2020-03-05 00:00:00', NULL, NULL),
(121, NULL, '301', '20161031/BTT02/00293', 5, 1, 10, 7, 7, '2020-03-11', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 301', '2020-03-05 00:00:00', NULL, NULL),
(122, NULL, '302', '20161031/BTT02/01743', 5, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'DMS 300-BTT02', 0, 'ECG 302', '2020-03-05 00:00:00', NULL, NULL),
(123, NULL, '303', '20161031/BTT02/01729', 5, 1, 10, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 303', '2020-03-05 00:00:00', NULL, NULL),
(124, NULL, '304', '20140721/BBT02/00290', 5, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 304', '2020-03-05 00:00:00', NULL, NULL),
(125, NULL, '305', 'D1081A0753', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'CONTEC 8000', 0, NULL, '2020-03-05 00:00:00', NULL, NULL),
(126, NULL, '401', 'DG401', 15, 9, 9, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', NULL, 9, 'DG 401', '2020-03-05 00:00:00', NULL, NULL),
(127, NULL, '402', 'DG402', 15, 9, 9, 6, 6, '2020-02-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'EQUIPO DAÑADO', 9, 'DG 402', '2020-03-05 00:00:00', NULL, NULL),
(128, '18a13cd3f35261938.jpg', '403', '180403', 15, 9, 9, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:37:31', NULL, 'felipem', 'Usado también para Uci Fatima&nbsp;', 9, 'DG 403', '2020-03-05 00:00:00', NULL, NULL),
(129, NULL, '404', 'DG404', 15, 9, 9, 4, 4, '2020-02-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 9, 'DG 404', '2020-03-05 00:00:00', NULL, NULL),
(130, NULL, '405', 'DG405', 15, 9, 9, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Estaba con la ubicación de la CSF pero realmente está en uso en HROB. 02-03-2020 Camilo.<div><br></div><div>Se verifica nuevamente y se establece ingreso en HROB 04-03-2020. Felipe&nbsp;</div>', 9, 'DG 405', '2020-03-05 00:00:00', NULL, NULL),
(131, NULL, '406', 'DG406', 15, 9, 9, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en HTU, realmente se encuentra en HROB, pendiente localizar el dispositivo de este registro.<div><br></div><div>Actualizo, el día 17/Marzo/2020 se llevó este equipo desde HROB (Palmira) a FHSJ (Buga).</div>', 9, 'DG 406', '2020-03-05 00:00:00', NULL, NULL),
(132, NULL, '407', '180407', 15, 9, 9, 6, 6, '2018-06-08', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'FUNDACION HOSPITAL SANJOSE, Marcado', 9, 'DG 407', '2020-03-05 00:00:00', NULL, NULL),
(133, NULL, '451', 'B97K125', 4, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 ,  ,', 0, 'DES 451', '2020-03-05 00:00:00', NULL, NULL),
(134, NULL, '452', 'B97K12587', 4, 13, 21, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020.', 21, 'DES 452', '2020-03-05 00:00:00', '2020-07-07 19:26:21', NULL),
(135, NULL, '453', 'B97K12574', 4, 13, 21, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE', 21, 'DES 453', '2020-03-05 00:00:00', NULL, NULL),
(136, NULL, '454', '98278', 4, 4, 27, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Funciona normalmente en el HROB', 27, 'DES 454', '2020-03-05 00:00:00', NULL, NULL),
(137, NULL, '455', '12129094', 4, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER', 0, 'DES 455', '2020-03-05 00:00:00', NULL, NULL),
(138, NULL, '456', '12127290', 4, 4, 19, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'ARTEMA , IP 22 , CALI , HMC', 19, 'DES 456', '2020-03-05 00:00:00', NULL, NULL),
(139, NULL, '457', '05126132', 4, 23, 35, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020 Camilo.', 35, 'DES 457', '2020-03-05 00:00:00', '2020-07-07 19:18:13', NULL),
(140, NULL, '458', '90427', 4, 4, 27, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06-Marzo-2020', 27, 'DES 458', '2020-03-05 00:00:00', NULL, NULL),
(141, NULL, '601', 'A7V87A', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 601', '2020-03-05 00:00:00', NULL, NULL),
(142, NULL, '602', 'HN3F91QC100398', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 602', '2020-03-05 00:00:00', NULL, NULL),
(143, NULL, '603', 'U082MA00', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DELL ,  , INTEGRADO ,', 0, 'PC 603', '2020-03-05 00:00:00', NULL, NULL),
(144, NULL, '604', 'CLON', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'INTEL ,  , JANUS ,', 0, 'PC 604', '2020-03-05 00:00:00', NULL, NULL),
(145, NULL, '606', 'CS02589545', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 606', '2020-03-05 00:00:00', NULL, NULL),
(146, NULL, '607', 'VS81211172', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 607', '2020-03-05 00:00:00', NULL, NULL),
(147, NULL, '608', '6MC2190SSX', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 608', '2020-03-05 00:00:00', NULL, NULL),
(148, NULL, '609', '3CR2221DW9', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 609', '2020-03-05 00:00:00', NULL, NULL),
(149, NULL, '610', 'VS81225546', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 610', '2020-03-05 00:00:00', NULL, NULL),
(150, NULL, '611', '5CB40840JJ', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , INTEGRADO ,', 0, 'PC 611', '2020-03-05 00:00:00', NULL, NULL),
(151, NULL, '612', 'HYXS91LD200746', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 612', '2020-03-05 00:00:00', NULL, NULL),
(152, NULL, '636', '5CM31902QS', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'HP ,  , INTEGRADO ,', 0, 'PC 636', '2020-03-05 00:00:00', NULL, NULL),
(153, NULL, '614', '12052393514', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LG ,  , JONUS ,', 0, 'PC 614', '2020-03-05 00:00:00', NULL, NULL),
(154, NULL, '615', '335255', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , AOPEN ,', 0, 'PC 615', '2020-03-05 00:00:00', NULL, NULL),
(155, NULL, '617', '5CD440256Q', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , HP ,', 0, 'PC 617', '2020-03-05 00:00:00', NULL, NULL),
(156, NULL, '618', 'CS01429411', 11, 10, 11, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO, TODO EN UNO', 11, 'PC 618', '2020-03-05 00:00:00', NULL, NULL),
(157, NULL, '619', 'PF0N6RLY', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NA ,', 0, 'PC 619', '2020-03-05 00:00:00', NULL, NULL),
(158, 'bcdf768ef42798802.jpg', '620', 'CS02593439', 11, 10, 11, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div><div>02-21-2020 se traslada a hidc</div>', 11, 'PC 620', '2020-03-05 00:00:00', NULL, NULL),
(159, NULL, '701', '10-10-10', 7, 14, 22, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PERFECT 10 , PERFECT 10&nbsp;', 22, 'BT 701', '2020-03-05 00:00:00', NULL, NULL),
(160, NULL, '702', '201409026161', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1 ,  ,', 0, 'BT 702', '2020-03-05 00:00:00', NULL, NULL),
(161, NULL, '704', 'SN: 270765 JC', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', ',  ,  ,', 0, 'BT 704', '2020-03-05 00:00:00', NULL, NULL),
(162, NULL, '705', '201312044116', 7, 12, 18, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1', 18, 'BT 705', '2020-03-05 00:00:00', NULL, NULL),
(163, NULL, '706', '201409026192', 7, 12, 18, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1&nbsp;', 18, 'BT 706', '2020-03-05 00:00:00', NULL, NULL),
(164, NULL, '708', '201509043123', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS ,  ,  ,', 0, 'BT 708', '2020-03-05 00:00:00', NULL, NULL),
(165, NULL, '710', '201509043152', 7, 12, 18, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1 , HIDC ,', 18, 'BT 710', '2020-03-05 00:00:00', NULL, NULL),
(166, NULL, '751', '20140625/BTT/01068', 6, 1, 10, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, funciona normalmente en CSF 06/Marzo/2020 Camilo.&nbsp;', 10, 'STR 751', '2020-03-05 00:00:00', NULL, NULL),
(167, NULL, '752', '20140625/BTT/01107', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20140625/BTR/01107 ,  ,', 0, 'STR 752', '2020-03-05 00:00:00', NULL, NULL),
(168, NULL, '753', '20160725/BTT/01304', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01304 ,  ,', 0, 'STR 753', '2020-03-05 00:00:00', NULL, NULL),
(169, NULL, '754', '20160104/BTT/01253', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160104/BTR/01253 ,  ,', 0, 'STR 754', '2020-03-05 00:00:00', NULL, NULL),
(170, NULL, '755', '20141201/BTT/01123', 6, 1, 10, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20141201/BTR/01123', 10, 'STR 755', '2020-03-05 00:00:00', NULL, NULL),
(171, NULL, '756', '20160725/BTT/01307', 6, 1, 10, 9, 9, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>', 10, 'STR 756', '2020-03-05 00:00:00', NULL, NULL),
(172, NULL, '757', '20150129/BTT/01136', 6, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20150129/BTR/01136 ,&nbsp;', 10, 'STR 757', '2020-03-05 00:00:00', NULL, NULL),
(173, NULL, '758', '20160104/BTR/01253', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 758', '2020-03-05 00:00:00', NULL, NULL),
(174, NULL, '759', '4712AU3681E', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'QRSCardUSB , NA ,  ,', 0, 'STR 759', '2020-03-05 00:00:00', NULL, NULL),
(175, NULL, '760', 'SN BTR01-343', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 760', '2020-03-05 00:00:00', NULL, NULL),
(176, NULL, '761', 'SM 311SN: D111E0004', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT03 ,  ,  ,', 0, 'STR 761', '2020-03-05 00:00:00', NULL, NULL),
(177, NULL, '762', 'SN: 20140721/BTT02/00290', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT02 ,  ,  ,', 0, 'STR 762', '2020-03-05 00:00:00', NULL, NULL),
(178, NULL, '763', '20160725/BTT/01299', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01299 ,  ,', 0, 'STR 763', '2020-03-05 00:00:00', NULL, NULL),
(179, NULL, '901', 'SM 10', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , BUGA , HSJ', 0, 'HOL 901', '2020-03-05 00:00:00', NULL, NULL),
(180, NULL, '902', 'SM 11', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , BUGA , HSJ', 0, 'HOL 902', '2020-03-05 00:00:00', NULL, NULL),
(181, NULL, '903', 'SM 903', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , CALI , ADMIN.', NULL, 'HOL 903', '2020-03-05 00:00:00', NULL, NULL),
(182, NULL, '904', 'SM 13', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , TULUA , HTU', 0, 'HOL 904', '2020-03-05 00:00:00', NULL, NULL),
(183, NULL, '905', 'SM 14', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 8 GB , CASA , CASA', 42, 'USB 905', '2020-03-05 00:00:00', NULL, NULL),
(184, NULL, '906', 'SM 15', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA , TECNOLOGIA', 42, 'USB 906', '2020-03-05 00:00:00', NULL, NULL),
(185, NULL, '907', 'SM 16', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 907', '2020-03-05 00:00:00', NULL, NULL),
(186, NULL, '908', 'SM 17', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 908', '2020-03-05 00:00:00', NULL, NULL),
(187, NULL, '909', 'SM 18', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 909', '2020-03-05 00:00:00', NULL, NULL),
(188, NULL, '910', 'SM 19', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 910', '2020-03-05 00:00:00', NULL, NULL),
(189, NULL, '911', 'SM 20', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 911', '2020-03-05 00:00:00', NULL, NULL),
(190, NULL, '912', 'SM 21', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 912', '2020-03-05 00:00:00', NULL, NULL),
(191, NULL, '913', 'SM 22', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 913', '2020-03-05 00:00:00', NULL, NULL),
(192, NULL, '914', 'SM 23', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 914', '2020-03-05 00:00:00', NULL, NULL),
(193, NULL, '915', 'SM24', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 915', '2020-03-05 00:00:00', NULL, NULL),
(194, NULL, '801', 'VND3J28585', 16, 25, 42, 5, 5, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO', 42, 'USB 801', '2020-03-05 00:00:00', NULL, NULL),
(195, NULL, '802', 'CN26RBK063', 16, 25, 42, 3, 3, NULL, 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 802', '2020-03-05 00:00:00', NULL, NULL),
(196, NULL, '803', 'CN22SBTFP', 16, 25, 42, 3, 3, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 803', '2020-03-05 00:00:00', NULL, NULL),
(197, NULL, '804', 'SE8Y011865', 16, 25, 42, 2, 2, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO', 42, 'USB 804', '2020-03-05 00:00:00', NULL, NULL),
(198, NULL, '805', 'SE8Y018164', 16, 25, 42, 3, 3, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Sin verificacion<div><br></div><div><br></div>', 42, 'USB 805', '2020-03-05 00:00:00', NULL, NULL),
(199, 'd7dffdba6db4acaa5.jpg', '134', '3A-11289', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 10:59:48 pm', '0000-00-00 00:00:00', 'felipem', 'superadmin', 'Estuche, Cable<span style=\"font-size: 0.857em;\">, China</span><span style=\"font-size: 0.857em;\">&nbsp;Noviembre 2019, Actualizo</span><span style=\"font-size: 0.857em;\">, funciona normalmente, HTU 06/Marzo/2020, Camilo.</span>', 1, 'HOLTER 134', '2019-12-05 00:00:00', NULL, NULL),
(200, '0517ef6974ab1463b.jpg', '135', '3A-11288', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:01:16 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>', 1, 'HOLTER 135', '2019-12-05 00:00:00', NULL, NULL),
(201, 'c830bd855b5933e47.jpg', '136', '3A-11286', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:06:13 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 136', '2019-12-05 00:00:00', NULL, NULL),
(202, '9cd28935c6a69d0a3.jpg', '137', '3A-11292', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 11:07:54 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 137', '2019-12-05 00:00:00', NULL, NULL),
(203, 'ec24083a0b7fcd5a2.jpg', '138', '3A-11291', 1, 1, 1, 4, 4, '2019-12-09', 1, '5/12/2019 11:08:49 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>', NULL, 'HOLTER 138', '2019-12-05 00:00:00', NULL, NULL),
(204, 'caeae04f26da4d82b.jpg', '139', '3A-11290', 1, 1, 1, 5, 5, '2019-12-05', 1, '5/12/2019 11:09:40 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\"><br></span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo.</span></div>', 1, 'HOLTER 139', '2019-12-05 00:00:00', NULL, NULL),
(205, NULL, 'PRUE', '123456', 10, 7, 6, 7, 7, '2019-12-27', 2, '27/12/2019 09:54:45 am', NULL, 'bodega.felipe', NULL, '<br>', 6, 'CAB PRUE', '2019-12-27 00:00:00', NULL, NULL),
(206, NULL, '590', '19120200001', 2, 2, 12, 9, 9, '2020-11-19', 1, '27/12/2019 01:58:38 pm', '2020-11-19 15:03:15', 'felipem', 'andresl', '<br>', 12, 'MAPA 590', '2019-12-27 00:00:00', NULL, NULL),
(207, NULL, '591', '19120200002', 2, 2, 12, 7, 7, '2019-12-27', 1, '27/12/2019 02:01:17 pm', NULL, 'felipem', NULL, 'Noviembre 2019 China', 12, 'MAPA 591', '2019-12-27 00:00:00', NULL, NULL),
(208, NULL, '592', '19120200010', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 592', '2020-03-05 00:00:00', NULL, NULL),
(209, NULL, '593', '19120200012', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, el equipo se encontraba en bodega, ahora funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 2, 'MAPA 593', '2020-03-05 00:00:00', NULL, NULL),
(210, NULL, '594', '19120200022', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 594', '2020-03-05 00:00:00', NULL, NULL),
(211, NULL, '595', '19120200023', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 595', '2020-03-05 00:00:00', NULL, NULL),
(212, NULL, '596', '19120200027', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 596', '2020-03-05 00:00:00', NULL, NULL),
(213, NULL, '597', '19120200028', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 597', '2020-03-05 00:00:00', NULL, NULL),
(214, NULL, '598', '19120200029', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 598', '2020-03-05 00:00:00', NULL, NULL),
(215, NULL, '599', '19120200030', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 599', '2020-03-05 00:00:00', NULL, NULL),
(216, NULL, '140', '3A-11294', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:01:33 am', NULL, 'felipem', NULL, 'China 5 de 15 enero 2020', 1, 'HOLTER 140', '2020-02-04 00:00:00', NULL, NULL),
(217, NULL, '141', '3A-11300', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:02:23 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 141', '2020-02-04 00:00:00', NULL, NULL),
(218, NULL, '142', '3A-11301', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:03:28 am', NULL, 'felipem', NULL, '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 142', '2020-02-04 00:00:00', NULL, NULL),
(219, NULL, '143', '3A-11302', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:04:38 am', NULL, 'felipem', NULL, '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 143', '2020-02-04 00:00:00', NULL, NULL),
(220, NULL, '144', '3A-11304', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:08:00 am', '2020-11-19 15:00:27', 'felipem', 'andresl', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 144', '2020-02-04 00:00:00', NULL, NULL),
(221, NULL, '408', 'DG408', 15, 9, 9, 4, 4, '2020-02-18', 1, '18/2/2020 10:21:55 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', 'Actualizo, Funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 9, 'DG 408', '2020-02-18 00:00:00', NULL, NULL),
(222, NULL, '409', 'DG409', 15, 9, 9, 6, 6, '2016-02-08', 3, '18/2/2020 10:23:46 pm', NULL, 'felipem', NULL, 'No funciona', 9, 'DG 409', '2020-02-18 00:00:00', NULL, NULL),
(223, NULL, '622', 'MP1FZWXE', 11, 10, 15, 11, 11, '2020-02-18', 1, '19/2/2020 11:54:09 am', NULL, 'felipem', NULL, '<br>', 15, 'PC 622', '2020-02-19 00:00:00', NULL, NULL),
(224, NULL, '410', 'DG410', 15, 9, 9, 5, 5, '2020-02-21', 1, '20/2/2020 05:41:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Equipo comprado de segunda enviado 20-02-2020 por flm para csf.<div>Equipo reemplazado por el DG 414</div>', 9, 'DG 410', '2020-02-20 00:00:00', NULL, NULL),
(225, NULL, '413', 'DG413', 15, 9, 9, 5, 5, '2020-02-26', 1, '26/2/2020 05:13:35 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal', 9, 'DG 413', '2020-02-26 00:00:00', NULL, NULL),
(226, NULL, '411', 'DG411', 15, 9, 9, 1, 1, '2020-02-29', 1, '29/2/2020 10:15:52 am', NULL, 'camiloz', NULL, 'Tenia el codigo DG 407 se le otorga el codigo DG 411, Camilo.<div><br></div>', 9, 'DG 411', '2020-02-29 00:00:00', NULL, NULL),
(227, NULL, '412', 'DG412', 15, 9, 9, 2, 2, '2020-02-29', 1, '29/2/2020 10:32:18 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'PENDIENTE POR CAMBIAR CODIGO DEL 401 AL 412', 9, 'DG 412', '2020-02-29 00:00:00', NULL, NULL),
(228, NULL, '414', 'DG414', 15, 9, 9, 5, 5, '2020-03-04', 3, '4/3/2020 11:09:00 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'El equipo no prende, completamente dañado', 9, 'DG 414', '2020-03-04 00:00:00', NULL, NULL),
(229, NULL, '1001', 'X5NQ011045', 17, 17, 28, 1, 1, '2020-03-05', 1, '5/3/2020 11:16:49 am', NULL, 'felipem', NULL, 'Ingreso nueva 05-03-1989', 28, 'SM 1001', '2020-03-05 00:00:00', NULL, NULL),
(230, NULL, '703', '201509043107', 7, 12, 18, 4, 4, '2020-03-06', 1, '6/3/2020 08:57:47 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Se trajo desde Palmira por Luis García', 18, 'BT 703', '2020-03-06 00:00:00', NULL, NULL),
(231, NULL, '605', '303NDWE33345', 11, 18, 29, 4, 4, '2020-03-06', 1, '6/3/2020 11:23:42 am', NULL, 'camiloz', NULL, '<br>', 29, 'PC 605', '2020-03-06 00:00:00', NULL, NULL),
(232, NULL, '546', 'SM546', 18, 19, 30, 4, 4, '2020-03-06', 1, '6/3/2020 12:15:29 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 30, 'SM 546', '2020-03-06 00:00:00', NULL, NULL),
(233, NULL, '917', 'SM 917', 20, 20, 31, 4, 4, '2020-03-06', 1, '6/3/2020 12:40:16 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', '<br>', 31, 'SM 917', '2020-03-06 00:00:00', NULL, NULL),
(234, NULL, '508', '1078535', 19, 21, 32, 4, 4, '2020-03-06', 1, '6/3/2020 12:52:36 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Actualizado, Funcionamiento normal, 06/Marzo/2020, Camilo.', 32, 'SM 508', '2020-03-06 00:00:00', NULL, NULL),
(235, NULL, '916', 'SM 916', 20, 20, 31, 2, 2, '2020-02-26', 1, '21/3/2020 01:15:27 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', 'Actualizo, el dispositivo funciona normalmente en HMC, 21/03/2020, Camilo.', 31, 'SM 916', '2020-03-21 00:00:00', NULL, NULL),
(236, NULL, '273', '03CX6P', 13, 3, 33, 1, 1, '2020-05-06', 1, '6/5/2020 06:19:31 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 33, 'TR 273', '2020-05-06 00:00:00', NULL, NULL),
(237, NULL, '274', 'US99N01847', 13, 3, 16, 1, 1, '2020-05-07', 1, '7/5/2020 11:57:32 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 16, 'TR 274', '2020-05-07 00:00:00', NULL, NULL),
(238, NULL, '1002', 'IHTD56VD1', 21, 22, 34, 1, 1, '2020-05-08', 1, '8/5/2020 08:54:23 am', NULL, 'felipem', NULL, 'SJUG7749AA', 34, 'CEL 1002', '2020-05-08 00:00:00', NULL, NULL),
(239, NULL, '629', 'CS02781268', 11, 10, 11, 4, 4, '2020-05-22', 1, '22/5/2020 01:27:33 pm', NULL, 'felipem', NULL, '<br>', 11, 'PC 629', '2020-05-22 00:00:00', NULL, NULL),
(240, NULL, '626', 'MP12PNNW', 11, 10, 36, 4, 4, '2020-05-22', 1, '22/5/2020 01:55:36 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Utilizado para ECG', 36, 'PC 626', '2020-05-22 00:00:00', NULL, NULL);
INSERT INTO `dispositivos_backup_20201119231714` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(241, NULL, '635', 'MP189C54', 11, 10, 37, 4, 4, NULL, 1, '22/5/2020 02:53:45 pm', NULL, 'felipem', NULL, '<br>', 37, 'PC 635', '2020-05-22 00:00:00', NULL, NULL),
(242, NULL, '628', '5CM3340JQV', 11, 24, 38, 4, 4, '2020-05-22', 1, '22/5/2020 03:15:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<div>Función: HOLTER Y MAPA</div><div><div><br></div></div>', 38, 'PC 628', '2020-05-22 00:00:00', NULL, NULL),
(243, NULL, '713', 'L1317121', 18, 19, 30, 9, 9, '2019-11-01', 1, NULL, '0000-00-00 00:00:00', 'lgarcia', 'superadmin', 'EQUIPO PROCEDENTE DE HFPS', 30, 'SM 713', '2020-09-28 16:18:07', '2020-09-28 16:10:48', NULL),
(244, NULL, '351', '00267', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 39, 'CAB 351', '2020-10-01 16:40:44', NULL, NULL),
(245, NULL, '352', '00272', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, NULL, 'superadmin', NULL, '<br>', 39, 'CAB 352', '2020-10-01 16:46:00', NULL, NULL),
(246, NULL, '550', '17030100017', 2, 2, 2, 4, 4, '2020-10-08', 1, NULL, NULL, 'superadmin', NULL, '<br>', 2, 'MAPA 550', '2020-10-08 15:08:34', NULL, NULL),
(247, NULL, '613', 'MXL3162BRT', 11, 24, 40, 4, 4, '2020-09-22', 1, NULL, NULL, 'superadmin', NULL, '<br>', 40, 'PC 613', '2020-10-15 12:25:44', NULL, NULL),
(248, NULL, '275', 'US30432955', 13, 3, 45, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, 'Sonda trasesofagica', 45, 'TR 275', '2020-10-15 16:45:56', NULL, NULL),
(249, NULL, '276', 'US99N02863', 13, 3, 16, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 16, 'TR 276', '2020-10-15 16:47:56', NULL, NULL),
(250, NULL, '711', '201606060064', 7, 12, 18, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 18, 'BT 711', '2020-10-15 17:07:24', NULL, NULL),
(251, NULL, '815', 'UKTY002906', 17, 17, 46, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 46, 'SM 815', '2020-10-15 17:41:09', NULL, NULL),
(252, NULL, '816', 'X5E8006892', 17, 17, 47, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 47, 'SM 816', '2020-10-15 17:45:41', NULL, NULL),
(253, NULL, '817', 'SE8Y017621', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 817', '2020-10-15 17:48:07', NULL, NULL),
(254, NULL, '818', 'SE8Y017614', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 818', '2020-10-15 17:50:16', NULL, NULL),
(255, NULL, '145', '145', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 145', '2020-10-15 17:55:00', NULL, NULL),
(256, NULL, '146', '146', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 146', '2020-10-15 17:56:35', NULL, NULL),
(257, NULL, '147', '147', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 147', '2020-10-15 17:57:25', NULL, NULL),
(258, NULL, 'SM', 'OL1019199', 18, 19, 30, 3, 3, '2019-08-19', 1, NULL, NULL, 'lgarcia', NULL, NULL, 30, 'SM SM', '2020-11-09 21:32:07', NULL, NULL),
(259, NULL, '148', '', 1, 0, 0, 0, NULL, NULL, 4, NULL, NULL, NULL, NULL, 'HOLTER', NULL, 'HOLTER 148', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispositivos_backup_20201119233100`
--

CREATE TABLE `dispositivos_backup_20201119233100` (
  `id_dispo` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) DEFAULT NULL,
  `codigo` varchar(4) NOT NULL,
  `serial` varchar(40) NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `ubicacion_abre` int(10) UNSIGNED DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado2` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `foto` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado1` datetime DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `dispositivos_backup_20201119233100`
--

INSERT INTO `dispositivos_backup_20201119233100` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(1, 'a0c7a87f98ff4c423.jpg', '101', '3A-7125', 1, 1, 1, 6, 6, '2018-11-20', 1, '27/11/2019 09:37:37 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span>', 1, 'HOLTER 101', '2019-11-27 00:00:00', NULL, NULL),
(2, '8710a41018a7c4e42.jpg', '102', '3A-11605', 1, 1, 1, 1, 1, '2019-11-27', 1, '27/11/2019 09:40:39 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 1, 'HOLTER 102', '2019-11-27 00:00:00', NULL, NULL),
(3, '6630f469154d3320d.jpg', '103', '3A-8045', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Tenia el serial 3A-8044, pero se cambia a 3A-8045', 1, 'HOLTER 103', '2020-03-05 00:00:00', NULL, NULL),
(4, '796ef58bbcaf815d5.jpg', '104', '3A-11606', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 104', '2020-03-05 00:00:00', NULL, NULL),
(5, 'a1ca49f2da0d3858a.jpg', '105', '3A-11931', 1, 1, 1, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes tenia el 3A-1039', 1, 'HOLTER 105', '2020-03-05 00:00:00', NULL, NULL),
(6, 'fa31345b2dec3f75d.jpg', '106', '3A-11607', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 3a-11934', 1, 'HOLTER 106', '2020-03-05 00:00:00', NULL, NULL),
(7, '62f8f3afd6062cf73.jpg', '107', '3A-11608', 1, 1, 1, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 107', '2020-03-05 00:00:00', NULL, NULL),
(8, '16a714e4f58af1930.jpg', '108', '3A-2151', 1, 1, 1, 6, 6, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece como HOLTER 123, se le debe actualizar a 108 y colocar este serial', 1, 'HOLTER 108', '2020-03-05 00:00:00', NULL, NULL),
(9, 'fa647e5ab77632692.jpg', '109', '3A-9421', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 1, 'HOLTER 109', '2020-03-05 00:00:00', NULL, NULL),
(10, '476bc82bf3bed4616.jpg', '110', '3A-9310', 1, 1, 1, 6, 6, '2018-11-20', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 110', '2020-03-05 00:00:00', NULL, NULL),
(11, 'a456dd57854fcd34f.jpg', '111', '3A-11609', 1, 1, 1, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 111', '2020-03-05 00:00:00', NULL, NULL),
(12, 'e79c8397c6fa85698.jpg', '112', '3A-12178', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 130 pero es 112&nbsp;', 1, 'HOLTER 112', '2020-03-05 00:00:00', NULL, NULL),
(13, '79766d196e19eb1db.jpg', '113', '3A-9311', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparece con 3A-11948', 1, 'HOLTER 113', '2020-03-05 00:00:00', NULL, NULL),
(14, '1c1558589faf656c3.jpg', '114', '3A-11815', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, se encuentra en FHSJ, funcionamiento normal 04-Marzo-2020, Camilo.', 1, 'HOLTER 114', '2020-03-05 00:00:00', NULL, NULL),
(15, 'a47482f69e6bb1445.jpg', '115', '3A-05295', 1, 1, 1, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo fundido en desfibrilacion según estado de las tarjetas', 1, 'HOLTER 115', '2020-03-05 00:00:00', NULL, NULL),
(16, 'fad32fe2f96f1b6f2.jpg', '116', '3A-8940', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.', 1, 'HOLTER 116', '2020-03-05 00:00:00', NULL, NULL),
(17, 'f6216dd5ac51a2f82.jpg', '117', '3A-9484', 1, 1, 1, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 117', '2020-03-05 00:00:00', NULL, NULL),
(18, '6b9ff54089c40cbf3.jpg', '118', '3A-9486', 1, 1, 1, 4, 4, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 118', '2020-03-05 00:00:00', NULL, NULL),
(19, 'ad6939e30d7026eee.jpg', '119', '3A-9572', 1, 1, 1, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Actualizo, funciona normal en FHSJ 17/Marzo/2020 Camilo.', 1, 'HOLTER 119', '2020-03-05 00:00:00', NULL, NULL),
(20, '636978098692e0616.jpg', '120', '3A-9487', 1, 1, 1, 2, 2, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 1, 'HOLTER 120', '2020-03-05 00:00:00', NULL, NULL),
(21, '05353fef66bf34245.jpg', '121', '3A-8946', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal, se cambia la etiqueta del código y del serial 06-Marzo-2020, Camilo.', 1, 'HOLTER 121', '2020-03-05 00:00:00', NULL, NULL),
(22, '753003a921f5f02ec.jpg', '122', '3A-9485', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, se cambió la etiqueta del código y del serial, 06/Marzo/2020.', 1, 'HOLTER 122', '2020-03-05 00:00:00', NULL, NULL),
(23, '5cc88d4b86424bb7d.jpg', '123', '3A-1993', 1, 1, 1, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Antes holter 112, Felipe.<div><br><div>El Holter 123 realmente se encuentra en FHSJ, pendiente revisar el clon en HIDC 04-Marzo-2020, Camilo.</div></div>', 1, 'HOLTER 123', '2020-03-05 00:00:00', NULL, NULL),
(24, 'b89cb8d899578dd8b.jpg', '124', '3A-10318', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', '', 'felipem', 'Aparece con 3a-9311 en csf', 1, 'HOLTER 124', '2020-03-05 00:00:00', NULL, NULL),
(25, '2b4b8d4d10dddbe8b.jpg', '125', '3A-11816', 1, 1, 1, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparece con el serial 3A -7125, el cual es el serial del HOLTER 101, se cambian etiquetas y queda con el serial&nbsp; 3A-11816. 06/Marzo/2020. Camilo', 1, 'HOLTER 125', '2020-03-05 00:00:00', NULL, NULL),
(26, '06b68e149d1227503.jpg', '126', '3A-8044', 1, 1, 1, 13, 13, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Antes 3A-11818', 1, 'HOLTER 126', '2020-03-05 00:00:00', NULL, NULL),
(27, 'f4964486301b6afe2.jpg', '127', '3A-12175', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 1, 'HOLTER 127', '2020-03-05 00:00:00', NULL, NULL),
(28, 'd179e9e2972b91703.jpg', '128', '3A-12176', 1, 1, 1, 5, 5, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo', 1, 'HOLTER 128', '2020-03-05 00:00:00', NULL, NULL),
(29, '9e12d7e5ca84cd0f4.jpg', '129', '3A-11940', 1, 1, 1, 6, 6, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'HOLTER 129', '2020-03-05 00:00:00', NULL, NULL),
(57, 'c11ec5b51107d1e95.jpg', '130', '3A-11295', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 04:45:35 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 130', '2019-11-29 00:00:00', NULL, NULL),
(31, '3cc42f4af7a84575e.jpg', '201', 'US97804887', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Aparecía en Bodega, realmente se encuentra en CSF, Funciona normalmente, 06/Marzo/2020, Camilo.', 3, 'ECO 201', '2020-03-05 00:00:00', NULL, NULL),
(32, '536dec1c5345e2b59.jpg', '202', 'US97806979', 3, 3, 3, 6, 6, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 202', '2020-03-05 00:00:00', NULL, NULL),
(33, '2edf9e5e9f0adf0b0.jpg', '203', '3728A01811', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 203', '2020-03-05 00:00:00', NULL, NULL),
(34, 'b35b0af60ed58ef47.jpg', '204', 'US50210876', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente está en HROB. 02/Marzo/2020 Camilo.<div><br></div><div>Actualizo, el día de hoy 17/Marzo/2020 se lleva equipo desde HROB (Palmira) a FHSJ (Buga), Camilo.</div>', 3, 'ECO 204', '2020-03-05 00:00:00', NULL, NULL),
(35, 'd6bb8df0f60c48ab0.jpg', '205', '3728A00382', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 205', '2020-03-05 00:00:00', NULL, NULL),
(36, '620450dbf61c4c19f.jpg', '206', '9708A09160', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Aparecía en HTU, realmente está en Bodega. 05/Marzo/2020. Camilo', 3, 'ECO 206', '2020-03-05 00:00:00', NULL, NULL),
(37, '41736a0baf4cb45e3.jpg', '207', '71674', 3, 6, 5, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 207', '2020-03-05 00:00:00', NULL, NULL),
(38, '0eb9fdc48d520b1a8.jpg', '208', '001838VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 208', '2020-03-05 00:00:00', NULL, NULL),
(39, 'f7c6ea863e2c80a47.jpg', '209', '055133VI', 3, 5, 4, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 209', '2020-03-05 00:00:00', NULL, NULL),
(40, 'c86621e1a393abd5e.jpg', '210', '3728A01073', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmente en HTU, 21/Marzo/2020, Camilo.', 3, 'ECO 210', '2020-03-05 00:00:00', NULL, NULL),
(41, '158d01f499364579d.jpg', '211', 'USO0211434', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 211', '2020-03-05 00:00:00', NULL, NULL),
(42, '387a137ebf96710b6.jpg', '212', 'US97807671', 3, 3, 3, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 3, 'ECO 212', '2020-03-05 00:00:00', NULL, NULL),
(43, '5ed931c87e7ad07f3.jpg', '213', '9708A04535', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 213', '2020-03-05 00:00:00', NULL, NULL),
(44, 'e624e9cfa97691728.jpg', '214', '3728A00160', 3, 3, 3, 7, 7, '2019-12-04', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'No se encuentra en FHSJ', 3, 'ECO 214', '2020-03-05 00:00:00', NULL, NULL),
(45, '54d451022df8c30e6.jpg', '215', 'US97703545', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 215', '2020-03-05 00:00:00', NULL, NULL),
(46, '0b54a448e7a5a93aa.jpg', '216', 'US97808948', 3, 3, 3, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 3, 'ECO 216', '2020-03-05 00:00:00', NULL, NULL),
(47, '630053d26916342ca.jpg', '217', 'US97800318', 3, 3, 3, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, 'ECO 217', '2020-03-05 00:00:00', NULL, NULL),
(48, '9972d0e70d8ca4ca6.jpg', '218', '3728A02302', 3, 3, 3, 6, 6, '2020-02-20', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizado, se encontraba en UCI río se deja en FHSJ 20-02-2018<div><br></div><div>Actualizo, equipo fuera de servicio, en reparación, 04/Marzo/2020, Camilo.<br><div><br></div></div>', 3, 'ECO 218', '2020-03-05 00:00:00', NULL, NULL),
(49, '2e9dc0bf01f8da61f.jpg', '219', 'US80210739', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 3, 'ECO 219', '2020-03-05 00:00:00', NULL, NULL),
(50, '6e5eaa081a347bc98.jpg', '220', 'US97703205', 3, 3, 3, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'ECO 220', '2020-03-05 00:00:00', NULL, NULL),
(51, '10b8abd62f6418d47.jpg', '221', 'US97804106', 3, 3, 3, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, Funciona normalmete en HROB, 02/Marzo/2020, Camilo.', 3, 'ECO 221', '2020-03-05 00:00:00', NULL, NULL),
(52, 'b6efed45a482c8c76.jpg', '222', 'US97804535', 3, 3, 3, 7, 7, '2020-03-05', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, está en Bodega inactivo 05/Marzo/2020. Camilo', 3, 'ECO 222', '2020-03-05 00:00:00', NULL, NULL),
(53, '326d48038544d0081.jpg', '223', 'US70421029', 3, 3, 3, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en Bodega, realmente se encuentra en Clínica San Francisco 06/Marzo/2020, Camilo', 3, 'ECO 223', '2020-03-05 00:00:00', NULL, NULL),
(54, '2114d2a34043479d1.jpg', '224', 'US97703258', 3, 3, 3, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:27:42', NULL, 'felipem', NULL, 3, 'ECO 224', '2020-03-05 00:00:00', NULL, NULL),
(55, '99c80bd69f2b7d401.jpg', '225', 'USD0211891', 3, 3, 3, 4, 4, '2019-12-05', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Verificar código puesto en el equipo&nbsp;', NULL, 'ECO 225', '2020-03-05 00:00:00', NULL, NULL),
(56, 'a4df3e5a8cd507c06.jpg', '621', 'JCN0GR05Z985517', 11, 7, 6, 7, 7, '2019-08-07', 1, '27/11/2019 06:30:03 pm', '0000-00-00 00:00:00', 'vilmao', 'felipem', '<br>', 6, 'PC 621', '2019-11-27 00:00:00', NULL, NULL),
(58, '2fb3b0f1c5f8919a5.jpg', '131', '3A-11287', 1, 1, 1, 9, 9, '2019-11-29', 1, '29/11/2019 05:06:08 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 131', '2019-11-29 00:00:00', NULL, NULL),
(59, '982c34a6a71597fe5.jpg', '132', '3A-11293', 1, 1, 1, 1, 1, '2019-12-03', 1, '3/12/2019 12:38:32 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Con estuche y cable de paciente<div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 132', '2019-12-03 00:00:00', NULL, NULL),
(60, '5c9c7e2c36eb50b7f.jpg', '133', '3A-11285', 1, 1, 1, 1, 1, '2019-11-29', 1, '3/12/2019 12:41:03 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche&nbsp;<div>Cable paciente&nbsp;</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 133', '2019-12-03 00:00:00', NULL, NULL),
(61, 'f19510876e2eb19cd.jpeg', '551', 'IP309100004', 2, 2, 7, 3, 3, NULL, 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', NULL, 'MAPA 551', '2020-03-05 00:00:00', NULL, NULL),
(62, '1d40088c027c6481a.jpeg', '552', '00087513', 2, 8, 8, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 552', '2020-03-05 00:00:00', NULL, NULL),
(63, NULL, '553', '140206005', 2, 2, 7, 3, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 553', '2020-03-05 00:00:00', NULL, NULL),
(64, NULL, '554', 'IP1602200009', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'flm', 2, 'MAPA 554', '2020-03-05 00:00:00', NULL, NULL),
(65, NULL, '555', 'IP1602200106', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 555', '2020-03-05 00:00:00', NULL, NULL),
(66, NULL, '556', 'IP1605200325', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 556', '2020-03-05 00:00:00', NULL, NULL),
(67, NULL, '557', 'IP1602200057', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en HTU, se coloca nueva etiqueta con código y serial. 06/Marzo/2020, Camilo', 2, 'MAPA 557', '2020-03-05 00:00:00', NULL, NULL),
(68, NULL, '558', 'IP1602200170', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 558', '2020-03-05 00:00:00', NULL, NULL),
(69, NULL, '559', 'IP1602200295', 2, 2, 2, 7, 7, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', NULL, 'MAPA 559', '2020-03-05 00:00:00', NULL, NULL),
(70, NULL, '560', 'IP1602200047', 2, 2, 2, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 560', '2020-03-05 00:00:00', NULL, NULL),
(71, NULL, '561', 'IP1602200018', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 561', '2020-03-05 00:00:00', NULL, NULL),
(72, '781ea01118ca82192.jpeg', '562', '17030100040', 2, 2, 2, 6, 6, '2019-12-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funciona normalmente en FHSJ, 17/Marzo/2020, Camilo.', 2, 'MAPA 562', '2020-03-05 00:00:00', NULL, NULL),
(73, NULL, '564', 'IP1701400086', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 564', '2020-03-05 00:00:00', NULL, NULL),
(74, NULL, '565', 'IP1701400310', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 565', '2020-03-05 00:00:00', NULL, NULL),
(75, NULL, '566', 'IP1701400267', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 566', '2020-03-05 00:00:00', NULL, NULL),
(76, NULL, '567', 'IP1701400116', 2, 2, 2, 5, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 567', '2020-03-05 00:00:00', NULL, NULL),
(77, NULL, '568', 'IP1701400067', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 568', '2020-03-05 00:00:00', NULL, NULL),
(78, 'f0babaaecbf90b169.jpeg', '569', '17030100024', 2, 2, 2, 7, 7, '2019-12-27', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Equipo dañado&nbsp;', 2, 'MAPA 569', '2020-03-05 00:00:00', NULL, NULL),
(79, NULL, '570', '17030100023', 2, 2, 2, 10, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 570', '2020-03-05 00:00:00', NULL, NULL),
(80, '7cdb97b607ea37f77.jpeg', '571', '17030100042', 2, 2, 2, 1, 1, '2019-12-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Se encuentra en mantenimiento en la Bodega', 2, 'MAPA 571', '2020-03-05 00:00:00', NULL, NULL),
(81, NULL, '572', '17030100190', 2, 2, 2, 2, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 572', '2020-03-05 00:00:00', NULL, NULL),
(82, '3bd9b5e8c99cc1012.jpeg', '573', '17030100192', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<br>', 2, 'MAPA 573', '2020-03-05 00:00:00', NULL, NULL),
(83, NULL, '574', '17030100036', 2, 2, 2, 9, 9, '2020-11-19', 1, NULL, '2020-11-19 14:52:16', NULL, 'andresl', NULL, 2, 'MAPA 574', '2020-03-05 00:00:00', NULL, NULL),
(84, NULL, '575', '17030100031', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, Funcionamiento normal CSF, 06/Marzo/2020, Camilo.', 2, 'MAPA 575', '2020-03-05 00:00:00', NULL, NULL),
(85, NULL, '576', '17030100016', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 576', '2020-03-05 00:00:00', NULL, NULL),
(86, NULL, '577', '17030100188', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 577', '2020-03-05 00:00:00', NULL, NULL),
(87, 'dbd1a242bfc1e2cbf.jpeg', '578', '17030100055', 2, 2, 2, 1, 1, '2019-12-03', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'El dispositivo opera normalmente en el HIDC', 2, 'MAPA 578', '2020-03-05 00:00:00', NULL, NULL),
(88, NULL, '579', '17030100033', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 579', '2020-03-05 00:00:00', NULL, NULL),
(89, NULL, '580', '17030100037', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 580', '2020-03-05 00:00:00', NULL, NULL),
(90, NULL, '581', '17030100029', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '2020-11-19 14:23:27', NULL, 'andresl', '<br>', 2, 'MAPA 581', '2020-03-05 00:00:00', NULL, NULL),
(91, NULL, '582', '17030100030', 2, 2, 2, 4, 4, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 582', '2020-03-05 00:00:00', NULL, NULL),
(92, NULL, '583', '17030100027', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 583', '2020-03-05 00:00:00', NULL, NULL),
(93, NULL, '584', '17030100052', 2, 2, 2, 9, 9, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', '<br>', 2, 'MAPA 584', '2020-03-05 00:00:00', NULL, NULL),
(94, NULL, '585', '17030100032', 2, 2, 2, 3, 3, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'flm', 2, 'MAPA 585', '2020-03-05 00:00:00', NULL, NULL),
(95, NULL, '586', '17030100021', 2, 2, 2, 7, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 586', '2020-03-05 00:00:00', NULL, NULL),
(96, NULL, '587', '17030100041', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 587', '2020-03-05 00:00:00', NULL, NULL),
(97, NULL, '588', '17030100174', 2, 2, 2, 4, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 588', '2020-03-05 00:00:00', NULL, NULL),
(98, NULL, '589', '17030100184', 2, 2, 2, 6, 6, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', '<span style=\"font-size: 11.998px;\">Actualizo, funcionamiento normal en FHSJ, 04-Marzo-2020, Camilo.</span><br>', 2, 'MAPA 589', '2020-03-05 00:00:00', NULL, NULL),
(99, NULL, '251', '404262WX8', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 251', '2020-03-05 00:00:00', NULL, NULL),
(100, NULL, '252', '333407WX5', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 3Sc-RS', 0, 'TR 252', '2020-03-05 00:00:00', NULL, NULL),
(101, NULL, '253', 'US97310163', 13, 3, 25, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 25, 'TR 253', '2020-03-05 00:00:00', NULL, NULL),
(102, NULL, '254', 'US99N01008', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , S3', 0, 'TR 254', '2020-03-05 00:00:00', NULL, NULL),
(103, NULL, '255', '02M7J5', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 11-3L', 0, 'TR 255', '2020-03-05 00:00:00', NULL, NULL),
(104, NULL, '256', '03DNL7', 13, 3, 20, 3, 3, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'PHILIPS , IPx-7', 20, 'TR 256', '2020-03-05 00:00:00', NULL, NULL),
(105, NULL, '257', 'US99N01942', 13, 3, 16, 1, 1, '2020-05-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'SIUI , 21311A', 16, 'TR 257', '2020-03-05 00:00:00', NULL, NULL),
(106, NULL, '258', '3709A00256', 13, 3, 17, 2, 2, NULL, 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 258', '2020-03-05 00:00:00', NULL, NULL),
(107, NULL, '259', 'US97404881', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , T62110', 0, 'TR 259', '2020-03-05 00:00:00', NULL, NULL),
(108, NULL, '260', 'US99300683', 13, 3, 24, 7, 7, '2020-03-03', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SE ENCUENTRA EN LA REFORMA', 24, 'TR 260', '2020-03-05 00:00:00', NULL, NULL),
(109, NULL, '261', 'US97302745', 13, 3, 17, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21350A', 17, 'TR 261', '2020-03-05 00:00:00', NULL, NULL),
(110, NULL, '262', 'US99N04288', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS&nbsp;', 16, 'TR 262', '2020-03-05 00:00:00', NULL, NULL),
(111, NULL, '263', '02M7PY', 13, 3, 20, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:43:01', NULL, 'felipem', 'Actualizo, funciona normalmente en HTU, 06/Marzo/2020. Camilo', 20, 'TR 263', '2020-03-05 00:00:00', NULL, NULL),
(112, NULL, '264', '3630A00395', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21367A', 0, 'TR 264', '2020-03-05 00:00:00', NULL, NULL),
(113, NULL, '265', '18ZHQQ', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21369A', 0, 'TR 265', '2020-03-05 00:00:00', NULL, NULL),
(114, NULL, '266', '02QDYM', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 266', '2020-03-05 00:00:00', NULL, NULL),
(115, NULL, '267', '142585PD9', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'GE , 6S-RS', 0, 'TR 267', '2020-03-05 00:00:00', NULL, NULL),
(116, NULL, '268', 'US99300678', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD , 21321A', 0, 'TR 268', '2020-03-05 00:00:00', NULL, NULL),
(117, NULL, '269', 'US99N03966', 13, 3, 16, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILLIPS , 21311A', 16, 'TR 269', '2020-03-05 00:00:00', NULL, NULL),
(118, NULL, '270', 'US97403824', 13, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HEWLETT PACKARD ,', 0, 'TR 270', '2020-03-05 00:00:00', NULL, NULL),
(119, NULL, '271', '3716A00792', 13, 3, 26, 7, 7, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'Estaba en el HIDC, el dia 4 de Marzo se llevará para el HMC', 26, 'TR 271', '2020-03-05 00:00:00', NULL, NULL),
(120, NULL, '272', 'US99N04681', 13, 3, 16, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PHILIPS , 21311A', 16, 'TR 272', '2020-03-05 00:00:00', NULL, NULL),
(121, NULL, '301', '20161031/BTT02/00293', 5, 1, 10, 7, 7, '2020-03-11', 2, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 301', '2020-03-05 00:00:00', NULL, NULL),
(122, NULL, '302', '20161031/BTT02/01743', 5, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'DMS 300-BTT02', 0, 'ECG 302', '2020-03-05 00:00:00', NULL, NULL),
(123, NULL, '303', '20161031/BTT02/01729', 5, 1, 10, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 303', '2020-03-05 00:00:00', NULL, NULL),
(124, NULL, '304', '20140721/BBT02/00290', 5, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT02', 10, 'ECG 304', '2020-03-05 00:00:00', NULL, NULL),
(125, NULL, '305', 'D1081A0753', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'CONTEC 8000', 0, NULL, '2020-03-05 00:00:00', NULL, NULL),
(126, NULL, '401', 'DG401', 15, 9, 9, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', NULL, 9, 'DG 401', '2020-03-05 00:00:00', NULL, NULL),
(127, NULL, '402', 'DG402', 15, 9, 9, 6, 6, '2020-02-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'EQUIPO DAÑADO', 9, 'DG 402', '2020-03-05 00:00:00', NULL, NULL),
(128, '18a13cd3f35261938.jpg', '403', '180403', 15, 9, 9, 9, 9, '2020-11-17', 1, NULL, '2020-11-17 15:37:31', NULL, 'felipem', 'Usado también para Uci Fatima&nbsp;', 9, 'DG 403', '2020-03-05 00:00:00', NULL, NULL),
(129, NULL, '404', 'DG404', 15, 9, 9, 4, 4, '2020-02-07', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 9, 'DG 404', '2020-03-05 00:00:00', NULL, NULL),
(130, NULL, '405', 'DG405', 15, 9, 9, 3, 3, '2020-03-02', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Estaba con la ubicación de la CSF pero realmente está en uso en HROB. 02-03-2020 Camilo.<div><br></div><div>Se verifica nuevamente y se establece ingreso en HROB 04-03-2020. Felipe&nbsp;</div>', 9, 'DG 405', '2020-03-05 00:00:00', NULL, NULL),
(131, NULL, '406', 'DG406', 15, 9, 9, 6, 6, '2020-03-17', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Aparecía en HTU, realmente se encuentra en HROB, pendiente localizar el dispositivo de este registro.<div><br></div><div>Actualizo, el día 17/Marzo/2020 se llevó este equipo desde HROB (Palmira) a FHSJ (Buga).</div>', 9, 'DG 406', '2020-03-05 00:00:00', NULL, NULL),
(132, NULL, '407', '180407', 15, 9, 9, 6, 6, '2018-06-08', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'FUNDACION HOSPITAL SANJOSE, Marcado', 9, 'DG 407', '2020-03-05 00:00:00', NULL, NULL),
(133, NULL, '451', 'B97K125', 4, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 ,  ,', 0, 'DES 451', '2020-03-05 00:00:00', NULL, NULL),
(134, NULL, '452', 'B97K12587', 4, 13, 21, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020.', 21, 'DES 452', '2020-03-05 00:00:00', '2020-07-07 19:26:21', NULL),
(135, NULL, '453', 'B97K12574', 4, 13, 21, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ZOLL , PD 1200 , BUGA , HOSPITAL SAN JOSE', 21, 'DES 453', '2020-03-05 00:00:00', NULL, NULL),
(136, NULL, '454', '98278', 4, 4, 27, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Funciona normalmente en el HROB', 27, 'DES 454', '2020-03-05 00:00:00', NULL, NULL),
(137, NULL, '455', '12129094', 4, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'ARTEMA , IP 22 , SANTANDER , HOSPITAL FRANCISCO DE PAULA SANTANDER', 0, 'DES 455', '2020-03-05 00:00:00', NULL, NULL),
(138, NULL, '456', '12127290', 4, 4, 19, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'ARTEMA , IP 22 , CALI , HMC', 19, 'DES 456', '2020-03-05 00:00:00', NULL, NULL),
(139, NULL, '457', '05126132', 4, 23, 35, 7, 7, '2020-07-07', 3, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'En reparaciòn 07/Julio/2020 Camilo.', 35, 'DES 457', '2020-03-05 00:00:00', '2020-07-07 19:18:13', NULL),
(140, NULL, '458', '90427', 4, 4, 27, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, funcionamiento normal HTU, 06-Marzo-2020', 27, 'DES 458', '2020-03-05 00:00:00', NULL, NULL),
(141, NULL, '601', 'A7V87A', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 601', '2020-03-05 00:00:00', NULL, NULL),
(142, NULL, '602', 'HN3F91QC100398', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 602', '2020-03-05 00:00:00', NULL, NULL),
(143, NULL, '603', 'U082MA00', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DELL ,  , INTEGRADO ,', 0, 'PC 603', '2020-03-05 00:00:00', NULL, NULL),
(144, NULL, '604', 'CLON', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'INTEL ,  , JANUS ,', 0, 'PC 604', '2020-03-05 00:00:00', NULL, NULL),
(145, NULL, '606', 'CS02589545', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 606', '2020-03-05 00:00:00', NULL, NULL),
(146, NULL, '607', 'VS81211172', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 607', '2020-03-05 00:00:00', NULL, NULL),
(147, NULL, '608', '6MC2190SSX', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 608', '2020-03-05 00:00:00', NULL, NULL),
(148, NULL, '609', '3CR2221DW9', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , COMPAQ ,', 0, 'PC 609', '2020-03-05 00:00:00', NULL, NULL),
(149, NULL, '610', 'VS81225546', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , INTEGRADO ,', 0, 'PC 610', '2020-03-05 00:00:00', NULL, NULL),
(150, NULL, '611', '5CB40840JJ', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , INTEGRADO ,', 0, 'PC 611', '2020-03-05 00:00:00', NULL, NULL),
(151, NULL, '612', 'HYXS91LD200746', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SAMSUNG ,  , INTEGRADO ,', 0, 'PC 612', '2020-03-05 00:00:00', NULL, NULL),
(152, NULL, '636', '5CM31902QS', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'HP ,  , INTEGRADO ,', 0, 'PC 636', '2020-03-05 00:00:00', NULL, NULL),
(153, NULL, '614', '12052393514', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LG ,  , JONUS ,', 0, 'PC 614', '2020-03-05 00:00:00', NULL, NULL),
(154, NULL, '615', '335255', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'COMPAQ ,  , AOPEN ,', 0, 'PC 615', '2020-03-05 00:00:00', NULL, NULL),
(155, NULL, '617', '5CD440256Q', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'HP ,  , HP ,', 0, 'PC 617', '2020-03-05 00:00:00', NULL, NULL),
(156, NULL, '618', 'CS01429411', 11, 10, 11, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO, TODO EN UNO', 11, 'PC 618', '2020-03-05 00:00:00', NULL, NULL),
(157, NULL, '619', 'PF0N6RLY', 11, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NA ,', 0, 'PC 619', '2020-03-05 00:00:00', NULL, NULL),
(158, 'bcdf768ef42798802.jpg', '620', 'CS02593439', 11, 10, 11, 1, 1, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'LENOVO ,  , NZ<div>Todo en uno traído de HFPS referencia modelo 10160</div><div>02-21-2020 se traslada a hidc</div>', 11, 'PC 620', '2020-03-05 00:00:00', NULL, NULL),
(159, NULL, '701', '10-10-10', 7, 14, 22, 6, 6, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'PERFECT 10 , PERFECT 10&nbsp;', 22, 'BT 701', '2020-03-05 00:00:00', NULL, NULL),
(160, NULL, '702', '201409026161', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1 ,  ,', 0, 'BT 702', '2020-03-05 00:00:00', NULL, NULL),
(161, NULL, '704', 'SN: 270765 JC', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', ',  ,  ,', 0, 'BT 704', '2020-03-05 00:00:00', NULL, NULL),
(162, NULL, '705', '201312044116', 7, 12, 18, 3, 3, '2020-03-04', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1', 18, 'BT 705', '2020-03-05 00:00:00', NULL, NULL),
(163, NULL, '706', '201409026192', 7, 12, 18, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1&nbsp;', 18, 'BT 706', '2020-03-05 00:00:00', NULL, NULL),
(164, NULL, '708', '201509043123', 7, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS ,  ,  ,', 0, 'BT 708', '2020-03-05 00:00:00', NULL, NULL),
(165, NULL, '710', '201509043152', 7, 12, 18, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'SPORT FITNESS , JS-5000B-1 , HIDC ,', 18, 'BT 710', '2020-03-05 00:00:00', NULL, NULL),
(166, NULL, '751', '20140625/BTT/01068', 6, 1, 10, 5, 5, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Actualizo, funciona normalmente en CSF 06/Marzo/2020 Camilo.&nbsp;', 10, 'STR 751', '2020-03-05 00:00:00', NULL, NULL),
(167, NULL, '752', '20140625/BTT/01107', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20140625/BTR/01107 ,  ,', 0, 'STR 752', '2020-03-05 00:00:00', NULL, NULL),
(168, NULL, '753', '20160725/BTT/01304', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01304 ,  ,', 0, 'STR 753', '2020-03-05 00:00:00', NULL, NULL),
(169, NULL, '754', '20160104/BTT/01253', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160104/BTR/01253 ,  ,', 0, 'STR 754', '2020-03-05 00:00:00', NULL, NULL),
(170, NULL, '755', '20141201/BTT/01123', 6, 1, 10, 2, 2, '2020-02-26', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20141201/BTR/01123', 10, 'STR 755', '2020-03-05 00:00:00', NULL, NULL),
(171, NULL, '756', '20160725/BTT/01307', 6, 1, 10, 9, 9, '2019-12-10', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01307<div>Viene de Santander HFPS 10-12-2019</div>', 10, 'STR 756', '2020-03-05 00:00:00', NULL, NULL),
(172, NULL, '757', '20150129/BTT/01136', 6, 1, 10, 1, 1, '2020-02-29', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20150129/BTR/01136 ,&nbsp;', 10, 'STR 757', '2020-03-05 00:00:00', NULL, NULL),
(173, NULL, '758', '20160104/BTR/01253', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 758', '2020-03-05 00:00:00', NULL, NULL),
(174, NULL, '759', '4712AU3681E', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'QRSCardUSB , NA ,  ,', 0, 'STR 759', '2020-03-05 00:00:00', NULL, NULL),
(175, NULL, '760', 'SN BTR01-343', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 ,  ,  ,', 0, 'STR 760', '2020-03-05 00:00:00', NULL, NULL),
(176, NULL, '761', 'SM 311SN: D111E0004', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT03 ,  ,  ,', 0, 'STR 761', '2020-03-05 00:00:00', NULL, NULL),
(177, NULL, '762', 'SN: 20140721/BTT02/00290', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS300-BTT02 ,  ,  ,', 0, 'STR 762', '2020-03-05 00:00:00', NULL, NULL),
(178, NULL, '763', '20160725/BTT/01299', 6, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'DMS 300-BTT01 , 20160725/BTR/01299 ,  ,', 0, 'STR 763', '2020-03-05 00:00:00', NULL, NULL),
(179, NULL, '901', 'SM 10', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , BUGA , HSJ', 0, 'HOL 901', '2020-03-05 00:00:00', NULL, NULL),
(180, NULL, '902', 'SM 11', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , BUGA , HSJ', 0, 'HOL 902', '2020-03-05 00:00:00', NULL, NULL),
(181, NULL, '903', 'SM 903', 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'KINGSTON , 8 GB , CALI , ADMIN.', NULL, 'HOL 903', '2020-03-05 00:00:00', NULL, NULL),
(182, NULL, '904', 'SM 13', 0, 0, 0, 0, 0, NULL, 0, NULL, '0000-00-00 00:00:00', NULL, 'felipem', 'KINGSTON , 8 GB , TULUA , HTU', 0, 'HOL 904', '2020-03-05 00:00:00', NULL, NULL),
(183, NULL, '905', 'SM 14', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 8 GB , CASA , CASA', 42, 'USB 905', '2020-03-05 00:00:00', NULL, NULL),
(184, NULL, '906', 'SM 15', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA , TECNOLOGIA', 42, 'USB 906', '2020-03-05 00:00:00', NULL, NULL),
(185, NULL, '907', 'SM 16', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 907', '2020-03-05 00:00:00', NULL, NULL),
(186, NULL, '908', 'SM 17', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 908', '2020-03-05 00:00:00', NULL, NULL),
(187, NULL, '909', 'SM 18', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 909', '2020-03-05 00:00:00', NULL, NULL),
(188, NULL, '910', 'SM 19', 16, 25, 42, 6, 6, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , BUGA , HSJ', 42, 'USB 910', '2020-03-05 00:00:00', NULL, NULL),
(189, NULL, '911', 'SM 20', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 911', '2020-03-05 00:00:00', NULL, NULL),
(190, NULL, '912', 'SM 21', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 912', '2020-03-05 00:00:00', NULL, NULL),
(191, NULL, '913', 'SM 22', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 913', '2020-03-05 00:00:00', NULL, NULL),
(192, NULL, '914', 'SM 23', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 914', '2020-03-05 00:00:00', NULL, NULL),
(193, NULL, '915', 'SM24', 16, 25, 42, 7, 7, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'KINGSTON , 16 GB , CASA ,', 42, 'USB 915', '2020-03-05 00:00:00', NULL, NULL),
(194, NULL, '801', 'VND3J28585', 16, 25, 42, 5, 5, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'IMPRESORA HP 1100 DESKJET ,  , TULUA , CLINICA SAN FRANCISCO', 42, 'USB 801', '2020-03-05 00:00:00', NULL, NULL),
(195, NULL, '802', 'CN26RBK063', 16, 25, 42, 3, 3, NULL, 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 802', '2020-03-05 00:00:00', NULL, NULL),
(196, NULL, '803', 'CN22SBTFP', 16, 25, 42, 3, 3, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'OFFICEJET PRO 8600 PLUS ,  , PALMIRA , HOSPITAL RAUL OREJUELA BUENO', 42, 'USB 803', '2020-03-05 00:00:00', NULL, NULL),
(197, NULL, '804', 'SE8Y011865', 16, 25, 42, 2, 2, '2020-10-15', 3, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'EPSON WORKFORCE ,  , CALI , HOSPITAL MARIO CORREA RENGIFO', 42, 'USB 804', '2020-03-05 00:00:00', NULL, NULL),
(198, NULL, '805', 'SE8Y018164', 16, 25, 42, 3, 3, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', 'Sin verificacion<div><br></div><div><br></div>', 42, 'USB 805', '2020-03-05 00:00:00', NULL, NULL),
(199, 'd7dffdba6db4acaa5.jpg', '134', '3A-11289', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 10:59:48 pm', '0000-00-00 00:00:00', 'felipem', 'superadmin', 'Estuche, Cable<span style=\"font-size: 0.857em;\">, China</span><span style=\"font-size: 0.857em;\">&nbsp;Noviembre 2019, Actualizo</span><span style=\"font-size: 0.857em;\">, funciona normalmente, HTU 06/Marzo/2020, Camilo.</span>', 1, 'HOLTER 134', '2019-12-05 00:00:00', NULL, NULL),
(200, '0517ef6974ab1463b.jpg', '135', '3A-11288', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:01:16 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div>China Noviembre 2019</div>', 1, 'HOLTER 135', '2019-12-05 00:00:00', NULL, NULL),
(201, 'c830bd855b5933e47.jpg', '136', '3A-11286', 1, 1, 1, 5, 5, '2019-12-11', 1, '5/12/2019 11:06:13 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', 1, 'HOLTER 136', '2019-12-05 00:00:00', NULL, NULL),
(202, '9cd28935c6a69d0a3.jpg', '137', '3A-11292', 1, 1, 1, 4, 4, '2019-12-05', 1, '5/12/2019 11:07:54 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Estuche<div>Cable</div><div><br></div><div><span style=\"font-size: 11.998px;\">China Noviembre 2019</span><br></div>', NULL, 'HOLTER 137', '2019-12-05 00:00:00', NULL, NULL),
(203, 'ec24083a0b7fcd5a2.jpg', '138', '3A-11291', 1, 1, 1, 4, 4, '2019-12-09', 1, '5/12/2019 11:08:49 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div>', NULL, 'HOLTER 138', '2019-12-05 00:00:00', NULL, NULL),
(204, 'caeae04f26da4d82b.jpg', '139', '3A-11290', 1, 1, 1, 5, 5, '2019-12-05', 1, '5/12/2019 11:09:40 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', '<span style=\"font-size: 11.998px;\">Estuche</span><div style=\"font-size: 11.998px;\">Cable</div><div style=\"font-size: 11.998px;\"><br></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">China Noviembre 2019</span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\"><br></span></div><div style=\"font-size: 11.998px;\"><span style=\"font-size: 11.998px;\">Actualizo, funciona normalmente en CSF, 06/Marzo/2020, Camilo.</span></div>', 1, 'HOLTER 139', '2019-12-05 00:00:00', NULL, NULL),
(205, NULL, 'PRUE', '123456', 10, 7, 6, 7, 7, '2019-12-27', 2, '27/12/2019 09:54:45 am', NULL, 'bodega.felipe', NULL, '<br>', 6, 'CAB PRUE', '2019-12-27 00:00:00', NULL, NULL),
(206, NULL, '590', '19120200001', 2, 2, 12, 9, 9, '2020-11-19', 1, '27/12/2019 01:58:38 pm', '2020-11-19 15:03:15', 'felipem', 'andresl', '<br>', 12, 'MAPA 590', '2019-12-27 00:00:00', NULL, NULL),
(207, NULL, '591', '19120200002', 2, 2, 12, 7, 7, '2019-12-27', 1, '27/12/2019 02:01:17 pm', NULL, 'felipem', NULL, 'Noviembre 2019 China', 12, 'MAPA 591', '2019-12-27 00:00:00', NULL, NULL),
(208, NULL, '592', '19120200010', 2, 2, 2, 4, 4, '2019-12-27', 1, NULL, '0000-00-00 00:00:00', NULL, 'superadmin', '<br>', 2, 'MAPA 592', '2020-03-05 00:00:00', NULL, NULL),
(209, NULL, '593', '19120200012', 2, 2, 2, 4, 4, '2020-03-06', 1, NULL, '0000-00-00 00:00:00', NULL, 'camiloz', 'Actualizo, el equipo se encontraba en bodega, ahora funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 2, 'MAPA 593', '2020-03-05 00:00:00', NULL, NULL),
(210, NULL, '594', '19120200022', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 594', '2020-03-05 00:00:00', NULL, NULL),
(211, NULL, '595', '19120200023', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 595', '2020-03-05 00:00:00', NULL, NULL),
(212, NULL, '596', '19120200027', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 596', '2020-03-05 00:00:00', NULL, NULL),
(213, NULL, '597', '19120200028', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 597', '2020-03-05 00:00:00', NULL, NULL),
(214, NULL, '598', '19120200029', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 598', '2020-03-05 00:00:00', NULL, NULL),
(215, NULL, '599', '19120200030', 2, 2, 2, 7, NULL, '0000-00-00', 1, NULL, NULL, NULL, NULL, NULL, NULL, 'MAPA 599', '2020-03-05 00:00:00', NULL, NULL),
(216, NULL, '140', '3A-11294', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:01:33 am', NULL, 'felipem', NULL, 'China 5 de 15 enero 2020', 1, 'HOLTER 140', '2020-02-04 00:00:00', NULL, NULL),
(217, NULL, '141', '3A-11300', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:02:23 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 141', '2020-02-04 00:00:00', NULL, NULL),
(218, NULL, '142', '3A-11301', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:03:28 am', NULL, 'felipem', NULL, '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 142', '2020-02-04 00:00:00', NULL, NULL),
(219, NULL, '143', '3A-11302', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:04:38 am', NULL, 'felipem', NULL, '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 143', '2020-02-04 00:00:00', NULL, NULL),
(220, NULL, '144', '3A-11304', 1, 1, 1, 9, 9, '2020-02-04', 1, '4/2/2020 11:08:00 am', '2020-11-19 15:00:27', 'felipem', 'andresl', '<span style=\"font-size: 11.998px;\">China 5 de 15 enero 2020</span><br>', 1, 'HOLTER 144', '2020-02-04 00:00:00', NULL, NULL),
(221, NULL, '408', 'DG408', 15, 9, 9, 4, 4, '2020-02-18', 1, '18/2/2020 10:21:55 pm', '0000-00-00 00:00:00', 'felipem', 'camiloz', 'Actualizo, Funciona normalmente en HTU, 06/Marzo/2020, Camilo.', 9, 'DG 408', '2020-02-18 00:00:00', NULL, NULL),
(222, NULL, '409', 'DG409', 15, 9, 9, 6, 6, '2016-02-08', 3, '18/2/2020 10:23:46 pm', NULL, 'felipem', NULL, 'No funciona', 9, 'DG 409', '2020-02-18 00:00:00', NULL, NULL),
(223, NULL, '622', 'MP1FZWXE', 11, 10, 15, 11, 11, '2020-02-18', 1, '19/2/2020 11:54:09 am', NULL, 'felipem', NULL, '<br>', 15, 'PC 622', '2020-02-19 00:00:00', NULL, NULL),
(224, NULL, '410', 'DG410', 15, 9, 9, 5, 5, '2020-02-21', 1, '20/2/2020 05:41:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Equipo comprado de segunda enviado 20-02-2020 por flm para csf.<div>Equipo reemplazado por el DG 414</div>', 9, 'DG 410', '2020-02-20 00:00:00', NULL, NULL),
(225, NULL, '413', 'DG413', 15, 9, 9, 5, 5, '2020-02-26', 1, '26/2/2020 05:13:35 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal', 9, 'DG 413', '2020-02-26 00:00:00', NULL, NULL),
(226, NULL, '411', 'DG411', 15, 9, 9, 1, 1, '2020-02-29', 1, '29/2/2020 10:15:52 am', NULL, 'camiloz', NULL, 'Tenia el codigo DG 407 se le otorga el codigo DG 411, Camilo.<div><br></div>', 9, 'DG 411', '2020-02-29 00:00:00', NULL, NULL),
(227, NULL, '412', 'DG412', 15, 9, 9, 2, 2, '2020-02-29', 1, '29/2/2020 10:32:18 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'PENDIENTE POR CAMBIAR CODIGO DEL 401 AL 412', 9, 'DG 412', '2020-02-29 00:00:00', NULL, NULL),
(228, NULL, '414', 'DG414', 15, 9, 9, 5, 5, '2020-03-04', 3, '4/3/2020 11:09:00 am', '0000-00-00 00:00:00', 'felipem', 'felipem', 'El equipo no prende, completamente dañado', 9, 'DG 414', '2020-03-04 00:00:00', NULL, NULL),
(229, NULL, '1001', 'X5NQ011045', 17, 17, 28, 1, 1, '2020-03-05', 1, '5/3/2020 11:16:49 am', NULL, 'felipem', NULL, 'Ingreso nueva 05-03-1989', 28, 'SM 1001', '2020-03-05 00:00:00', NULL, NULL),
(230, NULL, '703', '201509043107', 7, 12, 18, 4, 4, '2020-03-06', 1, '6/3/2020 08:57:47 am', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Se trajo desde Palmira por Luis García', 18, 'BT 703', '2020-03-06 00:00:00', NULL, NULL),
(231, NULL, '605', '303NDWE33345', 11, 18, 29, 4, 4, '2020-03-06', 1, '6/3/2020 11:23:42 am', NULL, 'camiloz', NULL, '<br>', 29, 'PC 605', '2020-03-06 00:00:00', NULL, NULL),
(232, NULL, '546', 'SM546', 18, 19, 30, 4, 4, '2020-03-06', 1, '6/3/2020 12:15:29 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Funcionamiento normal HTU, 06/Marzo/2020, Camilo.', 30, 'SM 546', '2020-03-06 00:00:00', NULL, NULL),
(233, NULL, '917', 'SM 917', 20, 20, 31, 4, 4, '2020-03-06', 1, '6/3/2020 12:40:16 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', '<br>', 31, 'SM 917', '2020-03-06 00:00:00', NULL, NULL),
(234, NULL, '508', '1078535', 19, 21, 32, 4, 4, '2020-03-06', 1, '6/3/2020 12:52:36 pm', '0000-00-00 00:00:00', 'camiloz', 'camiloz', 'Actualizado, Funcionamiento normal, 06/Marzo/2020, Camilo.', 32, 'SM 508', '2020-03-06 00:00:00', NULL, NULL),
(235, NULL, '916', 'SM 916', 20, 20, 31, 2, 2, '2020-02-26', 1, '21/3/2020 01:15:27 pm', '0000-00-00 00:00:00', 'camiloz', 'felipem', 'Actualizo, el dispositivo funciona normalmente en HMC, 21/03/2020, Camilo.', 31, 'SM 916', '2020-03-21 00:00:00', NULL, NULL),
(236, NULL, '273', '03CX6P', 13, 3, 33, 1, 1, '2020-05-06', 1, '6/5/2020 06:19:31 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 33, 'TR 273', '2020-05-06 00:00:00', NULL, NULL),
(237, NULL, '274', 'US99N01847', 13, 3, 16, 1, 1, '2020-05-07', 1, '7/5/2020 11:57:32 am', '0000-00-00 00:00:00', 'felipem', 'felipem', '<br>', 16, 'TR 274', '2020-05-07 00:00:00', NULL, NULL),
(238, NULL, '1002', 'IHTD56VD1', 21, 22, 34, 1, 1, '2020-05-08', 1, '8/5/2020 08:54:23 am', NULL, 'felipem', NULL, 'SJUG7749AA', 34, 'CEL 1002', '2020-05-08 00:00:00', NULL, NULL),
(239, NULL, '629', 'CS02781268', 11, 10, 11, 4, 4, '2020-05-22', 1, '22/5/2020 01:27:33 pm', NULL, 'felipem', NULL, '<br>', 11, 'PC 629', '2020-05-22 00:00:00', NULL, NULL),
(240, NULL, '626', 'MP12PNNW', 11, 10, 36, 4, 4, '2020-05-22', 1, '22/5/2020 01:55:36 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', 'Utilizado para ECG', 36, 'PC 626', '2020-05-22 00:00:00', NULL, NULL);
INSERT INTO `dispositivos_backup_20201119233100` (`id_dispo`, `imagen`, `codigo`, `serial`, `tipo_dispositivo`, `marca`, `modelo`, `ubicacion`, `ubicacion_abre`, `fecha_ingreso`, `estado`, `creado2`, `editado`, `creado_por`, `editado_por`, `descripcion`, `foto`, `code`, `creado`, `creado1`, `precio`) VALUES
(241, NULL, '635', 'MP189C54', 11, 10, 37, 4, 4, NULL, 1, '22/5/2020 02:53:45 pm', NULL, 'felipem', NULL, '<br>', 37, 'PC 635', '2020-05-22 00:00:00', NULL, NULL),
(242, NULL, '628', '5CM3340JQV', 11, 24, 38, 4, 4, '2020-05-22', 1, '22/5/2020 03:15:52 pm', '0000-00-00 00:00:00', 'felipem', 'felipem', '<div>Función: HOLTER Y MAPA</div><div><div><br></div></div>', 38, 'PC 628', '2020-05-22 00:00:00', NULL, NULL),
(243, NULL, '713', 'L1317121', 18, 19, 30, 9, 9, '2019-11-01', 1, NULL, '0000-00-00 00:00:00', 'lgarcia', 'superadmin', 'EQUIPO PROCEDENTE DE HFPS', 30, 'SM 713', '2020-09-28 16:18:07', '2020-09-28 16:10:48', NULL),
(244, NULL, '351', '00267', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 39, 'CAB 351', '2020-10-01 16:40:44', NULL, NULL),
(245, NULL, '352', '00272', 10, 1, 39, 7, 7, '2020-10-01', 1, NULL, NULL, 'superadmin', NULL, '<br>', 39, 'CAB 352', '2020-10-01 16:46:00', NULL, NULL),
(246, NULL, '550', '17030100017', 2, 2, 2, 4, 4, '2020-10-08', 1, NULL, NULL, 'superadmin', NULL, '<br>', 2, 'MAPA 550', '2020-10-08 15:08:34', NULL, NULL),
(247, NULL, '613', 'MXL3162BRT', 11, 24, 40, 4, 4, '2020-09-22', 1, NULL, NULL, 'superadmin', NULL, '<br>', 40, 'PC 613', '2020-10-15 12:25:44', NULL, NULL),
(248, NULL, '275', 'US30432955', 13, 3, 45, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, 'Sonda trasesofagica', 45, 'TR 275', '2020-10-15 16:45:56', NULL, NULL),
(249, NULL, '276', 'US99N02863', 13, 3, 16, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 16, 'TR 276', '2020-10-15 16:47:56', NULL, NULL),
(250, NULL, '711', '201606060064', 7, 12, 18, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 18, 'BT 711', '2020-10-15 17:07:24', NULL, NULL),
(251, NULL, '815', 'UKTY002906', 17, 17, 46, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 46, 'SM 815', '2020-10-15 17:41:09', NULL, NULL),
(252, NULL, '816', 'X5E8006892', 17, 17, 47, 4, 4, '2020-10-15', 1, NULL, NULL, 'superadmin', NULL, '<br>', 47, 'SM 816', '2020-10-15 17:45:41', NULL, NULL),
(253, NULL, '817', 'SE8Y017621', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 817', '2020-10-15 17:48:07', NULL, NULL),
(254, NULL, '818', 'SE8Y017614', 17, 17, 41, 4, 4, '2020-10-15', 1, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', '<br>', 41, 'SM 818', '2020-10-15 17:50:16', NULL, NULL),
(255, NULL, '145', '145', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 145', '2020-10-15 17:55:00', NULL, NULL),
(256, NULL, '146', '146', 23, 26, 44, 7, 7, NULL, 4, NULL, '0000-00-00 00:00:00', 'superadmin', 'superadmin', 'HOLTER', 44, 'LIBRE 146', '2020-10-15 17:56:35', NULL, NULL),
(257, NULL, '147', '147', 23, 26, 44, 7, 7, '2020-11-19', 4, NULL, '2020-11-19 23:25:57', 'superadmin', 'felipem', 'HOLTER', 44, 'LIBRE 147', '2020-10-15 17:57:25', NULL, NULL),
(258, NULL, 'SM', 'OL1019199', 18, 19, 30, 3, 3, '2019-08-19', 1, NULL, NULL, 'lgarcia', NULL, NULL, 30, 'SM SM', '2020-11-09 21:32:07', NULL, NULL),
(311, NULL, '199', 'HOLTER', 1, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, 'HOLTER', NULL, 'HOLTER 148', NULL, NULL, NULL),
(336, NULL, '250', 'ECO', 3, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(360, NULL, '300', 'TRANSDUCTOR', 13, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(405, NULL, '350', 'ECG', 5, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(453, NULL, '400', 'CABLE', 10, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(489, NULL, '450', 'DIGITALIZADOR', 15, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(531, NULL, '500', 'DESFIBRILADOR', 4, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(555, NULL, '525', 'BOMBA DE INFUSION', 19, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(580, NULL, '600', 'ASPIRADOR', 18, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(654, NULL, '700', 'COMPUTADOR', 11, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(694, NULL, '750', 'BANDA TROTADORA', 7, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(731, NULL, '800', 'STRESS TEST', 6, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(822, NULL, '900', 'IMPRESORA', 17, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(904, NULL, '999', 'MEMORIA USB', 16, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1003, NULL, '1100', 'DISPONIBLE', 23, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(953, NULL, '1050', 'TELEFONO CELULAR', 21, 26, 44, 16, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispo_detalles_tecnicos`
--

CREATE TABLE `dispo_detalles_tecnicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_dispo` int(10) UNSIGNED DEFAULT NULL,
  `detalles` text,
  `creado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dispo_fuera`
--

CREATE TABLE `dispo_fuera` (
  `id` int(10) UNSIGNED NOT NULL,
  `img` int(10) UNSIGNED DEFAULT NULL,
  `codigo` int(10) UNSIGNED DEFAULT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `ubicacion_final` int(10) UNSIGNED DEFAULT NULL,
  `tipo_fuera_servicio` int(10) UNSIGNED NOT NULL,
  `fecha_reporte` date NOT NULL,
  `descripcion` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `edtado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id` int(10) UNSIGNED NOT NULL,
  `dispo_codigo` int(10) UNSIGNED NOT NULL,
  `dispo_serial` int(10) UNSIGNED DEFAULT NULL,
  `tipo_docu` int(10) UNSIGNED NOT NULL,
  `carga` varchar(40) NOT NULL,
  `descripcion` text NOT NULL,
  `fecha_carga` varchar(40) DEFAULT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `creado1` datetime NOT NULL,
  `enlace` text,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field13` varchar(40) DEFAULT NULL,
  `field14` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id`, `dispo_codigo`, `dispo_serial`, `tipo_docu`, `carga`, `descripcion`, `fecha_carga`, `timestamp`, `update`, `creado_por`, `editado_por`, `creado1`, `enlace`, `creado`, `editado`, `field13`, `field14`) VALUES
(1, 56, 56, 1, 'carta_la_capellana.docx', 'Factura', '27/11/2019', '1574898656', NULL, 'vilmao', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL),
(2, 243, 243, 2, '-HOJA_DE_VIDA-ASPIRADOR.pdf', '-', '28/9/2020', '1601328288', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-09-28 16:24:48', NULL, NULL, NULL),
(3, 232, 232, 2, 'HOJA_DE_VIDA_ASPIRADOR.pdf', 'HOJA DE VIDA DE  EQUIPO BIOMEDICO', '6/11/2020', '1604681771', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-06 11:56:11', NULL, NULL, NULL),
(4, 232, 232, 1, 'FACTURA_DE_COMPRA.pdf', 'EQUIPO COMPRADO A CEMECO', '6/11/2020', '1604681831', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-06 11:57:11', NULL, NULL, NULL),
(5, 232, 232, 5, 'DIAN.pdf', 'DIAN', '6/11/2020', '1604681868', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-06 11:57:48', NULL, NULL, NULL),
(6, 232, 232, 3, 'REGISTRO_INVIMA.pdf', 'INVIMA', '6/11/2020', '1604681923', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-06 11:58:43', NULL, NULL, NULL),
(8, 250, 250, 2, 'BANDA_CAMINADORA.pdf', 'HOJA DE VIDA', '8/11/2020', '1604869104', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-08 15:58:24', NULL, NULL, NULL),
(9, 258, 258, 1, 'FACTURA.pdf', 'FACTURA DE COMPRA', '9/11/2020', '1604975736', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-09 21:35:36', NULL, NULL, NULL),
(10, 258, 258, 11, 'ACTA_DE_ENTREGA.pdf', 'ACTA', '9/11/2020', '1604975972', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-09 21:39:32', NULL, NULL, NULL),
(11, 258, 258, 3, 'REGISTRO_INVIMA.pdf', 'RS', '9/11/2020', '1604976015', '9/11/2020 09:42:26 pm', 'lgarcia', 'lgarcia', '0000-00-00 00:00:00', NULL, '2020-11-09 21:40:15', '2020-11-09 21:42:26', NULL, NULL),
(12, 258, 258, 12, 'GARANTIA.pdf', 'GARANTIA POR UN AÑO', '9/11/2020', '1604976310', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-09 21:45:10', NULL, NULL, NULL),
(13, 258, 258, 5, 'DIAN.pdf', 'DIAN', '9/11/2020', '1604976400', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-09 21:46:40', NULL, NULL, NULL),
(14, 258, 258, 2, 'HOJA_DE_VIDA_ASPIRADOR.pdf', 'HOJA DE VIDA', '9/11/2020', '1604976506', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', NULL, '2020-11-09 21:48:26', NULL, NULL, NULL),
(22, 242, 242, 2, '', 'EQUIPO:&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">HP 18 All in One</span><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">MODEL: </span><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">18-1311la</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">S/N: </span><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">5CM3340JQV</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC - 628</span></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1isTQkVAmpEfx8fN7iDOWgc1TcY69gZXrPWPDSilIR3Q/edit#gid=994663647', '2021-02-23 12:12:51', NULL, NULL, NULL),
(16, 587, 587, 2, '', '<google-sheets-html-origin style=\"color: rgb(0, 0, 0); font-size: medium;\"><br><span style=\"font-family: Arial; font-size: 13.3333px; text-align: center;\">COMPUTADOR: HP&nbsp;</span><span style=\"font-family: Arial; font-size: 13.3333px; text-align: center;\">COMPAQ ELITE 8300</span></google-sheets-html-origin><div><google-sheets-html-origin style=\"color: rgb(0, 0, 0); font-size: medium;\"><span style=\"font-family: Arial; font-size: 13.3333px; text-align: center;\">S/N:&nbsp;</span></google-sheets-html-origin><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">303NDWE33346</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">HTU</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC - 631</span></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1JK58LY74b9IjR1lvEjQngww8G7Un6mDyKsgg65I77VQ/edit#gid=994663647', '2021-02-23 11:43:21', NULL, NULL, NULL),
(17, 585, 585, 2, '', 'COMPUTADOR:&nbsp;DELL&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">OPTIPLEX 3010</span><div>S/N:&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">43329862585</span></div><div>PC - 627</div>', '23/2/2021', NULL, NULL, 'a.lopez', 'a.lopez', '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1WcMe_h_HpxVc4QtwAEiIriY4_zJOs1mCwYHJxeD2eQk/edit#gid=994663647', '2021-02-23 11:48:42', '2021-02-23 11:49:41', NULL, NULL),
(18, 586, 586, 2, '', 'EQUIPO: HP COMPAQ ELITE 8300<div><div>S/N:&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">43229868586</span></div></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC- 630</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\"><br></span></div>', '23/2/2021', NULL, NULL, 'a.lopez', 'a.lopez', '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1xqHbTWejXI-ST9b9Zh4ziXN0ziMe3-pkwEWMgD9G0Ew/edit#gid=994663647', '2021-02-23 11:53:37', '2021-02-23 11:57:20', NULL, NULL),
(19, 654, 654, 2, '', 'EQUIPO: LENOVO OPTIPLEX 3010<div>S/N:&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">41455837477</span></div><div><span style=\"font-size: 13px; white-space: pre-wrap; color: rgb(0, 0, 0); font-family: Arial; text-align: center;\">PC - 700</span></div><div><div><br></div></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1tHpRAlRvNiUP9T-SwEuSDF1q_w7dZsKdkI8v93li5Vo/edit#gid=994663647', '2021-02-23 12:00:32', NULL, NULL, NULL),
(20, 231, 231, 2, '', 'EQUIPO: LG&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">19EN33SA </span><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">S/N: </span><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">303NDWE33345</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC - 605</span></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', NULL, '2021-02-23 12:04:55', NULL, NULL, NULL),
(21, 240, 240, 2, '', 'EQUIPO: LENOVO 80MJ<div>S/N:&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">MP12PNNW</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC - 626</span></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1OU2HnovnV8VxauDhoA8Cu2vI2JtYtcyD7-ly4x_T4k4/edit#gid=994663647', '2021-02-23 12:09:45', NULL, NULL, NULL),
(23, 241, 241, 2, '', 'EQUIPO: LENOVO 80UC<div>S/N:&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">MP189C54</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC - 635</span></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1g3JQ3hFk39rBq-w9FjMTiT8juyQeR8OpFKUjor05CDw/edit#gid=994663647', '2021-02-23 12:15:24', NULL, NULL, NULL),
(24, 239, 239, 2, '', 'EQUIPO: LENOVO 6260<div>S/N&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">CS02781268</span></div><div><span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; text-align: center; white-space: pre-wrap;\">PC - 629</span></div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1ynw0-D5rRPXhyhVPHmzVKRb-zS5HZU2jjGezplTcV70/edit#gid=994663647', '2021-02-23 12:18:09', NULL, NULL, NULL),
(25, 247, 247, 2, '', 'EQUIPO: LENOVO COMPAQ ELITE 8300<div>S/N&nbsp;<span style=\"color: rgb(0, 0, 0); font-family: Arial; font-size: 10pt; text-align: center;\">MXL3162BRT</span></div><div>PC - 613</div>', '23/2/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/13VJDenAxTYK3_oPU7rrIg84oOuvZVN6VlpW4PCafeYk/edit#gid=994663647', '2021-02-23 12:20:22', NULL, NULL, NULL),
(26, 26, 26, 2, '', 'HOLTER - 126<div>S/N 3A-8044</div><div>MEDE</div>', '1/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://drive.google.com/file/d/16O2DfGBMd9Tt58Pn-z3RXBpUHhXJAbH5/view', '2021-03-01 09:53:49', NULL, NULL, NULL),
(27, 9, 9, 2, '', 'HOLTER 109<div>S/N 3A-9421</div><div>MEDE</div>', '1/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1ohBM3A8o46lKosOXCgddtrRy0mFSmGSG/edit?rtpof=true#gid=758493639', '2021-03-01 10:04:21', NULL, NULL, NULL),
(28, 23, 23, 2, '', 'HOLTER 123<div>S/N 3A-1993</div><div>MEDE</div>', '1/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/131AjbGXTP4DCHDvcVpz_zzJ0u5O51QxO/edit#gid=758493639', '2021-03-01 10:10:19', NULL, NULL, NULL),
(29, 59, 59, 2, '', 'HOLTER 132<div>S/N 3A-11293</div><div>MEDE</div>', '1/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1dqVhYyQsN7-zaj7OthSc-6cSbkemfVCk/edit#gid=758493639', '2021-03-01 10:19:27', NULL, NULL, NULL),
(30, 2, 2, 2, '', 'HOLTER 102<div>S/N 11605</div><div>MEDE</div>', '1/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1fjWPmFt5cP77r077dtTw5skDx9uCBJXu/edit#gid=758493639', '2021-03-01 10:25:22', NULL, NULL, NULL),
(31, 679, 679, 2, '', 'BANDA DE TROTE&nbsp;<div>BT-735</div><div>S/N&nbsp;<span style=\"background-color: rgb(255, 217, 102); color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; white-space: pre-wrap;\">20140118</span></div><div><span style=\"background-color: rgb(255, 217, 102); color: rgb(0, 0, 0); font-family: Arial; font-size: 13px; white-space: pre-wrap;\">MEDE</span></div>', '1/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1INW71QYKI2M1JStEsGYzz97G1iRZsT_IQnAjqJ-ATqw/edit#gid=370685471', '2021-03-01 13:57:25', NULL, NULL, NULL),
(33, 92, 92, 2, '', 'MAPA 583<div>S/N 17030100027</div><div>CIDIGO MAPA- 583</div>', '3/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1eI-WdD2M30-OQ5fJbFwZZQ_4kx_7Dvpf/edit#gid=1298647118', '2021-03-03 16:54:38', NULL, NULL, NULL),
(34, 3, 3, 2, '', 'holter-103<div><div>S/N 3A-8045</div></div><div>EN LA SEDE HROB</div>', '4/3/2021', NULL, NULL, 'a.lopez', NULL, '0000-00-00 00:00:00', 'https://docs.google.com/spreadsheets/d/1kcMdjAuaAlJ9ZmxGYDxSOPh8bueTtSIV3DovgwrDD1M/edit#gid=370685471', '2021-03-04 11:34:18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docu_modelo`
--

CREATE TABLE `docu_modelo` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL,
  `marca` int(10) UNSIGNED DEFAULT NULL,
  `modelo` int(10) UNSIGNED DEFAULT NULL,
  `tipo_documento` int(10) UNSIGNED DEFAULT NULL,
  `documento` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `enlace` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_dispo` int(10) UNSIGNED DEFAULT NULL,
  `no_factura` varchar(40) NOT NULL,
  `fecha_factura` varchar(40) NOT NULL,
  `proveedor` int(10) UNSIGNED DEFAULT NULL,
  `identificacion` int(10) UNSIGNED DEFAULT NULL,
  `enlace_doc` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fichatecnica`
--

CREATE TABLE `fichatecnica` (
  `id` int(10) UNSIGNED NOT NULL,
  `dispo_codigo` int(10) UNSIGNED NOT NULL,
  `dispo_serial` int(10) UNSIGNED DEFAULT NULL,
  `hardware` text,
  `software` text,
  `enlace` text,
  `carga` varchar(40) DEFAULT NULL,
  `fecha_carga` date DEFAULT NULL,
  `creado` timestamp NULL DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fichatecnica`
--

INSERT INTO `fichatecnica` (`id`, `dispo_codigo`, `dispo_serial`, `hardware`, `software`, `enlace`, `carga`, `fecha_carga`, `creado`, `creado_por`, `editado`, `editado_por`) VALUES
(1, 242, 242, '<span style=\"font-size: 11.998px;\">Sistema Operativo:&nbsp;Windows 7 Ultimate 64-bit (6.1, Build 7601) Service</span><div style=\"font-size: 11.998px;\">Memoria RAM: 4G</div><div style=\"font-size: 11.998px;\">Procesador:&nbsp;AMD E1-1500 APU with Radeon(tm) HD Graphics (2 CPUs), ~1.5GHz</div><div style=\"font-size: 11.998px;\">Disco Duro: 1TB</div>', '<div style=\"\">Office 365<br>CardioScan<br>ABPM50<br></div>', NULL, NULL, '2020-06-24', '2020-06-25 01:37:35', 'felipem', '2020-06-24 20:38:32', 'felipem');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiero`
--

CREATE TABLE `financiero` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_dispo` int(10) UNSIGNED DEFAULT NULL,
  `precio` decimal(10,0) NOT NULL,
  `unidad` int(10) UNSIGNED DEFAULT NULL,
  `enlace` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `financiero`
--

INSERT INTO `financiero` (`id`, `id_dispo`, `precio`, `unidad`, `enlace`, `creado`, `creado_por`, `editado`, `editado_por`) VALUES
(1, 1007, 550000, 1007, NULL, '2020-12-01 17:47:37', 'felipem', NULL, NULL),
(2, 23, 300000, 23, 'TARJETA MADRE XX1', '2021-02-19 10:03:14', 'felipem', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `financiero_mobiliario`
--

CREATE TABLE `financiero_mobiliario` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_dispo` int(10) UNSIGNED DEFAULT NULL,
  `precio` decimal(10,0) NOT NULL,
  `unidad` int(10) UNSIGNED DEFAULT NULL,
  `enlace` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formatos`
--

CREATE TABLE `formatos` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_formato` int(10) UNSIGNED NOT NULL,
  `tipo_dispositivo` int(10) UNSIGNED NOT NULL,
  `formato` text,
  `descripcion` text,
  `creado` datetime DEFAULT NULL,
  `nombre_formato` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foto_observacion`
--

CREATE TABLE `foto_observacion` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_observacion` int(10) UNSIGNED DEFAULT NULL,
  `foto` varchar(40) NOT NULL,
  `descripcion` text NOT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_dispo`
--

CREATE TABLE `grupo_dispo` (
  `id_grupo_dispo` int(10) UNSIGNED NOT NULL,
  `grupo_dispo` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo_dispo`
--

INSERT INTO `grupo_dispo` (`id_grupo_dispo`, `grupo_dispo`, `descripcion`) VALUES
(1, 'ACCESORIO', 'Accesorio del dispositivo'),
(2, 'REPUESTO', 'Repuesto para el dispositivo'),
(3, 'EQUIPO', 'Equipo o dispositivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hojadevida`
--

CREATE TABLE `hojadevida` (
  `id` int(10) UNSIGNED NOT NULL,
  `dispo_codigo` int(10) UNSIGNED NOT NULL,
  `dispo_serial` int(10) UNSIGNED DEFAULT NULL,
  `hardware` int(10) UNSIGNED DEFAULT NULL,
  `software` text,
  `carga` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagen` varchar(40) NOT NULL,
  `tipo_dispo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` int(10) UNSIGNED NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`id`, `imagen`, `tipo_dispo`, `marca`, `modelo`, `descripcion`) VALUES
(1, 'd0f94ddf2d0c66e91.jpg', 1, 1, 1, 'Holter'),
(2, '0f3cd7645201167b9.jpg', 3, 3, 3, 'PHILIPS'),
(3, '3c7ab71c1d907fca8.jpg', 15, 9, 9, 'Digitalizador para ecografo'),
(4, '1ad1671dda11eda6d.jpg', 6, 1, 10, NULL),
(5, '72a4aa7397261433b.jpg', 11, 10, 11, 'MODELO 10160');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimientos`
--

CREATE TABLE `mantenimientos` (
  `id_mtto` int(10) UNSIGNED NOT NULL,
  `codigo` int(10) UNSIGNED NOT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `fecha_mtto` date NOT NULL,
  `tipo_mtto` int(10) UNSIGNED NOT NULL,
  `responsable` int(10) UNSIGNED NOT NULL,
  `id_responsable` int(10) UNSIGNED DEFAULT NULL,
  `documento` varchar(100) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field13` varchar(40) DEFAULT NULL,
  `field14` varchar(40) DEFAULT NULL,
  `documento_drive` text,
  `unidad` int(10) UNSIGNED NOT NULL,
  `unidad_abreviado` int(10) UNSIGNED DEFAULT NULL,
  `descripcion` text NOT NULL,
  `estado_final` int(10) UNSIGNED NOT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `mantenimientos`
--

INSERT INTO `mantenimientos` (`id_mtto`, `codigo`, `serial`, `fecha_mtto`, `tipo_mtto`, `responsable`, `id_responsable`, `documento`, `timestamp`, `update`, `creado_por`, `editado_por`, `creado`, `editado`, `field13`, `field14`, `documento_drive`, `unidad`, `unidad_abreviado`, `descripcion`, `estado_final`, `tipo_dispo`) VALUES
(1, 70, 70, '2020-02-19', 1, 3, 3, 'desktop-ic-aio-a340.jpg', '0000-00-00 00:00:00', NULL, 'felipem', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(2, 232, 232, '2017-11-05', 1, 6, 6, 'MANTENIMIENTO_001.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', 'felipem', '0000-00-00 00:00:00', '2020-11-20 16:05:07', NULL, NULL, NULL, 15, 15, '<div>SE REALIZA MANTENIMIENTO PREVENTIVO, LIMPIEZA INTERNA Y EXTERNA, REVISIÓN DE TARJETAS ELECTRÓNICAS,</div><div>&nbsp;LIMPIEZA DE CONTACTOS, REVISION CABLE DE PODER, SE REALIZAN PRUEBAS DE FUNCIONAMIENTO</div><div>Y SE ENTREGA AL SERVICIO EN OPTIMAS CONDICIONES.</div>', 1, NULL),
(3, 232, 232, '2018-05-05', 1, 6, 6, 'MANTENIMIENTO_002.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-06 12:01:46', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(4, 232, 232, '2018-11-05', 1, 6, 6, 'MANTENIMIENTO_003.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-06 12:02:57', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(5, 232, 232, '2019-05-05', 1, 6, 6, 'MANTENIMIENTO_004.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-06 12:03:47', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(6, 232, 232, '2019-11-05', 1, 6, 6, 'MANTENIMIENTO_005.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-06 12:04:41', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(7, 232, 232, '2020-05-05', 1, 6, 6, 'MANTENIMIENTO_006.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-06 12:06:13', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(8, 250, 250, '2016-08-10', 1, 6, 6, 'MANTENIMIENTO_001.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 15:56:06', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(9, 250, 250, '2017-02-10', 1, 6, 6, 'MANTENIMIENTO_002.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 15:59:20', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(10, 250, 250, '2017-08-22', 1, 6, 6, 'MANTENIMIENTO_003.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 16:00:08', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(11, 250, 250, '2018-02-22', 1, 6, 6, 'MANTENIMIENTO_004.pdf', '0000-00-00 00:00:00', '8/11/2020 04:02:59 pm', 'lgarcia', 'lgarcia', '0000-00-00 00:00:00', '2020-11-08 16:01:31', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(12, 250, 250, '2018-06-05', 1, 6, 6, 'MANTENIMIENTO_005.xlsx.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 16:04:40', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(13, 250, 250, '2018-12-05', 1, 6, 6, 'MANTENIMIENTO_006.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 16:13:26', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(14, 250, 250, '2019-06-05', 1, 6, 6, 'MANTENIMIENTO_007.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 16:18:39', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(15, 250, 250, '2019-11-05', 1, 6, 6, 'MANTENIMIENTO_008.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 16:22:19', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(16, 250, 250, '2020-05-05', 1, 6, 6, 'MANTENIMIENTO_009.pdf', '0000-00-00 00:00:00', NULL, 'lgarcia', NULL, '0000-00-00 00:00:00', '2020-11-08 16:23:58', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(17, 258, 258, '2020-08-19', 1, 6, 6, 'MANTENIMIENTO_001.pdf', '0000-00-00 00:00:00', '9/11/2020 09:49:10 pm', 'lgarcia', 'lgarcia', '0000-00-00 00:00:00', '2020-11-09 21:33:23', NULL, NULL, NULL, 0, NULL, '', 0, NULL),
(18, 23, 23, '2021-02-19', 2, 9, 9, NULL, NULL, NULL, 'felipem', 'felipem', '2021-02-19 10:00:19', '2021-02-19 10:02:51', NULL, NULL, NULL, 7, 7, 'Describo todo loque quiera.<div><br></div><div><ol><li>sjdlkajsdas</li><li>sdasd</li></ol><div>se le pone la tarjeta numero xxx</div></div><div><br></div><div>el envio de bta 8000 y xxx</div><div><br></div>', 3, 23),
(19, 25, 25, '2020-10-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-02-24 10:43:10', NULL, NULL, NULL, NULL, 4, 4, '&nbsp;INSPECCION FISICA<div><br><div>Se procede a verificar el estado del equipo, del cable y de la comunicación con el pc donde se encuentra instalado el software. Inspección correcta, buena comunicación sin errores.</div><div><br></div><div>LIMPIEZA INTERNA Y EXTERNA</div><div><br></div><div>Se procede a retirar el polvo con alcohol isopropílico, se verifican los puntos de alimentación DC desde la batería a la tarjeta, se verifica que no se encuentre sulfatación&nbsp;</div><div><br></div><div>VERIFICACION</div><div><br></div><div>Se realiza prueba de grabación de datos, registro de ECG y comunicación con el PC para la descarga de la grabación.</div><div><br></div><div>RECOMENDACIONES</div><div><br></div><div>Se hace la notación de no aplicar amonio cuaternario a la grabadora o a los cables, por que deteriora el material de los cables y puede dañar también la grabadora por sulfatación sobre las tarjetas. Se debe desinfectar&nbsp; con agua y jabón, frotar el cable utilizando guantes, retirar el jabón con abundante agua y secar con toalla de papel. Luego se debe aplicar 2 gotas de aceite mineral para la lubricación del cable. La grabadora de puede desinfectar con un paño húmedo de alcohol y luego se retira el alcohol con un paño húmedo de agua. guardar en un ambiente seco preferiblemente en su estuche.</div><div><br></div><div>El estuche se debe limpiar con un paño húmedo de agua y jabón, se retira el jabón con un paño húmedo de agua.</div><div><br></div><div>Utilizar par el estudio baterías AAA alcalinas.</div><div><br></div><div>Hacer las recomendaciones al paciente de no ingresar a zonas húmedas, no bañarse con el equipo instalado, no manipular el equipo, llenar el registro de síntomas, no golpear el equipo y evitar las zonas donde hay influencia de campos magnéticos.</div><div><br></div><div><br></div><div><br></div><div><br></div><div><br></div><div>&nbsp;</div><div><br></div><div><br></div></div>', 1, 25),
(20, 12, 12, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'l.solarte', '2021-04-08 14:04:51', '2021-04-08 14:39:39', NULL, NULL, NULL, 2, 2, 'Se realiza limpieza general, se verifica los contactos de positivo y negativo, se retira polvo, se verifica el estado de los cables de paciente, optimo estado, estuche en buen estado. Comunicación con el computador excelente. Se cambia la tapa de la bateria por estar en mal estado, se coloca una nueva', 1, 12),
(21, 42, 42, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'a.lopez', '2021-04-08 14:38:37', '2021-04-13 11:39:40', NULL, NULL, NULL, 2, 2, 'Se realiza limpieza general interna con desarme de todas las tarjetas, se limpian con alcohol isopropilico todas las tarjetas, se limpia el ventilador lateral, se desarma y se limpian todos los módulos de la fuente, se limpia el transformador interno. Se verifica los voltajes de la fuente de +3 +5 -5 +12 -12 +24&nbsp; +85 +170 -170, en la tarjeta power regulation y en la fuente principal. se ajustan los puntos de contacto de +3, +5 y +5DVC. se verifica la ventilación de la fuente. todas la tarjetas se verifica su estado físico y se verifica los voltajes en los puntos de contacto según manual. Se Instalan y se ajustan las tarjetas, se realiza limpieza externa y de teclados, se limpia el track ball, se verifica su buen funcionamiento, se realiza limpieza de las pantallas táctiles, Se realiza test extendido y queda el equipo sin errores, se realiza calibración de las pantallas táctiles. Se realiza prueba de todos los botones desde el menú. Se realiza prueba de sonido y por ultimo se realiza prueba de grabación en el pinnacle. Hoy se instala la batería de la tarjeta procesador de gráficos.&nbsp;', 1, 42),
(22, 20, 20, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'l.solarte', '2021-04-08 14:42:16', '2021-04-08 14:47:28', NULL, NULL, NULL, 2, 2, '<p class=\"MsoNormal\"><span xss=\"removed\">Se realiza limpieza\r\ngeneral, se verifica los contactos de positivo y negativo, se retira polvo, se\r\nverifica el estado de los cables de paciente, optimo estado, estuche en buen\r\nestado. Comunicación con el computador excelente. Cambio de tapa de bateria . Por mal estado</span><o></o></p>', 1, 20),
(23, 260, 260, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-08 14:49:19', NULL, NULL, NULL, NULL, 2, 2, '<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Helvetica, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Se realiza limpieza\r\ngeneral, se verifica los contactos de positivo y negativo, se retira polvo, se\r\nverifica el estado de los cables de paciente, optimo estado, estuche en buen\r\nestado. Comunicación con el computador excelente. Se cambia tapa de batería por mal estado</span><o:p></o:p></p>', 1, 260),
(24, 4, 4, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'l.solarte', '2021-04-08 15:13:29', '2021-04-08 15:35:02', NULL, NULL, NULL, 2, 2, '<p class=\"MsoNormal\"><span xss=\"removed\">Se realiza limpieza\r\ngeneral, se verifica los contactos de positivo y negativo, se retira polvo, se\r\nverifica el estado de los cables de paciente, optimo estado, estuche en buen\r\nestado. Comunicación con el computador excelente. Se requiere programar cambio de cable por presentar daño en un latiguillo</span><o></o></p>', 1, 4),
(27, 163, 163, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-08 15:40:26', NULL, NULL, NULL, NULL, 2, 2, 'Se realiza limpieza de todos componentes, se realiza lubricación de los rodillos, se realiza limpieza de caucho tapiz, se aplica silicona emulsionada para reducir la fricción, se tensiona&nbsp; el caucho tapiz, se realizan pruebas de funcionamiento de velocidad e inclinacion.&nbsp;', 1, 163),
(25, 314, 314, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-08 15:18:14', NULL, NULL, NULL, NULL, 2, 2, '<p class=\"MsoNormal\"><span style=\"font-size: 9pt; line-height: 107%; font-family: Helvetica, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Se realiza limpieza\r\ngeneral interna con desarme de todas las tarjetas, se limpian con alcohol\r\nisopropilico todas las tarjetas, se limpia el ventilador lateral, se desarma y\r\nse limpian todos los módulos de la fuente, se limpia el transformador interno.\r\nSe verifica los voltajes de la fuente de +3 +5 -5 +12 -12 +24&nbsp; +85 +170\r\n-170, en la tarjeta power regulation y en la fuente principal. se ajustan los\r\npuntos de contacto de +3, +5 y +5DVC. se verifica la ventilación de la fuente.\r\ntodas las tarjetas se verifica su estado físico y se verifica los voltajes en\r\nlos puntos de contacto según manual. Se Instalan y se ajustan las tarjetas, se\r\nrealiza limpieza externa y de teclados, se limpia el track ball, se verifica su\r\nbuen funcionamiento, se realiza limpieza de las pantallas táctiles, Se realiza\r\ntest extendido y queda el equipo sin errores, se realiza calibración de las\r\npantallas táctiles. Se realiza prueba de todos los botones desde el menú. Se\r\nrealiza prueba de sonido y por ultimo se realiza prueba de grabación en el pinnacle.&nbsp;<o:p></o:p></span></p>', 1, 314),
(26, 81, 81, '2021-04-08', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-08 15:29:55', NULL, NULL, NULL, NULL, 2, 2, 'Se realiza limpieza general externa y en las tarjetas. Se verifica los puntos de contacto de las baterias. Se verifica el estado del manguito, se realiza&nbsp; prueba ce comunicacion con el computador y prueba de funcionamiento en persona no paciente.', 1, 81),
(28, 12, 12, '2021-04-08', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', 'a.lopez', '2021-04-11 12:38:59', '2021-04-11 12:41:06', NULL, NULL, NULL, 2, 2, '<div>Se realiza limpieza general, se verifica los contactos de positivo y negativo, se retira polvo, se verifica el estado de los cables de paciente, optimo estado, estuche en buen estado. Comunicación con el computador excelente. Se cambia la tapa de la bateria por estar en mal estado, se coloca una nueva<br></div>', 1, 12),
(29, 227, 227, '2021-04-08', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-11 12:45:09', NULL, NULL, NULL, NULL, 2, 2, 'Se realiza limpieza general, se verifica los contactos de positivo y negativo, se retira polvo, se verifica el estado de los cables de paciente, optimo estado, estuche en buen estado. Comunicación con el computador excelente. Se cambia la tapa de la bateria por estar en mal estado, se coloca una nueva<br>', 1, 227),
(30, 20, 20, '2021-04-08', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-11 12:48:40', NULL, NULL, NULL, NULL, 2, 2, 'Se realiza limpieza general, se verifica los contactos de positivo y negativo, se retira polvo, se verifica el estado de los cables de paciente, optimo estado, estuche en buen estado. Comunicación con el computador excelente. Se cambia la tapa de la bateria por estar en mal estado, se coloca una nueva<br>', 1, 20),
(31, 42, 42, '2021-04-08', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', 'a.lopez', '2021-04-11 12:52:41', '2021-04-13 11:39:55', NULL, NULL, NULL, 2, 2, '<div><span xss=\"removed\"><br></span></div><div><span style=\"font-size: 11.998px;\">Se realiza limpieza general interna con desarme de todas las tarjetas, se limpian con alcohol isopropilico todas las tarjetas, se limpia el ventilador lateral, se desarma y se limpian todos los módulos de la fuente, se limpia el transformador interno. Se verifica los voltajes de la fuente de +3 +5 -5 +12 -12 +24&nbsp; +85 +170 -170, en la tarjeta power regulation y en la fuente principal. se ajustan los puntos de contacto de +3, +5 y +5DVC. se verifica la ventilación de la fuente. todas la tarjetas se verifica su estado físico y se verifica los voltajes en los puntos de contacto según manual. Se Instalan y se ajustan las tarjetas, se realiza limpieza externa y de teclados, se limpia el track ball, se verifica su buen funcionamiento, se realiza limpieza de las pantallas táctiles, Se realiza test extendido y queda el equipo sin errores, se realiza calibración de las pantallas táctiles. Se realiza prueba de todos los botones desde el menú. Se realiza prueba de sonido y por ultimo se realiza prueba de grabación en el pinnacle. Hoy se instala la batería de la tarjeta procesador de gráficos.&nbsp;</span><br></div><div><span xss=\"removed\"><br></span></div><div><br></div>', 1, 42),
(46, 126, 126, '2021-04-09', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-13 12:20:32', NULL, NULL, NULL, NULL, 2, 2, 'SE HACE MANTENIMIENTO GENERAL DEL EQUIPO, RE VERIFICAN LOS CONTACTOS, SE LIMPIA LA TARJETA, SE HACE LIMPIEZA DE LA PARTE EXTERNA DEL EQUIPO Y SE DESINFECTA&nbsp;', 1, 126),
(47, 49, 49, '2021-04-13', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 09:58:09', NULL, NULL, NULL, NULL, 1, 1, '<span lang=\"ES-CO\" style=\"font-size: 9pt; line-height: 107%; font-family: Helvetica, sans-serif; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Se realiza limpieza\r\ngeneral interna con desarme de todas las tarjetas, se limpian con alcohol\r\nisopropilico todas las tarjetas, se limpia el ventilador lateral, se desarma y\r\nse limpian todos los módulos de la fuente, se limpia el transformador interno.\r\nSe verifica los voltajes de la fuente de +3 +5 -5 +12 -12 +24&nbsp; +85 +170\r\n-170, en la tarjeta power regulation y en la fuente principal. se ajustan los\r\npuntos de contacto de +3, +5 y +5DVC. se verifica la ventilación de la fuente.\r\ntodas las tarjetas se verifica su estado físico y se verifica los voltajes en\r\nlos puntos de contacto según manual. Se Instalan y se ajustan las tarjetas, se\r\nrealiza limpieza externa y de teclados, se limpia el track ball, se verifica su\r\nbuen funcionamiento, se realiza limpieza de las pantallas táctiles, Se realiza\r\ntest extendido y queda el equipo sin errores, se realiza calibración de las\r\npantallas táctiles. Se realiza prueba de todos los botones desde el menú. Se\r\nrealiza prueba de sonido y por ultimo se realiza prueba de grabación en el\r\npinnacle.</span><br>', 1, 49),
(32, 163, 163, '2021-04-08', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-11 12:59:14', NULL, NULL, NULL, NULL, 2, 2, 'Revisión y mantenimiento de banda de trote 706 y tensión de banda&nbsp;<div>Observación:</div><div>Tiene tapa de lado lateral izquierdo dañada en los seguros&nbsp;</div>', 1, 163),
(33, 138, 138, '2021-04-10', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-11 13:13:36', NULL, NULL, NULL, NULL, 2, 2, '<div><span style=\"font-size: 11.998px;\">Se realizó mantenimiento y limpieza, se sopletiaron las tarjetas, se requiere cambio de batería, ya se envió a Bogotá. La batería para reparación del componente&nbsp;</span></div>', 2, 138),
(34, 218, 218, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-12 09:16:46', NULL, NULL, NULL, NULL, 1, 1, 'Se realiza limpieza externa e interna, se retira partículas de polvo, NO se evidencia sulfatación. cable de paciente en buen estado, comunicación con el PC excelente. Tapa porta batería buena. grabadora queda funcional.', 1, 218),
(35, 60, 60, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-12 10:19:52', NULL, NULL, NULL, NULL, 1, 1, 'Se realiza limpieza externa, se retira polvo de la parte interna en las tarjetas, tapa de porta batería en buen estado, cable de paciente en buen estado, comunicación con el PC excelente, grabadora apta para utilizar con paciente.', 1, 60),
(36, 6, 6, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'a.lopez', '2021-04-12 10:28:26', '2021-04-14 10:02:34', NULL, NULL, NULL, 1, 1, 'Se realiza limpieza externa, se verifica cable de paciente en buen estado, se verifica la tapa de porta bataria en buen estado, se realiza limpieza de las tarjetas retirando el polvo y&nbsp; no hay presencia de sulfatación.', 1, 6),
(37, 218, 218, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'a.lopez', '2021-04-12 10:48:05', '2021-04-13 11:35:22', NULL, NULL, NULL, 1, 1, 'se realiza limpieza externa, se verifica tapa de porta batería en buen estado, cable de paciente en buen estado. se limpian tarjetas retirando polvo no hay sulfatacion&nbsp;', 1, 218),
(38, 57, 57, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', 'a.lopez', '2021-04-12 11:04:47', '2021-04-14 10:14:57', NULL, NULL, NULL, 1, 1, '<span xss=\"removed\">se realiza limpieza externa, se verifica tapa de porta batería en buen estado, cable de paciente en buen estado. se limpian tarjetas retirando polvo no hay sulfatacion</span><br>', 1, 57),
(39, 59, 59, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-12 11:24:40', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">se realiza limpieza externa, se verifica tapa de porta batería en buen estado, cable de paciente en buen estado. se limpian tarjetas retirando polvo no hay sulfatacion</span><br>', 1, 59),
(40, 172, 172, '2021-04-12', 1, 9, 9, NULL, NULL, NULL, 'l.solarte', NULL, '2021-04-12 12:36:52', NULL, NULL, NULL, NULL, 1, 1, '<br><div><span style=\"font-size: 11.998px;\">se realiza limpieza externa, se verifica que no tiene la tapa de porta batería, cable de paciente en buen estado. se limpian tarjetas retirando polvo no hay sulfatacion</span><br></div><div><span style=\"font-size: 11.998px;\"><br></span></div><div>OBSERVACIÓN: SE DEBEN RETIRAR LAS BATERÍAS AL FINAL DE CADA JORNADA .</div>', 1, 172),
(41, 93, 93, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', 'a.lopez', '2021-04-12 17:45:21', '2021-04-13 09:06:34', NULL, NULL, NULL, 7, 7, 'Se hizo mantenimiento al equipo se revisaron la válvulas&nbsp; y se envió a San Fernando para cambio de la manguera&nbsp;', 1, 93),
(42, 87, 87, '2021-04-13', 1, 9, 9, NULL, NULL, NULL, 'a.lopez', 'a.lopez', '2021-04-13 08:52:51', '2021-04-14 10:04:15', NULL, NULL, NULL, 1, 1, 'Se realiza limpieza general, se retira polvo de las tarjetas, no hay sulfatacion. no hay fugas en el circuito neumatico. pruebas de funcionamiento correcto.', 1, 87),
(43, 80, 80, '2021-04-13', 1, 9, 9, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-13 09:09:25', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">Se realiza limpieza general, se retira polvo de las tarjetas, no hay sulfatacion. no hay fugas en el circuito neumatico. pruebas de funcionamiento correcto.</span><br>', 1, 80),
(44, 165, 165, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-13 11:33:58', NULL, NULL, NULL, NULL, 1, 1, 'SE HACE MANTENIMIENTO GENERAL DEL EQUIPO, SE LIMPIA LA BANDA, SE PRUEBA LA VELOCIDAD&nbsp;', 1, 165),
(45, 218, 218, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-13 11:35:54', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">se realiza limpieza externa, se verifica tapa de porta batería en buen estado, cable de paciente en buen estado. se limpian tarjetas retirando polvo no hay sulfatacion&nbsp;</span><br>', 1, 218),
(48, 50, 50, '2021-04-13', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', 'a.lopez', '2021-04-14 10:00:24', '2021-04-14 10:01:54', NULL, NULL, NULL, 1, 1, '<span lang=\"ES-CO\" xss=\"removed\">Se realiza limpieza\r\ngeneral interna con desarme de todas las tarjetas, se limpian con alcohol\r\nisopropilico todas las tarjetas, se limpia el ventilador lateral, se desarma y\r\nse limpian todos los módulos de la fuente, se limpia el transformador interno.\r\nSe verifica los voltajes de la fuente de +3 +5 -5 +12 -12 +24&nbsp; +85 +170\r\n-170, en la tarjeta power regulation y en la fuente principal. se ajustan los\r\npuntos de contacto de +3, +5 y +5DVC. se verifica la ventilación de la fuente.\r\ntodas las tarjetas se verifica su estado físico y se verifica los voltajes en\r\nlos puntos de contacto según manual. Se Instalan y se ajustan las tarjetas, se\r\nrealiza limpieza externa y de teclados, se limpia el track ball, se verifica su\r\nbuen funcionamiento, se realiza limpieza de las pantallas táctiles, Se realiza\r\ntest extendido y queda el equipo sin errores, se realiza calibración de las\r\npantallas táctiles. Se realiza prueba de todos los botones desde el menú. Se\r\nrealiza prueba de sonido y por ultimo se realiza prueba de grabación en el\r\npinnacle.</span><div>SE CAMBIA TRAJETA KEY SCANER COMPLETA&nbsp;</div><div>SE CAMBIA BATERIA DEL EQUIPO&nbsp;</div><div><div><span lang=\"ES-CO\" xss=\"removed\"><br></span></div><div><span lang=\"ES-CO\" xss=\"removed\"><br></span></div></div>', 1, 50),
(49, 6, 6, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 10:03:10', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">Se realiza limpieza externa, se verifica cable de paciente en buen estado, se verifica la tapa de porta bataria en buen estado, se realiza limpieza de las tarjetas retirando el polvo y&nbsp; no hay presencia de sulfatación.</span><br>', 1, 6),
(50, 87, 87, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 10:04:59', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">Se realiza limpieza general, se retira polvo de las tarjetas, no hay sulfatacion. no hay fugas en el circuito neumatico. pruebas de funcionamiento correcto.</span><br>', 1, 87),
(51, 80, 80, '2021-04-13', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 10:06:04', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">Se realiza limpieza general, se retira polvo de las tarjetas, no hay sulfatacion. no hay fugas en el circuito neumatico. pruebas de funcionamiento correcto.</span><br>', 1, 80),
(52, 57, 57, '2021-04-14', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 10:15:29', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">se realiza limpieza externa, se verifica tapa de porta batería en buen estado, cable de paciente en buen estado. se limpian tarjetas retirando polvo no hay sulfatacion</span><br>', 1, 57),
(53, 226, 226, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 10:17:36', NULL, NULL, NULL, NULL, 1, 1, '<span style=\"font-size: 11.998px;\">se realiza limpieza externa, cable de </span>conexión<span style=\"font-size: 11.998px;\">&nbsp;en buen estado. se limpia la tarjeta retirando polvo no hay sulfatacion</span><br>', 1, 226),
(54, 128, 128, '2021-04-12', 1, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-14 10:19:57', NULL, NULL, NULL, NULL, 1, 1, '<span xss=\"removed\" style=\"font-size: 11.998px;\">se realiza limpieza externa, cable de&nbsp;</span><span style=\"font-size: 11.998px;\">conexión</span><span xss=\"removed\" style=\"font-size: 11.998px;\">&nbsp;en buen estado. se limpia la tarjeta retirando polvo no hay sulfatacion</span><br>', 1, 128),
(55, 42, 42, '2021-04-15', 2, 8, 8, NULL, NULL, NULL, 'a.lopez', NULL, '2021-04-15 10:50:53', NULL, NULL, NULL, NULL, 2, 2, '<span lang=\"es\" style=\"font-size:11.0pt;line-height:\r\n115%;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:Arial;mso-ansi-language:\r\n#000A;mso-fareast-language:ES-CO;mso-bidi-language:AR-SA\">se hizo reparación a\r\nun equipo (Ecógrafo), en la unidad HMC por el cual una de las llantas el equipo\r\nestaba chocando con la rejilla de filtro del equipo,&nbsp;</span><br>', 1, 42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id_marca` int(10) UNSIGNED NOT NULL,
  `marca` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id_marca`, `marca`, `descripcion`) VALUES
(1, 'DMS', NULL),
(2, 'CONTEC', NULL),
(3, 'PHILIPS', NULL),
(4, 'WELCH ALLYN', NULL),
(5, 'GENERAL ELECTRIC', NULL),
(6, 'SIMENS', NULL),
(7, 'ASUS', NULL),
(8, 'SUNTECH', NULL),
(9, 'PINNACLE', NULL),
(10, 'LENOVO', 'COMPUTADORES LENOVO'),
(12, 'SPORT FITNESS', NULL),
(13, 'ZOLL', NULL),
(14, 'PERFECT10', NULL),
(15, 'ACER', NULL),
(16, 'SIUI', NULL),
(17, 'EPSON', NULL),
(18, 'LG', NULL),
(19, 'SMAF', NULL),
(20, 'ZKTECO', NULL),
(21, 'NIPRO', NULL),
(22, 'MOTOROLA', NULL),
(23, 'ARTEMA', NULL),
(24, 'HP', 'HP PARA OFICINA'),
(25, 'KINGSTON', 'Memoria USB'),
(26, 'NA', NULL),
(27, 'HUAWEI', 'LUA-03'),
(28, 'INNOMED', NULL),
(29, 'Janus', NULL),
(30, 'OmRon', 'Modelo \r\nHEM-7114'),
(31, 'KTJ thermo', 'TA218'),
(32, 'SAMSUNG', NULL),
(33, 'DELL', NULL),
(34, 'DIGITAL DOPPLER', 'Tarjeta digital doppler para sonos 5500'),
(35, 'MAGOM', NULL),
(36, 'GREN MAY', 'Termo higrometro'),
(37, 'GMD', NULL),
(38, 'SONOS 5500', NULL),
(39, 'LINKSYS', NULL),
(40, 'STANLEY', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_modelo`
--

CREATE TABLE `marca_modelo` (
  `id_mmodelo` int(10) UNSIGNED NOT NULL,
  `marca` int(10) UNSIGNED NOT NULL,
  `modelo` varchar(40) NOT NULL,
  `fabricante` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `img` varchar(40) NOT NULL,
  `field6` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marca_modelo`
--

INSERT INTO `marca_modelo` (`id_mmodelo`, `marca`, `modelo`, `fabricante`, `descripcion`, `img`, `field6`, `creado`, `creado_por`) VALUES
(1, 1, '300-3A', 'DMSoftware', '<br>', '13f4515ec1b290738.jpg', NULL, NULL, NULL),
(2, 2, 'ABPM50', 'CONTEC MEDICAL', '<br>', 'c5a4e52ffd1371037.png', NULL, NULL, NULL),
(3, 3, 'SONOS5500', 'PHILIPS', '<br>', 'b8a5974b09f495570.jpg', NULL, NULL, NULL),
(4, 5, 'VIVID-i', 'GE', '<br>', '3dd6d629302078b98.jpg', NULL, NULL, NULL),
(5, 6, 'ACUSON CYPRESS', 'SIMENS', '<br>', '34f8b1277a9587f5c.jpg', NULL, NULL, NULL),
(6, 7, 'X407U', 'ASUS', '<br>', '7c374d1d63b19e374.jpg', NULL, NULL, NULL),
(7, 2, 'CONTEC 8000', 'CONTEC MEDICAL', '<br>', 'd1f4078ebe073796e.jpg', NULL, NULL, NULL),
(8, 8, 'BRAVO 24H', 'SUNTECH', '<br>', '4968e54c14cc0374d.jpg', NULL, NULL, NULL),
(9, 9, 'VIDEO CAPTURE', 'PINNACLE', '<br>', '17faafb62b0451cd3.jpg', NULL, NULL, NULL),
(10, 1, 'DMS-300BTT/BTR', 'DMSoftware', '<br>', '59ac2dd651d16fa03.jpg', NULL, NULL, NULL),
(11, 10, 'TODO EN UNO C260', 'LENOVO', '<br>', '0071cada5585eced1.jpg', NULL, NULL, NULL),
(12, 2, 'PM50', 'CONTEC MEDICAL', 'MONITOR DE PRESIÓN ARTERIAL CON SPO', '2886d30c54ea0035d.jpg', NULL, NULL, NULL),
(13, 1, 'SOFTWARE CARDIOSCAN 76A', 'CONTEC MEDICAL', 'SOFTWARE HOLTER CARDIOSCAN 76A', 'a4aa682eab47aabbf.jpg', NULL, NULL, NULL),
(14, 1, 'SOFTWARE CARDIOSCAN SATELLITE', 'CONTEC MEDICAL', 'CARDIOSCAN SATELLITE 2020', 'a224ed359a40d1c73.jpg', NULL, NULL, NULL),
(15, 10, 'AIO A340', 'LENOVO', 'COMPUTADOR TODO EN UNO CORE i5 G8', '79225158bd18a3b6f.jpg', NULL, NULL, NULL),
(16, 3, '21311A', 'PHILIPS', 'Transductor o sonda de referencia s3', 'f92192ee36757b969.jpg', NULL, NULL, NULL),
(17, 3, '21350A', 'PHILIPS', 'Transductor s8 pediátrico', '9cdcca9829b39bf0c.jpg', NULL, NULL, NULL),
(18, 12, 'JS-5000B-1', 'SPORT FITNESS', '<br>', '3a59617ab28a1a4f9.jpg', NULL, NULL, NULL),
(19, 4, 'IP22', 'ARTEMA', '<br>', '0ccecf6da6df9d8ef.jpg', NULL, NULL, NULL),
(20, 3, 'IPX-7', 'PHILIPS', '<br>', 'f6d31817f8eed03c7.jpg', NULL, NULL, NULL),
(21, 13, 'PD 1200', 'ZOLL MEDICAL', '<br>', 'b9551239478536bcc.jpg', NULL, NULL, NULL),
(22, 14, 'PERFECT10', 'PERFECT10', '<br>', 'a4431b8946b1671e6.jpg', NULL, NULL, NULL),
(23, 15, 'ASPIRE Z1-601', 'ACER', 'Equipo de computo todo en uno&nbsp;', 'fdcae9ccffb776572.jpg', NULL, NULL, NULL),
(24, 3, '21356A', 'HEWLETT PACKARD', 'HEWLETT PACKARD', '6da7ac27c53ce45bb.jpg', NULL, NULL, NULL),
(25, 3, '21330A', 'HEWLETT PACKARD', 'HEWLETT PACKARD', 'ebd011d0abb9f9004.jpg', NULL, NULL, NULL),
(26, 3, '21369A', 'HEWLETT PACKARD', 'HEWLETT PACKARD', 'f524cae427b8d7d95.jpg', NULL, NULL, NULL),
(27, 4, 'PIC 30', 'WELCH ALLYN', '<br>', '193661f9297760a25.jpg', NULL, NULL, NULL),
(28, 17, 'L5190', 'EPSON', 'L5190 impresora multifuncional con scanner, fax, WiFi, pantalla LCD color&nbsp;', '2367c21ed40024791.jpg', NULL, NULL, NULL),
(29, 18, '19EN33SA', 'LG', 'Pantalla LG', 'df5ab3fadf89e4a1e.jpg', NULL, NULL, NULL),
(30, 19, 'SXT-5A', 'SMAF', '<br>', '615291fa14ef7d72c.jpg', NULL, NULL, NULL),
(31, 20, 'TX628', 'ZKTeco', '<br>', '3abb6580f800a94a4.jpg', NULL, NULL, NULL),
(32, 21, 'FP-960', 'NIPRO CORPORATION', '<br>', 'ebfdad944b71e9446.jpg', NULL, NULL, NULL),
(33, 3, 'T6210', 'PHILIPS', 'SONDA TRASESOFAGICA&nbsp;', 'dd847adc7bce4c1d8.jpg', NULL, NULL, NULL),
(34, 22, 'XT1601', 'MOTOROLA', '<div>Nombre del sistema operativo Android</div><div>Versión del sistema operativo 6.0.1 Marshmallow</div><div>Año de lanzamiento 2016</div><div>Peso 137 g</div><div>Altura 144.4 mm</div><div>Ancho 72 mm</div><div>Profundidad 9.9 mm</div><div>Pantalla táctil Sí</div><div>Tamaño de la pantalla 5 \"</div><div>Resolución de la pantalla 720 px x 1280 px</div><div>Píxeles por pulgada 294 ppi</div><div>Tecnología de pantalla IPS</div><div>Cámara Sí</div><div>Cantidad de cámaras traseras 1</div><div>Resolución de la cámara trasera principal 8 Mpx</div><div>Resolución de video de la cámara trasera 1920 px x 1080 px</div><div>Apertura del diafragma de la cámara trasera f 2.2</div><div>Cantidad de cámaras frontales 1</div><div>Resolución de la cámara frontal principal 5 Mpx</div><div>Resolución de video de la cámara frontal 1920 px x 1080 px</div><div>Apertura del diafragma de la cámara frontal f 2.2</div><div>Flash en la cámara frontal No</div><div>Red 4G/LTE</div><div>Cantidad de ranuras para tarjeta SIM 2</div><div>Tamaños de tarjeta SIM compatibles Micro-SIM</div><div>Tipos de tarjeta de memoria microSD</div><div>Capacidad máxima de la tarjeta de memoria 256 GB</div><div>Modelo del procesador Snapdragon 410</div><div>Modelos de CPU 4x1.2 GHz Cortex-A53</div><div>Cantidad de núcleos del procesador 4</div><div>Velocidad del procesador 1.2 GHz</div><div>Modelo de GPU Adreno 306</div><div>Teclado QWERTY físico No</div><div>Conector USB Sí</div><div>Wi-Fi Sí</div><div>GPS Sí</div><div>Bluetooth Sí</div><div>Mini HDMI No</div><div>Radio Sí</div><div>Lector de huella digital No</div><div>Reconocimiento facial Sí</div><div>Acelerómetro Sí</div><div>Sensor de proximidad Sí</div><div>Giroscopio No</div><div>Resistente a salpicaduras No</div><div>Resistente al agua No</div><div>A prueba de agua No</div><div>Tipo de batería Polímero de litio</div><div>Capacidad de la batería 2800 mAh</div><div>Batería removible Sí</div>', 'f7462b97dfea33400.jpg', NULL, NULL, NULL),
(35, 23, 'CARDIO AID 200-B', 'ARTEMA', 'Desfibrilador ARTEMA', '8df4132e249af5884.jpg', NULL, NULL, NULL),
(36, 10, '80MJ', 'LENOVO', 'COMPUTADOR PORTÁTIL&nbsp;', '116a5c5700bde702d.jpg', NULL, NULL, NULL),
(37, 10, '80UC', NULL, 'LENOVO IDEAPAD 110-14ISK', 'b6cfcf85b37de3e60.jpg', NULL, NULL, NULL),
(38, 24, '18-1311la', 'HP', 'HP 18 All in One PC', '2f1b64c7b710a4d6d.jpg', NULL, NULL, NULL),
(39, 1, 'HDMI-USB', 'DMS', 'Cable HDMI-USB para Holter 300-3A', 'e569ccd3f6c17cbd4.jpg', NULL, NULL, NULL),
(40, 24, 'COMPAQ ELITE 8300', 'HP', 'Computador compaq corporativo', 'ea0da6ce4eacee00b.jpg', NULL, NULL, NULL),
(41, 17, 'M205', 'Epson', '<br>', '747d772870e46040b.png', NULL, NULL, NULL),
(42, 25, '16 GB', 'Kingston', '<br>', '7e9dcd983a3881a42.jpg', NULL, NULL, NULL),
(43, 25, '8 GB', 'Kingston', '<br>', '319dab040f1ed6c76.jpg', NULL, NULL, NULL),
(44, 26, 'NA', 'NA', '<br>', 'd725d05d62b91ec37.gif', NULL, NULL, NULL),
(45, 3, '21378A', 'PHILIPS', 'Sonda trasesofagica', 'ed6e962d9f505b765.jpg', NULL, NULL, NULL),
(46, 17, 'M200', 'EPSON', '<br>', '7a4747319b8736f47.png', NULL, NULL, NULL),
(47, 17, 'L3150', 'EPSON', '<br>', 'f4700cdac1b796249.jpg', NULL, NULL, NULL),
(48, 1, 'ECG ACQUISITION SYSTEM', 'DMS', 'Sistema de ECG paciente CardioScan', 'b19aec19eb79c178c.jpg', NULL, '2020-11-20 14:18:54', NULL),
(49, 10, 'THINKPAD T430', 'LENOVO', 'Para windows 7', '0beb55f6427682bb1.jpg', NULL, '2020-11-23 18:23:17', NULL),
(50, 27, 'LUA-L03', 'HUAWEI TECNOLOGIES CO', 'Telefono celular marca HUAWEI color negro&nbsp;<div>model: Huawei LUA-L03</div><div>IMEI: 869531025226337</div>', '74c59f6011deb1309.jpg', NULL, '2020-11-24 11:34:09', NULL),
(51, 3, 'BCY3-27IS', 'PHILIPS', '<br>', '9eefae72307d21027.jpeg', NULL, '2020-11-24 12:13:51', NULL),
(52, 10, 'THINKPAD L412', 'Lenovo', 'Portable&nbsp;', '7f5d59eabc0057779.jpg', NULL, '2020-11-28 12:29:56', NULL),
(53, 2, 'PM50+SPO', 'CONTEC MEDICAL', 'MAPA con SPO software 3.31 descargable desde la caja', 'f0f81c2e0e75b0ef8.jpg', NULL, '2020-11-28 12:42:46', NULL),
(54, 17, 'M100', 'EPSON', 'Impresora de tinta continua color negro&nbsp;', '310874c72166360fe.png', NULL, '2020-11-28 12:44:21', NULL),
(55, 32, 'SyncMaster 943snx', 'SAMSUNG', 'Monitor&nbsp;', '056a532f565bff575.jpg', NULL, '2020-11-28 12:47:06', NULL),
(56, 33, 'OPTIPLEX 3010', 'DELL', 'Torre de computador&nbsp;', '61df6c1916ab25ad9.jpg', NULL, '2020-11-28 12:49:31', NULL),
(57, 1, 'Bluethoot ECG Reciber / Transmiter', 'DMSoftware', 'Stress test Bluetooth Completo emisor y receptor&nbsp;', '754c4e9fd47ece327.jpg', NULL, '2020-11-28 13:08:55', NULL),
(58, 3, 'SONOS 7500', NULL, 'ECO GRAFO SONOS 7500<div>S/N&nbsp; USN0421329</div><div>MARCA PHILIPS<br><div><br></div><div><br></div></div>', '422527d658c88ee3a.jpg', NULL, '2020-12-01 10:12:20', NULL),
(59, 23, 'CARDIO AID 200', 'ARTEMA', 'Desfibrilador&nbsp;', '80fe5f314e45e680e.jpg', NULL, '2020-12-02 08:49:03', NULL),
(60, 33, 'LATITUDE - ULTRABOOK', 'DELL', '<br>', '802f2ba3d0e7a6ca4.jpg', NULL, '2020-12-15 17:12:35', 'andresl'),
(61, 35, 'MG-500', 'Magom', '<br>', '46cd83b13c144cf24.jpeg', NULL, '2021-01-07 10:57:01', 'felipem'),
(62, 10, 'Ideapad S145', NULL, '<br>', '3715a59833f670c5f.png', NULL, '2021-01-08 10:56:17', 'andresl'),
(63, 24, 'SMART TANK 515 PLUS', NULL, '<br>', '864dc9d74efb65ba1.jpg', NULL, '2021-01-08 13:26:39', 'andresl'),
(64, 32, 'SAMSUNG XPRESS M2070W', NULL, '<br>', '1ec15b014c555d374.jpg', NULL, '2021-01-08 13:36:56', 'andresl'),
(65, 36, 'GM', 'NA', 'Termo Higrometro&nbsp;', '52d8bd474d8002572.jpg', NULL, '2021-01-25 18:05:35', 'felipem'),
(66, 10, 'THINKPAD X131e', NULL, '<br>', '7af52077f08bac30b.jpg', NULL, '2021-01-26 10:11:43', 'a.lopez'),
(67, 37, 'GMD KIT', 'GMD', 'Kit de tensiómetro con fonendoscopio', 'dfb4c027732ef66f8.gif', NULL, '2021-01-27 13:05:13', 'felipem'),
(68, 39, 'AC1750 MU-MIMO', 'LYNKSYS', '<br>', 'c1b6652fae548be50.jpeg', NULL, '2021-02-23 09:55:09', 'felipem'),
(69, 40, 'STPT600', 'STANLEY', '<br>', '14acc37beaeaf8e48.jpg', NULL, '2021-02-23 10:18:06', 'felipem'),
(70, 24, 'ProBook', NULL, '<br>', '8312411d2839b849c.jpg', NULL, '2021-02-23 10:34:28', 'a.lopez'),
(71, 29, 'PENTIUM DUAL CORE', 'JANUS', '<br>', 'b389a395040fd620c.PNG', NULL, '2021-04-06 17:22:32', 'a.lopez'),
(72, 38, 'SONOS 5500', 'SONOS', '<br>', 'c559560ddca1b5f61.jpg', NULL, '2021-04-08 14:55:34', 'a.lopez');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership_grouppermissions`
--

CREATE TABLE `membership_grouppermissions` (
  `permissionID` int(10) UNSIGNED NOT NULL,
  `groupID` int(10) UNSIGNED DEFAULT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `allowInsert` tinyint(4) NOT NULL DEFAULT '0',
  `allowView` tinyint(4) NOT NULL DEFAULT '0',
  `allowEdit` tinyint(4) NOT NULL DEFAULT '0',
  `allowDelete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `membership_grouppermissions`
--

INSERT INTO `membership_grouppermissions` (`permissionID`, `groupID`, `tableName`, `allowInsert`, `allowView`, `allowEdit`, `allowDelete`) VALUES
(1, 2, 'dispositivos', 1, 3, 3, 3),
(2, 2, 'compras', 1, 3, 3, 3),
(3, 2, 'contactos', 1, 3, 3, 3),
(4, 2, 'movimientos', 1, 3, 3, 3),
(5, 2, 'documentos', 1, 3, 3, 3),
(6, 2, 'mantenimientos', 1, 3, 3, 3),
(7, 2, 'calibraciones', 1, 3, 3, 3),
(8, 2, 'mobiliario', 1, 3, 3, 3),
(9, 2, 'unidades', 1, 3, 3, 3),
(10, 2, 'marcas', 1, 3, 3, 3),
(11, 2, 'marca_modelo', 1, 3, 3, 3),
(12, 2, 'accesorios', 1, 3, 3, 3),
(13, 2, 'ciudades', 1, 3, 3, 3),
(14, 2, 'tipo_relacion', 1, 3, 3, 3),
(15, 2, 'tipo_dispositivo', 1, 3, 3, 3),
(16, 2, 'tipo_documento', 1, 3, 3, 3),
(17, 2, 'tipo_iden', 1, 3, 3, 3),
(18, 2, 'tipo_razon_social', 1, 3, 3, 3),
(19, 2, 'tipo_contacto', 1, 3, 3, 3),
(20, 2, 'tipo_mobiliario', 1, 3, 3, 3),
(21, 2, 'tipo_estado_dispo', 1, 3, 3, 3),
(22, 2, 'grupo_dispo', 1, 3, 3, 3),
(23, 2, 'tipo_movimiento', 1, 3, 3, 3),
(24, 2, 'tipo_estado_movi', 1, 3, 3, 3),
(25, 2, 'tipo_estado_verifica', 1, 3, 3, 3),
(26, 2, 'tipo_mtto', 1, 3, 3, 3),
(27, 2, 'tipo_calibracion', 1, 3, 3, 3),
(28, 2, 'imagenes', 1, 3, 3, 3),
(1066, 3, 'tipo_rh', 0, 0, 0, 0),
(1065, 3, 'tipo_documento_ss', 0, 0, 0, 0),
(1064, 3, 'tipo_fuera_servicio', 0, 0, 0, 0),
(1063, 3, 'accesorio_dispo_relacion', 0, 0, 0, 0),
(1062, 3, 'foto_observacion', 1, 3, 3, 1),
(1061, 3, 'tipo_estado_observaciones', 0, 3, 0, 0),
(1060, 3, 'tipo_formatos', 0, 3, 3, 0),
(1059, 3, 'dispo_detalles_tecnicos', 0, 3, 3, 0),
(1058, 3, 'tipo_accesorio', 0, 0, 0, 0),
(1057, 3, 'movi_recepcion', 1, 3, 3, 1),
(1056, 3, 'movi_envio', 1, 3, 3, 1),
(1055, 3, 'tipo_grupo_mobilia', 0, 0, 0, 0),
(1054, 3, 'tipo_calibracion', 0, 3, 0, 0),
(1053, 3, 'tipo_mtto', 0, 3, 0, 0),
(479, 3, 'fichatecnica', 0, 0, 0, 0),
(1052, 3, 'tipo_estado_verifica', 0, 3, 0, 0),
(1051, 3, 'tipo_estado_movi', 0, 3, 0, 0),
(1050, 3, 'tipo_movimiento', 0, 3, 0, 0),
(1049, 3, 'observaciones', 1, 3, 3, 1),
(1048, 3, 'tipo_estado_dispo', 1, 3, 0, 0),
(1047, 3, 'tipo_mobiliario', 1, 3, 0, 0),
(1046, 3, 'tipo_contacto', 0, 3, 0, 0),
(642, 4, 'tipo_iden', 0, 0, 0, 0),
(641, 4, 'tipo_documento', 0, 3, 0, 0),
(640, 4, 'tipo_dispositivo', 0, 3, 0, 0),
(639, 4, 'tipo_relacion', 0, 3, 0, 0),
(638, 4, 'grupo_dispo', 0, 0, 0, 0),
(637, 4, 'facturas', 0, 0, 0, 0),
(636, 4, 'docu_modelo', 0, 0, 0, 0),
(635, 4, 'prestador', 0, 0, 0, 0),
(634, 4, 'financiero_mobiliario', 0, 0, 0, 0),
(633, 4, 'financiero', 0, 0, 0, 0),
(632, 4, 'hojadevida', 0, 0, 0, 0),
(631, 4, 'verificacion', 0, 0, 0, 0),
(630, 4, 'imagenes', 1, 3, 2, 2),
(629, 4, 'ciudades', 1, 3, 2, 2),
(628, 4, 'accesorios', 1, 3, 2, 2),
(627, 4, 'marca_modelo', 1, 3, 2, 2),
(626, 4, 'marcas', 1, 3, 2, 2),
(625, 4, 'unidades', 0, 3, 0, 0),
(624, 4, 'calibraciones', 1, 3, 2, 2),
(623, 4, 'mantenimientos', 1, 3, 2, 2),
(622, 4, 'documentos', 1, 3, 2, 2),
(621, 4, 'movimientos', 1, 3, 2, 2),
(620, 4, 'contactos', 1, 3, 2, 2),
(619, 4, 'compras', 1, 3, 2, 2),
(618, 4, 'mobiliario', 1, 3, 2, 2),
(617, 4, 'dispositivos', 0, 3, 2, 2),
(680, 5, 'tipo_iden', 1, 2, 2, 2),
(679, 5, 'tipo_documento', 1, 2, 2, 2),
(678, 5, 'tipo_dispositivo', 1, 2, 2, 2),
(677, 5, 'tipo_relacion', 1, 2, 2, 2),
(676, 5, 'grupo_dispo', 1, 2, 2, 2),
(675, 5, 'facturas', 0, 0, 0, 0),
(674, 5, 'docu_modelo', 0, 0, 0, 0),
(673, 5, 'prestador', 0, 0, 0, 0),
(672, 5, 'financiero_mobiliario', 0, 0, 0, 0),
(671, 5, 'financiero', 0, 0, 0, 0),
(670, 5, 'hojadevida', 0, 0, 0, 0),
(669, 5, 'verificacion', 0, 0, 0, 0),
(668, 5, 'imagenes', 1, 2, 2, 2),
(667, 5, 'ciudades', 1, 2, 2, 2),
(666, 5, 'accesorios', 1, 2, 2, 2),
(665, 5, 'marca_modelo', 1, 2, 2, 2),
(664, 5, 'marcas', 1, 2, 2, 2),
(663, 5, 'unidades', 1, 2, 2, 2),
(662, 5, 'calibraciones', 1, 2, 2, 2),
(661, 5, 'mantenimientos', 1, 2, 2, 2),
(660, 5, 'documentos', 1, 2, 2, 2),
(659, 5, 'movimientos', 1, 2, 2, 2),
(658, 5, 'contactos', 1, 2, 2, 2),
(657, 5, 'compras', 1, 2, 2, 2),
(656, 5, 'mobiliario', 1, 2, 2, 2),
(655, 5, 'dispositivos', 0, 2, 2, 2),
(1045, 3, 'tipo_razon_social', 0, 3, 0, 0),
(1044, 3, 'tipo_iden', 0, 3, 0, 0),
(1043, 3, 'tipo_documento', 1, 3, 3, 0),
(1042, 3, 'tipo_dispositivo', 1, 3, 3, 0),
(1041, 3, 'tipo_relacion', 0, 3, 0, 0),
(393, 6, 'tipo_calibracion', 0, 3, 0, 0),
(392, 6, 'tipo_mtto', 0, 3, 0, 0),
(391, 6, 'tipo_estado_verifica', 0, 3, 0, 0),
(390, 6, 'tipo_estado_movi', 0, 3, 0, 0),
(389, 6, 'tipo_movimiento', 0, 3, 0, 0),
(388, 6, 'grupo_dispo', 0, 3, 0, 0),
(387, 6, 'tipo_estado_dispo', 0, 3, 0, 0),
(386, 6, 'tipo_mobiliario', 0, 3, 0, 0),
(385, 6, 'tipo_contacto', 0, 3, 0, 0),
(384, 6, 'tipo_razon_social', 0, 3, 0, 0),
(383, 6, 'tipo_iden', 0, 3, 0, 0),
(382, 6, 'tipo_documento', 0, 3, 0, 0),
(381, 6, 'tipo_dispositivo', 0, 3, 0, 0),
(380, 6, 'tipo_relacion', 0, 3, 0, 0),
(379, 6, 'ciudades', 0, 3, 0, 0),
(378, 6, 'accesorios', 0, 3, 0, 0),
(377, 6, 'marca_modelo', 0, 3, 0, 0),
(376, 6, 'marcas', 0, 3, 0, 0),
(370, 6, 'movimientos', 1, 3, 0, 0),
(371, 6, 'documentos', 0, 3, 0, 0),
(372, 6, 'mantenimientos', 0, 3, 0, 0),
(373, 6, 'calibraciones', 0, 3, 0, 0),
(374, 6, 'mobiliario', 0, 3, 0, 0),
(375, 6, 'unidades', 0, 3, 0, 0),
(369, 6, 'contactos', 0, 3, 0, 0),
(368, 6, 'compras', 0, 3, 0, 0),
(367, 6, 'dispositivos', 0, 3, 0, 0),
(365, 2, 'codigoserial', 1, 3, 3, 3),
(366, 2, 'verificacion', 1, 3, 3, 3),
(394, 6, 'imagenes', 0, 3, 0, 0),
(395, 6, 'codigoserial', 0, 0, 0, 0),
(396, 6, 'verificacion', 1, 3, 3, 3),
(397, 2, 'tipo_grupo_mobilia', 1, 3, 3, 3),
(398, 2, 'docu_modelo', 1, 3, 3, 3),
(399, 2, 'fichatecnica', 1, 3, 3, 3),
(1040, 3, 'grupo_dispo', 0, 3, 0, 0),
(1039, 3, 'dispo_fuera', 0, 0, 0, 0),
(1037, 3, 'facturas', 1, 3, 2, 1),
(432, 7, 'dispositivos', 0, 3, 0, 0),
(433, 7, 'mobiliario', 0, 0, 0, 0),
(434, 7, 'compras', 0, 0, 0, 0),
(435, 7, 'contactos', 0, 0, 0, 0),
(436, 7, 'movimientos', 0, 0, 0, 0),
(437, 7, 'documentos', 1, 3, 3, 3),
(438, 7, 'mantenimientos', 0, 0, 0, 0),
(439, 7, 'calibraciones', 0, 0, 0, 0),
(440, 7, 'unidades', 0, 0, 0, 0),
(441, 7, 'marcas', 0, 0, 0, 0),
(442, 7, 'marca_modelo', 0, 0, 0, 0),
(443, 7, 'accesorios', 0, 0, 0, 0),
(444, 7, 'ciudades', 0, 0, 0, 0),
(445, 7, 'imagenes', 0, 0, 0, 0),
(446, 7, 'verificacion', 0, 0, 0, 0),
(447, 7, 'fichatecnica', 0, 0, 0, 0),
(448, 7, 'docu_modelo', 0, 0, 0, 0),
(449, 7, 'tipo_relacion', 0, 0, 0, 0),
(450, 7, 'tipo_dispositivo', 0, 0, 0, 0),
(451, 7, 'tipo_documento', 0, 0, 0, 0),
(452, 7, 'tipo_iden', 0, 0, 0, 0),
(453, 7, 'tipo_razon_social', 0, 0, 0, 0),
(454, 7, 'tipo_contacto', 0, 0, 0, 0),
(455, 7, 'tipo_mobiliario', 0, 0, 0, 0),
(456, 7, 'tipo_estado_dispo', 0, 0, 0, 0),
(457, 7, 'grupo_dispo', 0, 0, 0, 0),
(458, 7, 'tipo_movimiento', 0, 0, 0, 0),
(459, 7, 'tipo_estado_movi', 0, 0, 0, 0),
(460, 7, 'tipo_estado_verifica', 0, 0, 0, 0),
(461, 7, 'tipo_mtto', 0, 0, 0, 0),
(462, 7, 'tipo_calibracion', 0, 0, 0, 0),
(463, 7, 'tipo_grupo_mobilia', 0, 0, 0, 0),
(756, 3, 'imagenes', 1, 3, 0, 0),
(496, 2, 'hojadevida', 1, 3, 3, 3),
(497, 2, 'financiero', 1, 3, 3, 3),
(498, 2, 'financiero_mobiliario', 1, 3, 3, 3),
(499, 2, 'prestador', 1, 3, 3, 3),
(500, 2, 'movi_envio', 1, 3, 3, 3),
(501, 2, 'movi_recepcion', 1, 3, 3, 3),
(502, 2, 'facturas', 1, 3, 3, 3),
(1038, 3, 'formatos', 0, 0, 0, 0),
(1036, 3, 'docu_modelo', 0, 0, 0, 0),
(1035, 3, 'prestador', 0, 0, 0, 0),
(1034, 3, 'financiero_mobiliario', 0, 0, 0, 0),
(1033, 3, 'financiero', 0, 0, 0, 0),
(1032, 3, 'hojadevida', 0, 0, 0, 0),
(643, 4, 'tipo_razon_social', 0, 3, 0, 0),
(644, 4, 'tipo_contacto', 0, 0, 0, 0),
(645, 4, 'tipo_mobiliario', 0, 0, 0, 0),
(646, 4, 'tipo_estado_dispo', 0, 3, 0, 0),
(647, 4, 'tipo_movimiento', 0, 3, 0, 0),
(648, 4, 'tipo_estado_movi', 0, 3, 0, 0),
(649, 4, 'tipo_estado_verifica', 0, 3, 0, 0),
(650, 4, 'tipo_mtto', 0, 3, 0, 0),
(651, 4, 'tipo_calibracion', 0, 3, 0, 0),
(652, 4, 'tipo_grupo_mobilia', 0, 0, 0, 0),
(653, 4, 'movi_envio', 0, 0, 0, 0),
(654, 4, 'movi_recepcion', 0, 0, 0, 0),
(681, 5, 'tipo_razon_social', 1, 2, 2, 2),
(682, 5, 'tipo_contacto', 1, 2, 2, 2),
(683, 5, 'tipo_mobiliario', 1, 2, 2, 2),
(684, 5, 'tipo_estado_dispo', 1, 2, 2, 2),
(685, 5, 'tipo_movimiento', 1, 2, 2, 2),
(686, 5, 'tipo_estado_movi', 1, 2, 2, 2),
(687, 5, 'tipo_estado_verifica', 1, 2, 2, 2),
(688, 5, 'tipo_mtto', 1, 2, 2, 2),
(689, 5, 'tipo_calibracion', 1, 2, 2, 2),
(690, 5, 'tipo_grupo_mobilia', 0, 0, 0, 0),
(691, 5, 'movi_envio', 0, 0, 0, 0),
(692, 5, 'movi_recepcion', 0, 0, 0, 0),
(693, 2, 'formatos', 1, 3, 3, 3),
(694, 2, 'dispo_detalles_tecnicos', 1, 3, 3, 3),
(695, 2, 'tipo_formatos', 1, 3, 3, 3),
(696, 2, 'observaciones', 1, 3, 3, 3),
(697, 2, 'tipo_estado_observaciones', 1, 3, 3, 3),
(698, 2, 'foto_observacion', 1, 3, 3, 3),
(1031, 3, 'verificacion', 1, 3, 3, 1),
(1030, 3, 'ciudades', 0, 3, 0, 0),
(1029, 3, 'accesorios', 1, 3, 3, 2),
(1028, 3, 'marca_modelo', 1, 3, 1, 1),
(1027, 3, 'marcas', 1, 3, 0, 0),
(1026, 3, 'unidades', 0, 3, 1, 1),
(787, 2, 'dispo_fuera', 1, 3, 3, 3),
(788, 2, 'tipo_accesorio', 1, 3, 3, 3),
(789, 2, 'progreso', 1, 3, 3, 3),
(790, 2, 'accesorio_dispo_relacion', 1, 3, 3, 3),
(791, 2, 'tipo_fuera_servicio', 1, 3, 3, 3),
(792, 2, 'acceso_remoto', 1, 3, 3, 3),
(1025, 3, 'calibraciones', 1, 3, 3, 1),
(1024, 3, 'mantenimientos', 1, 3, 3, 1),
(1023, 3, 'documentos', 1, 3, 3, 3),
(1022, 3, 'movimientos', 1, 3, 3, 3),
(1021, 3, 'contactos', 1, 3, 3, 1),
(1020, 3, 'compras', 1, 3, 3, 1),
(1019, 3, 'mobiliario', 1, 3, 3, 1),
(1018, 3, 'dispositivos', 0, 3, 3, 1),
(891, 2, 'tipo_documento_ss', 1, 3, 3, 3),
(892, 2, 'tipo_rh', 1, 3, 3, 3),
(893, 2, 'tipo_estado_observa', 1, 3, 3, 3),
(894, 2, 'perfil', 1, 3, 3, 3),
(895, 2, 'consentimieto', 1, 3, 3, 3),
(896, 2, 'perfil_familia', 1, 3, 3, 3),
(897, 2, 'tipo_parentesco', 1, 3, 3, 3),
(898, 2, 'perfil_soportes', 1, 3, 3, 3),
(899, 2, 'seguridad_social', 1, 3, 3, 3),
(1118, 8, 'dispo_detalles_tecnicos', 0, 3, 0, 0),
(1117, 8, 'tipo_accesorio', 0, 0, 0, 0),
(1116, 8, 'movi_recepcion', 0, 3, 0, 0),
(1115, 8, 'movi_envio', 0, 0, 0, 0),
(1114, 8, 'tipo_grupo_mobilia', 0, 3, 0, 0),
(1113, 8, 'tipo_calibracion', 0, 0, 0, 0),
(1112, 8, 'tipo_mtto', 0, 3, 0, 0),
(1111, 8, 'tipo_estado_verifica', 0, 0, 0, 0),
(1110, 8, 'tipo_estado_movi', 0, 3, 0, 0),
(1109, 8, 'tipo_movimiento', 0, 3, 0, 0),
(1108, 8, 'observaciones', 0, 3, 0, 0),
(1107, 8, 'tipo_estado_dispo', 0, 3, 0, 0),
(1106, 8, 'tipo_mobiliario', 0, 3, 0, 0),
(1105, 8, 'tipo_contacto', 0, 3, 0, 0),
(1104, 8, 'tipo_razon_social', 0, 3, 0, 0),
(1103, 8, 'tipo_iden', 0, 0, 0, 0),
(1102, 8, 'tipo_documento', 0, 3, 0, 0),
(1101, 8, 'tipo_dispositivo', 0, 3, 0, 0),
(1100, 8, 'tipo_relacion', 0, 0, 0, 0),
(1099, 8, 'grupo_dispo', 0, 0, 0, 0),
(1098, 8, 'dispo_fuera', 1, 3, 1, 1),
(1097, 8, 'formatos', 0, 0, 0, 0),
(1096, 8, 'facturas', 0, 3, 0, 0),
(1095, 8, 'docu_modelo', 0, 0, 0, 0),
(1094, 8, 'prestador', 0, 0, 0, 0),
(1093, 8, 'financiero_mobiliario', 0, 0, 0, 0),
(1092, 8, 'financiero', 0, 0, 0, 0),
(1091, 8, 'hojadevida', 0, 3, 3, 0),
(1090, 8, 'verificacion', 1, 3, 1, 1),
(1089, 8, 'ciudades', 0, 0, 0, 0),
(1088, 8, 'accesorios', 0, 3, 0, 0),
(1087, 8, 'marca_modelo', 0, 3, 0, 0),
(1086, 8, 'marcas', 0, 3, 0, 0),
(1085, 8, 'unidades', 0, 3, 0, 0),
(1084, 8, 'calibraciones', 1, 3, 3, 1),
(1083, 8, 'mantenimientos', 1, 3, 3, 1),
(1082, 8, 'documentos', 0, 0, 0, 0),
(1081, 8, 'movimientos', 1, 3, 0, 0),
(1080, 8, 'contactos', 0, 0, 0, 0),
(1079, 8, 'compras', 0, 0, 0, 0),
(1078, 8, 'mobiliario', 0, 0, 0, 0),
(1077, 8, 'dispositivos', 0, 3, 0, 0),
(958, 2, 'costos_mtto', 1, 3, 3, 3),
(959, 9, 'dispositivos', 0, 3, 1, 0),
(960, 9, 'mobiliario', 0, 3, 1, 0),
(961, 9, 'compras', 0, 3, 1, 0),
(962, 9, 'contactos', 0, 3, 1, 0),
(963, 9, 'movimientos', 0, 3, 1, 0),
(964, 9, 'documentos', 0, 3, 1, 0),
(965, 9, 'mantenimientos', 0, 3, 1, 0),
(966, 9, 'calibraciones', 0, 3, 1, 0),
(967, 9, 'unidades', 0, 3, 1, 0),
(968, 9, 'marcas', 0, 3, 1, 0),
(969, 9, 'marca_modelo', 0, 3, 1, 0),
(970, 9, 'accesorios', 0, 3, 1, 0),
(971, 9, 'ciudades', 0, 3, 1, 0),
(972, 9, 'verificacion', 0, 3, 1, 0),
(973, 9, 'hojadevida', 0, 3, 1, 0),
(974, 9, 'financiero', 0, 3, 1, 0),
(975, 9, 'financiero_mobiliario', 0, 3, 1, 0),
(976, 9, 'prestador', 0, 3, 1, 0),
(977, 9, 'docu_modelo', 0, 3, 1, 0),
(978, 9, 'facturas', 0, 3, 1, 0),
(979, 9, 'formatos', 0, 3, 1, 0),
(980, 9, 'dispo_fuera', 0, 3, 1, 0),
(981, 9, 'grupo_dispo', 0, 3, 1, 0),
(982, 9, 'tipo_relacion', 0, 3, 1, 0),
(983, 9, 'tipo_dispositivo', 0, 3, 1, 0),
(984, 9, 'tipo_documento', 0, 3, 1, 0),
(985, 9, 'tipo_iden', 0, 3, 1, 0),
(986, 9, 'tipo_razon_social', 0, 3, 1, 0),
(987, 9, 'tipo_contacto', 0, 3, 1, 0),
(988, 9, 'tipo_mobiliario', 0, 3, 1, 0),
(989, 9, 'tipo_estado_dispo', 0, 3, 1, 0),
(990, 9, 'observaciones', 0, 3, 1, 0),
(991, 9, 'tipo_movimiento', 0, 3, 1, 0),
(992, 9, 'tipo_estado_movi', 0, 3, 1, 0),
(993, 9, 'tipo_estado_verifica', 0, 3, 1, 0),
(994, 9, 'tipo_mtto', 0, 3, 1, 0),
(995, 9, 'tipo_calibracion', 0, 3, 1, 0),
(996, 9, 'tipo_grupo_mobilia', 0, 3, 1, 0),
(997, 9, 'movi_envio', 0, 3, 1, 0),
(998, 9, 'movi_recepcion', 0, 3, 1, 0),
(999, 9, 'tipo_accesorio', 0, 3, 1, 0),
(1000, 9, 'dispo_detalles_tecnicos', 0, 3, 1, 0),
(1001, 9, 'tipo_formatos', 0, 3, 1, 0),
(1002, 9, 'tipo_estado_observaciones', 0, 3, 1, 0),
(1003, 9, 'foto_observacion', 0, 3, 1, 0),
(1004, 9, 'accesorio_dispo_relacion', 0, 3, 1, 0),
(1005, 9, 'tipo_fuera_servicio', 0, 3, 1, 0),
(1006, 9, 'tipo_documento_ss', 0, 3, 1, 0),
(1007, 9, 'tipo_rh', 0, 3, 1, 0),
(1008, 9, 'progreso', 0, 3, 1, 0),
(1009, 9, 'tipo_estado_observa', 0, 3, 1, 0),
(1010, 9, 'acceso_remoto', 0, 3, 1, 0),
(1011, 9, 'perfil', 0, 3, 1, 0),
(1012, 9, 'consentimieto', 0, 3, 1, 0),
(1013, 9, 'perfil_familia', 0, 3, 1, 0),
(1014, 9, 'tipo_parentesco', 0, 3, 1, 0),
(1015, 9, 'perfil_soportes', 0, 3, 1, 0),
(1016, 9, 'seguridad_social', 0, 3, 1, 0),
(1017, 9, 'costos_mtto', 0, 3, 1, 0),
(1067, 3, 'progreso', 0, 0, 0, 0),
(1068, 3, 'tipo_estado_observa', 0, 0, 0, 0),
(1069, 3, 'acceso_remoto', 1, 3, 3, 3),
(1070, 3, 'perfil', 0, 0, 0, 0),
(1071, 3, 'consentimieto', 0, 0, 0, 0),
(1072, 3, 'perfil_familia', 0, 0, 0, 0),
(1073, 3, 'tipo_parentesco', 0, 0, 0, 0),
(1074, 3, 'perfil_soportes', 0, 0, 0, 0),
(1075, 3, 'seguridad_social', 0, 0, 0, 0),
(1076, 3, 'costos_mtto', 1, 3, 3, 1),
(1119, 8, 'tipo_formatos', 0, 0, 0, 0),
(1120, 8, 'tipo_estado_observaciones', 0, 3, 0, 0),
(1121, 8, 'foto_observacion', 0, 3, 0, 0),
(1122, 8, 'accesorio_dispo_relacion', 0, 0, 0, 0),
(1123, 8, 'tipo_fuera_servicio', 0, 0, 0, 0),
(1124, 8, 'tipo_documento_ss', 0, 0, 0, 0),
(1125, 8, 'tipo_rh', 0, 0, 0, 0),
(1126, 8, 'progreso', 0, 0, 0, 0),
(1127, 8, 'tipo_estado_observa', 0, 0, 0, 0),
(1128, 8, 'acceso_remoto', 0, 0, 0, 0),
(1129, 8, 'perfil', 0, 3, 0, 0),
(1130, 8, 'consentimieto', 0, 3, 0, 0),
(1131, 8, 'perfil_familia', 0, 3, 0, 0),
(1132, 8, 'tipo_parentesco', 0, 3, 0, 0),
(1133, 8, 'perfil_soportes', 0, 3, 0, 0),
(1134, 8, 'seguridad_social', 0, 3, 0, 0),
(1135, 8, 'costos_mtto', 1, 3, 3, 1),
(1136, 2, 'tipo_costo', 1, 3, 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership_groups`
--

CREATE TABLE `membership_groups` (
  `groupID` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  `allowSignup` tinyint(4) DEFAULT NULL,
  `needsApproval` tinyint(4) DEFAULT NULL,
  `allowCSVImport` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `membership_groups`
--

INSERT INTO `membership_groups` (`groupID`, `name`, `description`, `allowSignup`, `needsApproval`, `allowCSVImport`) VALUES
(1, 'anonymous', 'Anonymous group created automatically on 2019-11-26', 0, 0, 0),
(2, 'Admins', 'Admin group created automatically on 2019-11-26', 0, 1, 0),
(3, 'APOYO EN TECNOLOGÍA', 'Apoyo en Auditoria y seguimiento de equipos', 0, 1, 0),
(4, 'COMPRAS', 'Registros de compras', 0, 1, 0),
(5, 'BODEGA', 'Bodega de insumos', 0, 1, 0),
(6, 'ENFERMEROS', 'Apoyo administrativo del personal de enfermería.', 1, 1, 0),
(7, 'INGENIEROS', 'Ingenieros Biomedicos', 0, 1, 0),
(8, 'MANTENIMIENTO', 'Reportes de mantenimiento', 0, 1, 0),
(9, 'AUDITORA', '', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership_userpermissions`
--

CREATE TABLE `membership_userpermissions` (
  `permissionID` int(10) UNSIGNED NOT NULL,
  `memberID` varchar(100) NOT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `allowInsert` tinyint(4) NOT NULL DEFAULT '0',
  `allowView` tinyint(4) NOT NULL DEFAULT '0',
  `allowEdit` tinyint(4) NOT NULL DEFAULT '0',
  `allowDelete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `membership_userpermissions`
--

INSERT INTO `membership_userpermissions` (`permissionID`, `memberID`, `tableName`, `allowInsert`, `allowView`, `allowEdit`, `allowDelete`) VALUES
(80, 'c.zambrano', 'tipo_estado_movi', 0, 3, 0, 0),
(79, 'c.zambrano', 'tipo_movimiento', 0, 3, 0, 0),
(78, 'c.zambrano', 'grupo_dispo', 0, 3, 0, 0),
(77, 'c.zambrano', 'tipo_estado_dispo', 0, 3, 0, 0),
(76, 'c.zambrano', 'tipo_mobiliario', 0, 3, 0, 0),
(75, 'c.zambrano', 'tipo_contacto', 0, 3, 0, 0),
(74, 'c.zambrano', 'tipo_razon_social', 0, 3, 0, 0),
(73, 'c.zambrano', 'tipo_iden', 0, 3, 0, 0),
(72, 'c.zambrano', 'tipo_documento', 1, 3, 3, 0),
(71, 'c.zambrano', 'tipo_dispositivo', 1, 3, 3, 0),
(70, 'c.zambrano', 'tipo_relacion', 0, 3, 0, 0),
(69, 'c.zambrano', 'ciudades', 1, 3, 3, 1),
(68, 'c.zambrano', 'accesorios', 1, 3, 3, 2),
(67, 'c.zambrano', 'marca_modelo', 1, 3, 3, 3),
(66, 'c.zambrano', 'marcas', 1, 3, 3, 3),
(65, 'c.zambrano', 'unidades', 1, 3, 3, 1),
(64, 'c.zambrano', 'mobiliario', 1, 3, 3, 1),
(63, 'c.zambrano', 'calibraciones', 1, 3, 3, 1),
(62, 'c.zambrano', 'mantenimientos', 1, 3, 3, 1),
(61, 'c.zambrano', 'documentos', 1, 3, 3, 2),
(60, 'c.zambrano', 'movimientos', 1, 3, 3, 2),
(59, 'c.zambrano', 'contactos', 1, 3, 3, 1),
(58, 'c.zambrano', 'compras', 1, 3, 3, 1),
(57, 'c.zambrano', 'dispositivos', 1, 3, 3, 1),
(81, 'c.zambrano', 'tipo_estado_verifica', 0, 3, 0, 0),
(82, 'c.zambrano', 'tipo_mtto', 0, 3, 0, 0),
(83, 'c.zambrano', 'tipo_calibracion', 0, 3, 0, 0),
(84, 'c.zambrano', 'imagenes', 0, 3, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership_userrecords`
--

CREATE TABLE `membership_userrecords` (
  `recID` bigint(20) UNSIGNED NOT NULL,
  `tableName` varchar(100) DEFAULT NULL,
  `pkValue` varchar(255) DEFAULT NULL,
  `memberID` varchar(100) DEFAULT NULL,
  `dateAdded` bigint(20) UNSIGNED DEFAULT NULL,
  `dateUpdated` bigint(20) UNSIGNED DEFAULT NULL,
  `groupID` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `membership_userrecords`
--

INSERT INTO `membership_userrecords` (`recID`, `tableName`, `pkValue`, `memberID`, `dateAdded`, `dateUpdated`, `groupID`) VALUES
(1, 'marcas', '1', 'felipem', 1574829510, 1590176307, 2),
(2, 'marcas', '2', 'felipem', 1574829531, 1590176396, 2),
(3, 'marcas', '3', 'felipem', 1574829584, 1574829584, 2),
(4, 'marca_modelo', '1', 'felipem', 1574829694, 1577399542, 2),
(5, 'marca_modelo', '2', 'felipem', 1574829745, 1577399923, 2),
(6, 'marcas', '4', 'felipem', 1574829808, 1583334227, 2),
(7, 'ciudades', '1', 'felipem', 1574830554, 1574830554, 2),
(8, 'ciudades', '2', 'felipem', 1574830609, 1574830609, 2),
(9, 'ciudades', '3', 'felipem', 1574830626, 1574830626, 2),
(10, 'ciudades', '4', 'felipem', 1574830643, 1574830643, 2),
(11, 'tipo_relacion', '1', 'felipem', 1574830693, 1574830693, 2),
(12, 'tipo_relacion', '2', 'felipem', 1574830752, 1574830752, 2),
(13, 'tipo_documento', '1', 'felipem', 1574830863, 1574830863, 2),
(14, 'tipo_documento', '2', 'felipem', 1574830895, 1574830895, 2),
(15, 'tipo_documento', '3', 'felipem', 1574830953, 1574830953, 2),
(16, 'tipo_documento', '4', 'felipem', 1574831031, 1574831031, 2),
(17, 'tipo_documento', '5', 'felipem', 1574831081, 1574831081, 2),
(18, 'tipo_iden', '1', 'felipem', 1574834160, 1574834160, 2),
(19, 'tipo_iden', '2', 'felipem', 1574834252, 1574834252, 2),
(20, 'tipo_razon_social', '1', 'felipem', 1574834321, 1574834321, 2),
(21, 'tipo_razon_social', '2', 'felipem', 1574834373, 1574834373, 2),
(22, 'tipo_razon_social', '3', 'felipem', 1574834447, 1574834447, 2),
(23, 'tipo_movimiento', '1', 'felipem', 1574834517, 1605664735, 2),
(24, 'tipo_movimiento', '2', 'felipem', 1574834562, 1605664901, 2),
(25, 'tipo_estado_movi', '1', 'felipem', 1574834603, 1605664955, 2),
(26, 'tipo_estado_movi', '2', 'felipem', 1574834630, 1605664987, 2),
(27, 'tipo_estado_verifica', '1', 'felipem', 1574834704, 1574834704, 2),
(28, 'tipo_estado_verifica', '2', 'felipem', 1574834759, 1574834759, 2),
(29, 'tipo_mtto', '1', 'felipem', 1574834807, 1574834807, 2),
(30, 'tipo_mtto', '2', 'felipem', 1574834842, 1574834842, 2),
(31, 'tipo_calibracion', '1', 'felipem', 1574834902, 1574839002, 2),
(32, 'tipo_documento', '6', 'felipem', 1574834955, 1574834955, 2),
(33, 'tipo_documento', '7', 'felipem', 1574834975, 1574834975, 2),
(34, 'tipo_documento', '8', 'felipem', 1574835018, 1574835018, 2),
(35, 'tipo_documento', '9', 'felipem', 1574835065, 1574835065, 2),
(36, 'unidades', '1', 'felipem', 1574835252, 1603305246, 2),
(37, 'unidades', '2', 'felipem', 1574835293, 1606512694, 2),
(38, 'unidades', '3', 'felipem', 1574835336, 1574835797, 2),
(39, 'unidades', '4', 'felipem', 1574835373, 1611610033, 2),
(40, 'unidades', '5', 'felipem', 1574835421, 1574835906, 2),
(41, 'unidades', '6', 'felipem', 1574835472, 1613505115, 2),
(42, 'tipo_relacion', '3', 'felipem', 1574838334, 1574838334, 2),
(43, 'grupo_dispo', '1', 'felipem', 1574838397, 1574838397, 2),
(44, 'grupo_dispo', '2', 'felipem', 1574838417, 1574838417, 2),
(45, 'grupo_dispo', '3', 'felipem', 1574838483, 1574838483, 2),
(46, 'tipo_dispositivo', '1', 'felipem', 1574838498, 1606176729, 2),
(47, 'tipo_dispositivo', '2', 'felipem', 1574838508, 1606174418, 2),
(48, 'tipo_dispositivo', '3', 'felipem', 1574838519, 1606176742, 2),
(49, 'tipo_dispositivo', '4', 'felipem', 1574838530, 1602786210, 2),
(50, 'tipo_dispositivo', '5', 'felipem', 1574838549, 1602786033, 2),
(51, 'tipo_dispositivo', '6', 'felipem', 1574838571, 1606174929, 2),
(52, 'tipo_dispositivo', '7', 'felipem', 1574838584, 1606174631, 2),
(53, 'tipo_dispositivo', '8', 'felipem', 1574838598, 1606176394, 2),
(54, 'tipo_dispositivo', '9', 'felipem', 1574838609, 1606176462, 2),
(55, 'tipo_dispositivo', '10', 'felipem', 1574838654, 1602785911, 2),
(56, 'tipo_contacto', '1', 'felipem', 1574838724, 1574838724, 2),
(57, 'tipo_contacto', '2', 'felipem', 1574838738, 1574838738, 2),
(58, 'tipo_mobiliario', '1', 'felipem', 1574838781, 1574838781, 2),
(59, 'tipo_mobiliario', '2', 'felipem', 1574838792, 1574838792, 2),
(60, 'tipo_estado_dispo', '1', 'felipem', 1574838831, 1574838831, 2),
(61, 'tipo_estado_dispo', '2', 'felipem', 1574838892, 1574838892, 2),
(62, 'tipo_calibracion', '2', 'felipem', 1574839025, 1574839025, 2),
(63, 'imagenes', '1', 'felipem', 1574849750, 1574849750, 2),
(64, 'tipo_dispositivo', '11', 'felipem', 1574864502, 1606175890, 2),
(581, 'dispositivos', '239', 'felipem', 1590172053, 1614100719, 2),
(66, 'dispositivos', '1', 'felipem', 1574865457, 1589206081, 2),
(67, 'unidades', '7', 'felipem', 1574865611, 1612831254, 2),
(68, 'dispositivos', '2', 'felipem', 1574865639, 1618862474, 2),
(69, 'dispositivos', '3', 'felipem', 1574866234, 1615214193, 2),
(70, 'dispositivos', '4', 'felipem', 1574866234, 1617912779, 2),
(71, 'dispositivos', '5', 'felipem', 1574866234, 1589206439, 2),
(72, 'dispositivos', '6', 'felipem', 1574866234, 1618412596, 2),
(73, 'dispositivos', '7', 'felipem', 1574866234, 1589206483, 2),
(74, 'dispositivos', '8', 'felipem', 1574866234, 1611942843, 2),
(75, 'dispositivos', '9', 'felipem', 1574866234, 1614611072, 2),
(76, 'dispositivos', '10', 'felipem', 1574866234, 1589206548, 2),
(77, 'dispositivos', '11', 'felipem', 1574866234, 1617912640, 2),
(78, 'dispositivos', '12', 'felipem', 1574866234, 1589206583, 2),
(79, 'dispositivos', '13', 'felipem', 1574866234, 1611600198, 2),
(80, 'dispositivos', '14', 'felipem', 1574866234, 1589206616, 2),
(81, 'dispositivos', '15', 'felipem', 1574866234, 1577465988, 2),
(82, 'dispositivos', '16', 'felipem', 1574866234, 1589206668, 2),
(83, 'dispositivos', '17', 'felipem', 1574866234, 1618851389, 2),
(84, 'dispositivos', '18', 'felipem', 1574866234, 1606582913, 2),
(85, 'dispositivos', '19', 'felipem', 1574866234, 1589206983, 2),
(86, 'dispositivos', '20', 'felipem', 1574866234, 1618163332, 2),
(87, 'dispositivos', '21', 'felipem', 1574866234, 1583518343, 2),
(88, 'dispositivos', '22', 'felipem', 1574866234, 1583521444, 2),
(89, 'dispositivos', '23', 'felipem', 1574866234, 1614611427, 2),
(90, 'dispositivos', '24', 'felipem', 1574866234, 1612196890, 2),
(91, 'dispositivos', '25', 'felipem', 1574866234, 1583521519, 2),
(92, 'dispositivos', '26', 'felipem', 1574866234, 1614610458, 2),
(93, 'dispositivos', '27', 'felipem', 1574866234, 1583528275, 2),
(94, 'dispositivos', '28', 'felipem', 1574866234, 1583528887, 2),
(95, 'dispositivos', '29', 'felipem', 1574866234, 1574868549, 2),
(141, 'unidades', '9', 'felipem', 1575063919, 1603304809, 2),
(97, 'marca_modelo', '3', 'felipem', 1574866475, 1577399943, 2),
(98, 'marcas', '5', 'felipem', 1574866578, 1574866578, 2),
(99, 'marca_modelo', '4', 'felipem', 1574866600, 1577400000, 2),
(100, 'marcas', '6', 'felipem', 1574866615, 1574866615, 2),
(101, 'marca_modelo', '5', 'felipem', 1574866630, 1577400114, 2),
(102, 'dispositivos', '31', 'felipem', 1574867923, 1589207091, 2),
(103, 'dispositivos', '32', 'felipem', 1574867923, 1575007909, 2),
(104, 'dispositivos', '33', 'felipem', 1574867923, 1575007952, 2),
(105, 'dispositivos', '34', 'felipem', 1574867923, 1607091524, 2),
(106, 'dispositivos', '35', 'felipem', 1574867923, 1575008392, 2),
(107, 'dispositivos', '36', 'felipem', 1574867923, 1583464207, 2),
(108, 'dispositivos', '37', 'felipem', 1574867923, 1575008641, 2),
(109, 'dispositivos', '38', 'felipem', 1574867923, 1575008669, 2),
(110, 'dispositivos', '39', 'felipem', 1574867923, 1575008656, 2),
(111, 'dispositivos', '40', 'felipem', 1574867923, 1584811699, 2),
(112, 'dispositivos', '41', 'felipem', 1574867923, 1575008466, 2),
(113, 'dispositivos', '42', 'felipem', 1574867923, 1618332006, 2),
(114, 'dispositivos', '43', 'felipem', 1574867923, 1575008554, 2),
(115, 'dispositivos', '44', 'felipem', 1574867923, 1582992429, 2),
(116, 'dispositivos', '45', 'felipem', 1574867923, 1575008527, 2),
(117, 'dispositivos', '46', 'felipem', 1574867923, 1583365535, 2),
(118, 'dispositivos', '47', 'felipem', 1574867923, 1575008577, 2),
(119, 'dispositivos', '48', 'felipem', 1574867923, 1618585956, 2),
(120, 'dispositivos', '49', 'felipem', 1574867923, 1618412302, 2),
(121, 'dispositivos', '50', 'felipem', 1574867923, 1618412524, 2),
(122, 'dispositivos', '51', 'felipem', 1574867923, 1584808984, 2),
(123, 'dispositivos', '52', 'felipem', 1574867923, 1583464771, 2),
(124, 'dispositivos', '53', 'felipem', 1574867923, 1583526201, 2),
(125, 'dispositivos', '54', 'felipem', 1574867923, 1618357602, 2),
(126, 'dispositivos', '55', 'felipem', 1574867923, 1575522391, 2),
(127, 'imagenes', '2', 'felipem', 1574868316, 1574868316, 2),
(128, 'ciudades', '5', 'felipem', 1574869823, 1574869823, 2),
(129, 'ciudades', '6', 'felipem', 1574869842, 1574869842, 2),
(130, 'ciudades', '7', 'felipem', 1574869849, 1574869849, 2),
(131, 'marcas', '7', 'felipem', 1574895268, 1574895268, 2),
(132, 'marca_modelo', '6', 'felipem', 1574895347, 1577400170, 2),
(133, 'unidades', '8', 'v.ocampo', 1574897330, 1574897330, 3),
(134, 'dispositivos', '56', 'v.ocampo', 1574897403, 1590169117, 3),
(135, 'contactos', '1', 'v.ocampo', 1574897672, 1574897672, 3),
(582, 'marca_modelo', '36', 'felipem', 1590173659, 1590173659, 2),
(137, 'contactos', '2', 'v.ocampo', 1574898334, 1574898334, 3),
(138, 'movimientos', '1', 'v.ocampo', 1574898368, 1574898610, 3),
(139, 'movimientos', '2', 'v.ocampo', 1574898574, 1574898574, 3),
(140, 'documentos', '1', 'v.ocampo', 1574898656, 1574898656, 3),
(142, 'dispositivos', '57', 'felipem', 1575063935, 1618413335, 2),
(143, 'movimientos', '3', 'felipem', 1575064319, 1575064476, 2),
(144, 'contactos', '3', 'felipem', 1575064426, 1575604417, 2),
(145, 'dispositivos', '58', 'felipem', 1575065168, 1613500942, 2),
(146, 'movimientos', '4', 'felipem', 1575065215, 1575065215, 2),
(147, 'dispositivos', '59', 'felipem', 1575351512, 1614611983, 2),
(148, 'dispositivos', '60', 'felipem', 1575351663, 1575605068, 2),
(149, 'unidades', '10', 'felipem', 1575351742, 1575351742, 2),
(150, 'movimientos', '5', 'felipem', 1575351866, 1575398311, 2),
(151, 'movimientos', '6', 'felipem', 1575351956, 1575398354, 2),
(152, 'marca_modelo', '7', 'felipem', 1575377974, 1577401079, 2),
(153, 'marcas', '8', 'felipem', 1575378007, 1575378007, 2),
(154, 'marca_modelo', '8', 'felipem', 1575378054, 1577401155, 2),
(155, 'dispositivos', '61', 'felipem', 1575378241, 1575522095, 2),
(156, 'dispositivos', '62', 'felipem', 1575378241, 1575521959, 2),
(157, 'dispositivos', '63', 'felipem', 1575378241, 1575378241, 2),
(158, 'dispositivos', '64', 'felipem', 1575378241, 1618851122, 2),
(1817, 'movimientos', '174', 'c.zambrano', 1611589185, 1611589413, 3),
(160, 'dispositivos', '66', 'felipem', 1575378241, 1575378241, 2),
(161, 'dispositivos', '67', 'felipem', 1575378241, 1583522721, 2),
(162, 'dispositivos', '68', 'felipem', 1575378241, 1575378241, 2),
(163, 'dispositivos', '69', 'felipem', 1575378241, 1606860782, 2),
(164, 'dispositivos', '70', 'felipem', 1575378241, 1584483326, 2),
(165, 'dispositivos', '71', 'felipem', 1575378241, 1575378241, 2),
(166, 'dispositivos', '72', 'felipem', 1575378241, 1584483466, 2),
(167, 'dispositivos', '73', 'felipem', 1575378241, 1575378241, 2),
(168, 'dispositivos', '74', 'felipem', 1575378241, 1575378241, 2),
(169, 'dispositivos', '75', 'felipem', 1575378241, 1575378241, 2),
(170, 'dispositivos', '76', 'felipem', 1575378241, 1575378241, 2),
(171, 'dispositivos', '77', 'felipem', 1575378241, 1575378241, 2),
(172, 'dispositivos', '78', 'felipem', 1575378241, 1618850995, 2),
(173, 'dispositivos', '79', 'felipem', 1575378241, 1575378241, 2),
(174, 'dispositivos', '80', 'felipem', 1575378241, 1618412771, 2),
(175, 'dispositivos', '81', 'felipem', 1575378241, 1606137610, 2),
(176, 'dispositivos', '82', 'felipem', 1575378241, 1618322559, 2),
(177, 'dispositivos', '83', 'felipem', 1575378241, 1605815536, 2),
(178, 'dispositivos', '84', 'felipem', 1575378241, 1602189176, 2),
(179, 'dispositivos', '85', 'felipem', 1575378241, 1615214802, 2),
(180, 'dispositivos', '86', 'felipem', 1575378241, 1618850620, 2),
(181, 'dispositivos', '87', 'felipem', 1575378241, 1618412706, 2),
(182, 'dispositivos', '88', 'felipem', 1575378241, 1606848382, 2),
(183, 'dispositivos', '89', 'felipem', 1575378241, 1575378241, 2),
(184, 'dispositivos', '90', 'felipem', 1575378241, 1605813807, 2),
(185, 'dispositivos', '91', 'felipem', 1575378241, 1602188137, 2),
(186, 'dispositivos', '92', 'felipem', 1575378241, 1618851254, 2),
(187, 'dispositivos', '93', 'felipem', 1575378241, 1618322120, 2),
(188, 'dispositivos', '94', 'felipem', 1575378241, 1618850956, 2),
(189, 'dispositivos', '95', 'felipem', 1575378241, 1575378241, 2),
(190, 'dispositivos', '96', 'felipem', 1575378241, 1614706146, 2),
(191, 'dispositivos', '97', 'felipem', 1575378241, 1611598419, 2),
(192, 'dispositivos', '98', 'felipem', 1575378241, 1583365075, 2),
(193, 'tipo_estado_dispo', '3', 'felipem', 1575378727, 1575378727, 2),
(194, 'movimientos', '7', 'felipem', 1575389101, 1583364723, 2),
(195, 'movimientos', '8', 'felipem', 1575389208, 1575389208, 2),
(196, 'movimientos', '9', 'felipem', 1575389401, 1575389401, 2),
(197, 'movimientos', '10', 'felipem', 1575389413, 1575389442, 2),
(198, 'movimientos', '11', 'felipem', 1575389518, 1575389518, 2),
(199, 'movimientos', '12', 'felipem', 1575389522, 1575389599, 2),
(200, 'movimientos', '13', 'felipem', 1575389687, 1575389687, 2),
(201, 'movimientos', '14', 'felipem', 1575389692, 1575389721, 2),
(202, 'movimientos', '15', 'felipem', 1575389892, 1575389923, 2),
(203, 'movimientos', '16', 'felipem', 1575390057, 1575390133, 2),
(204, 'movimientos', '17', 'felipem', 1575390067, 1575390067, 2),
(205, 'movimientos', '18', 'felipem', 1575390402, 1575390402, 2),
(206, 'movimientos', '19', 'felipem', 1575390415, 1575390455, 2),
(207, 'movimientos', '20', 'felipem', 1575398679, 1575398679, 2),
(208, 'movimientos', '21', 'felipem', 1575398690, 1575398714, 2),
(209, 'movimientos', '22', 'felipem', 1575398751, 1575398751, 2),
(210, 'movimientos', '23', 'felipem', 1575398754, 1575398780, 2),
(211, 'dispositivos', '99', 'felipem', 1575556995, 1582990908, 2),
(212, 'dispositivos', '100', 'felipem', 1575556995, 1582990926, 2),
(213, 'dispositivos', '101', 'felipem', 1575556995, 1583273854, 2),
(214, 'dispositivos', '102', 'felipem', 1575556995, 1582990980, 2),
(215, 'dispositivos', '103', 'felipem', 1575556995, 1582991012, 2),
(216, 'dispositivos', '104', 'felipem', 1575556995, 1589261513, 2),
(217, 'dispositivos', '105', 'felipem', 1575556995, 1588870908, 2),
(218, 'dispositivos', '106', 'felipem', 1575556995, 1617994327, 2),
(219, 'dispositivos', '107', 'felipem', 1575556995, 1583030323, 2),
(220, 'dispositivos', '108', 'felipem', 1575556995, 1583272369, 2),
(221, 'dispositivos', '109', 'felipem', 1575556995, 1582768002, 2),
(222, 'dispositivos', '110', 'felipem', 1575556995, 1582768144, 2),
(223, 'dispositivos', '111', 'felipem', 1575556995, 1605645781, 2),
(224, 'dispositivos', '112', 'felipem', 1575556995, 1583030334, 2),
(225, 'dispositivos', '113', 'felipem', 1575556995, 1583030341, 2),
(226, 'dispositivos', '114', 'felipem', 1575556995, 1582768699, 2),
(227, 'dispositivos', '115', 'felipem', 1575556995, 1583030348, 2),
(228, 'dispositivos', '116', 'felipem', 1575556995, 1583030354, 2),
(229, 'dispositivos', '117', 'felipem', 1575556995, 1582768248, 2),
(230, 'dispositivos', '118', 'felipem', 1575556995, 1583030361, 2),
(231, 'dispositivos', '119', 'felipem', 1575556995, 1588806543, 2),
(232, 'dispositivos', '120', 'felipem', 1575556995, 1606746803, 2),
(233, 'dispositivos', '121', 'felipem', 1575556995, 1583909660, 2),
(234, 'dispositivos', '122', 'felipem', 1575556995, 1605900145, 2),
(235, 'dispositivos', '123', 'felipem', 1575556995, 1584477777, 2),
(236, 'dispositivos', '124', 'felipem', 1575556995, 1582995624, 2),
(237, 'dispositivos', '125', 'felipem', 1575556995, 1602784499, 2),
(238, 'dispositivos', '126', 'felipem', 1575556995, 1618334450, 2),
(239, 'dispositivos', '127', 'felipem', 1575556995, 1582082276, 2),
(240, 'dispositivos', '128', 'felipem', 1575556995, 1618413887, 2),
(241, 'dispositivos', '129', 'felipem', 1575556995, 1583506623, 2),
(242, 'dispositivos', '130', 'felipem', 1575556995, 1584809184, 2),
(243, 'dispositivos', '131', 'felipem', 1575556995, 1584810038, 2),
(244, 'dispositivos', '132', 'felipem', 1575556995, 1615321934, 2),
(245, 'dispositivos', '133', 'felipem', 1575556995, 1583333573, 2),
(246, 'dispositivos', '134', 'felipem', 1575556995, 1594168095, 2),
(247, 'dispositivos', '135', 'felipem', 1575556995, 1582769298, 2),
(248, 'dispositivos', '136', 'felipem', 1575556995, 1583334407, 2),
(249, 'dispositivos', '137', 'felipem', 1575556995, 1606916978, 2),
(250, 'dispositivos', '138', 'felipem', 1575556995, 1618164834, 2),
(251, 'dispositivos', '139', 'felipem', 1575556995, 1618328603, 2),
(252, 'dispositivos', '140', 'felipem', 1575556995, 1583504409, 2),
(253, 'dispositivos', '141', 'felipem', 1575556995, 1582991548, 2),
(254, 'dispositivos', '142', 'felipem', 1575556995, 1582991567, 2),
(255, 'dispositivos', '143', 'felipem', 1575556995, 1582991602, 2),
(256, 'dispositivos', '144', 'felipem', 1575556995, 1582991619, 2),
(257, 'dispositivos', '145', 'felipem', 1575556995, 1582991630, 2),
(258, 'dispositivos', '146', 'felipem', 1575556995, 1582991657, 2),
(259, 'dispositivos', '147', 'felipem', 1575556995, 1583036758, 2),
(260, 'dispositivos', '148', 'felipem', 1575556995, 1583036775, 2),
(261, 'dispositivos', '149', 'felipem', 1575556995, 1606233921, 2),
(262, 'dispositivos', '150', 'felipem', 1575556995, 1582991907, 2),
(263, 'dispositivos', '151', 'felipem', 1575556995, 1617748780, 2),
(264, 'dispositivos', '152', 'felipem', 1575556995, 1617749000, 2),
(265, 'dispositivos', '153', 'felipem', 1575556995, 1617748562, 2),
(266, 'dispositivos', '154', 'felipem', 1575556995, 1583036920, 2),
(267, 'dispositivos', '155', 'felipem', 1575556995, 1583036938, 2),
(268, 'dispositivos', '156', 'felipem', 1575556995, 1583335129, 2),
(269, 'dispositivos', '157', 'felipem', 1575556995, 1606858734, 2),
(270, 'dispositivos', '158', 'felipem', 1575556995, 1583948569, 2),
(271, 'dispositivos', '159', 'felipem', 1575556995, 1584478256, 2),
(272, 'dispositivos', '160', 'felipem', 1575556995, 1606857821, 2),
(273, 'dispositivos', '161', 'felipem', 1575556995, 1583036170, 2),
(274, 'dispositivos', '162', 'felipem', 1575556995, 1583333338, 2),
(275, 'dispositivos', '163', 'felipem', 1575556995, 1606232856, 2),
(276, 'dispositivos', '164', 'felipem', 1575556995, 1583036195, 2),
(277, 'dispositivos', '165', 'felipem', 1575556995, 1618412804, 2),
(278, 'dispositivos', '166', 'felipem', 1575556995, 1602784163, 2),
(279, 'dispositivos', '167', 'felipem', 1575556995, 1606586959, 2),
(280, 'dispositivos', '168', 'felipem', 1575556995, 1583030096, 2),
(281, 'dispositivos', '169', 'felipem', 1575556995, 1583030103, 2),
(282, 'dispositivos', '170', 'felipem', 1575556995, 1611604067, 2),
(283, 'dispositivos', '171', 'felipem', 1575556995, 1607374640, 2),
(284, 'dispositivos', '172', 'felipem', 1575556995, 1583030012, 2),
(285, 'dispositivos', '173', 'felipem', 1575556995, 1583030125, 2),
(286, 'dispositivos', '174', 'felipem', 1575556995, 1583030141, 2),
(287, 'dispositivos', '175', 'felipem', 1575556995, 1583030151, 2),
(288, 'dispositivos', '176', 'felipem', 1575556995, 1583030161, 2),
(289, 'dispositivos', '177', 'felipem', 1575556995, 1583030168, 2),
(290, 'dispositivos', '178', 'felipem', 1575556995, 1583030177, 2),
(1463, 'dispositivos', '1008', 'felipem', 1605850856, 1618409366, 2),
(1462, 'dispositivos', '1007', 'felipem', 1605850827, 1606233532, 2),
(1461, 'dispositivos', '1006', 'felipem', 1605850797, 1607029787, 2),
(1460, 'dispositivos', '1005', 'felipem', 1605850760, 1606751268, 2),
(295, 'dispositivos', '183', 'felipem', 1575556995, 1602785347, 2),
(296, 'dispositivos', '184', 'felipem', 1575556995, 1602785425, 2),
(297, 'dispositivos', '185', 'felipem', 1575556995, 1602785523, 2),
(298, 'dispositivos', '186', 'felipem', 1575556995, 1602785550, 2),
(299, 'dispositivos', '187', 'felipem', 1575556995, 1602785584, 2),
(300, 'dispositivos', '188', 'felipem', 1575556995, 1602785671, 2),
(301, 'dispositivos', '189', 'felipem', 1575556995, 1602785703, 2),
(302, 'dispositivos', '190', 'felipem', 1575556995, 1602785728, 2),
(303, 'dispositivos', '191', 'felipem', 1575556995, 1602785759, 2),
(304, 'dispositivos', '192', 'felipem', 1575556995, 1602785305, 2),
(305, 'dispositivos', '193', 'felipem', 1575556995, 1602785262, 2),
(306, 'dispositivos', '194', 'felipem', 1575556995, 1605850334, 2),
(307, 'dispositivos', '195', 'felipem', 1575556995, 1605850376, 2),
(308, 'dispositivos', '196', 'felipem', 1575556995, 1605850453, 2),
(309, 'dispositivos', '197', 'felipem', 1575556995, 1606241038, 2),
(310, 'dispositivos', '198', 'felipem', 1575556995, 1605850586, 2),
(311, 'dispositivos', '199', 'felipem', 1575604788, 1602186379, 2),
(312, 'dispositivos', '200', 'felipem', 1575604876, 1611603465, 2),
(313, 'dispositivos', '201', 'felipem', 1575605173, 1577462817, 2),
(314, 'dispositivos', '202', 'felipem', 1575605274, 1611600565, 2),
(315, 'dispositivos', '203', 'felipem', 1575605329, 1576021294, 2),
(316, 'dispositivos', '204', 'felipem', 1575605380, 1583528044, 2),
(317, 'tipo_dispositivo', '13', 'felipem', 1575608022, 1606176802, 2),
(318, 'tipo_dispositivo', '14', 'felipem', 1575608065, 1606175246, 2),
(319, 'tipo_dispositivo', '15', 'felipem', 1575608099, 1602785845, 2),
(320, 'tipo_dispositivo', '16', 'felipem', 1575608195, 1602786124, 2),
(321, 'tipo_dispositivo', '17', 'felipem', 1575608209, 1602786172, 2),
(322, 'marcas', '9', 'felipem', 1575955734, 1575955804, 2),
(323, 'marca_modelo', '9', 'felipem', 1575955782, 1577400361, 2),
(324, 'imagenes', '3', 'felipem', 1575956131, 1575956131, 2),
(325, 'marca_modelo', '10', 'felipem', 1575989158, 1577400316, 2),
(326, 'imagenes', '4', 'felipem', 1576011693, 1576011693, 2),
(327, 'marcas', '10', 'felipem', 1576012278, 1590176273, 2),
(328, 'marca_modelo', '11', 'felipem', 1576012373, 1577402451, 2),
(329, 'imagenes', '5', 'felipem', 1576012597, 1576012597, 2),
(330, 'movimientos', '24', 'felipem', 1576021182, 1576021182, 2),
(331, 'movimientos', '25', 'felipem', 1576021189, 1576021223, 2),
(332, 'movimientos', '26', 'felipem', 1576021351, 1576021351, 2),
(333, 'movimientos', '27', 'felipem', 1576021357, 1576021393, 2),
(334, 'movimientos', '28', 'felipem', 1576021488, 1576021488, 2),
(335, 'movimientos', '29', 'felipem', 1576021568, 1576021607, 2),
(336, 'tipo_iden', '3', 'felipem', 1576021965, 1576021965, 2),
(337, 'contactos', '4', 'felipem', 1576022106, 1576022121, 2),
(1574, 'verificacion', '142', 'a.lopez', 1606576788, 1606576788, 3),
(340, 'marca_modelo', '12', 'felipem', 1577400398, 1577400940, 2),
(1464, 'dispositivos', '1009', 'felipem', 1605851285, 1613423543, 2),
(342, 'movimientos', '30', 'felipem', 1577462666, 1577462666, 2),
(343, 'movimientos', '31', 'felipem', 1577462670, 1577462702, 2),
(344, 'movimientos', '32', 'felipem', 1577462855, 1577462855, 2),
(345, 'movimientos', '33', 'felipem', 1577462859, 1577462889, 2),
(346, 'movimientos', '34', 'felipem', 1577462994, 1577462994, 2),
(347, 'movimientos', '35', 'felipem', 1577463010, 1577463027, 2),
(348, 'movimientos', '36', 'felipem', 1577469270, 1577469270, 2),
(349, 'movimientos', '37', 'felipem', 1577469277, 1577469296, 2),
(350, 'movimientos', '38', 'felipem', 1577469582, 1577469582, 2),
(351, 'dispositivos', '206', 'felipem', 1577473118, 1605816195, 2),
(352, 'dispositivos', '207', 'felipem', 1577473277, 1577473277, 2),
(353, 'dispositivos', '208', 'felipem', 1577474118, 1614706070, 2),
(354, 'dispositivos', '209', 'felipem', 1577474118, 1618851055, 2),
(355, 'dispositivos', '210', 'felipem', 1577474118, 1577474118, 2),
(356, 'dispositivos', '211', 'felipem', 1577474118, 1577474118, 2),
(357, 'dispositivos', '212', 'felipem', 1577474118, 1577474118, 2),
(358, 'dispositivos', '213', 'felipem', 1577474118, 1577474118, 2),
(359, 'dispositivos', '214', 'felipem', 1577474118, 1577474118, 2),
(360, 'dispositivos', '215', 'felipem', 1577474118, 1577474118, 2),
(361, 'marca_modelo', '13', 'felipem', 1577761250, 1577761250, 2),
(362, 'marca_modelo', '14', 'felipem', 1577761291, 1577761291, 2),
(363, 'dispositivos', '216', 'felipem', 1580832093, 1611599412, 2),
(364, 'dispositivos', '217', 'felipem', 1580832143, 1618862174, 2),
(365, 'dispositivos', '218', 'felipem', 1580832208, 1618331801, 2),
(366, 'dispositivos', '219', 'felipem', 1580832278, 1611599676, 2),
(367, 'dispositivos', '220', 'felipem', 1580832480, 1605816027, 2),
(368, 'movimientos', '39', 'felipem', 1580832555, 1580832555, 2),
(369, 'movimientos', '40', 'felipem', 1580832560, 1580832572, 2),
(370, 'movimientos', '41', 'felipem', 1580832576, 1580832583, 2),
(371, 'movimientos', '42', 'felipem', 1580832587, 1580832606, 2),
(372, 'movimientos', '43', 'felipem', 1580832608, 1580832629, 2),
(373, 'dispositivos', '221', 'felipem', 1582082515, 1583507404, 2),
(374, 'dispositivos', '222', 'felipem', 1582082626, 1582082626, 2),
(375, 'marca_modelo', '15', 'felipem', 1582131046, 1582131114, 2),
(376, 'unidades', '11', 'felipem', 1582131201, 1582131201, 2),
(377, 'dispositivos', '223', 'felipem', 1582131249, 1582131249, 2),
(1573, 'prestador', '1', 'felipem', 1606509048, 1606512671, 2),
(379, 'movimientos', '44', 'felipem', 1582132200, 1582132200, 2),
(380, 'mantenimientos', '1', 'felipem', 1582132389, 1582132389, 2),
(381, 'movimientos', '45', 'felipem', 1582231965, 1582231965, 2),
(382, 'marca_modelo', '16', 'felipem', 1582237925, 1582238033, 2),
(383, 'marca_modelo', '17', 'felipem', 1582238255, 1582754378, 2),
(384, 'dispositivos', '224', 'felipem', 1582238512, 1583338032, 2),
(386, 'dispositivos', '225', 'c.zambrano', 1582755215, 1583507057, 3),
(387, 'marcas', '12', 'c.zambrano', 1582757737, 1582757743, 3),
(388, 'marca_modelo', '18', 'c.zambrano', 1582758125, 1582758125, 3),
(389, 'marca_modelo', '19', 'c.zambrano', 1582763298, 1582763347, 3),
(390, 'marca_modelo', '20', 'c.zambrano', 1582767359, 1582767382, 3),
(391, 'marcas', '13', 'c.zambrano', 1582769072, 1582769075, 3),
(392, 'marca_modelo', '21', 'c.zambrano', 1582769242, 1582769248, 3),
(393, 'marcas', '14', 'c.zambrano', 1582769445, 1582769448, 3),
(394, 'marca_modelo', '22', 'c.zambrano', 1582772003, 1584478372, 3),
(395, 'dispositivos', '226', 'c.zambrano', 1582989352, 1618413479, 3),
(396, 'contactos', '5', 'c.zambrano', 1582989584, 1582989584, 3),
(397, 'movimientos', '46', 'c.zambrano', 1582989688, 1582989688, 3),
(398, 'movimientos', '47', 'c.zambrano', 1582989773, 1582989773, 3),
(399, 'movimientos', '48', 'c.zambrano', 1582990170, 1582990170, 3),
(400, 'dispositivos', '227', 'c.zambrano', 1582990338, 1618163146, 3),
(401, 'movimientos', '49', 'c.zambrano', 1582990415, 1582990707, 3),
(402, 'marcas', '15', 'c.zambrano', 1582991305, 1582991305, 3),
(403, 'marca_modelo', '23', 'c.zambrano', 1582991379, 1582991379, 3),
(404, 'movimientos', '50', 'c.zambrano', 1582994275, 1583035939, 3),
(405, 'movimientos', '51', 'c.zambrano', 1582994526, 1582994535, 3),
(406, 'movimientos', '52', 'c.zambrano', 1582994981, 1582994981, 3),
(407, 'movimientos', '53', 'c.zambrano', 1582995585, 1582995585, 3),
(408, 'movimientos', '54', 'c.zambrano', 1583024440, 1583029648, 3),
(413, 'movimientos', '59', 'c.zambrano', 1583029865, 1583029869, 3),
(410, 'movimientos', '56', 'c.zambrano', 1583025135, 1583025135, 3),
(414, 'movimientos', '60', 'c.zambrano', 1583180592, 1583180604, 3),
(415, 'movimientos', '61', 'c.zambrano', 1583180902, 1583180916, 3),
(416, 'movimientos', '62', 'c.zambrano', 1583181944, 1583181944, 3),
(417, 'movimientos', '63', 'c.zambrano', 1583182372, 1583182388, 3),
(418, 'movimientos', '64', 'c.zambrano', 1583200265, 1583200991, 3),
(419, 'movimientos', '65', 'c.zambrano', 1583200747, 1583200747, 3),
(420, 'movimientos', '66', 'c.zambrano', 1583201372, 1583201372, 3),
(421, 'movimientos', '67', 'c.zambrano', 1583201509, 1583201509, 3),
(422, 'movimientos', '68', 'c.zambrano', 1583201860, 1583201860, 3),
(423, 'movimientos', '69', 'c.zambrano', 1583201973, 1583201983, 3),
(424, 'movimientos', '70', 'c.zambrano', 1583202582, 1583202582, 3),
(425, 'movimientos', '71', 'c.zambrano', 1583202831, 1583202890, 3),
(426, 'marcas', '16', 'c.zambrano', 1583241334, 1583241334, 3),
(427, 'marca_modelo', '24', 'c.zambrano', 1583269790, 1583272304, 3),
(428, 'movimientos', '72', 'c.zambrano', 1583271547, 1583271547, 3),
(429, 'marca_modelo', '25', 'c.zambrano', 1583273673, 1583273696, 3),
(430, 'movimientos', '73', 'c.zambrano', 1583273788, 1583273788, 3),
(431, 'marca_modelo', '26', 'c.zambrano', 1583289906, 1583289909, 3),
(432, 'movimientos', '74', 'c.zambrano', 1583290210, 1583290210, 3),
(433, 'movimientos', '75', 'c.zambrano', 1583290406, 1583290414, 3),
(434, 'movimientos', '76', 'c.zambrano', 1583333311, 1583333311, 3),
(435, 'movimientos', '77', 'c.zambrano', 1583333455, 1583333455, 3),
(436, 'marca_modelo', '27', 'c.zambrano', 1583334203, 1583334203, 3),
(437, 'movimientos', '78', 'c.zambrano', 1583334401, 1583334401, 3),
(438, 'movimientos', '79', 'c.zambrano', 1583335083, 1583335083, 3),
(439, 'dispositivos', '228', 'felipem', 1583338140, 1583338497, 2),
(440, 'movimientos', '80', 'felipem', 1583338399, 1583338399, 2),
(441, 'movimientos', '81', 'c.zambrano', 1583363529, 1583363532, 3),
(442, 'movimientos', '82', 'c.zambrano', 1583363799, 1583363799, 3),
(443, 'movimientos', '83', 'c.zambrano', 1583363979, 1583363979, 3),
(444, 'movimientos', '84', 'c.zambrano', 1583364574, 1583364574, 3),
(445, 'movimientos', '85', 'c.zambrano', 1583364654, 1583364654, 3),
(446, 'movimientos', '86', 'c.zambrano', 1583364901, 1583364904, 3),
(447, 'movimientos', '87', 'c.zambrano', 1583365060, 1583365063, 3),
(1819, 'movi_recepcion', '32', 'c.zambrano', 1611589401, 1611589401, 3),
(449, 'movimientos', '89', 'c.zambrano', 1583365526, 1583365529, 3),
(450, 'marcas', '17', 'felipem', 1583424616, 1583424616, 2),
(451, 'marca_modelo', '28', 'felipem', 1583424817, 1583424817, 2),
(452, 'dispositivos', '229', 'felipem', 1583425009, 1605851070, 2),
(453, 'movimientos', '90', 'c.zambrano', 1583464281, 1583464281, 3),
(454, 'movimientos', '91', 'c.zambrano', 1583464371, 1583464386, 3),
(455, 'movimientos', '92', 'c.zambrano', 1583464667, 1583464667, 3),
(456, 'dispositivos', '230', 'c.zambrano', 1583503067, 1583503380, 3),
(457, 'movimientos', '93', 'c.zambrano', 1583503149, 1583503432, 3),
(458, 'movimientos', '94', 'c.zambrano', 1583504271, 1583504271, 3),
(459, 'movimientos', '95', 'c.zambrano', 1583505115, 1583505159, 3),
(460, 'movimientos', '96', 'c.zambrano', 1583505629, 1583505629, 3),
(461, 'movimientos', '97', 'c.zambrano', 1583506591, 1583506609, 3),
(462, 'movimientos', '98', 'c.zambrano', 1583506779, 1583506779, 3),
(463, 'movimientos', '99', 'c.zambrano', 1583507355, 1583507355, 3),
(464, 'marcas', '18', 'c.zambrano', 1583511521, 1583511521, 3),
(465, 'marca_modelo', '29', 'c.zambrano', 1583511769, 1583511769, 3),
(466, 'dispositivos', '231', 'c.zambrano', 1583511822, 1614097936, 3),
(467, 'movimientos', '100', 'c.zambrano', 1583512768, 1583512768, 3),
(468, 'tipo_dispositivo', '18', 'c.zambrano', 1583513874, 1606174777, 3),
(469, 'marcas', '19', 'c.zambrano', 1583514014, 1583514014, 3),
(470, 'marca_modelo', '30', 'c.zambrano', 1583514233, 1605849574, 3),
(471, 'dispositivos', '232', 'c.zambrano', 1583514929, 1606586631, 3),
(472, 'movimientos', '101', 'c.zambrano', 1583514995, 1583514995, 3),
(473, 'tipo_dispositivo', '19', 'c.zambrano', 1583515183, 1606174332, 3),
(474, 'tipo_dispositivo', '20', 'c.zambrano', 1583515824, 1606174399, 3),
(475, 'marcas', '20', 'c.zambrano', 1583515895, 1583516330, 3),
(476, 'marca_modelo', '31', 'c.zambrano', 1583516351, 1583516351, 3),
(477, 'dispositivos', '233', 'c.zambrano', 1583516416, 1589261712, 3),
(478, 'marcas', '21', 'c.zambrano', 1583516693, 1583516693, 3),
(479, 'marca_modelo', '32', 'c.zambrano', 1583517016, 1583517016, 3),
(480, 'dispositivos', '234', 'c.zambrano', 1583517156, 1583517270, 3),
(481, 'movimientos', '102', 'c.zambrano', 1583517250, 1583517250, 3),
(482, 'movimientos', '103', 'c.zambrano', 1583518338, 1583518338, 3),
(483, 'movimientos', '104', 'c.zambrano', 1583522068, 1583522068, 3),
(484, 'movimientos', '105', 'c.zambrano', 1583522149, 1583522149, 3),
(485, 'movimientos', '106', 'c.zambrano', 1583522452, 1583522452, 3),
(486, 'movimientos', '107', 'c.zambrano', 1583526034, 1583526060, 3),
(487, 'movimientos', '108', 'c.zambrano', 1583526136, 1583526136, 3),
(488, 'movimientos', '109', 'c.zambrano', 1583526739, 1583526739, 3),
(489, 'movimientos', '110', 'c.zambrano', 1583528255, 1583528255, 3),
(490, 'movimientos', '111', 'c.zambrano', 1583528862, 1583528862, 3),
(491, 'movimientos', '112', 'c.zambrano', 1583529201, 1583529201, 3),
(492, 'movimientos', '113', 'c.zambrano', 1583529301, 1583529301, 3),
(493, 'movimientos', '114', 'c.zambrano', 1583948451, 1583948451, 3),
(494, 'movimientos', '115', 'c.zambrano', 1583948540, 1583948540, 3),
(495, 'movimientos', '116', 'c.zambrano', 1584378707, 1584378713, 3),
(496, 'movimientos', '117', 'c.zambrano', 1584467133, 1584467155, 3),
(497, 'movimientos', '118', 'c.zambrano', 1584477747, 1584477754, 3),
(498, 'movimientos', '119', 'c.zambrano', 1584478236, 1584478236, 3),
(499, 'movimientos', '120', 'c.zambrano', 1584482638, 1584482642, 3),
(500, 'movimientos', '121', 'c.zambrano', 1584483314, 1584483320, 3),
(501, 'movimientos', '122', 'c.zambrano', 1584483453, 1584483453, 3),
(502, 'movimientos', '123', 'c.zambrano', 1584484360, 1584484360, 3),
(503, 'movimientos', '124', 'c.zambrano', 1584484547, 1584484547, 3),
(504, 'movimientos', '125', 'c.zambrano', 1584809503, 1584809759, 3),
(505, 'movimientos', '126', 'c.zambrano', 1584809603, 1584809743, 3),
(506, 'dispositivos', '235', 'c.zambrano', 1584810927, 1606415226, 3),
(507, 'movimientos', '127', 'c.zambrano', 1584810976, 1584811014, 3),
(508, 'movimientos', '128', 'c.zambrano', 1584811554, 1584811554, 3),
(509, 'verificacion', '1', 'felipem', 1588196147, 1588196147, 2),
(510, 'verificacion', '2', 'crueda', 1588274706, 1604443552, 6),
(511, 'verificacion', '3', 'crueda', 1588274745, 1588274745, 6),
(512, 'verificacion', '4', 'crueda', 1588274771, 1588274771, 6),
(513, 'verificacion', '5', 'crueda', 1588274812, 1588274812, 6),
(514, 'verificacion', '6', 'hrestrepo', 1588276519, 1605643310, 6),
(515, 'verificacion', '7', 'hrestrepo', 1588276561, 1604443567, 6),
(516, 'verificacion', '8', 'hrestrepo', 1588276643, 1588276643, 6),
(517, 'verificacion', '9', 'hrestrepo', 1588276811, 1588276811, 6),
(518, 'verificacion', '10', 'hrestrepo', 1588276868, 1588276868, 6),
(519, 'verificacion', '11', 'hrestrepo', 1588277267, 1588277267, 6),
(520, 'verificacion', '12', 'hrestrepo', 1588277305, 1588277305, 6),
(521, 'verificacion', '13', 'hrestrepo', 1588277384, 1588277384, 6),
(522, 'verificacion', '14', 'hrestrepo', 1588277433, 1588277433, 6),
(523, 'verificacion', '15', 'hrestrepo', 1588277496, 1588277496, 6),
(524, 'verificacion', '16', 'hrestrepo', 1588277719, 1588277719, 6),
(525, 'verificacion', '17', 'crueda', 1588277721, 1588277721, 6),
(526, 'verificacion', '18', 'hrestrepo', 1588277756, 1588277756, 6),
(527, 'verificacion', '19', 'crueda', 1588277772, 1588277772, 6),
(528, 'verificacion', '20', 'crueda', 1588277846, 1588277846, 6),
(529, 'verificacion', '21', 'hrestrepo', 1588277938, 1588277938, 6),
(530, 'verificacion', '22', 'crueda', 1588277955, 1588277955, 6),
(531, 'verificacion', '23', 'hrestrepo', 1588278098, 1588278098, 6),
(532, 'verificacion', '24', 'hrestrepo', 1588278307, 1588278307, 6),
(533, 'verificacion', '25', 'crueda', 1588278674, 1588278770, 6),
(534, 'verificacion', '26', 'hrestrepo', 1588278879, 1588278879, 6),
(535, 'verificacion', '27', 'j.villa', 1588429793, 1588429793, 6),
(536, 'verificacion', '28', 'j.villa', 1588430315, 1588430315, 6),
(537, 'verificacion', '29', 'j.villa', 1588430354, 1588430354, 6),
(538, 'verificacion', '30', 'j.villa', 1588430400, 1588430400, 6),
(539, 'verificacion', '31', 'j.villa', 1588430532, 1588430532, 6),
(540, 'verificacion', '32', 'j.villa', 1588431259, 1588431259, 6),
(541, 'verificacion', '33', 'j.villa', 1588431305, 1588431305, 6),
(542, 'verificacion', '34', 'j.villa', 1588431444, 1588431444, 6),
(543, 'verificacion', '35', 'j.villa', 1588431471, 1588431471, 6),
(544, 'verificacion', '36', 'j.villa', 1588432813, 1588432813, 6),
(545, 'verificacion', '37', 'j.villa', 1588433135, 1588433135, 6),
(546, 'verificacion', '38', 'j.villa', 1588433227, 1588433227, 6),
(547, 'verificacion', '39', 'j.villa', 1588433271, 1588433271, 6),
(548, 'verificacion', '40', 'j.villa', 1588433298, 1588433298, 6),
(549, 'verificacion', '41', 'j.villa', 1588433374, 1588433374, 6),
(550, 'verificacion', '42', 'j.villa', 1588433419, 1588433419, 6),
(551, 'movimientos', '129', 'felipem', 1588806157, 1588806157, 2),
(552, 'unidades', '12', 'felipem', 1588806324, 1606427430, 2),
(553, 'movimientos', '130', 'felipem', 1588806419, 1588806422, 2),
(554, 'movimientos', '131', 'felipem', 1588806528, 1588806528, 2),
(555, 'marca_modelo', '33', 'felipem', 1588807139, 1588807139, 2),
(556, 'dispositivos', '236', 'felipem', 1588807171, 1618325618, 2),
(557, 'movimientos', '132', 'felipem', 1588807223, 1588807223, 2),
(558, 'movimientos', '133', 'felipem', 1588807477, 1588807570, 2),
(559, 'dispositivos', '237', 'felipem', 1588870652, 1606745872, 2),
(560, 'movimientos', '134', 'felipem', 1588870714, 1588870714, 2),
(561, 'movimientos', '135', 'felipem', 1588870899, 1588870899, 2),
(562, 'tipo_dispositivo', '21', 'felipem', 1588885347, 1606175400, 2),
(563, 'marcas', '22', 'felipem', 1588885386, 1588885386, 2),
(564, 'marca_modelo', '34', 'felipem', 1588885612, 1588885612, 2),
(565, 'marcas', '23', 'felipem', 1588940327, 1588940327, 2),
(566, 'marca_modelo', '35', 'felipem', 1588940513, 1594167340, 2),
(567, 'verificacion', '43', 'ibet', 1588945435, 1588945435, 6),
(568, 'verificacion', '44', 'ibet', 1588945506, 1588945506, 6),
(569, 'verificacion', '45', 'ibet', 1588945922, 1588945922, 6),
(570, 'dispositivos', '238', 'felipem', 1588946063, 1588946063, 2),
(571, 'verificacion', '46', 'felipem', 1588946088, 1588946088, 2),
(572, 'verificacion', '47', 'ibet', 1588946203, 1588946203, 6),
(573, 'verificacion', '48', 'ibet', 1588946424, 1588946424, 6),
(574, 'verificacion', '49', 'ibet', 1588946894, 1588946894, 6),
(575, 'verificacion', '50', 'ibet', 1588947008, 1588947008, 6),
(576, 'verificacion', '51', 'ibet', 1588947098, 1588947098, 6),
(577, 'verificacion', '52', 'ibet', 1588947220, 1588947220, 6),
(578, 'verificacion', '53', 'ibet', 1588947360, 1588947360, 6),
(579, 'verificacion', '54', 'ibet', 1588947431, 1588947431, 6),
(580, 'verificacion', '55', 'ibet', 1588948491, 1588948491, 6),
(583, 'dispositivos', '240', 'felipem', 1590173736, 1614098245, 2),
(584, 'marca_modelo', '37', 'felipem', 1590176256, 1590176256, 2),
(585, 'dispositivos', '241', 'felipem', 1590177225, 1614097788, 2),
(586, 'marcas', '24', 'felipem', 1590178187, 1590178195, 2),
(587, 'marca_modelo', '38', 'felipem', 1590178359, 1590178518, 2),
(588, 'dispositivos', '242', 'felipem', 1590178552, 1614097896, 2),
(589, 'tipo_documento', '10', 'felipem', 1593013355, 1593013355, 2),
(590, 'fichatecnica', '1', 'felipem', 1593049055, 1593049112, 2),
(591, 'movimientos', '136', 'c.zambrano', 1594167656, 1594167676, 3),
(592, 'movimientos', '137', 'c.zambrano', 1594168052, 1594168057, 3),
(593, 'unidades', '13', 'felipem', 1599773478, 1603304905, 2),
(594, 'movimientos', '138', 'felipem', 1599773594, 1599773594, 2),
(595, 'contactos', '6', 'l.garcia', 1599774323, 1599774323, 3),
(596, 'calibraciones', '1', 'l.garcia', 1599774665, 1599774665, 3),
(597, 'movimientos', '139', 'superadmin', 1599775006, 1599775006, 2),
(599, 'dispositivos', '243', 'l.garcia', 1601327887, 1607373854, 3),
(600, 'documentos', '2', 'l.garcia', 1601328288, 1601328288, 3),
(601, 'marca_modelo', '39', 'superadmin', 1601587345, 1601587345, 2),
(602, 'dispositivos', '244', 'superadmin', 1601588444, 1601588676, 2),
(603, 'movimientos', '140', 'superadmin', 1601588594, 1601588594, 2),
(604, 'dispositivos', '245', 'superadmin', 1601588760, 1601588760, 2),
(605, 'movimientos', '141', 'superadmin', 1601588818, 1601588818, 2),
(606, 'verificacion', '56', 'superadmin', 1602186025, 1602186025, 2),
(607, 'verificacion', '57', 'superadmin', 1602186078, 1602186078, 2),
(608, 'verificacion', '58', 'superadmin', 1602186123, 1602186123, 2),
(609, 'verificacion', '59', 'superadmin', 1602186213, 1602186213, 2),
(610, 'verificacion', '60', 'superadmin', 1602187164, 1602187164, 2),
(611, 'verificacion', '61', 'superadmin', 1602187226, 1602187383, 2),
(612, 'dispositivos', '246', 'superadmin', 1602187714, 1602187714, 2),
(613, 'verificacion', '62', 'superadmin', 1602187731, 1602187731, 2),
(614, 'verificacion', '63', 'superadmin', 1602188156, 1602188156, 2),
(615, 'verificacion', '64', 'superadmin', 1602189203, 1602189203, 2),
(616, 'tipo_dispositivo', '23', 'superadmin', 1602782313, 1602782313, 2),
(617, 'marca_modelo', '40', 'superadmin', 1602782668, 1602782668, 2),
(618, 'dispositivos', '247', 'superadmin', 1602782744, 1614097689, 2),
(619, 'marca_modelo', '41', 'superadmin', 1602782983, 1602783950, 2),
(620, 'marcas', '25', 'superadmin', 1602784828, 1602784828, 2),
(621, 'marca_modelo', '42', 'superadmin', 1602784869, 1602784869, 2),
(622, 'marca_modelo', '43', 'superadmin', 1602784930, 1602784930, 2),
(623, 'verificacion', '65', 'superadmin', 1602790571, 1602790571, 2),
(624, 'marcas', '26', 'superadmin', 1602793582, 1602793582, 2),
(625, 'marca_modelo', '44', 'superadmin', 1602793681, 1605847290, 2),
(626, 'marca_modelo', '45', 'superadmin', 1602798203, 1602798203, 2),
(627, 'dispositivos', '248', 'superadmin', 1602798356, 1602798356, 2),
(628, 'dispositivos', '249', 'superadmin', 1602798476, 1602798476, 2),
(629, 'dispositivos', '250', 'superadmin', 1602799644, 1602799644, 2),
(630, 'marca_modelo', '46', 'superadmin', 1602801618, 1602801634, 2),
(631, 'dispositivos', '251', 'superadmin', 1602801669, 1602801669, 2),
(632, 'marca_modelo', '47', 'superadmin', 1602801879, 1602801879, 2),
(633, 'dispositivos', '252', 'superadmin', 1602801941, 1602801941, 2),
(634, 'dispositivos', '253', 'superadmin', 1602802087, 1602802164, 2),
(635, 'dispositivos', '254', 'superadmin', 1602802216, 1602802309, 2),
(636, 'tipo_estado_dispo', '4', 'superadmin', 1602802489, 1602802489, 2),
(637, 'dispositivos', '255', 'superadmin', 1602802500, 1611615312, 2),
(638, 'dispositivos', '256', 'superadmin', 1602802595, 1611615264, 2),
(639, 'dispositivos', '257', 'superadmin', 1602802645, 1611615231, 2),
(640, 'verificacion', '66', 'superadmin', 1603139591, 1603139633, 2),
(641, 'unidades', '14', 'superadmin', 1603304941, 1603304941, 2),
(642, 'unidades', '15', 'superadmin', 1603305082, 1603305082, 2),
(643, 'contactos', '7', 'l.garcia', 1604517790, 1604517790, 3),
(644, 'calibraciones', '2', 'l.garcia', 1604681364, 1604681364, 3),
(645, 'calibraciones', '3', 'l.garcia', 1604681516, 1604681516, 3),
(646, 'calibraciones', '4', 'l.garcia', 1604681582, 1604681596, 3),
(647, 'documentos', '3', 'l.garcia', 1604681771, 1604681771, 3),
(648, 'documentos', '4', 'l.garcia', 1604681831, 1604681831, 3),
(649, 'documentos', '5', 'l.garcia', 1604681868, 1604681868, 3),
(650, 'documentos', '6', 'l.garcia', 1604681923, 1604681923, 3),
(651, 'mantenimientos', '2', 'l.garcia', 1604682051, 1605906307, 3),
(652, 'mantenimientos', '3', 'l.garcia', 1604682106, 1604682106, 3),
(653, 'mantenimientos', '4', 'l.garcia', 1604682177, 1604682177, 3),
(654, 'mantenimientos', '5', 'l.garcia', 1604682227, 1604682227, 3),
(655, 'mantenimientos', '6', 'l.garcia', 1604682281, 1604682281, 3),
(656, 'mantenimientos', '7', 'l.garcia', 1604682373, 1604682373, 3),
(659, 'documentos', '8', 'l.garcia', 1604869104, 1604869104, 3),
(658, 'mantenimientos', '8', 'l.garcia', 1604868966, 1604868966, 3),
(660, 'mantenimientos', '9', 'l.garcia', 1604869160, 1604869160, 3),
(661, 'mantenimientos', '10', 'l.garcia', 1604869208, 1604869208, 3),
(662, 'mantenimientos', '11', 'l.garcia', 1604869291, 1604869379, 3),
(663, 'mantenimientos', '12', 'l.garcia', 1604869480, 1604869480, 3),
(664, 'mantenimientos', '13', 'l.garcia', 1604870006, 1604870006, 3),
(665, 'mantenimientos', '14', 'l.garcia', 1604870319, 1604870319, 3),
(666, 'mantenimientos', '15', 'l.garcia', 1604870539, 1604870539, 3),
(667, 'mantenimientos', '16', 'l.garcia', 1604870638, 1604870638, 3),
(668, 'dispositivos', '258', 'l.garcia', 1604975527, 1605849586, 3),
(669, 'mantenimientos', '17', 'l.garcia', 1604975603, 1604976550, 3),
(670, 'calibraciones', '5', 'l.garcia', 1604975697, 1604975697, 3),
(671, 'documentos', '9', 'l.garcia', 1604975736, 1604975736, 3),
(672, 'tipo_documento', '11', 'l.garcia', 1604975844, 1604975844, 3),
(673, 'documentos', '10', 'l.garcia', 1604975972, 1604975972, 3),
(674, 'documentos', '11', 'l.garcia', 1604976015, 1604976146, 3),
(675, 'tipo_documento', '12', 'l.garcia', 1604976245, 1604976245, 3),
(676, 'documentos', '12', 'l.garcia', 1604976310, 1604976310, 3),
(677, 'documentos', '13', 'l.garcia', 1604976400, 1604976400, 3),
(678, 'documentos', '14', 'l.garcia', 1604976506, 1604976506, 3),
(679, 'tipo_dispositivo', '24', 'l.garcia', 1605216279, 1606176605, 3),
(680, 'tipo_dispositivo', '25', 'l.garcia', 1605216345, 1606176245, 3),
(681, 'verificacion', '67', 'superadmin', 1605297185, 1605297185, 2),
(682, 'verificacion', '68', 'superadmin', 1605297292, 1605297292, 2),
(683, 'movimientos', '142', 'felipem', 1605297432, 1605297432, 2),
(684, 'movi_envio', '1', 'felipem', 1605297491, 1605297491, 2),
(685, 'verificacion', '69', 'a.lopez', 1605634660, 1605634660, 3),
(686, 'verificacion', '70', 'a.lopez', 1605635103, 1605635103, 3),
(687, 'movimientos', '143', 'a.lopez', 1605635262, 1605635262, 3),
(688, 'contactos', '8', 'a.lopez', 1605635491, 1605635491, 3),
(689, 'movi_envio', '2', 'a.lopez', 1605635516, 1605635516, 3),
(690, 'verificacion', '71', 'a.lopez', 1605636343, 1605636343, 3),
(692, 'movimientos', '144', 'felipem', 1605644879, 1605644879, 2),
(693, 'movi_envio', '3', 'felipem', 1605644920, 1605644920, 2),
(694, 'movi_recepcion', '1', 'felipem', 1605645008, 1605645008, 2),
(695, 'movimientos', '145', 'felipem', 1605645270, 1605645270, 2),
(696, 'movi_recepcion', '2', 'felipem', 1605645337, 1605645337, 2),
(697, 'movimientos', '146', 'felipem', 1605645641, 1605645653, 2),
(698, 'movi_recepcion', '3', 'felipem', 1605645707, 1605645707, 2),
(699, 'verificacion', '73', 'a.lopez', 1605730413, 1605730413, 3),
(700, 'verificacion', '74', 'a.lopez', 1605730535, 1605730535, 3),
(701, 'verificacion', '75', 'a.lopez', 1605810037, 1605810037, 3),
(704, 'verificacion', '77', 'a.lopez', 1605813623, 1605813623, 3),
(703, 'verificacion', '76', 'a.lopez', 1605811559, 1605811559, 3),
(705, 'movimientos', '148', 'a.lopez', 1605814752, 1605815502, 3),
(706, 'movi_envio', '4', 'a.lopez', 1605815391, 1605815391, 3),
(707, 'movi_recepcion', '4', 'a.lopez', 1605815491, 1605815491, 3),
(708, 'verificacion', '78', 'a.lopez', 1605816014, 1605816014, 3),
(709, 'movimientos', '149', 'a.lopez', 1605816215, 1605816482, 3),
(710, 'movi_envio', '5', 'a.lopez', 1605816297, 1605816297, 3),
(711, 'movi_recepcion', '5', 'a.lopez', 1605816461, 1605816461, 3),
(712, 'verificacion', '79', 'a.lopez', 1605816559, 1605816559, 3),
(713, 'dispositivos', '259', 'felipem', 1605844916, 1605844916, 2),
(714, 'unidades', '16', 'felipem', 1605846105, 1605846105, 2),
(715, 'dispositivos', '260', 'felipem', 1605846551, 1617994224, 2),
(716, 'dispositivos', '261', 'felipem', 1605846551, 1605846861, 2),
(717, 'dispositivos', '262', 'felipem', 1605846551, 1605846861, 2),
(718, 'dispositivos', '263', 'felipem', 1605846551, 1605846861, 2),
(719, 'dispositivos', '264', 'felipem', 1605846551, 1605846861, 2),
(720, 'dispositivos', '265', 'felipem', 1605846551, 1605846861, 2),
(721, 'dispositivos', '266', 'felipem', 1605846551, 1605846861, 2),
(722, 'dispositivos', '267', 'felipem', 1605846551, 1605846861, 2),
(723, 'dispositivos', '268', 'felipem', 1605846551, 1605846861, 2),
(724, 'dispositivos', '269', 'felipem', 1605846551, 1605846861, 2),
(725, 'dispositivos', '270', 'felipem', 1605846551, 1605846861, 2),
(726, 'dispositivos', '271', 'felipem', 1605846551, 1605846861, 2),
(727, 'dispositivos', '272', 'felipem', 1605846551, 1605846861, 2),
(728, 'dispositivos', '273', 'felipem', 1605846551, 1605846861, 2),
(729, 'dispositivos', '274', 'felipem', 1605846551, 1605846861, 2),
(730, 'dispositivos', '275', 'felipem', 1605846551, 1605846861, 2),
(731, 'dispositivos', '276', 'felipem', 1605846551, 1605846861, 2),
(732, 'dispositivos', '277', 'felipem', 1605846551, 1605846861, 2),
(733, 'dispositivos', '278', 'felipem', 1605846551, 1605846861, 2),
(734, 'dispositivos', '279', 'felipem', 1605846551, 1605846861, 2),
(735, 'dispositivos', '280', 'felipem', 1605846551, 1605846861, 2),
(736, 'dispositivos', '281', 'felipem', 1605846552, 1605846861, 2),
(737, 'dispositivos', '282', 'felipem', 1605846552, 1605846861, 2),
(738, 'dispositivos', '283', 'felipem', 1605846552, 1605846861, 2),
(739, 'dispositivos', '284', 'felipem', 1605846552, 1605846861, 2),
(740, 'dispositivos', '285', 'felipem', 1605846552, 1605846861, 2),
(741, 'dispositivos', '286', 'felipem', 1605846552, 1605846861, 2),
(742, 'dispositivos', '287', 'felipem', 1605846552, 1605846861, 2),
(743, 'dispositivos', '288', 'felipem', 1605846552, 1605846861, 2),
(744, 'dispositivos', '289', 'felipem', 1605846552, 1605846862, 2),
(745, 'dispositivos', '290', 'felipem', 1605846552, 1605846862, 2),
(746, 'dispositivos', '291', 'felipem', 1605846552, 1605846862, 2),
(747, 'dispositivos', '292', 'felipem', 1605846552, 1605846862, 2),
(748, 'dispositivos', '293', 'felipem', 1605846552, 1605846862, 2),
(749, 'dispositivos', '294', 'felipem', 1605846552, 1605846862, 2),
(750, 'dispositivos', '295', 'felipem', 1605846552, 1605846862, 2),
(751, 'dispositivos', '296', 'felipem', 1605846552, 1605846862, 2),
(752, 'dispositivos', '297', 'felipem', 1605846553, 1605846862, 2),
(753, 'dispositivos', '298', 'felipem', 1605846553, 1605846862, 2),
(754, 'dispositivos', '299', 'felipem', 1605846553, 1605846862, 2),
(755, 'dispositivos', '300', 'felipem', 1605846553, 1605846862, 2),
(756, 'dispositivos', '301', 'felipem', 1605846553, 1605846862, 2),
(757, 'dispositivos', '302', 'felipem', 1605846553, 1605846862, 2),
(758, 'dispositivos', '303', 'felipem', 1605846553, 1605846862, 2),
(759, 'dispositivos', '304', 'felipem', 1605846553, 1605846862, 2),
(760, 'dispositivos', '305', 'felipem', 1605846553, 1605846862, 2),
(761, 'dispositivos', '306', 'felipem', 1605846553, 1605846862, 2),
(762, 'dispositivos', '307', 'felipem', 1605846553, 1605846862, 2),
(763, 'dispositivos', '308', 'felipem', 1605846553, 1605846862, 2),
(764, 'dispositivos', '309', 'felipem', 1605846553, 1605846862, 2),
(765, 'dispositivos', '310', 'felipem', 1605846553, 1605846862, 2),
(766, 'dispositivos', '311', NULL, 1605846553, 1605846553, NULL),
(767, 'dispositivos', '312', 'felipem', 1605846553, 1606576978, 2),
(768, 'dispositivos', '313', 'felipem', 1605846553, 1614633974, 2),
(769, 'dispositivos', '314', 'felipem', 1605846553, 1617913001, 2),
(770, 'dispositivos', '315', 'felipem', 1605846553, 1605846862, 2),
(771, 'dispositivos', '316', 'felipem', 1605846553, 1605846862, 2),
(772, 'dispositivos', '317', 'felipem', 1605846553, 1605846862, 2),
(773, 'dispositivos', '318', 'felipem', 1605846553, 1607030106, 2),
(774, 'dispositivos', '319', 'felipem', 1605846553, 1605846862, 2);
INSERT INTO `membership_userrecords` (`recID`, `tableName`, `pkValue`, `memberID`, `dateAdded`, `dateUpdated`, `groupID`) VALUES
(775, 'dispositivos', '320', 'felipem', 1605846553, 1605846862, 2),
(776, 'dispositivos', '321', 'felipem', 1605846553, 1605846862, 2),
(777, 'dispositivos', '322', 'felipem', 1605846553, 1605846862, 2),
(778, 'dispositivos', '323', 'felipem', 1605846553, 1605846863, 2),
(779, 'dispositivos', '324', 'felipem', 1605846553, 1605846863, 2),
(780, 'dispositivos', '325', 'felipem', 1605846553, 1605846863, 2),
(781, 'dispositivos', '326', 'felipem', 1605846553, 1605846863, 2),
(782, 'dispositivos', '327', 'felipem', 1605846553, 1605846863, 2),
(783, 'dispositivos', '328', 'felipem', 1605846553, 1605846863, 2),
(784, 'dispositivos', '329', 'felipem', 1605846553, 1605846863, 2),
(785, 'dispositivos', '330', 'felipem', 1605846553, 1605846863, 2),
(786, 'dispositivos', '331', 'felipem', 1605846553, 1605846863, 2),
(787, 'dispositivos', '332', 'felipem', 1605846553, 1605846863, 2),
(788, 'dispositivos', '333', 'felipem', 1605846553, 1605846863, 2),
(789, 'dispositivos', '334', 'felipem', 1605846553, 1605846863, 2),
(790, 'dispositivos', '335', 'felipem', 1605846553, 1605846863, 2),
(791, 'dispositivos', '336', NULL, 1605846553, 1613749479, NULL),
(792, 'dispositivos', '337', 'felipem', 1605846553, 1605846553, 2),
(793, 'dispositivos', '338', 'felipem', 1605846553, 1606586338, 2),
(794, 'dispositivos', '339', 'felipem', 1605846553, 1605846863, 2),
(795, 'dispositivos', '340', 'felipem', 1605846553, 1605846863, 2),
(796, 'dispositivos', '341', 'felipem', 1605846553, 1605846863, 2),
(797, 'dispositivos', '342', 'felipem', 1605846553, 1605846863, 2),
(798, 'dispositivos', '343', 'felipem', 1605846553, 1605846863, 2),
(799, 'dispositivos', '344', 'felipem', 1605846553, 1605846863, 2),
(800, 'dispositivos', '345', 'felipem', 1605846553, 1605846863, 2),
(801, 'dispositivos', '346', 'felipem', 1605846553, 1605846863, 2),
(802, 'dispositivos', '347', 'felipem', 1605846553, 1605846863, 2),
(803, 'dispositivos', '348', 'felipem', 1605846553, 1605846863, 2),
(804, 'dispositivos', '349', 'felipem', 1605846553, 1605846863, 2),
(805, 'dispositivos', '350', 'felipem', 1605846553, 1617994591, 2),
(806, 'dispositivos', '351', 'felipem', 1605846553, 1605846863, 2),
(807, 'dispositivos', '352', 'felipem', 1605846553, 1607030360, 2),
(808, 'dispositivos', '353', 'felipem', 1605846553, 1605846863, 2),
(809, 'dispositivos', '354', 'felipem', 1605846553, 1605846863, 2),
(810, 'dispositivos', '355', 'felipem', 1605846554, 1618331144, 2),
(811, 'dispositivos', '356', 'felipem', 1605846554, 1605846863, 2),
(812, 'dispositivos', '357', 'felipem', 1605846554, 1605846863, 2),
(813, 'dispositivos', '358', 'felipem', 1605846554, 1605846863, 2),
(814, 'dispositivos', '359', 'felipem', 1605846554, 1605846863, 2),
(815, 'dispositivos', '360', NULL, 1605846554, 1618329265, NULL),
(816, 'dispositivos', '361', 'felipem', 1605846554, 1605900201, 2),
(817, 'dispositivos', '362', 'felipem', 1605846554, 1605846863, 2),
(818, 'dispositivos', '363', 'felipem', 1605846554, 1605846863, 2),
(819, 'dispositivos', '364', 'felipem', 1605846554, 1605846863, 2),
(820, 'dispositivos', '365', 'felipem', 1605846554, 1605846863, 2),
(821, 'dispositivos', '366', 'felipem', 1605846554, 1605846863, 2),
(822, 'dispositivos', '367', 'felipem', 1605846554, 1605846863, 2),
(823, 'dispositivos', '368', 'felipem', 1605846555, 1605846863, 2),
(824, 'dispositivos', '369', 'felipem', 1605846555, 1605846863, 2),
(825, 'dispositivos', '370', 'felipem', 1605846555, 1605846863, 2),
(826, 'dispositivos', '371', 'felipem', 1605846555, 1605846863, 2),
(827, 'dispositivos', '372', 'felipem', 1605846555, 1605846863, 2),
(828, 'dispositivos', '373', 'felipem', 1605846555, 1605846863, 2),
(829, 'dispositivos', '374', 'felipem', 1605846555, 1605846863, 2),
(830, 'dispositivos', '375', 'felipem', 1605846555, 1605846863, 2),
(831, 'dispositivos', '376', 'felipem', 1605846555, 1605846863, 2),
(832, 'dispositivos', '377', 'felipem', 1605846555, 1605846863, 2),
(833, 'dispositivos', '378', 'felipem', 1605846555, 1605846863, 2),
(834, 'dispositivos', '379', 'felipem', 1605846555, 1605846863, 2),
(835, 'dispositivos', '380', 'felipem', 1605846555, 1605846863, 2),
(836, 'dispositivos', '381', 'felipem', 1605846555, 1605846864, 2),
(837, 'dispositivos', '382', 'felipem', 1605846555, 1605846864, 2),
(838, 'dispositivos', '383', 'felipem', 1605846555, 1605846864, 2),
(839, 'dispositivos', '384', 'felipem', 1605846555, 1605846864, 2),
(840, 'dispositivos', '385', 'felipem', 1605846555, 1605846864, 2),
(841, 'dispositivos', '386', 'felipem', 1605846555, 1605846864, 2),
(842, 'dispositivos', '387', 'felipem', 1605846555, 1605846864, 2),
(843, 'dispositivos', '388', 'felipem', 1605846555, 1605846864, 2),
(844, 'dispositivos', '389', 'felipem', 1605846555, 1605846864, 2),
(845, 'dispositivos', '390', 'felipem', 1605846555, 1605846864, 2),
(846, 'dispositivos', '391', 'felipem', 1605846555, 1605846864, 2),
(847, 'dispositivos', '392', 'felipem', 1605846555, 1605846864, 2),
(848, 'dispositivos', '393', 'felipem', 1605846555, 1605846864, 2),
(849, 'dispositivos', '394', 'felipem', 1605846555, 1605846864, 2),
(850, 'dispositivos', '395', 'felipem', 1605846555, 1605846864, 2),
(851, 'dispositivos', '396', 'felipem', 1605846555, 1605846864, 2),
(852, 'dispositivos', '397', 'felipem', 1605846555, 1605846864, 2),
(853, 'dispositivos', '398', 'felipem', 1605846555, 1605846864, 2),
(854, 'dispositivos', '399', 'felipem', 1605846555, 1605846864, 2),
(855, 'dispositivos', '400', 'felipem', 1605846555, 1605846864, 2),
(856, 'dispositivos', '401', 'felipem', 1605846556, 1605846864, 2),
(857, 'dispositivos', '402', 'felipem', 1605846556, 1605846864, 2),
(858, 'dispositivos', '403', 'felipem', 1605846556, 1605846864, 2),
(859, 'dispositivos', '404', 'felipem', 1605846556, 1605846864, 2),
(860, 'dispositivos', '405', NULL, 1605846556, 1605846556, NULL),
(861, 'dispositivos', '406', 'felipem', 1605846556, 1606318575, 2),
(862, 'dispositivos', '407', 'felipem', 1605846556, 1605846864, 2),
(863, 'dispositivos', '408', 'felipem', 1605846556, 1605846864, 2),
(864, 'dispositivos', '409', 'felipem', 1605846556, 1605846865, 2),
(865, 'dispositivos', '410', 'felipem', 1605846556, 1605846865, 2),
(866, 'dispositivos', '411', 'felipem', 1605846556, 1605846865, 2),
(867, 'dispositivos', '412', 'felipem', 1605846556, 1605846865, 2),
(868, 'dispositivos', '413', 'felipem', 1605846556, 1605846865, 2),
(869, 'dispositivos', '414', 'felipem', 1605846556, 1605846865, 2),
(870, 'dispositivos', '415', 'felipem', 1605846556, 1605846865, 2),
(871, 'dispositivos', '416', 'felipem', 1605846556, 1605846865, 2),
(872, 'dispositivos', '417', 'felipem', 1605846556, 1605846865, 2),
(873, 'dispositivos', '418', 'felipem', 1605846556, 1605846865, 2),
(874, 'dispositivos', '419', 'felipem', 1605846556, 1605846865, 2),
(875, 'dispositivos', '420', 'felipem', 1605846556, 1605846865, 2),
(876, 'dispositivos', '421', 'felipem', 1605846556, 1605846865, 2),
(877, 'dispositivos', '422', 'felipem', 1605846556, 1605846865, 2),
(878, 'dispositivos', '423', 'felipem', 1605846556, 1605846865, 2),
(879, 'dispositivos', '424', 'felipem', 1605846556, 1605846865, 2),
(880, 'dispositivos', '425', 'felipem', 1605846556, 1605846865, 2),
(881, 'dispositivos', '426', 'felipem', 1605846556, 1605846865, 2),
(882, 'dispositivos', '427', 'felipem', 1605846556, 1605846865, 2),
(883, 'dispositivos', '428', 'felipem', 1605846556, 1605846865, 2),
(884, 'dispositivos', '429', 'felipem', 1605846556, 1605846865, 2),
(885, 'dispositivos', '430', 'felipem', 1605846556, 1605846865, 2),
(886, 'dispositivos', '431', 'felipem', 1605846556, 1605846865, 2),
(887, 'dispositivos', '432', 'felipem', 1605846556, 1605846865, 2),
(888, 'dispositivos', '433', 'felipem', 1605846556, 1605846865, 2),
(889, 'dispositivos', '434', 'felipem', 1605846556, 1605846865, 2),
(890, 'dispositivos', '435', 'felipem', 1605846556, 1606239006, 2),
(891, 'dispositivos', '436', 'felipem', 1605846556, 1605846865, 2),
(892, 'dispositivos', '437', 'felipem', 1605846556, 1605846865, 2),
(893, 'dispositivos', '438', 'felipem', 1605846556, 1605846865, 2),
(894, 'dispositivos', '439', 'felipem', 1605846556, 1605846865, 2),
(895, 'dispositivos', '440', 'felipem', 1605846556, 1605846865, 2),
(896, 'dispositivos', '441', 'felipem', 1605846556, 1605846865, 2),
(897, 'dispositivos', '442', 'felipem', 1605846556, 1605846865, 2),
(898, 'dispositivos', '443', 'felipem', 1605846556, 1605846866, 2),
(899, 'dispositivos', '444', 'felipem', 1605846556, 1605846866, 2),
(900, 'dispositivos', '445', 'felipem', 1605846556, 1605846866, 2),
(901, 'dispositivos', '446', 'felipem', 1605846556, 1605846866, 2),
(902, 'dispositivos', '447', 'felipem', 1605846556, 1605846866, 2),
(903, 'dispositivos', '448', 'felipem', 1605846556, 1605846866, 2),
(904, 'dispositivos', '449', 'felipem', 1605846556, 1605846866, 2),
(905, 'dispositivos', '450', 'felipem', 1605846556, 1605846866, 2),
(906, 'dispositivos', '451', 'felipem', 1605846557, 1605846866, 2),
(907, 'dispositivos', '452', 'felipem', 1605846557, 1605846866, 2),
(908, 'dispositivos', '453', NULL, 1605846557, 1605846557, NULL),
(909, 'dispositivos', '454', 'felipem', 1605846557, 1605889185, 2),
(910, 'dispositivos', '455', 'felipem', 1605846557, 1605846866, 2),
(911, 'dispositivos', '456', 'felipem', 1605846557, 1605846866, 2),
(912, 'dispositivos', '457', 'felipem', 1605846557, 1605846866, 2),
(913, 'dispositivos', '458', 'felipem', 1605846557, 1605846866, 2),
(914, 'dispositivos', '459', 'felipem', 1605846557, 1605846866, 2),
(915, 'dispositivos', '460', 'felipem', 1605846557, 1605846866, 2),
(916, 'dispositivos', '461', 'felipem', 1605846557, 1605846866, 2),
(917, 'dispositivos', '462', 'felipem', 1605846557, 1605846866, 2),
(918, 'dispositivos', '463', 'felipem', 1605846557, 1605846866, 2),
(919, 'dispositivos', '464', 'felipem', 1605846557, 1605846866, 2),
(920, 'dispositivos', '465', 'felipem', 1605846557, 1605846866, 2),
(921, 'dispositivos', '466', 'felipem', 1605846557, 1605846866, 2),
(922, 'dispositivos', '467', 'felipem', 1605846557, 1605846866, 2),
(923, 'dispositivos', '468', 'felipem', 1605846557, 1605846866, 2),
(924, 'dispositivos', '469', 'felipem', 1605846557, 1605846866, 2),
(925, 'dispositivos', '470', 'felipem', 1605846557, 1605846866, 2),
(926, 'dispositivos', '471', 'felipem', 1605846557, 1617975531, 2),
(927, 'dispositivos', '472', 'felipem', 1605846557, 1605846866, 2),
(928, 'dispositivos', '473', 'felipem', 1605846557, 1605846866, 2),
(929, 'dispositivos', '474', 'felipem', 1605846557, 1605846866, 2),
(930, 'dispositivos', '475', 'felipem', 1605846557, 1605846866, 2),
(931, 'dispositivos', '476', 'felipem', 1605846557, 1605846866, 2),
(932, 'dispositivos', '477', 'felipem', 1605846557, 1605846866, 2),
(933, 'dispositivos', '478', 'felipem', 1605846557, 1605846866, 2),
(934, 'dispositivos', '479', 'felipem', 1605846557, 1605846866, 2),
(935, 'dispositivos', '480', 'felipem', 1605846557, 1605846866, 2),
(936, 'dispositivos', '481', 'felipem', 1605846558, 1605846866, 2),
(937, 'dispositivos', '482', 'felipem', 1605846558, 1605846866, 2),
(938, 'dispositivos', '483', 'felipem', 1605846558, 1605846866, 2),
(939, 'dispositivos', '484', 'felipem', 1605846558, 1605846866, 2),
(940, 'dispositivos', '485', 'felipem', 1605846558, 1605846866, 2),
(941, 'dispositivos', '486', 'felipem', 1605846558, 1605846866, 2),
(942, 'dispositivos', '487', 'felipem', 1605846558, 1605846866, 2),
(943, 'dispositivos', '488', 'felipem', 1605846558, 1605846866, 2),
(944, 'dispositivos', '489', NULL, 1605846558, 1605846558, NULL),
(945, 'dispositivos', '490', 'felipem', 1605846558, 1605846558, 2),
(946, 'dispositivos', '491', 'felipem', 1605846558, 1605846867, 2),
(947, 'dispositivos', '492', 'felipem', 1605846558, 1605846867, 2),
(948, 'dispositivos', '493', 'felipem', 1605846558, 1605846867, 2),
(949, 'dispositivos', '494', 'felipem', 1605846558, 1605846867, 2),
(950, 'dispositivos', '495', 'felipem', 1605846558, 1605846867, 2),
(951, 'dispositivos', '496', 'felipem', 1605846558, 1605846867, 2),
(952, 'dispositivos', '497', 'felipem', 1605846558, 1605846867, 2),
(953, 'dispositivos', '498', 'felipem', 1605846558, 1605846867, 2),
(954, 'dispositivos', '499', 'felipem', 1605846558, 1605846867, 2),
(955, 'dispositivos', '500', 'felipem', 1605846558, 1605846867, 2),
(956, 'dispositivos', '501', 'felipem', 1605846558, 1605846867, 2),
(957, 'dispositivos', '502', 'felipem', 1605846558, 1605846867, 2),
(958, 'dispositivos', '503', 'felipem', 1605846558, 1605846867, 2),
(959, 'dispositivos', '504', 'felipem', 1605846558, 1605846867, 2),
(960, 'dispositivos', '505', 'felipem', 1605846558, 1605846867, 2),
(961, 'dispositivos', '506', 'felipem', 1605846558, 1605846867, 2),
(962, 'dispositivos', '507', 'felipem', 1605846558, 1605846867, 2),
(963, 'dispositivos', '508', 'felipem', 1605846558, 1605846867, 2),
(964, 'dispositivos', '509', 'felipem', 1605846558, 1605846867, 2),
(965, 'dispositivos', '510', 'felipem', 1605846558, 1605846867, 2),
(966, 'dispositivos', '511', 'felipem', 1605846558, 1605846867, 2),
(967, 'dispositivos', '512', 'felipem', 1605846559, 1605846868, 2),
(968, 'dispositivos', '513', 'felipem', 1605846559, 1605846868, 2),
(969, 'dispositivos', '514', 'felipem', 1605846559, 1618328523, 2),
(970, 'dispositivos', '515', 'felipem', 1605846559, 1605846868, 2),
(971, 'dispositivos', '516', 'felipem', 1605846559, 1605846868, 2),
(972, 'dispositivos', '517', 'felipem', 1605846559, 1605846868, 2),
(973, 'dispositivos', '518', 'felipem', 1605846559, 1605846868, 2),
(974, 'dispositivos', '519', 'felipem', 1605846559, 1605846868, 2),
(975, 'dispositivos', '520', 'felipem', 1605846559, 1605846868, 2),
(976, 'dispositivos', '521', 'felipem', 1605846559, 1605846868, 2),
(977, 'dispositivos', '522', 'felipem', 1605846559, 1605846868, 2),
(978, 'dispositivos', '523', 'felipem', 1605846559, 1605846868, 2),
(979, 'dispositivos', '524', 'felipem', 1605846559, 1605846868, 2),
(980, 'dispositivos', '525', 'felipem', 1605846559, 1605846868, 2),
(981, 'dispositivos', '526', 'felipem', 1605846559, 1605846868, 2),
(982, 'dispositivos', '527', 'felipem', 1605846559, 1605846868, 2),
(983, 'dispositivos', '528', 'felipem', 1605846559, 1605846868, 2),
(984, 'dispositivos', '529', 'felipem', 1605846559, 1605846868, 2),
(985, 'dispositivos', '530', 'felipem', 1605846559, 1605846868, 2),
(986, 'dispositivos', '531', NULL, 1605846559, 1605846559, NULL),
(987, 'dispositivos', '532', 'felipem', 1605846559, 1605846559, 2),
(988, 'dispositivos', '533', 'felipem', 1605846559, 1605846868, 2),
(989, 'dispositivos', '534', 'felipem', 1605846559, 1605846868, 2),
(990, 'dispositivos', '535', 'felipem', 1605846559, 1605846868, 2),
(991, 'dispositivos', '536', 'felipem', 1605846559, 1605846868, 2),
(992, 'dispositivos', '537', 'felipem', 1605846559, 1605846869, 2),
(993, 'dispositivos', '538', 'felipem', 1605846559, 1605846869, 2),
(994, 'dispositivos', '539', 'felipem', 1605846559, 1605846869, 2),
(995, 'dispositivos', '540', 'felipem', 1605846559, 1605846869, 2),
(996, 'dispositivos', '541', 'felipem', 1605846559, 1605846869, 2),
(997, 'dispositivos', '542', 'felipem', 1605846559, 1605846869, 2),
(998, 'dispositivos', '543', 'felipem', 1605846559, 1605846869, 2),
(999, 'dispositivos', '544', 'felipem', 1605846559, 1605846869, 2),
(1000, 'dispositivos', '545', 'felipem', 1605846559, 1605846869, 2),
(1001, 'dispositivos', '546', 'felipem', 1605846559, 1605846869, 2),
(1002, 'dispositivos', '547', 'felipem', 1605846559, 1605846869, 2),
(1003, 'dispositivos', '548', 'felipem', 1605846559, 1605846869, 2),
(1004, 'dispositivos', '549', 'felipem', 1605846559, 1605846869, 2),
(1005, 'dispositivos', '550', 'felipem', 1605846559, 1605846869, 2),
(1006, 'dispositivos', '551', 'felipem', 1605846559, 1605846869, 2),
(1007, 'dispositivos', '552', 'felipem', 1605846559, 1606584775, 2),
(1008, 'dispositivos', '553', 'felipem', 1605846559, 1605846869, 2),
(1009, 'dispositivos', '554', 'felipem', 1605846559, 1605846869, 2),
(1010, 'dispositivos', '555', NULL, 1605846559, 1605846559, NULL),
(1465, 'verificacion', '80', 'felipem', 1605880154, 1605880154, 2),
(1012, 'dispositivos', '557', 'felipem', 1605846559, 1606413431, 2),
(1013, 'dispositivos', '558', 'felipem', 1605846559, 1606146766, 2),
(1014, 'dispositivos', '559', 'felipem', 1605846559, 1605846869, 2),
(1015, 'dispositivos', '560', 'felipem', 1605846559, 1605846869, 2),
(1016, 'dispositivos', '561', 'felipem', 1605846559, 1605846869, 2),
(1017, 'dispositivos', '562', 'felipem', 1605846560, 1605846869, 2),
(1018, 'dispositivos', '563', 'felipem', 1605846560, 1605846869, 2),
(1019, 'dispositivos', '564', 'felipem', 1605846560, 1605846869, 2),
(1020, 'dispositivos', '565', 'felipem', 1605846560, 1605846869, 2),
(1021, 'dispositivos', '566', 'felipem', 1605846560, 1607092971, 2),
(1022, 'dispositivos', '567', 'felipem', 1605846560, 1605846869, 2),
(1023, 'dispositivos', '568', 'felipem', 1605846560, 1605846869, 2),
(1024, 'dispositivos', '569', 'felipem', 1605846560, 1605846869, 2),
(1025, 'dispositivos', '570', 'felipem', 1605846560, 1605846869, 2),
(1026, 'dispositivos', '571', 'felipem', 1605846560, 1605846869, 2),
(1027, 'dispositivos', '572', 'felipem', 1605846560, 1605846869, 2),
(1028, 'dispositivos', '573', 'felipem', 1605846560, 1605846869, 2),
(1029, 'dispositivos', '574', 'felipem', 1605846560, 1605846869, 2),
(1030, 'dispositivos', '575', 'felipem', 1605846560, 1605846869, 2),
(1031, 'dispositivos', '576', 'felipem', 1605846560, 1605846869, 2),
(1032, 'dispositivos', '577', 'felipem', 1605846560, 1605846869, 2),
(1033, 'dispositivos', '578', 'felipem', 1605846560, 1605846869, 2),
(1034, 'dispositivos', '579', 'felipem', 1605846560, 1605849202, 2),
(1035, 'dispositivos', '580', NULL, 1605846560, 1605849319, NULL),
(1036, 'dispositivos', '581', 'felipem', 1605846560, 1606173900, 2),
(1037, 'dispositivos', '582', 'felipem', 1605846560, 1606750793, 2),
(1038, 'dispositivos', '583', 'felipem', 1605846560, 1606751791, 2),
(1039, 'dispositivos', '584', 'felipem', 1605846560, 1606751860, 2),
(1040, 'dispositivos', '585', 'felipem', 1605846560, 1607005490, 2),
(1041, 'dispositivos', '586', 'felipem', 1605846560, 1614099453, 2),
(1042, 'dispositivos', '587', 'felipem', 1605846560, 1614098403, 2),
(1043, 'dispositivos', '588', 'felipem', 1605846560, 1606325247, 2),
(1044, 'dispositivos', '589', 'felipem', 1605846560, 1610121709, 2),
(1045, 'dispositivos', '590', 'felipem', 1605846561, 1611353188, 2),
(1046, 'dispositivos', '591', 'felipem', 1605846561, 1611675312, 2),
(1047, 'dispositivos', '592', 'felipem', 1605846561, 1617748343, 2),
(1048, 'dispositivos', '593', 'felipem', 1605846561, 1617748042, 2),
(1049, 'dispositivos', '594', 'felipem', 1605846561, 1614095172, 2),
(1050, 'dispositivos', '595', 'felipem', 1605846561, 1605846870, 2),
(1051, 'dispositivos', '596', 'felipem', 1605846561, 1605846870, 2),
(1052, 'dispositivos', '597', 'felipem', 1605846561, 1605846870, 2),
(1053, 'dispositivos', '598', 'felipem', 1605846561, 1605846870, 2),
(1054, 'dispositivos', '599', 'felipem', 1605846561, 1605846870, 2),
(1055, 'dispositivos', '600', 'felipem', 1605846561, 1605846870, 2),
(1056, 'dispositivos', '601', 'felipem', 1605846561, 1605846870, 2),
(1057, 'dispositivos', '602', 'felipem', 1605846561, 1605846870, 2),
(1058, 'dispositivos', '603', 'felipem', 1605846561, 1605846870, 2),
(1059, 'dispositivos', '604', 'felipem', 1605846561, 1605846870, 2),
(1060, 'dispositivos', '605', 'felipem', 1605846561, 1605846870, 2),
(1061, 'dispositivos', '606', 'felipem', 1605846561, 1605846871, 2),
(1062, 'dispositivos', '607', 'felipem', 1605846561, 1605846871, 2),
(1063, 'dispositivos', '608', 'felipem', 1605846561, 1605846871, 2),
(1064, 'dispositivos', '609', 'felipem', 1605846561, 1605846871, 2),
(1065, 'dispositivos', '610', 'felipem', 1605846561, 1605846871, 2),
(1066, 'dispositivos', '611', 'felipem', 1605846561, 1605846871, 2),
(1067, 'dispositivos', '612', 'felipem', 1605846561, 1605846871, 2),
(1068, 'dispositivos', '613', 'felipem', 1605846561, 1605846871, 2),
(1069, 'dispositivos', '614', 'felipem', 1605846561, 1605846871, 2),
(1070, 'dispositivos', '615', 'felipem', 1605846561, 1610129172, 2),
(1071, 'dispositivos', '616', 'felipem', 1605846561, 1610129120, 2),
(1072, 'dispositivos', '617', 'felipem', 1605846561, 1610129480, 2),
(1073, 'dispositivos', '618', 'felipem', 1605846561, 1605846871, 2),
(1074, 'dispositivos', '619', 'felipem', 1605846561, 1605846871, 2),
(1075, 'dispositivos', '620', 'felipem', 1605846561, 1610125532, 2),
(1076, 'dispositivos', '621', 'felipem', 1605846561, 1605846872, 2),
(1077, 'dispositivos', '622', 'felipem', 1605846561, 1610125547, 2),
(1078, 'dispositivos', '623', 'felipem', 1605846561, 1605846872, 2),
(1079, 'dispositivos', '624', 'felipem', 1605846561, 1605846872, 2),
(1080, 'dispositivos', '625', 'felipem', 1605846561, 1605846872, 2),
(1081, 'dispositivos', '626', 'felipem', 1605846561, 1605846872, 2),
(1082, 'dispositivos', '627', 'felipem', 1605846561, 1605846872, 2),
(1083, 'dispositivos', '628', 'felipem', 1605846562, 1605846872, 2),
(1084, 'dispositivos', '629', 'felipem', 1605846562, 1605846872, 2),
(1085, 'dispositivos', '630', 'felipem', 1605846562, 1608070814, 2),
(1086, 'dispositivos', '631', 'felipem', 1605846562, 1607029166, 2),
(1087, 'dispositivos', '632', 'felipem', 1605846562, 1605846872, 2),
(1088, 'dispositivos', '633', 'felipem', 1605846562, 1606584691, 2),
(1089, 'dispositivos', '634', 'felipem', 1605846562, 1615487274, 2),
(1090, 'dispositivos', '635', 'felipem', 1605846562, 1605846872, 2),
(1091, 'dispositivos', '636', 'felipem', 1605846562, 1605846872, 2),
(1092, 'dispositivos', '637', 'felipem', 1605846562, 1605846872, 2),
(1093, 'dispositivos', '638', 'felipem', 1605846562, 1605846872, 2),
(1094, 'dispositivos', '639', 'felipem', 1605846562, 1605846872, 2),
(1095, 'dispositivos', '640', 'felipem', 1605846562, 1605846872, 2),
(1096, 'dispositivos', '641', 'felipem', 1605846562, 1605846872, 2),
(1097, 'dispositivos', '642', 'felipem', 1605846562, 1605846872, 2),
(1098, 'dispositivos', '643', 'felipem', 1605846562, 1605846872, 2),
(1099, 'dispositivos', '644', 'felipem', 1605846562, 1605846872, 2),
(1100, 'dispositivos', '645', 'felipem', 1605846562, 1605846872, 2),
(1101, 'dispositivos', '646', 'felipem', 1605846562, 1605846872, 2),
(1102, 'dispositivos', '647', 'felipem', 1605846562, 1605846872, 2),
(1103, 'dispositivos', '648', 'felipem', 1605846562, 1605846872, 2),
(1104, 'dispositivos', '649', 'felipem', 1605846562, 1605846872, 2),
(1105, 'dispositivos', '650', 'felipem', 1605846562, 1605846872, 2),
(1106, 'dispositivos', '651', 'felipem', 1605846562, 1605846872, 2),
(1107, 'dispositivos', '652', 'felipem', 1605846562, 1605846872, 2),
(1108, 'dispositivos', '653', 'felipem', 1605846562, 1605846872, 2),
(1109, 'dispositivos', '654', NULL, 1605846564, 1609271262, NULL),
(1110, 'dispositivos', '655', 'felipem', 1605846564, 1605846564, 2),
(1111, 'dispositivos', '656', 'felipem', 1605846564, 1605846872, 2),
(1112, 'dispositivos', '657', 'felipem', 1605846564, 1605846872, 2),
(1113, 'dispositivos', '658', 'felipem', 1605846565, 1605846872, 2),
(1114, 'dispositivos', '659', 'felipem', 1605846565, 1605846872, 2),
(1115, 'dispositivos', '660', 'felipem', 1605846565, 1605846872, 2),
(1116, 'dispositivos', '661', 'felipem', 1605846565, 1605846872, 2),
(1117, 'dispositivos', '662', 'felipem', 1605846565, 1605846872, 2),
(1118, 'dispositivos', '663', 'felipem', 1605846566, 1605846872, 2),
(1119, 'dispositivos', '664', 'felipem', 1605846566, 1605846872, 2),
(1120, 'dispositivos', '665', 'felipem', 1605846566, 1605846872, 2),
(1121, 'dispositivos', '666', 'felipem', 1605846566, 1605846872, 2),
(1122, 'dispositivos', '667', 'felipem', 1605846566, 1605846872, 2),
(1123, 'dispositivos', '668', 'felipem', 1605846566, 1605846872, 2),
(1124, 'dispositivos', '669', 'felipem', 1605846566, 1605846872, 2),
(1125, 'dispositivos', '670', 'felipem', 1605846566, 1605846872, 2),
(1126, 'dispositivos', '671', 'felipem', 1605846566, 1605846872, 2),
(1127, 'dispositivos', '672', 'felipem', 1605846566, 1605846872, 2),
(1128, 'dispositivos', '673', 'felipem', 1605846566, 1605846872, 2),
(1129, 'dispositivos', '674', 'felipem', 1605846566, 1605846872, 2),
(1130, 'dispositivos', '675', 'felipem', 1605846566, 1605846872, 2),
(1131, 'dispositivos', '676', 'felipem', 1605846566, 1605846872, 2),
(1132, 'dispositivos', '677', 'felipem', 1605846567, 1605846872, 2),
(1133, 'dispositivos', '678', 'felipem', 1605846567, 1605846872, 2),
(1134, 'dispositivos', '679', 'felipem', 1605846567, 1614625060, 2),
(1135, 'dispositivos', '680', 'felipem', 1605846567, 1605846872, 2),
(1136, 'dispositivos', '681', 'felipem', 1605846567, 1605846872, 2),
(1137, 'dispositivos', '682', 'felipem', 1605846567, 1605846872, 2),
(1138, 'dispositivos', '683', 'felipem', 1605846567, 1605846872, 2),
(1139, 'dispositivos', '684', 'felipem', 1605846567, 1605846873, 2),
(1140, 'dispositivos', '685', 'felipem', 1605846567, 1605846873, 2),
(1141, 'dispositivos', '686', 'felipem', 1605846567, 1605846873, 2),
(1142, 'dispositivos', '687', 'felipem', 1605846567, 1605846873, 2),
(1143, 'dispositivos', '688', 'felipem', 1605846567, 1605846873, 2),
(1144, 'dispositivos', '689', 'felipem', 1605846567, 1605846873, 2),
(1145, 'dispositivos', '690', 'felipem', 1605846568, 1605846873, 2),
(1146, 'dispositivos', '691', 'felipem', 1605846568, 1605846873, 2),
(1147, 'dispositivos', '692', 'felipem', 1605846568, 1605846873, 2),
(1148, 'dispositivos', '693', 'felipem', 1605846568, 1605846873, 2),
(1149, 'dispositivos', '694', NULL, 1605846568, 1605846568, NULL),
(1150, 'dispositivos', '695', 'felipem', 1605846568, 1605846568, 2),
(1151, 'dispositivos', '696', 'felipem', 1605846568, 1614717452, 2),
(1152, 'dispositivos', '697', 'felipem', 1605846568, 1605846873, 2),
(1153, 'dispositivos', '698', 'felipem', 1605846568, 1605846873, 2),
(1154, 'dispositivos', '699', 'felipem', 1605846569, 1605846873, 2),
(1155, 'dispositivos', '700', 'felipem', 1605846569, 1605846873, 2),
(1156, 'dispositivos', '701', 'felipem', 1605846569, 1605846873, 2),
(1157, 'dispositivos', '702', 'felipem', 1605846569, 1605846873, 2),
(1158, 'dispositivos', '703', 'felipem', 1605846569, 1605846873, 2),
(1159, 'dispositivos', '704', 'felipem', 1605846569, 1605846873, 2),
(1160, 'dispositivos', '705', 'felipem', 1605846569, 1605846873, 2),
(1161, 'dispositivos', '706', 'felipem', 1605846569, 1605846873, 2),
(1162, 'dispositivos', '707', 'felipem', 1605846569, 1605846873, 2),
(1163, 'dispositivos', '708', 'felipem', 1605846569, 1605846873, 2),
(1164, 'dispositivos', '709', 'felipem', 1605846570, 1605846873, 2),
(1165, 'dispositivos', '710', 'felipem', 1605846570, 1605846874, 2),
(1166, 'dispositivos', '711', 'felipem', 1605846570, 1605846874, 2),
(1167, 'dispositivos', '712', 'felipem', 1605846570, 1605846874, 2),
(1168, 'dispositivos', '713', 'felipem', 1605846570, 1605846874, 2),
(1169, 'dispositivos', '714', 'felipem', 1605846570, 1605846874, 2),
(1170, 'dispositivos', '715', 'felipem', 1605846570, 1605846874, 2),
(1171, 'dispositivos', '716', 'felipem', 1605846570, 1605846874, 2),
(1172, 'dispositivos', '717', 'felipem', 1605846571, 1605846874, 2),
(1173, 'dispositivos', '718', 'felipem', 1605846571, 1605846874, 2),
(1174, 'dispositivos', '719', 'felipem', 1605846571, 1605846874, 2),
(1175, 'dispositivos', '720', 'felipem', 1605846571, 1605846874, 2),
(1176, 'dispositivos', '721', 'felipem', 1605846571, 1605846874, 2),
(1177, 'dispositivos', '722', 'felipem', 1605846571, 1605846874, 2),
(1178, 'dispositivos', '723', 'felipem', 1605846571, 1605846874, 2),
(1179, 'dispositivos', '724', 'felipem', 1605846571, 1605846874, 2),
(1180, 'dispositivos', '725', 'felipem', 1605846571, 1605846874, 2),
(1181, 'dispositivos', '726', 'felipem', 1605846571, 1605846874, 2),
(1182, 'dispositivos', '727', 'felipem', 1605846571, 1605846874, 2),
(1183, 'dispositivos', '728', 'felipem', 1605846571, 1605846874, 2),
(1184, 'dispositivos', '729', 'felipem', 1605846571, 1605846874, 2),
(1185, 'dispositivos', '730', 'felipem', 1605846571, 1605846874, 2),
(1186, 'dispositivos', '731', NULL, 1605846571, 1605846571, NULL),
(1187, 'dispositivos', '732', 'felipem', 1605846571, 1610130539, 2),
(1188, 'dispositivos', '733', 'felipem', 1605846571, 1610131192, 2),
(1189, 'dispositivos', '734', 'felipem', 1605846571, 1605846874, 2),
(1190, 'dispositivos', '735', 'felipem', 1605846571, 1605846874, 2),
(1191, 'dispositivos', '736', 'felipem', 1605846572, 1605846874, 2),
(1192, 'dispositivos', '737', 'felipem', 1605846572, 1607092097, 2),
(1193, 'dispositivos', '738', 'felipem', 1605846572, 1607092313, 2),
(1194, 'dispositivos', '739', 'felipem', 1605846572, 1605846874, 2),
(1195, 'dispositivos', '740', 'felipem', 1605846572, 1605846875, 2),
(1196, 'dispositivos', '741', 'felipem', 1605846572, 1606946106, 2),
(1197, 'dispositivos', '742', 'felipem', 1605846572, 1605846875, 2),
(1198, 'dispositivos', '743', 'felipem', 1605846572, 1605846875, 2),
(1199, 'dispositivos', '744', 'felipem', 1605846573, 1605846875, 2),
(1200, 'dispositivos', '745', 'felipem', 1605846573, 1605846875, 2),
(1201, 'dispositivos', '746', 'felipem', 1605846573, 1605846875, 2),
(1202, 'dispositivos', '747', 'felipem', 1605846573, 1605846875, 2),
(1203, 'dispositivos', '748', 'felipem', 1605846573, 1605846875, 2),
(1204, 'dispositivos', '749', 'felipem', 1605846573, 1606748379, 2),
(1205, 'dispositivos', '750', 'felipem', 1605846573, 1605846875, 2),
(1206, 'dispositivos', '751', 'felipem', 1605846573, 1605846875, 2),
(1207, 'dispositivos', '752', 'felipem', 1605846573, 1605846875, 2),
(1208, 'dispositivos', '753', 'felipem', 1605846574, 1605846875, 2),
(1209, 'dispositivos', '754', 'felipem', 1605846574, 1605846875, 2),
(1210, 'dispositivos', '755', 'felipem', 1605846574, 1605846875, 2),
(1211, 'dispositivos', '756', 'felipem', 1605846574, 1605846875, 2),
(1212, 'dispositivos', '757', 'felipem', 1605846574, 1606860913, 2),
(1213, 'dispositivos', '758', 'felipem', 1605846574, 1605846875, 2),
(1214, 'dispositivos', '759', 'felipem', 1605846574, 1610131178, 2),
(1215, 'dispositivos', '760', 'felipem', 1605846574, 1605846875, 2),
(1216, 'dispositivos', '761', 'felipem', 1605846574, 1605846875, 2),
(1217, 'dispositivos', '762', 'felipem', 1605846574, 1605846875, 2),
(1218, 'dispositivos', '763', 'felipem', 1605846574, 1605846875, 2),
(1219, 'dispositivos', '764', 'felipem', 1605846574, 1605846875, 2),
(1220, 'dispositivos', '765', 'felipem', 1605846574, 1605846875, 2),
(1221, 'dispositivos', '766', 'felipem', 1605846574, 1605846875, 2),
(1222, 'dispositivos', '767', 'felipem', 1605846574, 1605846875, 2),
(1223, 'dispositivos', '768', 'felipem', 1605846574, 1605846875, 2),
(1224, 'dispositivos', '769', 'felipem', 1605846574, 1605846875, 2),
(1225, 'dispositivos', '770', 'felipem', 1605846574, 1605846875, 2),
(1226, 'dispositivos', '771', 'felipem', 1605846574, 1605846875, 2),
(1227, 'dispositivos', '772', 'felipem', 1605846574, 1605846875, 2),
(1228, 'dispositivos', '773', 'felipem', 1605846574, 1605846875, 2),
(1229, 'dispositivos', '774', 'felipem', 1605846574, 1605846875, 2),
(1230, 'dispositivos', '775', 'felipem', 1605846574, 1605846875, 2),
(1231, 'dispositivos', '776', 'felipem', 1605846574, 1605846875, 2),
(1232, 'dispositivos', '777', 'felipem', 1605846574, 1605846875, 2),
(1233, 'dispositivos', '778', 'felipem', 1605846574, 1605846875, 2),
(1234, 'dispositivos', '779', 'felipem', 1605846574, 1605846875, 2),
(1235, 'dispositivos', '780', 'felipem', 1605846574, 1605846875, 2),
(1236, 'dispositivos', '781', 'felipem', 1605846574, 1605846875, 2),
(1237, 'dispositivos', '782', 'felipem', 1605846574, 1605846875, 2),
(1238, 'dispositivos', '783', 'felipem', 1605846574, 1605846875, 2),
(1239, 'dispositivos', '784', 'felipem', 1605846574, 1605846875, 2),
(1240, 'dispositivos', '785', 'felipem', 1605846574, 1605846875, 2),
(1241, 'dispositivos', '786', 'felipem', 1605846574, 1605846875, 2),
(1242, 'dispositivos', '787', 'felipem', 1605846574, 1605846875, 2),
(1243, 'dispositivos', '788', 'felipem', 1605846574, 1605846875, 2),
(1244, 'dispositivos', '789', 'felipem', 1605846574, 1605846875, 2),
(1245, 'dispositivos', '790', 'felipem', 1605846574, 1605846875, 2),
(1246, 'dispositivos', '791', 'felipem', 1605846574, 1605846875, 2),
(1247, 'dispositivos', '792', 'felipem', 1605846574, 1605846875, 2),
(1248, 'dispositivos', '793', 'felipem', 1605846574, 1605846875, 2),
(1249, 'dispositivos', '794', 'felipem', 1605846574, 1605846875, 2),
(1250, 'dispositivos', '795', 'felipem', 1605846574, 1605846875, 2),
(1251, 'dispositivos', '796', 'felipem', 1605846574, 1605846876, 2),
(1252, 'dispositivos', '797', 'felipem', 1605846574, 1605846876, 2),
(1253, 'dispositivos', '798', 'felipem', 1605846574, 1605846876, 2),
(1254, 'dispositivos', '799', 'felipem', 1605846574, 1605846876, 2),
(1255, 'dispositivos', '800', 'felipem', 1605846574, 1605846876, 2),
(1256, 'dispositivos', '801', 'felipem', 1605846575, 1605846876, 2),
(1257, 'dispositivos', '802', 'felipem', 1605846575, 1605846876, 2),
(1258, 'dispositivos', '803', 'felipem', 1605846575, 1605846876, 2),
(1259, 'dispositivos', '804', 'felipem', 1605846575, 1615912568, 2),
(1260, 'dispositivos', '805', 'felipem', 1605846575, 1605846876, 2),
(1261, 'dispositivos', '806', 'felipem', 1605846575, 1605846876, 2),
(1262, 'dispositivos', '807', 'felipem', 1605846575, 1605846876, 2),
(1263, 'dispositivos', '808', 'felipem', 1605846575, 1605846876, 2),
(1264, 'dispositivos', '809', 'felipem', 1605846575, 1605846876, 2),
(1265, 'dispositivos', '810', 'felipem', 1605846575, 1605846876, 2),
(1266, 'dispositivos', '811', 'felipem', 1605846575, 1605846876, 2),
(1267, 'dispositivos', '812', 'felipem', 1605846575, 1605846876, 2),
(1268, 'dispositivos', '813', 'felipem', 1605846575, 1605846876, 2),
(1269, 'dispositivos', '814', 'felipem', 1605846575, 1605846876, 2),
(1270, 'dispositivos', '815', 'felipem', 1605846575, 1605846876, 2),
(1271, 'dispositivos', '816', 'felipem', 1605846575, 1605846876, 2),
(1272, 'dispositivos', '817', 'felipem', 1605846575, 1605846876, 2),
(1273, 'dispositivos', '818', 'felipem', 1605846575, 1605846876, 2),
(1274, 'dispositivos', '819', 'felipem', 1605846575, 1605846876, 2),
(1275, 'dispositivos', '820', 'felipem', 1605846575, 1605846876, 2),
(1276, 'dispositivos', '821', 'felipem', 1605846575, 1605846876, 2),
(1277, 'dispositivos', '822', NULL, 1605846575, 1615490397, NULL),
(1459, 'dispositivos', '1004', 'felipem', 1605850706, 1614093584, 2),
(1279, 'dispositivos', '824', 'felipem', 1605846576, 1605846876, 2),
(1280, 'dispositivos', '825', 'felipem', 1605846576, 1605846876, 2),
(1281, 'dispositivos', '826', 'felipem', 1605846576, 1605846876, 2),
(1282, 'dispositivos', '827', 'felipem', 1605846576, 1605846876, 2),
(1283, 'dispositivos', '828', 'felipem', 1605846576, 1605846876, 2),
(1284, 'dispositivos', '829', 'felipem', 1605846576, 1605846876, 2),
(1285, 'dispositivos', '830', 'felipem', 1605846576, 1605846876, 2),
(1286, 'dispositivos', '831', 'felipem', 1605846576, 1605846876, 2),
(1287, 'dispositivos', '832', 'felipem', 1605846576, 1605846876, 2),
(1288, 'dispositivos', '833', 'felipem', 1605846576, 1605846876, 2),
(1289, 'dispositivos', '834', 'felipem', 1605846576, 1605846877, 2),
(1290, 'dispositivos', '835', 'felipem', 1605846576, 1605846877, 2),
(1291, 'dispositivos', '836', 'felipem', 1605846576, 1605846877, 2),
(1292, 'dispositivos', '837', 'felipem', 1605846576, 1605846877, 2),
(1293, 'dispositivos', '838', 'felipem', 1605846576, 1605846877, 2),
(1294, 'dispositivos', '839', 'felipem', 1605846576, 1605846877, 2),
(1295, 'dispositivos', '840', 'felipem', 1605846576, 1605846877, 2),
(1296, 'dispositivos', '841', 'felipem', 1605846576, 1605846877, 2),
(1297, 'dispositivos', '842', 'felipem', 1605846576, 1605846877, 2),
(1298, 'dispositivos', '843', 'felipem', 1605846576, 1605846877, 2),
(1299, 'dispositivos', '844', 'felipem', 1605846576, 1605846877, 2),
(1300, 'dispositivos', '845', 'felipem', 1605846576, 1605846877, 2),
(1301, 'dispositivos', '846', 'felipem', 1605846576, 1605846877, 2),
(1302, 'dispositivos', '847', 'felipem', 1605846576, 1605846877, 2),
(1303, 'dispositivos', '848', 'felipem', 1605846576, 1605846877, 2),
(1304, 'dispositivos', '849', 'felipem', 1605846576, 1605846877, 2),
(1305, 'dispositivos', '850', 'felipem', 1605846576, 1605846877, 2),
(1306, 'dispositivos', '851', 'felipem', 1605846576, 1605846877, 2),
(1307, 'dispositivos', '852', 'felipem', 1605846576, 1605846877, 2),
(1308, 'dispositivos', '853', 'felipem', 1605846576, 1605846877, 2),
(1309, 'dispositivos', '854', 'felipem', 1605846576, 1605846877, 2),
(1310, 'dispositivos', '855', 'felipem', 1605846576, 1605846877, 2),
(1311, 'dispositivos', '856', 'felipem', 1605846576, 1605846877, 2),
(1312, 'dispositivos', '857', 'felipem', 1605846576, 1605846878, 2),
(1313, 'dispositivos', '858', 'felipem', 1605846577, 1605846878, 2),
(1314, 'dispositivos', '859', 'felipem', 1605846577, 1605846878, 2),
(1315, 'dispositivos', '860', 'felipem', 1605846577, 1605846878, 2),
(1316, 'dispositivos', '861', 'felipem', 1605846577, 1605846878, 2),
(1317, 'dispositivos', '862', 'felipem', 1605846577, 1605846878, 2),
(1318, 'dispositivos', '863', 'felipem', 1605846577, 1605846878, 2),
(1319, 'dispositivos', '864', 'felipem', 1605846577, 1605846878, 2),
(1320, 'dispositivos', '865', 'felipem', 1605846577, 1605846878, 2),
(1321, 'dispositivos', '866', 'felipem', 1605846577, 1605846878, 2),
(1322, 'dispositivos', '867', 'felipem', 1605846577, 1605846878, 2),
(1323, 'dispositivos', '868', 'felipem', 1605846577, 1605846878, 2),
(1324, 'dispositivos', '869', 'felipem', 1605846577, 1605846878, 2),
(1325, 'dispositivos', '870', 'felipem', 1605846577, 1605846878, 2),
(1326, 'dispositivos', '871', 'felipem', 1605846577, 1605846878, 2),
(1327, 'dispositivos', '872', 'felipem', 1605846577, 1605846878, 2),
(1328, 'dispositivos', '873', 'felipem', 1605846577, 1605846878, 2),
(1329, 'dispositivos', '874', 'felipem', 1605846577, 1605846878, 2),
(1330, 'dispositivos', '875', 'felipem', 1605846577, 1605846878, 2),
(1331, 'dispositivos', '876', 'felipem', 1605846577, 1605846878, 2),
(1332, 'dispositivos', '877', 'felipem', 1605846577, 1605846878, 2),
(1333, 'dispositivos', '878', 'felipem', 1605846577, 1605846878, 2),
(1334, 'dispositivos', '879', 'felipem', 1605846577, 1605846878, 2),
(1335, 'dispositivos', '880', 'felipem', 1605846577, 1605846878, 2),
(1336, 'dispositivos', '881', 'felipem', 1605846577, 1605846878, 2),
(1337, 'dispositivos', '882', 'felipem', 1605846577, 1605846878, 2),
(1338, 'dispositivos', '883', 'felipem', 1605846577, 1605846878, 2),
(1339, 'dispositivos', '884', 'felipem', 1605846577, 1605846878, 2),
(1340, 'dispositivos', '885', 'felipem', 1605846577, 1605846878, 2),
(1341, 'dispositivos', '886', 'felipem', 1605846577, 1605846878, 2),
(1342, 'dispositivos', '887', 'felipem', 1605846577, 1605846878, 2),
(1343, 'dispositivos', '888', 'felipem', 1605846577, 1605846878, 2),
(1344, 'dispositivos', '889', 'felipem', 1605846577, 1605846878, 2),
(1345, 'dispositivos', '890', 'felipem', 1605846577, 1605846878, 2),
(1346, 'dispositivos', '891', 'felipem', 1605846577, 1605846878, 2),
(1347, 'dispositivos', '892', 'felipem', 1605846577, 1605846878, 2),
(1348, 'dispositivos', '893', 'felipem', 1605846577, 1605846878, 2),
(1349, 'dispositivos', '894', 'felipem', 1605846577, 1605846878, 2),
(1350, 'dispositivos', '895', 'felipem', 1605846577, 1605846878, 2),
(1351, 'dispositivos', '896', 'felipem', 1605846577, 1605846878, 2),
(1352, 'dispositivos', '897', 'felipem', 1605846577, 1605846878, 2),
(1353, 'dispositivos', '898', 'felipem', 1605846577, 1605846878, 2),
(1354, 'dispositivos', '899', 'felipem', 1605846577, 1605846878, 2),
(1355, 'dispositivos', '900', 'felipem', 1605846578, 1605846878, 2),
(1356, 'dispositivos', '901', 'felipem', 1605846578, 1605846878, 2),
(1357, 'dispositivos', '902', 'felipem', 1605846578, 1605846878, 2),
(1358, 'dispositivos', '903', 'felipem', 1605846578, 1605846878, 2),
(1359, 'dispositivos', '904', NULL, 1605846578, 1605846578, NULL),
(1360, 'dispositivos', '905', 'felipem', 1605846578, 1610118660, 2),
(1361, 'dispositivos', '906', 'felipem', 1605846578, 1605846578, 2),
(1362, 'dispositivos', '907', 'felipem', 1605846578, 1605846878, 2),
(1363, 'dispositivos', '908', 'felipem', 1605846578, 1605846878, 2),
(1364, 'dispositivos', '909', 'felipem', 1605846578, 1605846878, 2),
(1365, 'dispositivos', '910', 'felipem', 1605846578, 1605846878, 2),
(1366, 'dispositivos', '911', 'felipem', 1605846578, 1605846878, 2),
(1367, 'dispositivos', '912', 'felipem', 1605846578, 1605846878, 2),
(1368, 'dispositivos', '913', 'felipem', 1605846578, 1605846879, 2),
(1369, 'dispositivos', '914', 'felipem', 1605846578, 1605846879, 2),
(1370, 'dispositivos', '915', 'felipem', 1605846578, 1605846879, 2),
(1371, 'dispositivos', '916', 'felipem', 1605846578, 1605846879, 2),
(1372, 'dispositivos', '917', 'felipem', 1605846578, 1605846879, 2),
(1373, 'dispositivos', '918', 'felipem', 1605846578, 1605846879, 2),
(1374, 'dispositivos', '919', 'felipem', 1605846578, 1605846879, 2),
(1375, 'dispositivos', '920', 'felipem', 1605846578, 1605846879, 2),
(1376, 'dispositivos', '921', 'felipem', 1605846578, 1605846879, 2),
(1377, 'dispositivos', '922', 'felipem', 1605846578, 1605846879, 2),
(1378, 'dispositivos', '923', 'felipem', 1605846578, 1611770816, 2),
(1379, 'dispositivos', '924', 'felipem', 1605846578, 1611770944, 2),
(1380, 'dispositivos', '925', 'felipem', 1605846578, 1611779634, 2),
(1381, 'dispositivos', '926', 'felipem', 1605846578, 1605846879, 2),
(1382, 'dispositivos', '927', 'felipem', 1605846578, 1605846879, 2),
(1383, 'dispositivos', '928', 'felipem', 1605846578, 1605846879, 2),
(1384, 'dispositivos', '929', 'felipem', 1605846578, 1605846879, 2),
(1385, 'dispositivos', '930', 'felipem', 1605846578, 1605846879, 2),
(1386, 'dispositivos', '931', 'felipem', 1605846578, 1605846879, 2),
(1387, 'dispositivos', '932', 'felipem', 1605846578, 1605846879, 2),
(1388, 'dispositivos', '933', 'felipem', 1605846578, 1614617304, 2),
(1389, 'dispositivos', '934', 'felipem', 1605846578, 1614094070, 2),
(1390, 'dispositivos', '935', 'felipem', 1605846578, 1614094114, 2),
(1391, 'dispositivos', '936', 'felipem', 1605846578, 1606236184, 2),
(1392, 'dispositivos', '937', 'felipem', 1605846578, 1614095361, 2),
(1393, 'dispositivos', '938', 'felipem', 1605846578, 1605846879, 2),
(1394, 'dispositivos', '939', 'felipem', 1605846579, 1605846879, 2),
(1395, 'dispositivos', '940', 'felipem', 1605846579, 1605846879, 2),
(1396, 'dispositivos', '941', 'felipem', 1605846579, 1605846879, 2),
(1397, 'dispositivos', '942', 'felipem', 1605846579, 1605846879, 2),
(1398, 'dispositivos', '943', 'felipem', 1605846579, 1611616023, 2),
(1399, 'dispositivos', '944', 'felipem', 1605846579, 1605846879, 2),
(1400, 'dispositivos', '945', 'felipem', 1605846579, 1605846879, 2),
(1401, 'dispositivos', '946', 'felipem', 1605846579, 1605846879, 2),
(1402, 'dispositivos', '947', 'felipem', 1605846579, 1605846879, 2),
(1403, 'dispositivos', '948', 'felipem', 1605846579, 1605846879, 2),
(1404, 'dispositivos', '949', 'felipem', 1605846579, 1605846879, 2),
(1405, 'dispositivos', '950', 'felipem', 1605846579, 1605846879, 2),
(1406, 'dispositivos', '951', 'felipem', 1605846579, 1605846879, 2),
(1407, 'dispositivos', '952', 'felipem', 1605846579, 1605846879, 2),
(1408, 'dispositivos', '953', NULL, 1605846579, 1605846579, NULL),
(1409, 'dispositivos', '954', 'felipem', 1605846579, 1614091796, 2),
(1410, 'dispositivos', '955', 'felipem', 1605846579, 1614091791, 2),
(1411, 'dispositivos', '956', 'felipem', 1605846579, 1614091799, 2),
(1412, 'dispositivos', '957', 'felipem', 1605846579, 1614090114, 2),
(1413, 'dispositivos', '958', 'felipem', 1605846579, 1614090107, 2),
(1414, 'dispositivos', '959', 'felipem', 1605846579, 1614090100, 2),
(1415, 'dispositivos', '960', 'felipem', 1605846579, 1614090095, 2),
(1416, 'dispositivos', '961', 'felipem', 1605846579, 1605846879, 2),
(1417, 'dispositivos', '962', 'felipem', 1605846579, 1605846879, 2),
(1418, 'dispositivos', '963', 'felipem', 1605846579, 1605846879, 2),
(1419, 'dispositivos', '964', 'felipem', 1605846579, 1605846879, 2),
(1420, 'dispositivos', '965', 'felipem', 1605846579, 1605846879, 2),
(1421, 'dispositivos', '966', 'felipem', 1605846579, 1605846879, 2),
(1422, 'dispositivos', '967', 'felipem', 1605846579, 1605846880, 2),
(1423, 'dispositivos', '968', 'felipem', 1605846579, 1605846880, 2),
(1424, 'dispositivos', '969', 'felipem', 1605846580, 1605846880, 2),
(1425, 'dispositivos', '970', 'felipem', 1605846580, 1605846880, 2),
(1426, 'dispositivos', '971', 'felipem', 1605846580, 1605846880, 2),
(1427, 'dispositivos', '972', 'felipem', 1605846580, 1605846880, 2),
(1428, 'dispositivos', '973', 'felipem', 1605846580, 1605846880, 2),
(1429, 'dispositivos', '974', 'felipem', 1605846580, 1605846880, 2),
(1430, 'dispositivos', '975', 'felipem', 1605846580, 1605846880, 2),
(1431, 'dispositivos', '976', 'felipem', 1605846580, 1605846880, 2),
(1432, 'dispositivos', '977', 'felipem', 1605846580, 1605846880, 2),
(1433, 'dispositivos', '978', 'felipem', 1605846580, 1605846880, 2),
(1434, 'dispositivos', '979', 'felipem', 1605846580, 1605846880, 2),
(1435, 'dispositivos', '980', 'felipem', 1605846580, 1605846880, 2),
(1436, 'dispositivos', '981', 'felipem', 1605846580, 1605846880, 2),
(1437, 'dispositivos', '982', 'felipem', 1605846580, 1605846880, 2),
(1438, 'dispositivos', '983', 'felipem', 1605846580, 1605846880, 2),
(1439, 'dispositivos', '984', 'felipem', 1605846580, 1605846880, 2),
(1440, 'dispositivos', '985', 'felipem', 1605846580, 1605846880, 2),
(1441, 'dispositivos', '986', 'felipem', 1605846580, 1605846880, 2),
(1442, 'dispositivos', '987', 'felipem', 1605846580, 1605846880, 2),
(1443, 'dispositivos', '988', 'felipem', 1605846580, 1605846880, 2),
(1444, 'dispositivos', '989', 'felipem', 1605846580, 1605846880, 2),
(1445, 'dispositivos', '990', 'felipem', 1605846580, 1605846880, 2),
(1446, 'dispositivos', '991', 'felipem', 1605846580, 1605846880, 2),
(1447, 'dispositivos', '992', 'felipem', 1605846580, 1605846880, 2),
(1448, 'dispositivos', '993', 'felipem', 1605846580, 1605846880, 2),
(1449, 'dispositivos', '994', 'felipem', 1605846580, 1605846880, 2),
(1450, 'dispositivos', '995', 'felipem', 1605846580, 1605846880, 2),
(1451, 'dispositivos', '996', 'felipem', 1605846580, 1605846880, 2),
(1452, 'dispositivos', '997', 'felipem', 1605846580, 1605846880, 2),
(1453, 'dispositivos', '998', 'felipem', 1605846580, 1605846880, 2),
(1454, 'dispositivos', '999', 'felipem', 1605846580, 1605846880, 2),
(1455, 'dispositivos', '1000', 'felipem', 1605846580, 1605846881, 2),
(1456, 'dispositivos', '1001', 'felipem', 1605846580, 1605846881, 2),
(1457, 'dispositivos', '1002', 'felipem', 1605846580, 1605846881, 2),
(1458, 'dispositivos', '1003', NULL, 1605846580, 1605846580, NULL),
(1466, 'verificacion', '81', 'felipem', 1605880207, 1605880207, 2),
(1467, 'movimientos', '150', 'felipem', 1605880267, 1605880267, 2),
(1468, 'movi_envio', '6', 'felipem', 1605880332, 1605880332, 2),
(1469, 'movi_recepcion', '6', 'felipem', 1605880425, 1605880425, 2),
(1470, 'verificacion', '82', 'felipem', 1605882109, 1605882109, 2),
(1471, 'verificacion', '83', 'felipem', 1605885901, 1605885901, 2),
(1472, 'verificacion', '84', 'a.lopez', 1605886901, 1605886901, 3),
(1473, 'marca_modelo', '48', 'felipem', 1605899934, 1605899934, 2),
(1474, 'contactos', '9', 'felipem', 1605900369, 1605900461, 2),
(1475, 'tipo_estado_observaciones', '1', 'felipem', 1605913770, 1605913853, 2),
(1476, 'tipo_estado_observaciones', '2', 'felipem', 1605913820, 1605913820, 2),
(1477, 'tipo_estado_observaciones', '3', 'felipem', 1605913876, 1605914121, 2),
(1478, 'observaciones', '1', 'a.lopez', 1605914151, 1605914151, 3),
(1479, 'verificacion', '85', 'a.lopez', 1605914293, 1605914293, 3),
(1480, 'verificacion', '86', 'a.lopez', 1606137566, 1606137566, 3),
(1481, 'verificacion', '87', 'a.lopez', 1606137825, 1606137825, 3),
(1482, 'observaciones', '2', 'a.lopez', 1606137899, 1606137899, 3),
(1483, 'movimientos', '151', 'a.lopez', 1606138550, 1606138550, 3),
(1484, 'movi_envio', '7', 'a.lopez', 1606138698, 1606138698, 3),
(1485, 'movi_recepcion', '7', 'a.lopez', 1606138789, 1606138789, 3),
(1486, 'verificacion', '88', 'a.lopez', 1606139072, 1606139072, 3),
(1487, 'verificacion', '89', 'a.lopez', 1606141300, 1606141300, 3),
(1488, 'observaciones', '3', 'a.lopez', 1606141478, 1606141478, 3),
(1489, 'verificacion', '90', 'a.lopez', 1606141538, 1606141538, 3),
(1490, 'verificacion', '91', 'a.lopez', 1606142066, 1606142066, 3),
(1491, 'verificacion', '92', 'a.lopez', 1606142377, 1606142377, 3),
(1492, 'verificacion', '93', 'a.lopez', 1606142956, 1606142956, 3),
(1538, 'observaciones', '8', 'a.lopez', 1606320396, 1606320396, 3),
(1494, 'movimientos', '152', 'a.lopez', 1606143403, 1606143727, 3),
(1495, 'movi_envio', '8', 'a.lopez', 1606143521, 1606143691, 3),
(1496, 'movi_recepcion', '8', 'a.lopez', 1606143643, 1606143643, 3),
(1497, 'verificacion', '94', 'a.lopez', 1606144232, 1606144232, 3),
(1498, 'movimientos', '153', 'a.lopez', 1606146335, 1606146663, 3),
(1499, 'movi_envio', '9', 'a.lopez', 1606146396, 1606146396, 3),
(1500, 'movi_recepcion', '9', 'a.lopez', 1606146635, 1606146635, 3),
(1501, 'verificacion', '95', 'a.lopez', 1606146743, 1606146743, 3),
(1502, 'marca_modelo', '49', 'felipem', 1606173797, 1606173797, 2),
(1510, 'verificacion', '98', 'a.lopez', 1606227539, 1606227539, 3),
(1508, 'verificacion', '96', 'a.lopez', 1606225073, 1606225073, 3),
(1506, 'tipo_dispositivo', '29', 'felipem', 1606175138, 1606175343, 2),
(1507, 'tipo_dispositivo', '30', 'felipem', 1606175433, 1606175433, 2),
(1511, 'observaciones', '5', 'a.lopez', 1606227666, 1606227666, 3),
(1512, 'verificacion', '99', 'a.lopez', 1606228036, 1606228036, 3),
(1539, 'marcas', '28', 'a.lopez', 1606320740, 1606320740, 3),
(1514, 'verificacion', '101', 'a.lopez', 1606229627, 1606229627, 3),
(1531, 'verificacion', '111', 'a.lopez', 1606313094, 1606313094, 3),
(1516, 'verificacion', '103', 'a.lopez', 1606232326, 1606232326, 3),
(1517, 'verificacion', '104', 'a.lopez', 1606232770, 1606232770, 3),
(1518, 'verificacion', '105', 'a.lopez', 1606233379, 1606233379, 3),
(1519, 'verificacion', '106', 'a.lopez', 1606234164, 1606234164, 3),
(1520, 'observaciones', '6', 'a.lopez', 1606234331, 1606234331, 3),
(1521, 'marcas', '27', 'a.lopez', 1606235010, 1606235010, 3),
(1522, 'marca_modelo', '50', 'a.lopez', 1606235650, 1606235660, 3),
(1523, 'verificacion', '107', 'a.lopez', 1606236170, 1606236170, 3),
(1524, 'marca_modelo', '51', 'a.lopez', 1606238031, 1606238036, 3),
(1525, 'verificacion', '108', 'a.lopez', 1606238697, 1606238697, 3),
(1526, 'movimientos', '154', 'a.lopez', 1606239111, 1606239480, 3),
(1527, 'movi_envio', '10', 'a.lopez', 1606239430, 1606239430, 3),
(1528, 'movi_recepcion', '10', 'a.lopez', 1606239468, 1606239468, 3),
(1529, 'verificacion', '109', 'a.lopez', 1606239555, 1606239555, 3),
(1530, 'verificacion', '110', 'a.lopez', 1606239777, 1606239777, 3),
(1532, 'verificacion', '112', 'a.lopez', 1606316370, 1606316370, 3),
(1533, 'verificacion', '113', 'a.lopez', 1606316770, 1606316770, 3),
(1534, 'verificacion', '114', 'a.lopez', 1606317011, 1606317011, 3);
INSERT INTO `membership_userrecords` (`recID`, `tableName`, `pkValue`, `memberID`, `dateAdded`, `dateUpdated`, `groupID`) VALUES
(1535, 'observaciones', '7', 'a.lopez', 1606317376, 1606317376, 3),
(1536, 'verificacion', '115', 'a.lopez', 1606318710, 1606318710, 3),
(1537, 'verificacion', '116', 'a.lopez', 1606320011, 1606320011, 3),
(1540, 'observaciones', '9', 'a.lopez', 1606321330, 1606321330, 3),
(1541, 'verificacion', '117', 'a.lopez', 1606321459, 1606321459, 3),
(1542, 'verificacion', '118', 'a.lopez', 1606321567, 1606321567, 3),
(1543, 'verificacion', '119', 'a.lopez', 1606321867, 1606321867, 3),
(1544, 'observaciones', '10', 'a.lopez', 1606322344, 1606322491, 3),
(1545, 'verificacion', '120', 'a.lopez', 1606322578, 1606322578, 3),
(1546, 'verificacion', '121', 'a.lopez', 1606322964, 1606322964, 3),
(1547, 'verificacion', '122', 'a.lopez', 1606323079, 1606323079, 3),
(1548, 'verificacion', '123', 'a.lopez', 1606323221, 1606323221, 3),
(1549, 'verificacion', '124', 'a.lopez', 1606323363, 1606323363, 3),
(1550, 'verificacion', '125', 'a.lopez', 1606323438, 1606323438, 3),
(1551, 'verificacion', '126', 'a.lopez', 1606323615, 1606323615, 3),
(1552, 'verificacion', '127', 'a.lopez', 1606323757, 1606323757, 3),
(1553, 'verificacion', '128', 'a.lopez', 1606323944, 1606323944, 3),
(1554, 'marcas', '29', 'a.lopez', 1606324682, 1606324682, 3),
(1555, 'verificacion', '129', 'a.lopez', 1606325203, 1606325203, 3),
(1556, 'observaciones', '11', 'a.lopez', 1606325406, 1606325406, 3),
(1557, 'verificacion', '130', 'a.lopez', 1606406255, 1606406255, 3),
(1572, 'verificacion', '141', 'a.lopez', 1606414410, 1606414410, 3),
(1559, 'verificacion', '132', 'a.lopez', 1606407367, 1606407367, 3),
(1560, 'observaciones', '12', 'a.lopez', 1606407951, 1606407951, 3),
(1561, 'verificacion', '133', 'a.lopez', 1606408665, 1606408665, 3),
(1562, 'verificacion', '134', 'a.lopez', 1606409502, 1606409502, 3),
(1563, 'movimientos', '155', 'a.lopez', 1606409730, 1606409730, 3),
(1564, 'movi_envio', '11', 'a.lopez', 1606410247, 1606410247, 3),
(1565, 'movi_recepcion', '11', 'a.lopez', 1606410308, 1606410308, 3),
(1566, 'verificacion', '135', 'a.lopez', 1606411024, 1606411024, 3),
(1567, 'verificacion', '136', 'a.lopez', 1606411974, 1606411974, 3),
(1568, 'verificacion', '137', 'a.lopez', 1606412030, 1606412030, 3),
(1569, 'verificacion', '138', 'a.lopez', 1606412197, 1606412197, 3),
(1570, 'verificacion', '139', 'a.lopez', 1606412932, 1606412932, 3),
(1571, 'verificacion', '140', 'a.lopez', 1606413415, 1606413415, 3),
(1575, 'verificacion', '143', 'a.lopez', 1606577020, 1606577020, 3),
(1576, 'verificacion', '144', 'a.lopez', 1606577174, 1606577174, 3),
(1577, 'verificacion', '145', 'a.lopez', 1606577255, 1606577255, 3),
(1578, 'observaciones', '13', 'a.lopez', 1606577442, 1606577442, 3),
(1579, 'verificacion', '146', 'a.lopez', 1606577804, 1606577804, 3),
(1580, 'verificacion', '147', 'a.lopez', 1606578013, 1606578013, 3),
(1581, 'observaciones', '14', 'a.lopez', 1606578388, 1606578388, 3),
(1582, 'verificacion', '148', 'a.lopez', 1606578487, 1606578487, 3),
(1583, 'verificacion', '149', 'a.lopez', 1606578702, 1606578702, 3),
(1584, 'verificacion', '150', 'a.lopez', 1606579061, 1606579061, 3),
(1585, 'marcas', '30', 'a.lopez', 1606579428, 1606579428, 3),
(1586, 'verificacion', '151', 'a.lopez', 1606579723, 1606860746, 3),
(1587, 'marcas', '31', 'a.lopez', 1606580146, 1606580146, 3),
(1588, 'verificacion', '152', 'a.lopez', 1606580535, 1606580535, 3),
(1589, 'verificacion', '153', 'a.lopez', 1606580725, 1606580725, 3),
(1590, 'verificacion', '154', 'a.lopez', 1606580926, 1606580926, 3),
(1591, 'verificacion', '155', 'a.lopez', 1606582076, 1606582076, 3),
(1592, 'verificacion', '156', 'a.lopez', 1606582236, 1606582236, 3),
(1593, 'verificacion', '157', 'a.lopez', 1606582342, 1606582342, 3),
(1594, 'verificacion', '158', 'a.lopez', 1606582435, 1606582435, 3),
(1595, 'verificacion', '159', 'a.lopez', 1606582738, 1606582738, 3),
(1596, 'verificacion', '160', 'a.lopez', 1606582952, 1606582952, 3),
(1597, 'marca_modelo', '52', 'felipem', 1606584596, 1606584596, 2),
(1598, 'verificacion', '161', 'a.lopez', 1606584668, 1606584668, 3),
(1599, 'verificacion', '162', 'felipem', 1606584745, 1606584745, 2),
(1600, 'verificacion', '163', 'a.lopez', 1606584968, 1606584968, 3),
(1601, 'marca_modelo', '53', 'felipem', 1606585366, 1606585366, 2),
(1602, 'marca_modelo', '54', 'felipem', 1606585461, 1606585461, 2),
(1603, 'marcas', '32', 'felipem', 1606585584, 1606585584, 2),
(1604, 'marca_modelo', '55', 'felipem', 1606585626, 1606585626, 2),
(1605, 'marcas', '33', 'felipem', 1606585737, 1606585737, 2),
(1606, 'marca_modelo', '56', 'felipem', 1606585771, 1606585771, 2),
(1607, 'verificacion', '164', 'a.lopez', 1606586386, 1606586386, 3),
(1608, 'verificacion', '165', 'felipem', 1606586491, 1606586491, 2),
(1609, 'verificacion', '166', 'a.lopez', 1606586744, 1606586744, 3),
(1610, 'marca_modelo', '57', 'felipem', 1606586935, 1606586935, 2),
(1611, 'verificacion', '167', 'felipem', 1606586977, 1606586977, 2),
(1612, 'verificacion', '168', 'a.lopez', 1606587207, 1606587207, 3),
(1613, 'verificacion', '169', 'a.lopez', 1606588508, 1606588508, 3),
(1614, 'verificacion', '170', 'a.lopez', 1606588644, 1606588644, 3),
(1615, 'verificacion', '171', 'a.lopez', 1606588794, 1606588794, 3),
(1818, 'movi_envio', '33', 'c.zambrano', 1611589299, 1611589299, 3),
(1617, 'verificacion', '173', 'a.lopez', 1606745479, 1606745479, 3),
(1618, 'verificacion', '174', 'a.lopez', 1606745596, 1606745596, 3),
(1619, 'verificacion', '175', 'a.lopez', 1606745953, 1606745953, 3),
(1620, 'verificacion', '176', 'a.lopez', 1606746258, 1606746258, 3),
(1621, 'verificacion', '177', 'a.lopez', 1606746580, 1606746580, 3),
(1622, 'verificacion', '178', 'a.lopez', 1606746694, 1606746694, 3),
(1623, 'verificacion', '179', 'a.lopez', 1606746903, 1606746903, 3),
(1624, 'verificacion', '180', 'a.lopez', 1606747422, 1606747422, 3),
(1625, 'verificacion', '181', 'a.lopez', 1606747530, 1606747530, 3),
(1626, 'verificacion', '182', 'a.lopez', 1606747735, 1606747735, 3),
(1627, 'verificacion', '183', 'a.lopez', 1606748004, 1606748004, 3),
(1628, 'verificacion', '184', 'a.lopez', 1606748084, 1606748084, 3),
(1629, 'verificacion', '185', 'a.lopez', 1606748347, 1606748347, 3),
(1630, 'verificacion', '186', 'a.lopez', 1606748880, 1606748880, 3),
(1631, 'verificacion', '187', 'a.lopez', 1606749110, 1606749110, 3),
(1632, 'verificacion', '188', 'a.lopez', 1606749463, 1606749463, 3),
(1633, 'verificacion', '189', 'a.lopez', 1606749691, 1606749691, 3),
(1634, 'verificacion', '190', 'a.lopez', 1606749820, 1606749820, 3),
(1635, 'verificacion', '191', 'a.lopez', 1606750331, 1606750331, 3),
(1636, 'verificacion', '192', 'a.lopez', 1606750551, 1606750551, 3),
(1637, 'verificacion', '193', 'a.lopez', 1606750820, 1606750820, 3),
(1638, 'verificacion', '194', 'a.lopez', 1606751166, 1606751166, 3),
(1639, 'verificacion', '195', 'a.lopez', 1606751491, 1606751491, 3),
(1640, 'verificacion', '196', 'a.lopez', 1606751894, 1606751894, 3),
(1641, 'verificacion', '197', 'a.lopez', 1606763519, 1606763519, 3),
(1642, 'marca_modelo', '58', 'a.lopez', 1606835540, 1606835948, 3),
(1643, 'verificacion', '198', 'a.lopez', 1606836540, 1606836540, 3),
(1644, 'verificacion', '199', 'a.lopez', 1606857899, 1606857899, 3),
(1645, 'verificacion', '200', 'a.lopez', 1606858110, 1606858110, 3),
(1646, 'verificacion', '201', 'a.lopez', 1606858376, 1606858376, 3),
(1647, 'verificacion', '202', 'a.lopez', 1606858485, 1606858485, 3),
(1648, 'verificacion', '203', 'a.lopez', 1606858919, 1606858919, 3),
(1649, 'observaciones', '15', 'a.lopez', 1606859003, 1606859003, 3),
(1650, 'verificacion', '204', 'a.lopez', 1606860496, 1606860496, 3),
(1651, 'verificacion', '205', 'a.lopez', 1606860540, 1606860540, 3),
(1652, 'verificacion', '206', 'a.lopez', 1606860634, 1606860634, 3),
(1653, 'verificacion', '207', 'a.lopez', 1606860768, 1606860768, 3),
(1654, 'verificacion', '208', 'a.lopez', 1606860936, 1606860936, 3),
(1655, 'financiero', '1', 'felipem', 1606862857, 1606862857, 2),
(1656, 'marcas', '34', 'felipem', 1606916008, 1606916008, 2),
(1657, 'marca_modelo', '59', 'felipem', 1606916943, 1606916943, 2),
(1658, 'observaciones', '16', 'felipem', 1606917059, 1606917059, 2),
(1659, 'verificacion', '209', 'a.lopez', 1606946174, 1606946174, 3),
(1660, 'movimientos', '156', 'felipem', 1606947006, 1606947006, 2),
(1661, 'movi_envio', '12', 'felipem', 1606947096, 1606947096, 2),
(1662, 'ciudades', '8', 'felipem', 1606947287, 1606947287, 2),
(1663, 'contactos', '10', 'felipem', 1606947344, 1606947344, 2),
(1664, 'movi_recepcion', '12', 'felipem', 1606947365, 1606947365, 2),
(1665, 'verificacion', '210', 'a.lopez', 1606947733, 1606947733, 3),
(1666, 'verificacion', '211', 'a.lopez', 1606952129, 1606952129, 3),
(1667, 'observaciones', '17', 'a.lopez', 1606952191, 1606952191, 3),
(1668, 'verificacion', '212', 'a.lopez', 1607025220, 1607025220, 3),
(1669, 'verificacion', '213', 'a.lopez', 1607025385, 1607025426, 3),
(1670, 'verificacion', '214', 'a.lopez', 1607026551, 1607026551, 3),
(1671, 'verificacion', '215', 'a.lopez', 1607026813, 1607026813, 3),
(1672, 'verificacion', '216', 'a.lopez', 1607026920, 1607026920, 3),
(1673, 'movimientos', '157', 'a.lopez', 1607026939, 1607027135, 3),
(1674, 'movi_envio', '13', 'a.lopez', 1607027017, 1607027017, 3),
(1675, 'movi_recepcion', '13', 'a.lopez', 1607027106, 1607027106, 3),
(1676, 'movimientos', '158', 'a.lopez', 1607027193, 1607027475, 3),
(1677, 'movi_envio', '14', 'a.lopez', 1607027318, 1607027318, 3),
(1678, 'movi_recepcion', '14', 'a.lopez', 1607027461, 1607027461, 3),
(1679, 'verificacion', '217', 'a.lopez', 1607028973, 1607028973, 3),
(1680, 'movimientos', '159', 'a.lopez', 1607028984, 1607029110, 3),
(1681, 'movi_envio', '15', 'a.lopez', 1607029029, 1607029029, 3),
(1682, 'movi_recepcion', '15', 'a.lopez', 1607029102, 1607029102, 3),
(1683, 'observaciones', '18', 'a.lopez', 1607029158, 1607029158, 3),
(1684, 'verificacion', '218', 'a.lopez', 1607029276, 1607029276, 3),
(1685, 'movimientos', '160', 'a.lopez', 1607029297, 1607029297, 3),
(1686, 'movi_envio', '16', 'a.lopez', 1607029337, 1607029337, 3),
(1687, 'movi_recepcion', '16', 'a.lopez', 1607029384, 1607029388, 3),
(1688, 'movimientos', '161', 'a.lopez', 1607029653, 1607029750, 3),
(1689, 'movi_envio', '17', 'a.lopez', 1607029688, 1607029688, 3),
(1690, 'movi_recepcion', '17', 'a.lopez', 1607029740, 1607029740, 3),
(1691, 'verificacion', '219', 'a.lopez', 1607029771, 1607029771, 3),
(1692, 'movimientos', '162', 'a.lopez', 1607029927, 1607030085, 3),
(1693, 'movi_envio', '18', 'a.lopez', 1607029966, 1607029966, 3),
(1694, 'movi_recepcion', '18', 'a.lopez', 1607030027, 1607030027, 3),
(1695, 'movimientos', '163', 'a.lopez', 1607030369, 1607030369, 3),
(1696, 'movi_envio', '19', 'a.lopez', 1607030404, 1607030404, 3),
(1697, 'verificacion', '220', 'a.lopez', 1607091562, 1607091562, 3),
(1698, 'observaciones', '19', 'a.lopez', 1607091815, 1607091815, 3),
(1699, 'verificacion', '221', 'a.lopez', 1607092051, 1607092051, 3),
(1700, 'observaciones', '20', 'a.lopez', 1607092230, 1607092230, 3),
(1701, 'verificacion', '222', 'a.lopez', 1607092290, 1607092290, 3),
(1702, 'movimientos', '164', 'a.lopez', 1607092547, 1607092547, 3),
(1703, 'movi_envio', '20', 'a.lopez', 1607092691, 1607092691, 3),
(1704, 'movi_recepcion', '19', 'a.lopez', 1607092838, 1607092838, 3),
(1705, 'verificacion', '223', 'a.lopez', 1607092914, 1607092914, 3),
(1706, 'movi_envio', '21', 'l.garcia', 1607121219, 1607121219, 3),
(1707, 'movi_recepcion', '20', 'l.garcia', 1607121364, 1607121364, 3),
(1708, 'verificacion', '224', 'a.lopez', 1607372806, 1607372806, 3),
(1709, 'verificacion', '225', 'a.lopez', 1607373474, 1607373474, 3),
(1710, 'verificacion', '226', 'a.lopez', 1607373832, 1607373832, 3),
(1711, 'verificacion', '227', 'a.lopez', 1607374011, 1607374011, 3),
(1712, 'verificacion', '228', 'a.lopez', 1607374165, 1607374165, 3),
(1713, 'verificacion', '229', 'a.lopez', 1607374212, 1607374212, 3),
(1714, 'verificacion', '230', 'a.lopez', 1607374370, 1607374370, 3),
(1715, 'verificacion', '231', 'a.lopez', 1607374421, 1607374421, 3),
(1716, 'verificacion', '232', 'a.lopez', 1607374687, 1607374687, 3),
(1717, 'verificacion', '233', 'a.lopez', 1607374897, 1607374897, 3),
(1718, 'verificacion', '234', 'a.lopez', 1607375030, 1607375030, 3),
(1719, 'verificacion', '235', 'a.lopez', 1607375157, 1607375157, 3),
(1720, 'unidades', '17', 'felipem', 1607530654, 1607530653, 2),
(1721, 'verificacion', '236', 'a.lopez', 1607606261, 1607606261, 3),
(1722, 'verificacion', '237', 'a.lopez', 1607606494, 1607606494, 3),
(1723, 'verificacion', '238', 'a.lopez', 1607606612, 1607606612, 3),
(1724, 'verificacion', '239', 'a.lopez', 1607606685, 1607606685, 3),
(1725, 'verificacion', '240', 'a.lopez', 1607606907, 1607606907, 3),
(1726, 'verificacion', '241', 'a.lopez', 1607606973, 1607606973, 3),
(1727, 'verificacion', '242', 'a.lopez', 1607607230, 1607607230, 3),
(1728, 'verificacion', '243', 'a.lopez', 1607607847, 1607607847, 3),
(1729, 'verificacion', '244', 'a.lopez', 1607608245, 1607608245, 3),
(1730, 'verificacion', '245', 'a.lopez', 1607609797, 1607609797, 3),
(1731, 'verificacion', '246', 'a.lopez', 1607609966, 1607609966, 3),
(1732, 'verificacion', '247', 'a.lopez', 1607610023, 1607610023, 3),
(1733, 'acceso_remoto', '1', 'a.lopez', 1607616395, 1607723628, 3),
(1734, 'acceso_remoto', '2', 'a.lopez', 1607616552, 1607616552, 3),
(1735, 'tipo_estado_observaciones', '4', 'felipem', 1607619067, 1607619100, 2),
(1736, 'observaciones', '21', 'felipem', 1607619139, 1607619139, 2),
(1737, 'verificacion', '248', 'felipem', 1607619499, 1607619499, 2),
(1738, 'acceso_remoto', '3', 'a.lopez', 1607620253, 1607620253, 3),
(1739, 'acceso_remoto', '4', 'a.lopez', 1607620381, 1607972989, 3),
(1740, 'acceso_remoto', '5', 'a.lopez', 1607620807, 1607973078, 3),
(1741, 'acceso_remoto', '6', 'a.lopez', 1607631995, 1607631995, 3),
(1742, 'acceso_remoto', '7', 'a.lopez', 1607632977, 1607632977, 3),
(1743, 'verificacion', '249', 'felipem', 1607644314, 1607644314, 2),
(1744, 'acceso_remoto', '8', 'a.lopez', 1607723388, 1607723394, 3),
(1745, 'marca_modelo', '60', 'a.lopez', 1608070356, 1608070361, 3),
(1746, 'verificacion', '250', 'a.lopez', 1608070521, 1608070521, 3),
(1747, 'movimientos', '165', 'felipem', 1608745207, 1608745207, 2),
(1748, 'movi_envio', '22', 'felipem', 1608745264, 1608745264, 2),
(1749, 'movi_recepcion', '21', 'felipem', 1608745413, 1608745413, 2),
(1750, 'verificacion', '251', 'felipem', 1608746046, 1608746046, 2),
(1751, 'verificacion', '252', 'felipem', 1608746147, 1608746147, 2),
(1752, 'movimientos', '166', 'a.lopez', 1609270915, 1609270915, 3),
(1753, 'movi_envio', '23', 'a.lopez', 1609271027, 1609271027, 3),
(1754, 'movi_recepcion', '22', 'a.lopez', 1609271109, 1609271109, 3),
(1755, 'verificacion', '253', 'a.lopez', 1609271176, 1609271176, 3),
(1756, 'acceso_remoto', '9', 'felipem', 1609790676, 1609790942, 2),
(1757, 'acceso_remoto', '10', 'felipem', 1609797115, 1609797351, 2),
(1758, 'tipo_dispositivo', '31', 'felipem', 1609938115, 1609938115, 2),
(1759, 'marcas', '35', 'felipem', 1610034990, 1610034990, 2),
(1760, 'marca_modelo', '61', 'felipem', 1610035021, 1610035021, 2),
(1761, 'verificacion', '254', 'a.lopez', 1610118325, 1610118325, 3),
(1762, 'verificacion', '255', 'a.lopez', 1610118528, 1610118528, 3),
(1763, 'verificacion', '256', 'a.lopez', 1610119200, 1610119200, 3),
(1764, 'verificacion', '257', 'a.lopez', 1610119429, 1610119429, 3),
(1765, 'verificacion', '258', 'a.lopez', 1610119565, 1610119565, 3),
(1766, 'verificacion', '259', 'a.lopez', 1610119864, 1610119864, 3),
(1767, 'verificacion', '260', 'a.lopez', 1610120054, 1610120054, 3),
(1768, 'verificacion', '261', 'a.lopez', 1610120178, 1610120178, 3),
(1769, 'verificacion', '262', 'a.lopez', 1610120275, 1610120275, 3),
(1770, 'marca_modelo', '62', 'a.lopez', 1610121377, 1610121377, 3),
(1771, 'verificacion', '263', 'a.lopez', 1610121657, 1610121657, 3),
(1772, 'verificacion', '264', 'a.lopez', 1610124715, 1610124715, 3),
(1774, 'verificacion', '266', 'a.lopez', 1610125120, 1610125120, 3),
(1775, 'verificacion', '267', 'a.lopez', 1610127436, 1610127436, 3),
(1776, 'verificacion', '268', 'a.lopez', 1610129010, 1610129010, 3),
(1777, 'verificacion', '269', 'a.lopez', 1610129412, 1610129431, 3),
(1778, 'verificacion', '270', 'a.lopez', 1610129975, 1610129975, 3),
(1779, 'marca_modelo', '63', 'a.lopez', 1610130399, 1610130399, 3),
(1780, 'verificacion', '271', 'a.lopez', 1610130474, 1610130474, 3),
(1781, 'verificacion', '272', 'a.lopez', 1610130710, 1610130710, 3),
(1782, 'marca_modelo', '64', 'a.lopez', 1610131016, 1610131016, 3),
(1783, 'verificacion', '273', 'a.lopez', 1610131113, 1610131113, 3),
(1784, 'movimientos', '167', 'c.zambrano', 1610458327, 1610459729, 3),
(1785, 'movimientos', '168', 'c.zambrano', 1610458390, 1610458390, 3),
(1786, 'movi_envio', '24', 'c.zambrano', 1610458765, 1610458765, 3),
(1787, 'movi_envio', '25', 'c.zambrano', 1610459276, 1610459276, 3),
(1788, 'movi_recepcion', '23', 'c.zambrano', 1610459339, 1610459339, 3),
(1789, 'movi_envio', '26', 'c.zambrano', 1610459459, 1610459459, 3),
(1790, 'movi_recepcion', '24', 'c.zambrano', 1610459666, 1610459666, 3),
(1791, 'movimientos', '169', 'c.zambrano', 1610459913, 1610460129, 3),
(1792, 'movi_envio', '27', 'c.zambrano', 1610460090, 1610460090, 3),
(1793, 'movimientos', '170', 'c.zambrano', 1610460168, 1610460758, 3),
(1794, 'movi_recepcion', '25', 'c.zambrano', 1610460271, 1610460271, 3),
(1795, 'movi_envio', '28', 'c.zambrano', 1610460361, 1610460361, 3),
(1796, 'movi_envio', '29', 'c.zambrano', 1610460566, 1610460566, 3),
(1797, 'movi_recepcion', '26', 'c.zambrano', 1610460746, 1610460746, 3),
(1798, 'movi_recepcion', '27', 'felipem', 1610474169, 1610474169, 2),
(1799, 'verificacion', '274', 'felipem', 1610474220, 1610474220, 2),
(1800, 'movi_recepcion', '28', 'felipem', 1610474392, 1610474392, 2),
(1801, 'verificacion', '275', 'felipem', 1610474478, 1610474478, 2),
(1802, 'acceso_remoto', '11', 'felipem', 1610575106, 1610575154, 2),
(1803, 'perfil', '1', 'felipem', 1611073889, 1611073889, 2),
(1804, 'verificacion', '276', 'a.lopez', 1611353176, 1611353176, 3),
(1805, 'movimientos', '171', 'c.zambrano', 1611584852, 1611585349, 3),
(1806, 'movi_envio', '30', 'c.zambrano', 1611585191, 1611585191, 3),
(1807, 'movi_recepcion', '29', 'c.zambrano', 1611585322, 1611585322, 3),
(1808, 'movimientos', '172', 'c.zambrano', 1611585501, 1611585764, 3),
(1809, 'movi_envio', '31', 'c.zambrano', 1611585693, 1611585693, 3),
(1810, 'movi_recepcion', '30', 'c.zambrano', 1611585753, 1611585753, 3),
(1811, 'verificacion', '277', 'c.zambrano', 1611585999, 1611585999, 3),
(1812, 'verificacion', '278', 'c.zambrano', 1611586140, 1611586140, 3),
(1813, 'movimientos', '173', 'c.zambrano', 1611587280, 1611587498, 3),
(1814, 'movi_envio', '32', 'c.zambrano', 1611587419, 1611587419, 3),
(1815, 'movi_recepcion', '31', 'c.zambrano', 1611587490, 1611587490, 3),
(1816, 'verificacion', '279', 'c.zambrano', 1611587603, 1611587669, 3),
(1820, 'verificacion', '280', 'c.zambrano', 1611589628, 1611589628, 3),
(1821, 'movimientos', '175', 'c.zambrano', 1611589746, 1611589951, 3),
(1822, 'movi_envio', '34', 'c.zambrano', 1611589867, 1611589867, 3),
(1823, 'movi_recepcion', '33', 'c.zambrano', 1611589932, 1611589932, 3),
(1824, 'movimientos', '176', 'c.zambrano', 1611590018, 1611590902, 3),
(1825, 'movi_envio', '35', 'c.zambrano', 1611590091, 1611590866, 3),
(1826, 'movi_recepcion', '34', 'c.zambrano', 1611590154, 1611590154, 3),
(1827, 'verificacion', '281', 'c.zambrano', 1611590314, 1611590314, 3),
(1828, 'verificacion', '282', 'c.zambrano', 1611590351, 1611590351, 3),
(1829, 'movimientos', '177', 'c.zambrano', 1611593341, 1611593341, 3),
(1830, 'movimientos', '178', 'c.zambrano', 1611597749, 1611598164, 3),
(1831, 'movi_envio', '36', 'c.zambrano', 1611597977, 1611597977, 3),
(1832, 'movi_recepcion', '35', 'c.zambrano', 1611598054, 1611598054, 3),
(1834, 'movimientos', '179', 'c.zambrano', 1611598510, 1611598715, 3),
(1835, 'movi_envio', '37', 'c.zambrano', 1611598649, 1611598649, 3),
(1836, 'movi_recepcion', '37', 'c.zambrano', 1611598707, 1611598707, 3),
(1837, 'movimientos', '180', 'c.zambrano', 1611598810, 1611599054, 3),
(1838, 'movi_envio', '38', 'c.zambrano', 1611598945, 1611598945, 3),
(1839, 'movi_recepcion', '38', 'c.zambrano', 1611599046, 1611599046, 3),
(1840, 'movimientos', '181', 'c.zambrano', 1611599143, 1611599284, 3),
(1841, 'movi_envio', '39', 'c.zambrano', 1611599208, 1611599208, 3),
(1842, 'movi_recepcion', '39', 'c.zambrano', 1611599275, 1611599275, 3),
(1843, 'verificacion', '283', 'c.zambrano', 1611599398, 1611599398, 3),
(1844, 'verificacion', '284', 'c.zambrano', 1611599468, 1611599468, 3),
(1845, 'verificacion', '285', 'c.zambrano', 1611599562, 1611599562, 3),
(1846, 'movimientos', '182', 'c.zambrano', 1611599802, 1611599969, 3),
(1847, 'movi_envio', '40', 'c.zambrano', 1611599906, 1611599906, 3),
(1848, 'movi_recepcion', '40', 'c.zambrano', 1611599958, 1611599958, 3),
(1849, 'verificacion', '286', 'c.zambrano', 1611600186, 1611600186, 3),
(1850, 'movimientos', '183', 'c.zambrano', 1611600254, 1611600520, 3),
(1851, 'movi_envio', '41', 'c.zambrano', 1611600352, 1611600352, 3),
(1852, 'movi_recepcion', '41', 'c.zambrano', 1611600512, 1611600512, 3),
(1853, 'verificacion', '287', 'c.zambrano', 1611600548, 1611600548, 3),
(1854, 'movimientos', '184', 'c.zambrano', 1611600665, 1611600784, 3),
(1855, 'movi_envio', '42', 'c.zambrano', 1611600723, 1611600723, 3),
(1856, 'movi_recepcion', '42', 'c.zambrano', 1611600776, 1611600776, 3),
(1857, 'verificacion', '288', 'c.zambrano', 1611600816, 1611600816, 3),
(1858, 'movimientos', '185', 'c.zambrano', 1611603712, 1611603998, 3),
(1859, 'movi_envio', '43', 'c.zambrano', 1611603835, 1611603835, 3),
(1860, 'movi_recepcion', '43', 'c.zambrano', 1611603987, 1611603987, 3),
(1861, 'verificacion', '289', 'c.zambrano', 1611604055, 1611604055, 3),
(1862, 'verificacion', '290', 'felipem', 1611610900, 1611610900, 2),
(1863, 'verificacion', '291', 'felipem', 1611610974, 1611610974, 2),
(1864, 'verificacion', '292', 'felipem', 1611611016, 1611611016, 2),
(1865, 'verificacion', '293', 'felipem', 1611612084, 1611613162, 2),
(1866, 'verificacion', '294', 'felipem', 1611612107, 1611612107, 2),
(1867, 'verificacion', '295', 'felipem', 1611612148, 1611612341, 2),
(1868, 'tipo_dispositivo', '32', 'felipem', 1611614705, 1611614705, 2),
(1869, 'marcas', '36', 'felipem', 1611615839, 1611615839, 2),
(1870, 'marca_modelo', '65', 'felipem', 1611615935, 1611615935, 2),
(1871, 'verificacion', '296', 'felipem', 1611616111, 1611616111, 2),
(1872, 'marca_modelo', '66', 'a.lopez', 1611673903, 1611673903, 3),
(1874, 'verificacion', '298', 'a.lopez', 1611674782, 1611674782, 3),
(1875, 'marcas', '37', 'felipem', 1611770470, 1611770470, 2),
(1876, 'marca_modelo', '67', 'felipem', 1611770713, 1611770713, 2),
(1877, 'verificacion', '299', 'felipem', 1611770801, 1611770801, 2),
(1878, 'verificacion', '300', 'felipem', 1611770929, 1611770929, 2),
(1879, 'movimientos', '186', 'a.lopez', 1612196701, 1612196701, 3),
(1880, 'verificacion', '301', 'a.lopez', 1612196854, 1612196854, 3),
(1881, 'verificacion', '302', 'felipem', 1612831115, 1612831115, 2),
(1883, 'movimientos', '187', 'a.lopez', 1612883545, 1612883545, 3),
(1884, 'movi_envio', '44', 'a.lopez', 1612883674, 1612883674, 3),
(1885, 'movimientos', '188', 'felipem', 1613155309, 1613155309, 2),
(1886, 'verificacion', '303', 'a.lopez', 1613495538, 1613495538, 3),
(1887, 'verificacion', '304', 'a.lopez', 1613500641, 1613500645, 3),
(1888, 'movimientos', '189', 'a.lopez', 1613500653, 1613500899, 3),
(1889, 'movi_envio', '45', 'a.lopez', 1613500758, 1613500758, 3),
(1890, 'movi_recepcion', '44', 'a.lopez', 1613500820, 1613500820, 3),
(1891, 'movimientos', '190', 'felipem', 1613746042, 1613746042, 2),
(1892, 'movi_envio', '46', 'felipem', 1613746217, 1613746283, 2),
(1893, 'movi_recepcion', '45', 'felipem', 1613746629, 1613746629, 2),
(1894, 'mantenimientos', '18', 'felipem', 1613746819, 1613746971, 2),
(1895, 'financiero', '2', 'felipem', 1613746994, 1613746994, 2),
(1896, 'marcas', '38', 'a.lopez', 1613748381, 1613748381, 3),
(1897, 'verificacion', '305', 'a.lopez', 1613749849, 1613749849, 3),
(1898, 'marcas', '39', 'felipem', 1614091869, 1614091902, 2),
(1899, 'marca_modelo', '68', 'felipem', 1614092109, 1614092109, 2),
(1900, 'tipo_dispositivo', '33', 'felipem', 1614092201, 1614092201, 2),
(1901, 'verificacion', '306', 'felipem', 1614092279, 1614092279, 2),
(1902, 'tipo_dispositivo', '34', 'felipem', 1614092855, 1614092855, 2),
(1903, 'marcas', '40', 'felipem', 1614092919, 1614092919, 2),
(1904, 'marca_modelo', '69', 'felipem', 1614093486, 1614093486, 2),
(1905, 'verificacion', '307', 'felipem', 1614093750, 1614093750, 2),
(1906, 'verificacion', '308', 'felipem', 1614093884, 1614093884, 2),
(1907, 'verificacion', '309', 'felipem', 1614094135, 1614094135, 2),
(1908, 'marca_modelo', '70', 'a.lopez', 1614094468, 1614094468, 3),
(1909, 'verificacion', '310', 'a.lopez', 1614094540, 1614094540, 3),
(1910, 'verificacion', '311', 'a.lopez', 1614094874, 1614094874, 3),
(1911, 'verificacion', '312', 'a.lopez', 1614095330, 1614095330, 3),
(1919, 'documentos', '22', 'a.lopez', 1614100371, 1614100371, 3),
(1913, 'documentos', '16', 'a.lopez', 1614098601, 1614098601, 3),
(1914, 'documentos', '17', 'a.lopez', 1614098922, 1614098981, 3),
(1915, 'documentos', '18', 'a.lopez', 1614099217, 1614099440, 3),
(1916, 'documentos', '19', 'a.lopez', 1614099632, 1614099632, 3),
(1917, 'documentos', '20', 'a.lopez', 1614099895, 1614099895, 3),
(1918, 'documentos', '21', 'a.lopez', 1614100185, 1614100185, 3),
(1920, 'documentos', '23', 'a.lopez', 1614100524, 1614100524, 3),
(1921, 'documentos', '24', 'a.lopez', 1614100689, 1614100689, 3),
(1922, 'documentos', '25', 'a.lopez', 1614100822, 1614100822, 3),
(1923, 'mantenimientos', '19', 'l.solarte', 1614181390, 1614181390, 8),
(1924, 'documentos', '26', 'a.lopez', 1614610429, 1614610429, 3),
(1925, 'documentos', '27', 'a.lopez', 1614611061, 1614611061, 3),
(1926, 'documentos', '28', 'a.lopez', 1614611419, 1614611419, 3),
(1927, 'documentos', '29', 'a.lopez', 1614611967, 1614611967, 3),
(1928, 'documentos', '30', 'a.lopez', 1614612322, 1614612322, 3),
(1929, 'verificacion', '313', 'a.lopez', 1614617283, 1614617283, 3),
(1930, 'documentos', '31', 'a.lopez', 1614625045, 1614625045, 3),
(1931, 'verificacion', '314', 'a.lopez', 1614705890, 1614706138, 3),
(1932, 'verificacion', '315', 'a.lopez', 1614706059, 1614706059, 3),
(1933, 'verificacion', '316', 'a.lopez', 1614706220, 1614706220, 3),
(1934, 'verificacion', '317', 'a.lopez', 1614717367, 1614717367, 3),
(1936, 'verificacion', '318', 'a.lopez', 1614806271, 1614806271, 3),
(1937, 'documentos', '33', 'a.lopez', 1614808478, 1614808478, 3),
(1938, 'documentos', '34', 'a.lopez', 1614875658, 1614875658, 3),
(1939, 'verificacion', '319', 'a.lopez', 1615487317, 1615487317, 3),
(1940, 'verificacion', '320', 'a.lopez', 1615490078, 1615490078, 3),
(1941, 'verificacion', '321', 'a.lopez', 1615912554, 1615912554, 3),
(1942, 'marca_modelo', '71', 'a.lopez', 1617747752, 1617747771, 3),
(1943, 'verificacion', '322', 'a.lopez', 1617747928, 1617747928, 3),
(1944, 'verificacion', '323', 'a.lopez', 1617748158, 1617748300, 3),
(1945, 'verificacion', '324', 'a.lopez', 1617748488, 1617748488, 3),
(1946, 'verificacion', '325', 'a.lopez', 1617748740, 1617748740, 3),
(1947, 'verificacion', '326', 'a.lopez', 1617748909, 1617748924, 3),
(1948, 'movimientos', '191', 'felipem', 1617888266, 1617888582, 2),
(1949, 'movi_envio', '47', 'felipem', 1617888502, 1617888512, 2),
(1950, 'mantenimientos', '20', 'l.solarte', 1617908691, 1617910779, 8),
(1951, 'mantenimientos', '21', 'l.solarte', 1617910717, 1618331980, 8),
(1952, 'mantenimientos', '22', 'l.solarte', 1617910936, 1617911248, 8),
(1953, 'mantenimientos', '23', 'l.solarte', 1617911359, 1617911359, 8),
(1954, 'marca_modelo', '72', 'a.lopez', 1617911734, 1617911734, 3),
(1955, 'mantenimientos', '24', 'l.solarte', 1617912809, 1617914102, 8),
(1956, 'verificacion', '327', 'a.lopez', 1617912974, 1617912974, 3),
(1957, 'mantenimientos', '25', 'l.solarte', 1617913094, 1617913094, 8),
(1958, 'mantenimientos', '26', 'l.solarte', 1617913795, 1617913795, 8),
(1959, 'movimientos', '192', 'a.lopez', 1617914119, 1617914329, 3),
(1960, 'movi_envio', '49', 'a.lopez', 1617914185, 1617914198, 3),
(1961, 'movi_recepcion', '46', 'a.lopez', 1617914267, 1617914279, 3),
(1962, 'verificacion', '328', 'a.lopez', 1617914424, 1617914424, 3),
(1963, 'mantenimientos', '27', 'l.solarte', 1617914426, 1617914426, 8),
(1964, 'verificacion', '329', 'a.lopez', 1617975501, 1617975501, 3),
(1965, 'verificacion', '330', 'a.lopez', 1617993923, 1617993923, 3),
(1966, 'verificacion', '331', 'a.lopez', 1617993998, 1617993998, 3),
(1967, 'verificacion', '332', 'a.lopez', 1617994077, 1617994077, 3),
(1968, 'verificacion', '333', 'a.lopez', 1617994129, 1617994129, 3),
(1969, 'verificacion', '334', 'a.lopez', 1617994210, 1617994210, 3),
(1970, 'verificacion', '335', 'a.lopez', 1617994289, 1617994289, 3),
(1971, 'verificacion', '336', 'a.lopez', 1617994523, 1617994523, 3),
(1972, 'movimientos', '193', 'a.lopez', 1617994769, 1617995034, 3),
(1973, 'movi_envio', '50', 'a.lopez', 1617994840, 1617994875, 3),
(1974, 'movi_recepcion', '47', 'a.lopez', 1617995012, 1617995026, 3),
(1975, 'mantenimientos', '28', 'a.lopez', 1618162739, 1618162866, 3),
(1976, 'mantenimientos', '29', 'a.lopez', 1618163109, 1618163109, 3),
(1977, 'mantenimientos', '30', 'a.lopez', 1618163320, 1618163320, 3),
(1978, 'mantenimientos', '31', 'a.lopez', 1618163561, 1618331995, 3),
(1979, 'mantenimientos', '32', 'a.lopez', 1618163954, 1618163954, 3),
(1980, 'mantenimientos', '33', 'a.lopez', 1618164816, 1618164816, 3),
(1981, 'mantenimientos', '34', 'l.solarte', 1618237006, 1618237006, 8),
(1982, 'mantenimientos', '35', 'l.solarte', 1618240792, 1618240792, 8),
(1983, 'mantenimientos', '36', 'l.solarte', 1618241306, 1618412554, 8),
(1984, 'mantenimientos', '37', 'l.solarte', 1618242485, 1618331722, 8),
(1985, 'movimientos', '194', 'l.solarte', 1618242725, 1618242725, 8),
(1986, 'movimientos', '195', 'l.solarte', 1618242734, 1618242734, 8),
(1987, 'movimientos', '196', 'l.solarte', 1618242759, 1618242759, 8),
(1988, 'movimientos', '197', 'a.lopez', 1618242870, 1618243099, 3),
(1989, 'movi_envio', '51', 'a.lopez', 1618242931, 1618242931, 3),
(1990, 'movi_recepcion', '48', 'a.lopez', 1618243091, 1618243091, 3),
(1991, 'movimientos', '198', 'l.solarte', 1618243174, 1618243174, 8),
(1992, 'movimientos', '199', 'l.solarte', 1618243183, 1618243183, 8),
(1993, 'movimientos', '200', 'a.lopez', 1618243238, 1618243530, 3),
(1994, 'movi_envio', '52', 'a.lopez', 1618243360, 1618243360, 3),
(1995, 'mantenimientos', '38', 'l.solarte', 1618243487, 1618413297, 8),
(1996, 'movi_recepcion', '49', 'a.lopez', 1618243495, 1618243495, 3),
(1997, 'verificacion', '337', 'a.lopez', 1618243592, 1618243592, 3),
(1998, 'mantenimientos', '39', 'l.solarte', 1618244680, 1618244680, 8),
(1999, 'verificacion', '338', 'a.lopez', 1618246087, 1618246180, 3),
(2000, 'verificacion', '339', 'a.lopez', 1618246260, 1618246260, 3),
(2001, 'mantenimientos', '40', 'l.solarte', 1618249012, 1618249012, 8),
(2002, 'movimientos', '201', 'a.lopez', 1618263548, 1618264180, 3),
(2003, 'movi_envio', '53', 'a.lopez', 1618263694, 1618263694, 3),
(2004, 'movi_recepcion', '50', 'a.lopez', 1618264022, 1618264169, 3),
(2005, 'mantenimientos', '41', 'a.lopez', 1618267521, 1618322794, 3),
(2006, 'mantenimientos', '42', 'a.lopez', 1618321971, 1618412655, 3),
(2007, 'verificacion', '340', 'a.lopez', 1618322100, 1618322100, 3),
(2008, 'verificacion', '341', 'a.lopez', 1618322224, 1618322224, 3),
(2009, 'verificacion', '342', 'a.lopez', 1618322392, 1618322392, 3),
(2010, 'costos_mtto', '1', 'a.lopez', 1618322544, 1618322777, 3),
(2011, 'verificacion', '343', 'a.lopez', 1618322552, 1618322552, 3),
(2012, 'mantenimientos', '43', 'a.lopez', 1618322965, 1618322965, 3),
(2013, 'observaciones', '22', 'felipem', 1618327436, 1618327436, 2),
(2014, 'verificacion', '344', 'a.lopez', 1618328250, 1618328250, 3),
(2015, 'verificacion', '345', 'a.lopez', 1618329144, 1618329144, 3),
(2016, 'verificacion', '346', 'a.lopez', 1618329645, 1618329645, 3),
(2017, 'mantenimientos', '44', 'a.lopez', 1618331638, 1618331638, 3),
(2018, 'mantenimientos', '45', 'a.lopez', 1618331754, 1618331754, 3),
(2019, 'mantenimientos', '46', 'a.lopez', 1618334432, 1618334432, 3),
(2020, 'verificacion', '347', 'a.lopez', 1618357089, 1618357089, 3),
(2021, 'verificacion', '348', 'a.lopez', 1618409169, 1618409169, 3),
(2022, 'mantenimientos', '47', 'a.lopez', 1618412289, 1618412289, 3),
(2023, 'mantenimientos', '48', 'a.lopez', 1618412424, 1618412514, 3),
(2024, 'mantenimientos', '49', 'a.lopez', 1618412590, 1618412590, 3),
(2025, 'mantenimientos', '50', 'a.lopez', 1618412699, 1618412699, 3),
(2026, 'mantenimientos', '51', 'a.lopez', 1618412764, 1618412764, 3),
(2027, 'mantenimientos', '52', 'a.lopez', 1618413329, 1618413329, 3),
(2028, 'mantenimientos', '53', 'a.lopez', 1618413456, 1618413456, 3),
(2029, 'verificacion', '349', 'a.lopez', 1618413557, 1618413557, 3),
(2030, 'mantenimientos', '54', 'a.lopez', 1618413597, 1618413597, 3),
(2031, 'mantenimientos', '55', 'a.lopez', 1618501854, 1618501854, 3),
(2032, 'verificacion', '350', 'a.lopez', 1618843860, 1618843860, 3),
(2033, 'verificacion', '351', 'a.lopez', 1618844136, 1618844136, 3),
(2034, 'verificacion', '352', 'a.lopez', 1618850522, 1618850580, 3),
(2035, 'tipo_estado_dispo', '5', 'felipem', 1618853960, 1618853960, 2),
(2036, 'movi_recepcion', '51', 'felipem', 1618861310, 1618861310, 2),
(2037, 'unidades', '19', 'felipem', 1618861841, 1618861866, 2),
(2038, 'verificacion', '353', 'a.lopez', 1618862157, 1618862157, 3),
(2039, 'verificacion', '354', 'a.lopez', 1618862435, 1618862435, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership_users`
--

CREATE TABLE `membership_users` (
  `memberID` varchar(100) NOT NULL,
  `passMD5` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signupDate` date DEFAULT NULL,
  `groupID` int(10) UNSIGNED DEFAULT NULL,
  `isBanned` tinyint(4) DEFAULT NULL,
  `isApproved` tinyint(4) DEFAULT NULL,
  `custom1` text,
  `custom2` text,
  `custom3` text,
  `custom4` text,
  `comments` text,
  `pass_reset_key` varchar(100) DEFAULT NULL,
  `pass_reset_expiry` int(10) UNSIGNED DEFAULT NULL,
  `flags` text,
  `allowCSVImport` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `membership_users`
--

INSERT INTO `membership_users` (`memberID`, `passMD5`, `email`, `signupDate`, `groupID`, `isBanned`, `isApproved`, `custom1`, `custom2`, `custom3`, `custom4`, `comments`, `pass_reset_key`, `pass_reset_expiry`, `flags`, `allowCSVImport`) VALUES
('guest', NULL, NULL, '2019-11-26', 1, 0, 1, NULL, NULL, NULL, NULL, 'Anonymous member created automatically on 2019-11-26', NULL, NULL, NULL, 0),
('felipem', '$2y$10$rmdod31npGwE.1/ItHglDuki0KwcwqhHh6mOT6Q3WIYUfkxDboRFy', 'tecnologia@starmedica.com.co', '2019-11-26', 2, 0, 1, NULL, NULL, NULL, NULL, 'Admin member created automatically on 2019-11-26\nRecord updated automatically on 2019-12-02\nRecord updated automatically on 2020-09-10\nRegistro actualizado automáticamente en fecha 2020-11-13', NULL, NULL, NULL, 0),
('l.garcia', '$2y$10$E.acyF5gBBk8yJFqh4ahzeIPa6z7wtWlPkCB1a6dA.ZT2oGAuZ68.', 'cardio.auditoria@gmail.com', '2019-11-27', 3, 0, 1, 'Luis Garcia', '313', 'C', '', '', NULL, NULL, NULL, 0),
('v.ocampo', '$2y$10$A6o/En7F23Qq/RjY8Gn4IuJLENkzWd1dVQZnT1Y8g2V5y/PV8W8R2', 'gerencia@starmedica.com.co', '2019-11-27', 3, 0, 1, 'Vilma Ocampo', '', '', '', '', NULL, NULL, NULL, 0),
('luzeidy', '$2y$10$lDpcGchI.5PN8u8gX6hblegSwqnPwIxUS5xlF0Q1q42kmDkpiOeyK', 'cardio.financiera@gmail.com', '2019-12-02', 4, 0, 1, 'Luzeidy Zambrano', '', 'Cali', '', '', NULL, NULL, NULL, 0),
('w.ibarra', '$2y$10$/TcJ0L.NNLnPWVDlLAgg4uwbWBVpWqolhApdNIcBc7oHbMUvLLvCC', 'cardio.procesos@gmail.com', '2019-12-02', 4, 0, 1, 'William Ibarra', 'Compras y Procesos', 'Cali', 'Starmedica', '', NULL, NULL, NULL, 0),
('bodega.felipe', '$2y$10$8uumdourCA3AZ9yS8mdQAOEDwy0AE9.V1ImVFvdZNMj4fWxoj2U/K', 'tecnologia@starmedica.com.co', '2019-12-27', 5, 0, 1, 'Felipe Molano', 'Bodega', 'Cali', 'STARMEDICA', '', NULL, NULL, NULL, 0),
('c.zambrano', '$2y$10$H7zFRIMfoGfQSR4Wtm4wietmX5Wztg9HYvyKYam5fn4CzG9PZmuJC', 'camilomunoz1064@gmail.com', '2020-02-12', 3, 0, 1, 'Camilo Zambrano', 'Tecnologia', 'Cali', 'Starmedica', '', NULL, NULL, NULL, 0),
('j.villa', '$2y$10$fAUMILtSTM9gC3A7L9G1/.UTSwp2ZGk2mgZI14BUUPB.zSHxbSfGu', 'hidc@starmedica.com.co', '2020-04-28', 6, 0, 1, 'Jorge Luis Villa', 'HIDC', '', 'Starmedica', 'FLM', NULL, NULL, NULL, 0),
('d.ruiz', '$2y$10$zl250b5ZfAtxDoAU37amRe9cV9b1SqziT74LvId5oS9E6F6xDnqNS', 'hidc@starmedica.com.co', '2020-04-28', 6, 0, 1, 'David Alexander Ruiz', '', 'Cali', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('crueda', '$2y$10$g7TKPlMqaNRPOcNKddCjC.fXNVAysgqODdevfLMws0csBPY88uCN.', 'Htu@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Claudia Rueda', '', '', '', '', NULL, NULL, NULL, 0),
('jpalacios', '$2y$10$MSS1F9nx885qv1C.phGJUuhQwcdG5FeKHL5posipPwp7yXpRLvOQ.', 'hfps@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Janeth Palacios', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('aospina', '$2y$10$CgtKty23b7MvRPlMaLHL2.E1jyzKEXYnD5UBgnmxW6JHjXrVFsVd6', 'hrob@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Andres Ospina', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('yulianar', '$2y$10$8odv2jQN88ULJENhSAceQeI/P4.SUL.kPhp/zAG1uShGMPlzVN0gu', 'administracion@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Yuliana Roa', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('aida', '$2y$10$G5yEj/vTKX5uONwQ3/AZOO.7B7G/blGSuyc8hlMha2p8.lb0AxArq', 'adm@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Aida Milena', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('sgiraldo', '$2y$10$oJB2ufw.JB0Kp0SNrfjrs.uwWwnSTmLN.g.Q46QRrWeNOKWfod3l.', 'Htu@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Samir Giraldo', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('mmartinez', '$2y$10$Ov/yHx.lawxWeR1qe8kmC.VYzzuLUwJnirowwhov0QhtkvrR4bL6K', 'fhsj@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Mary Martinez', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('srendon', '$2y$10$/i66ZuKkE/ntGxsnplW2tei6wu5CAl8zV4sN4HJyDeOO.osbUfSmC', 'fhsj@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Sebastian Rendon', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('diegoh', '$2y$10$de5wPgEICRaZkwxfJbK/4uW651uMMhDkZBlelc0DZpL7fA.6jqi2O', 'fhsj@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Diego Hernan', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('jvalencia', '$2y$10$n8oZGX1OnN.e6PFCvPj9s.e0i2sAlO8dmo1ekd2m/m5485yusKDrG', 'fhsj@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Javier Francisco Valencia', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('mnieto', '$2y$10$lv./nAtshgFyFG9S/CAiEOXEQzOLP7PMDkaHgjaL.VtoDwqgNZou6', 'holter.tulua@gmail.com', '2020-04-29', 6, 0, 1, 'Monica Nieto', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('dvalencia', '$2y$10$XZj.n9BTG0nxk7toXSNVxuELX.Fc6qXnrF40NbnM4pu.G/9ytzEIa', 'danielavalenciapaz@gmail.com', '2020-04-29', 6, 0, 1, 'Daniela Valencia', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('ccastro', '$2y$10$MXXE4w9Hh8P4zRH75NzaX.sgYW9KdP8wLrDLsbxSCH0jBXE.aVkAe', 'Htu@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Cesar Andres Castro', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('wibarra', '$2y$10$cXsANUC26QkJ7IaTSQYfT.zMnpkpRqoA40Bn/IkV/khO/swbZC/52', 'cardio.procesos@gmail.com', '2020-04-29', 6, 0, 1, 'William Ibarra', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('hrestrepo', '$2y$10$TfvQeTirXQWULOzm0l9YqOsVrkLvkPc5eZWOJcEPa3OjLS4/ig7DO', 'Htu@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Harold Restrepo', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('jesusv', '$2y$10$F/wiNTWif6co3wRHiU3N3.VI.r9Um5U8Lj308T15zBY7RCCvNDttq', 'tecnologia@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Jesus Davida Varela', '', 'Cali', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('carlosc', '$2y$10$IL5qlrI8g93W/GempahDS.QghlbheYM229q1YaFecIku1W0RDaRee', 'tecnologia@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Carlos Carvajal', '', 'Cali', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('ibet', '$2y$10$6hnxQZ7nk4Y6QroQh05cCOhXI2YCyMwoQJFX.0EWAoMxYJnnJNHGS', 'tecnologia@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Ibet Toro', '', 'Cali', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('wilmarh', '$2y$10$KpCQE8v6HKJ.oVKCwBjXfun57tTbTBMyieiSgwFsB2tHDxHDg9YI2', 'tecnologia@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Wilmar Hernán mamian', '', 'Cali', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('fvalencia', '$2y$10$HJ429dj/3nnqr8yffKmOrO6zyCoQP7vu9M8RO.Vt.PjPiVfXcVJ0u', 'hmc@starmedica.com.co', '2020-04-29', 6, 0, 1, 'Frank Valencia', '', '', '', 'member signed up through the registration form.', NULL, NULL, NULL, 0),
('jcastro', '$2y$10$EraHgKTE1CbyrdJm9jzve.2eJ744kfWheoJoMwPAkkb9gIeb5uYAO', 'cardio.calidad2@gmail.com', '2020-05-05', 6, 0, 1, 'Johana Castro', '', '', '', 'Flm', NULL, NULL, NULL, 0),
('a.lopez', '$2y$10$L3NzNmweoozV1nKAIn2ojuDkvG4Hu/Bwgpb9ToQM8IdQitvNYCE06', 'cardio.tecnologia@gmail.com', '2020-11-17', 3, 0, 1, 'Andrés López', 'Tecnología', 'Valle del Cauca', 'Starmedica', 'Flm', NULL, NULL, NULL, 0),
('l.solarte', '$2y$10$Mgchc4lttgZDuUicaIZX8eFwgroSnLSxjtQLo3sEgsR141RzYd25C', 'cardio.luissolarte@gmail.com', '2021-02-24', 8, 0, 1, 'Ing. Luis Alberto Solarte', '', 'Bogota', '', '\nmember updated his profile on 14/04/2021, 11:23 am from IP address 200.29.107.50', NULL, NULL, NULL, 0),
('p.maya', '$2y$10$.LMiA5lYG/j1Wzlz5D7kfepnmziIf5EXXqPYHKSw8/Zd6iBGpEcgq', 'procoachflm@gmail.com', '2021-03-01', 9, 0, 1, 'Paola Maya', 'Los Increíbles', 'Cali', '', '', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `membership_usersessions`
--

CREATE TABLE `membership_usersessions` (
  `memberID` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `agent` varchar(100) NOT NULL,
  `expiry_ts` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `membership_usersessions`
--

INSERT INTO `membership_usersessions` (`memberID`, `token`, `agent`, `expiry_ts`) VALUES
('a.lopez', 'PZ0KonCBiEJQwWpfbx5HS11V91YEj6', 'LuWl79twZzxW4TWCxo68yeueV4udty', 1615493560),
('l.solarte', 'FqEjsIYVR6MS6HKfXkkalSQ92SLLb7', '5isXQBKNWW24LSwA5m5fSZvvfYgtEn', 1616851918);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mobiliario`
--

CREATE TABLE `mobiliario` (
  `id_mobiliario` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(40) DEFAULT NULL,
  `tipo_mobiliario` int(10) UNSIGNED NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `unidad` int(10) UNSIGNED DEFAULT NULL,
  `uni_abreviado` int(10) UNSIGNED DEFAULT NULL,
  `descripcion` text,
  `accesorios` text,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `grupo` int(10) UNSIGNED DEFAULT NULL,
  `foto` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id_movi` int(10) UNSIGNED NOT NULL,
  `tipo_movimiento` int(10) UNSIGNED NOT NULL,
  `codigo_dispo` int(10) UNSIGNED NOT NULL,
  `serial_dispo` int(10) UNSIGNED DEFAULT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL,
  `origen` int(10) UNSIGNED NOT NULL,
  `fecha_movimiento` date NOT NULL,
  `destino` int(10) UNSIGNED NOT NULL,
  `responsable` int(10) UNSIGNED NOT NULL,
  `telefono_respon` int(10) UNSIGNED DEFAULT NULL,
  `estado_movi` int(10) UNSIGNED DEFAULT NULL,
  `verifica` int(10) UNSIGNED DEFAULT NULL,
  `estado_verifica` int(10) UNSIGNED NOT NULL,
  `comentarios` text,
  `creado2` varchar(40) DEFAULT NULL,
  `timestamp` varchar(40) DEFAULT NULL,
  `update` varchar(40) DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `documento` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `field18` varchar(40) DEFAULT NULL,
  `img` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id_movi`, `tipo_movimiento`, `codigo_dispo`, `serial_dispo`, `tipo_dispo`, `origen`, `fecha_movimiento`, `destino`, `responsable`, `telefono_respon`, `estado_movi`, `verifica`, `estado_verifica`, `comentarios`, `creado2`, `timestamp`, `update`, `creado_por`, `editado_por`, `documento`, `creado`, `editado`, `field18`, `img`) VALUES
(1, 1, 56, 56, 56, 1, '2019-11-27', 8, 2, 2, 1, NULL, 1, 'Ok', '27/11/2019 06:46:08 pm', '27/11/2019 06:46:08 pm', '27/11/2019 06:50:10 pm', 'vilmao', 'vilmao', NULL, '2019-11-27 00:00:00', NULL, NULL, NULL),
(2, 2, 56, 56, 56, 1, '2019-11-27', 8, 2, 2, 2, 56, 2, 'ok', '27/11/2019 06:49:34 pm', '27/11/2019 06:49:34 pm', NULL, 'vilmao', NULL, NULL, '2019-11-27 00:00:00', NULL, NULL, NULL),
(3, 2, 57, 57, 57, 7, '2019-11-29', 9, 3, 3, 1, 57, 2, 'Nuevo', '29/11/2019 04:51:59 pm', '29/11/2019 04:51:59 pm', '29/11/2019 04:54:36 pm', 'felipem', 'felipem', NULL, '2019-11-29 00:00:00', NULL, NULL, 13),
(4, 1, 58, 58, 58, 7, '2019-11-29', 9, 3, 3, 1, 58, 2, 'Nuevo', '29/11/2019 05:06:55 pm', '29/11/2019 05:06:55 pm', NULL, 'felipem', NULL, NULL, '2019-11-29 00:00:00', NULL, NULL, 13),
(5, 1, 17, 17, 17, 10, '2019-12-03', 1, 3, 3, 1, NULL, 1, 'Dispositivo trasladado a HIDC, se ha realizado un mantenimiento y limpieza', '3/12/2019 12:44:26 am', '3/12/2019 12:44:26 am', '3/12/2019 01:38:31 pm', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(6, 2, 17, 17, 17, 10, '2019-12-03', 1, 3, 3, 2, 17, 2, 'Recibido en la unidad', '3/12/2019 12:45:56 am', '3/12/2019 12:45:56 am', '3/12/2019 01:39:14 pm', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(7, 1, 23, 23, 23, 6, '2019-12-03', 1, 3, 3, 1, 23, 1, 'Update 3 de diciembre', '3/12/2019 11:05:01 am', '3/12/2019 11:05:01 am', '4/3/2020 06:32:03 pm', 'felipem', 'camiloz', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(8, 2, 23, 23, 23, 6, '2019-12-03', 1, 3, 3, 2, 23, 2, 'Recibido', '3/12/2019 11:06:48 am', '3/12/2019 11:06:48 am', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(9, 1, 26, 26, 26, 6, '2019-12-03', 1, 3, 3, NULL, NULL, 1, NULL, '3/12/2019 11:10:01 am', '3/12/2019 11:10:01 am', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(10, 2, 26, 26, 26, 6, '2019-12-03', 1, 3, 3, 2, 26, 2, NULL, '3/12/2019 11:10:13 am', '3/12/2019 11:10:13 am', '3/12/2019 11:10:42 am', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(11, 1, 2, 2, 2, 7, '2019-12-03', 1, 3, 3, 1, NULL, 1, NULL, '3/12/2019 11:11:58 am', '3/12/2019 11:11:58 am', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(12, 2, 2, 2, 2, 7, '2019-12-03', 1, 3, 3, 1, 2, 2, NULL, '3/12/2019 11:12:02 am', '3/12/2019 11:12:02 am', '3/12/2019 11:13:19 am', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(13, 1, 9, 9, 9, 7, '2019-12-03', 1, 3, 3, 1, NULL, 1, NULL, '3/12/2019 11:14:47 am', '3/12/2019 11:14:47 am', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(14, 2, 9, 9, 9, 7, '2019-12-03', 1, 3, 3, 1, 9, 2, NULL, '3/12/2019 11:14:52 am', '3/12/2019 11:14:52 am', '3/12/2019 11:15:21 am', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(15, 2, 80, 80, 80, 7, '2019-12-03', 1, 3, 3, 1, 80, 2, NULL, '3/12/2019 11:18:12 am', '3/12/2019 11:18:12 am', '3/12/2019 11:18:43 am', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(16, 2, 82, 82, 82, 7, '2019-12-03', 1, 3, 3, 1, 82, 2, NULL, '3/12/2019 11:20:57 am', '3/12/2019 11:20:57 am', '3/12/2019 11:22:13 am', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(17, 1, 82, 82, 82, 7, '2019-12-03', 1, 3, 3, 1, NULL, 1, NULL, '3/12/2019 11:21:07 am', '3/12/2019 11:21:07 am', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(18, 1, 50, 50, 50, 7, '2019-12-03', 1, 3, 3, 1, NULL, 1, NULL, '3/12/2019 11:26:42 am', '3/12/2019 11:26:42 am', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(19, 2, 50, 50, 50, 7, '2019-12-03', 1, 3, 3, 1, 50, 2, NULL, '3/12/2019 11:26:55 am', '3/12/2019 11:26:55 am', '3/12/2019 11:27:35 am', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(20, 1, 60, 60, 60, 7, '2019-12-03', 1, 3, 3, 1, NULL, 1, NULL, '3/12/2019 01:44:39 pm', '3/12/2019 01:44:39 pm', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(21, 2, 60, 60, 60, 7, '2019-12-03', 1, 3, 3, 1, 60, 2, NULL, '3/12/2019 01:44:50 pm', '3/12/2019 01:44:50 pm', '3/12/2019 01:45:14 pm', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 0),
(22, 1, 59, 59, 59, 7, '2019-12-03', 1, 3, 3, 1, NULL, 1, NULL, '3/12/2019 01:45:51 pm', '3/12/2019 01:45:51 pm', NULL, 'felipem', NULL, NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(23, 2, 59, 59, 59, 7, '2019-12-03', 1, 3, 3, 2, 59, 2, NULL, '3/12/2019 01:45:54 pm', '3/12/2019 01:45:54 pm', '3/12/2019 01:46:20 pm', 'felipem', 'felipem', NULL, '2019-12-03 00:00:00', NULL, NULL, 13),
(24, 1, 199, 199, 199, 7, '2019-12-10', 4, 3, 3, 1, NULL, 1, 'Nuevo, enviado con HR', '10/12/2019 06:39:42 pm', '10/12/2019 06:39:42 pm', NULL, 'felipem', NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, 13),
(25, 2, 199, 199, 199, 7, '2019-12-10', 4, 3, 3, 2, 199, 2, 'Nuevo, enviado con HR', '10/12/2019 06:39:49 pm', '10/12/2019 06:39:49 pm', '10/12/2019 06:40:23 pm', 'felipem', 'felipem', NULL, '2019-12-10 00:00:00', NULL, NULL, 13),
(26, 1, 203, 203, 203, 7, '2019-12-09', 4, 3, 3, 1, NULL, 1, 'Holter nuevo enviado con HR', '10/12/2019 06:42:31 pm', '10/12/2019 06:42:31 pm', NULL, 'felipem', NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, 0),
(27, 2, 203, 203, 203, 7, '2019-12-09', 4, 3, 3, 2, 203, 2, 'Recibido en HTU', '10/12/2019 06:42:37 pm', '10/12/2019 06:42:37 pm', '10/12/2019 06:43:13 pm', 'felipem', 'felipem', NULL, '2019-12-10 00:00:00', NULL, NULL, 0),
(28, 1, 202, 202, 202, 7, '2019-12-09', 4, 3, 3, 1, NULL, 1, 'Nuevo enviado con HR', '10/12/2019 06:44:48 pm', '10/12/2019 06:44:48 pm', NULL, 'felipem', NULL, NULL, '2019-12-10 00:00:00', NULL, NULL, 13),
(29, 2, 202, 202, 202, 7, '2019-12-09', 4, 3, 3, 2, 202, 2, 'Recibído en la unidad', '10/12/2019 06:46:08 pm', '10/12/2019 06:46:08 pm', '10/12/2019 06:46:47 pm', 'felipem', 'felipem', NULL, '2019-12-10 00:00:00', NULL, NULL, 13),
(30, 1, 200, 200, 200, 7, '2019-12-05', 5, 3, 3, 1, NULL, 1, '<br>', '27/12/2019 11:04:26 am', '27/12/2019 11:04:26 am', NULL, 'felipem', NULL, NULL, '2019-12-27 00:00:00', NULL, NULL, 13),
(31, 2, 200, 200, 200, 7, '2019-12-05', 5, 3, 3, 2, 200, 2, '<br>', '27/12/2019 11:04:29 am', '27/12/2019 11:04:29 am', '27/12/2019 11:05:02 am', 'felipem', 'felipem', NULL, '2019-12-27 00:00:00', NULL, NULL, 13),
(32, 1, 201, 201, 201, 7, '2019-12-05', 5, 3, 3, 1, NULL, 1, '<br>', '27/12/2019 11:07:35 am', '27/12/2019 11:07:35 am', NULL, 'felipem', NULL, NULL, '2019-12-27 00:00:00', NULL, NULL, NULL),
(33, 1, 201, 201, 201, 7, '2019-12-05', 5, 3, 3, 2, 201, 2, '<br>', '27/12/2019 11:07:39 am', '27/12/2019 11:07:39 am', '27/12/2019 11:08:09 am', 'felipem', 'felipem', NULL, '2019-12-27 00:00:00', NULL, NULL, NULL),
(34, 1, 204, 204, 204, 7, '2019-12-05', 5, 3, 3, 1, NULL, 1, '<br>', '27/12/2019 11:09:54 am', '27/12/2019 11:09:54 am', NULL, 'felipem', NULL, NULL, '2019-12-27 00:00:00', NULL, NULL, NULL),
(35, 2, 204, 204, 204, 7, '2019-12-05', 5, 3, 3, 2, 204, 2, '<br>', '27/12/2019 11:10:10 am', '27/12/2019 11:10:10 am', '27/12/2019 11:10:27 am', 'felipem', 'felipem', NULL, '2019-12-27 00:00:00', NULL, NULL, NULL),
(36, 1, 90, 90, 90, 7, '2019-12-16', 9, 3, 3, 1, NULL, 1, '<br>', '27/12/2019 12:54:30 pm', '27/12/2019 12:54:30 pm', NULL, 'felipem', NULL, NULL, '2019-12-27 00:00:00', NULL, NULL, NULL),
(37, 2, 90, 90, 90, 7, '2019-12-16', 9, 3, 3, 2, 90, 2, '<br>', '27/12/2019 12:54:37 pm', '27/12/2019 12:54:37 pm', '27/12/2019 12:54:56 pm', 'felipem', 'felipem', NULL, '2019-12-27 00:00:00', NULL, NULL, NULL),
(38, 1, 88, 88, 88, 1, '2019-12-09', 9, 3, 3, 1, 88, 2, '<br>', '27/12/2019 12:59:42 pm', '27/12/2019 12:59:42 pm', NULL, 'felipem', NULL, NULL, '2019-12-27 00:00:00', NULL, NULL, 0),
(39, 1, 216, 216, 216, 7, '2020-02-04', 9, 3, 3, 1, NULL, 1, NULL, '4/2/2020 11:09:15 am', '4/2/2020 11:09:15 am', NULL, 'felipem', NULL, NULL, '2020-02-04 00:00:00', NULL, NULL, 13),
(40, 1, 217, 217, 217, 7, '2020-02-04', 9, 3, 3, 1, NULL, 1, '<br>', '4/2/2020 11:09:20 am', '4/2/2020 11:09:20 am', '4/2/2020 11:09:32 am', 'felipem', 'felipem', NULL, '2020-02-04 00:00:00', NULL, NULL, 13),
(41, 1, 218, 218, 218, 7, '2020-02-04', 9, 3, 3, 1, NULL, 1, '<br>', '4/2/2020 11:09:36 am', '4/2/2020 11:09:36 am', '4/2/2020 11:09:43 am', 'felipem', 'felipem', NULL, '2020-02-04 00:00:00', NULL, NULL, 13),
(42, 1, 219, 219, 219, 7, '2020-02-04', 9, 3, 3, 1, NULL, 1, '<br>', '4/2/2020 11:09:47 am', '4/2/2020 11:09:47 am', '4/2/2020 11:10:06 am', 'felipem', 'felipem', NULL, '2020-02-04 00:00:00', NULL, NULL, 13),
(43, 1, 220, 220, 220, 7, '2020-02-04', 9, 3, 3, 1, NULL, 1, '<br>', '4/2/2020 11:10:08 am', '4/2/2020 11:10:08 am', '4/2/2020 11:10:29 am', 'felipem', 'felipem', NULL, '2020-02-04 00:00:00', NULL, NULL, 13),
(44, 1, 223, 223, 223, 11, '2020-02-19', 1, 3, 3, 1, NULL, 1, '<br>', '19/2/2020 12:10:00 pm', '19/2/2020 12:10:00 pm', NULL, 'felipem', NULL, NULL, '2020-02-19 00:00:00', NULL, NULL, NULL),
(45, 1, 44, 44, 44, 7, '2020-02-20', 7, 3, 3, 1, 44, 2, 'Equipo sacado de FHSJ', '20/2/2020 03:52:45 pm', '20/2/2020 03:52:45 pm', NULL, 'felipem', NULL, NULL, '2020-02-20 00:00:00', NULL, NULL, NULL),
(46, 1, 226, 226, 226, 1, '2020-02-29', 1, 5, 5, 1, NULL, 1, 'El equipo era el DG 407 y se le otorga el DG 411, CAMILO', '29/2/2020 10:21:28 am', '29/2/2020 10:21:28 am', NULL, 'camiloz', NULL, NULL, '2020-02-29 00:00:00', NULL, NULL, 17),
(47, 2, 226, 226, 226, 1, '2020-02-29', 1, 5, 5, 2, 226, 2, 'Equipo en funcionamiento normal', '29/2/2020 10:22:53 am', '29/2/2020 10:22:53 am', NULL, 'camiloz', NULL, NULL, '2020-02-29 00:00:00', NULL, NULL, 17),
(48, 1, 126, 126, 126, 1, '2020-02-29', 1, 5, 5, 2, 126, 2, 'SE MODIFICARÁ EL DIGITALIZADOR DEL HMC', '29/2/2020 10:29:30 am', '29/2/2020 10:29:30 am', NULL, 'camiloz', NULL, NULL, '2020-02-29 00:00:00', NULL, NULL, 17),
(49, 1, 227, 227, 227, 2, '2020-02-29', 2, 5, 5, 2, 227, 2, 'PENDIENTE POR CAMBIAR CÓDIGO DEL 401 AL 412', '29/2/2020 10:33:35 am', '29/2/2020 10:33:35 am', '29/2/2020 10:38:27 am', 'camiloz', 'camiloz', NULL, '2020-02-29 00:00:00', NULL, NULL, 17),
(50, 1, 120, 120, 120, 1, '2020-02-29', 1, 5, 5, 2, 120, 2, 'El transductor funciona normalmente en el HIDC', '29/2/2020 11:37:55 am', '29/2/2020 11:37:55 am', '29/2/2020 11:12:19 pm', 'camiloz', 'camiloz', NULL, '2020-02-29 00:00:00', NULL, NULL, NULL),
(51, 1, 165, 165, 165, 1, '2020-02-29', 1, 5, 5, 2, 165, 2, 'La trotadora funciona normalmente en el HIDC', '29/2/2020 11:42:06 am', '29/2/2020 11:42:06 am', '29/2/2020 11:42:15 am', 'camiloz', 'camiloz', NULL, '2020-02-29 00:00:00', NULL, NULL, 3),
(52, 1, 172, 172, 172, 1, '2020-02-29', 1, 5, 5, 2, 172, 2, 'El dispositivo se encuentra en funcionamiento normal en el HIDC', '29/2/2020 11:49:41 am', '29/2/2020 11:49:41 am', NULL, 'camiloz', NULL, NULL, '2020-02-29 00:00:00', NULL, NULL, 59),
(53, 1, 124, 124, 124, 1, '2020-02-29', 1, 5, 5, 2, 124, 2, 'Dispositivo funcionando normalmente en el HIDC', '29/2/2020 11:59:45 am', '29/2/2020 11:59:45 am', NULL, 'camiloz', NULL, NULL, '2020-02-29 00:00:00', NULL, NULL, 59),
(54, 1, 80, 80, 80, 1, '2020-02-29', 7, 5, 5, 1, 80, 2, 'El equipo sale de la unidad por motivo de revisión y mantenimiento, Camilo.', '29/2/2020 08:00:40 pm', '29/2/2020 08:00:40 pm', '29/2/2020 09:27:28 pm', 'camiloz', 'camiloz', NULL, '2020-02-29 00:00:00', NULL, NULL, 0),
(59, 1, 49, 49, 49, 1, '2020-02-29', 1, 5, 5, 2, 49, 2, 'El equipo funciona normalmente', '29/2/2020 09:31:05 pm', '29/2/2020 09:31:05 pm', '29/2/2020 09:31:09 pm', 'camiloz', 'camiloz', NULL, '2020-02-29 00:00:00', NULL, NULL, 0),
(56, 1, 87, 87, 87, 1, '2020-02-29', 1, 5, 5, 2, 87, 2, 'El dispositivo funciona normalmente', '29/2/2020 08:12:15 pm', '29/2/2020 08:12:15 pm', NULL, 'camiloz', NULL, NULL, '2020-02-29 00:00:00', NULL, NULL, 0),
(60, 1, 42, 42, 42, 2, '2020-02-26', 2, 5, 5, 2, 42, 2, '<br>', '2/3/2020 03:23:12 pm', '2/3/2020 03:23:12 pm', '2/3/2020 03:23:24 pm', 'camiloz', 'camiloz', NULL, '2020-03-02 00:00:00', NULL, NULL, 0),
(61, 1, 138, 138, 138, 2, '2020-02-26', 2, 5, 5, 2, 138, 2, '<br>', '2/3/2020 03:28:22 pm', '2/3/2020 03:28:22 pm', '2/3/2020 03:28:36 pm', 'camiloz', 'camiloz', NULL, '2020-03-02 00:00:00', NULL, NULL, 0),
(62, 1, 163, 163, 163, 2, '2020-02-26', 2, 5, 5, 2, 163, 2, '<br>', '2/3/2020 03:45:44 pm', '2/3/2020 03:45:44 pm', NULL, 'camiloz', NULL, NULL, '2020-03-02 00:00:00', NULL, NULL, 3),
(63, 1, 170, 170, 170, 2, '2020-02-26', 2, 5, 5, 2, 170, 2, '<br>', '2/3/2020 03:52:52 pm', '2/3/2020 03:52:52 pm', '2/3/2020 03:53:08 pm', 'camiloz', 'camiloz', NULL, '2020-03-02 00:00:00', NULL, NULL, 59),
(64, 1, 130, 130, 130, 5, '2020-03-02', 3, 5, 5, 1, 130, 1, 'Pendiente ubicar el digitalizador que salió de la Clínica San Francisco por reparación', '2/3/2020 08:51:05 pm', '2/3/2020 08:51:05 pm', '2/3/2020 09:03:11 pm', 'camiloz', 'camiloz', NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(65, 1, 130, 130, 130, 5, '2020-03-02', 3, 5, 5, 1, 130, 2, NULL, '2/3/2020 08:59:07 pm', '2/3/2020 08:59:07 pm', NULL, 'camiloz', NULL, NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(66, 1, 51, 51, 51, 7, '2020-03-02', 3, 5, 5, 1, 51, 1, '<br>', '2/3/2020 09:09:32 pm', '2/3/2020 09:09:32 pm', NULL, 'camiloz', NULL, NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(67, 1, 51, 51, 51, 7, '2020-03-02', 3, 5, 5, 1, 51, 2, '<br>', '2/3/2020 09:11:49 pm', '2/3/2020 09:11:49 pm', NULL, 'camiloz', NULL, NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(68, 1, 34, 34, 34, 7, '2020-03-02', 3, 5, 5, 1, 34, 1, '<br>', '2/3/2020 09:17:40 pm', '2/3/2020 09:17:40 pm', NULL, 'camiloz', NULL, NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(69, 1, 34, 34, 34, 7, '2020-03-02', 3, 5, 5, 1, 34, 2, '<br>', '2/3/2020 09:19:33 pm', '2/3/2020 09:19:33 pm', '2/3/2020 09:19:43 pm', 'camiloz', 'camiloz', NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(70, 1, 131, 131, 131, 4, '2020-03-02', 3, 5, 5, 1, 131, 1, '<br>', '2/3/2020 09:29:42 pm', '2/3/2020 09:29:42 pm', NULL, 'camiloz', NULL, NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(71, 1, 131, 131, 131, 4, '2020-03-02', 3, 5, 5, 1, 131, 2, '<br>', '2/3/2020 09:33:51 pm', '2/3/2020 09:33:51 pm', '2/3/2020 09:34:50 pm', 'camiloz', 'camiloz', NULL, '2020-03-02 00:00:00', NULL, NULL, NULL),
(72, 1, 108, 108, 108, 7, '2020-03-03', 7, 5, 5, 2, 108, 2, 'Se encuentra en la Reforma', '3/3/2020 04:39:07 pm', '3/3/2020 04:39:07 pm', NULL, 'camiloz', NULL, NULL, '2020-03-03 00:00:00', NULL, NULL, NULL),
(73, 1, 101, 101, 101, 7, '2020-03-03', 7, 5, 5, 2, 101, 2, '<br>', '3/3/2020 05:16:28 pm', '3/3/2020 05:16:28 pm', NULL, 'camiloz', NULL, NULL, '2020-03-03 00:00:00', NULL, NULL, NULL),
(74, 1, 119, 119, 119, 1, '2020-02-29', 1, 5, 5, 2, 119, 2, 'Esta en el HIDC', '3/3/2020 09:50:10 pm', '3/3/2020 09:50:10 pm', NULL, 'camiloz', NULL, NULL, '2020-03-03 00:00:00', NULL, NULL, NULL),
(75, 1, 119, 119, 119, 1, '2020-03-04', 2, 5, 5, 1, 119, 2, 'El Doctor Ruiz lleva el transductor del HIDC al HMC', '3/3/2020 09:53:26 pm', '3/3/2020 09:53:26 pm', '3/3/2020 09:53:34 pm', 'camiloz', 'camiloz', NULL, '2020-03-03 00:00:00', NULL, NULL, NULL),
(76, 1, 162, 162, 162, 3, '2020-03-04', 3, 5, 5, 2, 162, 2, 'Funciona normalmente en el HROB', '4/3/2020 09:48:31 am', '4/3/2020 09:48:31 am', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(77, 1, 104, 104, 104, 3, '2020-02-26', 3, 5, 5, 2, 104, 2, '<br>', '4/3/2020 09:50:55 am', '4/3/2020 09:50:55 am', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(78, 1, 136, 136, 136, 3, '2020-03-04', 3, 5, 5, 2, 136, 2, '<br>', '4/3/2020 10:06:41 am', '4/3/2020 10:06:41 am', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, 193661),
(79, 1, 156, 156, 156, 3, '2020-03-04', 3, 5, 5, 2, 156, 2, '<br>', '4/3/2020 10:18:03 am', '4/3/2020 10:18:03 am', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(80, 1, 130, 130, 130, 5, '2020-03-04', 3, 3, 3, 2, 130, 2, 'Se verifica en HROB&nbsp;', '4/3/2020 11:13:19 am', '4/3/2020 11:13:19 am', NULL, 'felipem', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(81, 1, 14, 14, 14, 6, '2020-03-04', 6, 5, 5, 2, 14, 2, '<br>', '4/3/2020 06:12:09 pm', '4/3/2020 06:12:09 pm', '4/3/2020 06:12:12 pm', 'camiloz', 'camiloz', NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(82, 1, 16, 16, 16, 6, '2020-03-04', 6, 5, 5, 2, 16, 2, '<br>', '4/3/2020 06:16:39 pm', '4/3/2020 06:16:39 pm', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, 13),
(83, 1, 10, 10, 10, 6, '2020-03-04', 6, 5, 5, 2, 10, 2, '<br>', '4/3/2020 06:19:39 pm', '4/3/2020 06:19:39 pm', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(84, 1, 23, 23, 23, 1, '2020-03-04', 6, 5, 5, 1, 23, 1, '<br>', '4/3/2020 06:29:34 pm', '4/3/2020 06:29:34 pm', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, 13),
(85, 2, 23, 23, 23, 1, '2020-03-04', 6, 5, 5, 2, 23, 2, '<br>', '4/3/2020 06:30:54 pm', '4/3/2020 06:30:54 pm', NULL, 'camiloz', NULL, NULL, '2020-03-04 00:00:00', NULL, NULL, 13),
(86, 1, 1, 1, 1, 6, '2020-03-04', 6, 5, 5, 2, 1, 2, '<br>', '4/3/2020 06:35:01 pm', '4/3/2020 06:35:01 pm', '4/3/2020 06:35:04 pm', 'camiloz', 'camiloz', NULL, '2020-03-04 00:00:00', NULL, NULL, 13),
(87, 1, 98, 98, 98, 6, '2020-03-04', 6, 5, 5, 2, 98, 2, '<br>', '4/3/2020 06:37:40 pm', '4/3/2020 06:37:40 pm', '4/3/2020 06:37:43 pm', 'camiloz', 'camiloz', NULL, '2020-03-04 00:00:00', NULL, NULL, 0),
(175, 0, 96, 96, 96, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 10:49:06', '2021-01-25 10:52:31', NULL, 0),
(176, 0, 96, 96, 96, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 10:53:38', '2021-01-25 11:08:22', NULL, 0),
(89, 1, 46, 46, 46, 6, '2020-03-04', 6, 5, 5, 2, 46, 2, '<br>', '4/3/2020 06:45:26 pm', '4/3/2020 06:45:26 pm', '4/3/2020 06:45:29 pm', 'camiloz', 'camiloz', NULL, '2020-03-04 00:00:00', NULL, NULL, NULL),
(90, 1, 36, 36, 36, 4, '2020-03-05', 7, 5, 5, 1, 36, 1, '<br>', '5/3/2020 10:11:21 pm', '5/3/2020 10:11:21 pm', NULL, 'camiloz', NULL, NULL, '2020-03-05 00:00:00', NULL, NULL, NULL),
(91, 1, 36, 36, 36, 4, '2020-03-05', 7, 5, 5, 1, 36, 2, '<br>', '5/3/2020 10:12:51 pm', '5/3/2020 10:12:51 pm', '5/3/2020 10:13:06 pm', 'camiloz', 'camiloz', NULL, '2020-03-05 00:00:00', NULL, NULL, NULL),
(92, 1, 52, 52, 52, 7, '2020-03-05', 7, 5, 5, 2, 52, 2, '<br>', '5/3/2020 10:17:47 pm', '5/3/2020 10:17:47 pm', NULL, 'camiloz', NULL, NULL, '2020-03-05 00:00:00', NULL, NULL, NULL),
(93, 1, 230, 230, 230, 4, '2020-03-06', 4, 5, 5, 2, 230, 2, '<br>', '6/3/2020 08:59:09 am', '6/3/2020 08:59:09 am', '6/3/2020 09:03:52 am', 'camiloz', 'camiloz', NULL, '2020-03-06 00:00:00', NULL, NULL, 3),
(94, 1, 140, 140, 140, 4, '2020-03-06', 4, 5, 5, 2, 140, 2, '<br>', '6/3/2020 09:17:51 am', '6/3/2020 09:17:51 am', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 193661),
(95, 1, 25, 25, 25, 4, '2020-03-06', 4, 5, 5, 2, 25, 2, '<br>', '6/3/2020 09:31:55 am', '6/3/2020 09:31:55 am', '6/3/2020 09:32:39 am', 'camiloz', 'camiloz', NULL, '2020-03-06 00:00:00', NULL, NULL, 13),
(96, 1, 22, 22, 22, 4, '2020-03-06', 4, 5, 5, 2, 22, 2, '<br>', '6/3/2020 09:40:29 am', '6/3/2020 09:40:29 am', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 13),
(97, 1, 129, 129, 129, 4, '2020-03-06', 4, 5, 5, 2, 129, 2, '<br>', '6/3/2020 09:56:31 am', '6/3/2020 09:56:31 am', '6/3/2020 09:56:49 am', 'camiloz', 'camiloz', NULL, '2020-03-06 00:00:00', NULL, NULL, 17),
(98, 1, 225, 225, 225, 5, '2020-02-26', 5, 5, 5, 2, 225, 2, '<br>', '6/3/2020 09:59:39 am', '6/3/2020 09:59:39 am', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(99, 1, 221, 221, 221, 4, '2020-03-06', 4, 5, 5, 2, 221, 2, '<br>', '6/3/2020 10:09:15 am', '6/3/2020 10:09:15 am', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 17),
(100, 1, 111, 111, 111, 4, '2020-03-06', 4, 5, 5, 2, 111, 2, '<br>', '6/3/2020 11:39:28 am', '6/3/2020 11:39:28 am', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(101, 1, 232, 232, 232, 4, '2020-03-06', 4, 5, 5, 2, 232, 2, '<br>', '6/3/2020 12:16:35 pm', '6/3/2020 12:16:35 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 615291),
(102, 1, 234, 234, 234, 4, '2020-03-06', 4, 5, 5, 2, 234, 2, '<br>', '6/3/2020 12:54:10 pm', '6/3/2020 12:54:10 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 0),
(103, 1, 21, 21, 21, 4, '2020-03-06', 4, 5, 5, 2, 21, 2, '<br>', '6/3/2020 01:12:18 pm', '6/3/2020 01:12:18 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 13),
(104, 1, 209, 209, 209, 7, '2020-03-06', 4, 5, 5, 1, 209, 1, '<br>', '6/3/2020 02:14:28 pm', '6/3/2020 02:14:28 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 0),
(105, 1, 209, 209, 209, 7, '2020-03-06', 4, 5, 5, 1, 209, 2, '<br>', '6/3/2020 02:15:49 pm', '6/3/2020 02:15:49 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, 0),
(106, 1, 67, 67, 67, 4, '2020-03-06', 4, 5, 5, 2, 67, 2, '<br>', '6/3/2020 02:20:52 pm', '6/3/2020 02:20:52 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(107, 1, 53, 53, 53, 7, '2020-03-06', 5, 5, 5, 1, 53, 1, '<br>', '6/3/2020 03:20:34 pm', '6/3/2020 03:20:34 pm', '6/3/2020 03:21:00 pm', 'camiloz', 'camiloz', NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(108, 1, 53, 53, 53, 7, '2020-03-06', 5, 5, 5, 1, 53, 2, '<br>', '6/3/2020 03:22:16 pm', '6/3/2020 03:22:16 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(109, 1, 84, 84, 84, 5, '2020-03-06', 5, 5, 5, 2, 84, 2, '<br>', '6/3/2020 03:32:19 pm', '6/3/2020 03:32:19 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(110, 1, 27, 27, 27, 5, '2020-03-06', 5, 5, 5, 2, 27, 2, '<br>', '6/3/2020 03:57:35 pm', '6/3/2020 03:57:35 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(111, 1, 28, 28, 28, 5, '2020-03-06', 5, 5, 5, 2, 28, 2, '<br>', '6/3/2020 04:07:42 pm', '6/3/2020 04:07:42 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(112, 1, 31, 31, 31, 7, '2020-03-06', 5, 5, 5, 1, 31, 1, '<br>', '6/3/2020 04:13:21 pm', '6/3/2020 04:13:21 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(113, 1, 31, 31, 31, 7, '2020-03-06', 5, 5, 5, 1, 31, 2, '<br>', '6/3/2020 04:15:01 pm', '6/3/2020 04:15:01 pm', NULL, 'camiloz', NULL, NULL, '2020-03-06 00:00:00', NULL, NULL, NULL),
(114, 1, 158, 158, 158, 10, '2019-02-21', 1, 3, 3, 1, 158, 1, '<br>', '11/3/2020 01:40:51 pm', '11/3/2020 01:40:51 pm', NULL, 'camiloz', NULL, NULL, '2020-03-11 00:00:00', NULL, NULL, NULL),
(115, 1, 158, 158, 158, 10, '2020-02-21', 1, 3, 3, 1, 158, 2, '<br>', '11/3/2020 01:42:20 pm', '11/3/2020 01:42:20 pm', NULL, 'camiloz', NULL, NULL, '2020-03-11 00:00:00', NULL, NULL, NULL),
(116, 1, 166, 166, 166, 5, '2020-03-06', 5, 5, 5, 1, 166, 2, '<br>', '16/3/2020 01:11:47 pm', '16/3/2020 01:11:47 pm', '16/3/2020 01:11:53 pm', 'camiloz', 'camiloz', NULL, '2020-03-16 00:00:00', NULL, NULL, NULL),
(117, 1, 34, 34, 34, 3, '2020-03-17', 6, 5, 5, 1, 34, 1, '<br>', '17/3/2020 01:45:33 pm', '17/3/2020 01:45:33 pm', '17/3/2020 01:45:55 pm', 'camiloz', 'camiloz', 'IMG_20200317_124457.jpg', '2020-03-17 00:00:00', NULL, NULL, NULL),
(118, 1, 123, 123, 123, 6, '2020-03-17', 6, 5, 5, 2, 123, 2, '<br>', '17/3/2020 04:42:27 pm', '17/3/2020 04:42:27 pm', '17/3/2020 04:42:34 pm', 'camiloz', 'camiloz', NULL, '2020-03-17 00:00:00', NULL, NULL, NULL),
(119, 1, 159, 159, 159, 6, '2020-03-17', 6, 5, 5, 2, 159, 2, '<br>', '17/3/2020 04:50:36 pm', '17/3/2020 04:50:36 pm', NULL, 'camiloz', NULL, NULL, '2020-03-17 00:00:00', NULL, NULL, 0),
(120, 1, 19, 19, 19, 6, '2020-03-17', 6, 5, 5, 2, 19, 2, '<br>', '17/3/2020 06:03:58 pm', '17/3/2020 06:03:58 pm', '17/3/2020 06:04:02 pm', 'camiloz', 'camiloz', NULL, '2020-03-17 00:00:00', NULL, NULL, NULL),
(121, 1, 70, 70, 70, 6, '2020-03-17', 6, 5, 5, 2, 70, 2, '<br>', '17/3/2020 06:15:14 pm', '17/3/2020 06:15:14 pm', '17/3/2020 06:15:20 pm', 'camiloz', 'camiloz', NULL, '2020-03-17 00:00:00', NULL, NULL, NULL),
(122, 1, 72, 72, 72, 6, '2020-03-17', 6, 5, 5, 2, 72, 2, '<br>', '17/3/2020 06:17:33 pm', '17/3/2020 06:17:33 pm', NULL, 'camiloz', NULL, NULL, '2020-03-17 00:00:00', NULL, NULL, 0),
(123, 1, 48, 48, 48, 6, '2020-03-04', 6, 5, 5, 2, 48, 2, '<br>', '17/3/2020 06:32:40 pm', '17/3/2020 06:32:40 pm', NULL, 'camiloz', NULL, NULL, '2020-03-17 00:00:00', NULL, NULL, 0),
(124, 1, 34, 34, 34, 3, '2020-03-17', 6, 5, 5, 1, 34, 2, '<br>', '17/3/2020 06:35:47 pm', '17/3/2020 06:35:47 pm', NULL, 'camiloz', NULL, NULL, '2020-03-17 00:00:00', NULL, NULL, NULL),
(125, 1, 131, 131, 131, 3, '2020-03-17', 6, 5, 5, 1, 131, 1, '<span style=\"font-size: 11.998px;\">Se envió junto con el Ecógrafo ECO 204.</span><br>', '21/3/2020 12:51:43 pm', '21/3/2020 12:51:43 pm', '21/3/2020 12:55:59 pm', 'camiloz', 'camiloz', NULL, '2020-03-21 00:00:00', NULL, NULL, NULL),
(126, 1, 131, 131, 131, 3, '2020-03-17', 6, 5, 5, 1, 131, 2, 'Se envió junto con el Ecógrafo ECO 204.', '21/3/2020 12:53:23 pm', '21/3/2020 12:53:23 pm', '21/3/2020 12:55:43 pm', 'camiloz', 'camiloz', NULL, '2020-03-21 00:00:00', NULL, NULL, NULL),
(127, 1, 235, 235, 235, 2, '2020-03-21', 2, 5, 5, 2, 235, 2, '<br>', '21/3/2020 01:16:16 pm', '21/3/2020 01:16:16 pm', '21/3/2020 01:16:54 pm', 'camiloz', 'camiloz', NULL, '2020-03-21 00:00:00', NULL, NULL, 3),
(128, 1, 40, 40, 40, 4, '2020-03-21', 4, 5, 5, 2, 40, 2, '<br>', '21/3/2020 01:25:54 pm', '21/3/2020 01:25:54 pm', NULL, 'camiloz', NULL, NULL, '2020-03-21 00:00:00', NULL, NULL, NULL),
(129, 1, 9, 9, 9, 6, '2020-05-06', 7, 3, 3, 1, 9, 2, 'Se verifica en HIDC pero no se encuentra, no existe reporte de traslado ni correo de por medio. Hoy se traslada a bodega con el fin de encontrar su ubicación.&nbsp;', '6/5/2020 06:02:37 pm', '6/5/2020 06:02:37 pm', NULL, 'felipem', NULL, NULL, '2020-05-06 00:00:00', NULL, NULL, 13),
(130, 1, 56, 56, 56, 1, '2020-05-06', 12, 3, 3, 1, 56, 2, 'ENTREGADO A SOL PARA SU TRABAJO DE PROGRAMACION', '6/5/2020 06:06:59 pm', '6/5/2020 06:06:59 pm', '6/5/2020 06:07:02 pm', 'felipem', 'felipem', NULL, '2020-05-06 00:00:00', NULL, NULL, NULL),
(131, 1, 119, 119, 119, 1, '2020-05-06', 7, 3, 3, 1, 119, 2, 'EN ESPERA DE ACTUALIZACIÓN DE ESTADO Y UBICACIÓN&nbsp;', '6/5/2020 06:08:48 pm', '6/5/2020 06:08:48 pm', NULL, 'felipem', NULL, NULL, '2020-05-06 00:00:00', NULL, NULL, NULL),
(132, 1, 236, 236, 236, 7, '2020-05-06', 1, 3, 3, 1, 236, 2, 'REGISTRADO POR CORREO ELECTRÓNICO&nbsp;', '6/5/2020 06:20:23 pm', '6/5/2020 06:20:23 pm', NULL, 'felipem', NULL, NULL, '2020-05-06 00:00:00', NULL, NULL, 0),
(133, 1, 120, 120, 120, 1, '2020-05-06', 1, 3, 3, 1, 120, 2, 'EN ESPERA DE VERIFICACIÓN PORQUE EL EQUIPO AL PARECER SE ENCUENTRA EN LA SALA DE COVID', '6/5/2020 06:24:37 pm', '6/5/2020 06:24:37 pm', '6/5/2020 06:26:10 pm', 'felipem', 'felipem', NULL, '2020-05-06 00:00:00', NULL, NULL, NULL),
(134, 1, 237, 237, 237, 7, '2020-05-06', 1, 3, 3, 1, 237, 2, 'VERIFICADO POR CORREO ELECTRÓNICO EN HIDC', '7/5/2020 11:58:34 am', '7/5/2020 11:58:34 am', NULL, 'felipem', NULL, NULL, '2020-05-07 00:00:00', NULL, NULL, 0),
(135, 1, 105, 105, 105, 7, '2020-05-07', 1, 3, 3, 1, 105, 2, 'VERIFICADO POR CORREO', '7/5/2020 12:01:39 pm', '7/5/2020 12:01:39 pm', NULL, 'felipem', NULL, NULL, '2020-05-07 00:00:00', NULL, NULL, NULL),
(136, 1, 139, 139, 139, 7, '2020-07-07', 7, 5, 5, 1, NULL, 0, 'Se envìa a reparaciòn.', NULL, NULL, '7/7/2020 07:21:16 pm', 'camiloz', 'camiloz', NULL, '0000-00-00 00:00:00', '2020-07-07 19:21:16', NULL, 8),
(137, 1, 134, 134, 134, 7, '2020-07-07', 7, 5, 5, 1, NULL, 0, 'Se lleva a reparaciòn.', NULL, NULL, '7/7/2020 07:27:37 pm', 'camiloz', 'camiloz', NULL, '0000-00-00 00:00:00', '2020-07-07 19:27:37', NULL, NULL),
(138, 1, 26, 26, 26, 1, '2020-09-01', 13, 3, 3, 2, NULL, 0, 'Se envia a medisun esensa', NULL, NULL, NULL, 'felipem', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, 13),
(139, 1, 9, 9, 9, 1, '2020-09-10', 13, 3, 3, 1, NULL, 0, 'Yo envié Felipe&nbsp;', NULL, NULL, NULL, 'superadmin', NULL, '15997749791773665205687412259682.jpg', '0000-00-00 00:00:00', NULL, NULL, 13),
(140, 1, 244, 244, 244, 7, '2020-10-01', 6, 3, 3, 1, NULL, 0, 'Enviado con el Dr Huberth Ruiz', NULL, NULL, NULL, 'superadmin', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, 0),
(141, 1, 245, 245, 245, 7, '2020-10-01', 6, 3, 3, 1, NULL, 0, 'Enviado con el Dr Huberth Ruiz', NULL, NULL, NULL, 'superadmin', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL),
(142, 0, 58, 58, 58, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2020-11-13 14:57:12', NULL, NULL, 13),
(143, 0, 42, 42, 42, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-11-17 12:47:42', NULL, NULL, 0),
(144, 0, 54, 54, 54, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2020-11-17 15:27:59', NULL, NULL, 0),
(145, 0, 128, 128, 128, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2020-11-17 15:34:30', NULL, NULL, 17),
(146, 0, 111, 111, 111, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', 'felipem', NULL, '2020-11-17 15:40:41', '2020-11-17 15:40:53', NULL, NULL),
(148, 0, 83, 83, 83, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-11-19 14:39:12', '2020-11-19 14:51:42', NULL, NULL),
(149, 0, 206, 206, 206, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-11-19 15:03:35', '2020-11-19 15:08:02', NULL, NULL),
(150, 0, 5, 5, 5, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2020-11-20 08:51:07', NULL, NULL, 13),
(151, 0, 57, 57, 57, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-11-23 08:35:50', NULL, NULL, 13),
(152, 0, 557, 557, 557, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-11-23 09:56:43', '2020-11-23 10:02:07', NULL, NULL),
(153, 0, 558, 558, 558, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-11-23 10:45:35', '2020-11-23 10:51:03', NULL, NULL),
(154, 0, 126, 126, 126, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-11-24 12:31:51', '2020-11-24 12:38:00', NULL, 17),
(155, 0, 170, 170, 170, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-11-26 11:55:30', NULL, NULL, 59),
(156, 0, 585, 585, 585, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2020-12-02 17:10:06', NULL, NULL, 61),
(157, 0, 59, 59, 59, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-12-03 15:22:19', '2020-12-03 15:25:35', NULL, 13),
(158, 0, 2, 2, 2, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-12-03 15:26:33', '2020-12-03 15:31:15', NULL, 13),
(159, 0, 631, 631, 631, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-12-03 15:56:24', '2020-12-03 15:58:30', NULL, 631),
(160, 0, 679, 679, 679, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-12-03 16:01:37', NULL, NULL, 3),
(161, 0, 1006, 1006, 1006, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-12-03 16:07:33', '2020-12-03 16:09:10', NULL, 1006),
(162, 0, 318, 318, 318, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', 'andresl', NULL, '2020-12-03 16:12:07', '2020-12-03 16:14:45', NULL, 0),
(163, 0, 352, 352, 352, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-12-03 16:19:29', NULL, NULL, 0),
(164, 0, 566, 566, 566, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-12-04 09:35:47', NULL, NULL, 566),
(165, 0, 25, 25, 25, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2020-12-23 12:40:07', NULL, NULL, 13),
(166, 0, 654, 654, 654, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'andresl', NULL, NULL, '2020-12-29 14:41:55', NULL, NULL, 61),
(167, 0, 220, 220, 220, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-12 08:32:07', '2021-01-12 08:55:29', NULL, 13),
(168, 0, 220, 220, 220, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', NULL, NULL, '2021-01-12 08:33:10', NULL, NULL, 13),
(169, 0, 57, 57, 57, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-12 08:58:33', '2021-01-12 09:02:09', NULL, 13),
(170, 0, 57, 57, 57, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-12 09:02:48', '2021-01-12 09:12:38', NULL, 13),
(171, 0, 218, 218, 218, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 09:27:32', '2021-01-25 09:35:49', NULL, 13),
(172, 0, 209, 209, 209, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 09:38:21', '2021-01-25 09:42:44', NULL, 0),
(173, 0, 208, 208, 208, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 10:08:00', '2021-01-25 10:11:38', NULL, 0),
(174, 0, 96, 96, 96, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 10:39:45', '2021-01-25 10:43:33', NULL, 0),
(177, 0, 96, 96, 96, 0, '0000-00-00', 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', NULL, NULL, '2021-01-25 11:49:01', NULL, NULL, 0),
(178, 0, 97, 97, 97, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:02:29', '2021-01-25 13:09:24', NULL, 0),
(179, 0, 219, 219, 219, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:15:10', '2021-01-25 13:18:35', NULL, 13),
(180, 0, 217, 217, 217, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:20:10', '2021-01-25 13:24:14', NULL, 13),
(181, 0, 216, 216, 216, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:25:43', '2021-01-25 13:28:04', NULL, 13),
(182, 0, 13, 13, 13, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:36:42', '2021-01-25 13:39:28', NULL, 13),
(183, 0, 202, 202, 202, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:44:14', '2021-01-25 13:48:40', NULL, 13),
(184, 0, 200, 200, 200, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 13:51:05', '2021-01-25 13:53:04', NULL, 13),
(185, 0, 170, 170, 170, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'camiloz', 'camiloz', NULL, '2021-01-25 14:41:52', '2021-01-25 14:46:38', NULL, 59),
(186, 0, 24, 24, 24, 0, '0000-00-00', 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', NULL, NULL, '2021-02-01 11:25:01', NULL, NULL, 13),
(187, 0, 200, 200, 200, 0, '0000-00-00', 0, 0, NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', NULL, NULL, '2021-02-09 10:12:25', NULL, NULL, 13),
(188, 0, 23, 23, 23, 0, '0000-00-00', 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2021-02-12 13:41:49', NULL, NULL, 13),
(189, 0, 58, 58, 58, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', 'a.lopez', NULL, '2021-02-16 13:37:33', '2021-02-16 13:41:39', NULL, 13),
(190, 0, 23, 23, 23, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', NULL, NULL, '2021-02-19 09:47:22', NULL, NULL, 13),
(191, 0, 219, 219, 219, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'felipem', 'felipem', NULL, '2021-04-08 08:24:26', '2021-04-08 08:29:42', NULL, 13),
(192, 0, 138, 138, 138, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', 'a.lopez', NULL, '2021-04-08 15:35:18', '2021-04-08 15:38:49', NULL, 0),
(193, 0, 138, 138, 138, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', 'a.lopez', NULL, '2021-04-09 13:59:29', '2021-04-09 14:03:54', NULL, 0),
(194, 0, 17, 17, 17, 0, '0000-00-00', 0, 0, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, 'l.solarte', NULL, NULL, '2021-04-12 10:52:05', NULL, NULL, 13),
(195, 0, 17, 17, 17, 0, '0000-00-00', 0, 0, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, 'l.solarte', NULL, NULL, '2021-04-12 10:52:14', NULL, NULL, 13),
(196, 0, 17, 17, 17, 0, '0000-00-00', 0, 0, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, 'l.solarte', NULL, NULL, '2021-04-12 10:52:39', NULL, NULL, 13),
(197, 0, 17, 17, 17, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', 'a.lopez', NULL, '2021-04-12 10:54:29', '2021-04-12 10:58:19', NULL, 13),
(198, 0, 59, 59, 59, 0, '0000-00-00', 0, 0, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, 'l.solarte', NULL, NULL, '2021-04-12 10:59:34', NULL, NULL, 13),
(199, 0, 59, 59, 59, 0, '0000-00-00', 0, 0, NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, 'l.solarte', NULL, NULL, '2021-04-12 10:59:43', NULL, NULL, 13),
(200, 0, 59, 59, 59, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', 'a.lopez', NULL, '2021-04-12 11:00:38', '2021-04-12 11:05:30', NULL, 13),
(201, 0, 93, 93, 93, 0, '0000-00-00', 0, 0, NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, 'a.lopez', 'a.lopez', NULL, '2021-04-12 16:39:08', '2021-04-12 16:49:40', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movi_envio`
--

CREATE TABLE `movi_envio` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_movi` int(10) UNSIGNED DEFAULT NULL,
  `fecha_envio` date NOT NULL,
  `origen` int(10) UNSIGNED DEFAULT NULL,
  `o_direccion` int(10) UNSIGNED DEFAULT NULL,
  `destino` varchar(100) NOT NULL,
  `ciudad` int(10) UNSIGNED NOT NULL,
  `d_direccion` text,
  `nombre` int(10) UNSIGNED NOT NULL,
  `telefono` int(10) UNSIGNED DEFAULT NULL,
  `detalles` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL,
  `ubicacion` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `movi_envio`
--

INSERT INTO `movi_envio` (`id`, `id_movi`, `fecha_envio`, `origen`, `o_direccion`, `destino`, `ciudad`, `d_direccion`, `nombre`, `telefono`, `detalles`, `creado`, `creado_por`, `editado`, `editado_por`, `ubicacion`) VALUES
(1, 142, '2020-11-13', 14, 14, 'Esensa', 1, 'Esensa medisun', 3, 3, '<br>', '2020-11-13 14:58:11', 'felipem', NULL, NULL, NULL),
(2, 143, '2020-11-16', 7, 7, 'Hospital mario correa', 1, 'Hmc', 8, 8, '<br>', '2020-11-17 12:51:56', 'andresl', NULL, NULL, NULL),
(3, 144, '2020-11-17', 7, 7, 'VIVA1A', 1, 'VIVA1A PROFAMILIA', 8, 8, '<br>', '2020-11-17 15:28:40', 'felipem', NULL, NULL, NULL),
(4, 148, '2020-11-19', 7, 7, 'bodega', 1, 'VIVA - 1A', 8, 8, 'se completo el proceso, el equipo se encuentra en buen estado&nbsp;<div>serial N° 17030100036</div><div>&nbsp; MAPA 574</div>', '2020-11-19 14:49:51', 'andresl', NULL, NULL, NULL),
(5, 149, '2020-11-19', 7, 7, 'BODEGA', 1, 'VIVA - 1A', 8, 8, '<br>', '2020-11-19 15:04:57', 'andresl', NULL, NULL, NULL),
(6, 150, '2020-11-20', 7, 7, 'HROB', 4, 'HROB', 3, 3, '<br>', '2020-11-20 08:52:12', 'felipem', NULL, NULL, NULL),
(7, 151, '2020-11-23', 9, 9, 'Viva -1A', 1, 'Mario correa', 8, 8, '<br>', '2020-11-23 08:38:18', 'andresl', NULL, NULL, NULL),
(8, 152, '2020-11-23', 7, 7, 'Mario correa', 1, 'Mario correa', 8, 8, '<br>', '2020-11-23 09:58:41', 'andresl', '2020-11-23 10:01:31', 'andresl', NULL),
(9, 153, '2020-11-23', 7, 7, 'Mario correa', 1, 'Mario correa', 8, 8, '<br>', '2020-11-23 10:46:36', 'andresl', NULL, NULL, NULL),
(10, 154, '2020-11-24', 1, 1, 'HMC', 1, 'HOSPITAL MARIO CORREA', 8, 8, '<br>', '2020-11-24 12:37:10', 'andresl', NULL, NULL, NULL),
(11, 155, '2020-11-26', 2, 2, 'HMC', 1, 'VIVA-1A', 8, 8, '<br>', '2020-11-26 12:04:07', 'andresl', NULL, NULL, NULL),
(12, 156, '2020-12-02', 7, 7, 'HTU', 3, 'HTU TULUA', 3, 3, 'Se envía con monitor teclado y mouse', '2020-12-02 17:11:36', 'felipem', NULL, NULL, NULL),
(13, 157, '2020-12-03', 7, 7, 'MEDISUN ESENSA', 1, 'MEDISUN ESENSA', 8, 8, 'se envio completo', '2020-12-03 15:23:37', 'andresl', NULL, NULL, NULL),
(14, 158, '2020-12-03', 7, 7, 'Medisun Esensa', 1, 'MEDISUN ESENSA', 8, 8, 'SE ENVIO COMPLETO POR EL  INGENIERO SOLARTE', '2020-12-03 15:28:38', 'andresl', NULL, NULL, NULL),
(15, 159, '2020-12-03', 7, 7, 'MEDISUN ESENSA', 1, 'MEDISUN ESENSA', 8, 8, NULL, '2020-12-03 15:57:09', 'andresl', NULL, NULL, NULL),
(16, 160, '2020-12-03', 7, 7, 'MEDISUN ESENSA', 1, 'MEDISUN ESENSA', 8, 8, NULL, '2020-12-03 16:02:17', 'andresl', NULL, NULL, NULL),
(17, 161, '2020-12-03', 7, 7, 'MEDISUN ESENSA', 1, 'MEDISUN ESENSA', 8, 8, NULL, '2020-12-03 16:08:07', 'andresl', NULL, NULL, NULL),
(18, 162, '2020-12-03', 7, 7, 'MEDISUN ESENSA', 1, 'MEDISUN ESENSA', 8, 8, NULL, '2020-12-03 16:12:46', 'andresl', NULL, NULL, NULL),
(19, 163, '2020-12-03', 7, 7, 'MEDISUN ESENSA', 1, 'MEDISUN ESENSA', 8, 8, NULL, '2020-12-03 16:20:04', 'andresl', NULL, NULL, NULL),
(20, 164, '2020-12-04', 7, 7, 'San José', 2, 'Hospital San José', 8, 8, 'El equipo se encontraba en el lugar', '2020-12-04 09:38:11', 'andresl', NULL, NULL, NULL),
(21, 89, '2020-12-04', 6, 6, 'HROB PALMIRA', 4, 'CARRERA 29  39-51', 10, 10, 'SE REALIZA TRASLADO POR ORDEN VERBAL DEL DR RUIZ', '2020-12-04 17:33:39', 'lgarcia', NULL, NULL, NULL),
(22, 165, '2020-12-23', 4, 4, 'SEDE SAN FERNANDO', 1, 'ESTO ES UNA PRUEBA', 3, 3, 'LO ENVIO CON TODO, ESTO ES UNA PRUEBA', '2020-12-23 12:41:04', 'felipem', NULL, NULL, NULL),
(23, 166, '2020-12-28', 17, 17, 'HOSPITAL TOMAS URIBE', 3, 'HOSPITAL TOMAS URIBE', 8, 8, NULL, '2020-12-29 14:43:47', 'andresl', NULL, NULL, NULL),
(24, 168, '2021-01-08', 9, 9, 'BODEGA - LA REFORMA', 1, NULL, 5, 5, 'SE ENVIA HOLTER 144 CON DESTINO A LA REFORMA', '2021-01-12 08:39:25', 'camiloz', NULL, NULL, NULL),
(25, 167, '2021-01-08', 9, 9, 'BODEGA - LA REFORMA', 1, NULL, 5, 5, 'SE HACE ENVIO DE HOLTER 144', '2021-01-12 08:47:56', 'camiloz', NULL, NULL, NULL),
(26, 167, '2021-01-12', 7, 7, 'HOSPITAL ISAIAS DUARTE CANCINO', 1, 'HIDC', 5, 5, 'SE HACE ENTREGA DE HOLTER 144 A LA ENFERMERA IVETH TORO', '2021-01-12 08:50:59', 'camiloz', NULL, NULL, NULL),
(27, 169, '2021-01-12', 2, 2, 'BODEGA - LA REFORMA', 1, NULL, 5, 5, 'SE HACE ENVIO DE HOLTER 130', '2021-01-12 09:01:30', 'camiloz', NULL, NULL, NULL),
(28, 170, '2021-01-08', 2, 2, 'BODEGA - LA REFORMA', 1, NULL, 5, 5, 'SE HACE ENVÍO DE HOLTER 130', '2021-01-12 09:06:01', 'camiloz', NULL, NULL, NULL),
(29, 170, '2021-01-12', 7, 7, 'HOSPITAL ISAIAS DUARTE CANCINO', 1, 'HIDC', 5, 5, 'SE HACE ENVÍO DE HOLTER 130', '2021-01-12 09:09:26', 'camiloz', NULL, NULL, NULL),
(30, 171, '2021-01-24', 9, NULL, 'HOSPITAL ISAIAS DUARTE CANCINO', 1, NULL, 5, 5, 'SE ENVIA HOLTER142 CON IVETH TORO', '2021-01-25 09:33:11', 'camiloz', NULL, NULL, 9),
(31, 172, '2021-01-24', 4, NULL, 'HOSPITAL ISAIAS DUARTE CANCINO', 1, NULL, 5, 5, 'SE ENVIÓ MAPA 593, CON IVETH TORO', '2021-01-25 09:41:33', 'camiloz', NULL, NULL, 4),
(32, 173, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, 'HTU', 5, 5, 'SE ENTREGA A FELIPE MOLANO', '2021-01-25 10:10:19', 'camiloz', NULL, NULL, 4),
(33, 174, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 10:41:39', 'camiloz', NULL, NULL, 4),
(34, 175, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, 'SE ENVIA A OFICINA SEDE SAN FERNANDO', '2021-01-25 10:51:07', 'camiloz', NULL, NULL, 4),
(35, 176, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 10:54:51', 'camiloz', '2021-01-25 11:07:46', 'camiloz', 4),
(36, 178, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:06:17', 'camiloz', NULL, NULL, 4),
(37, 179, '2021-01-25', 9, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:17:28', 'camiloz', NULL, NULL, 9),
(38, 180, '2021-01-25', 9, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:22:24', 'camiloz', NULL, NULL, 9),
(39, 181, '2021-01-25', 9, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:26:47', 'camiloz', NULL, NULL, 9),
(40, 182, '2021-01-25', 2, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:38:26', 'camiloz', NULL, NULL, 2),
(41, 183, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:45:51', 'camiloz', NULL, NULL, 4),
(42, 184, '2021-01-25', 4, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 13:52:03', 'camiloz', NULL, NULL, 4),
(43, 185, '2021-01-25', 9, NULL, 'BODEGA - SAN FERNANDO', 1, NULL, 5, 5, NULL, '2021-01-25 14:43:54', 'camiloz', NULL, NULL, 9),
(44, 187, '2021-02-09', 7, NULL, 'BODEGA LA REFORMA', 1, 'BODEGA LA REFORMA', 8, 8, NULL, '2021-02-09 10:14:34', 'a.lopez', NULL, NULL, 7),
(45, 189, '2021-02-15', 9, NULL, 'BODEGA SAN FERNANDO', 1, 'BODEGA SAN FERNANDO', 8, 8, 'el equipos se encontraba en la unidad de medisun esensa', '2021-02-16 13:39:18', 'a.lopez', NULL, NULL, 9),
(46, 190, '2021-02-12', 13, NULL, 'SEDE SAN FERNANDO', 1, 'CRA 37 5B 2-78\r\nBODEGA SAN FERNANDO', 8, 8, 'se hizo traslado del equipo por revisión y mantenimiento del equipo por que se encontraba presentando fallas', '2021-02-19 09:50:17', 'felipem', '2021-02-19 09:51:23', 'felipem', 13),
(47, 191, '2021-04-09', 7, NULL, 'HOSPITAL TOMAS URIBE', 3, NULL, 5, 5, 'SE ENVIA CON DR HUBERTH RUIZ A TULUA, PARA UN AMIGO.', '2021-04-08 08:28:22', 'felipem', '2021-04-08 08:28:32', 'felipem', 7),
(48, NULL, '2021-04-09', 7, NULL, 'HOSPITAL TOMAS URIBE', 3, NULL, 5, 5, 'SE ENVIA CON DR HUBERTH RUIZ A TULUA, PARA UN AMIGO.', '2021-04-08 08:29:54', 'felipem', NULL, NULL, 7),
(49, 192, '2021-04-07', 7, NULL, 'Mario correa', 1, 'HOSPITAL MARIO CORREA', 8, 8, NULL, '2021-04-08 15:36:25', 'a.lopez', '2021-04-08 15:36:38', 'a.lopez', 7),
(50, 193, '2021-04-09', 2, NULL, 'Bodega San Fernando', 1, 'Bodega San Fernando', 8, 8, 'Se lleva por cambio de baterías con ING solarte', '2021-04-09 14:00:40', 'a.lopez', '2021-04-09 14:01:15', 'a.lopez', 2),
(51, 197, '2021-04-12', 1, NULL, 'Bodega San Fernando', 1, NULL, 8, 8, NULL, '2021-04-12 10:55:31', 'a.lopez', NULL, NULL, 1),
(52, 200, '2021-04-12', 13, NULL, 'ISAIAS DUARTE CANCINO', 1, NULL, 8, 8, NULL, '2021-04-12 11:02:40', 'a.lopez', NULL, NULL, 13),
(53, 201, '2021-04-12', 1, NULL, 'Bodega San Fernando', 1, NULL, 8, 8, NULL, '2021-04-12 16:41:34', 'a.lopez', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movi_recepcion`
--

CREATE TABLE `movi_recepcion` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_movi` int(10) UNSIGNED DEFAULT NULL,
  `fecha_recepcion` date NOT NULL,
  `destino` int(10) UNSIGNED NOT NULL,
  `o_direccion` int(10) UNSIGNED DEFAULT NULL,
  `ciudad` int(10) UNSIGNED DEFAULT NULL,
  `verificado` text NOT NULL,
  `nombre` int(10) UNSIGNED NOT NULL,
  `telefono` int(10) UNSIGNED DEFAULT NULL,
  `detalles` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `editado` datetime DEFAULT NULL,
  `editado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `movi_recepcion`
--

INSERT INTO `movi_recepcion` (`id`, `id_movi`, `fecha_recepcion`, `destino`, `o_direccion`, `ciudad`, `verificado`, `nombre`, `telefono`, `detalles`, `creado`, `creado_por`, `editado`, `editado_por`) VALUES
(1, 144, '2020-11-17', 9, 9, 1, '1', 3, 3, '<br>', '2020-11-17 15:30:08', 'felipem', NULL, NULL),
(2, 145, '2020-11-17', 9, 9, 1, '0', 8, 8, '<br>', '2020-11-17 15:35:36', 'felipem', NULL, NULL),
(3, 146, '2020-11-17', 9, 9, 1, '0', 8, 8, '<br>', '2020-11-17 15:41:46', 'felipem', NULL, NULL),
(4, 148, '2020-11-19', 9, 9, 1, '1', 8, 8, 'SE RECIBIÓ EL EQUIPO, SE ENCUENTRA EN BUEN ESTADO Y FUNCIONANDO&nbsp;', '2020-11-19 14:51:31', 'andresl', NULL, NULL),
(5, 149, '2020-11-19', 9, 9, 1, '1', 8, 8, 'SE RECIBIÓ EL EQUIPO, SE ENCUENTRA EN BUEN ESTADO&nbsp;<div>S/N 19120200001</div><div>MAPA 590</div>', '2020-11-19 15:07:41', 'andresl', NULL, NULL),
(6, 150, '2020-11-20', 3, 3, 4, '1', 3, 3, '<br>', '2020-11-20 08:53:45', 'felipem', NULL, NULL),
(7, 151, '2020-11-23', 2, 2, 1, '1', 8, 8, '<br>', '2020-11-23 08:39:49', 'andresl', NULL, NULL),
(8, 152, '2020-11-23', 2, 2, 1, '1', 8, 8, 'El equipo se verifico, está en óptimas condiciones de uso', '2020-11-23 10:00:43', 'andresl', NULL, NULL),
(9, 153, '2020-11-23', 2, 2, 1, '1', 8, 8, 'El equipo no se encontraba agregado a la unidad HMC&nbsp;', '2020-11-23 10:50:35', 'andresl', NULL, NULL),
(10, 154, '2020-11-24', 2, 2, 1, '1', 8, 8, '<br>', '2020-11-24 12:37:47', 'andresl', NULL, NULL),
(11, 155, '2020-11-26', 9, 9, 1, '1', 8, 8, '<br>', '2020-11-26 12:05:08', 'andresl', NULL, NULL),
(12, 156, '2020-12-02', 4, 4, 3, '1', 10, 10, 'OK', '2020-12-02 17:16:05', 'felipem', NULL, NULL),
(13, 157, '2020-12-03', 13, 13, 1, '1', 8, 8, 'se recibio completo', '2020-12-03 15:25:06', 'andresl', NULL, NULL),
(14, 158, '2020-12-03', 13, 13, 1, '1', 8, 8, 'SE RECIBIO COMPLETO', '2020-12-03 15:31:01', 'andresl', NULL, NULL),
(15, 159, '2020-12-03', 13, 13, 1, '1', 8, 8, 'EL EQUIPO YA SE ENCONTRABA EN LA UNIDAD SOLO SE AGREGO A LA PLATAFORMA', '2020-12-03 15:58:22', 'andresl', NULL, NULL),
(16, 160, '2020-12-03', 13, 13, 1, '1', 8, 8, 'LAS BANDA YA SE ENCONTRABA EN LA UNIDAD, SOLO SE AGREGO A LA PLATAFORMA', '2020-12-03 16:03:03', 'andresl', '2020-12-03 16:03:08', 'andresl'),
(17, 161, '2020-12-03', 13, 13, 1, '1', 8, 8, 'EL EQUIPO SE ENCONTRABA EN LA UNIDAD SOLO SE AGREGO A LA PLATAFORMA', '2020-12-03 16:09:00', 'andresl', NULL, NULL),
(18, 162, '2020-12-03', 13, 13, 1, '1', 8, 8, 'EL EQUIPO SE ENCONTRABA EN LA UNIDAD, SOLO SE AGREGO A LA PLATAFORMA', '2020-12-03 16:13:46', 'andresl', NULL, NULL),
(19, 164, '2020-12-04', 6, 6, 2, '1', 8, 8, 'El equipo se encontraba en la unidad, solo se agrego a la plataforma', '2020-12-04 09:40:38', 'andresl', NULL, NULL),
(20, 89, '2020-12-04', 3, 3, 4, '1', 10, 10, 'SE RECIBE ECOGRAFO CON MONITOR Y UPC  SIN TRASDUCTOR SE REALIZAN PRUEBAS OPERACIONALES Y SE DEJA FUNCIONANO EN PERFECTAS CONDICIONES.', '2020-12-04 17:36:04', 'lgarcia', NULL, NULL),
(21, 165, '2020-12-23', 4, 4, 3, '1', 3, 3, 'PRUEBA', '2020-12-23 12:43:33', 'felipem', NULL, NULL),
(22, 166, '2020-12-29', 4, 4, 3, '1', 8, 8, 'SE ENTREGO EL EQUIPO EN OPTIMAS CONDICIONES DE USO', '2020-12-29 14:45:08', 'andresl', NULL, NULL),
(23, 167, '2021-01-08', 7, 7, 1, '1', 5, 5, 'SE RECIBE HOLTER 144', '2021-01-12 08:48:59', 'camiloz', NULL, NULL),
(24, 167, '2021-01-12', 1, 1, 1, '1', 5, 5, 'RECIBIÓ ENFERMERA IVETH TORO', '2021-01-12 08:54:26', 'camiloz', NULL, NULL),
(25, 170, '2021-01-08', 7, 7, 1, '1', 5, 5, 'SE RECIBIÓ HOLTER 130', '2021-01-12 09:04:31', 'camiloz', NULL, NULL),
(26, 170, '2021-01-12', 1, 1, 1, '0', 5, 5, 'RECIBIÓ ENFERMERA IVETH TORO', '2021-01-12 09:12:26', 'camiloz', NULL, NULL),
(27, 168, '2021-01-12', 1, 1, 1, '1', 3, 3, 'Verificado en san Fernando con camilo, auditado por Felipe', '2021-01-12 12:56:08', 'felipem', NULL, NULL),
(28, 169, '2021-01-12', 7, 7, 1, '1', 3, 3, NULL, '2021-01-12 12:59:52', 'felipem', NULL, NULL),
(29, 171, '2021-01-24', 1, 1, 1, '1', 5, 5, 'RECIBIÓ IVETH TORO', '2021-01-25 09:35:22', 'camiloz', NULL, NULL),
(30, 172, '2021-01-24', 1, 1, 1, '1', 5, 5, 'RECIBIÓ IVETH TORO', '2021-01-25 09:42:33', 'camiloz', NULL, NULL),
(31, 173, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 10:11:30', 'camiloz', NULL, NULL),
(32, 174, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 10:43:20', 'camiloz', NULL, NULL),
(33, 175, '2021-01-25', 17, 17, 17, '1', 3, 3, 'RECIBIÓ FELIPE MOLANO', '2021-01-25 10:52:12', 'camiloz', NULL, NULL),
(34, 176, '2021-01-25', 17, 17, 17, '1', 3, 3, 'RECIBIÓ FELIPE MOLANO', '2021-01-25 10:55:54', 'camiloz', NULL, NULL),
(35, 178, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:07:34', 'camiloz', NULL, NULL),
(37, 179, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:18:26', 'camiloz', NULL, NULL),
(38, 180, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:24:05', 'camiloz', NULL, NULL),
(39, 181, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:27:54', 'camiloz', NULL, NULL),
(40, 182, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:39:18', 'camiloz', NULL, NULL),
(41, 183, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:48:32', 'camiloz', NULL, NULL),
(42, 184, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 13:52:55', 'camiloz', NULL, NULL),
(43, 185, '2021-01-25', 17, 17, 17, '1', 3, 3, NULL, '2021-01-25 14:46:27', 'camiloz', NULL, NULL),
(44, 189, '2021-02-16', 7, 7, 7, '1', 8, 8, 'el equipo  se encontraba mal codificado', '2021-02-16 13:40:20', 'a.lopez', NULL, NULL),
(45, 190, '2021-02-19', 7, 7, 7, '1', 8, 8, 'En san Fernando.', '2021-02-19 09:57:08', 'felipem', NULL, NULL),
(46, 192, '2021-04-08', 2, 2, 2, '1', 8, 8, 'El equipo se encontraba en la unidad, pero se encontraba codificado con el mismo serial en la sede de San Fernando', '2021-04-08 15:37:46', 'a.lopez', '2021-04-08 15:37:59', 'a.lopez'),
(47, 193, '2021-04-09', 2, 2, 2, '1', 8, 8, 'El equipo se deja en San Fernando mientras se hace cambio de baterías', '2021-04-09 14:03:32', 'a.lopez', '2021-04-09 14:03:46', 'a.lopez'),
(48, 197, '2021-04-12', 7, 7, 7, '1', 8, 8, 'El equipo se cambió por la grabador holter 132 \r\nS/N 3A/11293', '2021-04-12 10:58:11', 'a.lopez', NULL, NULL),
(49, 200, '2021-04-12', 1, 1, 1, '1', 8, 8, 'El equipo se había cambiado a cambio del holter 117 que no estaba grabando', '2021-04-12 11:04:55', 'a.lopez', NULL, NULL),
(50, 201, '2021-04-12', 7, 7, 7, '1', 8, 8, 'se envió para hacer cambió de la manguera', '2021-04-12 16:47:02', 'a.lopez', '2021-04-12 16:49:29', 'a.lopez'),
(51, 191, '2021-04-16', 4, 4, 4, '1', 9, 9, 'Se realiza la entrega en la clínica UNIDES de Tuluá, El Holter en referencia, el dispositivo lo entrega el ingeniero solarte', '2021-04-19 14:41:50', 'felipem', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observaciones`
--

CREATE TABLE `observaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` int(10) UNSIGNED DEFAULT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `observacion` text NOT NULL,
  `estado` int(10) UNSIGNED NOT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL,
  `descrip_estado` int(10) UNSIGNED DEFAULT NULL,
  `estado_observacion` int(10) UNSIGNED DEFAULT NULL,
  `unidad` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `observaciones`
--

INSERT INTO `observaciones` (`id`, `codigo`, `tipo_dispo`, `serial`, `observacion`, `estado`, `creado`, `creado_por`, `descrip_estado`, `estado_observacion`, `unidad`) VALUES
(1, 162, 162, 162, '<p dir=\"ltr\">Requiere revisión, el motor no arrastra en baja velocidad, ademas el protector inferior&nbsp; de la barra derecha se encuentra partido</p>', 1, '2020-11-20 18:15:51', 'andresl', NULL, NULL, 0),
(2, 20, 20, 20, 'La tapa del equipo se encuentra en&nbsp; mal estado&nbsp;', 1, '2020-11-23 08:24:59', 'andresl', NULL, NULL, 0),
(3, 4, 4, 4, 'La tapa del equipo se encuentra en mal estado&nbsp;', 1, '2020-11-23 09:24:38', 'andresl', NULL, NULL, 0),
(5, 581, 581, 581, 'El equipo funciona bien, requiere actualizacion del sistemas ( Windows update )', 1, '2020-11-24 09:21:06', 'andresl', NULL, NULL, 0),
(6, 149, 149, 149, 'el dia de hoy se&nbsp; le&nbsp; actualizo el sistema de seguridad&nbsp; a el equipo&nbsp;', 1, '2020-11-24 11:12:11', 'andresl', NULL, NULL, 0),
(7, 172, 172, 172, 'La tapa trasera de el equipo se encuentra en mal estado&nbsp;<hr>', 1, '2020-11-25 10:16:15', 'andresl', NULL, NULL, 0),
(8, 138, 138, 138, 'El equipo no función correctamente se cambió el estado de funcionamiento a (inactivo)', 3, '2020-11-25 11:06:36', 'andresl', NULL, NULL, 0),
(9, 238, 238, 238, 'La batería del equipo se encuentra inflamada, y la pantalla&nbsp; se encuentra partida&nbsp;', 1, '2020-11-25 11:22:10', 'andresl', NULL, NULL, 0),
(10, 158, 158, 158, 'Se requiere licencia paquete de Microsoft office&nbsp;', 1, '2020-11-25 11:39:04', 'andresl', NULL, NULL, 0),
(11, 588, 588, 588, 'El equipo presenta fallas en el arracque, se nesecita licencia de paquete de Microsoft office 2013&nbsp;', 1, '2020-11-25 12:30:06', 'andresl', NULL, NULL, 0),
(12, 4, 4, 4, 'la tapa trasera del equipo se encuentra en mal estado&nbsp;', 1, '2020-11-26 11:25:51', 'andresl', NULL, NULL, 0),
(13, 231, 231, 231, 'Se le actualizo el controlador de audio&nbsp;', 1, '2020-11-28 10:30:42', 'andresl', NULL, NULL, 0),
(14, 242, 242, 242, 'Dentro de la pantalla del computador se encontró una mancha de tinta, además se timbra y se reinicia el equipo&nbsp;', 1, '2020-11-28 10:46:28', 'andresl', NULL, NULL, 0),
(15, 157, 157, 157, 'LA PILA DEL EQUIPO SE ENCUENTRA EN MAL ESTADO Y SE DEBE UTILIZAR EL PC CONECTADO A LA CORRIENTE&nbsp;', 1, '2020-12-01 16:43:23', 'andresl', NULL, NULL, 0),
(16, 137, 137, 137, 'No funciona&nbsp;', 3, '2020-12-02 08:50:59', 'felipem', NULL, NULL, 0),
(17, 46, 46, 46, 'Se le cambió la tarjeta digital doppler la anterior tarjeta estaba fallando', 0, '2020-12-02 18:36:31', 'andresl', NULL, NULL, 0),
(18, 631, 631, 631, 'EL EQUIPO YA SE ENCONTRABA EN LA UNIDAD SOLO SE AGREGO A LA PLATAFORMA', 1, '2020-12-03 15:59:18', 'andresl', NULL, NULL, 0),
(19, 737, 737, 737, 'El equipo no imprime, se queda pegada la banda', 2, '2020-12-04 09:23:35', 'andresl', NULL, NULL, 0),
(20, 738, 738, 738, 'La impresora presenta fallas al momento de fotocopiar', 2, '2020-12-04 09:30:30', 'andresl', NULL, NULL, 0),
(21, 58, 58, 58, 'Verificar datos porque se ingreso el HOLTER 200, se debe actualizar y colocar HOLTER 131', 4, '2020-12-10 11:52:19', 'felipem', NULL, NULL, 0),
(22, 54, 54, 54, 'Denle retira la procesadora de gráficos para poner el el equipo principal de la unidad. 700 USD', 3, '2021-04-13 10:23:56', 'felipem', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `apellidos` varchar(40) NOT NULL,
  `tipo_id` int(10) UNSIGNED NOT NULL,
  `numero_id` varchar(40) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `telefono` varchar(40) NOT NULL,
  `celular` varchar(40) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `rh` int(10) UNSIGNED DEFAULT NULL,
  `foto_perfil` varchar(40) DEFAULT NULL,
  `estado_civil` varchar(40) DEFAULT NULL,
  `direccion` text,
  `usuario` varchar(40) DEFAULT NULL,
  `contrasena` varchar(40) DEFAULT NULL,
  `optimo` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `perfil`
--

INSERT INTO `perfil` (`id`, `nombre`, `apellidos`, `tipo_id`, `numero_id`, `fecha_nacimiento`, `telefono`, `celular`, `email`, `rh`, `foto_perfil`, `estado_civil`, `direccion`, `usuario`, `contrasena`, `optimo`, `creado`, `creado_por`) VALUES
(1, 'FELipe', 'Molano', 1, '4234', '2021-01-12', '4234', '234234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-01-19 11:31:29', 'felipem');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_familia`
--

CREATE TABLE `perfil_familia` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perfil` int(10) UNSIGNED DEFAULT NULL,
  `identificacion` int(10) UNSIGNED DEFAULT NULL,
  `parentesco` int(10) UNSIGNED DEFAULT NULL,
  `nombre` varchar(40) NOT NULL,
  `telefono` varchar(40) NOT NULL,
  `direccion` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_soportes`
--

CREATE TABLE `perfil_soportes` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perfil` int(10) UNSIGNED DEFAULT NULL,
  `tipo_documento` int(10) UNSIGNED DEFAULT NULL,
  `nombre_documento` varchar(40) DEFAULT NULL,
  `docuemento` varchar(40) DEFAULT NULL,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestador`
--

CREATE TABLE `prestador` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `razon_social` int(10) UNSIGNED NOT NULL,
  `nit` int(10) UNSIGNED DEFAULT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prestador`
--

INSERT INTO `prestador` (`id`, `nombre`, `razon_social`, `nit`, `descripcion`) VALUES
(1, 'STARMEDICA', 1, 1, 'Factura por Starmedica SAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `progreso`
--

CREATE TABLE `progreso` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_observacion` int(10) UNSIGNED DEFAULT NULL,
  `estado_seguimiento` int(10) UNSIGNED NOT NULL,
  `descripcion` text NOT NULL,
  `credo` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seguridad_social`
--

CREATE TABLE `seguridad_social` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_perfil` int(10) UNSIGNED DEFAULT NULL,
  `tipo_documento` int(10) UNSIGNED DEFAULT NULL,
  `docuemento` varchar(40) DEFAULT NULL,
  `descripcion` text,
  `creado` datetime DEFAULT NULL,
  `creado_por` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_accesorio`
--

CREATE TABLE `tipo_accesorio` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_accesorio` varchar(40) NOT NULL,
  `descripcion` text,
  `prefijo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_calibracion`
--

CREATE TABLE `tipo_calibracion` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_calibracion` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_calibracion`
--

INSERT INTO `tipo_calibracion` (`id`, `tipo_calibracion`, `descripcion`) VALUES
(1, 'CALIBRACIÓN PROGRAMADA', 'Calibración realizada en periodos determinados por la ficha técnica del equipo'),
(2, 'CALIBRACION CORRECTIVA', 'Calibración realizada a demanda del dispositivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_contacto`
--

CREATE TABLE `tipo_contacto` (
  `id_tipo_contacto` int(10) UNSIGNED NOT NULL,
  `tipo_contacto` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_contacto`
--

INSERT INTO `tipo_contacto` (`id_tipo_contacto`, `tipo_contacto`, `descripcion`) VALUES
(1, 'PERSONA', 'Persona natural'),
(2, 'EMPRESA', 'Persona jurídica o empresa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_costo`
--

CREATE TABLE `tipo_costo` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_costo` varchar(40) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_dispositivo`
--

CREATE TABLE `tipo_dispositivo` (
  `id_tipodispo` int(10) UNSIGNED NOT NULL,
  `tipo_dispositivo` varchar(40) NOT NULL,
  `grupo_dispo` int(10) UNSIGNED NOT NULL,
  `prefijo` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_dispositivo`
--

INSERT INTO `tipo_dispositivo` (`id_tipodispo`, `tipo_dispositivo`, `grupo_dispo`, `prefijo`, `descripcion`) VALUES
(1, 'HOLTER', 3, 'HOLTER', '01 - DESDE 101 - HASTA 200'),
(2, 'MAPA', 3, 'MAPA', 'DESDE 550 - HASTA 600'),
(3, 'ECOGRAFO', 3, 'ECO', '02 - DESDE 201 - HASTA 250'),
(4, 'DESFIBRILADOR', 3, 'DES', 'DESDE 451 - HASTA 500'),
(5, 'ECG', 3, 'ECG', 'DESDE 301 - HASTA 350'),
(6, 'STRESS TEST', 3, 'STR', 'DESDE 751 - HASTA 780'),
(7, 'BANDA TROTADORA', 3, 'BT', 'DESDE 701 - HASTA 730'),
(8, 'TENSIOMETRO', 3, 'SM', 'DESDE 731 - HASTA 750'),
(9, 'FONENDOSCOPIO', 3, 'SM', 'DESDE 781 - HASTA 800'),
(10, 'CABLE ECG', 1, 'CAB', 'DESDE 351 - HASTA 400'),
(11, 'COMPUTADOR', 3, 'PC', 'DESDE 601 - HSTA 700.'),
(13, 'TRANSDUCTOR', 1, 'TR', '03 - DESDE 251 - HASTA 300'),
(14, 'DISPONIBLE 5', 3, 'DI5', 'DESDE - HASTA'),
(15, 'DIGITALIZADOR', 1, 'DG', 'DESDE 401 - HASTA 450'),
(16, 'MEMORIA USB', 1, 'USB', 'DESDE 901 - HASTA 950'),
(17, 'IMPRESORA', 1, 'SM', 'DESDE 801 - HASTA 850'),
(18, 'ASPIRADOR', 3, 'SM', 'DESDE 526 - HASTA 536'),
(19, 'BOMBA DE INFUSIÓN', 3, 'SM', 'DESDE 501 - HASTA 525'),
(20, 'LECTOR DE HUELLA', 3, 'SM', 'DESDE 537 - HASTA 549'),
(21, 'TELÉFONO CELULAR', 3, 'CEL', 'DESDE 1000 - HASTA 1050'),
(31, 'UPS', 3, 'UPS', 'Ups regulador'),
(32, 'TERMO HIGROMETRO', 3, 'THG', 'Termo higrómetros para medir la temperatura y los niveles de humendad'),
(23, 'SIN ASIGNAR', 3, 'LIBRE', 'Numero sin asignar'),
(24, 'BASCULA', 3, 'BASCULA', 'DESDE 851 - HASTA 900'),
(25, 'DISPONIBLE 7', 3, 'D7', NULL),
(29, 'DISPONIBLE 4', 3, 'D4', 'DESDE 951 - HASTA 999'),
(30, 'DISPONIBLE 6', 3, 'D6', 'DESDE 1051 - HASTA 1100'),
(33, 'ROUTER WIFI', 3, 'WIFI', 'ROUTER WIFI'),
(34, 'SOPLADOR', 3, 'SM', 'Soplador para ecocardiografos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento`
--

CREATE TABLE `tipo_documento` (
  `id_tipodoc` int(10) UNSIGNED NOT NULL,
  `tipo_doc` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_documento`
--

INSERT INTO `tipo_documento` (`id_tipodoc`, `tipo_doc`, `descripcion`) VALUES
(1, 'FACTURA', 'Factura de compra'),
(2, 'HOJA DE VIDA', 'Hoja de vida del dispositivo'),
(3, 'REGISTRO INVIMA', 'Registro invima'),
(4, 'CRONOGRAMA DE MANTENIMIENTO', 'Cronograma de mantenimiento'),
(5, 'DECLARACIÓN DE IMPORTACION', 'Declaración de importación'),
(6, 'FICHA TÉCNICA', 'Ficha técnica o documento de propiedades técnicas del equipo o dispositivo'),
(7, 'MANUAL DE USUARIO', 'Manual de usuario'),
(8, 'MANUAL DE SERVICIO', 'Manual de servicio del dispositivo'),
(9, 'PROTOCOLO DE LIMPIEZA Y DESINFECCIÓN', 'Protocolo, guía o manual deimpieza del equipo'),
(10, 'DXDIAG', 'Documento de información del sistema'),
(11, 'ACTA DE ENTREGA', NULL),
(12, 'GARANTIA', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_documento_ss`
--

CREATE TABLE `tipo_documento_ss` (
  `id_tipodoc` int(10) UNSIGNED NOT NULL,
  `tipo_doc` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado_dispo`
--

CREATE TABLE `tipo_estado_dispo` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado_dispo` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_estado_dispo`
--

INSERT INTO `tipo_estado_dispo` (`id`, `estado_dispo`, `descripcion`) VALUES
(1, 'ACTIVO', 'Dispositivo funcionando correctamente'),
(2, 'INACTIVO', 'Dispositivo en verificación y/o reparación'),
(3, 'FUERA DE SERVICIO', 'Equipo dañado'),
(4, 'SIN ASIGNAR', NULL),
(5, 'DE BAJA', 'Equipos que nunca más se vuelven a utilizar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado_movi`
--

CREATE TABLE `tipo_estado_movi` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado_movi` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_estado_movi`
--

INSERT INTO `tipo_estado_movi` (`id`, `estado_movi`, `descripcion`) VALUES
(1, 'EN PROCESO', 'Movimiento en proceso'),
(2, 'COMPLETADO', 'Movimiento terminado o completado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado_observa`
--

CREATE TABLE `tipo_estado_observa` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_estado_observa` varchar(40) DEFAULT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado_observaciones`
--

CREATE TABLE `tipo_estado_observaciones` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado_observa` varchar(40) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_estado_observaciones`
--

INSERT INTO `tipo_estado_observaciones` (`id`, `estado_observa`, `descripcion`) VALUES
(1, 'PARA REPARAR - ACTIVO', 'Cuando se requiere reparar un daño en el equipo y el equipo sigue en funcionamiento'),
(2, 'REQUIERE REPUESTO - INACTIVO', 'Cuando el equipo requiere un repuesto y está inactivo'),
(3, 'DAÑADO - INACTIVO', 'Cuando el equipo se encuentra inactivo por causa de algún daño repentino'),
(4, 'VERIFICAR DATOS', 'Algún dato o información de este equipo es incorrecta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_estado_verifica`
--

CREATE TABLE `tipo_estado_verifica` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado_verificado` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_estado_verifica`
--

INSERT INTO `tipo_estado_verifica` (`id`, `estado_verificado`, `descripcion`) VALUES
(1, 'ENVIADO - En espera', 'Cuando el dispositivo de ha enviado y se encuentra en espera de la recepción'),
(2, 'RECIBIDO - Verificado', 'Cuando el dispositivo ha sido recibido y verificado, se debe ingresar a actualizar el registro de la nueva ubicación en el dispositivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_formatos`
--

CREATE TABLE `tipo_formatos` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_formato` varchar(60) NOT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_fuera_servicio`
--

CREATE TABLE `tipo_fuera_servicio` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_fuera_servicio` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_grupo_mobilia`
--

CREATE TABLE `tipo_grupo_mobilia` (
  `id_gru_mo` int(10) UNSIGNED NOT NULL,
  `grupo` varchar(40) NOT NULL,
  `prefijo` varchar(40) DEFAULT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_iden`
--

CREATE TABLE `tipo_iden` (
  `id_iden` int(10) UNSIGNED NOT NULL,
  `tipo_iden` varchar(40) NOT NULL,
  `tipo_iden_abre` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_iden`
--

INSERT INTO `tipo_iden` (`id_iden`, `tipo_iden`, `tipo_iden_abre`) VALUES
(1, 'Cédula de ciudadanía', 'CC'),
(2, 'Número de identificación tributaria', 'NIT'),
(3, 'Identificación', 'ID');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_mobiliario`
--

CREATE TABLE `tipo_mobiliario` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_mobiliario` varchar(40) NOT NULL,
  `descripcion` text,
  `grupo_mobi` int(10) UNSIGNED DEFAULT NULL,
  `prefijo` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_mobiliario`
--

INSERT INTO `tipo_mobiliario` (`id`, `tipo_mobiliario`, `descripcion`, `grupo_mobi`, `prefijo`) VALUES
(1, 'MESA ESCRITORIO', 'todo tipo de mesa o escritorio', NULL, NULL),
(2, 'SILLA', 'Todo tipo de sillas', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_movimiento`
--

CREATE TABLE `tipo_movimiento` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_movimiento` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_movimiento`
--

INSERT INTO `tipo_movimiento` (`id`, `tipo_movimiento`, `descripcion`) VALUES
(1, 'EN PROCESO', 'Movimiento en proceso'),
(2, 'TERMINADO', 'Proceso de envío terminado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_mtto`
--

CREATE TABLE `tipo_mtto` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_mtto` varchar(40) NOT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_mtto`
--

INSERT INTO `tipo_mtto` (`id`, `tipo_mtto`, `descripcion`) VALUES
(1, 'PREVENTIVO', 'Mantenimiento preventivo'),
(2, 'CORRECTIVO', 'Mantenimiento correctivo, cambio de piezas o repuestos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_parentesco`
--

CREATE TABLE `tipo_parentesco` (
  `id` int(10) UNSIGNED NOT NULL,
  `parentesco` varchar(40) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_razon_social`
--

CREATE TABLE `tipo_razon_social` (
  `id` int(10) UNSIGNED NOT NULL,
  `razon_social` varchar(40) NOT NULL,
  `nit` varchar(40) DEFAULT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_razon_social`
--

INSERT INTO `tipo_razon_social` (`id`, `razon_social`, `nit`, `descripcion`) VALUES
(1, 'STARMEDICA SAS', '900.462.329-4', 'Starmedica SAS'),
(2, 'STARMEDICAL VALLE SAS', '900.713.421-2', 'STARMEDICAL VALLE SAS'),
(3, 'HUBERTH RUIZ HENAO', '76304351-2', 'Hubert Ruiz empresa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_relacion`
--

CREATE TABLE `tipo_relacion` (
  `id_tiporelac` int(10) UNSIGNED NOT NULL,
  `tipo_relac` varchar(40) NOT NULL,
  `descripcion` text,
  `creado` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_relacion`
--

INSERT INTO `tipo_relacion` (`id_tiporelac`, `tipo_relac`, `descripcion`, `creado`) VALUES
(1, 'PROVEEDOR', 'Proveedor de productos o servicios', NULL),
(2, 'MIEMBRO', 'Miembro de la empresa', NULL),
(3, 'ASISTENTE', 'Asistente de apoyo', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_rh`
--

CREATE TABLE `tipo_rh` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo_rh` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

CREATE TABLE `unidades` (
  `id_unidades` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `abreviado` varchar(40) NOT NULL,
  `ciudad` int(10) UNSIGNED DEFAULT NULL,
  `telefono` varchar(40) NOT NULL,
  `razon_social` int(10) UNSIGNED DEFAULT NULL,
  `logo` varchar(40) DEFAULT NULL,
  `direccion` text NOT NULL,
  `ubicacion` varchar(60) DEFAULT NULL,
  `prestador` int(10) UNSIGNED DEFAULT NULL,
  `dispositivos_costo` decimal(10,0) DEFAULT NULL,
  `mobiliario_costo` decimal(10,2) DEFAULT NULL,
  `costo_total` decimal(10,2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `unidades`
--

INSERT INTO `unidades` (`id_unidades`, `nombre`, `abreviado`, `ciudad`, `telefono`, `razon_social`, `logo`, `direccion`, `ubicacion`, `prestador`, `dispositivos_costo`, `mobiliario_costo`, `costo_total`) VALUES
(1, 'HOSPITAL ISAIAS DUARTE CANCINO', 'HIDC', 1, '3148835680', 1, '063f6e2df5bd2145b.png', '', NULL, NULL, 0, NULL, 0.00),
(2, 'HOSPITAL MARIO CORREA', 'HMC', 1, '3135507451', 1, '29caf302286e1a090.jpg', 'Cra. 78 #No. 2A - 00', 'CALI - CHORROS', 1, 550000, NULL, 0.00),
(3, 'HOSPITAL RAUL OREJUELA BUENO', 'HROB', 4, '3147281502', 2, NULL, '', NULL, NULL, 0, NULL, 0.00),
(4, 'HOSPITAL TOMAS URIBE', 'HTU', 3, '3127927840', 1, NULL, 'asda', NULL, NULL, 0, NULL, 0.00),
(5, 'CLÍNICA SAN FRANCISCO', 'CSF', 3, '3166261217', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(6, 'FUNDACIÓN HOSPITAL SAN JOSÉ', 'FHSJ', 2, '3135495504', 1, 'd7659fe6d37b3ae03.png', 'Carrera 8 No 17 - 52', 'https://www.fhsjb.org/web/', NULL, 0, NULL, 0.00),
(7, 'BODEGA SAN FERNANDO', 'BSFD', 1, '3135507417', NULL, NULL, 'Carrera 37 #5b2-78', 'SAN FERNANDO', NULL, 0, NULL, 0.00),
(8, 'ÁREA DE PROGRAMACIÓN', 'SOL BECERRA', 1, '', 2, NULL, '', NULL, NULL, 0, NULL, 0.00),
(9, 'VIVA-1A PROFAMILIA', 'VIVA', 1, '', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(10, 'HOSPITAL FRANCISCO DE PAULA SANTANDER', 'HFPS', 1, '', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(11, 'OFICINA PRINCIPAL LA REFORMA', 'SM REFORMA', 1, '3144110208', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(12, 'CLINICA UNIDOS POR LA VIDA', 'CUNI', 1, '', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(13, 'MEDISUN ESENSA', 'MEDE', 1, '', 1, NULL, '', NULL, NULL, 300000, NULL, 0.00),
(14, 'MEDISUN TEUQENDAMA', 'MEDT', 1, '', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(15, 'CLINICA DE REHABILITACIÓN', 'CRV', 3, '', 1, NULL, '', NULL, NULL, 0, NULL, 0.00),
(16, 'SIN ASIGNAR', 'SN', NULL, '', NULL, NULL, '', NULL, NULL, 0, NULL, 0.00),
(17, 'SEDE SAN FERNANDO', 'SFD', 1, '', NULL, 'eff043dfa83710bb0.png', 'Carrera 37 #5b2-78', 'San Fernando Sede Principal', 1, 0, NULL, 0.00),
(19, 'UNIDES', 'UDES', 3, '0322316169', NULL, NULL, 'Carrera 34 # 26 - 07', 'Clinica San Francisco piso 4', 1, 0, NULL, 0.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `verificacion`
--

CREATE TABLE `verificacion` (
  `id` int(10) UNSIGNED NOT NULL,
  `codigo` int(10) UNSIGNED DEFAULT NULL,
  `tipo_dispo` int(10) UNSIGNED DEFAULT NULL,
  `serial` int(10) UNSIGNED DEFAULT NULL,
  `ubicacion` int(10) UNSIGNED NOT NULL,
  `verificacion` varchar(40) NOT NULL,
  `comentario` text,
  `verifi_por` varchar(40) DEFAULT NULL,
  `fecha_verifi` datetime DEFAULT NULL,
  `abreviado` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `verificacion`
--

INSERT INTO `verificacion` (`id`, `codigo`, `tipo_dispo`, `serial`, `ubicacion`, `verificacion`, `comentario`, `verifi_por`, `fecha_verifi`, `abreviado`) VALUES
(1, 1, 1, 1, 1, '1', NULL, 'felipem', '0000-00-00 00:00:00', NULL),
(2, 18, 18, 18, 4, '1', NULL, 'crueda', '0000-00-00 00:00:00', 4),
(3, 21, 21, 21, 21, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(4, 22, 22, 22, 22, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(5, 25, 25, 25, 25, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(6, 199, 199, 199, 4, '1', '<br>', 'hrestrepo', '0000-00-00 00:00:00', 4),
(7, 202, 202, 202, 4, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', 4),
(8, 203, 203, 203, 203, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(9, 67, 67, 67, 67, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(10, 68, 68, 68, 68, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(11, 89, 89, 89, 89, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(12, 96, 96, 96, 96, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(13, 97, 97, 97, 97, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(14, 86, 86, 86, 86, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(15, 209, 209, 209, 209, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(16, 129, 129, 129, 129, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(17, 234, 234, 234, 234, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(18, 221, 221, 221, 221, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(19, 233, 233, 233, 233, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(20, 232, 232, 232, 232, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(21, 140, 140, 140, 140, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(22, 231, 231, 231, 231, '1', NULL, 'crueda', '0000-00-00 00:00:00', NULL),
(23, 111, 111, 111, 111, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(24, 40, 40, 40, 40, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(25, 230, 230, 230, 230, '1', 'esta banda con ese numero no esta en la unidad le envió al correo numero y serial de las dos bandas que manejamos aca en la unidad', 'crueda', '0000-00-00 00:00:00', NULL),
(26, 55, 55, 55, 55, '1', NULL, 'hrestrepo', '0000-00-00 00:00:00', NULL),
(27, 2, 2, 2, 2, '1', 'VERIFICADO.', 'jvilla', '0000-00-00 00:00:00', NULL),
(28, 9, 9, 9, 9, '1', 'SE REALIZA VERIFICACIÓN DE ESTE DISPOSITIVO Y SE EVIDENCIA QUE NO SE ENCUENTRA EN LA UNIDAD.', 'jvilla', '0000-00-00 00:00:00', NULL),
(29, 17, 17, 17, 17, '1', 'VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(30, 26, 26, 26, 26, '1', 'VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(31, 49, 49, 49, 49, '1', 'VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(32, 59, 59, 59, 59, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(33, 60, 60, 60, 60, '1', 'EQUIPO VERIFICADO CORRECTAMENTE', 'jvilla', '0000-00-00 00:00:00', NULL),
(34, 82, 82, 82, 82, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(35, 87, 87, 87, 87, '1', 'EQUIPO VERIFICADO CORRECTAMENTE', 'jvilla', '0000-00-00 00:00:00', NULL),
(36, 124, 124, 124, 124, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(37, 158, 158, 158, 158, '1', 'EQUIPO  VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(38, 165, 165, 165, 165, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(39, 172, 172, 172, 172, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(40, 226, 226, 226, 226, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(41, 229, 229, 229, 229, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(42, 80, 80, 80, 80, '1', 'EQUIPO VERIFICADO CORRECTAMENTE.', 'jvilla', '0000-00-00 00:00:00', NULL),
(43, 57, 57, 57, 57, '1', 'EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(44, 58, 58, 58, 58, '1', 'EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(45, 88, 88, 88, 88, '1', 'ESTE EQUIPO NO SE ENCUENTRA EN LA UNIDAD POR ESTE MOTIVO NO SE PUDO VERIFICAR.', 'ibet', '0000-00-00 00:00:00', NULL),
(46, 238, 238, 238, 238, '1', NULL, 'felipem', '0000-00-00 00:00:00', NULL),
(47, 90, 90, 90, 90, '1', 'EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(48, 93, 93, 93, 93, '1', 'EL EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(49, 171, 171, 171, 171, '1', 'ESTE EQUIPO NO SE ENCUENTRA EN LA UNIDAD POR ESTE MOTIVO NO SE PUDO VERIFICAR.', 'ibet', '0000-00-00 00:00:00', NULL),
(50, 216, 216, 216, 216, '1', 'ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(51, 217, 217, 217, 217, '1', 'ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(52, 218, 218, 218, 218, '1', 'ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(53, 219, 219, 219, 219, '1', 'ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(54, 220, 220, 220, 220, '1', 'ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(55, 92, 92, 92, 92, '1', 'ESTE EQUIPO HA SIDO VERIFICADO CORRECTAMENTE.', 'ibet', '0000-00-00 00:00:00', NULL),
(56, 203, 203, 203, 203, '1', 'Flm', 'superadmin', '0000-00-00 00:00:00', NULL),
(57, 202, 202, 202, 202, '1', 'Flm', 'superadmin', '0000-00-00 00:00:00', NULL),
(58, 21, 21, 21, 21, '1', NULL, 'superadmin', '0000-00-00 00:00:00', NULL),
(59, 25, 25, 25, 25, '1', 'Flm', 'superadmin', '0000-00-00 00:00:00', NULL),
(60, 208, 208, 208, 208, '1', 'Verificado hoy dentro de la unidad HTU, MAPA CON SPO', 'superadmin', '0000-00-00 00:00:00', NULL),
(61, 209, 209, 209, 209, '1', 'Verificado en HTU, equipo con SPO', 'superadmin', '0000-00-00 00:00:00', NULL),
(62, 246, 246, 246, 246, '1', 'flm', 'superadmin', '0000-00-00 00:00:00', NULL),
(63, 91, 91, 91, 91, '1', 'flm', 'superadmin', '0000-00-00 00:00:00', NULL),
(64, 84, 84, 84, 84, '1', 'flm', 'superadmin', '0000-00-00 00:00:00', NULL),
(65, 230, 230, 230, 230, '1', 'Se ha marcado el con pirograbador', 'superadmin', '0000-00-00 00:00:00', NULL),
(66, 247, 247, 247, 1, '1', 'Flm', 'superadmin', '0000-00-00 00:00:00', 1),
(67, 26, 26, 26, 13, '1', '<br>', 'superadmin', '2020-11-13 14:53:05', 13),
(68, 9, 9, 9, 13, '1', '<br>', 'superadmin', '2020-11-13 14:54:52', 13),
(69, 42, 42, 42, 2, '1', 'Prueba N.1', 'andresl', '2020-11-17 12:37:40', 2),
(70, 106, 106, 106, 2, '1', '<br>', 'andresl', '2020-11-17 12:45:03', 2),
(71, 81, 81, 81, 2, '1', '<br>', 'andresl', '2020-11-17 13:05:43', 2),
(73, 54, 54, 54, 9, '1', '<br>', 'andresl', '2020-11-18 15:13:33', 9),
(74, 128, 128, 128, 9, '1', '<br>', 'andresl', '2020-11-18 15:15:35', 9),
(75, 54, 54, 54, 9, '1', 'Cambio de tarjeta Digital Doppler', 'andresl', '2020-11-19 13:20:37', 9),
(76, 83, 83, 83, 9, '1', '<br>', 'andresl', '2020-11-19 13:45:59', 9),
(77, 90, 90, 90, 9, '1', '<br>', 'andresl', '2020-11-19 14:20:23', 9),
(78, 220, 220, 220, 9, '1', 'EL EQUIPO SE VERIFICO CORRECTAMENTE, LA TAPA DE LA BATERÍA SE ENCUENTRA EN MAL ESTADO&nbsp;', 'andresl', '2020-11-19 15:00:14', 9),
(79, 206, 206, 206, 9, '1', 'SE RECIBIÓ EL EQUIPO, SE ENCUENTRA EN BUEN ESTADO&nbsp;', 'andresl', '2020-11-19 15:09:19', 9),
(80, 5, 5, 5, 11, '1', '<br>', 'felipem', '2020-11-20 08:49:14', 11),
(81, 5, 5, 5, 3, '1', 'Feli', 'felipem', '2020-11-20 08:50:06', 3),
(82, 85, 85, 85, 3, '1', '<br>', 'felipem', '2020-11-20 09:21:49', 3),
(83, 130, 130, 130, 3, '1', '<br>', 'felipem', '2020-11-20 10:25:01', 3),
(84, 94, 94, 94, 3, '1', 'SE VERIFICO EL DISPOSITIVO, SE ENCU NTRA EN BUEN ESTADO Y FUNCIONANDO&nbsp;<div>S/N 17030100032</div><div>MAPA 585</div>', 'andresl', '2020-11-20 10:41:41', 3),
(85, 162, 162, 162, 3, '1', '<br>', 'andresl', '2020-11-20 18:18:13', 3),
(86, 81, 81, 81, 2, '1', 'Se verificó que el equipo&nbsp; está funcionando correctamente', 'andresl', '2020-11-23 08:19:26', 2),
(87, 20, 20, 20, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 08:23:45', 2),
(88, 57, 57, 57, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 08:44:32', 2),
(89, 260, 260, 260, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 09:21:40', 2),
(90, 4, 4, 4, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 09:25:38', 2),
(91, 42, 42, 42, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 09:34:26', 2),
(92, 163, 163, 163, 2, '1', 'Se verificó que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-23 09:39:37', 2),
(93, 138, 138, 138, 2, '1', 'Se verificó que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-23 09:49:16', 2),
(94, 557, 557, 557, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 10:10:32', 2),
(95, 558, 558, 558, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-23 10:52:23', 2),
(96, 42, 42, 42, 2, '1', 'El equipo se encuentra, funcionando correctamente', 'andresl', '2020-11-24 08:37:53', 2),
(142, 250, 250, 250, 4, '1', 'Se verificó que la banda de trote se encuentra en óptimas condiciones para el uso', 'andresl', '2020-11-28 10:19:48', 4),
(98, 581, 581, 581, 2, '1', 'Se verificó que el equipo funciona correctamente&nbsp;<div>&nbsp;</div>', 'andresl', '2020-11-24 09:18:59', 2),
(99, 558, 558, 558, 2, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-24 09:27:16', 2),
(117, 238, 238, 238, 1, '1', '<br>', 'andresl', '2020-11-25 11:24:19', 1),
(118, 229, 229, 229, 1, '1', 'Se verificó que el equipo está funcionando correctamente', 'andresl', '2020-11-25 11:26:07', 1),
(101, 57, 57, 57, 2, '1', 'Se verificó que el equipo se encuentra funcionando correctamente', 'andresl', '2020-11-24 09:53:47', 2),
(111, 50, 50, 50, 1, '1', 'El equipo se encuentra en óptimas condiciones de uso', 'andresl', '2020-11-25 09:04:54', 1),
(103, 557, 557, 557, 2, '1', 'SE VERIFICO QUE EL EQUIPO FUNCIONA CORRECTAMENTE&nbsp;', 'andresl', '2020-11-24 10:38:46', 2),
(104, 163, 163, 163, 2, '1', 'se verifico que la banda trotadora se encuentra en buen estado&nbsp;', 'andresl', '2020-11-24 10:46:10', 2),
(105, 1007, 1007, 1007, 2, '1', 'se verifico que el equipo esta funcionando correctamente&nbsp;&nbsp;', 'andresl', '2020-11-24 10:56:19', 2),
(106, 149, 149, 149, 2, '1', 'el equipo se encuentra en estado optimo para el uso&nbsp;', 'andresl', '2020-11-24 11:09:24', 2),
(107, 936, 936, 936, 2, '1', 'EL EQUIPO FUNCIONA CORRECTAMENTE&nbsp;', 'andresl', '2020-11-24 11:42:50', 2),
(108, 435, 435, 435, 2, '1', 'el cable se encuentra en buen estado&nbsp;', 'andresl', '2020-11-24 12:24:57', 2),
(109, 126, 126, 126, 2, '1', 'el equipo se encuentra en buen estado, y se encuentra funcionando correctamente&nbsp;', 'andresl', '2020-11-24 12:39:15', 2),
(110, 106, 106, 106, 2, '1', 'se verifico que el transductor esta funcionando correctamente&nbsp;', 'andresl', '2020-11-24 12:42:57', 2),
(112, 49, 49, 49, 1, '1', 'El equipo se encuentra en óptimas condiciones para el uso&nbsp;', 'andresl', '2020-11-25 09:59:30', 1),
(113, 165, 165, 165, 1, '1', 'La banda de trote se encuentra en óptimas condiciones para el uso', 'andresl', '2020-11-25 10:06:10', 1),
(114, 172, 172, 172, 1, '1', 'El equipo funciona correctamente&nbsp;', 'andresl', '2020-11-25 10:10:11', 1),
(115, 406, 406, 406, 1, '1', 'Se verificó que el equipo se encuentra en óptimas condiciones para el uso', 'andresl', '2020-11-25 10:38:30', 1),
(116, 138, 138, 138, 1, '1', 'El esquipo no se encuentra funcionando actualmente&nbsp;', 'andresl', '2020-11-25 11:00:11', 1),
(119, 158, 158, 158, 1, '1', 'El equipo funciona bien', 'andresl', '2020-11-25 11:31:07', 1),
(120, 237, 237, 237, 1, '1', NULL, 'andresl', '2020-11-25 11:42:58', 1),
(121, 236, 236, 236, 1, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-25 11:49:24', 1),
(122, 87, 87, 87, 1, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-25 11:51:19', 1),
(123, 17, 17, 17, 1, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-25 11:53:41', 1),
(124, 82, 82, 82, 1, '1', 'Sé verifico que el equipo funciona correctamente', 'andresl', '2020-11-25 11:56:03', 1),
(125, 80, 80, 80, 1, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-25 11:57:18', 1),
(126, 226, 226, 226, 1, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-25 12:00:15', 1),
(127, 60, 60, 60, 1, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-25 12:02:37', 1),
(128, 105, 105, 105, 1, '1', 'Se verificó que el equipo se encuentra en óptimas condiciones de uso', 'andresl', '2020-11-25 12:05:44', 1),
(129, 588, 588, 588, 1, '1', '<br>', 'andresl', '2020-11-25 12:26:43', 1),
(130, 1007, 1007, 1007, 2, '1', 'se verifico que el equipo se encuentra en optimas condiciones de uso&nbsp;<div><br></div>', 'andresl', '2020-11-26 10:57:35', 2),
(141, 558, 558, 558, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 13:13:30', 2),
(132, 4, 4, 4, 2, '1', 'se verifico que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-26 11:16:07', 2),
(133, 163, 163, 163, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 11:37:45', 2),
(134, 126, 126, 126, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 11:51:42', 2),
(135, 149, 149, 149, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 12:17:04', 2),
(136, 42, 42, 42, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 12:32:54', 2),
(137, 106, 106, 106, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 12:33:50', 2),
(138, 435, 435, 435, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 12:36:37', 2),
(139, 581, 581, 581, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 12:48:52', 2),
(140, 557, 557, 557, 2, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-11-26 12:56:55', 2),
(143, 312, 312, 312, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 10:23:40', 4),
(144, 249, 249, 249, 4, '1', 'Se verificó que el equipo se encuentra en óptimas condiciones para el uso', 'andresl', '2020-11-28 10:26:14', 4),
(145, 231, 231, 231, 4, '1', 'Se verificó que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-28 10:27:35', 4),
(146, 24, 24, 24, 4, '1', 'Se verificó que el equipo se encuentra en óptimas condiciones para el uso', 'andresl', '2020-11-28 10:36:44', 4),
(147, 242, 242, 242, 4, '1', 'Se verificó que el equipo funciona bien', 'andresl', '2020-11-28 10:40:13', 4),
(148, 252, 252, 252, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 10:48:07', 4),
(149, 586, 586, 586, 4, '1', 'Se agrego el equipo y se verificó que se encuentra funcionando correctamente', 'andresl', '2020-11-28 10:51:42', 4),
(150, 251, 251, 251, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 10:57:41', 4),
(151, 69, 69, 69, 9, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 11:08:42', 9),
(152, 568, 568, 568, 4, '1', 'El equipo se encuentra funcionando&nbsp;', 'andresl', '2020-11-28 11:22:15', 4),
(153, 140, 140, 140, 4, '1', 'El equipo funciona correctamente', 'andresl', '2020-11-28 11:25:25', 4),
(154, 232, 232, 232, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 11:28:46', 4),
(155, 40, 40, 40, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 11:47:56', 4),
(156, 21, 21, 21, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 11:50:36', 4),
(157, 203, 203, 203, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 11:52:22', 4),
(158, 22, 22, 22, 4, '1', 'Se verificó que el Holter funciona correctamente', 'andresl', '2020-11-28 11:53:55', 4),
(159, 200, 200, 200, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 11:58:58', 4),
(160, 199, 199, 199, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 12:02:32', 4),
(161, 247, 247, 247, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 12:31:08', 4),
(162, 633, 633, 633, 7, '1', 'Se entrega a Andrés López debido a que es Windows 7', 'felipem', '2020-11-28 12:32:25', 7),
(163, 552, 552, 552, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 12:36:08', 4),
(164, 338, 338, 338, 4, '1', 'Se grego el equipo y se verificó que funciona correctamente', 'andresl', '2020-11-28 12:59:46', 4),
(165, 230, 230, 230, 4, '1', 'Marcado&nbsp;', 'felipem', '2020-11-28 13:01:31', 4),
(166, 741, 741, 741, 4, '1', 'El equipo se agrego y se verificó que se encontrará funcionando&nbsp;', 'andresl', '2020-11-28 13:05:44', 4),
(167, 167, 167, 167, 4, '1', '<br>', 'felipem', '2020-11-28 13:09:37', 4),
(168, 587, 587, 587, 4, '1', 'Se agrego el equipo y se verificó que funcionara correctamente', 'andresl', '2020-11-28 13:13:27', 4),
(169, 209, 209, 209, 4, '1', 'Se verificó que el equipo se encontraba en buen estado', 'andresl', '2020-11-28 13:35:08', 4),
(170, 97, 97, 97, 4, '1', 'Se verificó que el equipo se encontrará funcionando correctamente', 'andresl', '2020-11-28 13:37:24', 4),
(171, 96, 96, 96, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-11-28 13:39:54', 4),
(280, 96, 96, 96, 17, '1', 'SE ENCUENTRA EN LA OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 10:47:08', 17),
(173, 238, 238, 238, 1, '1', 'se verifico que el equipo funciona correctamente', 'andresl', '2020-11-30 09:11:19', 1),
(174, 105, 105, 105, 1, '1', 'se verifico que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-30 09:13:16', 1),
(175, 236, 236, 236, 1, '1', 'se verifico que el equipo se encuentra en lugar y se encuentra en optimas condiciones de uso&nbsp;', 'andresl', '2020-11-30 09:19:13', 1),
(176, 172, 172, 172, 1, '1', 'se verifico que el equipo se encuentra en optimas condiciones de uso&nbsp;', 'andresl', '2020-11-30 09:24:18', 1),
(177, 165, 165, 165, 1, '1', 'se verifico que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-30 09:29:40', 1),
(178, 158, 158, 158, 1, '1', 'se verifico que el equipo se encuentra en optimas condiciones de uso&nbsp;<br>', 'andresl', '2020-11-30 09:31:34', 1),
(179, 124, 124, 124, 1, '1', 'se verifico que el quipo se encuentra en el lugar y esta en optimas condiciones para el uso&nbsp;<br>', 'andresl', '2020-11-30 09:35:03', 1),
(180, 87, 87, 87, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 09:43:42', 1),
(181, 80, 80, 80, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 09:45:30', 1),
(182, 82, 82, 82, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 09:48:55', 1),
(183, 50, 50, 50, 1, '1', 'se verifico que el equipo se encuentra en el lugar y funciona correctamente<br>', 'andresl', '2020-11-30 09:53:24', 1),
(184, 17, 17, 17, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 09:54:44', 1),
(185, 749, 749, 749, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 09:59:07', 1),
(186, 229, 229, 229, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:08:00', 1),
(187, 6, 6, 6, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:11:50', 1),
(188, 60, 60, 60, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:17:43', 1),
(189, 49, 49, 49, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:21:31', 1),
(190, 226, 226, 226, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:23:40', 1),
(191, 588, 588, 588, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:32:11', 1),
(192, 406, 406, 406, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:35:51', 1),
(193, 582, 582, 582, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:40:20', 1),
(194, 1005, 1005, 1005, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:46:06', 1),
(195, 583, 583, 583, 1, '1', 'se verifico que el equipo se encuentra funcionando correctamente<br>', 'andresl', '2020-11-30 10:51:31', 1),
(196, 584, 584, 584, 1, '1', 'se verifico que el equipo se encuentra en el lugar y esta en optimas condiciones para el uso&nbsp;<br>', 'andresl', '2020-11-30 10:58:14', 1),
(197, 1008, 1008, 1008, 1, '1', 'se verifico que el equipo funciona correctamente&nbsp;', 'andresl', '2020-11-30 14:11:59', 1),
(198, 313, 313, 313, 14, '1', 'se verifico que el quipo se encuentra en el lugar y esta en optimas condiciones para el uso&nbsp;<br>', 'andresl', '2020-12-01 10:29:00', 14),
(199, 160, 160, 160, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTA EN BUEN ESTADO PARA EL USO', 'andresl', '2020-12-01 16:24:59', 9),
(200, 54, 54, 54, 9, '1', '<div>se verifico que el equipo se encuentra funcionando correctamente&nbsp;</div><div><br></div>', 'andresl', '2020-12-01 16:28:30', 9),
(201, 111, 111, 111, 9, '1', '<div>se verifico que el equipo se encuentra funcionando correctamente&nbsp;</div><div><br></div>', 'andresl', '2020-12-01 16:32:56', 9),
(202, 128, 128, 128, 9, '1', '<div>se verifico que el equipo se encuentra funcionando correctamente&nbsp;</div><div><br></div>', 'andresl', '2020-12-01 16:34:45', 9),
(203, 157, 157, 157, 9, '1', '<div>se verifico que el equipo se encuentra funcionando correctamente&nbsp;</div><div><br></div>', 'andresl', '2020-12-01 16:41:59', 9),
(204, 206, 206, 206, 9, '1', 'se verifico que el equipo funciona correctamente&nbsp;', 'andresl', '2020-12-01 17:08:16', 9),
(205, 92, 92, 92, 9, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-12-01 17:09:00', 9),
(206, 170, 170, 170, 9, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-12-01 17:10:34', 9),
(207, 69, 69, 69, 9, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-12-01 17:12:48', 9),
(208, 757, 757, 757, 9, '1', '<span style=\"font-size: 11.998px;\">se verifico que el equipo funciona correctamente&nbsp;</span><br>', 'andresl', '2020-12-01 17:15:36', 9),
(209, 741, 741, 741, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-12-02 16:56:14', 4),
(210, 252, 252, 252, 4, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-12-02 17:22:13', 4),
(211, 46, 46, 46, 6, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-12-02 18:35:29', 6),
(212, 26, 26, 26, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 14:53:40', 13),
(213, 9, 9, 9, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 14:56:25', 13),
(214, 1009, 1009, 1009, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 15:15:51', 13),
(215, 2, 2, 2, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 15:20:13', 13),
(216, 59, 59, 59, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 15:22:00', 13),
(217, 631, 631, 631, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 15:56:13', 13),
(218, 679, 679, 679, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 16:01:16', 13),
(219, 1006, 1006, 1006, 13, '1', 'se verifico que el equipo se encuentra dentro de la unidad y que se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-03 16:09:31', 13),
(220, 34, 34, 34, 6, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-12-04 09:19:22', 6),
(221, 737, 737, 737, 6, '1', 'Se verificó que el equipo se encuentra en el lugar,  por otra parte no se está utilizando', 'andresl', '2020-12-04 09:27:31', 6),
(222, 738, 738, 738, 6, '1', 'Se verificó que el equipo se encuentra en el lugar, pero no se está utilizando', 'andresl', '2020-12-04 09:31:30', 6),
(223, 566, 566, 566, 6, '1', 'Se verificó que el equipo funciona correctamente', 'andresl', '2020-12-04 09:41:53', 6),
(224, 757, 757, 757, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:26:46', 9),
(225, 160, 160, 160, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:37:54', 9),
(226, 243, 243, 243, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN LA UNIDAD Y EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:43:52', 9),
(227, 54, 54, 54, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:46:51', 9),
(228, 157, 157, 157, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:49:25', 9),
(229, 111, 111, 111, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:50:12', 9),
(230, 92, 92, 92, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:52:50', 9),
(231, 83, 83, 83, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:53:41', 9),
(232, 128, 128, 128, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 15:58:07', 9),
(233, 90, 90, 90, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 16:01:37', 9),
(234, 69, 69, 69, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 16:03:50', 9),
(235, 93, 93, 93, 9, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2020-12-07 16:05:57', 9),
(236, 1007, 1007, 1007, 2, '1', 'se verifico que equipo se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-10 08:17:41', 2),
(237, 149, 149, 149, 2, '1', 'se verifico que el equipo se encuentra en optimas condiciones de uso', 'andresl', '2020-12-10 08:21:34', 2),
(238, 106, 106, 106, 2, '1', 'se verifico que el equipo se encuentra en optimas condiciones de uso', 'andresl', '2020-12-10 08:23:32', 2),
(239, 42, 42, 42, 2, '1', 'se verifico que el equipo se encuentra el optimas condiciones de uso', 'andresl', '2020-12-10 08:24:45', 2),
(240, 163, 163, 163, 2, '1', 'se verifico que el equipo se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-10 08:28:27', 2),
(241, 126, 126, 126, 2, '1', 'se verifico que el equipo esta funcionando correctamente', 'andresl', '2020-12-10 08:29:33', 2),
(242, 558, 558, 558, 2, '1', 'se verifico que el lector funcionara correctamente', 'andresl', '2020-12-10 08:33:50', 2),
(243, 435, 435, 435, 2, '1', 'se verifico que el cable ECG  esta en buen estado', 'andresl', '2020-12-10 08:44:07', 2),
(244, 557, 557, 557, 2, '1', 'se verifico que el equipo se encuentra en optimas condiciones de uso', 'andresl', '2020-12-10 08:50:45', 2),
(245, 936, 936, 936, 2, '1', 'se verifico que el celular se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-10 09:16:37', 2),
(246, 581, 581, 581, 2, '1', 'se verifico que el equipo funciona correctamente', 'andresl', '2020-12-10 09:19:26', 2),
(247, 149, 149, 149, 2, '1', 'se verifico que el equipo se encuentra en optimas condiciones para el uso', 'andresl', '2020-12-10 09:20:23', 2),
(248, 1009, 1009, 1009, 16, '1', 'Este equipo fue asignado con el HOLTER 200 pero fue un error de ingreso, se vuelve a dejar sin asignar.', 'felipem', '2020-12-10 11:58:19', 16),
(249, 46, 46, 46, 3, '1', 'Informado por Luis', 'felipem', '2020-12-10 18:51:54', 3),
(250, 630, 630, 630, 17, '1', NULL, 'andresl', '2020-12-15 17:15:21', 17),
(251, 630, 630, 630, 7, '1', 'HOY LO VI EN BODEGA', 'felipem', '2020-12-23 12:54:06', 7),
(252, 630, 630, 630, 17, '1', 'LISTO', 'felipem', '2020-12-23 12:55:47', 17),
(253, 654, 654, 654, 4, '1', 'EL COMPUTADOR SE ENTREGO Y SE VERIFICO QUE FUNCIONA CORRECTAMENTE', 'andresl', '2020-12-29 14:46:16', 4),
(254, 905, 905, 905, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:05:25', 17),
(255, 905, 905, 905, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:08:48', 17),
(256, 954, 954, 954, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:20:00', 17),
(257, 955, 955, 955, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:23:49', 17),
(258, 956, 956, 956, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:26:05', 17),
(259, 957, 957, 957, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:31:04', 17),
(260, 958, 958, 958, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:34:14', 17),
(261, 959, 959, 959, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:36:18', 17),
(262, 960, 960, 960, 17, '1', 'SE REGISTRO EL EQUIPO EN SEDE SFDO SIN UBICACION', 'andresl', '2021-01-08 10:37:55', 17),
(263, 589, 589, 589, 17, '1', 'SE VERIFICO QUE EL EQUIPO ESTA EN OPTIMAS CONDICIONES DE USO , SE HIZO ENTREGA DEL EQUIPO \r\nRESPONSABLE: \r\nNombre: GUSTAVO TRIVIÑO\r\nCC.16743591\r\nCorreo: gutrilop@gmail.com', 'andresl', '2021-01-08 11:00:57', 17),
(264, 622, 622, 622, 17, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCONTRABA DENTRO DE LA UNIDAD', 'andresl', '2021-01-08 11:51:55', 17),
(266, 620, 620, 620, 17, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES PARA EL USO', 'andresl', '2021-01-08 11:58:40', 17),
(267, 615, 615, 615, 17, '1', 'SE VERIFICO QUE EL EQUIP SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2021-01-08 12:37:16', 17),
(268, 616, 616, 616, 17, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2021-01-08 13:03:30', 17),
(269, 617, 617, 617, 17, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES PARA EL USO', 'andresl', '2021-01-08 13:10:12', 17),
(270, 618, 618, 618, 17, '1', 'SE VERIFICO QUE EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2021-01-08 13:19:35', 17),
(271, 732, 732, 732, 17, '1', 'EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES PARA EL USO', 'andresl', '2021-01-08 13:27:54', 17),
(272, 733, 733, 733, 17, '1', 'EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES PARA EL USO', 'andresl', '2021-01-08 13:31:50', 17),
(273, 759, 759, 759, 17, '1', 'EL EQUIPO SE ENCUENTRA EN OPTIMAS CONDICIONES DE USO', 'andresl', '2021-01-08 13:38:33', 17),
(274, 220, 220, 220, 1, '1', 'Se verifica en SFDO con camilo', 'felipem', '2021-01-12 12:57:00', 1),
(275, 57, 57, 57, 1, '1', 'Se verifica en SFDO con Camiloz', 'felipem', '2021-01-12 13:01:18', 1),
(276, 590, 590, 590, 14, '1', NULL, 'andresl', '2021-01-22 17:06:16', 14),
(277, 218, 218, 218, 1, '1', 'FUNCIONAMIENTO NORMAL EN EL HOSPITAL ISAÍAS DUARTE CANCINO', 'camiloz', '2021-01-25 09:46:39', 1),
(278, 209, 209, 209, 1, '1', 'FUNCIONA NORMALMENTE EN EL HOSPITAL ISAÍAS DUARTE CANCINO', 'camiloz', '2021-01-25 09:49:00', 1),
(279, 208, 208, 208, 17, '1', 'SE ENCUENTRA EN OFICINA SEDE SAN FERNANDO', 'camiloz', '2021-01-25 10:13:23', 17),
(281, 97, 97, 97, 17, '1', NULL, 'camiloz', '2021-01-25 10:58:34', 17),
(282, 97, 97, 97, 17, '1', NULL, 'camiloz', '2021-01-25 10:59:11', 17),
(283, 216, 216, 216, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 13:29:58', 17),
(284, 217, 217, 217, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 13:31:08', 17),
(285, 219, 219, 219, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 13:32:42', 17),
(286, 13, 13, 13, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 13:43:06', 17),
(287, 202, 202, 202, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 13:49:08', 17),
(288, 200, 200, 200, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 13:53:36', 17),
(289, 170, 170, 170, 17, '1', 'SE ENCUENTRA EN OFICINA SAN FERNANDO', 'camiloz', '2021-01-25 14:47:35', 17),
(290, 202, 202, 202, 17, '1', 'Sin estuche, procedente de viva1a', 'felipem', '2021-01-25 16:41:40', 17),
(291, 200, 200, 200, 17, '1', 'Con estuche', 'felipem', '2021-01-25 16:42:54', 17),
(292, 13, 13, 13, 17, '1', 'Si estuche proveniente de viva1a', 'felipem', '2021-01-25 16:43:36', 17),
(293, 219, 219, 219, 17, '1', 'Solo con cable', 'felipem', '2021-01-25 17:01:24', 17),
(294, 217, 217, 217, 17, '1', 'Solo con cable', 'felipem', '2021-01-25 17:01:47', 17),
(295, 216, 216, 216, 17, '1', 'Solo con cable', 'felipem', '2021-01-25 17:02:28', 17),
(296, 943, 943, 943, 17, '1', 'Entregado por luis garcia personalmente', 'felipem', '2021-01-25 18:08:31', 17),
(298, 591, 591, 591, 15, '1', 'verificación para la entrega del equipo\r\nel equipo se encuentra en optimas condiciones para el uso, se entrega con todos los programas actualizados y activados \r\nse revisa antes de la entrega a Sebastian Rendon identificado con numero de cédula 1115077383\r\n\r\nel equipo no tiene ubicación definida porque sebastian visita todas las sedes', 'a.lopez', '2021-01-26 10:26:22', 15),
(299, 923, 923, 923, 4, '1', NULL, 'felipem', '2021-01-27 13:06:41', 4),
(300, 924, 924, 924, 4, '1', NULL, 'felipem', '2021-01-27 13:08:49', 4),
(301, 24, 24, 24, 7, '1', NULL, 'a.lopez', '2021-02-01 11:27:34', 7),
(302, 220, 220, 220, 7, '1', 'SE ENTREGA A LA DRA NELLY CIFUENTES', 'felipem', '2021-02-08 19:38:35', 7),
(303, 23, 23, 23, 13, '1', 'EL EQUIPO SE ENCONTRABA EN LA UNIDAD DE ESENSA ESTABA PRESENTANDO FALLAS, PERO YA SE REPARO', 'a.lopez', '2021-02-16 12:12:18', 13),
(304, 58, 58, 58, 7, '1', 'el equipo se encuentra funcionando correctamente', 'a.lopez', '2021-02-16 13:37:21', 7),
(305, 336, 336, 336, 11, '1', NULL, 'a.lopez', '2021-02-19 10:50:49', 11),
(306, 1004, 1004, 1004, 7, '1', 'FLM', 'felipem', '2021-02-23 09:57:58', 7),
(307, 933, 933, 933, 17, '1', 'FLM', 'felipem', '2021-02-23 10:22:30', 17),
(308, 934, 934, 934, 7, '1', 'FLM', 'felipem', '2021-02-23 10:24:44', 7),
(309, 935, 935, 935, 8, '1', 'FLM', 'felipem', '2021-02-23 10:28:55', 8),
(310, 592, 592, 592, 17, '1', 'SE ENTREGA A CALIDAD', 'a.lopez', '2021-02-23 10:35:40', 17),
(311, 594, 594, 594, 17, '1', 'SE ENTREGA A TECNOLOGIA', 'a.lopez', '2021-02-23 10:41:13', 17),
(312, 937, 937, 937, 17, '1', NULL, 'a.lopez', '2021-02-23 10:48:50', 17),
(313, 933, 933, 933, 4, '1', 'se agrego a la unidad HTU como repetidor del router de claro', 'a.lopez', '2021-03-01 11:48:03', 4),
(314, 96, 96, 96, 11, '1', 'SE ENVIA A BODEGA Y SE ENTREGA A ACAMILO EL 29 DE ENERO', 'a.lopez', '2021-03-02 12:24:50', 11),
(315, 208, 208, 208, 11, '1', 'SE ENVIA A BODEGA Y SE ENTREGA A ACAMILO EL 29 DE ENERO', 'a.lopez', '2021-03-02 12:27:39', 11),
(316, 97, 97, 97, 11, '1', 'SE ENVIA A BODEGA Y SE ENTREGA A ACAMILO EL 29 DE ENERO', 'a.lopez', '2021-03-02 12:30:20', 11),
(317, 696, 696, 696, 3, '1', NULL, 'a.lopez', '2021-03-02 15:36:07', 3),
(318, 92, 92, 92, 13, '1', 'se envia  a la sede medisun esensa', 'a.lopez', '2021-03-03 16:17:51', 13),
(319, 634, 634, 634, 6, '1', NULL, 'a.lopez', '2021-03-11 13:28:37', 6),
(320, 822, 822, 822, 6, '1', NULL, 'a.lopez', '2021-03-11 14:14:38', 6),
(321, 804, 804, 804, 3, '1', NULL, 'a.lopez', '2021-03-16 11:35:54', 3),
(322, 593, 593, 593, 6, '1', NULL, 'a.lopez', '2021-04-06 17:25:28', 6),
(323, 592, 592, 592, 6, '1', 'POR VERIFICAR QUE EL EQUIPO SE ENCUENTRE EN LA UNIDAD PORQUE ESTABA AGRAGADO SFDO Y SE HABIA ENTREGADO A CALIDAD EN EL MES DE FEBRERO', 'a.lopez', '2021-04-06 17:29:18', 6),
(324, 153, 153, 153, 6, '1', 'FALTA POR VERIFICAR EL EQUIPO', 'a.lopez', '2021-04-06 17:34:48', 6),
(325, 151, 151, 151, 6, '1', 'FALTA VERIFICAR  EL MODELO DEL EQUIPO', 'a.lopez', '2021-04-06 17:39:00', 6),
(326, 152, 152, 152, 6, '1', 'FALTA POR VERIFICAR EL MODELO DEL EQUIPO', 'a.lopez', '2021-04-06 17:41:49', 6),
(327, 314, 314, 314, 2, '1', NULL, 'a.lopez', '2021-04-08 15:16:14', 2),
(328, 138, 138, 138, 2, '1', 'El equipo se encontraba en la unidad pero, codificado en bodega San Fernando', 'a.lopez', '2021-04-08 15:40:24', 2),
(329, 471, 471, 471, 2, '1', 'EL EQUIPO ESTA AGREGADO A AL UNIDAD PERO NO TIENEN NUMERO SERIAL Y SE PRESTA PARA LA UNIDAD MEDISUN TEQUENDAMA PARA EL JEFE LUIS', 'a.lopez', '2021-04-09 08:38:21', 2),
(330, 4, 4, 4, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:45:23', 2),
(331, 42, 42, 42, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:46:38', 2),
(332, 163, 163, 163, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:47:57', 2),
(333, 81, 81, 81, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:48:49', 2),
(334, 260, 260, 260, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:50:10', 2),
(335, 106, 106, 106, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:51:29', 2),
(336, 350, 350, 350, 2, '1', 'Se verifico que el equipo funciona correctamente', 'a.lopez', '2021-04-09 13:55:23', 2),
(337, 59, 59, 59, 1, '1', 'El equipo se cambió por otro dispositivo holter que no estaba funcionando holter 117', 'a.lopez', '2021-04-12 11:06:32', 1),
(338, 93, 93, 93, 7, '1', NULL, 'a.lopez', '2021-04-12 11:48:07', 7),
(339, 80, 80, 80, 7, '1', NULL, 'a.lopez', '2021-04-12 11:51:00', 7),
(340, 93, 93, 93, 1, '1', 'Se cambió manguera de brazalete y se lo llevó de nu voy a HIDC', 'a.lopez', '2021-04-13 08:55:00', 1),
(341, 80, 80, 80, 1, '1', NULL, 'a.lopez', '2021-04-13 08:57:04', 1),
(342, 209, 209, 209, 7, '1', NULL, 'a.lopez', '2021-04-13 08:59:52', 7),
(343, 82, 82, 82, 7, '1', NULL, 'a.lopez', '2021-04-13 09:02:32', 7),
(344, 514, 514, 514, 1, '1', 'EL EQUIPO SE ENCONTRABA EN LA UNIDAD PERO EL NUMERO DE CODIFICACIÓN NO ERA  EL ASIGNADO', 'a.lopez', '2021-04-13 10:37:30', 1),
(345, 360, 360, 360, 1, '1', NULL, 'a.lopez', '2021-04-13 10:52:24', 1),
(346, 355, 355, 355, 1, '1', NULL, 'a.lopez', '2021-04-13 11:00:45', 1),
(347, 54, 54, 54, 7, '1', NULL, 'a.lopez', '2021-04-13 18:38:09', 7),
(348, 1008, 1008, 1008, 1, '1', 'EL EQUIPO SE ENCUENTRA EN ESTADO OPTIMO', 'a.lopez', '2021-04-14 09:06:09', 1),
(349, 128, 128, 128, 1, '1', NULL, 'a.lopez', '2021-04-14 10:19:17', 1),
(350, 92, 92, 92, 7, '1', 'EL EQUIPO SE HABIA ENVIADO A MEDISUN ESENSA Y A LA FECHAVOLVIO A SAN FERNANDO, NO SE INFORMO LA LLEGADA DEL EQUIPO', 'a.lopez', '2021-04-19 09:51:00', 7),
(351, 94, 94, 94, 7, '1', 'EL EQUIOPOS SE REPORTO COMO DAÑADO SE ENVIA A BODEGA SAN FERNANDO PARA REVISARLO', 'a.lopez', '2021-04-19 09:55:36', 7),
(352, 86, 86, 86, 7, '1', 'EL EQUIPOS E ENVIO A REVISION PORQUE SE ENCONTRABA PRESENTANDO FALLOS', 'a.lopez', '2021-04-19 11:42:02', 7),
(353, 217, 217, 217, 7, '1', NULL, 'a.lopez', '2021-04-19 14:55:57', 7),
(354, 2, 2, 2, 7, '1', NULL, 'a.lopez', '2021-04-19 15:00:35', 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesorios`
--
ALTER TABLE `accesorios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `iddispo` (`iddispo`),
  ADD KEY `Accesorio` (`Accesorio`),
  ADD KEY `id_dispo` (`id_dispo`),
  ADD KEY `tipo_accesorio` (`tipo_accesorio`);

--
-- Indices de la tabla `accesorio_dispo_relacion`
--
ALTER TABLE `accesorio_dispo_relacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_accesorio` (`id_accesorio`),
  ADD KEY `id_dispo` (`id_dispo`);

--
-- Indices de la tabla `acceso_remoto`
--
ALTER TABLE `acceso_remoto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `anydesk_unique` (`anydesk`),
  ADD KEY `unidad` (`unidad`),
  ADD KEY `codigo_equipo` (`codigo_equipo`);

--
-- Indices de la tabla `appgini_csv_import_jobs`
--
ALTER TABLE `appgini_csv_import_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `memberID` (`memberID`);

--
-- Indices de la tabla `calibraciones`
--
ALTER TABLE `calibraciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `tipo_calibracion` (`tipo_calibracion`),
  ADD KEY `empresa` (`empresa`);

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`id_ciudad`),
  ADD UNIQUE KEY `ciudad_unique` (`ciudad`);

--
-- Indices de la tabla `codigoserial`
--
ALTER TABLE `codigoserial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `serial` (`serial`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compras`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `proveedor` (`proveedor`),
  ADD KEY `ciudad_origen` (`ciudad_origen`),
  ADD KEY `comprado_por` (`comprado_por`),
  ADD KEY `tipo_dispo` (`tipo_dispo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `identi` (`identi`);

--
-- Indices de la tabla `consentimieto`
--
ALTER TABLE `consentimieto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_perfil` (`id_perfil`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id_contactos`),
  ADD UNIQUE KEY `identificacion_unique` (`identificacion`),
  ADD KEY `tipo_contacto` (`tipo_contacto`),
  ADD KEY `tipo_iden` (`tipo_iden`),
  ADD KEY `ciudad` (`ciudad`),
  ADD KEY `tipo_relacion` (`tipo_relacion`);

--
-- Indices de la tabla `costos_mtto`
--
ALTER TABLE `costos_mtto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_mtto` (`id_mtto`);

--
-- Indices de la tabla `dispositivos`
--
ALTER TABLE `dispositivos`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20191127095023`
--
ALTER TABLE `dispositivos_backup_20191127095023`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20191127101832`
--
ALTER TABLE `dispositivos_backup_20191127101832`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20191203080334`
--
ALTER TABLE `dispositivos_backup_20191203080334`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20191205094300`
--
ALTER TABLE `dispositivos_backup_20191205094300`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20191227141506`
--
ALTER TABLE `dispositivos_backup_20191227141506`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20201119230142`
--
ALTER TABLE `dispositivos_backup_20201119230142`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20201119231714`
--
ALTER TABLE `dispositivos_backup_20201119231714`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispositivos_backup_20201119233100`
--
ALTER TABLE `dispositivos_backup_20201119233100`
  ADD PRIMARY KEY (`id_dispo`),
  ADD UNIQUE KEY `codigo_unique` (`codigo`),
  ADD UNIQUE KEY `serial_unique` (`serial`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `ubicacion` (`ubicacion`),
  ADD KEY `estado` (`estado`);

--
-- Indices de la tabla `dispo_detalles_tecnicos`
--
ALTER TABLE `dispo_detalles_tecnicos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dispo` (`id_dispo`);

--
-- Indices de la tabla `dispo_fuera`
--
ALTER TABLE `dispo_fuera`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `tipo_fuera_servicio` (`tipo_fuera_servicio`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dispo_codigo` (`dispo_codigo`),
  ADD KEY `tipo_docu` (`tipo_docu`);

--
-- Indices de la tabla `docu_modelo`
--
ALTER TABLE `docu_modelo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_dispo` (`tipo_dispo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`),
  ADD KEY `tipo_documento` (`tipo_documento`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `no_factura_unique` (`no_factura`),
  ADD KEY `id_dispo` (`id_dispo`),
  ADD KEY `proveedor` (`proveedor`);

--
-- Indices de la tabla `fichatecnica`
--
ALTER TABLE `fichatecnica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dispo_codigo` (`dispo_codigo`);

--
-- Indices de la tabla `financiero`
--
ALTER TABLE `financiero`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dispo` (`id_dispo`);

--
-- Indices de la tabla `financiero_mobiliario`
--
ALTER TABLE `financiero_mobiliario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_dispo` (`id_dispo`);

--
-- Indices de la tabla `formatos`
--
ALTER TABLE `formatos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_formato` (`tipo_formato`),
  ADD KEY `tipo_dispositivo` (`tipo_dispositivo`);

--
-- Indices de la tabla `foto_observacion`
--
ALTER TABLE `foto_observacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_observacion` (`id_observacion`);

--
-- Indices de la tabla `grupo_dispo`
--
ALTER TABLE `grupo_dispo`
  ADD PRIMARY KEY (`id_grupo_dispo`),
  ADD UNIQUE KEY `grupo_dispo_unique` (`grupo_dispo`);

--
-- Indices de la tabla `hojadevida`
--
ALTER TABLE `hojadevida`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dispo_codigo` (`dispo_codigo`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_dispo` (`tipo_dispo`),
  ADD KEY `marca` (`marca`),
  ADD KEY `modelo` (`modelo`);

--
-- Indices de la tabla `mantenimientos`
--
ALTER TABLE `mantenimientos`
  ADD PRIMARY KEY (`id_mtto`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `tipo_mtto` (`tipo_mtto`),
  ADD KEY `responsable` (`responsable`),
  ADD KEY `unidad` (`unidad`),
  ADD KEY `estado_final` (`estado_final`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id_marca`),
  ADD UNIQUE KEY `marca_unique` (`marca`);

--
-- Indices de la tabla `marca_modelo`
--
ALTER TABLE `marca_modelo`
  ADD PRIMARY KEY (`id_mmodelo`),
  ADD UNIQUE KEY `modelo_unique` (`modelo`),
  ADD KEY `marca` (`marca`);

--
-- Indices de la tabla `membership_grouppermissions`
--
ALTER TABLE `membership_grouppermissions`
  ADD PRIMARY KEY (`permissionID`),
  ADD UNIQUE KEY `groupID_tableName` (`groupID`,`tableName`);

--
-- Indices de la tabla `membership_groups`
--
ALTER TABLE `membership_groups`
  ADD PRIMARY KEY (`groupID`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `membership_userpermissions`
--
ALTER TABLE `membership_userpermissions`
  ADD PRIMARY KEY (`permissionID`),
  ADD UNIQUE KEY `memberID_tableName` (`memberID`,`tableName`);

--
-- Indices de la tabla `membership_userrecords`
--
ALTER TABLE `membership_userrecords`
  ADD PRIMARY KEY (`recID`),
  ADD UNIQUE KEY `tableName_pkValue` (`tableName`,`pkValue`(150)),
  ADD KEY `pkValue` (`pkValue`),
  ADD KEY `tableName` (`tableName`),
  ADD KEY `memberID` (`memberID`),
  ADD KEY `groupID` (`groupID`);

--
-- Indices de la tabla `membership_users`
--
ALTER TABLE `membership_users`
  ADD PRIMARY KEY (`memberID`),
  ADD KEY `groupID` (`groupID`);

--
-- Indices de la tabla `membership_usersessions`
--
ALTER TABLE `membership_usersessions`
  ADD UNIQUE KEY `memberID_token_agent` (`memberID`,`token`,`agent`),
  ADD KEY `memberID` (`memberID`),
  ADD KEY `expiry_ts` (`expiry_ts`);

--
-- Indices de la tabla `mobiliario`
--
ALTER TABLE `mobiliario`
  ADD PRIMARY KEY (`id_mobiliario`),
  ADD KEY `tipo_mobiliario` (`tipo_mobiliario`),
  ADD KEY `unidad` (`unidad`),
  ADD KEY `grupo` (`grupo`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id_movi`),
  ADD KEY `tipo_movimiento` (`tipo_movimiento`),
  ADD KEY `codigo_dispo` (`codigo_dispo`),
  ADD KEY `origen` (`origen`),
  ADD KEY `destino` (`destino`),
  ADD KEY `responsable` (`responsable`),
  ADD KEY `estado_movi` (`estado_movi`),
  ADD KEY `verifica` (`verifica`),
  ADD KEY `estado_verifica` (`estado_verifica`);

--
-- Indices de la tabla `movi_envio`
--
ALTER TABLE `movi_envio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_movi` (`id_movi`),
  ADD KEY `origen` (`origen`),
  ADD KEY `ciudad` (`ciudad`),
  ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `movi_recepcion`
--
ALTER TABLE `movi_recepcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_movi` (`id_movi`),
  ADD KEY `destino` (`destino`),
  ADD KEY `ciudad` (`ciudad`),
  ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `observaciones`
--
ALTER TABLE `observaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `estado` (`estado`),
  ADD KEY `estado_observacion` (`estado_observacion`),
  ADD KEY `unidad` (`unidad`);

--
-- Indices de la tabla `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tipo_id` (`tipo_id`),
  ADD KEY `rh` (`rh`);

--
-- Indices de la tabla `perfil_familia`
--
ALTER TABLE `perfil_familia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_perfil` (`id_perfil`),
  ADD KEY `parentesco` (`parentesco`);

--
-- Indices de la tabla `perfil_soportes`
--
ALTER TABLE `perfil_soportes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_perfil` (`id_perfil`),
  ADD KEY `tipo_documento` (`tipo_documento`);

--
-- Indices de la tabla `prestador`
--
ALTER TABLE `prestador`
  ADD PRIMARY KEY (`id`),
  ADD KEY `razon_social` (`razon_social`);

--
-- Indices de la tabla `progreso`
--
ALTER TABLE `progreso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_observacion` (`id_observacion`),
  ADD KEY `estado_seguimiento` (`estado_seguimiento`);

--
-- Indices de la tabla `seguridad_social`
--
ALTER TABLE `seguridad_social`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_perfil` (`id_perfil`),
  ADD KEY `tipo_documento` (`tipo_documento`);

--
-- Indices de la tabla `tipo_accesorio`
--
ALTER TABLE `tipo_accesorio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_calibracion`
--
ALTER TABLE `tipo_calibracion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipo_calibracion_unique` (`tipo_calibracion`);

--
-- Indices de la tabla `tipo_contacto`
--
ALTER TABLE `tipo_contacto`
  ADD PRIMARY KEY (`id_tipo_contacto`),
  ADD UNIQUE KEY `tipo_contacto_unique` (`tipo_contacto`);

--
-- Indices de la tabla `tipo_costo`
--
ALTER TABLE `tipo_costo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_dispositivo`
--
ALTER TABLE `tipo_dispositivo`
  ADD PRIMARY KEY (`id_tipodispo`),
  ADD UNIQUE KEY `tipo_dispositivo_unique` (`tipo_dispositivo`),
  ADD KEY `grupo_dispo` (`grupo_dispo`);

--
-- Indices de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  ADD PRIMARY KEY (`id_tipodoc`),
  ADD UNIQUE KEY `tipo_doc_unique` (`tipo_doc`);

--
-- Indices de la tabla `tipo_documento_ss`
--
ALTER TABLE `tipo_documento_ss`
  ADD PRIMARY KEY (`id_tipodoc`),
  ADD UNIQUE KEY `tipo_doc_unique` (`tipo_doc`);

--
-- Indices de la tabla `tipo_estado_dispo`
--
ALTER TABLE `tipo_estado_dispo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_estado_movi`
--
ALTER TABLE `tipo_estado_movi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `estado_movi_unique` (`estado_movi`);

--
-- Indices de la tabla `tipo_estado_observa`
--
ALTER TABLE `tipo_estado_observa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_estado_observaciones`
--
ALTER TABLE `tipo_estado_observaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_estado_verifica`
--
ALTER TABLE `tipo_estado_verifica`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `estado_verificado_unique` (`estado_verificado`);

--
-- Indices de la tabla `tipo_formatos`
--
ALTER TABLE `tipo_formatos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_fuera_servicio`
--
ALTER TABLE `tipo_fuera_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_grupo_mobilia`
--
ALTER TABLE `tipo_grupo_mobilia`
  ADD PRIMARY KEY (`id_gru_mo`);

--
-- Indices de la tabla `tipo_iden`
--
ALTER TABLE `tipo_iden`
  ADD PRIMARY KEY (`id_iden`),
  ADD UNIQUE KEY `tipo_iden_unique` (`tipo_iden`),
  ADD UNIQUE KEY `tipo_iden_abre_unique` (`tipo_iden_abre`);

--
-- Indices de la tabla `tipo_mobiliario`
--
ALTER TABLE `tipo_mobiliario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grupo_mobi` (`grupo_mobi`);

--
-- Indices de la tabla `tipo_movimiento`
--
ALTER TABLE `tipo_movimiento`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipo_movimiento_unique` (`tipo_movimiento`);

--
-- Indices de la tabla `tipo_mtto`
--
ALTER TABLE `tipo_mtto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tipo_mtto_unique` (`tipo_mtto`);

--
-- Indices de la tabla `tipo_parentesco`
--
ALTER TABLE `tipo_parentesco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_razon_social`
--
ALTER TABLE `tipo_razon_social`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `razon_social_unique` (`razon_social`);

--
-- Indices de la tabla `tipo_relacion`
--
ALTER TABLE `tipo_relacion`
  ADD PRIMARY KEY (`id_tiporelac`),
  ADD UNIQUE KEY `tipo_relac_unique` (`tipo_relac`);

--
-- Indices de la tabla `tipo_rh`
--
ALTER TABLE `tipo_rh`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`id_unidades`),
  ADD UNIQUE KEY `nombre_unique` (`nombre`),
  ADD UNIQUE KEY `abreviado_unique` (`abreviado`),
  ADD KEY `ciudad` (`ciudad`),
  ADD KEY `razon_social` (`razon_social`),
  ADD KEY `prestador` (`prestador`);

--
-- Indices de la tabla `verificacion`
--
ALTER TABLE `verificacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codigo` (`codigo`),
  ADD KEY `ubicacion` (`ubicacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesorios`
--
ALTER TABLE `accesorios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `accesorio_dispo_relacion`
--
ALTER TABLE `accesorio_dispo_relacion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `acceso_remoto`
--
ALTER TABLE `acceso_remoto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `calibraciones`
--
ALTER TABLE `calibraciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  MODIFY `id_ciudad` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `codigoserial`
--
ALTER TABLE `codigoserial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compras` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `consentimieto`
--
ALTER TABLE `consentimieto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id_contactos` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `costos_mtto`
--
ALTER TABLE `costos_mtto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `dispositivos`
--
ALTER TABLE `dispositivos`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1010;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20191127095023`
--
ALTER TABLE `dispositivos_backup_20191127095023`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20191127101832`
--
ALTER TABLE `dispositivos_backup_20191127101832`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20191203080334`
--
ALTER TABLE `dispositivos_backup_20191203080334`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20191205094300`
--
ALTER TABLE `dispositivos_backup_20191205094300`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20191227141506`
--
ALTER TABLE `dispositivos_backup_20191227141506`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20201119230142`
--
ALTER TABLE `dispositivos_backup_20201119230142`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=259;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20201119231714`
--
ALTER TABLE `dispositivos_backup_20201119231714`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;

--
-- AUTO_INCREMENT de la tabla `dispositivos_backup_20201119233100`
--
ALTER TABLE `dispositivos_backup_20201119233100`
  MODIFY `id_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1004;

--
-- AUTO_INCREMENT de la tabla `dispo_detalles_tecnicos`
--
ALTER TABLE `dispo_detalles_tecnicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dispo_fuera`
--
ALTER TABLE `dispo_fuera`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `docu_modelo`
--
ALTER TABLE `docu_modelo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fichatecnica`
--
ALTER TABLE `fichatecnica`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `financiero`
--
ALTER TABLE `financiero`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `financiero_mobiliario`
--
ALTER TABLE `financiero_mobiliario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `formatos`
--
ALTER TABLE `formatos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `foto_observacion`
--
ALTER TABLE `foto_observacion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grupo_dispo`
--
ALTER TABLE `grupo_dispo`
  MODIFY `id_grupo_dispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `hojadevida`
--
ALTER TABLE `hojadevida`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `mantenimientos`
--
ALTER TABLE `mantenimientos`
  MODIFY `id_mtto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id_marca` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `marca_modelo`
--
ALTER TABLE `marca_modelo`
  MODIFY `id_mmodelo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `membership_grouppermissions`
--
ALTER TABLE `membership_grouppermissions`
  MODIFY `permissionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1137;

--
-- AUTO_INCREMENT de la tabla `membership_groups`
--
ALTER TABLE `membership_groups`
  MODIFY `groupID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `membership_userpermissions`
--
ALTER TABLE `membership_userpermissions`
  MODIFY `permissionID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;

--
-- AUTO_INCREMENT de la tabla `membership_userrecords`
--
ALTER TABLE `membership_userrecords`
  MODIFY `recID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2040;

--
-- AUTO_INCREMENT de la tabla `mobiliario`
--
ALTER TABLE `mobiliario`
  MODIFY `id_mobiliario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id_movi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT de la tabla `movi_envio`
--
ALTER TABLE `movi_envio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT de la tabla `movi_recepcion`
--
ALTER TABLE `movi_recepcion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `observaciones`
--
ALTER TABLE `observaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `perfil_familia`
--
ALTER TABLE `perfil_familia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfil_soportes`
--
ALTER TABLE `perfil_soportes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `prestador`
--
ALTER TABLE `prestador`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `progreso`
--
ALTER TABLE `progreso`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seguridad_social`
--
ALTER TABLE `seguridad_social`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_accesorio`
--
ALTER TABLE `tipo_accesorio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_calibracion`
--
ALTER TABLE `tipo_calibracion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_contacto`
--
ALTER TABLE `tipo_contacto`
  MODIFY `id_tipo_contacto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_costo`
--
ALTER TABLE `tipo_costo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_dispositivo`
--
ALTER TABLE `tipo_dispositivo`
  MODIFY `id_tipodispo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `tipo_documento`
--
ALTER TABLE `tipo_documento`
  MODIFY `id_tipodoc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `tipo_documento_ss`
--
ALTER TABLE `tipo_documento_ss`
  MODIFY `id_tipodoc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_estado_dispo`
--
ALTER TABLE `tipo_estado_dispo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tipo_estado_movi`
--
ALTER TABLE `tipo_estado_movi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_estado_observa`
--
ALTER TABLE `tipo_estado_observa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_estado_observaciones`
--
ALTER TABLE `tipo_estado_observaciones`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_estado_verifica`
--
ALTER TABLE `tipo_estado_verifica`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_formatos`
--
ALTER TABLE `tipo_formatos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_fuera_servicio`
--
ALTER TABLE `tipo_fuera_servicio`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_grupo_mobilia`
--
ALTER TABLE `tipo_grupo_mobilia`
  MODIFY `id_gru_mo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_iden`
--
ALTER TABLE `tipo_iden`
  MODIFY `id_iden` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_mobiliario`
--
ALTER TABLE `tipo_mobiliario`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_movimiento`
--
ALTER TABLE `tipo_movimiento`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_mtto`
--
ALTER TABLE `tipo_mtto`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_parentesco`
--
ALTER TABLE `tipo_parentesco`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_razon_social`
--
ALTER TABLE `tipo_razon_social`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_relacion`
--
ALTER TABLE `tipo_relacion`
  MODIFY `id_tiporelac` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_rh`
--
ALTER TABLE `tipo_rh`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `unidades`
--
ALTER TABLE `unidades`
  MODIFY `id_unidades` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `verificacion`
--
ALTER TABLE `verificacion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=355;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
