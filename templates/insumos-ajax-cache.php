<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'insumos';

		/* data for selected record, or defaults if none is selected */
		var data = {
			tipo_insumo: <?php echo json_encode(array('id' => $rdata['tipo_insumo'], 'value' => $rdata['tipo_insumo'], 'text' => $jdata['tipo_insumo'])); ?>,
			marca: <?php echo json_encode(array('id' => $rdata['marca'], 'value' => $rdata['marca'], 'text' => $jdata['marca'])); ?>,
			presentacion: <?php echo json_encode(array('id' => $rdata['presentacion'], 'value' => $rdata['presentacion'], 'text' => $jdata['presentacion'])); ?>,
			id_compra: <?php echo json_encode(array('id' => $rdata['id_compra'], 'value' => $rdata['id_compra'], 'text' => $jdata['id_compra'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for tipo_insumo */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'tipo_insumo' && d.id == data.tipo_insumo.id)
				return { results: [ data.tipo_insumo ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for marca */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'marca' && d.id == data.marca.id)
				return { results: [ data.marca ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for presentacion */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'presentacion' && d.id == data.presentacion.id)
				return { results: [ data.presentacion ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_compra */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_compra' && d.id == data.id_compra.id)
				return { results: [ data.id_compra ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

