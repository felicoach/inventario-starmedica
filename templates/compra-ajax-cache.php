<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'compra';

		/* data for selected record, or defaults if none is selected */
		var data = {
			id_articulos: <?php echo json_encode(array('id' => $rdata['id_articulos'], 'value' => $rdata['id_articulos'], 'text' => $jdata['id_articulos'])); ?>,
			id_dispositivosbio: <?php echo json_encode(array('id' => $rdata['id_dispositivosbio'], 'value' => $rdata['id_dispositivosbio'], 'text' => $jdata['id_dispositivosbio'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for id_articulos */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_articulos' && d.id == data.id_articulos.id)
				return { results: [ data.id_articulos ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_dispositivosbio */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_dispositivosbio' && d.id == data.id_dispositivosbio.id)
				return { results: [ data.id_dispositivosbio ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

