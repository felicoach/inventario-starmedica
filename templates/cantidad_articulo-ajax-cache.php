<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'cantidad_articulo';

		/* data for selected record, or defaults if none is selected */
		var data = {
			id_articulo: <?php echo json_encode(array('id' => $rdata['id_articulo'], 'value' => $rdata['id_articulo'], 'text' => $jdata['id_articulo'])); ?>,
			nombe_articulo: <?php echo json_encode($jdata['nombe_articulo']); ?>,
			id_compra: <?php echo json_encode(array('id' => $rdata['id_compra'], 'value' => $rdata['id_compra'], 'text' => $jdata['id_compra'])); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for id_articulo */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_articulo' && d.id == data.id_articulo.id)
				return { results: [ data.id_articulo ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for id_articulo autofills */
		cache.addCheck(function(u, d) {
			if(u != tn + '_autofill.php') return false;

			for(var rnd in d) if(rnd.match(/^rnd/)) break;

			if(d.mfk == 'id_articulo' && d.id == data.id_articulo.id) {
				$j('#nombe_articulo' + d[rnd]).html(data.nombe_articulo);
				return true;
			}

			return false;
		});

		/* saved value for id_compra */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'id_compra' && d.id == data.id_compra.id)
				return { results: [ data.id_compra ], more: false, elapsed: 0.01 };
			return false;
		});

		cache.start();
	});
</script>

