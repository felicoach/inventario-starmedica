<?php
	$rdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $rdata)));
	$jdata = array_map('to_utf8', array_map('nl2br', array_map('html_attr_tags_ok', $jdata)));
?>
<script>
	$j(function() {
		var tn = 'codigoserial';

		/* data for selected record, or defaults if none is selected */
		var data = {
			codigo: <?php echo json_encode(array('id' => $rdata['codigo'], 'value' => $rdata['codigo'], 'text' => $jdata['codigo'])); ?>,
			serial: <?php echo json_encode(array('id' => $rdata['serial'], 'value' => $rdata['serial'], 'text' => $jdata['serial'])); ?>,
			tipo_dispo: <?php echo json_encode($jdata['tipo_dispo']); ?>
		};

		/* initialize or continue using AppGini.cache for the current table */
		AppGini.cache = AppGini.cache || {};
		AppGini.cache[tn] = AppGini.cache[tn] || AppGini.ajaxCache();
		var cache = AppGini.cache[tn];

		/* saved value for codigo */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'codigo' && d.id == data.codigo.id)
				return { results: [ data.codigo ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for serial */
		cache.addCheck(function(u, d) {
			if(u != 'ajax_combo.php') return false;
			if(d.t == tn && d.f == 'serial' && d.id == data.serial.id)
				return { results: [ data.serial ], more: false, elapsed: 0.01 };
			return false;
		});

		/* saved value for serial autofills */
		cache.addCheck(function(u, d) {
			if(u != tn + '_autofill.php') return false;

			for(var rnd in d) if(rnd.match(/^rnd/)) break;

			if(d.mfk == 'serial' && d.id == data.serial.id) {
				$j('#tipo_dispo' + d[rnd]).html(data.tipo_dispo);
				return true;
			}

			return false;
		});

		cache.start();
	});
</script>

